.class public Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;
.super Landroid/os/AsyncTask;
.source "SamsungPrinterStatus.java"

# interfaces
.implements Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SamsungPrinterStatus"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

.field private mDeviceName:Ljava/lang/String;

.field private mIPAddress:Ljava/lang/String;

.field private mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

.field private mReturnMessenger:Landroid/os/Messenger;

.field private mStatus:Ljava/lang/String;

.field private mStopMonitoring:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Message;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mContext:Landroid/content/Context;

    .line 32
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 33
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    .line 34
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    .line 36
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceName:Ljava/lang/String;

    .line 38
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 49
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mContext:Landroid/content/Context;

    .line 50
    if-eqz p2, :cond_0

    .line 51
    iget-object v0, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    .line 52
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    const-string v1, "printer-address"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    .line 56
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStopMonitoring:Z

    .line 57
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 58
    return-void

    .line 54
    :cond_0
    iput-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mContext:Landroid/content/Context;

    .line 32
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 33
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    .line 36
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceName:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 41
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    .line 43
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStopMonitoring:Z

    .line 45
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 46
    return-void
.end method

.method private getHPAlertString(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 250
    .local p1, "alerts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_38

    .line 251
    const-string v0, "coveropen"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cover-open"

    .line 309
    :goto_0
    return-object v0

    .line 252
    :cond_0
    const-string v0, "jam"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "jam"

    goto :goto_0

    .line 253
    :cond_1
    const-string v0, "subunit_missing"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "service-request"

    goto :goto_0

    .line 254
    :cond_2
    const-string v0, "subunit_life_almost_over"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "service-request"

    goto :goto_0

    .line 255
    :cond_3
    const-string v0, "subunit_life_over"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "service-request"

    goto :goto_0

    .line 256
    :cond_4
    const-string v0, "subunit_almost_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "service-request"

    goto :goto_0

    .line 257
    :cond_5
    const-string v0, "subunit_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "service-request"

    goto :goto_0

    .line 258
    :cond_6
    const-string v0, "subunit_almost_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "service-request"

    goto :goto_0

    .line 259
    :cond_7
    const-string v0, "subunit_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "service-request"

    goto :goto_0

    .line 260
    :cond_8
    const-string v0, "subunit_near_limit"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "service-request"

    goto :goto_0

    .line 261
    :cond_9
    const-string v0, "subunit_at_limit"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "service-request"

    goto :goto_0

    .line 262
    :cond_a
    const-string v0, "subunit_opened"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "service-request"

    goto :goto_0

    .line 263
    :cond_b
    const-string v0, "subunit_closed"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 264
    :cond_c
    const-string v0, "subunit_turned_on"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 265
    :cond_d
    const-string v0, "subunit_turned_off"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 266
    :cond_e
    const-string v0, "subunit_offline"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 267
    :cond_f
    const-string v0, "subunit_power_saver"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 268
    :cond_10
    const-string v0, "subunit_warming_up"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 269
    :cond_11
    const-string v0, "subunit_added"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 270
    :cond_12
    const-string v0, "subunit_removed"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 271
    :cond_13
    const-string v0, "subunit_resource_added"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 272
    :cond_14
    const-string v0, "subunit_resource_removed"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 273
    :cond_15
    const-string v0, "subunit_recoverable_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 274
    :cond_16
    const-string v0, "subunit_unrecoverable_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 275
    :cond_17
    const-string v0, "subunit_recoverable_storage_error"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 276
    :cond_18
    const-string v0, "subunit_unrecoverable_storage_error"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 277
    :cond_19
    const-string v0, "subunit_motor_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 278
    :cond_1a
    const-string v0, "subunit_memory_exhausted"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 279
    :cond_1b
    const-string v0, "subunit_under_temperature"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 280
    :cond_1c
    const-string v0, "subunit_over_temperature"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 281
    :cond_1d
    const-string v0, "subunit_timing_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 282
    :cond_1e
    const-string v0, "subunit_thermistor_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 283
    :cond_1f
    const-string v0, "dooropen"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    const-string v0, "cover-open"

    goto/16 :goto_0

    .line 284
    :cond_20
    const-string v0, "power_down"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, "device-offline"

    goto/16 :goto_0

    .line 285
    :cond_21
    const-string v0, "input_media_tray_missing"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const-string v0, "input-media-supply-empty"

    goto/16 :goto_0

    .line 286
    :cond_22
    const-string v0, "input_media_supply_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    const-string v0, "input-media-supply-empty"

    goto/16 :goto_0

    .line 287
    :cond_23
    const-string v0, "marker_fuser_under_temperature"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    const-string v0, "unknown"

    goto/16 :goto_0

    .line 288
    :cond_24
    const-string v0, "marker_fuser_over_temperature"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string v0, "unknown"

    goto/16 :goto_0

    .line 289
    :cond_25
    const-string v0, "marker_fuser_timing_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    const-string v0, "unknown"

    goto/16 :goto_0

    .line 290
    :cond_26
    const-string v0, "marker_fuser_thermistor_failure"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 291
    :cond_27
    const-string v0, "marker_toner_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "marker-toner-empty"

    goto/16 :goto_0

    .line 292
    :cond_28
    const-string v0, "marker_ink_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    const-string v0, "marker-ink-empty"

    goto/16 :goto_0

    .line 293
    :cond_29
    const-string v0, "marker_toner_almost_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const-string v0, "marker-toner-almost-empty"

    goto/16 :goto_0

    .line 294
    :cond_2a
    const-string v0, "marker_ink_almost_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v0, "marker-ink-almost-empty"

    goto/16 :goto_0

    .line 295
    :cond_2b
    const-string v0, "marker_waste_toner_receptacle_almost_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 296
    :cond_2c
    const-string v0, "marker_waste_ink_receptacle_almost_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 297
    :cond_2d
    const-string v0, "marker_waste_toner_receptacle_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 298
    :cond_2e
    const-string v0, "marker_waste_ink_receptacle_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 299
    :cond_2f
    const-string v0, "marker_opc_life_almost_over"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 300
    :cond_30
    const-string v0, "marker_opc_life_over"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 301
    :cond_31
    const-string v0, "marker_developer_almost_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 302
    :cond_32
    const-string v0, "marker_developer_empty"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 303
    :cond_33
    const-string v0, "marker_toner_cartridge_missing"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 304
    :cond_34
    const-string v0, "media_path_media_tray_missing"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 305
    :cond_35
    const-string v0, "media_path_media_tray_almost_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 306
    :cond_36
    const-string v0, "media_path_media_tray_full"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 307
    :cond_37
    const-string v0, "media_path_cannot_duplex_media_selected"

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    const-string v0, "service-request"

    goto/16 :goto_0

    .line 309
    :cond_38
    const-string v0, "unknown"

    goto/16 :goto_0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v9, 0x0

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "alerts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    iget-boolean v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStopMonitoring:Z

    if-eqz v6, :cond_0

    .line 212
    return-object v9

    .line 157
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceName:Ljava/lang/String;

    .line 158
    sget-boolean v6, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v6, :cond_1

    const-string v6, "SamsungPrinterStatus"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "device name = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_1
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    .line 161
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    const-string v7, "idl"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 162
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterAlerts(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 169
    :cond_2
    :goto_1
    const-string v3, "print-state-unknown"

    .line 170
    .local v3, "hpStatus":Ljava/lang/String;
    const-string v2, "unknown"

    .line 171
    .local v2, "hpAlert":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    if-nez v6, :cond_5

    .line 172
    const-string v3, "print-state-blocked"

    .line 173
    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->getHPAlertString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 178
    :goto_2
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    if-eqz v6, :cond_4

    .line 179
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 180
    .local v4, "readyBundle":Landroid/os/Bundle;
    const-string v6, "printer-status"

    invoke-virtual {v4, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v6, "print-state-blocked"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 182
    const-string v6, "print-job-blocked-info"

    invoke-virtual {v4, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_3
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v5, "returnIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_6

    .line 186
    const-string v6, "org.androidprinting.intent.ACTION_RETURN_GET_PRINTER_STATUS"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    const-string v6, "printer-address"

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-virtual {v5, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 194
    :goto_3
    const-string v6, "request-action"

    const-string v7, "org.androidprinting.intent.ACTION_RETURN_GET_PRINTER_STATUS"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    if-eqz v6, :cond_4

    .line 197
    :try_start_1
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mReturnMessenger:Landroid/os/Messenger;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v8, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 204
    .end local v4    # "readyBundle":Landroid/os/Bundle;
    .end local v5    # "returnIntent":Landroid/content/Intent;
    :cond_4
    :goto_4
    const/4 v6, 0x0

    :try_start_2
    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {p0, v6}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->publishProgress([Ljava/lang/Object;)V

    .line 205
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 206
    :catch_0
    move-exception v1

    .line 208
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 164
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "hpAlert":Ljava/lang/String;
    .end local v3    # "hpStatus":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 166
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 175
    .end local v1    # "e":Landroid/os/RemoteException;
    .restart local v2    # "hpAlert":Ljava/lang/String;
    .restart local v3    # "hpStatus":Ljava/lang/String;
    :cond_5
    const-string v3, "print-state-idle"

    goto :goto_2

    .line 191
    .restart local v4    # "readyBundle":Landroid/os/Bundle;
    .restart local v5    # "returnIntent":Landroid/content/Intent;
    :cond_6
    const-string v6, "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v6, "print-error"

    const-string v7, "communication-error"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 198
    :catch_2
    move-exception v6

    goto :goto_4
.end method

.method public getMediaSizes(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    const/4 v2, 0x0

    .line 369
    .local v2, "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v3, :cond_0

    .line 371
    :try_start_0
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 372
    .local v0, "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0, p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 375
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v3, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 381
    .end local v0    # "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 376
    :catch_0
    move-exception v1

    .line 377
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getPrinterName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 317
    const/4 v1, 0x0

    .line 319
    .local v1, "printerName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v2, :cond_0

    .line 321
    :try_start_0
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v2, p1}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 322
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "SamsungPrinterStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "printerName = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :cond_0
    :goto_0
    return-object v1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 332
    const/4 v1, 0x0

    .line 334
    .local v1, "status":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v2, :cond_0

    .line 336
    :try_start_0
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v2, p1}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 337
    sget-boolean v2, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "SamsungPrinterStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "status = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :cond_0
    :goto_0
    return-object v1

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isColorPrinter(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v2, 0x0

    .line 349
    .local v2, "isColor":Z
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v3, :cond_0

    .line 351
    :try_start_0
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 352
    .local v0, "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 353
    invoke-virtual {v0, p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 355
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v3, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v2, 0x1

    .line 356
    :goto_0
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "SamsungPrinterStatus"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isColor = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 363
    .end local v0    # "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :cond_0
    :goto_1
    return v2

    .line 355
    .restart local v0    # "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 358
    .end local v0    # "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :catch_0
    move-exception v1

    .line 359
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onDeviceManagerConnectorStarted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 73
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->removeDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)Z

    .line 78
    return-void

    .line 75
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 235
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->releaseService()V

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 243
    :cond_0
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 219
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 222
    new-instance v0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-direct {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;-><init>()V

    .line 223
    .local v0, "deviceInfo":Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceIpAddress(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceStatus(Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    const/4 v2, 0x3

    invoke-interface {v1, v0, v2}, Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;->onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V

    .line 230
    :cond_0
    return-void
.end method

.method public startMonitoring()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 64
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->InitializeDiscovery()Z

    .line 65
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->addDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)V

    .line 66
    return-void
.end method

.method public stopMonitoring(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->mStopMonitoring:Z

    .line 247
    return-void
.end method
