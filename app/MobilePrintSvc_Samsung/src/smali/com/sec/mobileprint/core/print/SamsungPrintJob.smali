.class public Lcom/sec/mobileprint/core/print/SamsungPrintJob;
.super Ljava/lang/Object;
.source "SamsungPrintJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;,
        Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;,
        Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;
    }
.end annotation


# static fields
.field public static final PRINT_JOB_ERROR_STATUS_INVALID_CONTENTS:I = 0x7

.field public static final PRINT_JOB_ERROR_STATUS_NOT_SUPPORTED_CONTENTS:I = 0x3

.field public static final PRINT_JOB_ERROR_STATUS_NOT_SUPPORTED_PRINTER:I = 0x6

.field public static final PRINT_JOB_ERROR_STATUS_OUTPUT_STREAM_ERROR:I = 0x4

.field public static final PRINT_JOB_ERROR_STATUS_OUT_OF_MEMEORY:I = 0x1

.field public static final PRINT_JOB_ERROR_STATUS_PRINTING_OPTION_ERROR:I = 0x2

.field public static final PRINT_JOB_ERROR_STATUS_PRINT_SERVICE_STILL_RUNNING:I = 0x5

.field public static final PRINT_JOB_STATUS_CANCELED:I = 0x6

.field public static final PRINT_JOB_STATUS_COMPLETED_JOB:I = 0x5

.field public static final PRINT_JOB_STATUS_COMPLETED_PAGE:I = 0x4

.field public static final PRINT_JOB_STATUS_ERROR:I = 0x7

.field public static final PRINT_JOB_STATUS_PREPARING:I = 0x0

.field public static final PRINT_JOB_STATUS_PRINTING:I = 0x3

.field public static final PRINT_JOB_STATUS_QUEUED:I = 0x64

.field public static final PRINT_JOB_STATUS_START_JOB:I = 0x1

.field public static final PRINT_JOB_STATUS_START_PAGE:I = 0x2


# instance fields
.field private mConnectionType:Lcom/sec/mobileprint/core/print/ConnectionType;

.field private mDeleteFilesAfterPrinting:Z

.field private mDeviceName:Ljava/lang/String;

.field private mFilePath:[Ljava/lang/String;

.field private mIPAddress:Ljava/lang/String;

.field private mJobId:I

.field private mPid:I

.field private mPortNo:I

.field private mSettings:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

.field private mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

.field private mVid:I


# direct methods
.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;II[Ljava/lang/String;)V
    .locals 2
    .param p1, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p2, "devicename"    # Ljava/lang/String;
    .param p3, "pid"    # I
    .param p4, "vid"    # I
    .param p5, "mFilePath"    # [Ljava/lang/String;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    .line 92
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getNewJobId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobId(I)V

    .line 93
    invoke-direct {p0, p5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setFilePaths([Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 95
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPortNo(I)V

    .line 96
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setDeviceName(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPid(I)V

    .line 99
    invoke-virtual {p0, p4}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setVid(I)V

    .line 100
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setConnectionType(Lcom/sec/mobileprint/core/print/ConnectionType;)V

    .line 101
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .line 102
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    const/16 v1, 0x64

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 103
    return-void
.end method

.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "ipAddress"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    .line 78
    invoke-virtual {p0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setDeviceName(Ljava/lang/String;)V

    .line 79
    invoke-direct {p0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 80
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPortNo(I)V

    .line 81
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    .line 82
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setConnectionType(Lcom/sec/mobileprint/core/print/ConnectionType;)V

    .line 83
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPid(I)V

    .line 84
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setVid(I)V

    .line 85
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .line 86
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mFilePath:[Ljava/lang/String;

    .line 87
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .line 88
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    const/16 v1, 0x64

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 89
    return-void
.end method

.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "ipAddress"    # Ljava/lang/String;
    .param p4, "mFilePath"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    .line 106
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getNewJobId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobId(I)V

    .line 107
    invoke-direct {p0, p4}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setFilePaths([Ljava/lang/String;)V

    .line 108
    invoke-direct {p0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 109
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPortNo(I)V

    .line 110
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    .line 111
    invoke-virtual {p0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setDeviceName(Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setConnectionType(Lcom/sec/mobileprint/core/print/ConnectionType;)V

    .line 113
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPid(I)V

    .line 114
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setVid(I)V

    .line 115
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .line 116
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    const/16 v1, 0x64

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 117
    return-void
.end method

.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "mFilePath"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    .line 51
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getNewJobId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobId(I)V

    .line 52
    invoke-direct {p0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setFilePaths([Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 54
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPortNo(I)V

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    .line 56
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setConnectionType(Lcom/sec/mobileprint/core/print/ConnectionType;)V

    .line 57
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setPid(I)V

    .line 58
    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setVid(I)V

    .line 59
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    const/16 v1, 0x64

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 61
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Z)V
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mFilePath:[Ljava/lang/String;

    return-object v0
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;II[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 6
    .param p0, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "vid"    # I
    .param p4, "mFilePath"    # [Ljava/lang/String;

    .prologue
    .line 125
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;II[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 1
    .param p0, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "mFilePath"    # [Ljava/lang/String;

    .prologue
    .line 129
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 1
    .param p0, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "mFilePath"    # [Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;Ljava/io/FileDescriptor;Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;)V
    .locals 3
    .param p0, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "mFileDescripter"    # Ljava/io/FileDescriptor;
    .param p4, "listener"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob$OnDocumentFileRecvd;

    .prologue
    .line 138
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;

    new-instance v1, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    aput-object p4, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$CopyFileTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 139
    return-void
.end method

.method private setFilePaths([Ljava/lang/String;)V
    .locals 0
    .param p1, "mFilePath"    # [Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mFilePath:[Ljava/lang/String;

    .line 171
    return-void
.end method

.method private setIPAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIPAddress"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mIPAddress:Ljava/lang/String;

    .line 163
    return-void
.end method

.method private setPrintJobSettings(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V
    .locals 0
    .param p1, "mSettings"    # Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mSettings:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    .line 155
    return-void
.end method


# virtual methods
.method public getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mConnectionType:Lcom/sec/mobileprint/core/print/ConnectionType;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePaths()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mFilePath:[Ljava/lang/String;

    return-object v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mIPAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getJobId()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mJobId:I

    return v0
.end method

.method public getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    return-object v0
.end method

.method public getPid()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mPid:I

    return v0
.end method

.method public getPortNo()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mPortNo:I

    return v0
.end method

.method public getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mSettings:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    return-object v0
.end method

.method public getVid()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mVid:I

    return v0
.end method

.method public setConnectionType(Lcom/sec/mobileprint/core/print/ConnectionType;)V
    .locals 0
    .param p1, "mConnectionType"    # Lcom/sec/mobileprint/core/print/ConnectionType;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mConnectionType:Lcom/sec/mobileprint/core/print/ConnectionType;

    .line 202
    return-void
.end method

.method public setDeleteFilesAfterPrinting(Z)V
    .locals 0
    .param p1, "mDeleteFilesAfterPrinting"    # Z

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    .line 238
    return-void
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDeviceName"    # Ljava/lang/String;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeviceName:Ljava/lang/String;

    .line 211
    return-void
.end method

.method public setJobId(I)V
    .locals 0
    .param p1, "mJobId"    # I

    .prologue
    .line 146
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mJobId:I

    .line 147
    return-void
.end method

.method public setJobStatus(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    iput p1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 192
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    iput p2, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    .line 193
    return-void
.end method

.method public setJobStatus(Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    iget v1, p1, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 187
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mStatus:Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    iget v1, p1, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    iput v1, v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    .line 188
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "mPid"    # I

    .prologue
    .line 219
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mPid:I

    .line 220
    return-void
.end method

.method public setPortNo(I)V
    .locals 0
    .param p1, "mPortNo"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mPortNo:I

    .line 179
    return-void
.end method

.method public setVid(I)V
    .locals 0
    .param p1, "mVid"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mVid:I

    .line 229
    return-void
.end method

.method public shouldDeleteFilesAfterPrinting()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->mDeleteFilesAfterPrinting:Z

    return v0
.end method
