.class public Lcom/sec/mobileprint/core/utils/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyFileUsingChannel(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p0, "pathSource"    # Ljava/lang/String;
    .param p1, "pathDest"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v7, 0x0

    .line 93
    .local v7, "inputStream":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 94
    .local v9, "outputStream":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 95
    .local v0, "fcin":Ljava/nio/channels/FileChannel;
    const/4 v5, 0x0

    .line 97
    .local v5, "fcout":Ljava/nio/channels/FileChannel;
    const/4 v11, 0x1

    .line 100
    .local v11, "result":Z
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .local v8, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {p1}, Lcom/sec/mobileprint/core/utils/FileUtil;->createFolder(Ljava/lang/String;)Z

    .line 104
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 106
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .local v10, "outputStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 107
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 110
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v3

    .line 111
    .local v3, "fileSize":J
    const-wide/16 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_13
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 123
    if-eqz v5, :cond_0

    .line 124
    :try_start_3
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_e

    .line 130
    :cond_0
    :goto_0
    :try_start_4
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f

    .line 136
    :goto_1
    :try_start_5
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_10

    .line 142
    :goto_2
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_11

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 149
    .end local v3    # "fileSize":J
    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    :goto_3
    return v11

    .line 113
    :catch_0
    move-exception v6

    .line 115
    .local v6, "e":Ljava/io/FileNotFoundException;
    :goto_4
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 116
    const/4 v11, 0x0

    .line 123
    if-eqz v5, :cond_1

    .line 124
    :try_start_8
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    .line 130
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_5
    :try_start_9
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 136
    :goto_6
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 142
    :goto_7
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_3

    .line 143
    :catch_1
    move-exception v6

    .line 145
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 125
    .local v6, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v6

    .line 127
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 131
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 133
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 137
    .end local v6    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 139
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 117
    .end local v6    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v6

    .line 119
    .restart local v6    # "e":Ljava/io/IOException;
    :goto_8
    :try_start_c
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 120
    const/4 v11, 0x0

    .line 123
    if-eqz v5, :cond_2

    .line 124
    :try_start_d
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    .line 130
    :cond_2
    :goto_9
    :try_start_e
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    .line 136
    :goto_a
    :try_start_f
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    .line 142
    :goto_b
    :try_start_10
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6

    goto :goto_3

    .line 143
    :catch_6
    move-exception v6

    .line 145
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 125
    :catch_7
    move-exception v6

    .line 127
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 131
    :catch_8
    move-exception v6

    .line 133
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 137
    :catch_9
    move-exception v6

    .line 139
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 121
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 123
    :goto_c
    if-eqz v5, :cond_3

    .line 124
    :try_start_11
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    .line 130
    :cond_3
    :goto_d
    :try_start_12
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    .line 136
    :goto_e
    :try_start_13
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    .line 142
    :goto_f
    :try_start_14
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d

    .line 147
    :goto_10
    throw v1

    .line 125
    :catch_a
    move-exception v6

    .line 127
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 131
    .end local v6    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v6

    .line 133
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 137
    .end local v6    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v6

    .line 139
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 143
    .end local v6    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v6

    .line 145
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_10

    .line 125
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "fileSize":J
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v6

    .line 127
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 131
    .end local v6    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v6

    .line 133
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 137
    .end local v6    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v6

    .line 139
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 143
    .end local v6    # "e":Ljava/io/IOException;
    :catch_11
    move-exception v6

    .line 145
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .line 121
    .end local v3    # "fileSize":J
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v1

    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_c

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v1

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_c

    .line 117
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_12
    move-exception v6

    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_13
    move-exception v6

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_8

    .line 113
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    :catch_14
    move-exception v6

    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v10    # "outputStream":Ljava/io/FileOutputStream;
    :catch_15
    move-exception v6

    move-object v9, v10

    .end local v10    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public static createFolder(Ljava/lang/String;)Z
    .locals 5
    .param p0, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 18
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 19
    .local v0, "filePath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 20
    .local v1, "folderPath":Ljava/io/File;
    const-string v2, "PrintJob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Create Folder : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 23
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 25
    const-string v2, "PrintJob"

    const-string v3, "ERROR - mkdirs"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const/4 v2, 0x0

    .line 30
    :goto_0
    return v2

    .line 28
    :cond_0
    const-string v2, "PrintJob"

    const-string v3, "Success : "

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "fileToBeDeleted":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 76
    :cond_0
    return-void
.end method

.method public static deleteFolder(Ljava/io/File;)V
    .locals 4
    .param p0, "targetFolder"    # Ljava/io/File;

    .prologue
    .line 44
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 45
    .local v0, "childFile":[Ljava/io/File;
    if-nez v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 49
    :cond_0
    array-length v2, v0

    .line 51
    .local v2, "size":I
    if-lez v2, :cond_1

    .line 53
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_2

    .line 64
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 55
    .restart local v1    # "i":I
    :cond_2
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 57
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 53
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_3
    aget-object v3, v0, v1

    invoke-static {v3}, Lcom/sec/mobileprint/core/utils/FileUtil;->deleteFolder(Ljava/io/File;)V

    goto :goto_2
.end method

.method public static deleteFolder(Ljava/lang/String;)V
    .locals 2
    .param p0, "folderPathToBeDeleted"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "targetFolder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-static {v0}, Lcom/sec/mobileprint/core/utils/FileUtil;->deleteFolder(Ljava/io/File;)V

    .line 40
    :cond_0
    return-void
.end method

.method public static getUniqueTempFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 155
    .local v0, "uid":Ljava/util/UUID;
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static saveFileForBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 80
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 81
    .local v1, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    const/4 v2, 0x1

    .end local v1    # "out":Ljava/io/FileOutputStream;
    :goto_0
    return v2

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 84
    const/4 v2, 0x0

    goto :goto_0
.end method
