.class public Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;
.super Ljava/lang/Object;
.source "SamsungPrintJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "JobStatus"
.end annotation


# instance fields
.field mStatusType:I

.field mStatusValue:I

.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V
    .locals 1

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 248
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    .line 249
    return-void
.end method


# virtual methods
.method public getStatusType()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    return v0
.end method

.method public getStatusValue()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    return v0
.end method

.method public setStatusType(I)V
    .locals 0
    .param p1, "mStatusType"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusType:I

    .line 256
    return-void
.end method

.method public setStatusValue(I)V
    .locals 0
    .param p1, "mStatusValue"    # I

    .prologue
    .line 261
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->mStatusValue:I

    .line 262
    return-void
.end method
