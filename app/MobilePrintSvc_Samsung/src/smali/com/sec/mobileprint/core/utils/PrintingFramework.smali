.class public Lcom/sec/mobileprint/core/utils/PrintingFramework;
.super Ljava/lang/Object;
.source "PrintingFramework.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;,
        Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;
    }
.end annotation


# instance fields
.field public PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

.field mContext:Landroid/content/Context;

.field private printServiceConnectedListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;",
            ">;"
        }
    .end annotation
.end field

.field serviceConnection:Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->printServiceConnectedListenerList:Ljava/util/ArrayList;

    .line 41
    iput-object p1, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/utils/PrintingFramework;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->printServiceConnectedListenerList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public InitializePrintingService()Z
    .locals 5

    .prologue
    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "ret":Z
    new-instance v2, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;

    invoke-direct {v2, p0}, Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;-><init>(Lcom/sec/mobileprint/core/utils/PrintingFramework;)V

    iput-object v2, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->serviceConnection:Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;

    .line 49
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.print.mobileprint.plugin.PrintingService"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->serviceConnection:Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 52
    return v1
.end method

.method public addPrintServiceConnectedListener(Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;)V
    .locals 1
    .param p1, "printServiceConnectedListener"    # Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->printServiceConnectedListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public releaseService()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->serviceConnection:Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->serviceConnection:Lcom/sec/mobileprint/core/utils/PrintingFramework$PrintingServiceConnection;

    .line 60
    return-void
.end method

.method public removePrintServiceConnectedListener(Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;)Z
    .locals 1
    .param p1, "printServiceConnectedListener"    # Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/PrintingFramework;->printServiceConnectedListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
