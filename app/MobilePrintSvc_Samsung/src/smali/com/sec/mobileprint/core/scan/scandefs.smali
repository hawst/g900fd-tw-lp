.class public Lcom/sec/mobileprint/core/scan/scandefs;
.super Ljava/lang/Object;
.source "scandefs.java"


# static fields
.field public static final A4:I = 0x1

.field public static final A5:I = 0x5

.field public static final Executive:I = 0x4

.field public static final JISB5:I = 0x10

.field public static final Legal:I = 0x6

.field public static final Letter:I = 0x2

.field public static final MIN_FREE_MEM_ON_SDCRAD:J = 0x14L

.field public static final P3point5x5:I = 0x7

.field public static final P4x6:I = 0x8

.field public static final P5x7:I = 0x9

.field public static final PAGE_HEIGHT_3POINT5x5:I = 0x1770

.field public static final PAGE_HEIGHT_3POINT5x5_INCH:D = 5.0

.field public static final PAGE_HEIGHT_4x6:I = 0x1c20

.field public static final PAGE_HEIGHT_4x6_INCH:D = 6.0

.field public static final PAGE_HEIGHT_5x7:I = 0x20d0

.field public static final PAGE_HEIGHT_5x7_INCH:D = 7.0

.field public static final PAGE_HEIGHT_A4:I = 0x36cf

.field public static final PAGE_HEIGHT_A4_INCH:D = 11.7

.field public static final PAGE_HEIGHT_A5:I = 0x26c1

.field public static final PAGE_HEIGHT_A5_INCH:D = 8.3

.field public static final PAGE_HEIGHT_EXECUTIVE:I = 0x3138

.field public static final PAGE_HEIGHT_EXECUTIVE_INCH:D = 10.5

.field public static final PAGE_HEIGHT_JISB5:I = 0x2f6d

.field public static final PAGE_HEIGHT_JISB5_INCH:D = 10.12

.field public static final PAGE_HEIGHT_LEGAL:I = 0x41a0

.field public static final PAGE_HEIGHT_LEGAL_INCH:D = 14.0

.field public static final PAGE_HEIGHT_LETTER:I = 0x3390

.field public static final PAGE_HEIGHT_LETTER_INCH:D = 11.0

.field public static final PAGE_HEIGHT_ROTATED_A3:I = 0x36cf

.field public static final PAGE_HEIGHT_ROTATED_A3_INCH:D = 11.69

.field public static final PAGE_HEIGHT_ROTATED_A4:I = 0x26c1

.field public static final PAGE_HEIGHT_ROTATED_A4_INCH:D = 8.3

.field public static final PAGE_HEIGHT_ROTATED_A5:I = 0x1b50

.field public static final PAGE_HEIGHT_ROTATED_A5_INCH:D = 5.8

.field public static final PAGE_HEIGHT_ROTATED_B4:I = 0x2e23

.field public static final PAGE_HEIGHT_ROTATED_B4_INCH:D = 9.84

.field public static final PAGE_HEIGHT_ROTATED_Executive:I = 0x21fc

.field public static final PAGE_HEIGHT_ROTATED_Executive_INCH:D = 7.25

.field public static final PAGE_HEIGHT_ROTATED_JISB5:I = 0x2196

.field public static final PAGE_HEIGHT_ROTATED_JISB5_INCH:D = 7.17

.field public static final PAGE_HEIGHT_ROTATED_LEDGER:I

.field public static final PAGE_HEIGHT_ROTATED_LEDGER_INCH:D = 11.0

.field public static final PAGE_HEIGHT_ROTATED_LEGAL:I = 0x27d8

.field public static final PAGE_HEIGHT_ROTATED_LEGAl_INCH:D = 8.5

.field public static final PAGE_HEIGHT_ROTATED_Letter:I = 0x27d8

.field public static final PAGE_HEIGHT_ROTATED_Letter_INCH:D = 8.5

.field public static final PAGE_HEIGHT_STATEMENT:I = 0x27d8

.field public static final PAGE_HEIGHT_STATEMENT_INCH:D = 8.5

.field public static final PAGE_WIDTH_3POINT5x5:I = 0x1068

.field public static final PAGE_WIDTH_3POINT5x5_INCH:D = 3.5

.field public static final PAGE_WIDTH_4x6:I = 0x12c0

.field public static final PAGE_WIDTH_4x6_INCH:D = 4.0

.field public static final PAGE_WIDTH_5x7:I = 0x1770

.field public static final PAGE_WIDTH_5x7_INCH:D = 5.0

.field public static final PAGE_WIDTH_A4:I = 0x26c1

.field public static final PAGE_WIDTH_A4_INCH:D = 8.3

.field public static final PAGE_WIDTH_A5:I = 0x1b50

.field public static final PAGE_WIDTH_A5_INCH:D = 5.8

.field public static final PAGE_WIDTH_EXECUTIVE:I = 0x21fc

.field public static final PAGE_WIDTH_EXECUTIVE_INCH:D = 7.25

.field public static final PAGE_WIDTH_JISB5:I = 0x2196

.field public static final PAGE_WIDTH_JISB5_INCH:D = 7.17

.field public static final PAGE_WIDTH_LEGAL:I = 0x27d8

.field public static final PAGE_WIDTH_LEGAl_INCH:D = 8.5

.field public static final PAGE_WIDTH_LETTER:I = 0x27d8

.field public static final PAGE_WIDTH_LETTER_INCH:D = 8.5

.field public static final PAGE_WIDTH_ROTATED_A3:I = 0x4d82

.field public static final PAGE_WIDTH_ROTATED_A3_INCH:D = 16.54

.field public static final PAGE_WIDTH_ROTATED_A4:I = 0x36cf

.field public static final PAGE_WIDTH_ROTATED_A4_INCH:D = 11.7

.field public static final PAGE_WIDTH_ROTATED_A5:I = 0x26c1

.field public static final PAGE_WIDTH_ROTATED_A5_INCH:D = 8.3

.field public static final PAGE_WIDTH_ROTATED_B4:I = 0x4125

.field public static final PAGE_WIDTH_ROTATED_B4_INCH:D = 13.9

.field public static final PAGE_WIDTH_ROTATED_Executive:I = 0x3138

.field public static final PAGE_WIDTH_ROTATED_Executive_INCH:D = 10.5

.field public static final PAGE_WIDTH_ROTATED_JISB5:I = 0x2f6d

.field public static final PAGE_WIDTH_ROTATED_JISB5_INCH:D = 10.12

.field public static final PAGE_WIDTH_ROTATED_LEDGER:I

.field public static final PAGE_WIDTH_ROTATED_LEDGER_INCH:D = 17.0

.field public static final PAGE_WIDTH_ROTATED_LEGAL:I = 0x41a0

.field public static final PAGE_WIDTH_ROTATED_LEGAL_INCH:D = 14.0

.field public static final PAGE_WIDTH_ROTATED_Letter:I = 0x3390

.field public static final PAGE_WIDTH_ROTATED_Letter_INCH:D = 11.0

.field public static final PAGE_WIDTH_STATEMENT:I = 0x19c8

.field public static final PAGE_WIDTH_STATEMENT_INCH:D = 5.5

.field public static final ROTATED_A3:I = 0xa

.field public static final ROTATED_A4:I = 0xe

.field public static final ROTATED_A5:I = 0xf

.field public static final ROTATED_B4:I = 0xb

.field public static final ROTATED_Executive:I = 0x12

.field public static final ROTATED_JISB5:I = 0x11

.field public static final ROTATED_LEDGER:I = 0xc

.field public static final ROTATED_LEGAL:I = 0xd

.field public static final ROTATED_Letter:I = 0x13

.field public static final Statement:I = 0x3

.field public static final W_MM_PER_10_INCHES:I = 0xfe

.field public static final W_PXU_PER_10_INCHES:I = 0x2ee0

.field public static final W_PXU_PER_INCH:I = 0x4b0

.field public static final kADF:I = 0x2

.field public static final kAccountingType_Accounting:I = 0x1

.field public static final kAccountingType_IDOnly:I = 0x2

.field public static final kAccountingType_Undefined:I = 0x0

.field public static final kAllPagesScaned:I = 0x10

.field public static final kAuthError:I = 0xc

.field public static final kAuto:I = 0x8

.field public static final kBadColor:I = -0x16

.field public static final kBadOriginX:I = -0x1a

.field public static final kBadOriginY:I = -0x1b

.field public static final kBadResolution:I = -0x17

.field public static final kBadScale:I = -0x18

.field public static final kBadSource:I = -0x19

.field public static final kBusy:I = 0x7

.field public static final kCanNotCreateCapabilities:I = -0xf

.field public static final kCloseError:I = -0x2

.field public static final kConversionFileError:I = -0x2a

.field public static final kConversionNoFile:I = -0x29

.field public static final kCoverOpen:I = 0x5

.field public static final kCustomArea:I = 0x1

.field public static final kDuplex:I = 0x4

.field public static final kEighth:I = 0x8

.field public static final kEmptyPassword:I = 0xd

.field public static final kExceedQuota:I = 0xf

.field public static final kFileCloseError:I = -0x1f

.field public static final kFileError:I = -0xb

.field public static final kFileType_BMP:I = 0x5

.field public static final kFileType_JPEG:I = 0x1

.field public static final kFileType_PDF:I = 0x6

.field public static final kFileType_PNG:I = 0x2

.field public static final kFileType_TIFF:I = 0x4

.field public static final kFlatbed:I = 0x1

.field public static final kFlushError:I = -0x1e

.field public static final kGRAY:I = 0x2

.field public static final kHalf:I = 0x2

.field public static final kHightRes:I = 0x4

.field public static final kInvalidParam:I = 0x8

.field public static final kInvalidUser:I = 0xa

.field public static final kInvalidePasswd:I = 0xb

.field public static final kJam:I = 0x4

.field public static final kJpegPrepareError:I = -0x2b

.field public static final kJpegPrepareNoFile:I = -0x29

.field public static final kJpegPrepareNoMemory:I = -0x28

.field public static final kJpegPrepateFileError:I = -0x2a

.field public static final kLocked:I = 0x6

.field public static final kLowMemory:I = -0xa

.field public static final kLowRes:I = 0x1

.field public static final kMakeDirError:I = -0x1c

.field public static final kMissingDocumentDirectory:I = -0x6

.field public static final kNoDocument:I = 0x3

.field public static final kNoExternalStorage:I = -0x12

.field public static final kNoFilledCapabilities:I = -0x4

.field public static final kNoFreeMemONSDCard:I = -0x22

.field public static final kNoGetCapabilities:I = -0x3

.field public static final kNoImageBlock:I = -0x32

.field public static final kNoMemory:I = -0x7

.field public static final kNoMemoryPreview:I = -0x9

.field public static final kNoPermission:I = 0xe

.field public static final kNoServiceAndCapabilities:I = -0x10

.field public static final kNoSuportedColorConversion:I = -0x21

.field public static final kNoSupportedColor:I = -0x8

.field public static final kNoWritableExternalStorage:I = -0x13

.field public static final kNormRes:I = 0x2

.field public static final kOK:I = 0x0

.field public static final kOne:I = 0x1

.field public static final kOpenError:I = -0x1

.field public static final kPaperArea:I = 0x0

.field public static final kParamError:I = -0xe

.field public static final kPermission_Default:I = 0x0

.field public static final kPermission_Group:I = 0x1

.field public static final kPermission_User:I = 0x0

.field public static final kPhotoExportError:I = -0x11

.field public static final kPngPrepareError:I = -0x2e

.field public static final kPngPrepareNoMemory:I = -0x2d

.field public static final kQuarter:I = 0x4

.field public static final kRBG:I = 0x1

.field public static final kRGB565:I = 0x4

.field public static final kRGBA4444:I = 0x5

.field public static final kRGBA8888:I = 0x3

.field public static final kScanHeightMorethenSource:I = -0x15

.field public static final kScanWidthMoreThenSource:I = -0x14

.field public static final kSecurityError:I = 0x9

.field public static final kServiceNotStarter:I = -0xc

.field public static final kServiceRunning:I = -0xd

.field public static final kSessionOpen:I = -0x20

.field public static final kSessionOpenError:I = -0x5

.field public static final kStatusBusy:I = 0x12

.field public static final kTimeOut:I = 0x11

.field public static final kUnitTestMessage:I = 0x12fd1

.field public static final kUnknownError:I = 0x2

.field public static final kUserCancel:I = 0x1

.field public static final kWriteError:I = -0x1d


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide v4, 0x40c7700000000000L    # 12000.0

    const-wide v2, 0x406fc00000000000L    # 254.0

    .line 165
    const-wide v0, 0x407afccccccccccdL    # 431.8

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v4

    div-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/sec/mobileprint/core/scan/scandefs;->PAGE_WIDTH_ROTATED_LEDGER:I

    .line 166
    const-wide v0, 0x4071766666666666L    # 279.4

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v4

    div-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/sec/mobileprint/core/scan/scandefs;->PAGE_HEIGHT_ROTATED_LEDGER:I

    .line 261
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPaperHeight(IZ)D
    .locals 12
    .param p0, "pap"    # I
    .param p1, "bInch"    # Z

    .prologue
    const-wide v10, 0x40c3608000000000L    # 9921.0

    const-wide/high16 v2, 0x4026000000000000L    # 11.0

    const-wide v8, 0x402099999999999aL    # 8.3

    const-wide v6, 0x40c3ec0000000000L    # 10200.0

    const-wide/high16 v4, 0x4021000000000000L    # 8.5

    .line 332
    const-wide/16 v0, 0x0

    .line 333
    .local v0, "ret":D
    packed-switch p0, :pswitch_data_0

    .line 392
    if-eqz p1, :cond_12

    const-wide v0, 0x4027666666666666L    # 11.7

    .line 395
    :goto_0
    return-wide v0

    .line 335
    :pswitch_0
    if-eqz p1, :cond_0

    move-wide v0, v2

    .line 336
    :goto_1
    goto :goto_0

    .line 335
    :cond_0
    const-wide v0, 0x40c9c80000000000L    # 13200.0

    goto :goto_1

    .line 338
    :pswitch_1
    if-eqz p1, :cond_1

    const-wide/high16 v0, 0x402c000000000000L    # 14.0

    .line 339
    :goto_2
    goto :goto_0

    .line 338
    :cond_1
    const-wide v0, 0x40d0680000000000L    # 16800.0

    goto :goto_2

    .line 341
    :pswitch_2
    if-eqz p1, :cond_2

    move-wide v0, v4

    .line 342
    :goto_3
    goto :goto_0

    :cond_2
    move-wide v0, v6

    .line 341
    goto :goto_3

    .line 344
    :pswitch_3
    if-eqz p1, :cond_3

    move-wide v0, v4

    .line 345
    :goto_4
    goto :goto_0

    :cond_3
    move-wide v0, v6

    .line 344
    goto :goto_4

    .line 347
    :pswitch_4
    if-eqz p1, :cond_4

    const-wide/high16 v0, 0x4025000000000000L    # 10.5

    .line 348
    :goto_5
    goto :goto_0

    .line 347
    :cond_4
    const-wide v0, 0x40c89c0000000000L    # 12600.0

    goto :goto_5

    .line 350
    :pswitch_5
    if-eqz p1, :cond_5

    move-wide v0, v8

    .line 351
    :goto_6
    goto :goto_0

    :cond_5
    move-wide v0, v10

    .line 350
    goto :goto_6

    .line 353
    :pswitch_6
    if-eqz p1, :cond_6

    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    .line 354
    :goto_7
    goto :goto_0

    .line 353
    :cond_6
    const-wide v0, 0x40b7700000000000L    # 6000.0

    goto :goto_7

    .line 356
    :pswitch_7
    if-eqz p1, :cond_7

    const-wide/high16 v0, 0x4018000000000000L    # 6.0

    .line 357
    :goto_8
    goto :goto_0

    .line 356
    :cond_7
    const-wide v0, 0x40bc200000000000L    # 7200.0

    goto :goto_8

    .line 359
    :pswitch_8
    if-eqz p1, :cond_8

    const-wide/high16 v0, 0x401c000000000000L    # 7.0

    .line 360
    :goto_9
    goto :goto_0

    .line 359
    :cond_8
    const-wide v0, 0x40c0680000000000L    # 8400.0

    goto :goto_9

    .line 362
    :pswitch_9
    if-eqz p1, :cond_9

    const-wide v0, 0x40276147ae147ae1L    # 11.69

    .line 363
    :goto_a
    goto :goto_0

    .line 362
    :cond_9
    const-wide v0, 0x40cb678000000000L    # 14031.0

    goto :goto_a

    .line 365
    :pswitch_a
    if-eqz p1, :cond_a

    const-wide v0, 0x4023ae147ae147aeL    # 9.84

    .line 366
    :goto_b
    goto :goto_0

    .line 365
    :cond_a
    const-wide v0, 0x40c7118000000000L    # 11811.0

    goto :goto_b

    .line 368
    :pswitch_b
    if-eqz p1, :cond_b

    move-wide v0, v2

    .line 369
    :goto_c
    goto :goto_0

    .line 368
    :cond_b
    sget v2, Lcom/sec/mobileprint/core/scan/scandefs;->PAGE_HEIGHT_ROTATED_LEDGER:I

    int-to-double v0, v2

    goto :goto_c

    .line 372
    :pswitch_c
    if-eqz p1, :cond_c

    move-wide v0, v8

    .line 373
    :goto_d
    goto :goto_0

    :cond_c
    move-wide v0, v10

    .line 372
    goto :goto_d

    .line 375
    :pswitch_d
    if-eqz p1, :cond_d

    const-wide v0, 0x4017333333333333L    # 5.8

    .line 376
    :goto_e
    goto/16 :goto_0

    .line 375
    :cond_d
    const-wide v0, 0x40bb500000000000L    # 6992.0

    goto :goto_e

    .line 378
    :pswitch_e
    if-eqz p1, :cond_e

    const-wide v0, 0x40243d70a3d70a3dL    # 10.12

    .line 379
    :goto_f
    goto/16 :goto_0

    .line 378
    :cond_e
    const-wide v0, 0x40c7b68000000000L    # 12141.0

    goto :goto_f

    .line 381
    :pswitch_f
    if-eqz p1, :cond_f

    const-wide v0, 0x401cae147ae147aeL    # 7.17

    .line 382
    :goto_10
    goto/16 :goto_0

    .line 381
    :cond_f
    const-wide v0, 0x40c0cb0000000000L    # 8598.0

    goto :goto_10

    .line 384
    :pswitch_10
    if-eqz p1, :cond_10

    const-wide/high16 v0, 0x401d000000000000L    # 7.25

    .line 385
    :goto_11
    goto/16 :goto_0

    .line 384
    :cond_10
    const-wide v0, 0x40c0fe0000000000L    # 8700.0

    goto :goto_11

    .line 387
    :pswitch_11
    if-eqz p1, :cond_11

    move-wide v0, v4

    .line 388
    :goto_12
    goto/16 :goto_0

    :cond_11
    move-wide v0, v6

    .line 387
    goto :goto_12

    .line 392
    :cond_12
    const-wide v0, 0x40cb678000000000L    # 14031.0

    goto/16 :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static getPaperIndexFromName(Ljava/lang/String;)I
    .locals 2
    .param p0, "paperName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 400
    const-string v1, "A4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 413
    :cond_0
    :goto_0
    return v0

    .line 402
    :cond_1
    const-string v1, "Letter"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 403
    const/4 v0, 0x2

    goto :goto_0

    .line 404
    :cond_2
    const-string v1, "Legal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405
    const/4 v0, 0x6

    goto :goto_0

    .line 406
    :cond_3
    const-string v1, "3.5x5in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 407
    const/4 v0, 0x7

    goto :goto_0

    .line 408
    :cond_4
    const-string v1, "4x6in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 409
    const/16 v0, 0x8

    goto :goto_0

    .line 410
    :cond_5
    const-string v1, "5x7in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public static getPaperWidth(IZ)D
    .locals 10
    .param p0, "pap"    # I
    .param p1, "bInch"    # Z

    .prologue
    const-wide v4, 0x40c3ec0000000000L    # 10200.0

    const-wide v8, 0x40c3608000000000L    # 9921.0

    const-wide/high16 v2, 0x4021000000000000L    # 8.5

    const-wide v6, 0x402099999999999aL    # 8.3

    .line 264
    const-wide/16 v0, 0x0

    .line 265
    .local v0, "ret":D
    packed-switch p0, :pswitch_data_0

    .line 325
    if-eqz p1, :cond_12

    move-wide v0, v6

    .line 328
    :goto_0
    return-wide v0

    .line 267
    :pswitch_0
    if-eqz p1, :cond_0

    move-wide v0, v2

    .line 268
    :goto_1
    goto :goto_0

    :cond_0
    move-wide v0, v4

    .line 267
    goto :goto_1

    .line 270
    :pswitch_1
    if-eqz p1, :cond_1

    move-wide v0, v2

    .line 271
    :goto_2
    goto :goto_0

    :cond_1
    move-wide v0, v4

    .line 270
    goto :goto_2

    .line 273
    :pswitch_2
    if-eqz p1, :cond_2

    const-wide/high16 v0, 0x402c000000000000L    # 14.0

    .line 274
    :goto_3
    goto :goto_0

    .line 273
    :cond_2
    const-wide v0, 0x40d0680000000000L    # 16800.0

    goto :goto_3

    .line 276
    :pswitch_3
    if-eqz p1, :cond_3

    const-wide/high16 v0, 0x4016000000000000L    # 5.5

    .line 277
    :goto_4
    goto :goto_0

    .line 276
    :cond_3
    const-wide v0, 0x40b9c80000000000L    # 6600.0

    goto :goto_4

    .line 279
    :pswitch_4
    if-eqz p1, :cond_4

    const-wide/high16 v0, 0x401d000000000000L    # 7.25

    .line 281
    :goto_5
    goto :goto_0

    .line 279
    :cond_4
    const-wide v0, 0x40c0fe0000000000L    # 8700.0

    goto :goto_5

    .line 283
    :pswitch_5
    if-eqz p1, :cond_5

    const-wide v0, 0x4017333333333333L    # 5.8

    .line 284
    :goto_6
    goto :goto_0

    .line 283
    :cond_5
    const-wide v0, 0x40bb500000000000L    # 6992.0

    goto :goto_6

    .line 286
    :pswitch_6
    if-eqz p1, :cond_6

    const-wide/high16 v0, 0x400c000000000000L    # 3.5

    .line 287
    :goto_7
    goto :goto_0

    .line 286
    :cond_6
    const-wide v0, 0x40b0680000000000L    # 4200.0

    goto :goto_7

    .line 289
    :pswitch_7
    if-eqz p1, :cond_7

    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    .line 290
    :goto_8
    goto :goto_0

    .line 289
    :cond_7
    const-wide v0, 0x40b2c00000000000L    # 4800.0

    goto :goto_8

    .line 292
    :pswitch_8
    if-eqz p1, :cond_8

    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    .line 293
    :goto_9
    goto :goto_0

    .line 292
    :cond_8
    const-wide v0, 0x40b7700000000000L    # 6000.0

    goto :goto_9

    .line 295
    :pswitch_9
    if-eqz p1, :cond_9

    const-wide v0, 0x40308a3d70a3d70aL    # 16.54

    .line 296
    :goto_a
    goto :goto_0

    .line 295
    :cond_9
    const-wide v0, 0x40d3608000000000L    # 19842.0

    goto :goto_a

    .line 298
    :pswitch_a
    if-eqz p1, :cond_a

    const-wide v0, 0x402bcccccccccccdL    # 13.9

    .line 299
    :goto_b
    goto :goto_0

    .line 298
    :cond_a
    const-wide v0, 0x40d0494000000000L    # 16677.0

    goto :goto_b

    .line 301
    :pswitch_b
    if-eqz p1, :cond_b

    const-wide/high16 v0, 0x4031000000000000L    # 17.0

    .line 302
    :goto_c
    goto :goto_0

    .line 301
    :cond_b
    sget v2, Lcom/sec/mobileprint/core/scan/scandefs;->PAGE_WIDTH_ROTATED_LEDGER:I

    int-to-double v0, v2

    goto :goto_c

    .line 305
    :pswitch_c
    if-eqz p1, :cond_c

    const-wide v0, 0x4027666666666666L    # 11.7

    .line 306
    :goto_d
    goto/16 :goto_0

    .line 305
    :cond_c
    const-wide v0, 0x40cb678000000000L    # 14031.0

    goto :goto_d

    .line 308
    :pswitch_d
    if-eqz p1, :cond_d

    move-wide v0, v6

    .line 309
    :goto_e
    goto/16 :goto_0

    :cond_d
    move-wide v0, v8

    .line 308
    goto :goto_e

    .line 311
    :pswitch_e
    if-eqz p1, :cond_e

    const-wide v0, 0x401cae147ae147aeL    # 7.17

    .line 312
    :goto_f
    goto/16 :goto_0

    .line 311
    :cond_e
    const-wide v0, 0x40c0cb0000000000L    # 8598.0

    goto :goto_f

    .line 314
    :pswitch_f
    if-eqz p1, :cond_f

    const-wide v0, 0x40243d70a3d70a3dL    # 10.12

    .line 315
    :goto_10
    goto/16 :goto_0

    .line 314
    :cond_f
    const-wide v0, 0x40c7b68000000000L    # 12141.0

    goto :goto_10

    .line 317
    :pswitch_10
    if-eqz p1, :cond_10

    const-wide/high16 v0, 0x4025000000000000L    # 10.5

    .line 318
    :goto_11
    goto/16 :goto_0

    .line 317
    :cond_10
    const-wide v0, 0x40c89c0000000000L    # 12600.0

    goto :goto_11

    .line 320
    :pswitch_11
    if-eqz p1, :cond_11

    const-wide/high16 v0, 0x4026000000000000L    # 11.0

    .line 321
    :goto_12
    goto/16 :goto_0

    .line 320
    :cond_11
    const-wide v0, 0x40c9c80000000000L    # 13200.0

    goto :goto_12

    :cond_12
    move-wide v0, v8

    .line 325
    goto/16 :goto_0

    .line 265
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method
