.class Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;
.super Ljava/lang/Object;
.source "SamsungPrintSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SecureRelease"
.end annotation


# instance fields
.field private mSecureReleaseUserName:Ljava/lang/String;

.field private mUseSecureRelease:Z

.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mUseSecureRelease:Z

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mSecureReleaseUserName:Ljava/lang/String;

    .line 91
    return-void
.end method

.method constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;ZLjava/lang/String;)V
    .locals 0
    .param p2, "useSecureRelease"    # Z
    .param p3, "SecureReleaseUserName"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mUseSecureRelease:Z

    .line 94
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mSecureReleaseUserName:Ljava/lang/String;

    .line 95
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mUseSecureRelease:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mSecureReleaseUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;Z)V
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mUseSecureRelease:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mSecureReleaseUserName:Ljava/lang/String;

    return-void
.end method
