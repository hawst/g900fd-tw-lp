.class public Lcom/sec/mobileprint/core/App;
.super Landroid/app/Application;
.source "App.java"


# static fields
.field public static final PRINT_SERVICE_GOOGLE_KITKAT:I = 0x2

.field public static final PRINT_SERVICE_HP_AMAZON:I = 0x3

.field public static final PRINT_SERVICE_SAMSUNG_API_SDK:I = 0x5

.field public static final PRINT_SERVICE_SAMSUNG_GALAXY:I = 0x1

.field public static final PRINT_SERVICE_SAMSUNG_INTENT_SDK:I = 0x4

.field public static final SCAN_SERVICE_SAMSUNG_API_SDK:I = 0x7

.field public static final SCAN_SERVICE_SAMSUNG_INTENT_SDK:I = 0x6

.field public static context:Landroid/content/Context;

.field private static mJobManager:Lcom/sec/mobileprint/core/print/SamsungJobManager;

.field public static service:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/mobileprint/core/App;->mJobManager:Lcom/sec/mobileprint/core/print/SamsungJobManager;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    .prologue
    .line 27
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 28
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    .line 29
    new-instance v1, Lcom/sec/mobileprint/core/print/SamsungJobManager;

    invoke-direct {v1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/core/App;->mJobManager:Lcom/sec/mobileprint/core/print/SamsungJobManager;

    .line 31
    :try_start_0
    const-string v1, "SamsungPrintServicePlugin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version No:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/mobileprint/core/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 32
    sget-object v4, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 31
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
