.class Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;
.super Ljava/lang/Object;
.source "SamsungPrintSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Confidential"
.end annotation


# instance fields
.field private mConfidentialPassword:Ljava/lang/String;

.field private mConfidentialUserName:Ljava/lang/String;

.field private mUseConfidential:Z

.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V
    .locals 1

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mUseConfidential:Z

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialPassword:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "useConfidential"    # Z
    .param p3, "userName"    # Ljava/lang/String;
    .param p4, "pasword"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-boolean p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mUseConfidential:Z

    .line 79
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialPassword:Ljava/lang/String;

    .line 81
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mUseConfidential:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialPassword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Z)V
    .locals 0

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mUseConfidential:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialPassword:Ljava/lang/String;

    return-void
.end method
