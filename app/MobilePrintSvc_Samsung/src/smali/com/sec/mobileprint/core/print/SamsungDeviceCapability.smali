.class public Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
.super Landroid/os/AsyncTask;
.source "SamsungDeviceCapability.java"

# interfaces
.implements Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SamsungDeviceCapability"


# instance fields
.field private mCapsBundle:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

.field private mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

.field private mDeviceName:Ljava/lang/String;

.field private mIPAddress:Ljava/lang/String;

.field private mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

.field private mMessenger:Landroid/os/Messenger;

.field private mServiceType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Messenger;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "returnMessenger"    # Landroid/os/Messenger;
    .param p4, "serviceType"    # I

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 33
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 36
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 43
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 44
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 47
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 48
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 49
    iput p4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 33
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 36
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 74
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 75
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 79
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "ipAddress"    # Ljava/lang/String;
    .param p4, "returnMessenger"    # Landroid/os/Messenger;
    .param p5, "serviceType"    # I

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 33
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 36
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 54
    iput-object p4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 55
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 56
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 58
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 59
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 60
    iput p5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "ipAddress"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 33
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 36
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 39
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 64
    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    .line 65
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    .line 66
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 69
    iput-object p4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 71
    return-void
.end method

.method private getColor(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 6
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 224
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v1, "colorCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "monochrome"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    const/4 v0, 0x0

    .line 228
    .local v0, "canPrintColor":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 229
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 230
    const-string v3, "color"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v4, "print-color-mode"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 239
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v3, v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setColor(I)V

    .line 241
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_1

    const-string v3, "SamsungDeviceCapability"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getColor] Color="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_1
    return-void

    .line 232
    :catch_0
    move-exception v2

    .line 234
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 6
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v1, "duplexCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 204
    .local v0, "canDuplex":I
    const-string v3, "one-sided"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 207
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 208
    const-string v3, "two-sided-short-edge"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    const-string v3, "two-sided-long-edge"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v4, "sides"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 218
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v3, v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDuplex(I)V

    .line 220
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_1

    const-string v3, "SamsungDeviceCapability"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getDuplex] Duplex="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_1
    return-void

    .line 211
    :catch_0
    move-exception v2

    .line 213
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getFullBleedSupport(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v1, "full-bleed-supported"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 351
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDeviceCapability"

    const-string v1, "[getFullBleedSupport] full bleed support=false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_0
    return-void
.end method

.method private getMediaSize(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 10
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 300
    iget v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    .line 302
    :try_start_0
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v7}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v4

    .line 303
    .local v4, "mediaSizeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    if-eqz v4, :cond_0

    .line 304
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .local v3, "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 308
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v7, "media-size-name"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "mediaSizeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :cond_0
    :goto_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v7}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v5

    .line 335
    .local v5, "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v6, v5}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setMediaSize(Ljava/util/List;)V

    .line 337
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-nez v7, :cond_5

    .line 345
    .end local v5    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :goto_3
    return-void

    .line 305
    .restart local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "mediaSizeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :cond_2
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 306
    .local v1, "mediaInfo":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->getMediaName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 310
    .end local v1    # "mediaInfo":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .end local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "mediaSizeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 316
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    iget v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    .line 317
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .restart local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "iso_a4_210x297mm"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    const-string v6, "na_letter_8.5x11in"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    const-string v6, "na_legal_8.5x14in"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v7, "media-size-name"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 324
    .end local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 325
    .restart local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, "iso_a4_210x297mm"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    const-string v6, "na_letter_8.5x11in"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    const-string v6, "na_legal_8.5x14in"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v7, "media-size-name"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 337
    .end local v3    # "mediaSizeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :cond_5
    :try_start_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    .line 338
    .local v2, "mediaSize":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    sget-boolean v7, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v7, :cond_1

    const-string v7, "SamsungDeviceCapability"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[getMediaSize] media size="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->getMediaName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 340
    .end local v2    # "mediaSize":Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .end local v5    # "mediaSizes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :catch_1
    move-exception v0

    .line 342
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method

.method private getMediaSource(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v0, "mediaSourceCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v2, "media-source"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 293
    sget-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SamsungDeviceCapability"

    const-string v2, "[getMediaSource] media source=auto"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_0
    return-void
.end method

.method private getMediatype(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 8
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 255
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v2, "mediaTypeCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 259
    :try_start_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v6, "media-type"

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v4, v4, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v7}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaTypes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :cond_0
    :goto_0
    :try_start_1
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v4, v4, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getMediaTypes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v3

    .line 274
    .local v3, "mediaTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v4, v3}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setMediaType(Ljava/util/List;)V

    .line 276
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_3

    .line 285
    .end local v3    # "mediaTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_2
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mServiceType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 267
    const-string v4, "auto"

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v5, "media-type"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 276
    .restart local v3    # "mediaTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    :try_start_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 277
    .local v1, "mediaType":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v5, :cond_1

    const-string v5, "SamsungDeviceCapability"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[getMediatype] media type="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 279
    .end local v1    # "mediaType":Ljava/lang/String;
    .end local v3    # "mediaTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_1
    move-exception v0

    .line 281
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method private getPrintQuality(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v0, "qualityCaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "high"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v2, "print-quality"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 250
    sget-boolean v1, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SamsungDeviceCapability"

    const-string v2, "[getPrintQuality] Quality=high"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    return-void
.end method

.method private getPrinterName(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v1, "printer-name"

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v1, "printer-make-model"

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDeviceCapability"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[getPrinterName] printer name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_0
    return-void
.end method

.method private getPrinterSuported()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 171
    const/4 v0, 0x0

    .line 173
    .local v0, "deviceName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 174
    :cond_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    :goto_0
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_1

    const-string v3, "SamsungDeviceCapability"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getPrinterSuported] ip="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", deviceName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 182
    new-instance v3, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-direct {v3}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;-><init>()V

    iput-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    .line 183
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v3, v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V

    .line 184
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceIpAddress(Ljava/lang/String;)V

    .line 188
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isSupportedPrinter(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v3

    if-eqz v3, :cond_2

    .line 189
    const/4 v2, 0x1

    .line 197
    :cond_2
    :goto_1
    return v2

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 109
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDeviceCapability"

    const-string v1, "[doInBackground]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getPrinterSuported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 113
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getColor(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 114
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getPrintQuality(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 115
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getMediatype(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 116
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getMediaSource(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 117
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getMediaSize(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 118
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getFullBleedSupport(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 119
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->getPrinterName(Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 120
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onDeviceManagerConnectorStarted()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 97
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDeviceCapability"

    const-string v1, "[onDeviceManagerConnectorStarted]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 101
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    :goto_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->removeDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)Z

    .line 105
    return-void

    .line 103
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    .line 130
    sget-boolean v3, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "SamsungDeviceCapability"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[onPostExecute] result="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 133
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_4

    .line 136
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 137
    .local v1, "returnIntent":Landroid/content/Intent;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    .line 138
    const-string v3, "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v3, "printer-address"

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string v3, "print-error"

    const-string v5, "communication-error"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    :goto_0
    const-string v3, "request-action"

    const-string v5, "org.androidprinting.intent.ACTION_GET_PRINT_OPTIONS"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const/4 v3, 0x0

    invoke-static {v4, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 149
    .local v2, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v1    # "returnIntent":Landroid/content/Intent;
    .end local v2    # "returnmsg":Landroid/os/Message;
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v3, :cond_2

    .line 163
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->releaseService()V

    .line 164
    iput-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 167
    :cond_2
    return-void

    .line 142
    .restart local v1    # "returnIntent":Landroid/content/Intent;
    :cond_3
    const-string v3, "org.androidprinting.intent.ACTION_RETURN_PRINT_CAPABILITIES"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v3, "printer-address"

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mCapsBundle:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0

    .line 150
    .restart local v2    # "returnmsg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 156
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "returnIntent":Landroid/content/Intent;
    .end local v2    # "returnmsg":Landroid/os/Message;
    :cond_4
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    if-eqz v3, :cond_1

    .line 157
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    :goto_2
    const/4 v6, 0x2

    invoke-interface {v5, v3, v6}, Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;->onDeviceDiscoveredOrUpdated(Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;I)V

    goto :goto_1

    :cond_5
    move-object v3, v4

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method public startDeviceManagerConnector()V
    .locals 2

    .prologue
    .line 85
    sget-boolean v0, Lcom/sec/mobileprint/kitkat/plugin/KitKatConstants;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "SamsungDeviceCapability"

    const-string v1, "[startDeviceManagerConnector]"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    new-instance v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 89
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->InitializeDiscovery()Z

    .line 90
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->addDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)V

    .line 91
    return-void
.end method
