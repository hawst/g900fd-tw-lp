.class public Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;
.super Landroid/os/AsyncTask;
.source "SamsungScannerCapability.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "[I>;"
    }
.end annotation


# instance fields
.field private mCapsBundle:Landroid/os/Bundle;

.field private mIPAddress:Ljava/lang/String;

.field private mMessenger:Landroid/os/Messenger;

.field private mServiceType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Messenger;I)V
    .locals 1
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "returnMessenger"    # Landroid/os/Messenger;
    .param p3, "serviceType"    # I

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mMessenger:Landroid/os/Messenger;

    .line 21
    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 22
    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mIPAddress:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mMessenger:Landroid/os/Messenger;

    .line 27
    iput-object p1, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mIPAddress:Ljava/lang/String;

    .line 28
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    .line 29
    iput p3, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mServiceType:I

    .line 30
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->doInBackground([Ljava/lang/Void;)[I

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[I
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 42
    new-instance v1, Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-direct {v1}, Lcom/sec/print/mobileprint/sf/ScannerLib;-><init>()V

    .line 43
    .local v1, "scanner":Lcom/sec/print/mobileprint/sf/ScannerLib;
    iget-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getCapabilities(Ljava/lang/String;)[I

    move-result-object v0

    .line 44
    .local v0, "caps":[I
    const/4 v2, 0x0

    aget v2, v0, v2

    if-nez v2, :cond_0

    .line 47
    .end local v0    # "caps":[I
    :goto_0
    return-object v0

    .restart local v0    # "caps":[I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCapability()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 34
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->onPostExecute([I)V

    return-void
.end method

.method protected onPostExecute([I)V
    .locals 10
    .param p1, "arr"    # [I

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 55
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mMessenger:Landroid/os/Messenger;

    if-eqz v8, :cond_2

    .line 58
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 59
    .local v1, "returnIntent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    .line 61
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v7, "scanSource":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "Flatbed"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    const/4 v8, 0x5

    aget v8, p1, v8

    and-int/lit8 v8, v8, 0x2

    const/4 v9, 0x2

    if-ne v8, v9, :cond_0

    .line 65
    const-string v8, "ADF"

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "scanQuality":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "Low"

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    const-string v8, "Medium"

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    const-string v8, "Heigh"

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v6, "scanSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "A4"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    const-string v8, "Letter"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    const-string v8, "Legal"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    const-string v8, "3.5x5in"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v8, "4x6in"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v8, "5x7in"

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v4, "scanMode":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "Color"

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v8, "Grayscale"

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v3, "scanFileType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "JPG"

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    const-string v8, "PNG"

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    const-string v8, "PDF"

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v9, "scan_source"

    invoke-virtual {v8, v9, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 91
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v9, "scan_resolution"

    invoke-virtual {v8, v9, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 92
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v9, "scan_size"

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 93
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v9, "scan_mode"

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 94
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    const-string v9, "scan_file_type"

    invoke-virtual {v8, v9, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 96
    .end local v3    # "scanFileType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "scanMode":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "scanQuality":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "scanSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "scanSource":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    if-nez p1, :cond_3

    .line 97
    const-string v8, "org.androidprinting.intent.ACTION_RETURN_SCAN_ERROR"

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v8, "scanner-address"

    iget-object v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v8, "scan-error"

    const-string v9, "communication-error"

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    :goto_0
    const-string v8, "request-action"

    const-string v9, "org.androidscanning.intent.ACTION_GET_SCAN_OPTIONS"

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 108
    .local v2, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v1    # "returnIntent":Landroid/content/Intent;
    .end local v2    # "returnmsg":Landroid/os/Message;
    :cond_2
    :goto_1
    return-void

    .line 101
    .restart local v1    # "returnIntent":Landroid/content/Intent;
    :cond_3
    const-string v8, "org.androidprinting.intent.ACTION_RETURN_SCAN_CAPABILITIES"

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v8, "scanner-address"

    iget-object v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    iget-object v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->mCapsBundle:Landroid/os/Bundle;

    invoke-virtual {v1, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0

    .line 109
    .restart local v2    # "returnmsg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
