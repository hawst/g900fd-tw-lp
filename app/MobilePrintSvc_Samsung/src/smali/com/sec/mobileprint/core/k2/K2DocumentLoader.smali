.class public Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
.super Ljava/lang/Object;
.source "K2DocumentLoader.java"

# interfaces
.implements Lcom/sec/mobileprint/core/k2/K2Observer;


# instance fields
.field private documentPassword:Ljava/lang/String;

.field isLoading:Z

.field isLoadingSuccess:Z

.field k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

.field lockLoading:Ljava/lang/Object;

.field totalPageCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->documentPassword:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    .line 11
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    .line 12
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoading:Z

    .line 13
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    .line 14
    iput v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->totalPageCount:I

    .line 7
    return-void
.end method

.method private isWorking()Z
    .locals 2

    .prologue
    .line 81
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoading:Z

    if-eqz v0, :cond_0

    .line 85
    monitor-exit v1

    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private notifyFinishLoading()V
    .locals 2

    .prologue
    .line 119
    const-string v0, "notifyFinishLoading"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoading:Z

    .line 123
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 120
    monitor-exit v1

    .line 125
    return-void

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startLoadingLock()V
    .locals 2

    .prologue
    .line 95
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    monitor-enter v1

    .line 97
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoading:Z

    .line 95
    monitor-exit v1

    .line 99
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private waitUntilFinishLoading()V
    .locals 3

    .prologue
    .line 103
    iget-object v2, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    monitor-enter v2

    .line 105
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoading:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 108
    :try_start_1
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->lockLoading:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 115
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public closeK2DocumentRenderer()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->removeObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V

    .line 54
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->closeDocument()V

    .line 55
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    .line 58
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 63
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->closeK2DocumentRenderer()V

    .line 64
    return-void
.end method

.method public getHandle()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    if-nez v0, :cond_0

    .line 70
    const/16 v0, -0x3e7

    .line 72
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->getK2Handle()I

    move-result v0

    goto :goto_0
.end method

.method public getTotalPageCount()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->totalPageCount:I

    return v0
.end method

.method public loadDocumentBlocking(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "documentFilePath"    # Ljava/lang/String;
    .param p3, "documentPassword"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isWorking()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    :goto_0
    return v0

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->startLoadingLock()V

    .line 24
    iput-object p3, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->documentPassword:Ljava/lang/String;

    .line 26
    invoke-static {}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->getInstance()Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    .line 27
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v1, p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->registerObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V

    .line 28
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v1, p2, p1, v0, v0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->executeOpenDocument(Ljava/lang/String;Landroid/content/Context;II)Z

    .line 30
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setPrintMode300DPI()V

    .line 36
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->waitUntilFinishLoading()V

    .line 38
    iget-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    if-nez v1, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->closeK2DocumentRenderer()V

    goto :goto_0

    .line 43
    :cond_1
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    if-eqz v0, :cond_2

    .line 44
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->getTotalPageCount()I

    move-result v0

    iput v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->totalPageCount:I

    .line 46
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyCannotOpenFile()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    .line 154
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 155
    const-string v0, "notifyCannotOpenFile"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method public notifyFileLoadingFinished()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    .line 146
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 147
    const-string v0, "notifyFileLoadingFinished"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public notifyLoadedPage(I)V
    .locals 1
    .param p1, "pageNum"    # I

    .prologue
    .line 139
    const-string v0, "notifyLoadedPage"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public notifyMemoryOverflow()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    .line 162
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 163
    const-string v0, "notifyMemoryOverflow"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public notifyPDFInputPassword()V
    .locals 3

    .prologue
    .line 169
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->k2DocumentRenderer:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    iget-object v2, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->documentPassword:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setPassword(Ljava/lang/String;)Z

    move-result v0

    .line 170
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 172
    const-string v1, "notifyPDFInputPassword - setPassword error"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->isLoadingSuccess:Z

    .line 174
    invoke-direct {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->notifyFinishLoading()V

    .line 176
    :cond_0
    const-string v1, "notifyPDFInputPassword"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public notifyReLoadedImagePage(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 183
    return-void
.end method

.method public notifyStartFileLoading()V
    .locals 1

    .prologue
    .line 132
    const-string v0, "notifyStartFileLoading"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public notifyUpdatablePage(I)V
    .locals 0
    .param p1, "pageNum"    # I

    .prologue
    .line 189
    return-void
.end method
