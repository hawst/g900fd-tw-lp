.class Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;
.super Ljava/util/TimerTask;
.source "SamsungDeviceDiscovery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StopDiscoveryOnTimeOut"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;


# direct methods
.method private constructor <init>(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;->this$0:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;-><init>(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 112
    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;->this$0:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->access$3(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;->this$0:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->access$4(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;Z)V

    .line 114
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;->this$0:Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->access$3(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
