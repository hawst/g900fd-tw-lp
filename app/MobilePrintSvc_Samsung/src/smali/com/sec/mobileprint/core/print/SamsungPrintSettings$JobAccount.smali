.class public Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;
.super Ljava/lang/Object;
.source "SamsungPrintSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "JobAccount"
.end annotation


# instance fields
.field private mJobAccGroupPermission:Z

.field private mJobAccPassword:Ljava/lang/String;

.field private mJobAccUserName:Ljava/lang/String;

.field private mUseJobAccounting:Z

.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mUseJobAccounting:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;

    .line 54
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccGroupPermission:Z

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "useJobAccounting"    # Z
    .param p3, "userName"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "groupPermission"    # Z

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-boolean p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mUseJobAccounting:Z

    .line 59
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;

    .line 61
    iput-boolean p5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccGroupPermission:Z

    .line 62
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mUseJobAccounting:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccGroupPermission:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mUseJobAccounting:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Z)V
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccGroupPermission:Z

    return-void
.end method
