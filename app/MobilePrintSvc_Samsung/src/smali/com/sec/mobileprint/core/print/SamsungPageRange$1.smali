.class Lcom/sec/mobileprint/core/print/SamsungPageRange$1;
.super Ljava/lang/Object;
.source "SamsungPageRange.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPageRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/mobileprint/core/print/SamsungPageRange;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 272
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPageRange;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/sec/mobileprint/core/print/SamsungPageRange;-><init>(Landroid/os/Parcel;Lcom/sec/mobileprint/core/print/SamsungPageRange;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPageRange$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/mobileprint/core/print/SamsungPageRange;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 282
    new-array v0, p1, [Lcom/sec/mobileprint/core/print/SamsungPageRange;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPageRange$1;->newArray(I)[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    move-result-object v0

    return-object v0
.end method
