.class public interface abstract Lcom/sec/mobileprint/core/k2/K2Observer;
.super Ljava/lang/Object;
.source "K2Observer.java"


# virtual methods
.method public abstract notifyCannotOpenFile()V
.end method

.method public abstract notifyFileLoadingFinished()V
.end method

.method public abstract notifyLoadedPage(I)V
.end method

.method public abstract notifyMemoryOverflow()V
.end method

.method public abstract notifyPDFInputPassword()V
.end method

.method public abstract notifyReLoadedImagePage(I)V
.end method

.method public abstract notifyStartFileLoading()V
.end method

.method public abstract notifyUpdatablePage(I)V
.end method
