.class public Lcom/sec/mobileprint/core/print/SamsungPrintingTask;
.super Landroid/os/AsyncTask;
.source "SamsungPrintingTask.java"

# interfaces
.implements Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;
.implements Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;,
        Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;",
        "Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;"
    }
.end annotation


# instance fields
.field private callbackPrintServiceStatus:Lcom/sec/print/mobileprint/IPrintStatusCallback;

.field k2DocumentLoader:Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

.field private mCancel:Z

.field private mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

.field private mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

.field private mJobCompletedListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;

.field private mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

.field private mPrintServiceConnected:Z

.field private mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

.field private mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

.field private mWaitObject:Ljava/lang/Object;

.field private syncobjectK2DocumentLoader:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;)V
    .locals 2
    .param p1, "job"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .param p2, "listener"    # Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->syncobjectK2DocumentLoader:Ljava/lang/Object;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->k2DocumentLoader:Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

    .line 305
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->callbackPrintServiceStatus:Lcom/sec/print/mobileprint/IPrintStatusCallback;

    .line 58
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mJobCompletedListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;

    .line 59
    check-cast p2, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .end local p2    # "listener":Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .line 60
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mWaitObject:Ljava/lang/Object;

    .line 62
    new-instance v0, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-direct {v0}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    .line 63
    new-instance v0, Lcom/sec/mobileprint/core/utils/PrintingFramework;

    sget-object v1, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/mobileprint/core/utils/PrintingFramework;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    .line 64
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/PrintingFramework;->addPrintServiceConnectedListener(Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;)V

    .line 65
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/PrintingFramework;->InitializePrintingService()Z

    .line 66
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->startDeviceManagerConnector()V

    .line 67
    return-void
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    return-object v0
.end method

.method private createImageSet()Lcom/sec/print/mobileprint/pagedata/PageSet;
    .locals 17

    .prologue
    .line 252
    new-instance v10, Lcom/sec/print/mobileprint/pagedata/PageSet;

    invoke-direct {v10}, Lcom/sec/print/mobileprint/pagedata/PageSet;-><init>()V

    .line 253
    .local v10, "pageSet":Lcom/sec/print/mobileprint/pagedata/PageSet;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v14}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getContentType()I

    move-result v14

    sget v15, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    if-ne v14, v15, :cond_5

    .line 254
    const-string v14, "k2ViewerJni"

    invoke-static {v14}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 255
    const/4 v7, 0x0

    .line 256
    .local v7, "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->syncobjectK2DocumentLoader:Ljava/lang/Object;

    monitor-enter v15

    .line 258
    :try_start_0
    new-instance v8, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

    invoke-direct {v8}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    .end local v7    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .local v8, "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 261
    sget-object v14, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v15}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v8, v14, v15, v0}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->loadDocumentBlocking(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 262
    invoke-virtual {v8}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->getHandle()I

    move-result v3

    .line 263
    .local v3, "handle":I
    invoke-virtual {v8}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->getTotalPageCount()I

    move-result v13

    .line 266
    .local v13, "totalPageCount":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v14}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getPageRange()[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    move-result-object v12

    .line 267
    .local v12, "sprArray":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    if-eqz v12, :cond_0

    array-length v14, v12

    if-lez v14, :cond_0

    .line 268
    const/4 v14, 0x0

    aget-object v14, v12, v14

    sget-object v15, Lcom/sec/mobileprint/core/print/SamsungPageRange;->ALL_PAGES:Lcom/sec/mobileprint/core/print/SamsungPageRange;

    invoke-virtual {v14, v15}, Lcom/sec/mobileprint/core/print/SamsungPageRange;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 269
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, v13, :cond_1

    .line 300
    .end local v3    # "handle":I
    .end local v4    # "i":I
    .end local v8    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .end local v12    # "sprArray":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .end local v13    # "totalPageCount":I
    :cond_0
    return-object v10

    .line 256
    .restart local v7    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    :catchall_0
    move-exception v14

    :goto_1
    :try_start_2
    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 270
    .end local v7    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .restart local v3    # "handle":I
    .restart local v4    # "i":I
    .restart local v8    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .restart local v12    # "sprArray":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .restart local v13    # "totalPageCount":I
    :cond_1
    new-instance v9, Lcom/sec/print/mobileprint/pagedata/Page;

    invoke-direct {v9}, Lcom/sec/print/mobileprint/pagedata/Page;-><init>()V

    .line 271
    .local v9, "page":Lcom/sec/print/mobileprint/pagedata/Page;
    new-instance v5, Lcom/sec/print/mobileprint/pagedata/K2MImageData;

    add-int/lit8 v14, v4, 0x1

    invoke-direct {v5, v3, v14}, Lcom/sec/print/mobileprint/pagedata/K2MImageData;-><init>(II)V

    .line 272
    .local v5, "imageData":Lcom/sec/print/mobileprint/pagedata/K2MImageData;
    invoke-virtual {v9, v5}, Lcom/sec/print/mobileprint/pagedata/Page;->add(Lcom/sec/print/mobileprint/pagedata/IPageData;)V

    .line 273
    invoke-virtual {v10, v9}, Lcom/sec/print/mobileprint/pagedata/PageSet;->add(Lcom/sec/print/mobileprint/pagedata/Page;)V

    .line 269
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 277
    .end local v4    # "i":I
    .end local v5    # "imageData":Lcom/sec/print/mobileprint/pagedata/K2MImageData;
    .end local v9    # "page":Lcom/sec/print/mobileprint/pagedata/Page;
    :cond_2
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    if-ge v4, v13, :cond_0

    .line 278
    array-length v15, v12

    const/4 v14, 0x0

    :goto_3
    if-lt v14, v15, :cond_3

    .line 277
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 278
    :cond_3
    aget-object v11, v12, v14

    .line 279
    .local v11, "spr":Lcom/sec/mobileprint/core/print/SamsungPageRange;
    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPageRange;->getStart()I

    move-result v16

    move/from16 v0, v16

    if-lt v4, v0, :cond_4

    invoke-virtual {v11}, Lcom/sec/mobileprint/core/print/SamsungPageRange;->getEnd()I

    move-result v16

    move/from16 v0, v16

    if-gt v4, v0, :cond_4

    .line 280
    new-instance v9, Lcom/sec/print/mobileprint/pagedata/Page;

    invoke-direct {v9}, Lcom/sec/print/mobileprint/pagedata/Page;-><init>()V

    .line 281
    .restart local v9    # "page":Lcom/sec/print/mobileprint/pagedata/Page;
    new-instance v5, Lcom/sec/print/mobileprint/pagedata/K2MImageData;

    add-int/lit8 v16, v4, 0x1

    move/from16 v0, v16

    invoke-direct {v5, v3, v0}, Lcom/sec/print/mobileprint/pagedata/K2MImageData;-><init>(II)V

    .line 282
    .restart local v5    # "imageData":Lcom/sec/print/mobileprint/pagedata/K2MImageData;
    invoke-virtual {v9, v5}, Lcom/sec/print/mobileprint/pagedata/Page;->add(Lcom/sec/print/mobileprint/pagedata/IPageData;)V

    .line 283
    invoke-virtual {v10, v9}, Lcom/sec/print/mobileprint/pagedata/PageSet;->add(Lcom/sec/print/mobileprint/pagedata/Page;)V

    .line 278
    .end local v5    # "imageData":Lcom/sec/print/mobileprint/pagedata/K2MImageData;
    .end local v9    # "page":Lcom/sec/print/mobileprint/pagedata/Page;
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 292
    .end local v3    # "handle":I
    .end local v4    # "i":I
    .end local v8    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .end local v11    # "spr":Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .end local v12    # "sprArray":[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .end local v13    # "totalPageCount":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v14}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v2

    .line 293
    .local v2, "fileList":[Ljava/lang/String;
    array-length v15, v2

    const/4 v14, 0x0

    :goto_4
    if-ge v14, v15, :cond_0

    aget-object v1, v2, v14

    .line 294
    .local v1, "file":Ljava/lang/String;
    new-instance v9, Lcom/sec/print/mobileprint/pagedata/Page;

    invoke-direct {v9}, Lcom/sec/print/mobileprint/pagedata/Page;-><init>()V

    .line 295
    .restart local v9    # "page":Lcom/sec/print/mobileprint/pagedata/Page;
    new-instance v6, Lcom/sec/print/mobileprint/pagedata/FileImageData;

    invoke-direct {v6, v1}, Lcom/sec/print/mobileprint/pagedata/FileImageData;-><init>(Ljava/lang/String;)V

    .line 296
    .local v6, "imagedata":Lcom/sec/print/mobileprint/pagedata/FileImageData;
    invoke-virtual {v9, v6}, Lcom/sec/print/mobileprint/pagedata/Page;->add(Lcom/sec/print/mobileprint/pagedata/IPageData;)V

    .line 297
    invoke-virtual {v10, v9}, Lcom/sec/print/mobileprint/pagedata/PageSet;->add(Lcom/sec/print/mobileprint/pagedata/Page;)V

    .line 293
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 256
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "fileList":[Ljava/lang/String;
    .end local v6    # "imagedata":Lcom/sec/print/mobileprint/pagedata/FileImageData;
    .end local v9    # "page":Lcom/sec/print/mobileprint/pagedata/Page;
    .restart local v8    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    :catchall_1
    move-exception v14

    move-object v7, v8

    .end local v8    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    .restart local v7    # "k2DocumentLoader":Lcom/sec/mobileprint/core/k2/K2DocumentLoader;
    goto :goto_1
.end method

.method private createOutputInfo()Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    .locals 5

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->isPrintToFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getOutputFilePath()Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/mobileprint/core/utils/FileUtil;->createFolder(Ljava/lang/String;)Z

    .line 239
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/FileOutputInfo;

    .end local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printerinfo/FileOutputInfo;-><init>(Ljava/lang/String;)V

    .line 247
    .end local v1    # "path":Ljava/lang/String;
    .restart local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_0
    :goto_0
    return-object v0

    .line 240
    :cond_1
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v2

    sget-object v3, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v2, v3, :cond_2

    .line 242
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;

    .end local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPortNo()I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;-><init>(Ljava/lang/String;I)V

    .line 243
    .restart local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v2

    sget-object v3, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v2, v3, :cond_0

    .line 244
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    .end local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getVid()I

    move-result v3

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPid()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;-><init>(Ljava/lang/String;II)V

    .restart local v0    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    goto :goto_0
.end method

.method private createPrinterInfo()Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 218
    const/4 v1, 0x0

    .line 219
    .local v1, "deviceName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 220
    .local v0, "color":I
    const/4 v3, 0x0

    .line 222
    .local v3, "supportedPDLTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v5

    sget-object v6, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v5, v6, :cond_2

    .line 223
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v5, v5, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 227
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v5, v5, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v3

    .line 228
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v5, v5, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceInfo()Lcom/sec/print/mobileprint/dm/DeviceInfo;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 229
    new-instance v2, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    check-cast v3, Ljava/util/ArrayList;

    .end local v3    # "supportedPDLTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-ne v0, v4, :cond_3

    :goto_1
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->createOutputInfo()Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    move-result-object v5

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;-><init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/sec/print/mobileprint/printerinfo/IOutputInfo;)V

    .line 230
    .local v2, "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    return-object v2

    .line 224
    .end local v2    # "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    .restart local v3    # "supportedPDLTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 225
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 229
    .end local v3    # "supportedPDLTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public cancelPrintJob(I)V
    .locals 2
    .param p1, "jobId"    # I

    .prologue
    .line 196
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintServiceConnected:Z

    if-eqz v1, :cond_0

    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    iget-object v1, v1, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    invoke-interface {v1}, Lcom/sec/print/mobileprint/ISamsungPrintingService;->cancel()V

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 98
    :try_start_0
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 145
    :goto_0
    return-object v5

    .line 99
    :cond_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v5

    sget-object v6, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v5, v6, :cond_3

    .line 100
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceIpAddress(Ljava/lang/String;)V

    .line 101
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 102
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v6, v6, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v7}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_a

    .line 118
    :try_start_1
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto :goto_0

    .line 104
    :cond_2
    :try_start_2
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 110
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 106
    .end local v1    # "e1":Landroid/os/RemoteException;
    :cond_3
    :try_start_3
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getConnectionType()Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v5

    sget-object v6, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    if-ne v5, v6, :cond_1

    .line 108
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceInfo:Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/mobileprint/core/print/DeviceInfoWithCaps;->setDeviceName(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 119
    :cond_4
    :try_start_4
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getPrintOptionAttributeSet()Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;

    move-result-object v3

    .line 120
    .local v3, "printOptionAttributeSet":Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0

    .line 121
    :cond_5
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->createPrinterInfo()Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    move-result-object v4

    .line 122
    .local v4, "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0

    .line 123
    :cond_6
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->createImageSet()Lcom/sec/print/mobileprint/pagedata/PageSet;

    move-result-object v2

    .line 125
    .local v2, "listImages":Lcom/sec/print/mobileprint/pagedata/PageSet;
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintServiceConnected:Z

    if-nez v5, :cond_7

    .line 126
    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mWaitObject:Ljava/lang/Object;

    monitor-enter v6
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 127
    :try_start_5
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    .line 126
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 130
    :cond_7
    if-eqz v3, :cond_9

    if-eqz v4, :cond_9

    .line 131
    :try_start_6
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    iget-object v5, v5, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    iget-object v6, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->callbackPrintServiceStatus:Lcom/sec/print/mobileprint/IPrintStatusCallback;

    invoke-interface {v5, v6}, Lcom/sec/print/mobileprint/ISamsungPrintingService;->registerCallback(Lcom/sec/print/mobileprint/IPrintStatusCallback;)V

    .line 132
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v5, :cond_8

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-result-object v5

    goto/16 :goto_0

    .line 126
    :catchall_0
    move-exception v5

    :try_start_7
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v5
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 135
    .end local v2    # "listImages":Lcom/sec/print/mobileprint/pagedata/PageSet;
    .end local v3    # "printOptionAttributeSet":Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .end local v4    # "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    :catch_1
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 137
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0

    .line 133
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "listImages":Lcom/sec/print/mobileprint/pagedata/PageSet;
    .restart local v3    # "printOptionAttributeSet":Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .restart local v4    # "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    :cond_8
    :try_start_9
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    iget-object v5, v5, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    invoke-interface {v5, v3, v2, v4}, Lcom/sec/print/mobileprint/ISamsungPrintingService;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 145
    :cond_9
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0

    .line 142
    .end local v2    # "listImages":Lcom/sec/print/mobileprint/pagedata/PageSet;
    .end local v3    # "printOptionAttributeSet":Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .end local v4    # "printerInfo":Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    :cond_a
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onDeviceManagerConnectorStarted()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 75
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->removeDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)Z

    .line 80
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 152
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 154
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->shouldDeleteFilesAfterPrinting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    :goto_0
    if-lt v3, v6, :cond_6

    .line 163
    :cond_0
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-eqz v3, :cond_1

    .line 164
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v5

    const/4 v6, 0x6

    invoke-interface {v3, v5, v6, v4}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;->onJobStatusUpdated(III)V

    .line 165
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mCancel:Z

    if-nez v3, :cond_2

    .line 166
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v4

    const/4 v5, 0x7

    const/4 v6, 0x2

    invoke-interface {v3, v4, v5, v6}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;->onJobStatusUpdated(III)V

    .line 168
    :cond_2
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mJobCompletedListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;->onJobCompleted(I)V

    .line 169
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->syncobjectK2DocumentLoader:Ljava/lang/Object;

    monitor-enter v4

    .line 171
    :try_start_1
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->k2DocumentLoader:Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

    if-eqz v3, :cond_3

    .line 173
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->k2DocumentLoader:Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/k2/K2DocumentLoader;->destroy()V

    .line 174
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->k2DocumentLoader:Lcom/sec/mobileprint/core/k2/K2DocumentLoader;

    .line 169
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    if-eqz v3, :cond_4

    .line 180
    :try_start_2
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    iget-object v3, v3, Lcom/sec/mobileprint/core/utils/PrintingFramework;->PrintService:Lcom/sec/print/mobileprint/ISamsungPrintingService;

    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->callbackPrintServiceStatus:Lcom/sec/print/mobileprint/IPrintStatusCallback;

    invoke-interface {v3, v4}, Lcom/sec/print/mobileprint/ISamsungPrintingService;->unregisterCallback(Lcom/sec/print/mobileprint/IPrintStatusCallback;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 185
    :goto_2
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/utils/PrintingFramework;->releaseService()V

    .line 186
    iput-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    .line 188
    :cond_4
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v3, :cond_5

    .line 189
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->releaseService()V

    .line 190
    iput-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 193
    :cond_5
    return-void

    .line 155
    :cond_6
    :try_start_3
    aget-object v2, v5, v3

    .line 156
    .local v2, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 155
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 160
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 169
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 181
    :catch_1
    move-exception v0

    .line 183
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method public onPrintServiceConnected()V
    .locals 2

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintServiceConnected:Z

    .line 87
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintingService:Lcom/sec/mobileprint/core/utils/PrintingFramework;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/PrintingFramework;->removePrintServiceConnectedListener(Lcom/sec/mobileprint/core/utils/PrintingFramework$IPrintServiceConnectedListener;)Z

    .line 88
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mWaitObject:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 88
    monitor-exit v1

    .line 91
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startDeviceManagerConnector()V
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    sget-object v1, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 212
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->InitializeDiscovery()Z

    .line 213
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mDeviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->addDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)V

    .line 214
    return-void
.end method
