.class public Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;
.super Ljava/lang/Object;
.source "K2DocumentRenderer.java"

# interfaces
.implements Lcom/k2mobile/k2lib$OnCreateImageListener;
.implements Lcom/k2mobile/k2lib$OnFileLoadingListener;
.implements Lcom/sec/mobileprint/core/k2/K2Subject;


# static fields
.field static isSkip:Z

.field private static volatile mK2DocumentInstance:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;


# instance fields
.field private final DEFAULT_ZOOM:I

.field private final PREVIEW_DPI:I

.field private isOpenedFile:Z

.field private lock:Ljava/util/concurrent/locks/Lock;

.field private mFileLoadingFinished:Z

.field private mK2Library:Lcom/k2mobile/k2lib;

.field public mScreenHeight:I

.field public mScreenWidth:I

.field private mZoom:I

.field private observers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/core/k2/K2Observer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 649
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isSkip:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x4b

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->PREVIEW_DPI:I

    .line 29
    iput v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->DEFAULT_ZOOM:I

    .line 30
    iput v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    .line 31
    iput v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenWidth:I

    .line 32
    iput v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenHeight:I

    .line 33
    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    .line 35
    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    .line 36
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->lock:Ljava/util/concurrent/locks/Lock;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Lcom/k2mobile/k2lib;

    invoke-direct {v0}, Lcom/k2mobile/k2lib;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0, p0}, Lcom/k2mobile/k2lib;->setOnCreateImageListener(Lcom/k2mobile/k2lib$OnCreateImageListener;)V

    .line 61
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0, p0}, Lcom/k2mobile/k2lib;->setOnFileLoadingListener(Lcom/k2mobile/k2lib$OnFileLoadingListener;)V

    .line 63
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->TEMP_K2_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/k2mobile/k2lib;->setTemporary(Ljava/lang/String;)I

    .line 64
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setPreviewMode100DPI()V

    .line 65
    return-void
.end method

.method private GetScaleRate(IIII)D
    .locals 8
    .param p1, "destWidth"    # I
    .param p2, "destHeight"    # I
    .param p3, "srcWidth"    # I
    .param p4, "srcHeight"    # I

    .prologue
    .line 531
    int-to-double v4, p1

    int-to-double v6, p3

    div-double v2, v4, v6

    .line 532
    .local v2, "scaleRateWidth":D
    int-to-double v4, p2

    int-to-double v6, p4

    div-double v0, v4, v6

    .line 534
    .local v0, "scaleRateHeight":D
    cmpl-double v4, v2, v0

    if-lez v4, :cond_0

    .end local v0    # "scaleRateHeight":D
    :goto_0
    return-wide v0

    .restart local v0    # "scaleRateHeight":D
    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2DocumentInstance:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    if-nez v0, :cond_1

    .line 44
    const-class v1, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    monitor-enter v1

    .line 46
    :try_start_0
    sget-object v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2DocumentInstance:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    invoke-direct {v0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;-><init>()V

    sput-object v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2DocumentInstance:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    .line 44
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :cond_1
    sget-object v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2DocumentInstance:Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getScaleRate(IIII)D
    .locals 8
    .param p1, "destWidth"    # I
    .param p2, "destHeight"    # I
    .param p3, "srcWidth"    # I
    .param p4, "srcHeight"    # I

    .prologue
    .line 252
    int-to-double v4, p1

    int-to-double v6, p3

    div-double v2, v4, v6

    .line 253
    .local v2, "scaleRateWidth":D
    int-to-double v4, p2

    int-to-double v6, p4

    div-double v0, v4, v6

    .line 255
    .local v0, "scaleRateHeight":D
    cmpl-double v4, v2, v0

    if-lez v4, :cond_0

    .end local v0    # "scaleRateHeight":D
    :goto_0
    return-wide v0

    .restart local v0    # "scaleRateHeight":D
    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method private initK2Library(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 539
    new-instance v6, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v6}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 540
    .local v6, "memoryInfo":Landroid/app/ActivityManager$MemoryInfo;
    const-string v7, "activity"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager;

    invoke-virtual {v7, v6}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 542
    iget-wide v9, v6, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    long-to-double v9, v9

    const-wide/high16 v11, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v9, v11

    double-to-int v7, v9

    int-to-long v9, v7

    invoke-static {v9, v10}, Lcom/k2mobile/k2lib;->setUseableSpace(J)V

    .line 543
    iget-object v7, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v9, 0x4b

    invoke-virtual {v7, v9}, Lcom/k2mobile/k2lib;->setDisplayDpi(I)I

    .line 544
    iget-object v7, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v9, 0xd

    invoke-virtual {v7, v9}, Lcom/k2mobile/k2lib;->setColorType(I)I

    .line 545
    iget-object v7, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Lcom/k2mobile/k2lib;->setPortrait(Z)I

    .line 546
    iget-object v7, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v7, v8}, Lcom/k2mobile/k2lib;->setShowBackground(I)I

    .line 551
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 552
    .local v2, "fontFolder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 557
    .local v5, "list":[Ljava/io/File;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 558
    .local v0, "arrayFontPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansFallback.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSans.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/Roboto-Regular.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansArabic.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansHebrew.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansHebrew-Regular.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansThai.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansArmenian.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansEthiopic-Regular.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/DroidSansGeorgian.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/NanumGothic.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/fonts/NanumMyungjo.ttf"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    array-length v9, v5

    move v7, v8

    :goto_0
    if-lt v7, v9, :cond_2

    .line 576
    :cond_0
    const/4 v4, 0x0

    .line 577
    .local v4, "fontPathAll":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 591
    iget-object v7, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v7, v4}, Lcom/k2mobile/k2lib;->setFont(Ljava/lang/String;)I

    .line 592
    return-void

    .line 571
    .end local v4    # "fontPathAll":Ljava/lang/String;
    :cond_2
    aget-object v1, v5, v7

    .line 572
    .local v1, "fontFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/16 v10, 0x64

    if-ge v8, v10, :cond_0

    .line 571
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 577
    .end local v1    # "fontFile":Ljava/io/File;
    .restart local v4    # "fontPathAll":Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 579
    .local v3, "fontPath":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isExists(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 581
    if-nez v4, :cond_4

    .line 583
    move-object v4, v3

    .line 584
    goto :goto_1

    .line 586
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private isExists(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 596
    if-nez p1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return v1

    .line 601
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 603
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 605
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public K2CreatePageImage(IIIIII)Landroid/graphics/Bitmap;
    .locals 17
    .param p1, "pageNum"    # I
    .param p2, "pos_x"    # I
    .param p3, "pos_y"    # I
    .param p4, "maxWidth"    # I
    .param p5, "maxHeight"    # I
    .param p6, "zoom"    # I

    .prologue
    .line 463
    move/from16 v6, p4

    .line 464
    .local v6, "width":I
    move/from16 v7, p5

    .line 465
    .local v7, "height":I
    const/16 v16, 0x0

    .line 466
    .local v16, "imageBitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 468
    .local v8, "arrImageBuffer":[I
    mul-int v1, v6, v7

    :try_start_0
    new-array v8, v1, [I

    .line 469
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->stopFileOpen()V

    .line 470
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    add-int/lit8 v2, p1, 0x1

    move/from16 v3, p6

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-virtual/range {v1 .. v8}, Lcom/k2mobile/k2lib;->createPageImageBlocking(IIIIII[I)I

    move-result v15

    .line 471
    .local v15, "errorCode":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->resumeFileOpen()V

    .line 472
    if-eqz v15, :cond_0

    .line 473
    const/4 v8, 0x0

    .line 474
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 475
    const/4 v1, 0x0

    .line 486
    .end local v15    # "errorCode":I
    :goto_0
    return-object v1

    .line 478
    .restart local v15    # "errorCode":I
    :cond_0
    const/4 v9, 0x0

    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move v10, v6

    move v11, v6

    move v12, v7

    invoke-static/range {v8 .. v13}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 484
    .end local v15    # "errorCode":I
    :goto_1
    const/4 v8, 0x0

    .line 485
    invoke-static {}, Ljava/lang/System;->gc()V

    move-object/from16 v1, v16

    .line 486
    goto :goto_0

    .line 479
    :catch_0
    move-exception v14

    .line 480
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v14}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 481
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 482
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public cancelImageLoad()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->cancelPageImage()I

    .line 181
    return-void
.end method

.method public closeDocument()V
    .locals 3

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->stopFileOpen()V

    .line 188
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->cancelImageLoad()V

    .line 189
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v1}, Lcom/k2mobile/k2lib;->closeFile()I

    move-result v0

    .line 190
    .local v0, "result":I
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    .line 191
    if-eqz v0, :cond_0

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error close :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 195
    return-void
.end method

.method public doNotifyCannotOpenFile()V
    .locals 3

    .prologue
    .line 732
    const-string v1, "doNotifyCannotOpenFile()"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 735
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 739
    return-void

    .line 735
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 737
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyCannotOpenFile()V

    goto :goto_0
.end method

.method public doNotifyFileLoadingFinished()V
    .locals 3

    .prologue
    .line 720
    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 727
    :goto_1
    return-void

    .line 720
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 722
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyFileLoadingFinished()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 724
    .end local v0    # "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public doNotifyImageReload(I)V
    .locals 3
    .param p1, "pageNum"    # I

    .prologue
    .line 781
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 785
    return-void

    .line 781
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 783
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0, p1}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyReLoadedImagePage(I)V

    goto :goto_0
.end method

.method public doNotifyLoadedPage(I)V
    .locals 3
    .param p1, "pageNum"    # I

    .prologue
    .line 710
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 714
    return-void

    .line 710
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 712
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0, p1}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyLoadedPage(I)V

    goto :goto_0
.end method

.method public doNotifyMemoryOverflow()V
    .locals 3

    .prologue
    .line 743
    const-string v1, "doNotifyMemoryOverflow()"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 745
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 749
    return-void

    .line 745
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 747
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyMemoryOverflow()V

    goto :goto_0
.end method

.method public doNotifyPDFInputPassword()V
    .locals 3

    .prologue
    .line 754
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 758
    return-void

    .line 754
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 756
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyPDFInputPassword()V

    goto :goto_0
.end method

.method public doNotifyStartFileLoading()V
    .locals 3

    .prologue
    .line 700
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isSkip:Z

    .line 701
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 705
    return-void

    .line 701
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 703
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyStartFileLoading()V

    goto :goto_0
.end method

.method public doNotifyUpdatablePage(I)V
    .locals 3
    .param p1, "pageNum"    # I

    .prologue
    .line 790
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 794
    return-void

    .line 790
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/k2/K2Observer;

    .line 792
    .local v0, "observer":Lcom/sec/mobileprint/core/k2/K2Observer;
    invoke-interface {v0, p1}, Lcom/sec/mobileprint/core/k2/K2Observer;->notifyUpdatablePage(I)V

    goto :goto_0
.end method

.method public executeOpenDocument(Ljava/lang/String;Landroid/content/Context;II)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "screenWidth"    # I
    .param p4, "screenHeight"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    iput-boolean v3, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    .line 153
    :try_start_0
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->initK2Library(Landroid/content/Context;)V

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "code_page":I
    iget-object v5, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v5, p1, v0}, Lcom/k2mobile/k2lib;->openFile(Ljava/lang/String;I)I

    move-result v2

    .line 156
    .local v2, "errcode":I
    if-eqz v2, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyCannotOpenFile()V

    .line 176
    .end local v0    # "code_page":I
    .end local v2    # "errcode":I
    :goto_0
    return v3

    .line 164
    .restart local v0    # "code_page":I
    .restart local v2    # "errcode":I
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    iput p3, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenWidth:I

    .line 172
    iput p4, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenHeight:I

    .line 174
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyStartFileLoading()V

    move v3, v4

    .line 176
    goto :goto_0

    .line 165
    .end local v0    # "code_page":I
    .end local v2    # "errcode":I
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception :: mK2Library.openFile() => "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public declared-synchronized getCropPageImageBitmap(II)Landroid/graphics/Bitmap;
    .locals 27
    .param p1, "pageNum"    # I
    .param p2, "widthHeight"    # I

    .prologue
    .line 362
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setAlwaysPortraitMode(Z)V

    .line 363
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v2, :cond_0

    .line 365
    const/16 v20, 0x0

    .line 407
    :goto_0
    monitor-exit p0

    return-object v20

    .line 367
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v3, 0x4b

    move/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v21

    .line 369
    .local v21, "rectDefaultImage":Landroid/graphics/Rect;
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v16

    .line 370
    .local v16, "defaultWidth":I
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v15

    .line 372
    .local v15, "defaultHeight":I
    move/from16 v0, v16

    if-ge v0, v15, :cond_1

    move/from16 v25, v16

    .line 373
    .local v25, "shortSize":I
    :goto_1
    move/from16 v0, p2

    int-to-double v2, v0

    move/from16 v0, v25

    int-to-double v4, v0

    div-double v23, v2, v4

    .line 375
    .local v23, "scaleRate":D
    const-wide v2, 0x4052c00000000000L    # 75.0

    mul-double v2, v2, v23

    double-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    move/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v22

    .line 381
    .local v22, "rectImage":Landroid/graphics/Rect;
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v26

    .line 382
    .local v26, "width":I
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->height()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v19

    .line 383
    .local v19, "height":I
    move/from16 v0, v26

    move/from16 v1, v19

    if-le v0, v1, :cond_2

    move/from16 v7, v19

    .line 385
    .local v7, "resizeSize":I
    :goto_2
    const/16 v20, 0x0

    .line 387
    .local v20, "imageBitmap":Landroid/graphics/Bitmap;
    mul-int v2, v7, v7

    :try_start_2
    new-array v9, v2, [I

    .line 388
    .local v9, "arrImageBuffer":[I
    const/16 v18, 0x0

    .line 389
    .local v18, "errorCode":I
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 390
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    sub-int v3, v26, v7

    div-int/lit8 v5, v3, 0x2

    sub-int v3, v19, v7

    div-int/lit8 v6, v3, 0x2

    move/from16 v3, p1

    move v8, v7

    invoke-virtual/range {v2 .. v9}, Lcom/k2mobile/k2lib;->createPageImageBlocking(IIIIII[I)I

    move-result v18

    .line 389
    monitor-exit p0

    .line 393
    if-eqz v18, :cond_3

    .line 395
    const/4 v9, 0x0

    .line 396
    const/16 v20, 0x0

    goto :goto_0

    .end local v7    # "resizeSize":I
    .end local v9    # "arrImageBuffer":[I
    .end local v18    # "errorCode":I
    .end local v19    # "height":I
    .end local v20    # "imageBitmap":Landroid/graphics/Bitmap;
    .end local v22    # "rectImage":Landroid/graphics/Rect;
    .end local v23    # "scaleRate":D
    .end local v25    # "shortSize":I
    .end local v26    # "width":I
    :cond_1
    move/from16 v25, v15

    .line 372
    goto :goto_1

    .restart local v19    # "height":I
    .restart local v22    # "rectImage":Landroid/graphics/Rect;
    .restart local v23    # "scaleRate":D
    .restart local v25    # "shortSize":I
    .restart local v26    # "width":I
    :cond_2
    move/from16 v7, v26

    .line 383
    goto :goto_2

    .line 389
    .restart local v7    # "resizeSize":I
    .restart local v9    # "arrImageBuffer":[I
    .restart local v18    # "errorCode":I
    .restart local v20    # "imageBitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 401
    .end local v9    # "arrImageBuffer":[I
    .end local v18    # "errorCode":I
    :catch_0
    move-exception v17

    .line 402
    .local v17, "e":Ljava/lang/OutOfMemoryError;
    :try_start_5
    invoke-virtual/range {v17 .. v17}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 403
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 404
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 399
    .end local v17    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v9    # "arrImageBuffer":[I
    .restart local v18    # "errorCode":I
    :cond_3
    const/4 v10, 0x0

    :try_start_6
    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move v11, v7

    move v12, v7

    move v13, v7

    invoke-static/range {v9 .. v14}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v20

    .line 400
    goto/16 :goto_0

    .line 362
    .end local v7    # "resizeSize":I
    .end local v9    # "arrImageBuffer":[I
    .end local v15    # "defaultHeight":I
    .end local v16    # "defaultWidth":I
    .end local v18    # "errorCode":I
    .end local v19    # "height":I
    .end local v20    # "imageBitmap":Landroid/graphics/Bitmap;
    .end local v21    # "rectDefaultImage":Landroid/graphics/Rect;
    .end local v22    # "rectImage":Landroid/graphics/Rect;
    .end local v23    # "scaleRate":D
    .end local v25    # "shortSize":I
    .end local v26    # "width":I
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getCropPageImageBitmap(III)Landroid/graphics/Bitmap;
    .locals 31
    .param p1, "pageNum"    # I
    .param p2, "nFrameWidth"    # I
    .param p3, "nFrameHeight"    # I

    .prologue
    .line 290
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setAlwaysPortraitMode(Z)V

    .line 291
    move/from16 v0, p2

    move/from16 v1, p3

    if-le v0, v1, :cond_0

    const/16 v23, 0x1

    .line 292
    .local v23, "isLandscapeFrame":Z
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    if-nez v5, :cond_1

    .line 294
    const/16 v22, 0x0

    .line 357
    :goto_1
    return-object v22

    .line 291
    .end local v23    # "isLandscapeFrame":Z
    :cond_0
    const/16 v23, 0x0

    goto :goto_0

    .line 297
    .restart local v23    # "isLandscapeFrame":Z
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v6, 0x4b

    move/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v27

    .line 299
    .local v27, "rectDefaultImage":Landroid/graphics/Rect;
    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->width()I

    move-result v19

    .line 300
    .local v19, "defaultWidth":I
    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Rect;->height()I

    move-result v18

    .line 302
    .local v18, "defaultHeight":I
    move/from16 v26, p2

    .line 303
    .local v26, "nTempFrameWidth":I
    move/from16 v25, p3

    .line 304
    .local v25, "nTempFrameHeight":I
    invoke-virtual/range {p0 .. p1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isLandscapePage(I)Z

    move-result v24

    .line 307
    .local v24, "isLandscapePage":Z
    if-eqz v23, :cond_3

    if-nez v24, :cond_3

    .line 310
    move/from16 v26, p3

    .line 311
    move/from16 v25, p2

    .line 319
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v25

    move/from16 v3, v19

    move/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->getScaleRate(IIII)D

    move-result-wide v29

    .line 321
    .local v29, "scaleRate":D
    const-wide v5, 0x4052c00000000000L    # 75.0

    mul-double v5, v5, v29

    double-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    .line 325
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    if-nez v5, :cond_4

    .line 327
    const/16 v22, 0x0

    goto :goto_1

    .line 312
    .end local v29    # "scaleRate":D
    :cond_3
    if-nez v23, :cond_2

    if-eqz v24, :cond_2

    .line 315
    move/from16 v26, p3

    .line 316
    move/from16 v25, p2

    goto :goto_2

    .line 330
    .restart local v29    # "scaleRate":D
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    move/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v28

    .line 332
    .local v28, "rectImage":Landroid/graphics/Rect;
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->width()I

    move-result v10

    .line 333
    .local v10, "width":I
    invoke-virtual/range {v28 .. v28}, Landroid/graphics/Rect;->height()I

    move-result v11

    .line 334
    .local v11, "height":I
    const/16 v22, 0x0

    .line 336
    .local v22, "imageBitmap":Landroid/graphics/Bitmap;
    mul-int v5, v10, v11

    :try_start_0
    new-array v12, v5, [I

    .line 337
    .local v12, "arrImageBuffer":[I
    const/16 v21, 0x0

    .line 338
    .local v21, "errorCode":I
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/k2mobile/k2lib;->createPageImageBlocking(IIIIII[I)I

    move-result v21

    .line 338
    monitor-exit p0

    .line 342
    if-eqz v21, :cond_5

    .line 344
    const/4 v12, 0x0

    .line 345
    const/16 v22, 0x0

    goto/16 :goto_1

    .line 338
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 351
    .end local v12    # "arrImageBuffer":[I
    .end local v21    # "errorCode":I
    :catch_0
    move-exception v20

    .line 352
    .local v20, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 353
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 354
    const/16 v22, 0x0

    goto/16 :goto_1

    .line 348
    .end local v20    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v12    # "arrImageBuffer":[I
    .restart local v21    # "errorCode":I
    :cond_5
    const/4 v13, 0x0

    :try_start_3
    sget-object v17, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move v14, v10

    move v15, v10

    move/from16 v16, v11

    invoke-static/range {v12 .. v17}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v22

    .line 349
    goto/16 :goto_1
.end method

.method public declared-synchronized getCropPageImageBitmapPDF(III)Landroid/graphics/Bitmap;
    .locals 24
    .param p1, "pageNum"    # I
    .param p2, "_width"    # I
    .param p3, "_height"    # I

    .prologue
    .line 412
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->setAlwaysPortraitMode(Z)V

    .line 413
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_0

    .line 415
    const/16 v18, 0x0

    .line 458
    :goto_0
    monitor-exit p0

    return-object v18

    .line 417
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v2, 0x4b

    move/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v19

    .line 419
    .local v19, "rectDefaultImage":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->width()I

    move-result v15

    .line 420
    .local v15, "defaultWidth":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->height()I

    move-result v14

    .line 422
    .local v14, "defaultHeight":I
    if-ge v15, v14, :cond_1

    move/from16 v23, v15

    .line 423
    .local v23, "shortSize":I
    :goto_1
    move/from16 v0, p2

    int-to-double v1, v0

    move/from16 v0, v23

    int-to-double v3, v0

    div-double v21, v1, v3

    .line 425
    .local v21, "scaleRate":D
    const-wide v1, 0x4052c00000000000L    # 75.0

    mul-double v1, v1, v21

    double-to-int v1, v1

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    .line 429
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    move/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v20

    .line 431
    .local v20, "rectImage":Landroid/graphics/Rect;
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v6

    .line 432
    .local v6, "width":I
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    .line 435
    .local v7, "height":I
    const/16 v18, 0x0

    .line 437
    .local v18, "imageBitmap":Landroid/graphics/Bitmap;
    mul-int v1, v6, v7

    :try_start_2
    new-array v8, v1, [I

    .line 438
    .local v8, "arrImageBuffer":[I
    const/16 v17, 0x0

    .line 439
    .local v17, "errorCode":I
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 441
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v2, p1

    invoke-virtual/range {v1 .. v8}, Lcom/k2mobile/k2lib;->createPageImageBlocking(IIIIII[I)I

    move-result v17

    .line 439
    monitor-exit p0

    .line 444
    if-eqz v17, :cond_2

    .line 446
    const/4 v8, 0x0

    .line 447
    const/16 v18, 0x0

    goto :goto_0

    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v8    # "arrImageBuffer":[I
    .end local v17    # "errorCode":I
    .end local v18    # "imageBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "rectImage":Landroid/graphics/Rect;
    .end local v21    # "scaleRate":D
    .end local v23    # "shortSize":I
    :cond_1
    move/from16 v23, v14

    .line 422
    goto :goto_1

    .line 439
    .restart local v6    # "width":I
    .restart local v7    # "height":I
    .restart local v8    # "arrImageBuffer":[I
    .restart local v17    # "errorCode":I
    .restart local v18    # "imageBitmap":Landroid/graphics/Bitmap;
    .restart local v20    # "rectImage":Landroid/graphics/Rect;
    .restart local v21    # "scaleRate":D
    .restart local v23    # "shortSize":I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 452
    .end local v8    # "arrImageBuffer":[I
    .end local v17    # "errorCode":I
    :catch_0
    move-exception v16

    .line 453
    .local v16, "e":Ljava/lang/OutOfMemoryError;
    :try_start_5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 454
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 455
    const/16 v18, 0x0

    goto :goto_0

    .line 450
    .end local v16    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v8    # "arrImageBuffer":[I
    .restart local v17    # "errorCode":I
    :cond_2
    const/4 v9, 0x0

    :try_start_6
    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move v10, v6

    move v11, v6

    move v12, v7

    invoke-static/range {v8 .. v13}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v18

    .line 451
    goto :goto_0

    .line 412
    .end local v6    # "width":I
    .end local v7    # "height":I
    .end local v8    # "arrImageBuffer":[I
    .end local v14    # "defaultHeight":I
    .end local v15    # "defaultWidth":I
    .end local v17    # "errorCode":I
    .end local v18    # "imageBitmap":Landroid/graphics/Bitmap;
    .end local v19    # "rectDefaultImage":Landroid/graphics/Rect;
    .end local v20    # "rectImage":Landroid/graphics/Rect;
    .end local v21    # "scaleRate":D
    .end local v23    # "shortSize":I
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getCurrentZoom()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    return v0
.end method

.method public getK2Handle()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->getViewerHandle()I

    move-result v0

    return v0
.end method

.method public getPageImageBitmap(III)Landroid/graphics/Bitmap;
    .locals 26
    .param p1, "pageNum"    # I
    .param p2, "maxWidth"    # I
    .param p3, "maxHeight"    # I

    .prologue
    .line 200
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    if-nez v5, :cond_0

    .line 202
    const/16 v22, 0x0

    .line 247
    :goto_0
    return-object v22

    .line 204
    :cond_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    if-nez v5, :cond_1

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v6, 0x4b

    move/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v23

    .line 207
    .local v23, "rectImage":Landroid/graphics/Rect;
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->width()I

    move-result v19

    .line 208
    .local v19, "defaultWidth":I
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->height()I

    move-result v18

    .line 209
    .local v18, "defaultHeight":I
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, v19

    move/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->GetScaleRate(IIII)D

    move-result-wide v24

    .line 210
    .local v24, "scaleRate":D
    const-wide v5, 0x4052c00000000000L    # 75.0

    mul-double v5, v5, v24

    double-to-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    .line 216
    .end local v18    # "defaultHeight":I
    .end local v19    # "defaultWidth":I
    .end local v23    # "rectImage":Landroid/graphics/Rect;
    .end local v24    # "scaleRate":D
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    move/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v23

    .line 217
    .restart local v23    # "rectImage":Landroid/graphics/Rect;
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->width()I

    move-result v10

    .line 218
    .local v10, "width":I
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Rect;->height()I

    move-result v11

    .line 220
    .local v11, "height":I
    const/16 v22, 0x0

    .line 222
    .local v22, "imageBitmap":Landroid/graphics/Bitmap;
    mul-int v5, v10, v11

    :try_start_0
    new-array v12, v5, [I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    .local v12, "arrImageBuffer":[I
    const/16 v21, 0x0

    .line 226
    .local v21, "errorCode":I
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->stopFileOpen()V

    .line 228
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v6, p1

    invoke-virtual/range {v5 .. v12}, Lcom/k2mobile/k2lib;->createPageImageBlocking(IIIIII[I)I

    move-result v21

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->resumeFileOpen()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 233
    if-eqz v21, :cond_2

    .line 235
    const/4 v12, 0x0

    .line 236
    const/16 v22, 0x0

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v5

    .line 231
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 232
    throw v5
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 241
    .end local v12    # "arrImageBuffer":[I
    .end local v21    # "errorCode":I
    :catch_0
    move-exception v20

    .line 242
    .local v20, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 243
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 244
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 239
    .end local v20    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v12    # "arrImageBuffer":[I
    .restart local v21    # "errorCode":I
    :cond_2
    const/4 v13, 0x0

    :try_start_3
    sget-object v17, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move v14, v10

    move v15, v10

    move/from16 v16, v11

    invoke-static/range {v12 .. v17}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v22

    .line 240
    goto/16 :goto_0
.end method

.method public getPageImageSize(I)Landroid/graphics/Rect;
    .locals 2
    .param p1, "pageNum"    # I

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    iget v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mZoom:I

    invoke-virtual {v0, p1, v1}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public getPortrait(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0, p1}, Lcom/k2mobile/k2lib;->isPortrait(I)I

    move-result v0

    return v0
.end method

.method public getTotalPageCount()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->pageCount()I

    move-result v0

    return v0
.end method

.method public isFileLoadingFinished()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    return v0
.end method

.method public isLandscapePage(I)Z
    .locals 7
    .param p1, "pageNum"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 259
    const/4 v0, 0x1

    .line 260
    .local v0, "isLandscapePage":Z
    iget-boolean v5, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    if-nez v5, :cond_0

    .line 275
    :goto_0
    return v4

    .line 264
    :cond_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v6, 0x4b

    invoke-virtual {v5, p1, v6}, Lcom/k2mobile/k2lib;->getPageImageSize(II)Landroid/graphics/Rect;

    move-result-object v1

    .line 266
    .local v1, "rectDefaultImage":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v5, p1}, Lcom/k2mobile/k2lib;->isPortrait(I)I

    move-result v2

    .line 267
    .local v2, "value":I
    if-ne v2, v3, :cond_1

    .line 268
    const/4 v0, 0x0

    :goto_1
    move v4, v0

    .line 275
    goto :goto_0

    .line 269
    :cond_1
    if-nez v2, :cond_2

    .line 270
    const/4 v0, 0x1

    .line 271
    goto :goto_1

    .line 272
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-le v5, v6, :cond_3

    move v0, v3

    :goto_2
    goto :goto_1

    :cond_3
    move v0, v4

    goto :goto_2
.end method

.method public isOpenedFile()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    return v0
.end method

.method public isPDFDocument()Z
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->fileType()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPortrait(I)I
    .locals 1
    .param p1, "page_index"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0, p1}, Lcom/k2mobile/k2lib;->isPortrait(I)I

    move-result v0

    return v0
.end method

.method public onCreateFinish(IZ)V
    .locals 0
    .param p1, "finishPageIndex"    # I
    .param p2, "hasUndrawedImages"    # Z

    .prologue
    .line 684
    return-void
.end method

.method public onErrorFile(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 631
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc

    if-ne p1, v0, :cond_2

    .line 632
    :cond_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyMemoryOverflow()V

    .line 636
    :cond_1
    :goto_0
    return-void

    .line 634
    :cond_2
    const/16 v0, 0x400

    if-eq p1, v0, :cond_1

    .line 635
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyCannotOpenFile()V

    goto :goto_0
.end method

.method public onErrorImage(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 691
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc

    if-ne p1, v0, :cond_1

    .line 692
    :cond_0
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyMemoryOverflow()V

    .line 695
    :goto_0
    return-void

    .line 694
    :cond_1
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyCannotOpenFile()V

    goto :goto_0
.end method

.method public onLoading(IZ)V
    .locals 1
    .param p1, "readingPercent"    # I
    .param p2, "readingFinished"    # Z

    .prologue
    .line 642
    if-eqz p2, :cond_0

    .line 644
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    .line 645
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyFileLoadingFinished()V

    .line 647
    :cond_0
    return-void
.end method

.method public onLoadingPage(I)V
    .locals 4
    .param p1, "readingPageCount"    # I

    .prologue
    const/16 v2, 0x64

    .line 655
    if-le p1, v2, :cond_0

    sget-boolean v1, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isSkip:Z

    if-eqz v1, :cond_0

    .line 668
    :goto_0
    return-void

    .line 657
    :cond_0
    if-le p1, v2, :cond_1

    .line 658
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isSkip:Z

    .line 659
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 660
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer$1;-><init>(Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;I)V

    .line 665
    const-wide/16 v2, 0x5dc

    .line 660
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 667
    .end local v0    # "timer":Ljava/util/Timer;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyLoadedPage(I)V

    goto :goto_0
.end method

.method public onPassword()V
    .locals 0

    .prologue
    .line 615
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyPDFInputPassword()V

    .line 616
    return-void
.end method

.method public onUpdateable(I)V
    .locals 0
    .param p1, "updateablePageIndex"    # I

    .prologue
    .line 675
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->doNotifyImageReload(I)V

    .line 676
    return-void
.end method

.method public registerObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V
    .locals 1
    .param p1, "o"    # Lcom/sec/mobileprint/core/k2/K2Observer;

    .prologue
    .line 763
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/mobileprint/core/k2/K2Observer;)V
    .locals 1
    .param p1, "o"    # Lcom/sec/mobileprint/core/k2/K2Observer;

    .prologue
    .line 772
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 776
    :cond_0
    return-void
.end method

.method public resumeFileOpen()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    if-eqz v0, :cond_0

    .line 133
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->continueLoading()I

    .line 138
    :cond_0
    return-void
.end method

.method public savePageImageData(ILjava/lang/String;)Z
    .locals 3
    .param p1, "pageNum"    # I
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 491
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 492
    .local v0, "tempFolderPath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 493
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 495
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 498
    const/4 v0, 0x0

    .line 499
    const/4 v1, 0x0

    .line 505
    :goto_0
    return v1

    .line 503
    :cond_0
    const/4 v0, 0x0

    .line 505
    iget v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenWidth:I

    iget v2, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mScreenHeight:I

    invoke-virtual {p0, p1, p2, v1, v2}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->savePageImageData(ILjava/lang/String;II)Z

    move-result v1

    goto :goto_0
.end method

.method public savePageImageData(ILjava/lang/String;II)Z
    .locals 6
    .param p1, "pageNum"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I

    .prologue
    const/4 v3, 0x0

    .line 510
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->getPageImageBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 511
    .local v1, "imageBitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 526
    :goto_0
    return v3

    .line 517
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 518
    .local v2, "out":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v1, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    const/4 v3, 0x1

    goto :goto_0

    .line 521
    .end local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 522
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setAlwaysPortraitMode(Z)V
    .locals 1
    .param p1, "alwaysPortrait"    # Z

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0, p1}, Lcom/k2mobile/k2lib;->setPortrait(Z)I

    .line 93
    return-void
.end method

.method public setOpenFile(Z)V
    .locals 0
    .param p1, "isOpened"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->isOpenedFile:Z

    .line 142
    return-void
.end method

.method public setPassword(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    .line 619
    iget-object v1, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v1, p1}, Lcom/k2mobile/k2lib;->setPassword(Ljava/lang/String;)I

    move-result v0

    .line 620
    .local v0, "err":I
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 623
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setPreviewMode100DPI()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v1, 0x4b

    invoke-virtual {v0, v1}, Lcom/k2mobile/k2lib;->setDisplayDpi(I)I

    .line 110
    return-void
.end method

.method public setPrintMode300DPI()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/k2mobile/k2lib;->setDisplayDpi(I)I

    .line 104
    return-void
.end method

.method public stopFileOpen()V
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mFileLoadingFinished:Z

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/mobileprint/core/k2/K2DocumentRenderer;->mK2Library:Lcom/k2mobile/k2lib;

    invoke-virtual {v0}, Lcom/k2mobile/k2lib;->breakLoading()I

    .line 128
    :cond_0
    return-void
.end method
