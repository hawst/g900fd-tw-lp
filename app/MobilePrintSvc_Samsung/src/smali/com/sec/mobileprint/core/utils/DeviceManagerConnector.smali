.class public Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;
.super Ljava/lang/Object;
.source "DeviceManagerConnector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;,
        Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;
    }
.end annotation


# instance fields
.field private deviceManagerConnectorListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;",
            ">;"
        }
    .end annotation
.end field

.field public deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

.field lock:Landroid/net/wifi/WifiManager$MulticastLock;

.field mContext:Landroid/content/Context;

.field serviceConnection:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

.field wifi:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerConnectorListenerList:Ljava/util/ArrayList;

    .line 45
    iput-object p1, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerConnectorListenerList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public InitializeDiscovery()Z
    .locals 5

    .prologue
    .line 51
    const/4 v1, 0x1

    .line 52
    .local v1, "ret":Z
    new-instance v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

    invoke-direct {v2, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;-><init>(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;)V

    iput-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->serviceConnection:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

    .line 53
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.print.mobileprint.plugin.DeviceManagerService"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->serviceConnection:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 57
    return v1
.end method

.method public addDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)V
    .locals 1
    .param p1, "deviceManagerConnectorListener"    # Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerConnectorListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public releaseService()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->serviceConnection:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->serviceConnection:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;

    .line 65
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->lock:Landroid/net/wifi/WifiManager$MulticastLock;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->lock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->release()V

    .line 69
    :cond_0
    return-void
.end method

.method public removeDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)Z
    .locals 1
    .param p1, "deviceManagerConnectorListener"    # Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerConnectorListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
