.class Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;
.super Lcom/sec/print/mobileprint/IPrintStatusCallback$Stub;
.source "SamsungPrintingTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/print/SamsungPrintingTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    .line 305
    invoke-direct {p0}, Lcom/sec/print/mobileprint/IPrintStatusCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public statusChanged(II)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 310
    const-string v0, ""

    .line 311
    .local v0, "typeString":Ljava/lang/String;
    const-string v1, ""

    .line 314
    .local v1, "valueString":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 396
    :goto_0
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mUpdateJobStatusListener:Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    invoke-static {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->access$2(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$1;->this$0:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->mPrintJob:Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-static {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->access$3(Lcom/sec/mobileprint/core/print/SamsungPrintingTask;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v3

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;->onJobStatusUpdated(III)V

    .line 397
    return-void

    .line 317
    :pswitch_0
    const-string v0, "prepare"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 318
    goto :goto_0

    .line 320
    :pswitch_1
    const-string v0, "start job"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Total Page="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 321
    goto :goto_0

    .line 324
    :pswitch_2
    const-string v0, "start page"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Page No="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 327
    goto :goto_0

    .line 329
    :pswitch_3
    const-string v0, "printing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "percentage="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 330
    goto :goto_0

    .line 333
    :pswitch_4
    const-string v0, "completed page"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "completed page No="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 335
    goto :goto_0

    .line 338
    :pswitch_5
    const-string v0, "completed job"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "total Page="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 340
    goto :goto_0

    .line 343
    :pswitch_6
    const-string v0, "canceled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    goto/16 :goto_0

    .line 347
    :pswitch_7
    const-string v0, "error"

    .line 348
    packed-switch p2, :pswitch_data_1

    goto/16 :goto_0

    .line 351
    :pswitch_8
    const-string v1, "Out of memory"

    .line 352
    goto/16 :goto_0

    .line 354
    :pswitch_9
    const-string v1, "printing option error"

    .line 355
    goto/16 :goto_0

    .line 357
    :pswitch_a
    const-string v1, "not supported contents"

    .line 358
    goto/16 :goto_0

    .line 360
    :pswitch_b
    const-string v1, "Output stream error"

    .line 361
    goto/16 :goto_0

    .line 363
    :pswitch_c
    const-string v1, "PrintService STILL_RUNNING"

    .line 364
    goto/16 :goto_0

    .line 366
    :pswitch_d
    const-string v1, "NOT_SUPPORTED_PRINTER"

    .line 367
    goto/16 :goto_0

    .line 369
    :pswitch_e
    const-string v1, "Invalid contents"

    goto/16 :goto_0

    .line 374
    :pswitch_f
    const-string v0, "PRINTING_STATUSTYPE_START_MAKE_FILE_DATA"

    .line 375
    goto/16 :goto_0

    .line 377
    :pswitch_10
    const-string v0, "PRINTING_STATUSTYPE_END_MAKE_FILE_DATA"

    .line 378
    goto/16 :goto_0

    .line 380
    :pswitch_11
    const-string v0, "PRINTING_STATUSTYPE_START_SEND_FILE_DATA"

    .line 381
    goto/16 :goto_0

    .line 383
    :pswitch_12
    const-string v0, "PRINTING_STATUSTYPE_START_SEND_FILE_PROGRESS"

    .line 384
    goto/16 :goto_0

    .line 386
    :pswitch_13
    const-string v0, "PRINTING_STATUSTYPE_END_SEND_FILE_DATA"

    .line 387
    goto/16 :goto_0

    .line 389
    :pswitch_14
    const-string v0, "PRINTING_STATUSTYPE_COMPLETED_FILE_JOB"

    goto/16 :goto_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 348
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
