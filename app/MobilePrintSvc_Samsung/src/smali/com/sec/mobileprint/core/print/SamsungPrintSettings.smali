.class public Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
.super Ljava/lang/Object;
.source "SamsungPrintSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;,
        Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;,
        Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;
    }
.end annotation


# static fields
.field public static PRINT_CONTENT_TYPE_DOCUMENT:I

.field public static PRINT_CONTENT_TYPE_NOT_SET:I

.field public static PRINT_CONTENT_TYPE_PHOTO:I


# instance fields
.field private mAppContentType:I

.field private mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

.field private mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

.field private mContentType:I

.field private mCopies:I

.field private mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

.field private mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

.field private mJobLabel:Ljava/lang/String;

.field private mMediaType:Ljava/lang/String;

.field private mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

.field private mOutputFileName:Ljava/lang/String;

.field private mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

.field private mPrintToFile:Z

.field private mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

.field private mSelectedMediaSize:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_NOT_SET:I

    .line 28
    const/4 v0, 0x1

    sput v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    .line 29
    const/4 v0, 0x2

    sput v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    const-string v0, "Letter"

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSelectedMediaSize:Ljava/lang/String;

    .line 102
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_ONE_SIDE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    .line 103
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .line 104
    iput v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mCopies:I

    .line 105
    sget v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    iput v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    .line 106
    const-string v0, "Normal"

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mMediaType:Ljava/lang/String;

    .line 107
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setPrintToFile(Z)V

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->PRINT_TO_FILE_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".prn"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    .line 110
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    .line 111
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    .line 112
    sget v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_NOT_SET:I

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setAppContentType(I)V

    .line 113
    new-array v0, v3, [Lcom/sec/mobileprint/core/print/SamsungPageRange;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 114
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    sget-object v1, Lcom/sec/mobileprint/core/print/SamsungPageRange;->ALL_PAGES:Lcom/sec/mobileprint/core/print/SamsungPageRange;

    aput-object v1, v0, v2

    .line 115
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_PORTRAINT:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .line 116
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;I)V
    .locals 3
    .param p1, "copies"    # I
    .param p2, "mediaSize"    # Ljava/lang/String;
    .param p3, "duplex"    # Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;
    .param p4, "chromaticity"    # Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;
    .param p5, "contentType"    # I

    .prologue
    const/4 v2, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mCopies:I

    .line 121
    iput-object p4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .line 122
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    .line 123
    iput p5, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    .line 124
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSelectedMediaSize:Ljava/lang/String;

    .line 125
    const-string v0, "Normal"

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mMediaType:Ljava/lang/String;

    .line 126
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setPrintToFile(Z)V

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/k2/CoreConstants;->PRINT_TO_FILE_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".prn"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    .line 129
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    .line 130
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    .line 131
    sget v0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_NOT_SET:I

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setAppContentType(I)V

    .line 132
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/mobileprint/core/print/SamsungPageRange;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 133
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    sget-object v1, Lcom/sec/mobileprint/core/print/SamsungPageRange;->ALL_PAGES:Lcom/sec/mobileprint/core/print/SamsungPageRange;

    aput-object v1, v0, v2

    .line 134
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_PORTRAINT:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .line 137
    return-void
.end method

.method private getMedia()Lcom/sec/print/mobileprint/printoptionattribute/Media;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 178
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v9, "deviceMediaSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSelectedMediaSize:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-static {v9}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->getSPSMediaList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v10

    .line 181
    .local v10, "mediaSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Media;

    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getMediaName()Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getWidth()I

    move-result v2

    .line 183
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getLength()I

    move-result v3

    .line 184
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mMediaType:Ljava/lang/String;

    .line 185
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getLeftMargin()I

    move-result v5

    .line 186
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v6}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getTopMargin()I

    move-result v6

    .line 187
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getRightMargin()I

    move-result v7

    .line 188
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    invoke-virtual {v8}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getBottoMargin()I

    move-result v8

    .line 181
    invoke-direct/range {v0 .. v8}, Lcom/sec/print/mobileprint/printoptionattribute/Media;-><init>(Ljava/lang/String;IILjava/lang/String;IIII)V

    .line 189
    .local v0, "media":Lcom/sec/print/mobileprint/printoptionattribute/Media;
    return-object v0
.end method


# virtual methods
.method public getAppContentType()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mAppContentType:I

    return v0
.end method

.method public getColor()Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    return-object v0
.end method

.method public getConfidentialInfo()Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    return-object v0
.end method

.method public getContentType()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    return v0
.end method

.method public getCopies()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mCopies:I

    return v0
.end method

.method public getDuplex()Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    return-object v0
.end method

.method public getJobLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSelectedMediaSize:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mMediaType:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientation()Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    return-object v0
.end method

.method public getOutputFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOutputFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getPageRange()[Lcom/sec/mobileprint/core/print/SamsungPageRange;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    return-object v0
.end method

.method public getPrintOptionAttributeSet()Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 142
    new-instance v7, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;

    invoke-direct {v7}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;-><init>()V

    .line 143
    .local v7, "printOptions":Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;

    iget v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mCopies:I

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Copies;-><init>(I)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 144
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 145
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 146
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mAppContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    if-ne v0, v1, :cond_1

    .line 147
    :cond_0
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mAppContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_NOT_SET:I

    if-ne v0, v1, :cond_2

    .line 148
    :cond_1
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Duplex;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Duplex;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 150
    :cond_2
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getMedia()Lcom/sec/print/mobileprint/printoptionattribute/Media;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 152
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mAppContentType:I

    sget v1, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    if-eq v0, v1, :cond_3

    .line 153
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;->BLACKOPTIMIZER_ON:Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 154
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/NUP;-><init>()V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 158
    :cond_3
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mUseJobAccounting:Z
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 159
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccUserName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;

    move-result-object v2

    .line 160
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccPassword:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    .line 161
    :goto_0
    iget-object v4, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->mJobAccGroupPermission:Z
    invoke-static {v4}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;)Z

    move-result v4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 159
    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 164
    :cond_4
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mUseConfidential:Z
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 165
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialUserName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->mConfidentialPassword:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 168
    :cond_5
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mUseSecureRelease:Z
    invoke-static {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->access$0(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 169
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    # getter for: Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->mSecureReleaseUserName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->access$1(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 172
    :cond_6
    return-object v7

    :cond_7
    move v3, v5

    .line 160
    goto :goto_0
.end method

.method public getSecureReleaseInfo()Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    return-object v0
.end method

.method public getmJobAccountingInfo()Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    return-object v0
.end method

.method public isPrintToFile()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPrintToFile:Z

    return v0
.end method

.method public setAppContentType(I)V
    .locals 0
    .param p1, "mAppContentType"    # I

    .prologue
    .line 281
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mAppContentType:I

    .line 282
    return-void
.end method

.method public setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V
    .locals 0
    .param p1, "mColor"    # Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mColor:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .line 263
    return-void
.end method

.method public setConfidentialInfo(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "useConfidential"    # Z
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "pasword"    # Ljava/lang/String;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    invoke-static {v0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    invoke-static {v0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$4(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mConfidential:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;

    invoke-static {v0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;->access$5(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$Confidential;Ljava/lang/String;)V

    .line 315
    return-void
.end method

.method public setContentType(I)V
    .locals 0
    .param p1, "mContentType"    # I

    .prologue
    .line 272
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mContentType:I

    .line 273
    return-void
.end method

.method public setCopies(I)V
    .locals 0
    .param p1, "mCopies"    # I

    .prologue
    .line 290
    iput p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mCopies:I

    .line 291
    return-void
.end method

.method public setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V
    .locals 0
    .param p1, "mDuplex"    # Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mDuplex:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    .line 245
    return-void
.end method

.method public setJobAccounttingInfo(ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "useJobAccounting"    # Z
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "groupPermission"    # Z

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-static {v0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$4(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-static {v0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$5(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-static {v0, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$6(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobAccount:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;

    invoke-static {v0, p4}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;->access$7(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$JobAccount;Z)V

    .line 304
    return-void
.end method

.method public setJobLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "mJobLabel"    # Ljava/lang/String;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mJobLabel:Ljava/lang/String;

    .line 198
    return-void
.end method

.method public setMediaSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSelectedMediaSize"    # Ljava/lang/String;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSelectedMediaSize:Ljava/lang/String;

    .line 225
    return-void
.end method

.method public setMediaType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMediaType"    # Ljava/lang/String;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mMediaType:Ljava/lang/String;

    .line 235
    return-void
.end method

.method public setOrientation(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V
    .locals 0
    .param p1, "mOrientation"    # Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOrientation:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .line 254
    return-void
.end method

.method public setOutputFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "mOutputFileName"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mOutputFileName:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public setPageRange([Lcom/sec/mobileprint/core/print/SamsungPageRange;)V
    .locals 0
    .param p1, "mPageRange"    # [Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPageRange:[Lcom/sec/mobileprint/core/print/SamsungPageRange;

    .line 336
    return-void
.end method

.method public setPrintToFile(Z)V
    .locals 0
    .param p1, "mPrintToFile"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mPrintToFile:Z

    .line 206
    return-void
.end method

.method public setSecureReleaseInfo(ZLjava/lang/String;)V
    .locals 1
    .param p1, "useSecureRelease"    # Z
    .param p2, "SecureReleaseUserName"    # Ljava/lang/String;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    invoke-static {v0, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->access$2(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;Z)V

    .line 325
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->mSecureRelease:Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;

    invoke-static {v0, p2}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;->access$3(Lcom/sec/mobileprint/core/print/SamsungPrintSettings$SecureRelease;Ljava/lang/String;)V

    .line 326
    return-void
.end method
