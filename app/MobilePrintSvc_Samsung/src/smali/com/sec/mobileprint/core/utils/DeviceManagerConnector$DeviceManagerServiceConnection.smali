.class Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;
.super Ljava/lang/Object;
.source "DeviceManagerConnector.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceManagerServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    .line 78
    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-static {p2}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    .line 80
    iget-object v3, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    iput-object v2, v3, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->wifi:Landroid/net/wifi/WifiManager;

    .line 81
    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v2, v2, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->wifi:Landroid/net/wifi/WifiManager;

    const-string v3, "fliing_lock"

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiManager;->createMulticastLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v1

    .line 82
    .local v1, "lock":Landroid/net/wifi/WifiManager$MulticastLock;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager$MulticastLock;->setReferenceCounted(Z)V

    .line 83
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$MulticastLock;->acquire()V

    .line 84
    iget-object v2, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    # getter for: Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerConnectorListenerList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->access$0(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    return-void

    .line 84
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;

    .line 85
    .local v0, "listener":Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;
    invoke-interface {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;->onDeviceManagerConnectorStarted()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$DeviceManagerServiceConnection;->this$0:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    .line 92
    return-void
.end method
