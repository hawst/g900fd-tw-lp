.class public Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
.super Landroid/os/AsyncTask;
.source "SamsungDeviceDiscovery.java"

# interfaces
.implements Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
        ">;>;",
        "Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;"
    }
.end annotation


# static fields
.field private static mTimeOut:I


# instance fields
.field private _stop:Z

.field private deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

.field private discoveredPrinters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

.field private mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

.field private mMessenger:Landroid/os/Messenger;

.field private reportedPrinters:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private timer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x4e20

    sput v0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mTimeOut:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Messenger;Lcom/sec/mobileprint/core/print/DeviceType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "returnMessenger"    # Landroid/os/Messenger;
    .param p3, "deviceType"    # Lcom/sec/mobileprint/core/print/DeviceType;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 32
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 34
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 35
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    .line 36
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    .line 41
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 63
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 64
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 65
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 66
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 32
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 34
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 35
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    .line 36
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    .line 41
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 45
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 46
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 47
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 48
    sget-object v0, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;Lcom/sec/mobileprint/core/print/DeviceType;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;
    .param p3, "deviceType"    # Lcom/sec/mobileprint/core/print/DeviceType;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 32
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 34
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 35
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    .line 36
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    .line 41
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 54
    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    .line 55
    iput-object p2, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    .line 56
    iput-object p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    .line 60
    return-void
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;Z)V
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;)Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    return-object v0
.end method

.method private reportPrinters()V
    .locals 10

    .prologue
    .line 185
    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    if-eqz v7, :cond_1

    .line 186
    iget-object v7, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 221
    :cond_1
    return-void

    .line 186
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 187
    .local v0, "devInfo":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 188
    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportedPrinters:Ljava/util/HashSet;

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    if-eqz v8, :cond_3

    .line 191
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 192
    .local v1, "discoveredDevice":Landroid/os/Bundle;
    const-string v8, "device.discovery.device_name"

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v8, "device.discovery.device_address"

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v2, Landroid/content/Intent;

    const-string v8, "org.androidprinting.intent.ACTION_RETURN_DEVICE_RESOLVED"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 195
    .local v2, "discoveryIntent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 196
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 198
    .local v6, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v6}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    .end local v1    # "discoveredDevice":Landroid/os/Bundle;
    .end local v2    # "discoveryIntent":Landroid/content/Intent;
    .end local v6    # "returnmsg":Landroid/os/Message;
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mListener:Lcom/sec/mobileprint/core/print/OnDeviceDiscovery;

    if-eqz v8, :cond_0

    .line 206
    new-instance v4, Landroid/os/Handler;

    sget-object v8, Lcom/sec/mobileprint/core/App;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v4, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 208
    .local v4, "mainHandler":Landroid/os/Handler;
    new-instance v5, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$1;

    invoke-direct {v5, p0, v0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$1;-><init>(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;Lcom/sec/print/mobileprint/dm/DeviceInfo;)V

    .line 215
    .local v5, "myRunnable":Ljava/lang/Runnable;
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 199
    .end local v4    # "mainHandler":Landroid/os/Handler;
    .end local v5    # "myRunnable":Ljava/lang/Runnable;
    .restart local v1    # "discoveredDevice":Landroid/os/Bundle;
    .restart local v2    # "discoveryIntent":Landroid/content/Intent;
    .restart local v6    # "returnmsg":Landroid/os/Message;
    :catch_0
    move-exception v3

    .line 201
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private startDeviceDiscovery()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    .line 84
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;

    .line 85
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;

    invoke-direct {v1, p0, v4}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;-><init>(Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery$StopDiscoveryOnTimeOut;)V

    sget v2, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mTimeOut:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 89
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v4}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 3
    .param p1, "arg0"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    const/4 v0, -0x1

    .line 129
    .local v0, "nStartResult":I
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, v1, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v1}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->stop()I

    .line 146
    :goto_1
    const/4 v1, 0x0

    return-object v1

    .line 130
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, v1, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->start(I)I

    move-result v0

    .line 133
    :cond_1
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    sget-object v2, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    if-ne v1, v2, :cond_3

    .line 134
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, v1, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v1}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getPrinters()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;

    .line 139
    :cond_2
    :goto_2
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->publishProgress([Ljava/lang/Object;)V

    .line 140
    const-wide/16 v1, 0x1f4

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    .line 143
    :catch_0
    move-exception v1

    goto :goto_1

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mDeviceType:Lcom/sec/mobileprint/core/print/DeviceType;

    sget-object v2, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_SCANNER:Lcom/sec/mobileprint/core/print/DeviceType;

    if-ne v1, v2, :cond_2

    .line 137
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, v1, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->deviceManagerService:Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    invoke-interface {v1}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;->getScanners()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->discoveredPrinters:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public onDeviceManagerConnectorStarted()V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceDiscovery()V

    .line 180
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->removeDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)Z

    .line 181
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 169
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->releaseService()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 173
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 162
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 0
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 154
    invoke-direct {p0}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->reportPrinters()V

    .line 155
    return-void
.end method

.method public startDeviceManagerConnector()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    .line 75
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->InitializeDiscovery()Z

    .line 76
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->deviceManagerConnector:Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/core/utils/DeviceManagerConnector;->addDiscoveryServiceListener(Lcom/sec/mobileprint/core/utils/DeviceManagerConnector$IDeviceManagerConnectorListener;)V

    .line 77
    return-void
.end method

.method public stopDeviceDiscovery()V
    .locals 2

    .prologue
    .line 96
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->_stop:Z

    .line 99
    :try_start_0
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
