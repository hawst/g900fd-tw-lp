.class public Lcom/sec/mobileprint/core/print/SamsungJobManager;
.super Ljava/lang/Object;
.source "SamsungJobManager.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# static fields
.field private static final JOBMANAGER_STATUS_IDLE:I = 0x0

.field private static final JOBMANAGER_STATUS_RUNNING:I = 0x1


# instance fields
.field private mJobmanagerStatus:I

.field private mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mPrintJobList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/core/print/SamsungPrintJob;",
            ">;"
        }
    .end annotation
.end field

.field private mPrintingTask:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mJobmanagerStatus:I

    .line 25
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 26
    return-void
.end method

.method private declared-synchronized deletePrintJob(I)V
    .locals 4
    .param p1, "jobId"    # I

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getJobIndexByJobId(I)I

    move-result v1

    .line 125
    .local v1, "index":I
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getJobIndexByJobId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    .end local v1    # "index":I
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private getJobIndexByJobId(I)I
    .locals 3
    .param p1, "jobId"    # I

    .prologue
    .line 134
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 141
    const/4 v0, -0x1

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 136
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .line 137
    .local v1, "printJob":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-virtual {v1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private requestPrint(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V
    .locals 1
    .param p1, "printJob"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .prologue
    .line 64
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mJobmanagerStatus:I

    .line 66
    new-instance v0, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    invoke-direct {v0, p1, p0}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;-><init>(Lcom/sec/mobileprint/core/print/SamsungPrintJob;Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IJobCompletedListener;)V

    iput-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintingTask:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    .line 67
    return-void
.end method


# virtual methods
.method public addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I
    .locals 1
    .param p1, "printJob"    # Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getNewJobId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobId(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mJobmanagerStatus:I

    if-nez v0, :cond_0

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->requestPrint(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    .line 60
    :cond_0
    invoke-virtual {p1}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v0

    return v0
.end method

.method public addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public cancelPrint(I)V
    .locals 9
    .param p1, "jobId"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x6

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getJobIndexByJobId(I)I

    move-result v1

    .line 73
    .local v1, "index":I
    const/4 v3, 0x0

    .line 75
    .local v3, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    :try_start_0
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->getStatusType()I

    move-result v5

    const/16 v6, 0x64

    if-eq v5, v6, :cond_1

    .line 82
    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->getStatusType()I

    move-result v5

    const/4 v6, 0x5

    if-eq v5, v6, :cond_1

    .line 83
    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->getStatusType()I

    move-result v5

    const/4 v6, 0x7

    if-eq v5, v6, :cond_1

    .line 84
    invoke-virtual {v3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->getStatusType()I

    move-result v5

    if-eq v5, v7, :cond_1

    .line 86
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintingTask:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    if-eqz v5, :cond_0

    .line 87
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintingTask:Lcom/sec/mobileprint/core/print/SamsungPrintingTask;

    invoke-virtual {v5, p1}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask;->cancelPrintJob(I)V

    .line 102
    :cond_0
    :goto_1
    return-void

    .line 90
    :cond_1
    if-eqz v3, :cond_0

    .line 91
    invoke-virtual {v3, v7, v8}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobStatus(II)V

    .line 93
    iget-object v5, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;>;"
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 97
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->deletePrintJob(I)V

    goto :goto_1

    .line 94
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .line 95
    .local v4, "listener":Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    invoke-interface {v4, p1, v7, v8}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;->onJobStatusUpdated(III)V

    goto :goto_2

    .line 77
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;>;"
    .end local v4    # "listener":Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public getAllJobs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/core/print/SamsungPrintJob;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    return-object v0
.end method

.method getNewJobId()I
    .locals 3

    .prologue
    .line 146
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .line 148
    .local v0, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 150
    .end local v0    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 3
    .param p1, "jobID"    # I

    .prologue
    .line 34
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 39
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 34
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .line 35
    .local v0, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v2

    if-ne v2, p1, :cond_0

    goto :goto_0
.end method

.method public onJobCompleted(I)V
    .locals 4
    .param p1, "jobId"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->deletePrintJob(I)V

    .line 110
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mJobmanagerStatus:I

    .line 111
    const-string v1, "Samsung Job Manager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Print Job completed with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v1, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 119
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    .line 113
    .local v0, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobStatus()Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/mobileprint/core/print/SamsungPrintJob$JobStatus;->getStatusType()I

    move-result v2

    const/16 v3, 0x64

    if-ne v2, v3, :cond_0

    .line 114
    invoke-direct {p0, v0}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->requestPrint(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)V

    .line 115
    const-string v1, "Samsung Job Manager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestPrint called for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getJobId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onJobStatusUpdated(III)V
    .locals 6
    .param p1, "jobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getJobIndexByJobId(I)I

    move-result v0

    .line 158
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 159
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 160
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mPrintJobList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    invoke-virtual {v3, p2, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->setJobStatus(II)V

    .line 164
    :cond_0
    const-string v3, "Samsung Job Manager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "JobId = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Value = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v3, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 171
    return-void

    .line 168
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .line 169
    .local v2, "listener":Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;
    invoke-interface {v2, p1, p2, p3}, Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;->onJobStatusUpdated(III)V

    goto :goto_0
.end method

.method public removeUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;

    .prologue
    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/sec/mobileprint/core/print/SamsungJobManager;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    goto :goto_0
.end method
