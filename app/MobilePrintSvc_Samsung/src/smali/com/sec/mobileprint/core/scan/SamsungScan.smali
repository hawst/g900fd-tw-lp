.class public Lcom/sec/mobileprint/core/scan/SamsungScan;
.super Landroid/os/AsyncTask;
.source "SamsungScan.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final MYDOCUMENT_FILE_PREFIX:Ljava/lang/String; = "SAM_IMG_"

.field private static final SCAN_FILE_PATH:Ljava/lang/String;

.field public static final SCAN_PROGRESS_END_PAGE:I = 0x2

.field public static final SCAN_PROGRESS_END_SAVE_FILE:I = 0x4

.field public static final SCAN_PROGRESS_START_PAGE:I = 0x1

.field public static final SCAN_PROGRESS_START_SAVE_FILE:I = 0x3

.field private static final SDCARD_ROOT_PATH:Ljava/lang/String;


# instance fields
.field private mIPAddress:Ljava/lang/String;

.field private mIsCanceled:Z

.field private mMessage:Landroid/os/Message;

.field private mMessenger:Landroid/os/Messenger;

.field mPaperSize:I

.field mPassword:Ljava/lang/String;

.field mPreviewScale:I

.field private mResult:Landroid/os/Bundle;

.field mScanColor:I

.field mScanFileName:Ljava/lang/String;

.field mScanFileType:I

.field mScanFolder:Ljava/lang/String;

.field mScanHeight:I

.field mScanQuality:I

.field private mScanSettings:Landroid/os/Bundle;

.field mScanSource:I

.field mScanWidth:I

.field mScannedFileNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceType:I

.field mUserName:Ljava/lang/String;

.field mXStartPoint:I

.field mYStartPoint:I

.field private scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->SDCARD_ROOT_PATH:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/mobileprint/core/scan/SamsungScan;->SDCARD_ROOT_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/SamsungPrintServicePlugin/Scan/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->SCAN_FILE_PATH:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/os/Message;I)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;
    .param p2, "serviceType"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    .line 36
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessage:Landroid/os/Message;

    .line 37
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIPAddress:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    .line 40
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    .line 42
    new-instance v0, Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/sf/ScannerLib;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    .line 44
    iput-boolean v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIsCanceled:Z

    .line 47
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mXStartPoint:I

    .line 48
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mYStartPoint:I

    .line 49
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanWidth:I

    .line 50
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanHeight:I

    .line 51
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    .line 52
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    .line 53
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    .line 54
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    .line 56
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    .line 57
    iput-object v2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    .line 58
    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mUserName:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPassword:Ljava/lang/String;

    .line 67
    iput-object p1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessage:Landroid/os/Message;

    .line 68
    if-eqz p1, :cond_1

    .line 69
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    .line 70
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    .line 71
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v1, "scanner-address"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIPAddress:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v1, "scan_image_folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 74
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->SCAN_FILE_PATH:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    .line 81
    :cond_1
    :goto_0
    iput p2, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mServiceType:I

    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    .line 83
    return-void

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public cancelScan()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/sf/ScannerLib;->asyncCancel()I

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIsCanceled:Z

    .line 96
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 39
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getCapabilities(Ljava/lang/String;)[I

    move-result-object v20

    .line 103
    .local v20, "caps":[I
    const/4 v2, 0x0

    aget v2, v20, v2

    if-eqz v2, :cond_0

    .line 104
    const/4 v2, 0x0

    aget v2, v20, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 254
    :goto_0
    return-object v2

    .line 106
    :cond_0
    const/16 v24, 0x0

    .line 107
    .local v24, "err":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/scan/SamsungScan;->setAllParameters()I

    move-result v24

    .line 108
    if-eqz v24, :cond_1

    .line 109
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 112
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/print/mobileprint/sf/ScannerLib;->startScanSession(Ljava/lang/String;)I

    move-result v24

    .line 113
    if-eqz v24, :cond_2

    .line 114
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 125
    :cond_2
    const/4 v2, 0x6

    aget v2, v20, v2

    if-nez v2, :cond_3

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    .line 128
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mXStartPoint:I

    .line 129
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mYStartPoint:I

    .line 130
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanWidth:I

    .line 131
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanHeight:I

    .line 132
    const/4 v7, 0x0

    .line 133
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    .line 134
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    .line 135
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    .line 136
    const/4 v11, 0x1

    .line 137
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    .line 138
    const/4 v13, 0x1

    .line 127
    invoke-virtual/range {v2 .. v13}, Lcom/sec/print/mobileprint/sf/ScannerLib;->setParameters(IIIIIIIIILjava/lang/String;I)I

    move-result v24

    .line 158
    :goto_1
    if-eqz v24, :cond_4

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    .line 160
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 141
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    .line 142
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mXStartPoint:I

    .line 143
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mYStartPoint:I

    .line 144
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanWidth:I

    .line 145
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanHeight:I

    .line 146
    const/4 v7, 0x0

    .line 147
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    .line 148
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    .line 149
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    .line 150
    const/4 v11, 0x1

    .line 151
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    .line 152
    const/4 v13, 0x1

    .line 153
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mUserName:Ljava/lang/String;

    .line 154
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPassword:Ljava/lang/String;

    .line 155
    const/16 v16, 0x1

    .line 156
    const/16 v17, 0x0

    .line 141
    invoke-virtual/range {v2 .. v17}, Lcom/sec/print/mobileprint/sf/ScannerLib;->setParameters2(IIIIIIIIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;II)I

    move-result v24

    goto :goto_1

    .line 163
    :cond_4
    const/16 v19, 0x0

    .line 164
    .local v19, "blockNumber":I
    const/16 v21, 0x0

    .line 165
    .local v21, "defBlockConvWidth":I
    const/16 v22, 0x0

    .line 168
    .local v22, "defImageConvHeight":I
    const/16 v34, 0x0

    .line 169
    .local v34, "pageNumber":I
    new-instance v35, Ljava/util/ArrayList;

    invoke-direct/range {v35 .. v35}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v35, "scannedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScannedFileNameList:Ljava/util/ArrayList;

    .line 172
    :cond_5
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, v34, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->publishProgress([Ljava/lang/Object;)V

    .line 175
    :cond_6
    add-int/lit8 v19, v19, 0x1

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->readImageBlock()[I

    move-result-object v18

    .line 177
    .local v18, "a":[I
    if-nez v18, :cond_7

    .line 178
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 180
    :cond_7
    const/4 v2, 0x0

    aget v24, v18, v2

    .line 181
    if-eqz v24, :cond_8

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    .line 183
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 185
    :cond_8
    const/4 v2, 0x5

    aget v21, v18, v2

    .line 186
    const/4 v2, 0x7

    aget v22, v18, v2

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->isLastBlock()[I

    move-result-object v31

    .line 189
    .local v31, "latstBlockArray":[I
    const/4 v2, 0x0

    aget v24, v31, v2

    .line 190
    if-eqz v24, :cond_9

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    .line 192
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 194
    :cond_9
    const/4 v2, 0x1

    aget v2, v31, v2

    if-eqz v2, :cond_6

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, v34, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->publishProgress([Ljava/lang/Object;)V

    .line 197
    add-int/lit8 v34, v34, 0x1

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/sec/mobileprint/core/scan/SamsungScan;->setFileName()I

    move-result v24

    .line 199
    if-eqz v24, :cond_a

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    .line 201
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 203
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/print/mobileprint/sf/ScannerLib;->nextPage(Ljava/lang/String;)[I

    move-result-object v33

    .line 204
    .local v33, "pageArray":[I
    const/4 v2, 0x0

    aget v24, v33, v2

    .line 205
    if-eqz v24, :cond_b

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    .line 207
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 210
    :cond_b
    const/4 v2, 0x1

    aget v2, v33, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v24

    .line 213
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mIsCanceled:Z

    if-nez v2, :cond_d

    .line 214
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->publishProgress([Ljava/lang/Object;)V

    .line 218
    :try_start_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    .line 219
    const-string v25, ".PNG"

    .line 220
    .local v25, "ext":Ljava/lang/String;
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_e

    .line 252
    .end local v25    # "ext":Ljava/lang/String;
    :cond_c
    :goto_3
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->publishProgress([Ljava/lang/Object;)V

    .line 254
    :cond_d
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 220
    .restart local v25    # "ext":Ljava/lang/String;
    :cond_e
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 221
    .local v26, "file":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 222
    .local v32, "outputFile":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    move-object/from16 v0, v26

    move-object/from16 v1, v32

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 223
    new-instance v27, Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 224
    .local v27, "forDeleteFile":Ljava/io/File;
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->delete()Z

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScannedFileNameList:Ljava/util/ArrayList;

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 249
    .end local v25    # "ext":Ljava/lang/String;
    .end local v26    # "file":Ljava/lang/String;
    .end local v27    # "forDeleteFile":Ljava/io/File;
    .end local v32    # "outputFile":Ljava/lang/String;
    :catch_0
    move-exception v23

    .line 250
    .local v23, "e":Ljava/lang/Exception;
    const/16 v24, -0x3

    goto :goto_3

    .line 227
    .end local v23    # "e":Ljava/lang/Exception;
    :cond_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_10

    .line 228
    const-string v25, ".pdf"

    .line 229
    .restart local v25    # "ext":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/mobileprint/core/scan/scandefs;->getPaperWidth(IZ)D

    move-result-wide v37

    .line 230
    .local v37, "width":D
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/mobileprint/core/scan/scandefs;->getPaperHeight(IZ)D

    move-result-wide v29

    .line 231
    .local v29, "height":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 232
    new-instance v5, Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".pdf"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 233
    const-wide/high16 v5, 0x4052000000000000L    # 72.0

    mul-double v5, v5, v37

    double-to-float v5, v5

    .line 234
    const-wide/high16 v6, 0x4052000000000000L    # 72.0

    mul-double v6, v6, v29

    double-to-float v6, v6

    .line 235
    const/high16 v7, 0x41900000    # 18.0f

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 231
    invoke-virtual/range {v2 .. v9}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImagesToPDF([Ljava/lang/String;Ljava/lang/String;FFFII)I

    .line 236
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScannedFileNameList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".pdf"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 238
    .restart local v26    # "file":Ljava/lang/String;
    new-instance v27, Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .restart local v27    # "forDeleteFile":Ljava/io/File;
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->delete()Z

    goto :goto_4

    .line 241
    .end local v25    # "ext":Ljava/lang/String;
    .end local v26    # "file":Ljava/lang/String;
    .end local v27    # "forDeleteFile":Ljava/io/File;
    .end local v29    # "height":D
    .end local v37    # "width":D
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_c

    .line 242
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 243
    .restart local v26    # "file":Ljava/lang/String;
    new-instance v28, Ljava/io/File;

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 244
    .local v28, "forRename":Ljava/io/File;
    new-instance v36, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ".JPG"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .local v36, "toRename":Ljava/io/File;
    move-object/from16 v0, v28

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 246
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScannedFileNameList:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".JPG"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/scan/SamsungScan;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 296
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 297
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 298
    .local v1, "returnIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 299
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_1

    .line 301
    const-string v3, "org.androidprinting.intent.ACTION_RETURN_SCAN_RESULT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "file_path_list"

    iget-object v5, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScannedFileNameList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 379
    :goto_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 380
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 382
    .local v2, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 383
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    .end local v2    # "returnmsg":Landroid/os/Message;
    :cond_0
    :goto_1
    return-void

    .line 306
    :cond_1
    const-string v3, "org.androidprinting.intent.ACTION_RETURN_SCAN_ERROR"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 373
    :pswitch_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kUnknownError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :pswitch_1
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kOpenError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :pswitch_2
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kCloseError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :pswitch_3
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kNoGetCapabilities"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :pswitch_4
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kSessionOpenError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    :pswitch_5
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kNoMemory"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 325
    :pswitch_6
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kNoExternalStorage"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :pswitch_7
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kNoWritableExternalStorage"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 331
    :pswitch_8
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kUserCancel"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 334
    :pswitch_9
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kUnknownError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 337
    :pswitch_a
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kJam"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 340
    :pswitch_b
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kCoverOpen"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 343
    :pswitch_c
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kLocked"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 346
    :pswitch_d
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kBusy"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 349
    :pswitch_e
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kSecurityError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 352
    :pswitch_f
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kInvalidUser"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 355
    :pswitch_10
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kInvalidePasswd"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 358
    :pswitch_11
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kAuthError"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 361
    :pswitch_12
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kEmptyPassword"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 364
    :pswitch_13
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kNoPermission"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 367
    :pswitch_14
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kExceedQuota"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 370
    :pswitch_15
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "scan-error"

    const-string v5, "kTimeOut"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 385
    .restart local v2    # "returnmsg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 308
    :pswitch_data_0
    .packed-switch -0x13
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/scan/SamsungScan;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 8
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 260
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 261
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 262
    .local v1, "returnIntent":Landroid/content/Intent;
    const-string v3, "org.androidprinting.intent.ACTION_RETURN_SCAN_PROGRESS"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    aget-object v3, p1, v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 280
    :goto_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 281
    const/4 v3, 0x0

    invoke-static {v3, v6, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 283
    .local v2, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 284
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :cond_0
    :goto_1
    return-void

    .line 265
    .end local v2    # "returnmsg":Landroid/os/Message;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_type"

    const-string v5, "progress_start_page"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_start_page_no"

    aget-object v5, p1, v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :pswitch_1
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_type"

    const-string v5, "progress_end_page"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_end_page_no"

    aget-object v5, p1, v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 273
    :pswitch_2
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_type"

    const-string v5, "progress_start_save_file"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :pswitch_3
    iget-object v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mResult:Landroid/os/Bundle;

    const-string v4, "progress_type"

    const-string v5, "progress_start_save_file"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    .restart local v2    # "returnmsg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/core/scan/SamsungScan;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setAllParameters()I
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 395
    const/4 v1, 0x0

    .line 397
    .local v1, "err":I
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/scan/SamsungScan;->setPaper()V

    .line 399
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "scan_mode"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "color":Ljava/lang/String;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    .line 401
    const-string v6, "Color"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 402
    iput v10, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    .line 411
    :goto_0
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "scan_source"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 412
    .local v5, "source":Ljava/lang/String;
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_7

    .line 413
    const-string v6, "Flatbed"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 414
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    .line 425
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "scan_resolution"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "quality":Ljava/lang/String;
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_a

    .line 427
    const-string v6, "Heigh"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 428
    iput v11, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    .line 441
    :goto_2
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "scan_file_type"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 442
    .local v2, "fileType":Ljava/lang/String;
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_d

    .line 443
    const-string v6, "JPG"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 444
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    .line 457
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "scan_scale"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 458
    .local v4, "scale":Ljava/lang/String;
    if-eqz v4, :cond_11

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_11

    .line 459
    const-string v6, "one"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 460
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    .line 473
    :cond_2
    :goto_4
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "username"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mUserName:Ljava/lang/String;

    .line 474
    iget-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v7, "password"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPassword:Ljava/lang/String;

    .line 476
    invoke-virtual {p0}, Lcom/sec/mobileprint/core/scan/SamsungScan;->setFileName()I

    move-result v1

    .line 478
    return v1

    .line 404
    .end local v2    # "fileType":Ljava/lang/String;
    .end local v3    # "quality":Ljava/lang/String;
    .end local v4    # "scale":Ljava/lang/String;
    .end local v5    # "source":Ljava/lang/String;
    :cond_3
    iput v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    goto/16 :goto_0

    .line 407
    :cond_4
    iput v10, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanColor:I

    goto/16 :goto_0

    .line 415
    .restart local v5    # "source":Ljava/lang/String;
    :cond_5
    const-string v6, "ADF"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 416
    iput v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    goto :goto_1

    .line 417
    :cond_6
    const-string v6, "Auto"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 418
    iput v12, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    goto/16 :goto_1

    .line 421
    :cond_7
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSource:I

    goto/16 :goto_1

    .line 429
    .restart local v3    # "quality":Ljava/lang/String;
    :cond_8
    const-string v6, "Medium"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 430
    iput v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    goto :goto_2

    .line 432
    :cond_9
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    goto :goto_2

    .line 436
    :cond_a
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanQuality:I

    goto :goto_2

    .line 445
    .restart local v2    # "fileType":Ljava/lang/String;
    :cond_b
    const-string v6, "PNG"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 446
    iput v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    goto :goto_3

    .line 447
    :cond_c
    const-string v6, "PDF"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 448
    const/4 v6, 0x6

    iput v6, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    goto :goto_3

    .line 452
    :cond_d
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileType:I

    goto :goto_3

    .line 461
    .restart local v4    # "scale":Ljava/lang/String;
    :cond_e
    const-string v6, "half"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 462
    iput v9, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    goto :goto_4

    .line 463
    :cond_f
    const-string v6, "quarter"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 464
    iput v11, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    goto :goto_4

    .line 465
    :cond_10
    const-string v6, "eighth"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 466
    iput v12, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    goto/16 :goto_4

    .line 470
    :cond_11
    iput v8, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPreviewScale:I

    goto/16 :goto_4
.end method

.method public setFileName()I
    .locals 24

    .prologue
    .line 494
    const/4 v15, 0x0

    .line 495
    .local v15, "err":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v21

    .line 496
    .local v21, "state":Ljava/lang/String;
    const/16 v17, 0x0

    .line 497
    .local v17, "fileIO":Z
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    .line 498
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 499
    .local v20, "path":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_0

    .line 500
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->mkdirs()Z

    move-result v17

    .line 501
    if-nez v17, :cond_0

    .line 502
    const/16 v15, -0x1c

    .line 564
    .end local v15    # "err":I
    :goto_0
    return v15

    .line 504
    .restart local v15    # "err":I
    :cond_0
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v22, "yyyy"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v12, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 505
    .local v12, "YearFormat":Ljava/text/SimpleDateFormat;
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v22, "MM"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v8, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 506
    .local v8, "MonthFormat":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v22, "dd"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v3, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 508
    .local v3, "DayFormat":Ljava/text/SimpleDateFormat;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v22, "HH"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v5, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 509
    .local v5, "HourFormat":Ljava/text/SimpleDateFormat;
    new-instance v19, Ljava/text/SimpleDateFormat;

    const-string v22, "mm"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 510
    .local v19, "minuteFormat":Ljava/text/SimpleDateFormat;
    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string v22, "ss"

    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v10, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 511
    .local v10, "SecFormat":Ljava/text/SimpleDateFormat;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 512
    .local v13, "YearStr":Ljava/lang/String;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    .line 513
    .local v9, "MonthStr":Ljava/lang/String;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 514
    .local v4, "DayStr":Ljava/lang/String;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 515
    .local v6, "HourStr":Ljava/lang/String;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 516
    .local v7, "MinuteStr":Ljava/lang/String;
    new-instance v22, Ljava/util/Date;

    invoke-direct/range {v22 .. v22}, Ljava/util/Date;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    .line 520
    .local v11, "SecStr":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 521
    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 522
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 523
    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 524
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 525
    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 526
    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 527
    const-string v23, "."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 528
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 529
    const-string v23, "."

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 530
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 520
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    .line 531
    const/4 v14, 0x0

    .line 532
    .local v14, "bUnique":Z
    const/16 v18, 0x0

    .line 534
    .local v18, "i":I
    :cond_1
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFolder:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    .local v16, "file":Ljava/io/File;
    if-eqz v16, :cond_2

    .line 536
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_4

    .line 537
    add-int/lit8 v18, v18, 0x1

    .line 538
    const/16 v22, 0x9

    move/from16 v0, v18

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 539
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "SAM_IMG_"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 540
    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 541
    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 542
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 543
    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 544
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 545
    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 546
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 539
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    .line 562
    :cond_2
    :goto_1
    if-eqz v14, :cond_1

    goto/16 :goto_0

    .line 548
    :cond_3
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "SAM_IMG_"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 549
    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 550
    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 551
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 552
    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 553
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 554
    const-string v23, "_0"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 555
    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 548
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    goto :goto_1

    .line 558
    :cond_4
    const/4 v14, 0x1

    .line 559
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanFileName:Ljava/lang/String;

    goto :goto_1
.end method

.method public setPaper()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 482
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    .line 483
    iget-object v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanSettings:Landroid/os/Bundle;

    const-string v2, "scan_size"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "paper":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    invoke-static {v0}, Lcom/sec/mobileprint/core/scan/scandefs;->getPaperIndexFromName(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    .line 487
    :cond_0
    iput v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mXStartPoint:I

    .line 488
    iput v3, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mYStartPoint:I

    .line 489
    iget v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    invoke-static {v1, v3}, Lcom/sec/mobileprint/core/scan/scandefs;->getPaperWidth(IZ)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanWidth:I

    .line 490
    iget v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mPaperSize:I

    invoke-static {v1, v3}, Lcom/sec/mobileprint/core/scan/scandefs;->getPaperHeight(IZ)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/sec/mobileprint/core/scan/SamsungScan;->mScanHeight:I

    .line 491
    return-void
.end method

.method public startScan()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 88
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/mobileprint/core/scan/SamsungScan;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
