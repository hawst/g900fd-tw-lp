.class public final enum Lcom/sec/mobileprint/core/print/ConnectionType;
.super Ljava/lang/Enum;
.source "ConnectionType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/mobileprint/core/print/ConnectionType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final enum CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

.field public static final enum CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/core/print/ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/sec/mobileprint/core/print/ConnectionType;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/sec/mobileprint/core/print/ConnectionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/sec/mobileprint/core/print/ConnectionType;

    const-string v3, "CONNECTION_TYPE_NW"

    invoke-direct {v2, v3, v1, v1}, Lcom/sec/mobileprint/core/print/ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    .line 11
    new-instance v2, Lcom/sec/mobileprint/core/print/ConnectionType;

    const-string v3, "CONNECTION_TYPE_USB"

    invoke-direct {v2, v3, v4, v4}, Lcom/sec/mobileprint/core/print/ConnectionType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/sec/mobileprint/core/print/ConnectionType;

    sget-object v3, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_NW:Lcom/sec/mobileprint/core/print/ConnectionType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/sec/mobileprint/core/print/ConnectionType;->CONNECTION_TYPE_USB:Lcom/sec/mobileprint/core/print/ConnectionType;

    aput-object v3, v2, v4

    sput-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->ENUM$VALUES:[Lcom/sec/mobileprint/core/print/ConnectionType;

    .line 13
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/sec/mobileprint/core/print/ConnectionType;->typesByValue:Ljava/util/Map;

    .line 15
    invoke-static {}, Lcom/sec/mobileprint/core/print/ConnectionType;->values()[Lcom/sec/mobileprint/core/print/ConnectionType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/sec/mobileprint/core/print/ConnectionType;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 51
    new-instance v1, Lcom/sec/mobileprint/core/print/ConnectionType$1;

    invoke-direct {v1}, Lcom/sec/mobileprint/core/print/ConnectionType$1;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/core/print/ConnectionType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 58
    return-void

    .line 15
    :cond_0
    aget-object v0, v2, v1

    .line 16
    sget-object v4, Lcom/sec/mobileprint/core/print/ConnectionType;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/sec/mobileprint/core/print/ConnectionType;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/sec/mobileprint/core/print/ConnectionType;->m_value:B

    .line 24
    return-void
.end method

.method public static forValue(I)Lcom/sec/mobileprint/core/print/ConnectionType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 31
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/print/ConnectionType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/mobileprint/core/print/ConnectionType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/core/print/ConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/sec/mobileprint/core/print/ConnectionType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/mobileprint/core/print/ConnectionType;->ENUM$VALUES:[Lcom/sec/mobileprint/core/print/ConnectionType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/mobileprint/core/print/ConnectionType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectionTypeValue()I
    .locals 1

    .prologue
    .line 27
    iget-byte v0, p0, Lcom/sec/mobileprint/core/print/ConnectionType;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/sec/mobileprint/core/print/ConnectionType;->m_value:B

    .line 48
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 43
    iget-byte v0, p0, Lcom/sec/mobileprint/core/print/ConnectionType;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 44
    return-void
.end method
