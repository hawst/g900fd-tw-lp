.class Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;
.super Landroid/os/Handler;
.source "SamsungIntentScanSerivce.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IncomingHandler"
.end annotation


# instance fields
.field mScan:Lcom/sec/mobileprint/core/scan/SamsungScan;

.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;)V
    .locals 1
    .param p1, "serviceRef"    # Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mScan:Lcom/sec/mobileprint/core/scan/SamsungScan;

    .line 69
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 70
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 74
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;

    .line 75
    .local v12, "serviceRef":Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;
    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/content/Intent;

    .line 76
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 78
    .local v2, "bundle":Landroid/os/Bundle;
    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "action":Ljava/lang/String;
    # getter for: Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;->access$0()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Action Recvd:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string v13, "org.androidscanning.intent.ACTION_START_DEVICE_DISCOVERY"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 82
    new-instance v4, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-virtual {v12}, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    sget-object v15, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_SCANNER:Lcom/sec/mobileprint/core/print/DeviceType;

    invoke-direct {v4, v13, v14, v15}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;-><init>(Landroid/content/Context;Landroid/os/Messenger;Lcom/sec/mobileprint/core/print/DeviceType;)V

    .line 83
    .local v4, "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    invoke-virtual {v4}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceManagerConnector()V

    .line 130
    .end local v4    # "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const-string v13, "org.androidscanning.intent.ACTION_GET_SCAN_OPTIONS"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 86
    const-string v13, "scanner-address"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 87
    .local v7, "ipAddress":Ljava/lang/String;
    new-instance v3, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;

    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v14, 0x4

    invoke-direct {v3, v7, v13, v14}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;-><init>(Ljava/lang/String;Landroid/os/Messenger;I)V

    .line 88
    .local v3, "deviceCapability":Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;
    invoke-virtual {v3}, Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;->getCapability()V

    goto :goto_0

    .line 90
    .end local v3    # "deviceCapability":Lcom/sec/mobileprint/core/scan/SamsungScannerCapability;
    .end local v7    # "ipAddress":Ljava/lang/String;
    :cond_2
    const-string v13, "org.androidscanning.intent.ACTION_SCAN"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 92
    new-instance v13, Lcom/sec/mobileprint/core/scan/SamsungScan;

    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v14}, Lcom/sec/mobileprint/core/scan/SamsungScan;-><init>(Landroid/os/Message;I)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mScan:Lcom/sec/mobileprint/core/scan/SamsungScan;

    .line 93
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mScan:Lcom/sec/mobileprint/core/scan/SamsungScan;

    invoke-virtual {v13}, Lcom/sec/mobileprint/core/scan/SamsungScan;->startScan()V

    goto :goto_0

    .line 95
    :cond_3
    const-string v13, "org.androidscanning.intent.ACTION_CANCEL_SCAN_JOB"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 96
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/mobileprint/intentsdk/scan/SamsungIntentScanSerivce$IncomingHandler;->mScan:Lcom/sec/mobileprint/core/scan/SamsungScan;

    invoke-virtual {v13}, Lcom/sec/mobileprint/core/scan/SamsungScan;->cancelScan()V

    goto :goto_0

    .line 97
    :cond_4
    const-string v13, "org.androidprinting.intent.ACTION_CONNECT_PRINTER_USING_WIFIDIRECT"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 99
    const/4 v8, 0x0

    .line 100
    .local v8, "messages":[Landroid/nfc/NdefMessage;
    const-string v13, "printer_ndef_message"

    invoke-virtual {v2, v13}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v9

    .line 101
    .local v9, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v9, :cond_5

    .line 102
    array-length v13, v9

    new-array v8, v13, [Landroid/nfc/NdefMessage;

    .line 103
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v13, v9

    if-lt v5, v13, :cond_6

    .line 107
    .end local v5    # "i":I
    :cond_5
    if-eqz v8, :cond_0

    .line 109
    const/4 v13, 0x0

    :try_start_0
    aget-object v13, v8, v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->createSamsungNdefMessage(Landroid/nfc/NdefMessage;)Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    move-result-object v11

    .line 110
    .local v11, "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->getRecordAtIndex(I)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v10

    .line 111
    .local v10, "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    if-eqz v10, :cond_0

    .line 112
    instance-of v13, v10, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    if-eqz v13, :cond_7

    .line 113
    check-cast v10, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    .end local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v10, v12, v13}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    .end local v11    # "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    :catch_0
    move-exception v13

    goto :goto_0

    .line 104
    .restart local v5    # "i":I
    :cond_6
    aget-object v13, v9, v5

    check-cast v13, Landroid/nfc/NdefMessage;

    aput-object v13, v8, v5

    .line 103
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 114
    .end local v5    # "i":I
    .restart local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .restart local v11    # "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    :cond_7
    :try_start_1
    instance-of v13, v10, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    if-eqz v13, :cond_8

    .line 115
    check-cast v10, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    .end local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v10, v12, v13}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0

    .line 116
    .restart local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_8
    instance-of v13, v10, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    if-eqz v13, :cond_9

    .line 117
    check-cast v10, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .end local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v10, v12, v13}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0

    .line 118
    .restart local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_9
    instance-of v13, v10, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    if-eqz v13, :cond_0

    .line 119
    check-cast v10, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .end local v10    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v10, v12, v13}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
