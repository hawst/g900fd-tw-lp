.class public Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;
.super Ljava/lang/Object;
.source "SamsungIntentPrintJob.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private mDeviceIPAddress:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mJobHandle:Ljava/lang/String;

.field private mMessenger:Landroid/os/Messenger;

.field private mPrintFiles:[Ljava/lang/String;

.field private mPrintSettings:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Landroid/os/Messenger;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "messenger"    # Landroid/os/Messenger;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->setPrintSettings(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0, p2}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->setMessenger(Landroid/os/Messenger;)V

    .line 31
    const-string v0, "printer-address"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->setDeviceIPAddress(Ljava/lang/String;)V

    .line 32
    const-string v0, "file-list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->setPrintFiles([Ljava/lang/String;)V

    .line 33
    const-string v0, "print-job-handle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->setJobHandle(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method private createQueuedStatus()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 197
    .local v0, "jobQueuedStatus":Landroid/os/Bundle;
    const-string v1, "print-job-handle"

    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mJobHandle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v1, "status-printer-state"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 199
    const-string v1, "print-job-status"

    const-string v2, "print-job-queued"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    return-object v0
.end method

.method public static replyFinalSetings(Landroid/os/Message;)V
    .locals 23
    .param p0, "msg"    # Landroid/os/Message;

    .prologue
    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Landroid/content/Intent;

    .line 115
    .local v16, "requestIntent":Landroid/content/Intent;
    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v19

    .line 117
    .local v19, "settings":Landroid/os/Bundle;
    const-string v20, "printer-address"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 118
    .local v3, "address":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_1

    .line 119
    :cond_0
    const/4 v3, 0x0

    .line 122
    :cond_1
    const-string v20, "copies"

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 123
    .local v6, "numCopies":I
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ge v6, v0, :cond_2

    .line 124
    const/4 v6, 0x1

    .line 126
    :cond_2
    const-string v20, "copies"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    const-string v20, "print-quality"

    const-string v21, "normal"

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 132
    .local v15, "quality":Ljava/lang/String;
    const-string v20, "print-quality"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v20, "high"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 134
    const/16 v17, 0x258

    .line 141
    .local v17, "resolution":I
    :goto_0
    const-string v20, "full-bleed"

    const-string v21, "off"

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "fullBleed":Ljava/lang/String;
    const-string v20, "on"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 143
    const/4 v8, 0x0

    .local v8, "page_margin_bottom":F
    move v10, v8

    .local v10, "page_margin_right":F
    move v9, v8

    .local v9, "page_margin_left":F
    move v11, v8

    .line 147
    .local v11, "page_margin_top":F
    :goto_1
    const-string v20, "full-bleed"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v20, "media-size-name"

    const-string v21, "na_letter_8.5x11in"

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "mediaSize":Ljava/lang/String;
    const-string v20, "media-size-name"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v20, "na_index-4x6_4x6in"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 152
    const/high16 v12, 0x40800000    # 4.0f

    .line 153
    .local v12, "page_width":F
    const/high16 v7, 0x40c00000    # 6.0f

    .line 162
    .local v7, "page_height":F
    :goto_2
    add-float v20, v9, v10

    sub-float v20, v12, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->intValue()I

    move-result v20

    mul-int v14, v20, v17

    .line 163
    .local v14, "printable_width":I
    add-float v20, v11, v8

    sub-float v20, v7, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->intValue()I

    move-result v20

    mul-int v13, v20, v17

    .line 165
    .local v13, "printable_height":I
    const-string v20, "print-resolution"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v20, "printable-pixel-width"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 167
    const-string v20, "printable-pixel-height"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    const-string v20, "page-width"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 170
    const-string v20, "page-height"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 171
    const-string v20, "page-margin-top"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 172
    const-string v20, "page-margin-left"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 173
    const-string v20, "page-margin-right"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 174
    const-string v20, "page-margin-bottom"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 176
    new-instance v18, Landroid/content/Intent;

    invoke-direct/range {v18 .. v18}, Landroid/content/Intent;-><init>()V

    .line 177
    .local v18, "returnIntent":Landroid/content/Intent;
    if-nez v3, :cond_8

    .line 178
    const-string v20, "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v20, "print-error"

    .line 181
    const-string v21, "communication-error"

    .line 179
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    :goto_3
    const-string v20, "request-action"

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_4
    return-void

    .line 135
    .end local v4    # "fullBleed":Ljava/lang/String;
    .end local v5    # "mediaSize":Ljava/lang/String;
    .end local v7    # "page_height":F
    .end local v8    # "page_margin_bottom":F
    .end local v9    # "page_margin_left":F
    .end local v10    # "page_margin_right":F
    .end local v11    # "page_margin_top":F
    .end local v12    # "page_width":F
    .end local v13    # "printable_height":I
    .end local v14    # "printable_width":I
    .end local v17    # "resolution":I
    .end local v18    # "returnIntent":Landroid/content/Intent;
    :cond_3
    const-string v20, "draft"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 136
    const/16 v17, 0x96

    .line 137
    .restart local v17    # "resolution":I
    goto/16 :goto_0

    .line 138
    .end local v17    # "resolution":I
    :cond_4
    const/16 v17, 0x12c

    .restart local v17    # "resolution":I
    goto/16 :goto_0

    .line 145
    .restart local v4    # "fullBleed":Ljava/lang/String;
    :cond_5
    const/high16 v8, 0x3e000000    # 0.125f

    .restart local v8    # "page_margin_bottom":F
    move v10, v8

    .restart local v10    # "page_margin_right":F
    move v9, v8

    .restart local v9    # "page_margin_left":F
    move v11, v8

    .restart local v11    # "page_margin_top":F
    goto/16 :goto_1

    .line 154
    .restart local v5    # "mediaSize":Ljava/lang/String;
    :cond_6
    const-string v20, "na_5x7_5x7in"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 155
    const/high16 v12, 0x40a00000    # 5.0f

    .line 156
    .restart local v12    # "page_width":F
    const/high16 v7, 0x40e00000    # 7.0f

    .line 157
    .restart local v7    # "page_height":F
    goto/16 :goto_2

    .line 158
    .end local v7    # "page_height":F
    .end local v12    # "page_width":F
    :cond_7
    const/high16 v12, 0x41080000    # 8.5f

    .line 159
    .restart local v12    # "page_width":F
    const/high16 v7, 0x41300000    # 11.0f

    .restart local v7    # "page_height":F
    goto/16 :goto_2

    .line 183
    .restart local v13    # "printable_height":I
    .restart local v14    # "printable_width":I
    .restart local v18    # "returnIntent":Landroid/content/Intent;
    :cond_8
    const-string v20, "org.androidprinting.intent.ACTION_RETURN_FINAL_PARAMS"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    invoke-virtual/range {v18 .. v19}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_3

    .line 190
    :catch_0
    move-exception v20

    goto :goto_4
.end method


# virtual methods
.method public getDeviceIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceIPAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getJobHandle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mJobHandle:Ljava/lang/String;

    return-object v0
.end method

.method public getMessenger()Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method public getPrintFiles()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintFiles:[Ljava/lang/String;

    return-object v0
.end method

.method public getPrintSettings()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    return-object v0
.end method

.method public getSamsungPrintJob()Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .locals 12

    .prologue
    .line 207
    new-instance v8, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    invoke-direct {v8}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;-><init>()V

    .line 208
    .local v8, "printSettings":Lcom/sec/mobileprint/core/print/SamsungPrintSettings;
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "copies"

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 209
    .local v1, "copies":I
    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 210
    :cond_0
    const/16 v9, 0x63

    if-le v1, v9, :cond_1

    const/16 v1, 0x63

    .line 211
    :cond_1
    invoke-virtual {v8, v1}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setCopies(I)V

    .line 214
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "media-size-name"

    const-string v11, "Letter"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 215
    .local v4, "mediaSizeName":Ljava/lang/String;
    invoke-virtual {v8, v4}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaSize(Ljava/lang/String;)V

    .line 218
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "media-type"

    const-string v11, "Normal"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "mediaType":Ljava/lang/String;
    invoke-virtual {v8, v5}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setMediaType(Ljava/lang/String;)V

    .line 223
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "sides"

    const-string v11, "one-sided"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 224
    .local v2, "duplex":Ljava/lang/String;
    const-string v9, "one-sided"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 225
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_ONE_SIDE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    .line 228
    :cond_2
    :goto_0
    const-string v9, "two-sided-short-edge"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 229
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_SHORT_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    .line 232
    :cond_3
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "print-color-mode"

    const-string v11, "monochrome"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "color":Ljava/lang/String;
    const-string v9, "monochrome"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 234
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    .line 239
    :goto_1
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "orientation-requested"

    const-string v11, "portrait"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 240
    .local v6, "orienatation":Ljava/lang/String;
    const-string v9, "portrait"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 241
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_PORTRAINT:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOrientation(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V

    .line 246
    :goto_2
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v10, "print-document-category"

    const-string v11, "Photo"

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 247
    .local v7, "printDocumentCategory":Ljava/lang/String;
    const-string v9, "Photo"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 248
    sget v9, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_PHOTO:I

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setContentType(I)V

    .line 254
    :goto_3
    iget-object v9, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceIPAddress:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintFiles:[Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->createPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintSettings;Ljava/lang/String;[Ljava/lang/String;)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v3

    .line 256
    .local v3, "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    return-object v3

    .line 226
    .end local v0    # "color":Ljava/lang/String;
    .end local v3    # "job":Lcom/sec/mobileprint/core/print/SamsungPrintJob;
    .end local v6    # "orienatation":Ljava/lang/String;
    .end local v7    # "printDocumentCategory":Ljava/lang/String;
    :cond_4
    const-string v9, "two-sided-long-edge"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 227
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;->DUPLEX_TWO_SIDE_LONG_EDGE:Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setDuplex(Lcom/sec/print/mobileprint/printoptionattribute/Duplex$EnumDuplex;)V

    goto :goto_0

    .line 236
    .restart local v0    # "color":Ljava/lang/String;
    :cond_5
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->COLOR:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setColor(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    goto :goto_1

    .line 243
    .restart local v6    # "orienatation":Ljava/lang/String;
    :cond_6
    sget-object v9, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->ORIENTATION_LANDSCAPE:Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setOrientation(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V

    goto :goto_2

    .line 250
    .restart local v7    # "printDocumentCategory":Ljava/lang/String;
    :cond_7
    sget v9, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->PRINT_CONTENT_TYPE_DOCUMENT:I

    invoke-virtual {v8, v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->setContentType(I)V

    goto :goto_3
.end method

.method public sendErrorMessage()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public setDeviceIPAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDeviceIPAddress"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceIPAddress:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDeviceName"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setJobHandle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mJobHandle"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mJobHandle:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setMessenger(Landroid/os/Messenger;)V
    .locals 0
    .param p1, "mMessenger"    # Landroid/os/Messenger;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mMessenger:Landroid/os/Messenger;

    .line 93
    return-void
.end method

.method public setPrintFiles([Ljava/lang/String;)V
    .locals 0
    .param p1, "mPrintFiles"    # [Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintFiles:[Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setPrintSettings(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "mPrintSettings"    # Landroid/os/Bundle;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    .line 73
    return-void
.end method

.method public validatePrintJob()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 96
    const/4 v0, 0x1

    .line 97
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mJobHandle:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mJobHandle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v1

    .line 99
    :cond_1
    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceIPAddress:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mDeviceIPAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v3, "file-list"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 102
    iget-object v2, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->mPrintSettings:Landroid/os/Bundle;

    const-string v3, "file-list"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 106
    const/4 v1, 0x1

    goto :goto_0
.end method
