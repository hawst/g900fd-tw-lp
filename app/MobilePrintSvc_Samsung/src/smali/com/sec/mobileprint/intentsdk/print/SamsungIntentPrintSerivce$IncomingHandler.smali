.class Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;
.super Landroid/os/Handler;
.source "SamsungIntentPrintSerivce.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IncomingHandler"
.end annotation


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)V
    .locals 1
    .param p1, "serviceRef"    # Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 84
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 24
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;

    .line 89
    .local v20, "serviceRef":Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Landroid/content/Intent;

    .line 90
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 92
    .local v4, "bundle":Landroid/os/Bundle;
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "action":Ljava/lang/String;
    # getter for: Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->access$0()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Action Recvd:"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v21, "org.androidprinting.intent.ACTION_START_DEVICE_DISCOVERY"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 96
    new-instance v6, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v22, v0

    sget-object v23, Lcom/sec/mobileprint/core/print/DeviceType;->DEVICETYPE_PRINTER:Lcom/sec/mobileprint/core/print/DeviceType;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v6, v0, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;-><init>(Landroid/content/Context;Landroid/os/Messenger;Lcom/sec/mobileprint/core/print/DeviceType;)V

    .line 97
    .local v6, "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    invoke-virtual {v6}, Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;->startDeviceManagerConnector()V

    .line 191
    .end local v6    # "deviceDiscovery":Lcom/sec/mobileprint/core/print/SamsungDeviceDiscovery;
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    const-string v21, "org.androidprinting.intent.ACTION_GET_PRINT_OPTIONS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 100
    const-string v21, "printer-address"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 101
    .local v9, "ipAddress":Ljava/lang/String;
    new-instance v5, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v5, v0, v9, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Messenger;I)V

    .line 102
    .local v5, "deviceCapability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    invoke-virtual {v5}, Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;->startDeviceManagerConnector()V

    goto :goto_0

    .line 104
    .end local v5    # "deviceCapability":Lcom/sec/mobileprint/core/print/SamsungDeviceCapability;
    .end local v9    # "ipAddress":Ljava/lang/String;
    :cond_2
    const-string v21, "org.androidprinting.intent.ACTION_GET_FINAL_PRINT_SETTINGS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 106
    invoke-static/range {p1 .. p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->replyFinalSetings(Landroid/os/Message;)V

    goto :goto_0

    .line 108
    :cond_3
    const-string v21, "org.androidprinting.intent.ACTION_PRINT"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 109
    new-instance v16, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;-><init>(Landroid/os/Bundle;Landroid/os/Messenger;)V

    .line 110
    .local v16, "printJob":Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->validatePrintJob()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 111
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v21

    invoke-virtual/range {v16 .. v16}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->getSamsungPrintJob()Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addPrintJob(Lcom/sec/mobileprint/core/print/SamsungPrintJob;)I

    move-result v13

    .line 112
    .local v13, "jobid":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->addUpdateListener(Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;)V

    .line 113
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    move-object/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;->getJobHandle()Ljava/lang/String;

    move-result-object v22

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v21 .. v23}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 119
    .end local v13    # "jobid":I
    .end local v16    # "printJob":Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintJob;
    :cond_4
    const-string v21, "org.androidprinting.intent.ACTION_CANCEL_PRINT_JOB"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 120
    const-string v21, "print-job-handle"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 121
    .local v10, "jobHandle":Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 122
    .local v12, "jobId":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    goto/16 :goto_0

    .line 123
    .end local v10    # "jobHandle":Ljava/lang/String;
    .end local v12    # "jobId":I
    :cond_5
    const-string v21, "org.androidprinting.intent.ACTION_CANCEL_ALL_PRINT_JOBS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 124
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v11

    .line 125
    .local v11, "jobHandleEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 126
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    move-object/from16 v21, v0

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 127
    .restart local v12    # "jobId":I
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->cancelPrint(I)V

    goto :goto_1

    .line 129
    .end local v11    # "jobHandleEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v12    # "jobId":I
    :cond_6
    const-string v21, "org.androidprinting.intent.ACTION_REGISTER_STATUS_RECEIVER"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 131
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 132
    # getter for: Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->access$1(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 133
    # getter for: Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->access$1(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 137
    :cond_7
    const-string v21, "org.androidprinting.intent.ACTION_UNREGISTER_STATUS_RECEIVER"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 139
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 140
    # getter for: Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;
    invoke-static/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->access$1(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)Ljava/util/LinkedList;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 142
    :cond_8
    const-string v21, "org.androidprinting.intent.ACTION_GET_JOB_STATUS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 143
    const-string v21, "org.androidprinting.intent.ACTION_START_MONITORING_PRINTER_STATUS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 146
    :cond_9
    new-instance v21, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;-><init>(Landroid/content/Context;Landroid/os/Message;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 147
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->startMonitoring()V

    goto/16 :goto_0

    .line 149
    :cond_a
    const-string v21, "org.androidprinting.intent.ACTION_GET_ALL_JOBS_STATUS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 151
    const-string v21, "org.androidprinting.intent.ACTION_GET_PRINTER_STATUS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_0

    .line 153
    const-string v21, "org.androidprinting.intent.ACTION_STOP_MONITORING_PRINTER_STATUS"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 155
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 156
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;->stopMonitoring(Landroid/os/Message;)V

    .line 157
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    goto/16 :goto_0

    .line 160
    :cond_b
    const-string v21, "org.androidprinting.intent.ACTION_CONNECT_PRINTER_USING_WIFIDIRECT"

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 162
    const/4 v14, 0x0

    .line 163
    .local v14, "messages":[Landroid/nfc/NdefMessage;
    const-string v21, "printer_ndef_message"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v17

    .line 164
    .local v17, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v17, :cond_c

    .line 165
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v14, v0, [Landroid/nfc/NdefMessage;

    .line 166
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v7, v0, :cond_d

    .line 170
    .end local v7    # "i":I
    :cond_c
    if-eqz v14, :cond_0

    .line 171
    const/16 v21, 0x0

    aget-object v21, v14, v21

    invoke-virtual/range {v21 .. v21}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v21

    const/16 v22, 0x0

    aget-object v21, v21, v22

    invoke-virtual/range {v21 .. v21}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v15

    .line 173
    .local v15, "payload":[B
    const/16 v21, 0x0

    aget-object v21, v14, v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->createSamsungNdefMessage(Landroid/nfc/NdefMessage;)Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    move-result-object v19

    .line 174
    .local v19, "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->getRecordAtIndex(I)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v18

    .line 175
    .local v18, "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    if-eqz v18, :cond_0

    .line 176
    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    move/from16 v21, v0

    if-eqz v21, :cond_e

    .line 177
    check-cast v18, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    .end local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0

    .line 167
    .end local v15    # "payload":[B
    .end local v19    # "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    .restart local v7    # "i":I
    :cond_d
    aget-object v21, v17, v7

    check-cast v21, Landroid/nfc/NdefMessage;

    aput-object v21, v14, v7

    .line 166
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 178
    .end local v7    # "i":I
    .restart local v15    # "payload":[B
    .restart local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .restart local v19    # "samsungMessage":Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    :cond_e
    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    move/from16 v21, v0

    if-eqz v21, :cond_f

    .line 179
    check-cast v18, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    .end local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0

    .line 180
    .restart local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_f
    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    move/from16 v21, v0

    if-eqz v21, :cond_10

    .line 181
    check-cast v18, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .end local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0

    .line 182
    .restart local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_10
    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    move/from16 v21, v0

    if-eqz v21, :cond_0

    .line 183
    check-cast v18, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .end local v18    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V

    goto/16 :goto_0
.end method
