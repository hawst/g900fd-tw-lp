.class public Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;
.super Landroid/app/Service;
.source "SamsungIntentPrintSerivce.java"

# interfaces
.implements Lcom/sec/mobileprint/core/print/SamsungPrintingTask$IUpdateJobStatus;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field mJobMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mMessenger:Landroid/os/Messenger;

.field mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

.field private mStatusCallbacks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "SamsungIntentPrintSerivce"

    sput-object v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mPrinterStatus:Lcom/sec/mobileprint/core/print/SamsungPrinterStatus;

    .line 367
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce$IncomingHandler;-><init>(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mMessenger:Landroid/os/Messenger;

    .line 37
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    return-object v0
.end method

.method private getJobHandlefromMap(I)Ljava/lang/String;
    .locals 5
    .param p1, "jobId"    # I

    .prologue
    .line 347
    iget-object v3, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 355
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 347
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 348
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 349
    .local v1, "key":Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 351
    .local v2, "value":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public getCopyNumber(III)I
    .locals 4
    .param p1, "currentPage"    # I
    .param p2, "requestedCopies"    # I
    .param p3, "pageCount"    # I

    .prologue
    const/4 v1, 0x1

    .line 333
    const/4 v0, 0x1

    .line 334
    .local v0, "copy":I
    div-int v2, p1, p3

    rem-int v3, p1, p3

    if-ne v3, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    add-int v0, v2, v1

    .line 335
    return v0
.end method

.method public getPageNumber(III)I
    .locals 1
    .param p1, "currentPage"    # I
    .param p2, "requestedCopies"    # I
    .param p3, "pageCount"    # I

    .prologue
    .line 339
    const/4 v0, 0x1

    .line 340
    .local v0, "pageNumber":I
    rem-int v0, p1, p3

    .line 341
    if-nez v0, :cond_0

    move v0, p3

    .line 342
    :cond_0
    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 64
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    .line 65
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    .line 66
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 72
    return-void
.end method

.method public onJobStatusUpdated(III)V
    .locals 12
    .param p1, "jobId"    # I
    .param p2, "type"    # I
    .param p3, "value"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 197
    const/4 v2, 0x0

    .line 198
    .local v2, "mTotalPages":I
    const-string v6, ""

    .line 199
    .local v6, "typeString":Ljava/lang/String;
    const-string v7, ""

    .line 200
    .local v7, "valueString":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 201
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 203
    .local v4, "returnIntent":Landroid/content/Intent;
    packed-switch p2, :pswitch_data_0

    .line 314
    :cond_0
    :goto_0
    const-string v8, "print-error"

    invoke-virtual {v4, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "type : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " value : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 316
    .local v5, "statusMessage":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 317
    :cond_1
    const-string v8, "org.androidprinting.intent.ACTION_RETURN_PRINT_JOB_STATUS"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 320
    :cond_2
    iget-object v8, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mStatusCallbacks:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    .line 328
    return-void

    .line 206
    .end local v5    # "statusMessage":Ljava/lang/String;
    :pswitch_0
    const-string v6, "prepare"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 207
    const-string v8, "org.androidprinting.intent.ACTION_RETURN_PRINT_STARTED"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v8, "request-action"

    const-string v9, "org.androidprinting.intent.ACTION_PRINT"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v8, "printer-address"

    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getIPAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const-string v8, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 213
    :pswitch_1
    const-string v6, "start job"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Total Page="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 214
    move v2, p3

    .line 215
    goto/16 :goto_0

    .line 218
    :pswitch_2
    const-string v6, "start page"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Page No="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 219
    const-string v8, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v8, "status-printer-state"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    const-string v8, "print-job-status"

    const-string v9, "print-job-running"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v8, "status-page-info"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 223
    const-string v8, "page-start-info"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 224
    const-string v8, "copy-num"

    .line 225
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v9

    .line 226
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    .line 224
    invoke-virtual {p0, p3, v9, v10}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getCopyNumber(III)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 227
    const-string v8, "page-num"

    .line 228
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v9

    .line 229
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    .line 227
    invoke-virtual {p0, p3, v9, v10}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getPageNumber(III)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 230
    const-string v8, "current-page"

    invoke-virtual {v0, v8, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 231
    const-string v8, "total-pages"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 235
    :pswitch_3
    const-string v6, "printing"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "percentage="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 236
    goto/16 :goto_0

    .line 239
    :pswitch_4
    const-string v6, "completed page"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "completed page No="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 240
    const-string v8, "print-job-handle"

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v8, "status-printer-state"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 242
    const-string v8, "print-job-status"

    const-string v9, "print-job-running"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v8, "status-page-info"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 244
    const-string v8, "page-start-info"

    invoke-virtual {v0, v8, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 245
    const-string v8, "copy-num"

    .line 246
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v9

    .line 247
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    .line 245
    invoke-virtual {p0, p3, v9, v10}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getCopyNumber(III)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 248
    const-string v8, "page-num"

    .line 249
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getPrintJobSettings()Lcom/sec/mobileprint/core/print/SamsungPrintSettings;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/mobileprint/core/print/SamsungPrintSettings;->getCopies()I

    move-result v9

    .line 250
    invoke-static {}, Lcom/sec/mobileprint/core/App;->getJobManager()Lcom/sec/mobileprint/core/print/SamsungJobManager;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/mobileprint/core/print/SamsungJobManager;->getSamsungPrintJob(I)Lcom/sec/mobileprint/core/print/SamsungPrintJob;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/mobileprint/core/print/SamsungPrintJob;->getFilePaths()[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    .line 248
    invoke-virtual {p0, p3, v9, v10}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getPageNumber(III)I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 251
    const-string v8, "current-page"

    invoke-virtual {v0, v8, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 252
    const-string v8, "total-pages"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 257
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "jobHandle":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 261
    const-string v6, "completed job"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "total Page="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 262
    const-string v8, "print-job-handle"

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v8, "status-printer-state"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 264
    const-string v8, "print-job-status"

    const-string v9, "print-job-complete"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v8, "print-job-done-result"

    const-string v9, "job-success"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v8, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v8, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 272
    .end local v1    # "jobHandle":Ljava/lang/String;
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    .restart local v1    # "jobHandle":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 275
    const-string v6, "canceled"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 276
    const-string v8, "print-job-handle"

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v8, "status-printer-state"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 278
    const-string v8, "print-job-status"

    const-string v9, "print-job-complete"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v8, "print-job-done-result"

    const-string v9, "job-cancelled"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v8, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-virtual {v8, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 285
    .end local v1    # "jobHandle":Ljava/lang/String;
    :pswitch_7
    const-string v6, "error"

    .line 286
    const-string v8, "org.androidprinting.intent.ACTION_RETURN_PRINT_ERROR"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    iget-object v8, p0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->mJobMap:Ljava/util/Hashtable;

    invoke-direct {p0, p1}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentPrintSerivce;->getJobHandlefromMap(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    packed-switch p3, :pswitch_data_1

    goto/16 :goto_0

    .line 291
    :pswitch_8
    const-string v7, "Out of memory"

    .line 292
    goto/16 :goto_0

    .line 294
    :pswitch_9
    const-string v7, "printing option error"

    .line 295
    goto/16 :goto_0

    .line 297
    :pswitch_a
    const-string v7, "not supported contents"

    .line 298
    goto/16 :goto_0

    .line 300
    :pswitch_b
    const-string v7, "Output stream error"

    .line 301
    goto/16 :goto_0

    .line 303
    :pswitch_c
    const-string v7, "PrintService STILL_RUNNING"

    .line 304
    goto/16 :goto_0

    .line 306
    :pswitch_d
    const-string v7, "NOT_SUPPORTED_PRINTER"

    .line 307
    goto/16 :goto_0

    .line 309
    :pswitch_e
    const-string v7, "Invalid contents"

    goto/16 :goto_0

    .line 320
    .restart local v5    # "statusMessage":Ljava/lang/String;
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 322
    .local v3, "messenger":Landroid/os/Messenger;
    const/4 v9, 0x0

    const/4 v10, 0x0

    :try_start_0
    invoke-static {v9, v10, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 323
    :catch_0
    move-exception v9

    goto/16 :goto_1

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 288
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 57
    const/4 v0, 0x2

    return v0
.end method
