.class public Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup;
.super Ljava/lang/Object;
.source "SamsungIntentMediaLookup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMediaSize(Ljava/lang/String;)Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;
    .locals 9
    .param p0, "mediaSize"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0xdb3

    const/16 v8, 0x9f6

    const/16 v7, 0x9b0

    const/16 v2, 0x32

    .line 10
    const-string v0, "iso_a4_210x297mm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;

    const-string v1, "A4"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;-><init>(Ljava/lang/String;IIIIII)V

    .line 17
    :goto_0
    return-object v0

    .line 12
    :cond_0
    const-string v0, "na_legal_8.5x14in"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    new-instance v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;

    const-string v1, "Legal"

    const/16 v6, 0x1068

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;-><init>(Ljava/lang/String;IIIIII)V

    goto :goto_0

    .line 14
    :cond_1
    const-string v0, "na_letter_8.5x11in"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15
    new-instance v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;

    const-string v1, "Letter"

    const/16 v6, 0xce4

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;-><init>(Ljava/lang/String;IIIIII)V

    goto :goto_0

    .line 17
    :cond_2
    new-instance v0, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;

    const-string v1, "A4"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/sec/mobileprint/intentsdk/print/SamsungIntentMediaLookup$MediaSize;-><init>(Ljava/lang/String;IIIIII)V

    goto :goto_0
.end method

.method public static getMediatype(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "hpMediaTypeString"    # Ljava/lang/String;

    .prologue
    .line 22
    const-string v0, "auto"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    const-string v0, "Normal"

    .line 27
    :goto_0
    return-object v0

    .line 24
    :cond_0
    const-string v0, "photographic-glossy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    const-string v0, "Photo"

    goto :goto_0

    .line 27
    :cond_1
    const-string v0, "Normal"

    goto :goto_0
.end method
