.class public interface abstract Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
.super Ljava/lang/Object;
.source "IOnDeviceInfoChangedListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onDeviceCapabilityUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onDeviceDiscovered(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onDeviceStatusUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onError(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
