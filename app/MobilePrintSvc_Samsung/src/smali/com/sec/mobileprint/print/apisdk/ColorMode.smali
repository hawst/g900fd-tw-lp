.class public final enum Lcom/sec/mobileprint/print/apisdk/ColorMode;
.super Ljava/lang/Enum;
.source "ColorMode.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final enum COLOR_MODE_COLOR:Lcom/sec/mobileprint/print/apisdk/ColorMode;

.field public static final enum COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/ColorMode;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    const-string v3, "COLOR_MODE_MONO"

    invoke-direct {v2, v3, v1, v1}, Lcom/sec/mobileprint/print/apisdk/ColorMode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 11
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    const-string v3, "COLOR_MODE_COLOR"

    invoke-direct {v2, v3, v4, v4}, Lcom/sec/mobileprint/print/apisdk/ColorMode;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_COLOR:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/sec/mobileprint/print/apisdk/ColorMode;

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    aput-object v3, v2, v1

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_COLOR:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    aput-object v3, v2, v4

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;->ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 12
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/ColorMode;->typesByValue:Ljava/util/Map;

    .line 14
    invoke-static {}, Lcom/sec/mobileprint/print/apisdk/ColorMode;->values()[Lcom/sec/mobileprint/print/apisdk/ColorMode;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/sec/mobileprint/print/apisdk/ColorMode;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 50
    new-instance v1, Lcom/sec/mobileprint/print/apisdk/ColorMode$1;

    invoke-direct {v1}, Lcom/sec/mobileprint/print/apisdk/ColorMode$1;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/print/apisdk/ColorMode;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 57
    return-void

    .line 14
    :cond_0
    aget-object v0, v2, v1

    .line 15
    sget-object v4, Lcom/sec/mobileprint/print/apisdk/ColorMode;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->m_value:B

    .line 23
    return-void
.end method

.method public static forValue(I)Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 30
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->typesByValue:Ljava/util/Map;

    int-to-byte v1, p0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/ColorMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getColorTypeValue()I
    .locals 1

    .prologue
    .line 26
    iget-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->m_value:B

    .line 47
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 42
    iget-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 43
    return-void
.end method
