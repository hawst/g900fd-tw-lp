.class public Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;
.super Ljava/lang/Object;
.source "SamsungPrintingService.java"


# static fields
.field private static mListener:Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

.field private static mSDKService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;)V
    .locals 0

    .prologue
    .line 15
    sput-object p0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mSDKService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    return-void
.end method

.method static synthetic access$1()Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mListener:Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mSDKService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    if-eqz v0, :cond_0

    .line 24
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mSDKService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    .line 26
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listener"    # Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

    .prologue
    .line 32
    sput-object p1, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mListener:Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

    .line 34
    invoke-static {p0}, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->isSamsungPrintPluginInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService$1;

    invoke-direct {v1}, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService$1;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->connect(Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mListener:Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;

    const-string v1, "Samsung Print Service Plugin id not installed. Please search for package name com.sec.app.samsungprintservice in play store"

    invoke-interface {v0, v1}, Lcom/sec/mobileprint/print/apisdk/OnInitializationListener;->onFaliure(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isSamsungPrintPluginInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 62
    .local v0, "output":Z
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 64
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.sec.app.samsungprintservice"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 65
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 67
    const/4 v0, 0x1

    .line 70
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v0

    .line 69
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public static release(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/SamsungPrintingService;->mSDKService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->getInstance()Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->releaseService(Landroid/content/Context;)V

    .line 57
    :cond_0
    return-void
.end method
