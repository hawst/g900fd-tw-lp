.class public final enum Lcom/sec/mobileprint/print/apisdk/MediaSize;
.super Ljava/lang/Enum;
.source "MediaSize.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_A3:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_A4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_A5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_A6:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_B5ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_C5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_C6ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_COM10:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_CUSTOM:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_DL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_ENV_NO9:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_ENV_PERSONAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_EXEC:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_FOLIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_JISB4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_JISB5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_LEGAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_LETTER:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_MONARCH:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_OFICIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_PHOTO_BOARDERLESS_3_5X5IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_PHOTO_BOARDERLESS_4X6IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_PHOTO_BOARDERLESS_5X7IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_STATEMENT:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field public static final enum MEDIA_SIZE_TABLOID:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mPaperName:Ljava/lang/String;

.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_LETTER"

    const-string v4, "Letter"

    invoke-direct {v2, v3, v1, v7, v4}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_LETTER:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 11
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_LEGAL"

    .line 12
    const-string v4, "Legal"

    invoke-direct {v2, v3, v7, v9, v4}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_LEGAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 13
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_OFICIO"

    const/4 v4, 0x2

    .line 14
    const/16 v5, 0xbe

    const-string v6, "Oficio"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_OFICIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 15
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_FOLIO"

    .line 16
    const/16 v4, 0xe

    const-string v5, "Folio"

    invoke-direct {v2, v3, v8, v4, v5}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_FOLIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 17
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_A3"

    const/4 v4, 0x4

    .line 18
    const/16 v5, 0x8

    const-string v6, "A3"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A3:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 19
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_TABLOID"

    .line 20
    const-string v4, "Tabloid"

    invoke-direct {v2, v3, v9, v8, v4}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_TABLOID:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 21
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_A4"

    .line 22
    const/16 v4, 0x9

    const-string v5, "A4"

    invoke-direct {v2, v3, v10, v4, v5}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 23
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_B5ENV"

    const/4 v4, 0x7

    .line 24
    const/16 v5, 0x22

    const-string v6, "B5ENV"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_B5ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 25
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_JISB4"

    const/16 v4, 0x8

    .line 26
    const/16 v5, 0xc

    const-string v6, "JISB4"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_JISB4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 27
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_JISB5"

    const/16 v4, 0x9

    .line 28
    const/16 v5, 0xd

    const-string v6, "JISB5"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_JISB5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 29
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_STATEMENT"

    const/16 v4, 0xa

    .line 30
    const-string v5, "Statement"

    invoke-direct {v2, v3, v4, v10, v5}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_STATEMENT:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 31
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_EXEC"

    const/16 v4, 0xb

    .line 32
    const/4 v5, 0x7

    const-string v6, "Executive"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_EXEC:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 33
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_A5"

    const/16 v4, 0xc

    .line 34
    const/16 v5, 0xb

    const-string v6, "A5"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 35
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_A6"

    const/16 v4, 0xd

    .line 36
    const/16 v5, 0x46

    const-string v6, "A6"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A6:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 37
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_MONARCH"

    const/16 v4, 0xe

    .line 38
    const/16 v5, 0x25

    const-string v6, "Monarch"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_MONARCH:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 39
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_COM10"

    const/16 v4, 0xf

    .line 40
    const/16 v5, 0x14

    const-string v6, "Com10"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_COM10:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 41
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_DL"

    const/16 v4, 0x10

    .line 42
    const/16 v5, 0x1b

    const-string v6, "DL"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_DL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 43
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_C5"

    const/16 v4, 0x11

    .line 44
    const/16 v5, 0x1c

    const-string v6, "C5"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_C5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 45
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_C6ENV"

    const/16 v4, 0x12

    .line 46
    const/16 v5, 0x1f

    const-string v6, "C6Env"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_C6ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 47
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_ENV_NO9"

    const/16 v4, 0x13

    .line 48
    const/16 v5, 0x13

    const-string v6, "ENV_NO9"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_ENV_NO9:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 49
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_ENV_PERSONAL"

    const/16 v4, 0x14

    .line 50
    const/16 v5, 0x26

    const-string v6, "ENV_PERSONAL"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_ENV_PERSONAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 51
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_PHOTO_BOARDERLESS_3_5X5IN"

    const/16 v4, 0x15

    const/16 v5, 0xfa

    const-string v6, "3.5x5in"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_3_5X5IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 52
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_PHOTO_BOARDERLESS_4X6IN"

    const/16 v4, 0x16

    const/16 v5, 0xfc

    const-string v6, "4x6in"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_4X6IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 53
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_PHOTO_BOARDERLESS_5X7IN"

    const/16 v4, 0x17

    const/16 v5, 0xfe

    const-string v6, "5x7in"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_5X7IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 55
    new-instance v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const-string v3, "MEDIA_SIZE_CUSTOM"

    const/16 v4, 0x18

    const/16 v5, 0x100

    const-string v6, "Custom"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/mobileprint/print/apisdk/MediaSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_CUSTOM:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    const/16 v2, 0x19

    new-array v2, v2, [Lcom/sec/mobileprint/print/apisdk/MediaSize;

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_LETTER:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v3, v2, v1

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_LEGAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v3, v2, v7

    const/4 v3, 0x2

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_OFICIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_FOLIO:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v3, v2, v8

    const/4 v3, 0x4

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A3:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_TABLOID:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v3, v2, v9

    sget-object v3, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v3, v2, v10

    const/4 v3, 0x7

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_B5ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_JISB4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_JISB5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_STATEMENT:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_EXEC:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A6:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_MONARCH:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_COM10:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_DL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_C5:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_C6ENV:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_ENV_NO9:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_ENV_PERSONAL:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_3_5X5IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_4X6IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_5X7IN:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_CUSTOM:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    aput-object v4, v2, v3

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 57
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/sec/mobileprint/print/apisdk/MediaSize;->typesByValue:Ljava/util/Map;

    .line 59
    invoke-static {}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->values()[Lcom/sec/mobileprint/print/apisdk/MediaSize;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/sec/mobileprint/print/apisdk/MediaSize;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 101
    new-instance v1, Lcom/sec/mobileprint/print/apisdk/MediaSize$1;

    invoke-direct {v1}, Lcom/sec/mobileprint/print/apisdk/MediaSize$1;-><init>()V

    sput-object v1, Lcom/sec/mobileprint/print/apisdk/MediaSize;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 108
    return-void

    .line 59
    :cond_0
    aget-object v0, v2, v1

    .line 60
    sget-object v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p3, "value"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->m_value:B

    .line 69
    iput-object p4, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->mPaperName:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public static forValue(I)Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 77
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->typesByValue:Ljava/util/Map;

    int-to-byte v1, p0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    return-object v0
.end method

.method public static values()[Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->ENUM$VALUES:[Lcom/sec/mobileprint/print/apisdk/MediaSize;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/mobileprint/print/apisdk/MediaSize;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->mPaperName:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaSizeTypeValue()I
    .locals 1

    .prologue
    .line 73
    iget-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->m_value:B

    .line 98
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 93
    iget-byte v0, p0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 94
    return-void
.end method
