.class public abstract Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;
.super Landroid/os/Binder;
.source "IOnDeviceInfoChangedListener.java"

# interfaces
.implements Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

.field static final TRANSACTION_onDeviceCapabilityUpdate:I = 0x2

.field static final TRANSACTION_onDeviceDiscovered:I = 0x1

.field static final TRANSACTION_onDeviceStatusUpdate:I = 0x3

.field static final TRANSACTION_onError:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p0, p0, v0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 97
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 42
    :sswitch_0
    const-string v2, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v2, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    sget-object v2, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    .line 55
    .local v0, "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->onDeviceDiscovered(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V

    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    goto :goto_1

    .line 61
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :sswitch_2
    const-string v2, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 64
    sget-object v2, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    .line 69
    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->onDeviceCapabilityUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V

    .line 70
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    goto :goto_2

    .line 75
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :sswitch_3
    const-string v2, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 78
    sget-object v2, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    .line 83
    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->onDeviceStatusUpdate(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;)V

    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 81
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    goto :goto_3

    .line 89
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :sswitch_4
    const-string v2, "com.sec.mobileprint.print.apisdk.IOnDeviceInfoChangedListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 92
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->onError(I)V

    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
