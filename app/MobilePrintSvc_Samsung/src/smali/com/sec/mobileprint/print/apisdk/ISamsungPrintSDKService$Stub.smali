.class public abstract Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;
.super Landroid/os/Binder;
.source "ISamsungPrintSDKService.java"

# interfaces
.implements Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

.field static final TRANSACTION_cancelPrintJob:I = 0x7

.field static final TRANSACTION_discoverPrinters:I = 0x1

.field static final TRANSACTION_getAllJobList:I = 0x8

.field static final TRANSACTION_getJobStatus:I = 0x9

.field static final TRANSACTION_getPrinterCapability:I = 0x3

.field static final TRANSACTION_startTrackingPrinterStatus:I = 0x4

.field static final TRANSACTION_stopPrinterDiscovery:I = 0x2

.field static final TRANSACTION_stopTrackingPrinterStatus:I = 0x5

.field static final TRANSACTION_submitPrintJob:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 148
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    move-result-object v0

    .line 50
    .local v0, "_arg0":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->discoverPrinters(Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V

    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    :sswitch_2
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->stopPrinterDiscovery()V

    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 63
    :sswitch_3
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    move-result-object v2

    .line 70
    .local v2, "_arg2":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->getPrinterCapability(Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    :sswitch_4
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_0

    .line 79
    sget-object v6, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;

    .line 85
    .local v0, "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;

    move-result-object v1

    .line 86
    .local v1, "_arg1":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    invoke-virtual {p0, v0, v1}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->startTrackingPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 82
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    .end local v1    # "_arg1":Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    goto :goto_1

    .line 92
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
    :sswitch_5
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->stopTrackingPrinterStatus()V

    .line 94
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 99
    :sswitch_6
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_1

    .line 102
    sget-object v6, Lcom/sec/mobileprint/print/apisdk/PrintJob;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrintJob;

    .line 108
    .local v0, "_arg0":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;

    move-result-object v1

    .line 109
    .local v1, "_arg1":Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;
    invoke-virtual {p0, v0, v1}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->submitPrintJob(Lcom/sec/mobileprint/print/apisdk/PrintJob;Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;)I

    move-result v3

    .line 110
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 105
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .end local v1    # "_arg1":Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;
    .end local v3    # "_result":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    goto :goto_2

    .line 116
    .end local v0    # "_arg0":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    :sswitch_7
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 119
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->cancelPrintJob(I)V

    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 125
    .end local v0    # "_arg0":I
    :sswitch_8
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->getAllJobList()Ljava/util/List;

    move-result-object v4

    .line 127
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/mobileprint/print/apisdk/PrintJob;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 128
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 133
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/mobileprint/print/apisdk/PrintJob;>;"
    :sswitch_9
    const-string v6, "com.sec.mobileprint.print.apisdk.ISamsungPrintSDKService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 136
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;->getJobStatus(I)Lcom/sec/mobileprint/print/apisdk/PrintJob;

    move-result-object v3

    .line 137
    .local v3, "_result":Lcom/sec/mobileprint/print/apisdk/PrintJob;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v3, :cond_2

    .line 139
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    invoke-virtual {v3, p3, v5}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 143
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
