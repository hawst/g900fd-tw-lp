.class public interface abstract Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;
.super Ljava/lang/Object;
.source "ISamsungPrintSDKService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService$Stub;
    }
.end annotation


# virtual methods
.method public abstract cancelPrintJob(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract discoverPrinters(Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getAllJobList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/PrintJob;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getJobStatus(I)Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPrinterCapability(Ljava/lang/String;Ljava/lang/String;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract startTrackingPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterInfo;Lcom/sec/mobileprint/print/apisdk/IOnDeviceInfoChangedListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract stopPrinterDiscovery()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract stopTrackingPrinterStatus()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract submitPrintJob(Lcom/sec/mobileprint/print/apisdk/PrintJob;Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
