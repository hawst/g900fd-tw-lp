.class Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;
.super Ljava/lang/Object;
.source "PrintServiceConnector.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;
    }
.end annotation


# static fields
.field static m_this:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;


# instance fields
.field mContext:Landroid/content/Context;

.field mListener:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;

.field mSDKPrintService:Lcom/sec/mobileprint/print/apisdk/ISamsungPrintSDKService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sput-object p0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->m_this:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;

    .line 27
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method public static getInstance()Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->m_this:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;

    return-object v0
.end method


# virtual methods
.method public connect(Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->mListener:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.print.mobileprint.plugin.sdk"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 37
    .local v1, "ret":Z
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector;->mListener:Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/mobileprint/print/apisdk/PrintServiceConnector$OnSDKPrintServiceConnectionListener;->onSDKPrintServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 51
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 57
    return-void
.end method

.method public releaseService(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 45
    return-void
.end method
