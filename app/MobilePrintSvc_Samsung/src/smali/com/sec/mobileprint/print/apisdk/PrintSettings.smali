.class public Lcom/sec/mobileprint/print/apisdk/PrintSettings;
.super Ljava/lang/Object;
.source "PrintSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/PrintSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

.field private mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

.field private mCopies:I

.field private mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

.field private mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

.field private mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

.field private mOutputFileName:Ljava/lang/String;

.field private mPrintToFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintSettings$1;

    invoke-direct {v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings$1;-><init>()V

    sput-object v0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 157
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 19
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->DUPLEX_MODE_SIMPLEX:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .line 20
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    .line 22
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ContentType;->PRINT_CONTENT_TYPE_PHOTO:Lcom/sec/mobileprint/print/apisdk/ContentType;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    .line 23
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/mobileprint/print/apisdk/MediaType;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    .line 24
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setPrintToFile(Z)V

    .line 25
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public constructor <init>(ILcom/sec/mobileprint/print/apisdk/MediaSize;Lcom/sec/mobileprint/print/apisdk/DuplexMode;Lcom/sec/mobileprint/print/apisdk/ColorMode;Lcom/sec/mobileprint/print/apisdk/ContentType;)V
    .locals 1
    .param p1, "copies"    # I
    .param p2, "mediaSize"    # Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .param p3, "duplex"    # Lcom/sec/mobileprint/print/apisdk/DuplexMode;
    .param p4, "chromaticity"    # Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .param p5, "contentType"    # Lcom/sec/mobileprint/print/apisdk/ContentType;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    .line 31
    iput-object p4, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 32
    iput-object p3, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .line 33
    iput-object p5, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    .line 34
    iput-object p2, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 35
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/mobileprint/print/apisdk/MediaType;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setPrintToFile(Z)V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaSize;->MEDIA_SIZE_A4:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 161
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->DUPLEX_MODE_SIMPLEX:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .line 162
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ColorMode;->COLOR_MODE_MONO:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 163
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    .line 164
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/ContentType;->PRINT_CONTENT_TYPE_PHOTO:Lcom/sec/mobileprint/print/apisdk/ContentType;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    .line 165
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/MediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/mobileprint/print/apisdk/MediaType;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    .line 166
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setPrintToFile(Z)V

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->setOutputFilePath(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->readFromParcel(Landroid/os/Parcel;)V

    .line 169
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    return-object v0
.end method

.method public getContentType()Lcom/sec/mobileprint/print/apisdk/ContentType;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    return-object v0
.end method

.method public getCopies()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    return v0
.end method

.method public getDuplex()Lcom/sec/mobileprint/print/apisdk/DuplexMode;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    return-object v0
.end method

.method public getMediaSize()Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    return-object v0
.end method

.method public getMediaType()Lcom/sec/mobileprint/print/apisdk/MediaType;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    return-object v0
.end method

.method public getOutputFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mOutputFileName:Ljava/lang/String;

    return-object v0
.end method

.method public isPrintToFile()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mPrintToFile:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->readFromParcel(Landroid/os/Parcel;)V

    .line 140
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/MediaType;->readFromParcel(Landroid/os/Parcel;)V

    .line 141
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->readFromParcel(Landroid/os/Parcel;)V

    .line 142
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/ColorMode;->readFromParcel(Landroid/os/Parcel;)V

    .line 143
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/ContentType;->readFromParcel(Landroid/os/Parcel;)V

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mPrintToFile:Z

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mOutputFileName:Ljava/lang/String;

    .line 147
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setColor(Lcom/sec/mobileprint/print/apisdk/ColorMode;)V
    .locals 0
    .param p1, "mColor"    # Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 96
    return-void
.end method

.method public setContentType(Lcom/sec/mobileprint/print/apisdk/ContentType;)V
    .locals 0
    .param p1, "mContentType"    # Lcom/sec/mobileprint/print/apisdk/ContentType;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    .line 106
    return-void
.end method

.method public setCopies(I)V
    .locals 0
    .param p1, "mCopies"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    .line 116
    return-void
.end method

.method public setDuplex(Lcom/sec/mobileprint/print/apisdk/DuplexMode;)V
    .locals 0
    .param p1, "mDuplex"    # Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .line 86
    return-void
.end method

.method public setMediaSize(Lcom/sec/mobileprint/print/apisdk/MediaSize;)V
    .locals 0
    .param p1, "mSelectedMediaSize"    # Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 66
    return-void
.end method

.method public setMediaType(Lcom/sec/mobileprint/print/apisdk/MediaType;)V
    .locals 0
    .param p1, "mMediaType"    # Lcom/sec/mobileprint/print/apisdk/MediaType;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    .line 76
    return-void
.end method

.method public setOutputFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "mOutputFileName"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mOutputFileName:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setPrintToFile(Z)V
    .locals 0
    .param p1, "mPrintToFile"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mPrintToFile:Z

    .line 47
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x0

    .line 128
    iget-object v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaSize:Lcom/sec/mobileprint/print/apisdk/MediaSize;

    invoke-virtual {v1, p1, v0}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->writeToParcel(Landroid/os/Parcel;I)V

    .line 129
    iget-object v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mMediaType:Lcom/sec/mobileprint/print/apisdk/MediaType;

    invoke-virtual {v1, p1, v0}, Lcom/sec/mobileprint/print/apisdk/MediaType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 130
    iget-object v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mDuplex:Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    invoke-virtual {v1, p1, v0}, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 131
    iget-object v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mColor:Lcom/sec/mobileprint/print/apisdk/ColorMode;

    invoke-virtual {v1, p1, v0}, Lcom/sec/mobileprint/print/apisdk/ColorMode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 132
    iget-object v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mContentType:Lcom/sec/mobileprint/print/apisdk/ContentType;

    invoke-virtual {v1, p1, v0}, Lcom/sec/mobileprint/print/apisdk/ContentType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 133
    iget v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mCopies:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-boolean v1, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mPrintToFile:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 135
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;->mOutputFileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    return-void
.end method
