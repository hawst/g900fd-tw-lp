.class Lcom/sec/mobileprint/print/apisdk/PrintSettings$1;
.super Ljava/lang/Object;
.source "PrintSettings.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/print/apisdk/PrintSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/mobileprint/print/apisdk/PrintSettings;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .locals 1
    .param p1, "pc"    # Landroid/os/Parcel;

    .prologue
    .line 152
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    invoke-direct {v0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 155
    new-array v0, p1, [Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintSettings$1;->newArray(I)[Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    move-result-object v0

    return-object v0
.end method
