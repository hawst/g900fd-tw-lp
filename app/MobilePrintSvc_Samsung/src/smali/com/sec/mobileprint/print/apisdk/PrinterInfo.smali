.class public Lcom/sec/mobileprint/print/apisdk/PrinterInfo;
.super Ljava/lang/Object;
.source "PrinterInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIPAddress:Ljava/lang/String;

.field private mPrinterName:Ljava/lang/String;

.field private mPrinterStatus:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

.field private mSupportedColorModes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
            ">;"
        }
    .end annotation
.end field

.field private mSupportedDuplexModes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/DuplexMode;",
            ">;"
        }
    .end annotation
.end field

.field private mSupportedMediaSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
            ">;"
        }
    .end annotation
.end field

.field private mSupportedMediaTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo$1;

    invoke-direct {v0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo$1;-><init>()V

    sput-object v0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 111
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    .line 26
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_IDLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "pc"    # Landroid/os/Parcel;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 116
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "pc"    # Landroid/os/Parcel;

    .prologue
    .line 120
    const-string v0, "NaveenDebug"

    const-string v1, "reading from parcel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    .line 125
    sget-object v0, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->PRINTER_STATUS_IDLE:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    .line 130
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 131
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/mobileprint/print/apisdk/ColorMode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 132
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/mobileprint/print/apisdk/MediaSize;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 133
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    sget-object v1, Lcom/sec/mobileprint/print/apisdk/MediaType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 134
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterStatus:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v0, p1}, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 136
    const-string v0, "NaveenDebug"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "read from parcel :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinterStatus()Lcom/sec/mobileprint/print/apisdk/PrinterStatus;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterStatus:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    return-object v0
.end method

.method public getSupportedColorModes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSupportedDuplexModes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/DuplexMode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSupportedMediaSizes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSupportedMediaTypes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setIPAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIPAddress"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setPrinterName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPrinterName"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setPrinterStatus(Lcom/sec/mobileprint/print/apisdk/PrinterStatus;)V
    .locals 0
    .param p1, "mPrinterStatus"    # Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterStatus:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    .line 82
    return-void
.end method

.method public setSupportedColorModes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/ColorMode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "mSupportsColorModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/ColorMode;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    .line 64
    return-void
.end method

.method public setSupportedDuplexModes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/DuplexMode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "mSupportedDuplexModes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/DuplexMode;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    .line 58
    return-void
.end method

.method public setSupportedMediaSizes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaSize;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "mSupportedMediaSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaSize;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    .line 70
    return-void
.end method

.method public setSupportedMediaTypes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/MediaType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "mSupportedMediatype":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/mobileprint/print/apisdk/MediaType;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    .line 76
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 143
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 146
    const-string v1, ""

    .line 147
    .local v1, "duplexString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 150
    const-string v0, ""

    .line 151
    .local v0, "colorString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 155
    const-string v2, ""

    .line 156
    .local v2, "mediaSizeString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 160
    const-string v3, ""

    .line 161
    .local v3, "mediaTypeString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 165
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Printer Name: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 166
    const-string v6, "Printer IP Address: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 167
    const-string v6, "Duplex Modes: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 168
    const-string v6, "Color Modes: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 169
    const-string v6, "Media Size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 170
    const-string v6, "Media Type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 165
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 172
    .local v4, "str":Ljava/lang/String;
    return-object v4

    .line 147
    .end local v0    # "colorString":Ljava/lang/String;
    .end local v2    # "mediaSizeString":Ljava/lang/String;
    .end local v3    # "mediaTypeString":Ljava/lang/String;
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/print/apisdk/DuplexMode;

    .line 148
    .local v4, "str":Lcom/sec/mobileprint/print/apisdk/DuplexMode;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/mobileprint/print/apisdk/DuplexMode;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 151
    .end local v4    # "str":Lcom/sec/mobileprint/print/apisdk/DuplexMode;
    .restart local v0    # "colorString":Ljava/lang/String;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/print/apisdk/ColorMode;

    .line 152
    .local v4, "str":Lcom/sec/mobileprint/print/apisdk/ColorMode;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/mobileprint/print/apisdk/ColorMode;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 156
    .end local v4    # "str":Lcom/sec/mobileprint/print/apisdk/ColorMode;
    .restart local v2    # "mediaSizeString":Ljava/lang/String;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/print/apisdk/MediaSize;

    .line 157
    .local v4, "str":Lcom/sec/mobileprint/print/apisdk/MediaSize;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/mobileprint/print/apisdk/MediaSize;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 161
    .end local v4    # "str":Lcom/sec/mobileprint/print/apisdk/MediaSize;
    .restart local v3    # "mediaTypeString":Ljava/lang/String;
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/mobileprint/print/apisdk/MediaType;

    .line 162
    .local v4, "str":Lcom/sec/mobileprint/print/apisdk/MediaType;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/mobileprint/print/apisdk/MediaType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 91
    const-string v0, "NaveenDebug"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "writing to parcel :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mIPAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedDuplexModes:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 95
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedColorModes:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 96
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaSizes:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 97
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mSupportedMediaTypes:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 98
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->mPrinterStatus:Lcom/sec/mobileprint/print/apisdk/PrinterStatus;

    invoke-virtual {v0, p1, p2}, Lcom/sec/mobileprint/print/apisdk/PrinterStatus;->writeToParcel(Landroid/os/Parcel;I)V

    .line 99
    const-string v0, "NaveenDebug"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrote to parcel :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/mobileprint/print/apisdk/PrinterInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-void
.end method
