.class Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;
.super Ljava/lang/Object;
.source "PrintJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/mobileprint/print/apisdk/PrintJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "JobStatus"
.end annotation


# instance fields
.field private mStatusType:I

.field private mStatusValue:I

.field final synthetic this$0:Lcom/sec/mobileprint/print/apisdk/PrintJob;


# direct methods
.method constructor <init>(Lcom/sec/mobileprint/print/apisdk/PrintJob;)V
    .locals 1

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->this$0:Lcom/sec/mobileprint/print/apisdk/PrintJob;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    .line 124
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    .line 125
    return-void
.end method

.method static synthetic access$0(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V
    .locals 0

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I

    return-void
.end method


# virtual methods
.method public getStatusType()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    return v0
.end method

.method public getStatusValue()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I

    return v0
.end method

.method public setStatusType(I)V
    .locals 0
    .param p1, "mStatusType"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I

    .line 132
    return-void
.end method

.method public setStatusValue(I)V
    .locals 0
    .param p1, "mStatusValue"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I

    .line 138
    return-void
.end method
