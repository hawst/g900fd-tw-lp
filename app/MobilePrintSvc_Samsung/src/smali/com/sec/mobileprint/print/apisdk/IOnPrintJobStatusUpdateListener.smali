.class public interface abstract Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener;
.super Ljava/lang/Object;
.source "IOnPrintJobStatusUpdateListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/IOnPrintJobStatusUpdateListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onPrintJobStatusUpdate(III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
