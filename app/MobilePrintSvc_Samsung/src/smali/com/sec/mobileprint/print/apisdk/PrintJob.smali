.class public Lcom/sec/mobileprint/print/apisdk/PrintJob;
.super Ljava/lang/Object;
.source "PrintJob.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/mobileprint/print/apisdk/PrintJob;",
            ">;"
        }
    .end annotation
.end field

.field public static final PRINT_JOB_ERROR_STATUS_INVALID_CONTENTS:I = 0x7

.field public static final PRINT_JOB_ERROR_STATUS_NOT_SUPPORTED_CONTENTS:I = 0x3

.field public static final PRINT_JOB_ERROR_STATUS_NOT_SUPPORTED_PRINTER:I = 0x6

.field public static final PRINT_JOB_ERROR_STATUS_OUTPUT_STREAM_ERROR:I = 0x4

.field public static final PRINT_JOB_ERROR_STATUS_OUT_OF_MEMEORY:I = 0x1

.field public static final PRINT_JOB_ERROR_STATUS_PRINTING_OPTION_ERROR:I = 0x2

.field public static final PRINT_JOB_ERROR_STATUS_PRINT_SERVICE_STILL_RUNNING:I = 0x5

.field public static final PRINT_JOB_STATUS_CANCELED:I = 0x6

.field public static final PRINT_JOB_STATUS_COMPLETED_JOB:I = 0x5

.field public static final PRINT_JOB_STATUS_COMPLETED_PAGE:I = 0x4

.field public static final PRINT_JOB_STATUS_ERROR:I = 0x7

.field public static final PRINT_JOB_STATUS_PREPARING:I = 0x0

.field public static final PRINT_JOB_STATUS_PRINTING:I = 0x3

.field public static final PRINT_JOB_STATUS_QUEUED:I = -0x1

.field public static final PRINT_JOB_STATUS_START_JOB:I = 0x1

.field public static final PRINT_JOB_STATUS_START_PAGE:I = 0x2


# instance fields
.field private mFilePathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIPAddress:Ljava/lang/String;

.field private mJobId:I

.field private mPortNo:I

.field private mSettings:Lcom/sec/mobileprint/print/apisdk/PrintSettings;

.field private mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob$1;

    invoke-direct {v0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$1;-><init>()V

    sput-object v0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 180
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    .line 184
    invoke-virtual {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->readFromParcel(Landroid/os/Parcel;)V

    .line 185
    return-void
.end method

.method private constructor <init>(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mSettings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 45
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setPortNo(I)V

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)V

    .line 47
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    .line 49
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    .line 51
    return-void
.end method

.method private constructor <init>(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "mSettings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/mobileprint/print/apisdk/PrintSettings;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "mFilePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-direct {p0, p3}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setFilePaths(Ljava/util/ArrayList;)V

    .line 37
    invoke-direct {p0, p2}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setIPAddress(Ljava/lang/String;)V

    .line 38
    const/16 v0, 0x238c

    invoke-virtual {p0, v0}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setPortNo(I)V

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob;->setPrintJobSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)V

    .line 40
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    .line 41
    return-void
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .locals 1
    .param p0, "mSettings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 58
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/mobileprint/print/apisdk/PrintJob;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPrintJob(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/sec/mobileprint/print/apisdk/PrintJob;
    .locals 1
    .param p0, "mSettings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .param p1, "ipAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/mobileprint/print/apisdk/PrintSettings;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/mobileprint/print/apisdk/PrintJob;"
        }
    .end annotation

    .prologue
    .line 54
    .local p2, "mFilePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/mobileprint/print/apisdk/PrintJob;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintSettings;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method private setFilePaths(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "mFilePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    .line 91
    return-void
.end method

.method private setIPAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIPAddress"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mIPAddress:Ljava/lang/String;

    .line 83
    return-void
.end method

.method private setPrintJobSettings(Lcom/sec/mobileprint/print/apisdk/PrintSettings;)V
    .locals 0
    .param p1, "mSettings"    # Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mSettings:Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    .line 75
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public getFilePaths()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mIPAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getJobId()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mJobId:I

    return v0
.end method

.method public getJobStatus()Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    return-object v0
.end method

.method public getPortNo()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mPortNo:I

    return v0
.end method

.method public getPrintJobSettings()Lcom/sec/mobileprint/print/apisdk/PrintSettings;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mSettings:Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 162
    const-class v0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mSettings:Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mJobId:I

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mIPAddress:Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mPortNo:I

    .line 166
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 167
    new-instance v0, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-direct {v0, p0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;-><init>(Lcom/sec/mobileprint/print/apisdk/PrintJob;)V

    iput-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    .line 168
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$1(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 169
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$3(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 170
    return-void
.end method

.method public setJobId(I)V
    .locals 0
    .param p1, "mJobId"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mJobId:I

    .line 67
    return-void
.end method

.method public setJobStatus(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-static {v0, p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$1(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 112
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    invoke-static {v0, p2}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$3(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 113
    return-void
.end method

.method public setJobStatus(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    # getter for: Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I
    invoke-static {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$0(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$1(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 107
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    # getter for: Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I
    invoke-static {p1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$2(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$3(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;I)V

    .line 108
    return-void
.end method

.method public setPortNo(I)V
    .locals 0
    .param p1, "mPortNo"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mPortNo:I

    .line 99
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mSettings:Lcom/sec/mobileprint/print/apisdk/PrintSettings;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 153
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mJobId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mIPAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    iget v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mPortNo:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mFilePathList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 157
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    # getter for: Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusType:I
    invoke-static {v0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$0(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/mobileprint/print/apisdk/PrintJob;->mStatus:Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;

    # getter for: Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->mStatusValue:I
    invoke-static {v0}, Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;->access$2(Lcom/sec/mobileprint/print/apisdk/PrintJob$JobStatus;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    return-void
.end method
