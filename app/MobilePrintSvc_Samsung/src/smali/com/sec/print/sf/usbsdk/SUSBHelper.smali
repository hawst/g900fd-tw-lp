.class final Lcom/sec/print/sf/usbsdk/SUSBHelper;
.super Ljava/lang/Object;
.source "SUSBHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;,
        Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;
    }
.end annotation


# static fields
.field private static final CFG_WAIT_TIMEOUT:I = 0x3e8


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDeviceId(Lcom/sec/print/sf/usbsdk/SUSBDevice;BBB[BI)I
    .locals 9
    .param p0, "dev"    # Lcom/sec/print/sf/usbsdk/SUSBDevice;
    .param p1, "configuration_value"    # B
    .param p2, "interface_number"    # B
    .param p3, "alt_setting"    # B
    .param p4, "data"    # [B
    .param p5, "length"    # I

    .prologue
    const/4 v2, 0x0

    .line 213
    new-instance v0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;

    invoke-direct {v0, p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;-><init>(Lcom/sec/print/sf/usbsdk/SUSBDevice;)V

    .line 214
    .local v0, "control":Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;
    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->open()I

    move-result v8

    .line 215
    .local v8, "result":I
    if-eqz v8, :cond_1

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBHelper::getDeviceId: GetDeviceId: can\'t open device control"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 217
    const/4 v2, -0x6

    .line 228
    :cond_0
    :goto_0
    return v2

    .line 220
    :cond_1
    const/16 v1, 0xa1

    add-int/lit8 v3, p1, -0x1

    shl-int/lit8 v4, p2, 0x8

    or-int/2addr v4, p3

    const/16 v7, 0x3e8

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->controlTransfer(IIII[BII)I

    move-result v8

    .line 221
    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->close()I

    .line 223
    if-gez v8, :cond_0

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBHelper::getDeviceId: GetDeviceId: controlTransfer failed with code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    move v2, v8

    .line 225
    goto :goto_0
.end method

.method static getDeviceId(Lcom/sec/print/sf/usbsdk/SUSBDevice;BBB)Ljava/lang/String;
    .locals 11
    .param p0, "dev"    # Lcom/sec/print/sf/usbsdk/SUSBDevice;
    .param p1, "configuration_value"    # B
    .param p2, "interface_number"    # B
    .param p3, "alt_setting"    # B

    .prologue
    .line 169
    const/16 v0, 0x100

    new-array v4, v0, [B

    .line 170
    .local v4, "buffer":[B
    array-length v5, v4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->getDeviceId(Lcom/sec/print/sf/usbsdk/SUSBDevice;BBB[BI)I

    move-result v7

    .line 172
    .local v7, "res":I
    if-nez v7, :cond_0

    .line 174
    const/4 v10, 0x0

    .line 175
    .local v10, "result_length":S
    const/4 v0, 0x0

    aget-byte v0, v4, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    int-to-short v10, v0

    .line 176
    const/4 v0, 0x1

    aget-byte v0, v4, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v10

    int-to-short v10, v0

    .line 177
    const/4 v8, 0x0

    .line 180
    .local v8, "result":Ljava/lang/String;
    :try_start_0
    new-instance v9, Ljava/lang/String;

    const/4 v0, 0x2

    add-int/lit8 v1, v10, -0x2

    invoke-static {v4, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-direct {v9, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    .end local v8    # "result":Ljava/lang/String;
    .local v9, "result":Ljava/lang/String;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUSBHelper::getDeviceId: GetDeviceId succeed: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v9

    .line 191
    .end local v9    # "result":Ljava/lang/String;
    .end local v10    # "result_length":S
    :goto_0
    return-object v8

    .line 182
    .restart local v8    # "result":Ljava/lang/String;
    .restart local v10    # "result_length":S
    :catch_0
    move-exception v6

    .line 183
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    const-string v0, "SUSBHelper::getDeviceId: UTF-8 encoding is not supported on device"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 190
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v8    # "result":Ljava/lang/String;
    .end local v10    # "result_length":S
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUSBHelper::getPrinterDetails: GetDeviceId: failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 191
    const/4 v8, 0x0

    goto :goto_0

    .line 182
    .restart local v9    # "result":Ljava/lang/String;
    .restart local v10    # "result_length":S
    :catch_1
    move-exception v6

    move-object v8, v9

    .end local v9    # "result":Ljava/lang/String;
    .restart local v8    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method static getPrinterDetails(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbInterface;)Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;
    .locals 4
    .param p0, "device"    # Landroid/hardware/usb/UsbDevice;
    .param p1, "printerInterface"    # Landroid/hardware/usb/UsbInterface;

    .prologue
    const/4 v2, 0x0

    .line 87
    if-nez p0, :cond_1

    move-object v1, v2

    .line 114
    :cond_0
    :goto_0
    return-object v1

    .line 90
    :cond_1
    invoke-static {p0}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->isDeviceClassPrinter(Landroid/hardware/usb/UsbDevice;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 91
    const-string v3, "SUSBHelper::getPrinterDetails: device is not a printer"

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move-object v1, v2

    .line 92
    goto :goto_0

    .line 95
    :cond_2
    new-instance v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;

    invoke-direct {v1}, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;-><init>()V

    .line 98
    .local v1, "result":Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;
    if-eqz p1, :cond_4

    .line 99
    iput-object p1, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 100
    invoke-static {p1}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->getPrinterInterfaceEndpoints(Landroid/hardware/usb/UsbInterface;)Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    .line 101
    iget-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    if-nez v3, :cond_0

    :cond_3
    move-object v1, v2

    .line 114
    goto :goto_0

    .line 104
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 105
    invoke-virtual {p0, v0}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 106
    iget-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v3, :cond_5

    .line 107
    iget-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->getPrinterInterfaceEndpoints(Landroid/hardware/usb/UsbInterface;)Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    .line 108
    iget-object v3, v1, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    if-nez v3, :cond_0

    .line 104
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static getPrinterInterfaceEndpoints(Landroid/hardware/usb/UsbInterface;)Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;
    .locals 8
    .param p0, "usbInterface"    # Landroid/hardware/usb/UsbInterface;

    .prologue
    .line 125
    if-eqz p0, :cond_4

    .line 126
    const/4 v2, 0x0

    .line 127
    .local v2, "epIn":Landroid/hardware/usb/UsbEndpoint;
    const/4 v3, 0x0

    .line 129
    .local v3, "epOut":Landroid/hardware/usb/UsbEndpoint;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    invoke-virtual {p0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v6

    if-lt v5, v6, :cond_0

    .line 143
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 144
    new-instance v4, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    invoke-direct {v4}, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;-><init>()V

    .line 145
    .local v4, "eps":Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;
    iput-object v2, v4, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;->defBulkInEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 146
    iput-object v3, v4, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;->defBulkOutEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 151
    .end local v2    # "epIn":Landroid/hardware/usb/UsbEndpoint;
    .end local v3    # "epOut":Landroid/hardware/usb/UsbEndpoint;
    .end local v4    # "eps":Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;
    .end local v5    # "j":I
    :goto_1
    return-object v4

    .line 130
    .restart local v2    # "epIn":Landroid/hardware/usb/UsbEndpoint;
    .restart local v3    # "epOut":Landroid/hardware/usb/UsbEndpoint;
    .restart local v5    # "j":I
    :cond_0
    invoke-virtual {p0, v5}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v1

    .line 131
    .local v1, "ep":Landroid/hardware/usb/UsbEndpoint;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 132
    invoke-virtual {v1}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v6

    const/16 v7, 0x80

    if-ne v6, v7, :cond_2

    const/4 v0, 0x1

    .line 133
    .local v0, "bEpDirIn":Z
    :goto_2
    if-eqz v0, :cond_3

    .line 134
    if-nez v2, :cond_1

    .line 135
    move-object v2, v1

    .line 129
    .end local v0    # "bEpDirIn":Z
    :cond_1
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 132
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 137
    .restart local v0    # "bEpDirIn":Z
    :cond_3
    if-nez v3, :cond_1

    .line 138
    move-object v3, v1

    goto :goto_3

    .line 151
    .end local v0    # "bEpDirIn":Z
    .end local v1    # "ep":Landroid/hardware/usb/UsbEndpoint;
    .end local v2    # "epIn":Landroid/hardware/usb/UsbEndpoint;
    .end local v3    # "epOut":Landroid/hardware/usb/UsbEndpoint;
    .end local v5    # "j":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static isDeviceClassPrinter(Landroid/hardware/usb/UsbDevice;)Z
    .locals 6
    .param p0, "device"    # Landroid/hardware/usb/UsbDevice;

    .prologue
    const/4 v5, 0x7

    const/4 v3, 0x1

    .line 64
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getDeviceClass()I

    move-result v0

    .line 65
    .local v0, "devClass":I
    if-ne v0, v5, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v3

    .line 68
    :cond_1
    if-nez v0, :cond_2

    .line 69
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v2

    .line 70
    .local v2, "interfaceCnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_3

    .line 76
    .end local v1    # "i":I
    .end local v2    # "interfaceCnt":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 71
    .restart local v1    # "i":I
    .restart local v2    # "interfaceCnt":I
    :cond_3
    invoke-virtual {p0, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v4

    if-eq v4, v5, :cond_0

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
