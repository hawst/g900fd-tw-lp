.class public Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;
.super Ljava/lang/Object;
.source "SAndroidUSBDevicePort.java"


# instance fields
.field private endpointIn:Landroid/hardware/usb/UsbEndpoint;

.field private endpointOut:Landroid/hardware/usb/UsbEndpoint;

.field private supportEPZ:Z

.field private usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

.field private usbInterface:Landroid/hardware/usb/UsbInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method

.method private clear()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_1
    :goto_0
    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 60
    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 61
    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointIn:Landroid/hardware/usb/UsbEndpoint;

    .line 62
    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointOut:Landroid/hardware/usb/UsbEndpoint;

    .line 63
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->supportEPZ:Z

    .line 65
    const-string v1, "Clear USB port"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 66
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()I
    .locals 1

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->clear()V

    .line 328
    const-string v0, "Close USB port"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getRawDescriptors()[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 309
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v1

    if-nez v1, :cond_0

    .line 310
    const-string v1, "Port is not opened"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 317
    :goto_0
    return-object v0

    .line 313
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-lt v1, v2, :cond_1

    .line 314
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->getRawDescriptors()[B

    move-result-object v0

    goto :goto_0

    .line 316
    :cond_1
    const-string v1, "API version 13 or higher need to be used"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    const-string v0, "Port is not opened"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 297
    const/4 v0, 0x0

    .line 300
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->getSerial()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized open(IIZZ)I
    .locals 12
    .param p1, "vid"    # I
    .param p2, "pid"    # I
    .param p3, "openForBulk"    # Z
    .param p4, "openForEPZ"    # Z

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 87
    const-string v9, "Port is opened alread"

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    const/4 v9, -0x2

    .line 167
    :goto_0
    monitor-exit p0

    return v9

    .line 91
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v9

    if-nez v9, :cond_1

    .line 92
    const-string v9, "USBSDK is not initialized"

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 93
    const/4 v9, -0x2

    goto :goto_0

    .line 97
    :cond_1
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->findDevice(II)Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    .line 98
    .local v0, "device":Landroid/hardware/usb/UsbDevice;
    if-nez v0, :cond_2

    .line 99
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Cannot find device with vid="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 100
    const/4 v9, -0x4

    goto :goto_0

    .line 105
    :cond_2
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->requestPermissionForDevice(Landroid/hardware/usb/UsbDevice;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 106
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "No permissions to access device with vid="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 107
    const/4 v9, -0x4

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v6

    .line 112
    .local v6, "interfaceCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-lt v5, v6, :cond_4

    .line 146
    :goto_2
    iget-object v9, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    if-nez v9, :cond_b

    .line 148
    const-string v9, "Suitable interface was not found!"

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 149
    const/4 v9, -0x4

    goto :goto_0

    .line 113
    :cond_4
    invoke-virtual {v0, v5}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v8

    .line 114
    .local v8, "usbInterface":Landroid/hardware/usb/UsbInterface;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Check interface "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 116
    const/4 v1, 0x0

    .line 117
    .local v1, "endpoint_IN":Landroid/hardware/usb/UsbEndpoint;
    const/4 v2, 0x0

    .line 119
    .local v2, "endpoint_OUT":Landroid/hardware/usb/UsbEndpoint;
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_3
    invoke-virtual {v8}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v9

    if-lt v7, v9, :cond_7

    .line 131
    if-eqz v1, :cond_a

    if-eqz v2, :cond_a

    .line 132
    if-eqz p3, :cond_5

    .line 133
    iput-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointIn:Landroid/hardware/usb/UsbEndpoint;

    .line 134
    iput-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointOut:Landroid/hardware/usb/UsbEndpoint;

    .line 137
    :cond_5
    if-eqz p4, :cond_6

    .line 138
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->supportEPZ:Z

    .line 140
    :cond_6
    iput-object v8, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 141
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Select interface "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 86
    .end local v0    # "device":Landroid/hardware/usb/UsbDevice;
    .end local v1    # "endpoint_IN":Landroid/hardware/usb/UsbEndpoint;
    .end local v2    # "endpoint_OUT":Landroid/hardware/usb/UsbEndpoint;
    .end local v5    # "i":I
    .end local v6    # "interfaceCount":I
    .end local v7    # "j":I
    .end local v8    # "usbInterface":Landroid/hardware/usb/UsbInterface;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 120
    .restart local v0    # "device":Landroid/hardware/usb/UsbDevice;
    .restart local v1    # "endpoint_IN":Landroid/hardware/usb/UsbEndpoint;
    .restart local v2    # "endpoint_OUT":Landroid/hardware/usb/UsbEndpoint;
    .restart local v5    # "i":I
    .restart local v6    # "interfaceCount":I
    .restart local v7    # "j":I
    .restart local v8    # "usbInterface":Landroid/hardware/usb/UsbInterface;
    :cond_7
    :try_start_2
    invoke-virtual {v8, v7}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    .line 121
    .local v3, "ep":Landroid/hardware/usb/UsbEndpoint;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Check endpoint "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_8

    .line 123
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v9

    if-nez v9, :cond_9

    .line 124
    move-object v2, v3

    .line 119
    :cond_8
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 125
    :cond_9
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v9

    const/16 v10, 0x80

    if-ne v9, v10, :cond_8

    .line 126
    move-object v1, v3

    goto :goto_4

    .line 112
    .end local v3    # "ep":Landroid/hardware/usb/UsbEndpoint;
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 152
    .end local v1    # "endpoint_IN":Landroid/hardware/usb/UsbEndpoint;
    .end local v2    # "endpoint_OUT":Landroid/hardware/usb/UsbEndpoint;
    .end local v7    # "j":I
    .end local v8    # "usbInterface":Landroid/hardware/usb/UsbInterface;
    :cond_b
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 154
    iget-object v9, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v9, :cond_c

    .line 155
    const-string v9, "Cannot create connection!"

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 156
    invoke-direct {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->clear()V

    .line 157
    const/4 v9, -0x4

    goto/16 :goto_0

    .line 160
    :cond_c
    const/4 v4, 0x1

    .line 161
    .local v4, "force":Z
    iget-object v9, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v10, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v9

    if-nez v9, :cond_d

    .line 162
    const-string v9, "Cannot claim connection!"

    invoke-static {v9}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 163
    invoke-direct {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 164
    const/4 v9, -0x4

    goto/16 :goto_0

    .line 167
    :cond_d
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public declared-synchronized read([BII)I
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "dataSizeForRead"    # I
    .param p3, "timeout"    # I

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v2

    if-nez v2, :cond_0

    .line 225
    const-string v2, "Port is not opened"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    const/4 v1, -0x2

    .line 246
    :goto_0
    monitor-exit p0

    return v1

    .line 229
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointIn:Landroid/hardware/usb/UsbEndpoint;

    if-nez v2, :cond_1

    .line 230
    const-string v2, "Port is not opened for bulk read"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    const/4 v1, -0x3

    goto :goto_0

    .line 235
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointIn:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v2, v3, p1, p2, p3}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    .line 236
    .local v1, "result":I
    if-ltz v1, :cond_2

    .line 237
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Data was read from port, buffer len = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " required size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " processed size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 244
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 246
    const/4 v1, -0x1

    goto :goto_0

    .line 241
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v1    # "result":I
    :cond_2
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot read data from USB, err="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 242
    const/4 v1, -0x6

    goto :goto_0

    .line 224
    .end local v1    # "result":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized requestOnEndpointZero(IIII[BII)I
    .locals 11
    .param p1, "bitmapRequestType"    # I
    .param p2, "requestCode"    # I
    .param p3, "value"    # I
    .param p4, "index"    # I
    .param p5, "buffer"    # [B
    .param p6, "dataSizeForRead"    # I
    .param p7, "timeout"    # I

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    const-string v1, "Port is not opened"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    const/4 v10, -0x2

    .line 285
    :goto_0
    monitor-exit p0

    return v10

    .line 268
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->supportEPZ:Z

    if-nez v1, :cond_1

    .line 269
    const-string v1, "Port is not opened for EP0 read"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    const/4 v10, -0x2

    goto :goto_0

    .line 274
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v10

    .line 275
    .local v10, "result":I
    if-ltz v10, :cond_2

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Data was read from port via EP0, buffer len = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " required size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processed size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 283
    .end local v10    # "result":I
    :catch_0
    move-exception v9

    .line 284
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 285
    const/4 v10, -0x1

    goto :goto_0

    .line 280
    .end local v9    # "e":Ljava/lang/RuntimeException;
    .restart local v10    # "result":I
    :cond_2
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot read data from USB via EP0, err="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 281
    const/4 v10, -0x6

    goto :goto_0

    .line 263
    .end local v10    # "result":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized write([BII)I
    .locals 4
    .param p1, "data"    # [B
    .param p2, "dataSizeToWrite"    # I
    .param p3, "timeout"    # I

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v2

    if-nez v2, :cond_0

    .line 190
    const-string v2, "Port is not opened"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    const/4 v1, -0x2

    .line 211
    :goto_0
    monitor-exit p0

    return v1

    .line 194
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointOut:Landroid/hardware/usb/UsbEndpoint;

    if-nez v2, :cond_1

    .line 195
    const-string v2, "Port is not opened for bulk write"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    const/4 v1, -0x3

    goto :goto_0

    .line 201
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->usbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->endpointOut:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v2, v3, p1, p2, p3}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    .line 202
    .local v1, "result":I
    if-ltz v1, :cond_2

    .line 203
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Data was written to port, buffer len = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " required size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " processed size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 209
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 211
    const/4 v1, -0x1

    goto :goto_0

    .line 206
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v1    # "result":I
    :cond_2
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot write data to USB, err="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 207
    const/4 v1, -0x6

    goto :goto_0

    .line 189
    .end local v1    # "result":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
