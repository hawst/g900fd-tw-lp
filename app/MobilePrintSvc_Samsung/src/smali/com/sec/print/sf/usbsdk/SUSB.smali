.class public final Lcom/sec/print/sf/usbsdk/SUSB;
.super Ljava/lang/Object;
.source "SUSB.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static Deinitialize()V
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->deinitialize()V

    .line 62
    :cond_0
    return-void
.end method

.method public static Initialize(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 39
    if-nez p0, :cond_0

    .line 40
    const-string v2, "SUSB::initialize: context is null"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 51
    :goto_0
    return v1

    .line 44
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 45
    .local v0, "currentapiVersion":I
    const/16 v2, 0xc

    if-lt v0, v2, :cond_1

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.usb.host"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 48
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->initialize(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0

    .line 50
    :cond_1
    const-string v2, "SUSB::initialize: USB is not supported"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    goto :goto_0
.end method
