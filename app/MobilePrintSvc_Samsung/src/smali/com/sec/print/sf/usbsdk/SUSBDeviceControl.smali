.class Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;
.super Ljava/lang/Object;
.source "SUSBDeviceControl.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

.field private isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/sec/print/sf/usbsdk/SUSBDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/sec/print/sf/usbsdk/SUSBDevice;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    .line 49
    return-void
.end method

.method static isRequestWrite(I)Z
    .locals 1
    .param p0, "request"    # I

    .prologue
    .line 131
    and-int/lit16 v0, p0, 0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method close()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 91
    const-string v1, "SUSBDeviceControl::close"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->closeControl()V

    .line 95
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method controlTransfer(IIII[BII)I
    .locals 11
    .param p1, "bitmapRequestType"    # I
    .param p2, "requestCode"    # I
    .param p3, "value"    # I
    .param p4, "index"    # I
    .param p5, "buffer"    # [B
    .param p6, "dataSizeForRead"    # I
    .param p7, "timeout"    # I

    .prologue
    .line 156
    const-string v1, "SUSBDeviceControl::controlTransfer"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened()Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    const-string v1, "SUSBDeviceControl::controlTransfer: Device control is not opened"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 160
    const/4 v10, -0x2

    .line 180
    :goto_0
    return v10

    .line 163
    :cond_0
    if-nez p6, :cond_1

    .line 164
    const/4 v10, 0x0

    goto :goto_0

    .line 167
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v10

    .line 168
    .local v10, "result":I
    if-ltz v10, :cond_2

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBDeviceControl::controlTransfer: Data was read from port via EP0, buffer len = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " required size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processed size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v10    # "result":I
    :catch_0
    move-exception v9

    .line 178
    .local v9, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBDeviceControl::controlTransfer: RuntimeException was thrown: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 180
    const/4 v10, -0x1

    goto :goto_0

    .line 173
    .end local v9    # "e":Ljava/lang/RuntimeException;
    .restart local v10    # "result":I
    :cond_2
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBDeviceControl::controlTransfer: Cannot read data from USB via EP0, err="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 175
    const/4 v10, -0x6

    goto :goto_0
.end method

.method isOpened()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method open()I
    .locals 2

    .prologue
    const/4 v0, -0x2

    .line 60
    const-string v1, "SUSBDeviceControl::open"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    :cond_0
    const-string v1, "SUSBDeviceControl::open: Device control can\'t be opened because it\'s already opened or device is not opened or empty"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 73
    :goto_0
    return v0

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isControlOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    const-string v1, "SUSBDeviceControl::open: Device control is already opened for this device"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 72
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceControl;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->openControl()V

    .line 73
    const/4 v0, 0x0

    goto :goto_0
.end method

.method reset()I
    .locals 1

    .prologue
    .line 108
    const-string v0, "SUSBDeviceControl::reset: Device reset not supported"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 109
    const/4 v0, -0x3

    return v0
.end method

.method setActiveConfiguration(B)I
    .locals 1
    .param p1, "configuration_value"    # B

    .prologue
    .line 120
    const-string v0, "SUSBDeviceControl::setActiveConfiguration: Device setActiveConfiguration not supported"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 121
    const/4 v0, -0x3

    return v0
.end method
