.class Lcom/sec/print/sf/usbsdk/SUSBDevice;
.super Ljava/lang/Object;
.source "SUSBDevice.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private connection:Landroid/hardware/usb/UsbDeviceConnection;

.field private controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private device:Landroid/hardware/usb/UsbDevice;

.field private deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

.field private interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    .line 64
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 54
    new-instance v0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    invoke-direct {v0, p1}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    .line 55
    return-void
.end method


# virtual methods
.method close()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 206
    const-string v1, "SUSBDevice::close"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isOpened()Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    const-string v1, "SUSBDevice::close: Can\'t close device: closed already"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 211
    const/4 v1, -0x2

    .line 229
    :goto_0
    return v1

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isInterfaceOpened()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isControlOpened()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 215
    :cond_1
    const-string v1, "SUSBDevice::close: Can\'t close device: interface or control is opened"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 218
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v1, :cond_3

    .line 219
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_3
    iput-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 228
    iput-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    .line 229
    const/4 v1, 0x0

    goto :goto_0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SUSBDevice::close: RuntimeException was thrown: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 224
    const/4 v1, -0x1

    goto :goto_0
.end method

.method closeControl()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 158
    return-void
.end method

.method closeInterface()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 181
    return-void
.end method

.method getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    return-object v0
.end method

.method getDeviceImpl()Landroid/hardware/usb/UsbDevice;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    return-object v0
.end method

.method public isControlOpened()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method isInterfaceOpened()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method isOpened()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method open()I
    .locals 4

    .prologue
    const/4 v1, -0x2

    const/4 v2, -0x4

    .line 92
    const-string v3, "SUSBDevice::open"

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isOpened()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    const-string v2, "SUSBDevice::open: Device is already opened"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 134
    :goto_0
    return v1

    .line 99
    :cond_0
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v3

    if-nez v3, :cond_1

    .line 100
    const-string v2, "SUSBDevice::open: USBSDK is not initialized"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->isOperable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 105
    const-string v2, "SUSBDevice::open: Device info is empty or device is not operable"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager()Landroid/hardware/usb/UsbManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    .line 112
    .local v0, "devList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    invoke-virtual {v1}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    iput-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    .line 113
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    if-nez v1, :cond_3

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDevice::open: Cannot find device with name="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move v1, v2

    .line 115
    goto :goto_0

    .line 119
    :cond_3
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1, v3}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->requestPermissionForDevice(Landroid/hardware/usb/UsbDevice;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDevice::open: No permissions to access device with name="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->deviceInfo:Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move v1, v2

    .line 121
    goto :goto_0

    .line 125
    :cond_4
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1, v3}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 127
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v1, :cond_5

    .line 128
    const-string v1, "SUSBDevice::open: Cannot create connection!"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 129
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->device:Landroid/hardware/usb/UsbDevice;

    move v1, v2

    .line 131
    goto/16 :goto_0

    .line 134
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method openControl()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->controlOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 151
    return-void
.end method

.method openInterface()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDevice;->interfaceOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 174
    return-void
.end method
