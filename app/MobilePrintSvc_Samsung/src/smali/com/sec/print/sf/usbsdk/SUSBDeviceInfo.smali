.class Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
.super Ljava/lang/Object;
.source "SUSBDeviceInfo.java"


# instance fields
.field private deviceId:Ljava/lang/String;

.field private modelName:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private numConfigurations:I

.field private operable:Z

.field private productId:I

.field private serialNumber:Ljava/lang/String;

.field private vendorId:I

.field private vendorName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->name:Ljava/lang/String;

    .line 60
    return-void
.end method

.method constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "vid"    # I
    .param p3, "pid"    # I
    .param p4, "deviceID"    # Ljava/lang/String;
    .param p5, "serialNumber"    # Ljava/lang/String;
    .param p6, "modelName"    # Ljava/lang/String;
    .param p7, "vendorName"    # Ljava/lang/String;
    .param p8, "numConfigs"    # I
    .param p9, "operable"    # Z

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->name:Ljava/lang/String;

    .line 86
    iput p2, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->vendorId:I

    .line 87
    iput p3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->productId:I

    .line 88
    iput-object p4, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->deviceId:Ljava/lang/String;

    .line 89
    iput-object p5, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->serialNumber:Ljava/lang/String;

    .line 90
    iput-object p6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->modelName:Ljava/lang/String;

    .line 91
    iput-object p7, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->vendorName:Ljava/lang/String;

    .line 92
    iput p8, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->numConfigurations:I

    .line 93
    iput-boolean p9, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->operable:Z

    .line 94
    return-void
.end method


# virtual methods
.method getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method getNumConfigurations()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->numConfigurations:I

    return v0
.end method

.method getProductID()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->productId:I

    return v0
.end method

.method getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method getVendorID()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->vendorId:I

    return v0
.end method

.method getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->vendorName:Ljava/lang/String;

    return-object v0
.end method

.method isOperable()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;->operable:Z

    return v0
.end method
