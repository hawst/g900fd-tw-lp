.class final Lcom/sec/print/sf/usbsdk/SUSBMngr;
.super Ljava/lang/Object;
.source "SUSBMngr.java"


# static fields
.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.sec.print.sf.usbsdk.USB_PERMISSION"

.field static volatile instance:Lcom/sec/print/sf/usbsdk/SUSBMngr;


# instance fields
.field private volatile lock:Ljava/lang/Object;

.field private final m_USBReceiver:Landroid/content/BroadcastReceiver;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation
.end field

.field private parentContext:Landroid/content/Context;

.field private permissionIntent:Landroid/app/PendingIntent;

.field private tmpDevice:Landroid/hardware/usb/UsbDevice;

.field private usbManager:Landroid/hardware/usb/UsbManager;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;

    .line 232
    new-instance v0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;

    invoke-direct {v0, p0}, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;-><init>(Lcom/sec/print/sf/usbsdk/SUSBMngr;)V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    .line 60
    return-void
.end method

.method static synthetic access$0(Lcom/sec/print/sf/usbsdk/SUSBMngr;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/print/sf/usbsdk/SUSBMngr;Landroid/hardware/usb/UsbDevice;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->tmpDevice:Landroid/hardware/usb/UsbDevice;

    return-void
.end method

.method static instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    if-nez v0, :cond_1

    .line 69
    const-class v1, Lcom/sec/print/sf/usbsdk/SUSBMngr;

    monitor-enter v1

    .line 70
    :try_start_0
    sget-object v0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/print/sf/usbsdk/SUSBMngr;

    invoke-direct {v0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;-><init>()V

    sput-object v0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    .line 69
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_1
    sget-object v0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method deinitialize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 109
    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    .line 110
    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    .line 112
    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->tmpDevice:Landroid/hardware/usb/UsbDevice;

    .line 113
    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 114
    return-void
.end method

.method findDevice(II)Landroid/hardware/usb/UsbDevice;
    .locals 7
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v5

    if-nez v5, :cond_0

    move-object v0, v4

    .line 167
    :goto_0
    return-object v0

    .line 147
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::findDevice: Enumerate device for vid="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 149
    :try_start_0
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v5}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v2

    .line 150
    .local v2, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::findDevice: Enumerate device: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 153
    .local v1, "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_2

    .line 166
    .end local v1    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .end local v2    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :goto_1
    const-string v5, "SUSBMngr::findDevice: device with pid vid was not found"

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move-object v0, v4

    .line 167
    goto :goto_0

    .line 154
    .restart local v1    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .restart local v2    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :cond_2
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    .line 155
    .local v0, "device":Landroid/hardware/usb/UsbDevice;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::findDevice: Enumerate device: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v5

    if-ne v5, p1, :cond_1

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v5

    if-ne v5, p2, :cond_1

    .line 158
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::findDevice: Found device with pid vid: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 162
    .end local v0    # "device":Landroid/hardware/usb/UsbDevice;
    .end local v1    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .end local v2    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :catch_0
    move-exception v3

    .line 163
    .local v3, "e":Ljava/lang/RuntimeException;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::findDevice: RuntimeException was thrown: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    goto :goto_1
.end method

.method initialize(Landroid/content/Context;)Z
    .locals 4
    .param p1, "parentService"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    :try_start_0
    const-string v1, "usb"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    iput-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 89
    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    if-nez v1, :cond_0

    .line 90
    const-string v1, "SUSBMngr::initialize: Cannot create USB Manager"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move v1, v2

    .line 100
    :goto_0
    return v1

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    .line 96
    const-string v1, "SUSBMngr::initialize: SUCCESS: creating USB Manager"

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    const/4 v1, 0x1

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/RuntimeException;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "SUSBMngr::initialize: RuntimeException was thrown: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    move v1, v2

    .line 100
    goto :goto_0
.end method

.method isInited()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 1
    .param p1, "device"    # Landroid/hardware/usb/UsbDevice;

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    const-string v0, "SUSBMngr::openDevice: usbManager is not inited"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    .line 182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    goto :goto_0
.end method

.method declared-synchronized requestPermissionForDevice(Landroid/hardware/usb/UsbDevice;)Z
    .locals 9
    .param p1, "device"    # Landroid/hardware/usb/UsbDevice;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 193
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SUSBMngr::requestPermissionForDevice: Request permission for device"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v4

    if-nez v4, :cond_1

    .line 195
    const-string v3, "SUSBMngr::requestPermissionForDevice: usbManager is not inited"

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 227
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 199
    :cond_1
    if-eqz p1, :cond_0

    .line 201
    :try_start_1
    iget-object v4, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->tmpDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {p1, v4}, Landroid/hardware/usb/UsbDevice;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v3

    .line 202
    goto :goto_0

    .line 205
    :cond_2
    iget-object v4, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 206
    const/4 v5, 0x0

    :try_start_2
    iput-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->tmpDevice:Landroid/hardware/usb/UsbDevice;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    :try_start_3
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    const/4 v6, 0x0

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.print.sf.usbsdk.USB_PERMISSION"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    .line 211
    new-instance v1, Landroid/content/IntentFilter;

    const-string v5, "com.sec.print.sf.usbsdk.USB_PERMISSION"

    invoke-direct {v1, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 212
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 214
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, p1, v6}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    .line 216
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 222
    :try_start_4
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 223
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    .line 205
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :goto_1
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 227
    :try_start_5
    iget-object v4, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->tmpDevice:Landroid/hardware/usb/UsbDevice;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::requestPermissionForDevice: InterruptException was thrown: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 222
    :try_start_7
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 223
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    goto :goto_1

    .line 205
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 193
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 219
    :catch_1
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_9
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SUSBMngr::requestPermissionForDevice: RuntimeException was thrown: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 222
    :try_start_a
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v5, v6}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 223
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    goto :goto_1

    .line 221
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_2
    move-exception v2

    .line 222
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->parentContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->m_USBReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v5}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 223
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->permissionIntent:Landroid/app/PendingIntent;

    .line 224
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method usbManager()Landroid/hardware/usb/UsbManager;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method
