.class Lcom/sec/print/sf/usbsdk/SUSBDeviceFinder;
.super Ljava/lang/Object;
.source "SUSBDeviceFinder.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static declared-synchronized findAllDevices()[Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    .locals 17

    .prologue
    .line 42
    const-class v15, Lcom/sec/print/sf/usbsdk/SUSBDeviceFinder;

    monitor-enter v15

    :try_start_0
    const-string v6, "SUSBDeviceFinder::findAllDevices"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 44
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v14, "foundList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;>;"
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->isInited()Z

    move-result v6

    if-nez v6, :cond_0

    .line 47
    const-string v6, "SUSBDeviceFinder::findAllDevices: USBSDK is not initialized"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    const/4 v10, 0x0

    .line 94
    :goto_0
    monitor-exit v15

    return-object v10

    .line 51
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->instance()Lcom/sec/print/sf/usbsdk/SUSBMngr;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->usbManager()Landroid/hardware/usb/UsbManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v12

    .line 53
    .local v12, "devicesMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    if-eqz v12, :cond_2

    .line 54
    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 89
    :cond_2
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 90
    const/4 v10, 0x0

    goto :goto_0

    .line 54
    :cond_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 55
    .local v13, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v6, "SUSBDeviceFinder::findAllDevices: Checking device "

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 57
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/usb/UsbDevice;

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->isDeviceClassPrinter(Landroid/hardware/usb/UsbDevice;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 58
    const-string v6, "SUSBDeviceFinder::findAllDevices: FindAllDevices: device is printer!"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 59
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 60
    .local v1, "deviceName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 61
    .local v9, "bOperable":Z
    const/4 v5, 0x0

    .line 62
    .local v5, "serial":Ljava/lang/String;
    const/4 v4, 0x0

    .line 63
    .local v4, "deviceID":Ljava/lang/String;
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v6}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    .line 64
    .local v3, "pid":I
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v6}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    .line 66
    .local v2, "vid":I
    new-instance v11, Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-direct {v11, v1}, Lcom/sec/print/sf/usbsdk/SUSBDevice;-><init>(Ljava/lang/String;)V

    .line 67
    .local v11, "device":Lcom/sec/print/sf/usbsdk/SUSBDevice;
    invoke-virtual {v11}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->open()I

    move-result v6

    if-nez v6, :cond_4

    .line 69
    const/4 v9, 0x1

    .line 72
    invoke-virtual {v11}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v6

    invoke-virtual {v6}, Landroid/hardware/usb/UsbDeviceConnection;->getSerial()Ljava/lang/String;

    move-result-object v5

    .line 75
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v11, v6, v7, v8}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->getDeviceId(Lcom/sec/print/sf/usbsdk/SUSBDevice;BBB)Ljava/lang/String;

    move-result-object v4

    .line 77
    invoke-virtual {v11}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->close()I

    .line 83
    :goto_2
    new-instance v0, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 84
    .local v0, "info":Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 42
    .end local v0    # "info":Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    .end local v1    # "deviceName":Ljava/lang/String;
    .end local v2    # "vid":I
    .end local v3    # "pid":I
    .end local v4    # "deviceID":Ljava/lang/String;
    .end local v5    # "serial":Ljava/lang/String;
    .end local v9    # "bOperable":Z
    .end local v11    # "device":Lcom/sec/print/sf/usbsdk/SUSBDevice;
    .end local v12    # "devicesMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    .end local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :catchall_0
    move-exception v6

    monitor-exit v15

    throw v6

    .line 80
    .restart local v1    # "deviceName":Ljava/lang/String;
    .restart local v2    # "vid":I
    .restart local v3    # "pid":I
    .restart local v4    # "deviceID":Ljava/lang/String;
    .restart local v5    # "serial":Ljava/lang/String;
    .restart local v9    # "bOperable":Z
    .restart local v11    # "device":Lcom/sec/print/sf/usbsdk/SUSBDevice;
    .restart local v12    # "devicesMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    .restart local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :cond_4
    :try_start_2
    const-string v6, "SUSBDeviceFinder::findAllDevices: FindAllDevices: device is not operable!"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_2

    .line 92
    .end local v1    # "deviceName":Ljava/lang/String;
    .end local v2    # "vid":I
    .end local v3    # "pid":I
    .end local v4    # "deviceID":Ljava/lang/String;
    .end local v5    # "serial":Ljava/lang/String;
    .end local v9    # "bOperable":Z
    .end local v11    # "device":Lcom/sec/print/sf/usbsdk/SUSBDevice;
    .end local v13    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :cond_5
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v6

    new-array v10, v6, [Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;

    .line 93
    .local v10, "array":[Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    invoke-interface {v14, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "array":[Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    check-cast v10, [Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    .restart local v10    # "array":[Lcom/sec/print/sf/usbsdk/SUSBDeviceInfo;
    goto/16 :goto_0
.end method
