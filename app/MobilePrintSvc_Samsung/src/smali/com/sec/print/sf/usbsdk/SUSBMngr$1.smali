.class Lcom/sec/print/sf/usbsdk/SUSBMngr$1;
.super Landroid/content/BroadcastReceiver;
.source "SUSBMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/sf/usbsdk/SUSBMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;


# direct methods
.method constructor <init>(Lcom/sec/print/sf/usbsdk/SUSBMngr;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;->this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    .line 232
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 236
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.sec.print.sf.usbsdk.USB_PERMISSION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;->this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    # getter for: Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->access$0(Lcom/sec/print/sf/usbsdk/SUSBMngr;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 239
    :try_start_0
    const-string v2, "device"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 241
    .local v1, "device":Landroid/hardware/usb/UsbDevice;
    const-string v2, "permission"

    const/4 v4, 0x0

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "SUSBMngr::BroadcastReceiver: permission granted for device "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;->this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    invoke-static {v2, v1}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->access$1(Lcom/sec/print/sf/usbsdk/SUSBMngr;Landroid/hardware/usb/UsbDevice;)V

    .line 247
    :goto_0
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;->this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    # getter for: Lcom/sec/print/sf/usbsdk/SUSBMngr;->lock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->access$0(Lcom/sec/print/sf/usbsdk/SUSBMngr;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 238
    monitor-exit v3

    .line 250
    .end local v1    # "device":Landroid/hardware/usb/UsbDevice;
    :cond_0
    return-void

    .line 245
    .restart local v1    # "device":Landroid/hardware/usb/UsbDevice;
    :cond_1
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBMngr$1;->this$0:Lcom/sec/print/sf/usbsdk/SUSBMngr;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/sec/print/sf/usbsdk/SUSBMngr;->access$1(Lcom/sec/print/sf/usbsdk/SUSBMngr;Landroid/hardware/usb/UsbDevice;)V

    goto :goto_0

    .line 238
    .end local v1    # "device":Landroid/hardware/usb/UsbDevice;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
