.class Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;
.super Ljava/lang/Object;
.source "SUSBDeviceDataTransfer.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field private static final INTERFACE_AUTODETECT:I = -0x1


# instance fields
.field private device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private usbInterface:Landroid/hardware/usb/UsbInterface;


# direct methods
.method constructor <init>(Lcom/sec/print/sf/usbsdk/SUSBDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/sec/print/sf/usbsdk/SUSBDevice;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 60
    iput-object p1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    .line 61
    return-void
.end method


# virtual methods
.method bulkRead([BII)I
    .locals 8
    .param p1, "buffer"    # [B
    .param p2, "dataSizeForRead"    # I
    .param p3, "timeout"    # I

    .prologue
    const/4 v7, -0x2

    .line 164
    const-string v0, "SUSBDeviceDataTransfer::bulkRead"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    const-string v0, "SUSBDeviceDataTransfer::bulkRead: Interface is not opened"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 190
    :goto_0
    return v7

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v0, :cond_1

    .line 172
    const-string v0, "SUSBDeviceDataTransfer::bulkRead: Interface is not opened for bulk read"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    const/4 v3, 0x0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BIII)I

    move-result v7

    .line 178
    .local v7, "result":I
    if-ltz v7, :cond_2

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUSBDeviceDataTransfer::bulkRead: Data was read from interface, buffer len = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 180
    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " required size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 181
    const-string v1, " processed size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 187
    .end local v7    # "result":I
    :catch_0
    move-exception v6

    .line 188
    .local v6, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v6}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUSBDeviceDataTransfer::bulkRead: RuntimeException was thrown: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 190
    const/4 v7, -0x1

    goto :goto_0

    .line 184
    .end local v6    # "e":Ljava/lang/RuntimeException;
    .restart local v7    # "result":I
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUSBDeviceDataTransfer::bulkRead: Cannot read data from USB, err="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 185
    const/4 v7, -0x6

    goto :goto_0
.end method

.method declared-synchronized bulkTransfer(B[BII)I
    .locals 8
    .param p1, "endpointAddress"    # B
    .param p2, "buffer"    # [B
    .param p3, "dataSizeForRead"    # I
    .param p4, "timeout"    # I

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    const-string v6, "SUSBDeviceDataTransfer::bulkTransfer"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v6

    if-nez v6, :cond_0

    .line 249
    const-string v6, "SUSBDeviceDataTransfer::bulkTransfer: Interface is not opened"

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    const/4 v5, -0x2

    .line 282
    :goto_0
    monitor-exit p0

    return v5

    .line 253
    :cond_0
    const/4 v0, 0x0

    .line 254
    .local v0, "curEndpoint":Landroid/hardware/usb/UsbEndpoint;
    :try_start_1
    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v6}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v4

    .line 255
    .local v4, "numEPs":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v4, :cond_1

    .line 263
    :goto_2
    if-nez v0, :cond_3

    .line 264
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SUSBDeviceDataTransfer::bulkTransfer: Endpoint with address "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " not found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 265
    const/4 v5, -0x4

    goto :goto_0

    .line 256
    :cond_1
    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v6, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v2

    .line 257
    .local v2, "ep":Landroid/hardware/usb/UsbEndpoint;
    invoke-virtual {v2}, Landroid/hardware/usb/UsbEndpoint;->getAddress()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    if-ne v6, p1, :cond_2

    .line 258
    move-object v0, v2

    .line 259
    goto :goto_2

    .line 255
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 269
    .end local v2    # "ep":Landroid/hardware/usb/UsbEndpoint;
    :cond_3
    :try_start_2
    iget-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v6}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v6

    invoke-virtual {v6, v0, p2, p3, p4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v5

    .line 270
    .local v5, "result":I
    if-ltz v5, :cond_4

    .line 271
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SUSBDeviceDataTransfer::bulkTransfer: Data transferred using EP "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", buffer len = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 272
    array-length v7, p2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " required size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 273
    const-string v7, " processed size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 271
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 279
    .end local v5    # "result":I
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 281
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SUSBDeviceDataTransfer::bulkTransfer: RuntimeException was thrown: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 282
    const/4 v5, -0x1

    goto/16 :goto_0

    .line 276
    .end local v1    # "e":Ljava/lang/RuntimeException;
    .restart local v5    # "result":I
    :cond_4
    :try_start_4
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SUSBDeviceDataTransfer::bulkTransfer: Cannot transfer data to/from USB, err="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 277
    const/4 v5, -0x6

    goto/16 :goto_0

    .line 246
    .end local v0    # "curEndpoint":Landroid/hardware/usb/UsbEndpoint;
    .end local v3    # "i":I
    .end local v4    # "numEPs":I
    .end local v5    # "result":I
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method bulkWrite([BII)I
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "dataSizeToWrite"    # I
    .param p3, "timeout"    # I

    .prologue
    .line 205
    const-string v2, "SUSBDeviceDataTransfer::bulkWrite"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v2

    if-nez v2, :cond_0

    .line 208
    const-string v2, "SUSBDeviceDataTransfer::bulkWrite: Interface is not opened"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 209
    const/4 v1, -0x2

    .line 231
    :goto_0
    return v1

    .line 212
    :cond_0
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v2, :cond_1

    .line 213
    const-string v2, "SUSBDeviceDataTransfer::bulkWrite: Interface is not opened for bulk write"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 214
    const/4 v1, -0x3

    goto :goto_0

    .line 218
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v2}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v2, v3, p1, p2, p3}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    .line 219
    .local v1, "result":I
    if-ltz v1, :cond_2

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDeviceDataTransfer::bulkWrite: Data was written to port, buffer len = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 221
    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " required size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 222
    const-string v3, " processed size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 220
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 228
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 230
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDeviceDataTransfer::bulkWrite: RuntimeException was thrown: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V

    .line 231
    const/4 v1, -0x1

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v1    # "result":I
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDeviceDataTransfer::bulkWrite: Cannot write data to USB, err="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logError(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 226
    const/4 v1, -0x6

    goto :goto_0
.end method

.method close()V
    .locals 2

    .prologue
    .line 143
    const-string v0, "SUSBDeviceDataTransfer::close"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 147
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->closeInterface()V

    .line 148
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 151
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string v0, "SUSBDeviceDataTransfer::close: Can\'t close (closed already)"

    invoke-static {v0}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method isEndpointDirectionWrite(B)Z
    .locals 4
    .param p1, "endpointAddress"    # B
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v2

    if-nez v2, :cond_1

    .line 307
    const-string v2, "SUSBDeviceDataTransfer::isEndpointDirectionWrite: Interface is not opened"

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 316
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    iget-object v2, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2, p1}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v0

    .line 312
    .local v0, "ep":Landroid/hardware/usb/UsbEndpoint;
    if-eqz v0, :cond_2

    .line 313
    invoke-virtual {v0}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 315
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SUSBDeviceDataTransfer::isEndpointDirectionWrite: Endpoint num."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can\'t be opened"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method isOpened()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method open()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->open(I)I

    move-result v0

    return v0
.end method

.method open(I)I
    .locals 8
    .param p1, "interfaceNumber"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v3, -0x2

    const/4 v4, -0x4

    const/4 v6, 0x0

    .line 73
    const-string v5, "SUSBDeviceDataTransfer::open"

    invoke-static {v5}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 75
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v5}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isOpened()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    :cond_0
    const-string v4, "SUSBDeviceDataTransfer::open: Device interface can\'t be opened because it\'s already opened or device is not opened or empty"

    invoke-static {v4}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 113
    :goto_0
    return v3

    .line 80
    :cond_1
    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v5}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->isInterfaceOpened()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 81
    const-string v4, "SUSBDeviceDataTransfer::open: Device interface is already opened for this device"

    invoke-static {v4}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_2
    const/4 v0, 0x0

    .line 86
    .local v0, "deviceInterface":Landroid/hardware/usb/UsbInterface;
    const/4 v3, -0x1

    if-eq p1, v3, :cond_3

    .line 87
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getDeviceImpl()Landroid/hardware/usb/UsbDevice;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    .line 89
    :cond_3
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getDeviceImpl()Landroid/hardware/usb/UsbDevice;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/print/sf/usbsdk/SUSBHelper;->getPrinterDetails(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbInterface;)Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;

    move-result-object v2

    .line 90
    .local v2, "printerDetails":Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;
    if-nez v2, :cond_4

    .line 91
    const-string v3, "SUSBDeviceDataTransfer::open: Suitable interface was not found!"

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    move v3, v4

    .line 92
    goto :goto_0

    .line 95
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "SUSBDeviceDataTransfer::open: Selected interface "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 96
    iget-object v3, v2, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->usbInterface:Landroid/hardware/usb/UsbInterface;

    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    .line 97
    iget-object v3, v2, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    iget-object v3, v3, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;->defBulkInEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 98
    iget-object v3, v2, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterDetails;->endpoints:Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;

    iget-object v3, v3, Lcom/sec/print/sf/usbsdk/SUSBHelper$PrinterEndpoints;->defBulkOutEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iput-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 101
    const/4 v1, 0x1

    .line 102
    .local v1, "force":Z
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->getConnectionImpl()Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v5, v7}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 103
    const-string v3, "SUSBDeviceDataTransfer::open: Cannot claim connection!"

    invoke-static {v3}, Lcom/sec/print/sf/usbsdk/SUSBLogger;->logDebug(Ljava/lang/String;)V

    .line 104
    iput-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 105
    iput-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 106
    iput-object v6, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->usbInterface:Landroid/hardware/usb/UsbInterface;

    move v3, v4

    .line 107
    goto :goto_0

    .line 110
    :cond_5
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->isOpened:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 111
    iget-object v3, p0, Lcom/sec/print/sf/usbsdk/SUSBDeviceDataTransfer;->device:Lcom/sec/print/sf/usbsdk/SUSBDevice;

    invoke-virtual {v3}, Lcom/sec/print/sf/usbsdk/SUSBDevice;->openInterface()V

    .line 113
    const/4 v3, 0x0

    goto :goto_0
.end method

.method setAltSettings(B)I
    .locals 1
    .param p1, "settings"    # B

    .prologue
    .line 295
    const/4 v0, -0x3

    return v0
.end method
