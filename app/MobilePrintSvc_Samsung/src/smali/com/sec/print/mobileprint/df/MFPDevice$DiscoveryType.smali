.class public final enum Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;
.super Ljava/lang/Enum;
.source "MFPDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/MFPDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiscoveryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CLOUD:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

.field public static final enum NETWORK:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

.field public static final enum USB:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

.field public static final enum WIFIDIRECT:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    const-string v1, "WIFIDIRECT"

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->WIFIDIRECT:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v3}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->NETWORK:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    const-string v1, "USB"

    invoke-direct {v0, v1, v4}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->USB:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    const-string v1, "CLOUD"

    invoke-direct {v0, v1, v5}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->CLOUD:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->WIFIDIRECT:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->NETWORK:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->USB:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->CLOUD:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
