.class public Lcom/sec/print/mobileprint/sf/scanView;
.super Landroid/view/View;
.source "scanView.java"


# instance fields
.field private backgroundBitmap:Landroid/graphics/Bitmap;

.field private byteBuffer:Ljava/nio/ByteBuffer;

.field private drawScanImage:Z

.field mColor:I

.field private previewBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x64

    .line 170
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 171
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanView;->setFocusable(Z)V

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/sf/scanView;->drawScanImage:Z

    .line 174
    const/16 v6, 0x64

    .line 175
    .local v6, "N":I
    new-array v3, v2, [I

    .line 176
    .local v3, "data8888":[I
    new-array v4, v2, [S

    .line 177
    .local v4, "data565":[S
    new-array v5, v2, [S

    .line 178
    .local v5, "data4444":[S
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    .line 179
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 180
    const/high16 v0, -0x10000

    invoke-static {v0}, Lcom/sec/print/mobileprint/sf/scanView;->premultiplyColor(I)I

    move-result v0

    const v1, -0xff0100

    invoke-static {v1}, Lcom/sec/print/mobileprint/sf/scanView;->premultiplyColor(I)I

    move-result v1

    invoke-static/range {v0 .. v5}, Lcom/sec/print/mobileprint/sf/scanView;->makeRamp(III[I[S[S)V

    .line 183
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    .line 184
    invoke-static {v3, v2}, Lcom/sec/print/mobileprint/sf/scanView;->makeBuffer([II)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 185
    .local v7, "intbuffer":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 186
    return-void
.end method

.method private static getA32(I)I
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 34
    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static getB32(I)I
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 32
    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static getG32(I)I
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 30
    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static getR32(I)I
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 28
    shr-int/lit8 v0, p0, 0x0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static makeBuffer([II)Ljava/nio/IntBuffer;
    .locals 3
    .param p0, "src"    # [I
    .param p1, "n"    # I

    .prologue
    .line 150
    mul-int v2, p1, p1

    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    .line 151
    .local v0, "dst":Ljava/nio/IntBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 154
    invoke-virtual {v0}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    .line 155
    return-object v0

    .line 152
    :cond_0
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static makeRamp(III[I[S[S)V
    .locals 13
    .param p0, "from"    # I
    .param p1, "to"    # I
    .param p2, "n"    # I
    .param p3, "ramp8888"    # [I
    .param p4, "ramp565"    # [S
    .param p5, "ramp4444"    # [S

    .prologue
    .line 76
    invoke-static {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getR32(I)I

    move-result v9

    shl-int/lit8 v8, v9, 0x17

    .line 77
    .local v8, "r":I
    invoke-static {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getG32(I)I

    move-result v9

    shl-int/lit8 v6, v9, 0x17

    .line 78
    .local v6, "g":I
    invoke-static {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getB32(I)I

    move-result v9

    shl-int/lit8 v1, v9, 0x17

    .line 79
    .local v1, "b":I
    invoke-static {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getA32(I)I

    move-result v9

    shl-int/lit8 v0, v9, 0x17

    .line 81
    .local v0, "a":I
    invoke-static {p1}, Lcom/sec/print/mobileprint/sf/scanView;->getR32(I)I

    move-result v9

    shl-int/lit8 v9, v9, 0x17

    sub-int/2addr v9, v8

    add-int/lit8 v10, p2, -0x1

    div-int v5, v9, v10

    .line 82
    .local v5, "dr":I
    invoke-static {p1}, Lcom/sec/print/mobileprint/sf/scanView;->getG32(I)I

    move-result v9

    shl-int/lit8 v9, v9, 0x17

    sub-int/2addr v9, v6

    add-int/lit8 v10, p2, -0x1

    div-int v4, v9, v10

    .line 83
    .local v4, "dg":I
    invoke-static {p1}, Lcom/sec/print/mobileprint/sf/scanView;->getB32(I)I

    move-result v9

    shl-int/lit8 v9, v9, 0x17

    sub-int/2addr v9, v1

    add-int/lit8 v10, p2, -0x1

    div-int v3, v9, v10

    .line 84
    .local v3, "db":I
    invoke-static {p1}, Lcom/sec/print/mobileprint/sf/scanView;->getA32(I)I

    move-result v9

    shl-int/lit8 v9, v9, 0x17

    sub-int/2addr v9, v0

    add-int/lit8 v10, p2, -0x1

    div-int v2, v9, v10

    .line 86
    .local v2, "da":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, p2, :cond_0

    .line 96
    return-void

    .line 87
    :cond_0
    shr-int/lit8 v9, v8, 0x17

    shr-int/lit8 v10, v6, 0x17

    shr-int/lit8 v11, v1, 0x17

    shr-int/lit8 v12, v0, 0x17

    invoke-static {v9, v10, v11, v12}, Lcom/sec/print/mobileprint/sf/scanView;->pack8888(IIII)I

    move-result v9

    aput v9, p3, v7

    .line 88
    shr-int/lit8 v9, v8, 0x1a

    shr-int/lit8 v10, v6, 0x19

    shr-int/lit8 v11, v1, 0x1a

    invoke-static {v9, v10, v11}, Lcom/sec/print/mobileprint/sf/scanView;->pack565(III)S

    move-result v9

    aput-short v9, p4, v7

    .line 89
    shr-int/lit8 v9, v8, 0x1b

    shr-int/lit8 v10, v6, 0x1b

    shr-int/lit8 v11, v1, 0x1b

    .line 90
    shr-int/lit8 v12, v0, 0x1b

    .line 89
    invoke-static {v9, v10, v11, v12}, Lcom/sec/print/mobileprint/sf/scanView;->pack4444(IIII)S

    move-result v9

    aput-short v9, p5, v7

    .line 91
    add-int/2addr v8, v5

    .line 92
    add-int/2addr v6, v4

    .line 93
    add-int/2addr v1, v3

    .line 94
    add-int/2addr v0, v2

    .line 86
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method private static mul255(II)I
    .locals 2
    .param p0, "c"    # I
    .param p1, "a"    # I

    .prologue
    .line 53
    mul-int v1, p0, p1

    add-int/lit16 v0, v1, 0x80

    .line 54
    .local v0, "prod":I
    shr-int/lit8 v1, v0, 0x8

    add-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x8

    return v1
.end method

.method private static pack4444(IIII)S
    .locals 2
    .param p0, "r"    # I
    .param p1, "g"    # I
    .param p2, "b"    # I
    .param p3, "a"    # I

    .prologue
    .line 49
    shl-int/lit8 v0, p3, 0x0

    shl-int/lit8 v1, p2, 0x4

    or-int/2addr v0, v1

    shl-int/lit8 v1, p1, 0x8

    or-int/2addr v0, v1

    shl-int/lit8 v1, p0, 0xc

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private static pack565(III)S
    .locals 2
    .param p0, "r"    # I
    .param p1, "g"    # I
    .param p2, "b"    # I

    .prologue
    .line 45
    shl-int/lit8 v0, p0, 0xb

    shl-int/lit8 v1, p1, 0x5

    or-int/2addr v0, v1

    shl-int/lit8 v1, p2, 0x0

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private static pack8888(IIII)I
    .locals 2
    .param p0, "r"    # I
    .param p1, "g"    # I
    .param p2, "b"    # I
    .param p3, "a"    # I

    .prologue
    .line 41
    shl-int/lit8 v0, p0, 0x0

    shl-int/lit8 v1, p1, 0x8

    or-int/2addr v0, v1

    shl-int/lit8 v1, p2, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, p3, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private static premultiplyColor(I)I
    .locals 5
    .param p0, "c"    # I

    .prologue
    .line 61
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 62
    .local v3, "r":I
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 63
    .local v2, "g":I
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    .line 64
    .local v1, "b":I
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 66
    .local v0, "a":I
    invoke-static {v3, v0}, Lcom/sec/print/mobileprint/sf/scanView;->mul255(II)I

    move-result v3

    .line 67
    invoke-static {v2, v0}, Lcom/sec/print/mobileprint/sf/scanView;->mul255(II)I

    move-result v2

    .line 68
    invoke-static {v1, v0}, Lcom/sec/print/mobileprint/sf/scanView;->mul255(II)I

    move-result v1

    .line 70
    invoke-static {v3, v2, v1, v0}, Lcom/sec/print/mobileprint/sf/scanView;->pack8888(IIII)I

    move-result v4

    return v4
.end method


# virtual methods
.method public addBlock(I[B)I
    .locals 8
    .param p1, "color"    # I
    .param p2, "b"    # [B

    .prologue
    .line 251
    const-string v5, "scanView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "addBlock: currPosition="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const/4 v3, 0x0

    .line 253
    .local v3, "err":I
    array-length v0, p2

    .line 254
    .local v0, "addCount":I
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    array-length v6, p2

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v6

    if-le v5, v6, :cond_0

    .line 255
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v5

    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    sub-int v0, v5, v6

    .line 257
    :cond_0
    :try_start_0
    iget v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    if-eq p1, v5, :cond_1

    .line 258
    const-string v5, "scanView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "badColor="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const/16 v5, -0x16

    .line 297
    :goto_0
    return v5

    .line 261
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 276
    const-string v5, "scanView"

    const-string v6, "color=default kBadColor"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const/16 v3, -0x16

    move v5, v3

    .line 278
    goto :goto_0

    .line 263
    :pswitch_0
    const-string v5, "scanView"

    const-string v6, "color=kRGBA8888"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, p2, v6, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :goto_1
    const-string v5, "scanView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "addBlock: newPosition="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 289
    .local v1, "currentPosition":I
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 291
    :try_start_1
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 296
    :goto_2
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move v5, v3

    .line 297
    goto :goto_0

    .line 268
    .end local v1    # "currentPosition":I
    :pswitch_1
    :try_start_2
    const-string v5, "scanView"

    const-string v6, "color=kRGBA4444 | kRGB565"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, p2, v6, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 281
    :catch_0
    move-exception v4

    .line 283
    .local v4, "overFlow":Ljava/nio/BufferOverflowException;
    const-string v5, "scanView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "addBlock: ERROR Buffer OverFlow="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const/16 v3, -0x9

    move v5, v3

    .line 285
    goto :goto_0

    .line 272
    .end local v4    # "overFlow":Ljava/nio/BufferOverflowException;
    :pswitch_2
    :try_start_3
    const-string v5, "scanView"

    const-string v6, "color=kGRAY"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, p2, v6, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_3
    .catch Ljava/nio/BufferOverflowException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 292
    .restart local v1    # "currentPosition":I
    :catch_1
    move-exception v2

    .line 293
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "scanView"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "addBlock: previewBitmap.copyPixels Exception : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/16 v3, -0x9

    goto :goto_2

    .line 261
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public createBitmapWithSize(III)I
    .locals 8
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "color"    # I

    .prologue
    const/4 v7, 0x0

    .line 189
    const/4 v2, 0x0

    .line 190
    .local v2, "err":I
    const/4 v0, 0x0

    .line 192
    .local v0, "bytePerSample":I
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    .line 193
    iput-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    .line 195
    :cond_0
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_1

    .line 196
    iput-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    .line 199
    :cond_1
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: Try to allocate"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 202
    packed-switch p3, :pswitch_data_0

    .line 227
    const/4 v4, -0x1

    :try_start_0
    iput v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 228
    const/16 v2, -0x16

    move v3, v2

    .line 247
    .end local v2    # "err":I
    .local v3, "err":I
    :goto_0
    return v3

    .line 204
    .end local v3    # "err":I
    .restart local v2    # "err":I
    :pswitch_0
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 205
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: Bitmap.createBitmap:ARGB_8888"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    .line 207
    const/4 v0, 0x4

    .line 231
    :goto_1
    const-string v4, "scanView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "createBitmapWithSize: ByteBuffer.allocate:Size="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 232
    mul-int v6, v0, p1

    mul-int/2addr v6, p2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 233
    const-string v6, ", W="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", H="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Comp="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 231
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    mul-int v4, v0, p1

    mul-int/2addr v4, p2

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_2
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    if-nez v4, :cond_2

    .line 242
    const/16 v2, -0x9

    :goto_3
    move v3, v2

    .line 247
    .end local v2    # "err":I
    .restart local v3    # "err":I
    goto :goto_0

    .line 210
    .end local v3    # "err":I
    .restart local v2    # "err":I
    :pswitch_1
    :try_start_1
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 211
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: Bitmap.createBitmap:RGB_565"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    .line 213
    const/4 v0, 0x2

    .line 215
    :pswitch_2
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 216
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: Bitmap.createBitmap:ARGB_4444"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    .line 218
    const/4 v0, 0x2

    .line 219
    goto :goto_1

    .line 221
    :pswitch_3
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanView;->mColor:I

    .line 222
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: Bitmap.createBitmap:ALPHA_8"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 224
    const/4 v0, 0x1

    .line 225
    goto :goto_1

    .line 235
    :catch_0
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    const-string v4, "scanView"

    const-string v5, "createBitmapWithSize: ERROR: outOfMemory"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iput-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    .line 238
    iput-object v7, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    goto :goto_2

    .line 244
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 245
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanView;->byteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_3

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 310
    const-string v9, "scanView"

    const-string v10, "onDraw: Enter"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getMeasuredWidth()I

    move-result v8

    .line 312
    .local v8, "w":I
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/sf/scanView;->getMeasuredHeight()I

    move-result v1

    .line 314
    .local v1, "h":I
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v11, v11, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 316
    .local v0, "dstRect":Landroid/graphics/Rect;
    iget-boolean v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->drawScanImage:Z

    if-nez v9, :cond_1

    .line 317
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_0

    .line 318
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 319
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 320
    new-instance v6, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v6, v11, v11, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 321
    .local v6, "srcRect":Landroid/graphics/Rect;
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->backgroundBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v9, v6, v0, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 322
    const-string v9, "scanView"

    const-string v10, "onDraw: draw backgroundBitmap"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    .end local v6    # "srcRect":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    const-string v9, "scanView"

    const-string v10, "onDraw: Exit"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    return-void

    .line 325
    :cond_1
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_0

    .line 326
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 327
    .local v3, "p_w":I
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 328
    .local v2, "p_h":I
    int-to-float v9, v2

    int-to-float v10, v3

    div-float v4, v9, v10

    .line 329
    .local v4, "ratioImage":F
    int-to-float v9, v1

    int-to-float v10, v8

    div-float v5, v9, v10

    .line 331
    .local v5, "rationView":F
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v11, v11, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 332
    .restart local v6    # "srcRect":Landroid/graphics/Rect;
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "dstRect":Landroid/graphics/Rect;
    invoke-direct {v0, v11, v11, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 334
    .restart local v0    # "dstRect":Landroid/graphics/Rect;
    cmpl-float v9, v5, v4

    if-lez v9, :cond_2

    .line 336
    int-to-float v9, v8

    mul-float/2addr v9, v4

    float-to-int v7, v9

    .line 337
    .local v7, "tmp":I
    sub-int v9, v1, v7

    div-int/lit8 v9, v9, 0x2

    iput v9, v0, Landroid/graphics/Rect;->top:I

    .line 338
    iget v9, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v7

    iput v9, v0, Landroid/graphics/Rect;->bottom:I

    .line 346
    :goto_1
    iget-object v9, p0, Lcom/sec/print/mobileprint/sf/scanView;->previewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v9, v6, v0, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 347
    const-string v9, "scanView"

    const-string v10, "onDraw: draw previewBitmap"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 341
    .end local v7    # "tmp":I
    :cond_2
    int-to-float v9, v1

    div-float/2addr v9, v4

    float-to-int v7, v9

    .line 342
    .restart local v7    # "tmp":I
    sub-int v9, v8, v7

    div-int/lit8 v9, v9, 0x2

    iput v9, v0, Landroid/graphics/Rect;->left:I

    .line 343
    iget v9, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v7

    iput v9, v0, Landroid/graphics/Rect;->right:I

    goto :goto_1
.end method

.method public updateImage(Z)V
    .locals 2
    .param p1, "draw"    # Z

    .prologue
    .line 300
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/sf/scanView;->drawScanImage:Z

    .line 301
    if-eqz p1, :cond_0

    .line 302
    const-string v0, "scanView"

    const-string v1, "updateImage: drawScanImage=TRUE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_0
    return-void

    .line 304
    :cond_0
    const-string v0, "scanView"

    const-string v1, "updateImage: drawScanImage=FALSE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
