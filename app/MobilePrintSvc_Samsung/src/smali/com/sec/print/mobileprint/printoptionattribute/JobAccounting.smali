.class public Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;
.super Ljava/lang/Object;
.source "JobAccounting.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private groupPermission:I

.field private idOnly:I

.field private password:Ljava/lang/String;

.field private requiredIDEncryption:I

.field private requiredPWEncryption:I

.field private userID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->readFromParcel(Landroid/os/Parcel;)V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 1
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "idOnly"    # Z
    .param p4, "groupPermission"    # Z
    .param p5, "requiredIDEncryption"    # Z
    .param p6, "requiredPWEncryption"    # Z

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    .line 17
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    .line 20
    invoke-direct {p0, p3}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->idOnly:I

    .line 21
    invoke-direct {p0, p4}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->groupPermission:I

    .line 22
    invoke-direct {p0, p5}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredIDEncryption:I

    .line 23
    invoke-direct {p0, p6}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredPWEncryption:I

    .line 24
    return-void
.end method

.method private booleanToInt(Z)I
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 86
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private intToBoolean(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    const/4 v0, 0x1

    .line 91
    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    return-object v0
.end method

.method public isGroupPermission()Z
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->groupPermission:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isIdOnly()Z
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->idOnly:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isRequiredIDEncryption()Z
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredIDEncryption:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isRequiredPWEncryption()Z
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredPWEncryption:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->idOnly:I

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->groupPermission:I

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredIDEncryption:I

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredPWEncryption:I

    .line 119
    return-void
.end method

.method public setGroupPermission(Z)V
    .locals 1
    .param p1, "groupPermission"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->groupPermission:I

    .line 66
    return-void
.end method

.method public setIdOnly(Z)V
    .locals 1
    .param p1, "idOnly"    # Z

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->idOnly:I

    .line 58
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setRequiredIDEncryption(Z)V
    .locals 1
    .param p1, "requiredIDEncryption"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredIDEncryption:I

    .line 74
    return-void
.end method

.method public setRequiredPWEncryption(Z)V
    .locals 1
    .param p1, "requiredPWEncryption"    # Z

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredPWEncryption:I

    .line 82
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->userID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->idOnly:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->groupPermission:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredIDEncryption:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->requiredPWEncryption:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    return-void
.end method
