.class public Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;
.super Ljava/lang/Object;
.source "FaxOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field faxNoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field location:Ljava/lang/String;

.field portNum:I

.field prefix:Ljava/lang/String;

.field productId:I

.field selectedPrinterIP:Ljava/lang/String;

.field tempFolder:Ljava/lang/String;

.field vendorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 21
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    .line 37
    const-string v1, ""

    const/16 v2, 0x238c

    const-string v3, ""

    const-string v4, ""

    move-object v0, p0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "selectedPrinterIP"    # Ljava/lang/String;
    .param p2, "portNum"    # I
    .param p3, "tempFolder"    # Ljava/lang/String;
    .param p4, "prefix"    # Ljava/lang/String;
    .param p5, "vendorId"    # I
    .param p6, "productId"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p3, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->tempFolder:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->prefix:Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 59
    iput p2, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->portNum:I

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->faxNoList:Ljava/util/ArrayList;

    .line 61
    iput p5, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->vendorId:I

    .line 62
    iput p6, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->productId:I

    .line 64
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getPortNum()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->portNum:I

    return v0
.end method

.method public getProductId()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->productId:I

    return v0
.end method

.method public getSelectedPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedTempFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->tempFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorId()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->vendorId:I

    return v0
.end method

.method public getfaxNoList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->faxNoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->tempFolder:Ljava/lang/String;

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->prefix:Ljava/lang/String;

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->portNum:I

    .line 199
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->faxNoList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->location:Ljava/lang/String;

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->vendorId:I

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->productId:I

    .line 204
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->location:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public setPortNum(I)V
    .locals 0
    .param p1, "portNum"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->portNum:I

    .line 163
    return-void
.end method

.method public setProductId(I)V
    .locals 0
    .param p1, "productId"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->productId:I

    .line 99
    return-void
.end method

.method public setSelectedPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->prefix:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setSelectedPrinterIP(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterIP"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setSelectedTempFolder(Ljava/lang/String;)V
    .locals 0
    .param p1, "tempFolder"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->tempFolder:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setVendorId(I)V
    .locals 0
    .param p1, "vendorId"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->vendorId:I

    .line 89
    return-void
.end method

.method public setfaxNoList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "faxNoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 146
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->faxNoList:Ljava/util/ArrayList;

    .line 147
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->tempFolder:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->prefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 185
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->portNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->faxNoList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 187
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->location:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 188
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->vendorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->productId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    return-void
.end method
