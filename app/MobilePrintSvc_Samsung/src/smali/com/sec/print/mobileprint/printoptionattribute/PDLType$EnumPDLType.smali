.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
.super Ljava/lang/Enum;
.source "PDLType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumPDLType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_AUTO:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_DIRECTPRINT:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_FAX:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_PCL3GUI:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_PDF:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_PWG:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

.field public static final enum PDLTYPE_SPL:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_AUTO"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_AUTO:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 11
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_PCL6"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 12
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_SPL"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_SPL:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 13
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_DIRECTPRINT"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_DIRECTPRINT:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 14
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_PCL3GUI"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL3GUI:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 15
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_FAX"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_FAX:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 16
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_PWG"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PWG:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 17
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    const-string v1, "PDLTYPE_PDF"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PDF:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_AUTO:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_SPL:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_DIRECTPRINT:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL3GUI:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_FAX:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PWG:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PDF:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->mValue:I

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->mValue:I

    return v0
.end method
