.class public Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;
.super Ljava/lang/Object;
.source "FaxResolution.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private resolutionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->readFromParcel(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;)V
    .locals 1
    .param p1, "eResolution"    # Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->resolutionType:I

    .line 49
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getResolution()Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
    .locals 6

    .prologue
    .line 63
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 71
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 63
    :cond_1
    aget-object v0, v1, v2

    .line 65
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->resolutionType:I

    if-eq v4, v5, :cond_0

    .line 63
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->resolutionType:I

    .line 89
    return-void
.end method

.method public setResolution(Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;)V
    .locals 1
    .param p1, "eResolution"    # Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->resolutionType:I

    .line 59
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->resolutionType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    return-void
.end method
