.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;
.super Ljava/lang/Enum;
.source "Collate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/Collate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumCollate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum COLLATE_COLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

.field public static final enum COLLATE_COLLATE_CMD:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

.field public static final enum COLLATE_UNCOLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 21
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    const-string v1, "COLLATE_UNCOLLATE"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_UNCOLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    .line 22
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    const-string v1, "COLLATE_COLLATE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    .line 23
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    const-string v1, "COLLATE_COLLATE_CMD"

    invoke-direct {v0, v1, v3, v5}, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE_CMD:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    .line 20
    new-array v0, v5, [Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_UNCOLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE_CMD:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->mValue:I

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->mValue:I

    return v0
.end method
