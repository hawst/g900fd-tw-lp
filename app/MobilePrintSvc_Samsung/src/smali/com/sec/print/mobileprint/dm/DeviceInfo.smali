.class public Lcom/sec/print/mobileprint/dm/DeviceInfo;
.super Lcom/sec/print/mobileprint/df/MFPDevice;
.source "DeviceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_DEVICE:I = 0x1

.field public static final USB_DEVICE:I = 0x2


# instance fields
.field private connectionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/dm/DeviceInfo;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148
    if-ne p0, p1, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v1

    .line 150
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 151
    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 153
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 154
    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 155
    .local v0, "other":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    iget v4, v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 156
    goto :goto_0
.end method

.method public getConnectionType()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    return v0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v4

    if-nez v4, :cond_0

    .line 67
    :goto_0
    return-object v3

    .line 50
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "printerURL":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 52
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ipp://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 53
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ipp://"

    const-string v6, "http://"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 55
    :cond_1
    new-instance v2, Ljava/net/URL;

    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 56
    .local v2, "url":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 58
    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 61
    .end local v1    # "printerURL":Ljava/lang/String;
    .end local v2    # "url":Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/net/MalformedURLException;
    goto :goto_0

    .line 67
    .end local v0    # "e":Ljava/net/MalformedURLException;
    .restart local v1    # "printerURL":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 140
    const/16 v0, 0x1f

    .line 141
    .local v0, "prime":I
    invoke-super {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;->hashCode()I

    move-result v1

    .line 142
    .local v1, "result":I
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    add-int v1, v2, v3

    .line 143
    return v1
.end method

.method public isIPPNetworkDevice()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v3

    if-nez v3, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v2

    .line 78
    :cond_1
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 79
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const/4 v2, 0x1

    goto :goto_0

    .line 83
    .end local v1    # "url":Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method

.method public isNetworkDevice()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 92
    iget v1, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    if-ne v1, v0, :cond_0

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUSBDevice()Z
    .locals 2

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 101
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 126
    return-void
.end method

.method public setConnectionType(I)V
    .locals 0
    .param p1, "connectionType"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 113
    return-void
.end method

.method public setNetworkDevice()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 117
    return-void
.end method

.method public setUSBDevice()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    .line 121
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Name:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Host:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Note:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/sec/print/mobileprint/df/MFPDevice;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 134
    invoke-super {p0, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDevice;->writeToParcel(Landroid/os/Parcel;I)V

    .line 135
    iget v0, p0, Lcom/sec/print/mobileprint/dm/DeviceInfo;->connectionType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    return-void
.end method
