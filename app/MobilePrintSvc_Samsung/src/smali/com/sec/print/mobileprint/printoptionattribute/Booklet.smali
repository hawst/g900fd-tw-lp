.class public Lcom/sec/print/mobileprint/printoptionattribute/Booklet;
.super Ljava/lang/Object;
.source "Booklet.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Booklet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bookletType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->readFromParcel(Landroid/os/Parcel;)V

    .line 58
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Booklet;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;)V
    .locals 1
    .param p1, "eBooklet"    # Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->bookletType:I

    .line 53
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public getCollate()Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
    .locals 6

    .prologue
    .line 67
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 75
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 67
    :cond_1
    aget-object v0, v1, v2

    .line 69
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->bookletType:I

    if-eq v4, v5, :cond_0

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->bookletType:I

    .line 93
    return-void
.end method

.method public setCollate(Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;)V
    .locals 1
    .param p1, "eCollate"    # Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->bookletType:I

    .line 63
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet;->bookletType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    return-void
.end method
