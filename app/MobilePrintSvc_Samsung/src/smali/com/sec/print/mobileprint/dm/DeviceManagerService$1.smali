.class Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;
.super Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;
.source "DeviceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/dm/DeviceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private portType:I

.field final synthetic this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    .line 167
    invoke-direct {p0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;-><init>()V

    .line 169
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    return-void
.end method

.method private isInkjetPrinter(Ljava/lang/String;)Z
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 366
    const-string v0, "SCX-2000FW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    const-string v0, "SCX-1490W"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 369
    :cond_0
    const/4 v0, 0x1

    .line 372
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    .locals 3
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->isInkjetPrinter(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 251
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .restart local v0    # "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "PCL3GUI"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 257
    :cond_0
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 258
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v0

    .line 263
    :cond_1
    :goto_1
    return-object v0

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v1}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->getLanguages(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 260
    :cond_3
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    .locals 5
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v3}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->getMediaSizeList(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/ArrayList;

    move-result-object v2

    .line 269
    .local v2, "spsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 270
    .local v1, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 271
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 278
    :goto_1
    return-object v1

    .line 271
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    .line 272
    .local v0, "media":Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # invokes: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->SPSMediaSizeToMediaSizeInfo(Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    invoke-static {v4, v0}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$1(Lcom/sec/print/mobileprint/dm/DeviceManagerService;Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    .end local v0    # "media":Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;
    :cond_1
    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;
    invoke-static {}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$2()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public getMediaTypes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    .locals 5
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v3}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->getMediaTypeList(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/ArrayList;

    move-result-object v1

    .line 296
    .local v1, "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    .line 297
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 308
    .end local v0    # "i":I
    :goto_1
    return-object v1

    .line 298
    .restart local v0    # "i":I
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 299
    .local v2, "mediatype":Ljava/lang/String;
    const-string v3, "PHOTO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 300
    const-string v3, "Photo"

    invoke-virtual {v1, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 297
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 305
    .end local v0    # "i":I
    .end local v2    # "mediatype":Ljava/lang/String;
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .restart local v1    # "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "Normal"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getPrinterAlerts(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getPrinterAlerts(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrinterName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "ipAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 403
    const/4 v0, 0x0

    .line 405
    .local v0, "printerName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406
    return-object v0
.end method

.method public getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrinters()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v0, "listDeviceInfo":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 221
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v3, v3, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->getPrinters()Ljava/util/List;

    move-result-object v2

    .line 222
    .local v2, "listUSBDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    if-eqz v2, :cond_0

    .line 223
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 228
    .end local v2    # "listUSBDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    :cond_0
    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 229
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v3, v3, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getPrinters()Ljava/util/List;

    move-result-object v1

    .line 230
    .local v1, "listNetworkDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    if-eqz v1, :cond_1

    .line 231
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 235
    .end local v1    # "listNetworkDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    :cond_1
    return-object v0
.end method

.method public getScanners()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getScanners()Ljava/util/List;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 2
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 354
    .local v0, "result":I
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 360
    :goto_0
    return v0

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    goto :goto_0
.end method

.method public isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 3
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 313
    const/4 v0, -0x1

    .line 314
    .local v0, "result":I
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->isInkjetPrinter(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    const/4 v1, 0x1

    .line 326
    :goto_0
    return v1

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v1}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->isPrinterColorModel(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 319
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 320
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 321
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    :cond_1
    :goto_1
    move v1, v0

    .line 326
    goto :goto_0

    .line 323
    :cond_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v1, v1, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    goto :goto_1
.end method

.method public isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 5
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    const/4 v4, -0x1

    .line 330
    const/4 v0, -0x1

    .line 331
    .local v0, "result":I
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->isInkjetPrinter(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    const/4 v0, 0x0

    move v1, v0

    .line 349
    .end local v0    # "result":I
    .local v1, "result":I
    :goto_0
    return v1

    .line 335
    .end local v1    # "result":I
    .restart local v0    # "result":I
    :cond_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->isSupportDuplex(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 338
    if-ne v0, v4, :cond_2

    .line 339
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v2, v2, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v2, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    .line 343
    :cond_1
    if-ne v0, v4, :cond_2

    .line 345
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->isSupportDuplex(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v0

    :cond_2
    move v1, v0

    .line 349
    .end local v0    # "result":I
    .restart local v1    # "result":I
    goto :goto_0
.end method

.method public isSupportedPrinter(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 4
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 378
    const/4 v1, 0x0

    .line 379
    .local v1, "ret":I
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    # getter for: Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    invoke-static {v2}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->isSupportedPrinter(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v2

    if-nez v2, :cond_2

    .line 381
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->isNetworkDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v2, v2, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v2, p1}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 383
    .local v0, "langs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    const-string v2, "PCL6"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "PCLXL"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384
    :cond_0
    const/4 v1, 0x1

    .line 390
    .end local v0    # "langs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return v1

    .line 388
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public start(I)I
    .locals 6
    .param p1, "portType"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "resultNet":I
    const/4 v1, 0x0

    .line 174
    .local v1, "resultUsb":I
    if-eq p1, v3, :cond_0

    if-eq p1, v4, :cond_0

    if-ne p1, v5, :cond_4

    .line 175
    :cond_0
    iput p1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    .line 180
    :goto_0
    const/4 v2, 0x0

    .line 181
    .local v2, "scanners":Z
    if-eq p1, v4, :cond_1

    .line 182
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v4, v4, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v4, v2}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->startDiscovery(Z)I

    move-result v1

    .line 185
    :cond_1
    if-eq p1, v5, :cond_2

    .line 186
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v4, v4, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v4, v2}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->startDiscovery(Z)I

    move-result v0

    .line 189
    :cond_2
    if-eq v1, v3, :cond_3

    if-ne v0, v3, :cond_5

    .line 192
    :cond_3
    :goto_1
    return v3

    .line 177
    .end local v2    # "scanners":Z
    :cond_4
    iput v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    goto :goto_0

    .line 192
    .restart local v2    # "scanners":Z
    :cond_5
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public stop()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "resultNet":I
    const/4 v1, 0x0

    .line 200
    .local v1, "resultUsb":I
    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 201
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v3, v3, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;->stopDiscovery()I

    move-result v1

    .line 204
    :cond_0
    iget v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->portType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 205
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;->this$0:Lcom/sec/print/mobileprint/dm/DeviceManagerService;

    iget-object v3, v3, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->stopDiscovery()I

    move-result v0

    .line 208
    :cond_1
    if-eq v1, v2, :cond_2

    if-ne v0, v2, :cond_3

    .line 211
    :cond_2
    :goto_0
    return v2

    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method
