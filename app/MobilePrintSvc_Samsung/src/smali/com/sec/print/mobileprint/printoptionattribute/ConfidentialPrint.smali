.class public Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;
.super Ljava/lang/Object;
.source "ConfidentialPrint.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private password:Ljava/lang/String;

.field private userID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->readFromParcel(Landroid/os/Parcel;)V

    .line 19
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->userID:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->password:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->userID:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->userID:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->password:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->password:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->userID:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->userID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ConfidentialPrint;->password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    return-void
.end method
