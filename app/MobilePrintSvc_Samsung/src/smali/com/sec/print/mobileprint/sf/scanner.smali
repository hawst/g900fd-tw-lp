.class public Lcom/sec/print/mobileprint/sf/scanner;
.super Landroid/app/Activity;
.source "scanner.java"


# instance fields
.field docSource:I

.field draw:Z

.field errors:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field errorsMessage:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field errorsTitle:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field fileName:Ljava/lang/String;

.field ipAddr:Ljava/lang/String;

.field isCreated:Z

.field mExternalStorageAvailable:Z

.field mExternalStorageWriteable:Z

.field paper:I

.field scale:I

.field scanColor:I

.field scanHeight:I

.field scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

.field scanResolution:I

.field scanWidth:I

.field scanX:I

.field scanY:I

.field scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->isCreated:Z

    .line 34
    return-void
.end method


# virtual methods
.method protected getCapability()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/sec/print/mobileprint/sf/scanner$2;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/sf/scanner$2;-><init>(Lcom/sec/print/mobileprint/sf/scanner;)V

    .line 76
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 77
    return-void
.end method

.method public handleExternalStorageState()I
    .locals 3

    .prologue
    const/16 v0, -0x12

    const/16 v1, -0x13

    .line 660
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageAvailable:Z

    if-nez v2, :cond_0

    .line 661
    const-string v1, "External Storage"

    invoke-virtual {p0, v0, v1}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 668
    :goto_0
    return v0

    .line 664
    :cond_0
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageWriteable:Z

    if-nez v0, :cond_1

    .line 665
    const-string v0, "External Storage"

    invoke-virtual {p0, v1, v0}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    move v0, v1

    .line 666
    goto :goto_0

    .line 668
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    sget v0, Lcom/sec/print/mobileprint/sf/R$layout;->main:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->setContentView(I)V

    .line 84
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->ScanView:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/sf/scanView;

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    .line 85
    iput-boolean v3, p0, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    .line 86
    iput-boolean v5, p0, Lcom/sec/print/mobileprint/sf/scanner;->isCreated:Z

    .line 87
    iget-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    iget-boolean v2, p0, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    invoke-virtual {v0, v2}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 91
    new-instance v0, Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/sf/ScannerLib;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    .line 93
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->result:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 94
    .local v10, "statusText":Landroid/widget/TextView;
    const-string v0, "Ready to Scan"

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iput-boolean v3, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageAvailable:Z

    .line 97
    iput-boolean v3, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageWriteable:Z

    .line 100
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    .line 101
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsTitle:Ljava/util/Vector;

    .line 102
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsMessage:Ljava/util/Vector;

    .line 105
    const-string v0, "106.109.8.96"

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->setIP(Ljava/lang/String;)V

    .line 106
    const/4 v3, 0x4

    const/4 v4, 0x3

    move-object v0, p0

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/print/mobileprint/sf/scanner;->setParams(IIIII)V

    .line 109
    const-string v0, "tesJPEG"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/print/mobileprint/sf/scanner;->setFileName(Ljava/lang/String;Ljava/lang/Integer;)I

    move-result v9

    .line 110
    .local v9, "err":I
    if-eqz v9, :cond_0

    .line 111
    const-string v0, "setFileName"

    invoke-virtual {p0, v9, v0}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 119
    :cond_0
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->buttonScan:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 120
    .local v8, "buttonScan":Landroid/widget/Button;
    new-instance v0, Lcom/sec/print/mobileprint/sf/scanner$3;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/sf/scanner$3;-><init>(Lcom/sec/print/mobileprint/sf/scanner;)V

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->buttonCancel:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 141
    .local v6, "buttonCancel":Landroid/widget/Button;
    new-instance v0, Lcom/sec/print/mobileprint/sf/scanner$4;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/sf/scanner$4;-><init>(Lcom/sec/print/mobileprint/sf/scanner;)V

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->buttonCapabilities:I

    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 154
    .local v7, "buttonCapabilities":Landroid/widget/Button;
    new-instance v0, Lcom/sec/print/mobileprint/sf/scanner$5;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/sf/scanner$5;-><init>(Lcom/sec/print/mobileprint/sf/scanner;)V

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 166
    iget-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    .line 172
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 175
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 176
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v0

    .line 177
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 180
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    invoke-virtual {v6, v7}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 181
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsTitle:Ljava/util/Vector;

    invoke-virtual {v6, v7}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 182
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsMessage:Ljava/util/Vector;

    invoke-virtual {v6, v7}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 183
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 184
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errors:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 185
    .local v1, "err":I
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsTitle:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 186
    .local v5, "title":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->errorsMessage:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 187
    .local v3, "msg":Ljava/lang/String;
    const/4 v6, 0x2

    new-array v4, v6, [Ljava/lang/String;

    aput-object v5, v4, v7

    const/4 v6, 0x1

    aput-object v3, v4, v6

    .line 189
    .local v4, "strArray":[Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 190
    .local v2, "intent":Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.sec.print.mobileprint.sf"

    const-string v8, "com.sec.print.mobileprint.sf.DialogActivity"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 191
    const-string v6, "com.sec.print.mobileprint.sf.DialogActivity"

    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    invoke-virtual {p0, v2, v1}, Lcom/sec/print/mobileprint/sf/scanner;->startActivityForResult(Landroid/content/Intent;I)V

    .line 200
    .end local v1    # "err":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "strArray":[Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public previewImageScale(IIII)I
    .locals 8
    .param p1, "convImageWidth"    # I
    .param p2, "convImageHeigh"    # I
    .param p3, "preview_Width"    # I
    .param p4, "preview_Height"    # I

    .prologue
    .line 602
    const/4 v3, 0x1

    .line 604
    .local v3, "value":I
    int-to-float v4, p1

    int-to-float v5, p2

    div-float v0, v4, v5

    .line 605
    .local v0, "ratioImage":F
    int-to-float v4, p3

    int-to-float v5, p4

    div-float v1, v4, v5

    .line 607
    .local v1, "rationView":F
    cmpl-float v4, v1, v0

    if-lez v4, :cond_0

    .line 609
    int-to-float v4, p2

    int-to-float v5, p4

    div-float/2addr v4, v5

    float-to-int v2, v4

    .line 615
    .local v2, "scale":I
    :goto_0
    const-string v4, "scanner"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "previewImageScale: Scale="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const/16 v4, 0x8

    if-lt v2, v4, :cond_1

    .line 617
    const/16 v3, 0x8

    .line 624
    :goto_1
    return v3

    .line 613
    .end local v2    # "scale":I
    :cond_0
    int-to-float v4, p1

    int-to-float v5, p4

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v2, v4

    .restart local v2    # "scale":I
    goto :goto_0

    .line 618
    :cond_1
    const/4 v4, 0x4

    if-lt v2, v4, :cond_2

    .line 619
    const/4 v3, 0x4

    goto :goto_1

    .line 620
    :cond_2
    const/4 v4, 0x2

    if-lt v2, v4, :cond_3

    .line 621
    const/4 v3, 0x2

    goto :goto_1

    .line 623
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public requestCapability()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x5

    .line 203
    const-string v4, "scanner"

    const-string v5, "requestCapability: START"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    sget v4, Lcom/sec/print/mobileprint/sf/R$id;->result:I

    invoke-virtual {p0, v4}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 205
    .local v3, "statusText":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    iget-object v5, p0, Lcom/sec/print/mobileprint/sf/scanner;->ipAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getCapabilities(Ljava/lang/String;)[I

    move-result-object v0

    .line 206
    .local v0, "arr":[I
    const/4 v4, 0x0

    aget v2, v0, v4

    .line 207
    .local v2, "err":I
    const-string v1, ""

    .line 209
    .local v1, "doc":Ljava/lang/String;
    if-nez v2, :cond_4

    .line 211
    aget v4, v0, v6

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v7, :cond_0

    .line 212
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "FB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    :cond_0
    aget v4, v0, v6

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v8, :cond_1

    .line 214
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":ADF"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 215
    :cond_1
    aget v4, v0, v6

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v9, :cond_2

    .line 216
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":Dup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    :cond_2
    aget v4, v0, v6

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_3

    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":A"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 220
    :cond_3
    const v4, 0x12fd1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Capabilities: FB_W="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v6, v0, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " FB_H="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v0, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 221
    const-string v6, " ADF_W="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ADF_H="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v0, v9

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DOC="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isADFHasPaper="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x7

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 220
    invoke-virtual {p0, v4, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 225
    :goto_0
    const-string v4, "scanner"

    const-string v5, "requestCapability: END"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    return-void

    .line 224
    :cond_4
    const-string v4, "Get Capabilities ERROR"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public scanImage()V
    .locals 49

    .prologue
    .line 231
    const/16 v35, 0x0

    .line 232
    .local v35, "err":I
    const/16 v36, 0x0

    .line 233
    .local v36, "firtBlockIsRead":Z
    const-string v41, ""

    .line 234
    .local v41, "message":Ljava/lang/String;
    const-string v5, "scanner"

    const-string v8, "scanImage: START"

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v47

    .line 239
    .local v47, "root":Ljava/io/File;
    new-instance v44, Ljava/io/File;

    invoke-virtual/range {v47 .. v47}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v8, "scanFiles//"

    move-object/from16 v0, v44

    invoke-direct {v0, v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    .local v44, "path":Ljava/io/File;
    const-string v5, "scanner"

    const-string v8, "!!! Entering in JNI !!!"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v8, Ljava/io/File;

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1.png"

    invoke-direct {v8, v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v5

    const/4 v5, 0x1

    new-instance v8, Ljava/io/File;

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "2.png"

    invoke-direct {v8, v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v5

    .line 243
    .local v6, "images_png":[Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "output_png.pdf"

    invoke-direct {v5, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 244
    .local v7, "pdfPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    const v8, 0x4414c000    # 595.0f

    const v9, 0x44528000    # 842.0f

    const/high16 v10, 0x41480000    # 12.5f

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual/range {v5 .. v12}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImagesToPDF([Ljava/lang/String;Ljava/lang/String;FFFII)I

    .line 249
    const-string v5, "scanner"

    const-string v8, "!!! Leaving in JNI !!!"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/scanView;->getWidth()I

    move-result v46

    .line 256
    .local v46, "previewWidth":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/scanView;->getHeight()I

    move-result v45

    .line 263
    .local v45, "previewHeight":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->ipAddr:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/ScannerLib;->startScanSession(Ljava/lang/String;)I

    move-result v35

    .line 264
    const v5, 0x12fd1

    const-string v8, "Start Scan Session"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 265
    if-eqz v35, :cond_0

    .line 266
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 267
    const-string v5, "Start Scan Session"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 533
    :goto_0
    return-void

    .line 271
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "call setParameters("

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 272
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanResolution:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->docSource:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 273
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 271
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 274
    const v5, 0x12fd1

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v5, v1}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 288
    const/16 v9, 0x12c0

    const/16 v10, 0x12c0

    const/16 v11, 0x12c0

    const/16 v12, 0x12c0

    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x3

    const/16 v16, 0x1

    move-object/from16 v8, p0

    invoke-virtual/range {v8 .. v16}, Lcom/sec/print/mobileprint/sf/scanner;->setParams(IIIIIIII)V

    .line 290
    const-string v5, "testImage"

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->setFileName(Ljava/lang/String;Ljava/lang/Integer;)I

    move-result v35

    .line 291
    if-eqz v35, :cond_1

    .line 292
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 293
    const-string v5, "setFileName"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 294
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 295
    goto/16 :goto_0

    .line 308
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanResolution:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/print/mobileprint/sf/scanner;->docSource:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    invoke-virtual/range {v8 .. v19}, Lcom/sec/print/mobileprint/sf/ScannerLib;->setParameters(IIIIIIIIILjava/lang/String;I)I

    move-result v35

    .line 310
    if-eqz v35, :cond_2

    .line 311
    const-string v5, "SCANNER"

    const-string v8, "ERROR SETTING PARAMETERS!!!"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const-string v5, "setParameters"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 320
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 321
    const-string v5, "End Scan Session"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 324
    :cond_2
    const v5, 0x12fd1

    const-string v8, "setParameters End"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 330
    const/16 v43, 0x0

    .line 344
    .local v43, "pageNumber":I
    const/16 v48, 0x1

    .line 351
    .local v48, "savePreview":Z
    :cond_3
    const/16 v22, 0x0

    .line 354
    .local v22, "blockNumber":I
    :cond_4
    add-int/lit8 v22, v22, 0x1

    .line 355
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "call readImageBlock:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->readImageBlock()[I

    move-result-object v20

    .line 365
    .local v20, "a":[I
    if-nez v20, :cond_5

    .line 368
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getError()I

    move-result v5

    const-string v8, "readImageBlock. return null"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 371
    :cond_5
    const/4 v5, 0x0

    aget v35, v20, v5

    .line 372
    if-eqz v35, :cond_6

    .line 374
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 375
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Err readImageBlock:="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 377
    goto/16 :goto_0

    .line 379
    :cond_6
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "end readImageBlock:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 383
    const/4 v5, 0x2

    aget v32, v20, v5

    .line 384
    .local v32, "defBlockWidth":I
    const/4 v5, 0x3

    aget v30, v20, v5

    .line 385
    .local v30, "defBlockHeight":I
    const/4 v5, 0x1

    aget v31, v20, v5

    .line 389
    .local v31, "defBlockSize":I
    const/4 v5, 0x4

    aget v28, v20, v5

    .line 390
    .local v28, "defBlockConvSize":I
    const/4 v5, 0x5

    aget v29, v20, v5

    .line 391
    .local v29, "defBlockConvWidth":I
    const/4 v5, 0x6

    aget v27, v20, v5

    .line 392
    .local v27, "defBlockConvHeight":I
    const/4 v5, 0x7

    aget v33, v20, v5

    .line 393
    .local v33, "defImageConvHeight":I
    move/from16 v34, v29

    .line 414
    .local v34, "defImageWidth":I
    move-object/from16 v0, p0

    move/from16 v1, v29

    move/from16 v2, v33

    move/from16 v3, v46

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/print/mobileprint/sf/scanner;->previewImageScale(IIII)I

    move-result v26

    .line 415
    .local v26, "calcScale":I
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    .line 416
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "previewImageScale: scale="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 417
    const-string v9, " preview:=("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v46

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v45

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 418
    const-string v9, ") converBlock:=("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v29

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v33

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 416
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    invoke-virtual {v5, v8, v9}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getBlockSize(II)[I

    move-result-object v23

    .line 423
    .local v23, "blockSize":[I
    const/4 v5, 0x0

    aget v35, v23, v5

    .line 424
    if-eqz v35, :cond_7

    .line 425
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Err getBlockSize:="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 426
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 427
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 428
    goto/16 :goto_0

    .line 431
    :cond_7
    const/4 v5, 0x1

    aget v38, v23, v5

    .line 432
    .local v38, "imageSize":I
    const/4 v5, 0x2

    aget v24, v23, v5

    .line 433
    .local v24, "blockWidth":I
    const/4 v5, 0x3

    aget v21, v23, v5

    .line 434
    .local v21, "blockHeight":I
    const/4 v5, 0x4

    aget v37, v23, v5

    .line 435
    .local v37, "imageHeight":I
    move/from16 v39, v24

    .line 440
    .local v39, "imageWidth":I
    if-nez v36, :cond_a

    .line 442
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "createBitmapWithSize: bitmapPreviewSize:=("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 443
    move/from16 v0, v39

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Color:="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 442
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    move/from16 v0, v39

    move/from16 v1, v37

    invoke-virtual {v5, v0, v1, v8}, Lcom/sec/print/mobileprint/sf/scanView;->createBitmapWithSize(III)I

    move-result v35

    .line 445
    if-eqz v35, :cond_9

    .line 447
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 448
    const-string v5, "createBitmapWithSize"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 449
    const/16 v5, -0x9

    move/from16 v0, v35

    if-ne v0, v5, :cond_8

    .line 450
    const/16 v48, 0x0

    goto/16 :goto_0

    .line 453
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    goto/16 :goto_0

    .line 456
    :cond_9
    const/16 v36, 0x1

    .line 459
    :cond_a
    if-eqz v48, :cond_b

    .line 464
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    invoke-virtual {v5, v8, v9}, Lcom/sec/print/mobileprint/sf/ScannerLib;->getImageBlock(II)[B

    move-result-object v25

    .line 468
    .local v25, "bytePreviewArray":[B
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    move-object/from16 v0, v25

    invoke-virtual {v5, v8, v0}, Lcom/sec/print/mobileprint/sf/scanView;->addBlock(I[B)I

    .line 472
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 473
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/scanView;->postInvalidate()V

    .line 477
    .end local v25    # "bytePreviewArray":[B
    :cond_b
    const v5, 0x12fd1

    const-string v8, "call isLastBlock"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 478
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->isLastBlock()[I

    move-result-object v40

    .line 479
    .local v40, "latstBlockArray":[I
    const/4 v5, 0x0

    aget v35, v40, v5

    .line 480
    if-eqz v35, :cond_c

    .line 482
    const-string v5, "Err isLastBlock"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 483
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 484
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 485
    goto/16 :goto_0

    .line 487
    :cond_c
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "end isLastBlock:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x1

    aget v9, v40, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 488
    const/4 v5, 0x1

    aget v5, v40, v5

    if-eqz v5, :cond_4

    .line 492
    add-int/lit8 v43, v43, 0x1

    .line 493
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "setFileName:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v43

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 494
    const-string v5, "untitled"

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->setFileName(Ljava/lang/String;Ljava/lang/Integer;)I

    move-result v35

    .line 495
    if-eqz v35, :cond_d

    .line 496
    const-string v5, "setFileName"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 497
    :cond_d
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "setFileName:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 498
    const v5, 0x12fd1

    const-string v8, "call nextPage"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 499
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/ScannerLib;->nextPage(Ljava/lang/String;)[I

    move-result-object v42

    .line 500
    .local v42, "pageArray":[I
    const/4 v5, 0x0

    aget v35, v42, v5

    .line 501
    if-eqz v35, :cond_e

    .line 503
    const-string v5, "nextPage"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 505
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 506
    goto/16 :goto_0

    .line 508
    :cond_e
    const v5, 0x12fd1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "end nextPage:="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x1

    aget v9, v42, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 511
    const/4 v5, 0x1

    aget v5, v42, v5

    const/4 v8, 0x1

    if-eq v5, v8, :cond_3

    .line 513
    const v5, 0x12fd1

    const-string v8, "call endScanSession"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 514
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/sf/ScannerLib;->endScanSession()I

    move-result v35

    .line 515
    const-string v5, "endScanSession"

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v1, v5}, Lcom/sec/print/mobileprint/sf/scanner;->showError(ILjava/lang/String;)V

    .line 517
    if-eqz v35, :cond_f

    .line 518
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    goto/16 :goto_0

    .line 521
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    .line 523
    const-string v5, "scanner"

    const-string v8, "!!! Converting !!!"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    const-string v5, "testImage"

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8}, Lcom/sec/print/mobileprint/sf/scanner;->setFileName(Ljava/lang/String;Ljava/lang/Integer;)I

    .line 525
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ".bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    const/4 v11, 0x5

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 526
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ".jpeg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    const/4 v11, 0x1

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 527
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    new-instance v8, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ".jpeg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".bmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x5

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 528
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    new-instance v8, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ".jpeg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".jpeg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 529
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    new-instance v8, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ".jpeg"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "2"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".png"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x2

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/sec/print/mobileprint/sf/ScannerLib;->convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I

    .line 531
    const-string v5, "scanner"

    const-string v8, "!!! End converting !!!"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public setFileName(Ljava/lang/String;Ljava/lang/Integer;)I
    .locals 9
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "num"    # Ljava/lang/Integer;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 628
    const/4 v0, 0x0

    .line 630
    .local v0, "err":I
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    .line 631
    .local v4, "state":Ljava/lang/String;
    const-string v5, "mounted"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 632
    iput-boolean v7, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageWriteable:Z

    iput-boolean v7, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageAvailable:Z

    .line 640
    :goto_0
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/sf/scanner;->handleExternalStorageState()I

    move-result v0

    .line 642
    if-eqz v0, :cond_2

    .line 657
    .end local v0    # "err":I
    :goto_1
    return v0

    .line 633
    .restart local v0    # "err":I
    :cond_0
    const-string v5, "mounted_ro"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 634
    iput-boolean v7, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageAvailable:Z

    .line 635
    iput-boolean v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageWriteable:Z

    goto :goto_0

    .line 637
    :cond_1
    iput-boolean v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageWriteable:Z

    iput-boolean v6, p0, Lcom/sec/print/mobileprint/sf/scanner;->mExternalStorageAvailable:Z

    goto :goto_0

    .line 644
    :cond_2
    const/4 v1, 0x0

    .line 645
    .local v1, "fileIO":Z
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    .line 646
    .local v3, "root":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "scanFiles/"

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    .local v2, "path":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    .line 648
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 649
    if-nez v1, :cond_3

    .line 650
    const/16 v0, -0x1c

    goto :goto_1

    .line 652
    :cond_3
    if-eqz p2, :cond_4

    .line 653
    new-instance v5, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".png"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    goto :goto_1

    .line 655
    :cond_4
    new-instance v5, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ".png"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/print/mobileprint/sf/scanner;->fileName:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public setIP(Ljava/lang/String;)V
    .locals 0
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    .line 536
    iput-object p1, p0, Lcom/sec/print/mobileprint/sf/scanner;->ipAddr:Ljava/lang/String;

    .line 537
    return-void
.end method

.method public setPaper(I)V
    .locals 3
    .param p1, "pap"    # I

    .prologue
    const/16 v2, 0x26c1

    const/16 v1, 0x27d8

    const/4 v0, 0x0

    .line 558
    iput p1, p0, Lcom/sec/print/mobileprint/sf/scanner;->paper:I

    .line 559
    packed-switch p1, :pswitch_data_0

    .line 592
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 593
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 594
    iput v2, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 595
    const/16 v0, 0x36cf

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    .line 598
    :goto_0
    return-void

    .line 561
    :pswitch_0
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 562
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 563
    iput v1, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 564
    const/16 v0, 0x3390

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    goto :goto_0

    .line 567
    :pswitch_1
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 568
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 569
    iput v1, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 570
    const/16 v0, 0x41a0

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    goto :goto_0

    .line 573
    :pswitch_2
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 574
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 575
    const/16 v0, 0x19c8

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 576
    iput v1, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    goto :goto_0

    .line 579
    :pswitch_3
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 580
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 581
    const/16 v0, 0x21fc

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 582
    const/16 v0, 0x3138

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    goto :goto_0

    .line 585
    :pswitch_4
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 586
    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 587
    const/16 v0, 0x1b50

    iput v0, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 588
    iput v2, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    goto :goto_0

    .line 559
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setParams(IIIII)V
    .locals 0
    .param p1, "pap"    # I
    .param p2, "source"    # I
    .param p3, "res"    # I
    .param p4, "color"    # I
    .param p5, "val"    # I

    .prologue
    .line 539
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/sf/scanner;->setPaper(I)V

    .line 540
    iput p2, p0, Lcom/sec/print/mobileprint/sf/scanner;->docSource:I

    .line 541
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanResolution:I

    .line 542
    iput p4, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    .line 543
    iput p5, p0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    .line 544
    return-void
.end method

.method public setParams(IIIIIIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I
    .param p5, "source"    # I
    .param p6, "res"    # I
    .param p7, "color"    # I
    .param p8, "val"    # I

    .prologue
    .line 547
    iput p1, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanX:I

    .line 548
    iput p1, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanY:I

    .line 549
    iput p3, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanWidth:I

    .line 550
    iput p4, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanHeight:I

    .line 551
    iput p5, p0, Lcom/sec/print/mobileprint/sf/scanner;->docSource:I

    .line 552
    iput p6, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanResolution:I

    .line 553
    iput p7, p0, Lcom/sec/print/mobileprint/sf/scanner;->scanColor:I

    .line 554
    iput p8, p0, Lcom/sec/print/mobileprint/sf/scanner;->scale:I

    .line 555
    return-void
.end method

.method public showError(ILjava/lang/String;)V
    .locals 4
    .param p1, "err"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 671
    const-string v0, ""

    .line 672
    .local v0, "errMessage":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 830
    :goto_0
    const-string v1, ""

    if-ne v0, v1, :cond_0

    .line 831
    const-string v1, "scanner"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    :goto_1
    return-void

    .line 674
    :sswitch_0
    const-string v0, "OK"

    .line 675
    goto :goto_0

    .line 677
    :sswitch_1
    const-string v0, "File Open Error"

    .line 678
    goto :goto_0

    .line 680
    :sswitch_2
    const-string v0, "File Open Error"

    .line 681
    goto :goto_0

    .line 683
    :sswitch_3
    const-string v0, "No Get Capabilities"

    .line 684
    goto :goto_0

    .line 686
    :sswitch_4
    const-string v0, "No Filled Capabilities"

    .line 687
    goto :goto_0

    .line 689
    :sswitch_5
    const-string v0, "Session Open Error"

    .line 690
    goto :goto_0

    .line 692
    :sswitch_6
    const-string v0, "Missing Document Dolfer"

    .line 693
    goto :goto_0

    .line 695
    :sswitch_7
    const-string v0, "No Memory"

    .line 696
    goto :goto_0

    .line 698
    :sswitch_8
    const-string v0, "Color is not supported"

    .line 699
    goto :goto_0

    .line 701
    :sswitch_9
    const-string v0, "No Memory for Preview"

    .line 702
    goto :goto_0

    .line 704
    :sswitch_a
    const-string v0, "Apps Low Memory"

    .line 705
    goto :goto_0

    .line 707
    :sswitch_b
    const-string v0, "File i/o Error"

    .line 708
    goto :goto_0

    .line 710
    :sswitch_c
    const-string v0, "Service Not Started"

    .line 711
    goto :goto_0

    .line 713
    :sswitch_d
    const-string v0, "Sevice is Running"

    .line 714
    goto :goto_0

    .line 716
    :sswitch_e
    const-string v0, "Param Error"

    .line 717
    goto :goto_0

    .line 719
    :sswitch_f
    const-string v0, "Can not Create Capabilities"

    .line 720
    goto :goto_0

    .line 722
    :sswitch_10
    const-string v0, "No Service or Capabilities"

    .line 723
    goto :goto_0

    .line 725
    :sswitch_11
    const-string v0, "SF: Operation Canceled by Used"

    .line 726
    goto :goto_0

    .line 728
    :sswitch_12
    const-string v0, "SF: Unknown Error"

    .line 729
    goto :goto_0

    .line 731
    :sswitch_13
    const-string v0, "SF: No Document"

    .line 732
    goto :goto_0

    .line 734
    :sswitch_14
    const-string v0, "SF: Paper Jam"

    .line 735
    goto :goto_0

    .line 737
    :sswitch_15
    const-string v0, "SF: Cover is Open"

    .line 738
    goto :goto_0

    .line 740
    :sswitch_16
    const-string v0, "SF: Locked"

    .line 741
    goto :goto_0

    .line 743
    :sswitch_17
    const-string v0, "SF: Device busy"

    .line 744
    goto :goto_0

    .line 746
    :sswitch_18
    const-string v0, "SF: Invlid Param"

    .line 747
    goto :goto_0

    .line 749
    :sswitch_19
    const-string v0, "SF: Security Error"

    .line 750
    goto :goto_0

    .line 752
    :sswitch_1a
    const-string v0, "SF: Invalide User"

    .line 753
    goto :goto_0

    .line 755
    :sswitch_1b
    const-string v0, "SF: Invalide Password"

    .line 756
    goto :goto_0

    .line 758
    :sswitch_1c
    const-string v0, "SF: Authorization Error"

    .line 759
    goto :goto_0

    .line 761
    :sswitch_1d
    const-string v0, "SF: Empty Password"

    .line 762
    goto :goto_0

    .line 764
    :sswitch_1e
    const-string v0, "SF: No Permission"

    .line 765
    goto :goto_0

    .line 767
    :sswitch_1f
    const-string v0, "SF: Exceed Quota"

    .line 768
    goto/16 :goto_0

    .line 770
    :sswitch_20
    const-string v0, "SF: All Pages Scanned"

    .line 771
    goto/16 :goto_0

    .line 773
    :sswitch_21
    const-string v0, "SF: TimeOut"

    .line 774
    goto/16 :goto_0

    .line 776
    :sswitch_22
    const-string v0, "SF: Status Busy"

    .line 777
    goto/16 :goto_0

    .line 779
    :sswitch_23
    const-string v0, "SF: Photo Export Error"

    .line 780
    goto/16 :goto_0

    .line 782
    :sswitch_24
    const-string v0, "No External Storage Card"

    .line 784
    :sswitch_25
    const-string v0, "External Card is Read-Only"

    .line 785
    goto/16 :goto_0

    .line 787
    :sswitch_26
    const-string v0, "Scan Area Width+OrigX > then Doc Source width"

    .line 788
    goto/16 :goto_0

    .line 790
    :sswitch_27
    const-string v0, "Scan Area Height+OrigY > then doc Source height"

    .line 791
    goto/16 :goto_0

    .line 793
    :sswitch_28
    const-string v0, "Non supported Color"

    .line 794
    goto/16 :goto_0

    .line 796
    :sswitch_29
    const-string v0, "Wrong quality parameters"

    .line 797
    goto/16 :goto_0

    .line 799
    :sswitch_2a
    const-string v0, "Wrong scale parameters"

    .line 800
    goto/16 :goto_0

    .line 802
    :sswitch_2b
    const-string v0, "Wrong Doc source parameters"

    .line 803
    goto/16 :goto_0

    .line 805
    :sswitch_2c
    const-string v0, "Origin X < 0 or > doc Source width"

    .line 806
    goto/16 :goto_0

    .line 808
    :sswitch_2d
    const-string v0, "Origin Y < 0 or > doc Source height"

    .line 809
    goto/16 :goto_0

    .line 811
    :sswitch_2e
    const-string v0, "Can not create directory"

    .line 812
    goto/16 :goto_0

    .line 814
    :sswitch_2f
    const-string v0, "Write file Error"

    .line 815
    goto/16 :goto_0

    .line 817
    :sswitch_30
    const-string v0, "Flush File Error"

    .line 818
    goto/16 :goto_0

    .line 820
    :sswitch_31
    const-string v0, "Close File Error"

    .line 821
    goto/16 :goto_0

    .line 823
    :sswitch_32
    const-string v0, "Session is Open"

    .line 824
    goto/16 :goto_0

    .line 827
    :sswitch_33
    const-string v0, ""

    goto/16 :goto_0

    .line 833
    :cond_0
    const-string v1, "scanner"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 672
    nop

    :sswitch_data_0
    .sparse-switch
        -0x20 -> :sswitch_32
        -0x1f -> :sswitch_31
        -0x1e -> :sswitch_30
        -0x1d -> :sswitch_2f
        -0x1c -> :sswitch_2e
        -0x1b -> :sswitch_2d
        -0x1a -> :sswitch_2c
        -0x19 -> :sswitch_2b
        -0x18 -> :sswitch_2a
        -0x17 -> :sswitch_29
        -0x16 -> :sswitch_28
        -0x15 -> :sswitch_27
        -0x14 -> :sswitch_26
        -0x13 -> :sswitch_25
        -0x12 -> :sswitch_24
        -0x11 -> :sswitch_23
        -0x10 -> :sswitch_10
        -0xf -> :sswitch_f
        -0xe -> :sswitch_e
        -0xd -> :sswitch_d
        -0xc -> :sswitch_c
        -0xb -> :sswitch_b
        -0xa -> :sswitch_a
        -0x9 -> :sswitch_9
        -0x8 -> :sswitch_8
        -0x7 -> :sswitch_7
        -0x6 -> :sswitch_6
        -0x5 -> :sswitch_5
        -0x4 -> :sswitch_4
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
        0x1 -> :sswitch_11
        0x2 -> :sswitch_12
        0x3 -> :sswitch_13
        0x4 -> :sswitch_14
        0x5 -> :sswitch_15
        0x6 -> :sswitch_16
        0x7 -> :sswitch_17
        0x9 -> :sswitch_18
        0xa -> :sswitch_19
        0xb -> :sswitch_1a
        0xc -> :sswitch_1b
        0xd -> :sswitch_1c
        0xe -> :sswitch_1d
        0xf -> :sswitch_1e
        0x10 -> :sswitch_1f
        0x11 -> :sswitch_20
        0x12 -> :sswitch_21
        0x13 -> :sswitch_22
        0x12fd1 -> :sswitch_33
    .end sparse-switch
.end method

.method protected startScanning()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/sec/print/mobileprint/sf/scanner$1;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/sf/scanner$1;-><init>(Lcom/sec/print/mobileprint/sf/scanner;)V

    .line 65
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 66
    return-void
.end method
