.class public abstract Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;
.super Landroid/os/Binder;
.source "IDiscoveryService.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/IDiscoveryService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/IDiscoveryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.print.mobileprint.df.IDiscoveryService"

.field static final TRANSACTION_IsCollateSuported:I = 0x13

.field static final TRANSACTION_IsConfidentialPrintAvailable:I = 0x17

.field static final TRANSACTION_IsDeviceSamsungFax:I = 0x12

.field static final TRANSACTION_IsDeviceSamsungPrinter:I = 0x10

.field static final TRANSACTION_IsDeviceSamsungScanner:I = 0x11

.field static final TRANSACTION_IsPrinterAlive:I = 0xf

.field static final TRANSACTION_IsPrinterColorModel:I = 0xe

.field static final TRANSACTION_SetCommunityName:I = 0x1

.field static final TRANSACTION_Start:I = 0x3

.field static final TRANSACTION_Stop:I = 0x4

.field static final TRANSACTION_getDeviceByMacAddress:I = 0x1d

.field static final TRANSACTION_getDeviceLocation:I = 0x1e

.field static final TRANSACTION_getDeviceMacAddress:I = 0x23

.field static final TRANSACTION_getDeviceMode:I = 0x9

.field static final TRANSACTION_getDeviceName:I = 0x14

.field static final TRANSACTION_getDuplex:I = 0xb

.field static final TRANSACTION_getFaxType:I = 0xa

.field static final TRANSACTION_getFaxes:I = 0x7

.field static final TRANSACTION_getIppDevices:I = 0x1c

.field static final TRANSACTION_getJobTable:I = 0x16

.field static final TRANSACTION_getLanguages:I = 0x8

.field static final TRANSACTION_getMediaSizes:I = 0xc

.field static final TRANSACTION_getMediaTypes:I = 0xd

.field static final TRANSACTION_getNWDeviceProperties:I = 0x24

.field static final TRANSACTION_getPrinterStatus:I = 0x15

.field static final TRANSACTION_getPrinters:I = 0x5

.field static final TRANSACTION_getScanners:I = 0x6

.field static final TRANSACTION_getSmbDevices:I = 0x1a

.field static final TRANSACTION_getSmbState:I = 0x1b

.field static final TRANSACTION_getUSBDeviceProperties:I = 0x25

.field static final TRANSACTION_getUsbDevices:I = 0x1f

.field static final TRANSACTION_isUSBDeviceFax:I = 0x22

.field static final TRANSACTION_isUSBDeviceScaner:I = 0x21

.field static final TRANSACTION_setSnmpV3Settings:I = 0x2

.field static final TRANSACTION_startSmb:I = 0x18

.field static final TRANSACTION_stopSmb:I = 0x19

.field static final TRANSACTION_wakeDeviceUp:I = 0x20


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/print/mobileprint/df/IDiscoveryService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/print/mobileprint/df/IDiscoveryService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/print/mobileprint/df/IDiscoveryService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 12
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 440
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x1

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->SetCommunityName(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 52
    const/4 v0, 0x1

    goto :goto_0

    .line 56
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_2
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 60
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    .line 69
    .local v3, "_arg2":Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    sget-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    .line 78
    .local v5, "_arg4":Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .local v6, "_arg5":Ljava/lang/String;
    move-object v0, p0

    .line 79
    invoke-virtual/range {v0 .. v6}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->setSnmpV3Settings(Ljava/lang/String;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 81
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    .end local v3    # "_arg2":Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    .end local v6    # "_arg5":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    goto :goto_1

    .line 75
    .restart local v4    # "_arg3":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "_arg4":Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    goto :goto_2

    .line 85
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    :sswitch_3
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 89
    .local v7, "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 90
    .local v2, "_arg1":I
    invoke-virtual {p0, v7, v2}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->Start(Ljava/util/List;I)I

    move-result v8

    .line 91
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    .end local v2    # "_arg1":I
    .end local v7    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "_result":I
    :sswitch_4
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->Stop()I

    move-result v8

    .line 99
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    .end local v8    # "_result":I
    :sswitch_5
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getPrinters()Ljava/util/List;

    move-result-object v9

    .line 107
    .local v9, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 108
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 109
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 113
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_6
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getScanners()Ljava/util/List;

    move-result-object v9

    .line 115
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 116
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 117
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 121
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_7
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getFaxes()Ljava/util/List;

    move-result-object v9

    .line 123
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 125
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 129
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_8
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 132
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getLanguages(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 133
    .local v11, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    invoke-virtual {p3, v11}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 135
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 139
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_9
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 142
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDeviceMode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 143
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 144
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 149
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_a
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 152
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getFaxType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 153
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 154
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 159
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_b
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 162
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDuplex(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 163
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 164
    invoke-virtual {p3, v11}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 165
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 169
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_c
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 172
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getMediaSizes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 173
    .local v10, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPMediaSize;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 175
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 179
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPMediaSize;>;"
    :sswitch_d
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 182
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getMediaTypes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 183
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    invoke-virtual {p3, v11}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 185
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 189
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_e
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 192
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsPrinterColorModel(Ljava/lang/String;)I

    move-result v8

    .line 193
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 195
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 199
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_f
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 202
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsPrinterAlive(Ljava/lang/String;)I

    move-result v8

    .line 203
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 204
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 209
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_10
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 212
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsDeviceSamsungPrinter(Ljava/lang/String;)I

    move-result v8

    .line 213
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 214
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 215
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 219
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_11
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsDeviceSamsungScanner(Ljava/lang/String;)I

    move-result v8

    .line 223
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 225
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 229
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_12
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 232
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsDeviceSamsungFax(Ljava/lang/String;)I

    move-result v8

    .line 233
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 234
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 235
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 239
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_13
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 242
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsCollateSuported(Ljava/lang/String;)I

    move-result v8

    .line 243
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 244
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 245
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 249
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_14
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 252
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 253
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 254
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 259
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_15
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 262
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getPrinterStatus(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDeviceStatus;

    move-result-object v8

    .line 263
    .local v8, "_result":Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 264
    if-eqz v8, :cond_2

    .line 265
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->writeToParcel(Landroid/os/Parcel;I)V

    .line 271
    :goto_3
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 269
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 275
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    :sswitch_16
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 278
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getJobTable(Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 279
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 280
    invoke-virtual {p3, v11}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 281
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 285
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_17
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 287
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 288
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->IsConfidentialPrintAvailable(Ljava/lang/String;)I

    move-result v8

    .line 289
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 290
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 295
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_18
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 299
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 304
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->startSmb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 305
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 306
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 307
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 311
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_19
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->stopSmb()I

    move-result v8

    .line 313
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 314
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 315
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 319
    .end local v8    # "_result":I
    :sswitch_1a
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getSmbDevices()Ljava/util/List;

    move-result-object v9

    .line 321
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 322
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 323
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 327
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_1b
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getSmbState()I

    move-result v8

    .line 329
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 335
    .end local v8    # "_result":I
    :sswitch_1c
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 338
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getIppDevices(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 339
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 340
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 341
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 345
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_1d
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 347
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 348
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDeviceByMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 349
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 350
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 351
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 355
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_1e
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 358
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDeviceLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 359
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 360
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 361
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 365
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_1f
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 366
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getUsbDevices()Ljava/util/List;

    move-result-object v9

    .line 367
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 368
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 369
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 373
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :sswitch_20
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 375
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 376
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->wakeDeviceUp(Ljava/lang/String;)I

    move-result v8

    .line 377
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 378
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 383
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_21
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 385
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 387
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 388
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->isUSBDeviceScaner(II)Z

    move-result v8

    .line 389
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 390
    if-eqz v8, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 391
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 390
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 395
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_22
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 399
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 400
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->isUSBDeviceFax(II)Z

    move-result v8

    .line 401
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 402
    if-eqz v8, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 403
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 402
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 407
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_23
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 409
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 410
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getDeviceMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 411
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 412
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 413
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 417
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_24
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 419
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 421
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 422
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getNWDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v8

    .line 423
    .local v8, "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 424
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 425
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 429
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Ljava/util/Map;
    :sswitch_25
    const-string v0, "com.sec.print.mobileprint.df.IDiscoveryService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 433
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 434
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;->getUSBDeviceProperties(II)Ljava/util/Map;

    move-result-object v8

    .line 435
    .restart local v8    # "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 436
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 437
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
