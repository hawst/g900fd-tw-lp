.class public Lcom/sec/print/mobileprint/sf/ScannerLib;
.super Ljava/lang/Object;
.source "ScannerLib.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const-string v0, "scanService"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native asyncCancel()I
.end method

.method public native convertImageFormat(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public native convertImagesToPDF([Ljava/lang/String;Ljava/lang/String;FFFII)I
.end method

.method public native endScanSession()I
.end method

.method public native getBlockSize(II)[I
.end method

.method public native getCapabilities(Ljava/lang/String;)[I
.end method

.method public native getCapabilitiesUSB(II)[I
.end method

.method public native getError()I
.end method

.method public native getIP()Ljava/lang/String;
.end method

.method public native getImageBlock(II)[B
.end method

.method public native isLastBlock()[I
.end method

.method public native nextPage(Ljava/lang/String;)[I
.end method

.method public native readImageBlock()[I
.end method

.method public native setParameters(IIIIIIIIILjava/lang/String;I)I
.end method

.method public native setParameters2(IIIIIIIIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;II)I
.end method

.method public native startScanSession(Ljava/lang/String;)I
.end method

.method public native startScanSessionUSB(II)I
.end method
