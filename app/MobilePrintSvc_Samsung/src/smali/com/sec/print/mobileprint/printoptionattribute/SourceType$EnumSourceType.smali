.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;
.super Ljava/lang/Enum;
.source "SourceType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/SourceType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumSourceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

.field public static final enum PRINT_SOURCE_DOCUMENT:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

.field public static final enum PRINT_SOURCE_PHOTO:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 27
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    const-string v1, "PRINT_SOURCE_PHOTO"

    .line 28
    invoke-direct {v0, v1, v3, v2}, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->PRINT_SOURCE_PHOTO:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    .line 29
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    const-string v1, "PRINT_SOURCE_DOCUMENT"

    .line 30
    invoke-direct {v0, v1, v2, v4}, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->PRINT_SOURCE_DOCUMENT:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    .line 26
    new-array v0, v4, [Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->PRINT_SOURCE_PHOTO:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->PRINT_SOURCE_DOCUMENT:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->mValue:I

    .line 37
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->mValue:I

    return v0
.end method
