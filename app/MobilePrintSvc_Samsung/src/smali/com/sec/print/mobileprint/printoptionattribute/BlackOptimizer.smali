.class public Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;
.super Ljava/lang/Object;
.source "BlackOptimizer.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private blackOptimizerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->readFromParcel(Landroid/os/Parcel;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;)V
    .locals 1
    .param p1, "eBlackOptimizer"    # Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->blackOptimizerType:I

    .line 38
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getBlackOptimizer()Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;
    .locals 6

    .prologue
    .line 62
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 70
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 62
    :cond_1
    aget-object v0, v1, v2

    .line 64
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->blackOptimizerType:I

    if-eq v4, v5, :cond_0

    .line 62
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->blackOptimizerType:I

    .line 89
    return-void
.end method

.method public setBlackOptimizer(Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;)V
    .locals 1
    .param p1, "eBlackOptimizer"    # Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer$EnumBlackOptimizer;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->blackOptimizerType:I

    .line 53
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/BlackOptimizer;->blackOptimizerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    return-void
.end method
