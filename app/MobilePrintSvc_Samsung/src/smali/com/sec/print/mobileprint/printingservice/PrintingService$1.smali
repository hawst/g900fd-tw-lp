.class Lcom/sec/print/mobileprint/printingservice/PrintingService$1;
.super Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;
.source "PrintingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printingservice/PrintingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/printingservice/PrintingService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    .line 30
    invoke-direct {p0}, Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 35
    const-string v0, "cancel"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->printJob:Lcom/sec/print/mobileprint/PrintJob;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/PrintJob;->cancel()V

    .line 37
    return-void
.end method

.method public print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)Z
    .locals 3
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docs"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
    .param p3, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->printJob:Lcom/sec/print/mobileprint/PrintJob;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/PrintJob;->isProcessing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    sget-object v1, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 47
    const/4 v2, 0x5

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->printJob:Lcom/sec/print/mobileprint/PrintJob;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/print/mobileprint/PrintJob;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)Z

    move-result v0

    goto :goto_0
.end method

.method public registerCallback(Lcom/sec/print/mobileprint/IPrintStatusCallback;)V
    .locals 1
    .param p1, "callbackStatus"    # Lcom/sec/print/mobileprint/IPrintStatusCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 66
    const-string v0, "ERROR : callbackStatus is null in RegisterCallback"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 70
    return-void
.end method

.method public unregisterCallback(Lcom/sec/print/mobileprint/IPrintStatusCallback;)V
    .locals 1
    .param p1, "callbackStatus"    # Lcom/sec/print/mobileprint/IPrintStatusCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 82
    if-nez p1, :cond_0

    .line 84
    const-string v0, "ERROR : callbackStatus is null in UnregisterCallback"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;->this$0:Lcom/sec/print/mobileprint/printingservice/PrintingService;

    iget-object v0, v0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 88
    return-void
.end method
