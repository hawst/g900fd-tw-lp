.class public Lcom/sec/print/mobileprint/df/DiscoveryResult;
.super Ljava/lang/Object;
.source "DiscoveryResult.java"


# instance fields
.field private exception:Ljava/lang/Throwable;

.field private host:Ljava/lang/String;

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->host:Ljava/lang/String;

    .line 13
    iput p2, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->version:I

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p2, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->exception:Ljava/lang/Throwable;

    .line 18
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->host:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getException()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->exception:Ljava/lang/Throwable;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->version:I

    return v0
.end method

.method public setException(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->exception:Ljava/lang/Throwable;

    .line 27
    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->host:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResult;->version:I

    .line 43
    return-void
.end method
