.class public Lcom/sec/print/mobileprint/PDLComposer;
.super Ljava/lang/Object;
.source "PDLComposer.java"


# instance fields
.field outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

.field statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    const-string v0, "k2ViewerJni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 145
    const-string v0, "SamsungPDLComposer_MD2"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 146
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/PrintingStatusMonitor;)V
    .locals 0
    .param p1, "statusMonitor"    # Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/print/mobileprint/PDLComposer;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 48
    return-void
.end method

.method private static native cancelJob()V
.end method

.method public static createFolder(Ljava/lang/String;)Z
    .locals 5
    .param p0, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 127
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    .local v0, "filePath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 129
    .local v1, "folderPath":Ljava/io/File;
    const-string v2, "PrintJob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Create Folder : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 132
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 134
    const-string v2, "PrintJob"

    const-string v3, "ERROR - mkdirs"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v2, 0x0

    .line 139
    :goto_0
    return v2

    .line 137
    :cond_0
    const-string v2, "PrintJob"

    const-string v3, "Success : "

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static native generatePDLData(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I
.end method

.method public static native getFrameCnt()I
.end method

.method public static native getFrameHeight()D
.end method

.method public static native getFramePosX(I)D
.end method

.method public static native getFramePosY(I)D
.end method

.method public static native getFrameRotate(I)Z
.end method

.method public static native getFrameWidth()D
.end method

.method public static native getImageBoxInfo(Ljava/lang/Object;[B)V
.end method

.method public static native getRSPFrameCntX()I
.end method

.method public static native getRSPFrameCntY()I
.end method

.method public static native getRSPFrameHeight()D
.end method

.method public static native getRSPFrameWidth()D
.end method

.method public static native getRSPMediaHeight()D
.end method

.method public static native getRSPMediaWidth()D
.end method

.method public static native makePhotoImageFile(Ljava/lang/Object;)V
.end method

.method private setDefaultAttribute(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V
    .locals 3
    .param p1, "listPrintOptionAttribute"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;

    .prologue
    const/16 v2, 0x258

    .line 116
    new-instance v1, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;

    invoke-direct {v1, v2, v2}, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;-><init>(II)V

    invoke-virtual {p1, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/SamsungMobilePrint/Temp/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "deletethis"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/mobileprint/PDLComposer;->createFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    new-instance v1, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;

    invoke-static {}, Lcom/sec/print/mobileprint/PrintJob;->getTempPrefixString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 123
    :cond_0
    return-void
.end method

.method public static native setFrameSize(DDD)Z
.end method

.method public static native setMediaSize(DDDDDD)V
.end method

.method public static native setNup(IDZ)Z
.end method

.method public static native setRSPSize(III)Z
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->cancelJob()V

    .line 109
    iget-object v0, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    invoke-interface {v0}, Lcom/sec/print/mobileprint/io/ISPSOutputStream;->cancel()V

    .line 110
    return-void
.end method

.method public print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I
    .locals 4
    .param p1, "listPrintOptionAttribute"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
    .param p3, "outputStream"    # Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    .prologue
    .line 88
    const/4 v1, 0x0

    .line 89
    .local v1, "err":I
    iput-object p3, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "about to call generatePDLData:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/PDLComposer;->setDefaultAttribute(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V

    .line 93
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    iget-object v3, p0, Lcom/sec/print/mobileprint/PDLComposer;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    invoke-static {p1, p2, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->generatePDLData(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 97
    :goto_0
    return v1

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Ljava/lang/String;)I
    .locals 6
    .param p1, "listPrintOptionAttribute"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
    .param p3, "filePath"    # Ljava/lang/String;

    .prologue
    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "err":I
    new-instance v2, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    invoke-direct {v2, p3}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 55
    .local v2, "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    iput-object v2, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    .line 58
    const-class v4, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;

    invoke-virtual {p1, v4}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;

    .line 60
    .local v3, "imageSize":Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "about to call print:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->getImageSizeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->getFrameWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->getFrameHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/PDLComposer;->setDefaultAttribute(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V

    .line 63
    :try_start_0
    iget-object v4, p0, Lcom/sec/print/mobileprint/PDLComposer;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    invoke-static {p1, p2, v2, v4}, Lcom/sec/print/mobileprint/PDLComposer;->generatePDLData(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 68
    :goto_0
    return v1

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Ljava/lang/String;I)I
    .locals 4
    .param p1, "listPrintOptionAttribute"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
    .param p3, "ipAddr"    # Ljava/lang/String;
    .param p4, "portNum"    # I

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "err":I
    new-instance v2, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;

    invoke-direct {v2, p3, p4}, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;-><init>(Ljava/lang/String;I)V

    .line 75
    .local v2, "netOutputStream":Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;
    iput-object v2, p0, Lcom/sec/print/mobileprint/PDLComposer;->outputStream:Lcom/sec/print/mobileprint/io/ISPSOutputStream;

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/PDLComposer;->setDefaultAttribute(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V

    .line 78
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/PDLComposer;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    invoke-static {p1, p2, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->generatePDLData(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 83
    :goto_0
    return v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
