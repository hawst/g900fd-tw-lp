.class public Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
.super Ljava/lang/Object;
.source "DiscoveryBroadcast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/DiscoveryBroadcast$Listener;
    }
.end annotation


# static fields
.field protected static final PRINTER_OID:Lorg/snmp4j/smi/OID;

.field private static final TAG:Ljava/lang/String; = "DiscoveryBroadcast"

.field private static snmpList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/snmp4j/Snmp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final MP_DEBUG:Z

.field private final discoveryResultsDispatcher:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

.field private final dispatcherThread:Ljava/lang/Thread;

.field private listener2:Lorg/snmp4j/event/ResponseListener;

.field private listener3:Lorg/snmp4j/event/ResponseListener;

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/DiscoveryListener;",
            ">;"
        }
    .end annotation
.end field

.field private pdu:Lorg/snmp4j/PDU;

.field private pdu3:Lorg/snmp4j/ScopedPDU;

.field private final receiveBufferSize:I

.field private final results:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/sec/print/mobileprint/df/DiscoveryResult;",
            ">;"
        }
    .end annotation
.end field

.field private snmp:Lorg/snmp4j/Snmp;

.field private snmp1Discovery:Z

.field private snmp1ReadComunity:Ljava/lang/String;

.field private snmp2Discovery:Z

.field private snmp2ReadComunity:Ljava/lang/String;

.field private snmp3ContextName:Ljava/lang/String;

.field private snmp3Discovery:Z

.field private target:Lorg/snmp4j/CommunityTarget;

.field private target3:Lorg/snmp4j/CommunityTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lorg/snmp4j/smi/OID;

    const/4 v1, 0x7

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>([I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->PRINTER_OID:Lorg/snmp4j/smi/OID;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmpList:Ljava/util/List;

    return-void

    .line 48
    nop

    :array_0
    .array-data 4
        0x1
        0x3
        0x6
        0x1
        0x2
        0x1
        0x2b
    .end array-data
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "concurrentThreadsNumber"    # I
    .param p2, "concurrentRequestsNumber"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->MP_DEBUG:Z

    .line 36
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp1Discovery:Z

    .line 40
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2Discovery:Z

    .line 44
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3Discovery:Z

    .line 56
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listeners:Ljava/util/List;

    .line 59
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    .line 60
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    .line 62
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    .line 63
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    .line 65
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    .line 66
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;

    .line 68
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp:Lorg/snmp4j/Snmp;

    .line 75
    iput p2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->receiveBufferSize:I

    .line 76
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, p2}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->results:Ljava/util/concurrent/BlockingQueue;

    .line 77
    new-instance v0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->results:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listeners:Ljava/util/List;

    invoke-direct {v0, v1, v2, p1}, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->discoveryResultsDispatcher:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

    .line 78
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->discoveryResultsDispatcher:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->dispatcherThread:Ljava/lang/Thread;

    .line 79
    return-void
.end method


# virtual methods
.method public addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/print/mobileprint/df/DiscoveryListener;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    return-void
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 248
    :try_start_0
    sget-object v1, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmpList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 256
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    .line 257
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    .line 258
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    .line 259
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    .line 260
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    .line 261
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->dispatcherThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 268
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->dispatcherThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    .line 269
    return-void

    .line 248
    :cond_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/Snmp;

    .line 249
    .local v0, "snmp":Lorg/snmp4j/Snmp;
    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {v0}, Lorg/snmp4j/Snmp;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 263
    .end local v0    # "snmp":Lorg/snmp4j/Snmp;
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public enableSnmp1(Ljava/lang/String;)V
    .locals 1
    .param p1, "readComunity"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp1Discovery:Z

    .line 83
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp1ReadComunity:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public enableSnmp2(Ljava/lang/String;)V
    .locals 1
    .param p1, "readComunity"    # Ljava/lang/String;

    .prologue
    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2Discovery:Z

    .line 88
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2ReadComunity:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public enableSnmp3(Ljava/lang/String;)V
    .locals 1
    .param p1, "contextName"    # Ljava/lang/String;

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3Discovery:Z

    .line 93
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3ContextName:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public initialize()V
    .locals 9

    .prologue
    .line 124
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    if-nez v6, :cond_3

    .line 129
    :try_start_0
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    if-nez v6, :cond_0

    .line 130
    new-instance v6, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast$Listener;

    const/4 v7, 0x2

    invoke-direct {v6, p0, v7}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast$Listener;-><init>(Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;I)V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    .line 132
    :cond_0
    iget-boolean v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3Discovery:Z

    if-eqz v6, :cond_1

    .line 133
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    if-nez v6, :cond_1

    .line 134
    new-instance v6, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast$Listener;

    const/4 v7, 0x3

    invoke-direct {v6, p0, v7}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast$Listener;-><init>(Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;I)V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    .line 138
    :cond_1
    const-string v6, "255.255.255.255"

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 139
    .local v0, "broadcastAddress":Ljava/net/InetAddress;
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v5

    .line 140
    .local v5, "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_4

    .line 195
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->dispatcherThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 202
    .end local v0    # "broadcastAddress":Ljava/net/InetAddress;
    .end local v5    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_3
    :goto_1
    return-void

    .line 141
    .restart local v0    # "broadcastAddress":Ljava/net/InetAddress;
    .restart local v5    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_4
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/NetworkInterface;

    .line 143
    .local v1, "iface":Ljava/net/NetworkInterface;
    invoke-static {v1}, Lcom/sec/print/mobileprint/df/DiscoveryUtil;->isNetworkIntefaceWorking(Ljava/net/NetworkInterface;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 144
    invoke-virtual {v1}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    .line 146
    .local v3, "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_9

    .line 157
    iget-boolean v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp1Discovery:Z

    if-nez v6, :cond_6

    iget-boolean v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2Discovery:Z

    if-eqz v6, :cond_7

    .line 159
    :cond_6
    new-instance v6, Lorg/snmp4j/PDU;

    invoke-direct {v6}, Lorg/snmp4j/PDU;-><init>()V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    .line 160
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    new-instance v7, Lorg/snmp4j/smi/VariableBinding;

    sget-object v8, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->PRINTER_OID:Lorg/snmp4j/smi/OID;

    invoke-direct {v7, v8}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 161
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    const/16 v7, -0x5f

    invoke-virtual {v6, v7}, Lorg/snmp4j/PDU;->setType(I)V

    .line 164
    new-instance v6, Lorg/snmp4j/CommunityTarget;

    invoke-direct {v6}, Lorg/snmp4j/CommunityTarget;-><init>()V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    .line 165
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    new-instance v7, Lorg/snmp4j/smi/OctetString;

    iget-object v8, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2ReadComunity:Ljava/lang/String;

    invoke-direct {v7, v8}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/CommunityTarget;->setCommunity(Lorg/snmp4j/smi/OctetString;)V

    .line 166
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    const-wide/32 v7, 0xea60

    invoke-virtual {v6, v7, v8}, Lorg/snmp4j/CommunityTarget;->setTimeout(J)V

    .line 167
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lorg/snmp4j/CommunityTarget;->setVersion(I)V

    .line 168
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    new-instance v7, Lorg/snmp4j/smi/UdpAddress;

    const/16 v8, 0xa1

    invoke-direct {v7, v0, v8}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/CommunityTarget;->setAddress(Lorg/snmp4j/smi/Address;)V

    .line 174
    :cond_7
    iget-boolean v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3Discovery:Z

    if-eqz v6, :cond_2

    .line 176
    new-instance v6, Lorg/snmp4j/ScopedPDU;

    invoke-direct {v6}, Lorg/snmp4j/ScopedPDU;-><init>()V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    .line 177
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3ContextName:Ljava/lang/String;

    if-eqz v6, :cond_8

    .line 178
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    new-instance v7, Lorg/snmp4j/smi/OctetString;

    iget-object v8, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3ContextName:Ljava/lang/String;

    invoke-direct {v7, v8}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/ScopedPDU;->setContextName(Lorg/snmp4j/smi/OctetString;)V

    .line 180
    :cond_8
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    new-instance v7, Lorg/snmp4j/smi/VariableBinding;

    sget-object v8, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->PRINTER_OID:Lorg/snmp4j/smi/OID;

    invoke-direct {v7, v8}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/ScopedPDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 181
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu3:Lorg/snmp4j/ScopedPDU;

    const/16 v7, -0x5f

    invoke-virtual {v6, v7}, Lorg/snmp4j/ScopedPDU;->setType(I)V

    .line 184
    new-instance v6, Lorg/snmp4j/CommunityTarget;

    invoke-direct {v6}, Lorg/snmp4j/CommunityTarget;-><init>()V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;

    .line 185
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;

    const-wide/32 v7, 0xea60

    invoke-virtual {v6, v7, v8}, Lorg/snmp4j/CommunityTarget;->setTimeout(J)V

    .line 186
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lorg/snmp4j/CommunityTarget;->setVersion(I)V

    .line 187
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target3:Lorg/snmp4j/CommunityTarget;

    new-instance v7, Lorg/snmp4j/smi/UdpAddress;

    const/16 v8, 0xa1

    invoke-direct {v7, v0, v8}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v6, v7}, Lorg/snmp4j/CommunityTarget;->setAddress(Lorg/snmp4j/smi/Address;)V

    goto/16 :goto_0

    .line 197
    .end local v0    # "broadcastAddress":Ljava/net/InetAddress;
    .end local v1    # "iface":Ljava/net/NetworkInterface;
    .end local v3    # "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v5    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v6

    goto/16 :goto_1

    .line 147
    .restart local v0    # "broadcastAddress":Ljava/net/InetAddress;
    .restart local v1    # "iface":Ljava/net/NetworkInterface;
    .restart local v3    # "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .restart local v5    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_9
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/InetAddress;

    .line 148
    .local v2, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/conn/util/InetAddressUtils;->isIPv4Address(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 149
    new-instance v4, Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    new-instance v6, Lorg/snmp4j/smi/UdpAddress;

    const/4 v7, 0x0

    invoke-direct {v6, v2, v7}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-direct {v4, v6}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;-><init>(Lorg/snmp4j/smi/UdpAddress;)V

    .line 150
    .local v4, "mapping":Lorg/snmp4j/transport/DefaultUdpTransportMapping;
    iget v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->receiveBufferSize:I

    invoke-virtual {v4, v6}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->setReceiveBufferSize(I)V

    .line 151
    new-instance v6, Lorg/snmp4j/Snmp;

    invoke-direct {v6, v4}, Lorg/snmp4j/Snmp;-><init>(Lorg/snmp4j/TransportMapping;)V

    iput-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp:Lorg/snmp4j/Snmp;

    .line 152
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v6}, Lorg/snmp4j/Snmp;->listen()V

    .line 153
    sget-object v6, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmpList:Ljava/util/List;

    iget-object v7, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2
.end method

.method newDeviceFound(Ljava/lang/String;I)V
    .locals 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    .line 276
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->results:Ljava/util/concurrent/BlockingQueue;

    new-instance v2, Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-direct {v2, p1, p2}, Lcom/sec/print/mobileprint/df/DiscoveryResult;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_0
    return-void

    .line 278
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method newDeviceFoundException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->results:Ljava/util/concurrent/BlockingQueue;

    new-instance v2, Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-direct {v2, p1, p2}, Lcom/sec/print/mobileprint/df/DiscoveryResult;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :goto_0
    return-void

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public removeDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/print/mobileprint/df/DiscoveryListener;

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 300
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    .line 208
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->initialize()V

    .line 210
    sget-object v1, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmpList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 223
    :goto_1
    return-void

    .line 210
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/Snmp;

    .line 211
    .local v0, "snmp":Lorg/snmp4j/Snmp;
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp1Discovery:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp2Discovery:Z

    if-eqz v2, :cond_3

    .line 212
    :cond_2
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener2:Lorg/snmp4j/event/ResponseListener;

    invoke-virtual {v0, v2, v3, v4, v5}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Ljava/lang/Object;Lorg/snmp4j/event/ResponseListener;)V

    .line 215
    :cond_3
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->snmp3Discovery:Z

    if-eqz v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->pdu:Lorg/snmp4j/PDU;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->target:Lorg/snmp4j/CommunityTarget;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->listener3:Lorg/snmp4j/event/ResponseListener;

    invoke-virtual {v0, v2, v3, v4, v5}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Ljava/lang/Object;Lorg/snmp4j/event/ResponseListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    .end local v0    # "snmp":Lorg/snmp4j/Snmp;
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public stop()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 242
    return-void
.end method
