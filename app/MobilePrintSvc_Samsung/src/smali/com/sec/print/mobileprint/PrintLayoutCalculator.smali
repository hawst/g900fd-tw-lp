.class public Lcom/sec/print/mobileprint/PrintLayoutCalculator;
.super Ljava/lang/Object;
.source "PrintLayoutCalculator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFrameCnt()I
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getFrameCnt()I

    move-result v0

    return v0
.end method

.method public getFrameHeight()D
    .locals 2

    .prologue
    .line 31
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getFrameHeight()D

    move-result-wide v0

    return-wide v0
.end method

.method public getFramePosX(I)D
    .locals 2
    .param p1, "nNo"    # I

    .prologue
    .line 36
    invoke-static {p1}, Lcom/sec/print/mobileprint/PDLComposer;->getFramePosX(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFramePosY(I)D
    .locals 2
    .param p1, "nNo"    # I

    .prologue
    .line 41
    invoke-static {p1}, Lcom/sec/print/mobileprint/PDLComposer;->getFramePosY(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFrameRotate(I)Z
    .locals 1
    .param p1, "nNo"    # I

    .prologue
    .line 46
    invoke-static {p1}, Lcom/sec/print/mobileprint/PDLComposer;->getFrameRotate(I)Z

    move-result v0

    return v0
.end method

.method public getFrameWidth()D
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getFrameWidth()D

    move-result-wide v0

    return-wide v0
.end method

.method public getImageBoxInfo(Ljava/lang/Object;[B)V
    .locals 0
    .param p1, "autoBox"    # Ljava/lang/Object;
    .param p2, "buf"    # [B

    .prologue
    .line 50
    invoke-static {p1, p2}, Lcom/sec/print/mobileprint/PDLComposer;->getImageBoxInfo(Ljava/lang/Object;[B)V

    .line 51
    return-void
.end method

.method public getRSPFrameCntX()I
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPFrameCntX()I

    move-result v0

    return v0
.end method

.method public getRSPFrameCntY()I
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPFrameCntY()I

    move-result v0

    return v0
.end method

.method public getRSPFrameHeight()D
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPFrameHeight()D

    move-result-wide v0

    return-wide v0
.end method

.method public getRSPFrameWidth()D
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPFrameWidth()D

    move-result-wide v0

    return-wide v0
.end method

.method public getRSPMediaHeight()D
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPMediaHeight()D

    move-result-wide v0

    return-wide v0
.end method

.method public getRSPMediaWidth()D
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lcom/sec/print/mobileprint/PDLComposer;->getRSPMediaWidth()D

    move-result-wide v0

    return-wide v0
.end method

.method public setFrameSize(DDD)Z
    .locals 1
    .param p1, "frameWidth"    # D
    .param p3, "frameHeight"    # D
    .param p5, "marginFrame"    # D

    .prologue
    .line 11
    invoke-static/range {p1 .. p6}, Lcom/sec/print/mobileprint/PDLComposer;->setFrameSize(DDD)Z

    move-result v0

    return v0
.end method

.method public setMediaSize(DDDDDD)V
    .locals 0
    .param p1, "mediaWidth"    # D
    .param p3, "mediaHeight"    # D
    .param p5, "marginLeft"    # D
    .param p7, "marginTop"    # D
    .param p9, "marginRight"    # D
    .param p11, "marginBottom"    # D

    .prologue
    .line 6
    invoke-static/range {p1 .. p12}, Lcom/sec/print/mobileprint/PDLComposer;->setMediaSize(DDDDDD)V

    .line 7
    return-void
.end method

.method public setNup(IDZ)Z
    .locals 1
    .param p1, "nup"    # I
    .param p2, "marginFrame"    # D
    .param p4, "directionUpAndRight"    # Z

    .prologue
    .line 16
    invoke-static {p1, p2, p3, p4}, Lcom/sec/print/mobileprint/PDLComposer;->setNup(IDZ)Z

    move-result v0

    return v0
.end method

.method public setRSPSize(III)Z
    .locals 1
    .param p1, "paper"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 54
    invoke-static {p1, p2, p3}, Lcom/sec/print/mobileprint/PDLComposer;->setRSPSize(III)Z

    move-result v0

    return v0
.end method
