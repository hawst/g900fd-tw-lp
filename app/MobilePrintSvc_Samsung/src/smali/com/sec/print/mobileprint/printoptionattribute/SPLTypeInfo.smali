.class public Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
.super Ljava/lang/Object;
.source "SPLTypeInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;


# static fields
.field public static final COMPRESSION_BANDED_JBIG:I = 0x3

.field public static final COMPRESSION_BANDED_JBIG_STR:Ljava/lang/String; = "Banded JBIG"

.field public static final COMPRESSION_FBXOR:I = 0x1

.field public static final COMPRESSION_FBXOR_STR:Ljava/lang/String; = "FBXOR"

.field public static final COMPRESSION_JBIG:I = 0x2

.field public static final COMPRESSION_JBIG_STR:Ljava/lang/String; = "JBIG"

.field public static final COMPRESSION_SCANLINE_MODITIFF:I = 0x4

.field public static final COMPRESSION_SCANLINE_MODITIFF_STR:Ljava/lang/String; = "SCANLINE_MODITIFF"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final ERROR_VALUE:I = 0x0

.field public static final SPLTYPE_SPL_V1:I = 0x1

.field public static final SPLTYPE_SPL_V2:I = 0x2

.field public static final SPLTYPE_SPL_V3:I = 0x3

.field public static final SPLTYPE_SPL_V5:I = 0x5

.field public static final WIDTHALIGN_FIXED:I = 0x2

.field public static final WIDTHALIGN_FIXED_STR:Ljava/lang/String; = "Fixed"

.field public static final WIDTHALIGN_PLATFORM:I = 0x1

.field public static final WIDTHALIGN_PLATFORM_STR:Ljava/lang/String; = "Platform"


# instance fields
.field private compressionType:I

.field private ctsFilePath:Ljava/lang/String;

.field private isError:Z

.field private splVersion:I

.field private widthAlignType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 191
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "filepathCTS"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->setCTSFilePath(Ljava/lang/String;)V

    .line 45
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    .line 46
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "filepathCTS"    # Ljava/lang/String;
    .param p2, "splVersion"    # I
    .param p3, "compressionTypeStr"    # Ljava/lang/String;
    .param p4, "widthAlignTypeStr"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 52
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->setCTSFilePath(Ljava/lang/String;)V

    .line 53
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    .line 54
    invoke-virtual {p0, p3}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->getCompressionTypeValue(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    .line 55
    invoke-virtual {p0, p4}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->getWidthTypeValue(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public getCTSFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->ctsFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getCompressionType()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    return v0
.end method

.method public getCompressionTypeValue(Ljava/lang/String;)I
    .locals 3
    .param p1, "compressionTypeStr"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 63
    if-nez p1, :cond_0

    .line 64
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 79
    :goto_0
    return v0

    .line 68
    :cond_0
    const-string v2, "FBXOR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 69
    goto :goto_0

    .line 70
    :cond_1
    const-string v2, "JBIG"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    const/4 v0, 0x2

    goto :goto_0

    .line 72
    :cond_2
    const-string v2, "Banded JBIG"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 73
    const/4 v0, 0x3

    goto :goto_0

    .line 74
    :cond_3
    const-string v2, "SCANLINE_MODITIFF"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 75
    const/4 v0, 0x4

    goto :goto_0

    .line 78
    :cond_4
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    goto :goto_0
.end method

.method public getSplVersion()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    return v0
.end method

.method public getWidthAlignType()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    return v0
.end method

.method public getWidthTypeValue(Ljava/lang/String;)I
    .locals 3
    .param p1, "widthAlignTypeStr"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 83
    if-nez p1, :cond_0

    .line 84
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 95
    :goto_0
    return v0

    .line 88
    :cond_0
    const-string v2, "Platform"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 89
    goto :goto_0

    .line 90
    :cond_1
    const-string v2, "Fixed"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    const/4 v0, 0x2

    goto :goto_0

    .line 94
    :cond_2
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->ctsFilePath:Ljava/lang/String;

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 170
    .local v0, "errorValue":I
    if-ne v0, v2, :cond_0

    .line 172
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    goto :goto_0
.end method

.method public setCTSFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePathCTSFilePath"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->ctsFilePath:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setCompressionType(I)V
    .locals 0
    .param p1, "compressionType"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    .line 126
    return-void
.end method

.method public setSplVersion(I)V
    .locals 0
    .param p1, "splVersion"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    .line 112
    return-void
.end method

.method public setWidthAlignType(I)V
    .locals 0
    .param p1, "widthAlignType"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    .line 140
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->ctsFilePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->splVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->compressionType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->widthAlignType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError:Z

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 161
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
