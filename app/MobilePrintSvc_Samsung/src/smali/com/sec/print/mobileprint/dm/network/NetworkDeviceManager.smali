.class public Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;
.super Ljava/lang/Object;
.source "NetworkDeviceManager.java"

# interfaces
.implements Lcom/sec/print/mobileprint/dm/IDeviceManager;


# instance fields
.field serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    .line 24
    return-void
.end method


# virtual methods
.method public doCreate()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public doDestroy()V
    .locals 1

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopmDNSDiscovery()V

    .line 35
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPDiscovery()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    .locals 5
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 104
    if-nez p1, :cond_0

    move-object v1, v2

    .line 115
    :goto_0
    return-object v1

    .line 109
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v1, "listLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getLanguageBySNMP(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 112
    goto :goto_0

    .line 113
    .end local v1    # "listLanguages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    .line 115
    goto :goto_0
.end method

.method public getPrinterAlerts(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "ipAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 224
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinterAlertsBySNMP(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 227
    :goto_0
    return-object v1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPrinterName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 171
    const/4 v2, 0x0

    .line 173
    .local v2, "printerName":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 174
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v5, -0x1

    invoke-virtual {v4, p1, v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v3

    .line 175
    .local v3, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v3, :cond_0

    .line 176
    const-string v4, "modelName"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v3    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    :goto_0
    return-object v2

    .line 178
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v2, 0x0

    .line 209
    .local v2, "status":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsPrinterAlive(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    .line 210
    .local v1, "is_alive":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 211
    const-string v2, "idl"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .end local v1    # "is_alive":Z
    :cond_0
    :goto_1
    return-object v2

    .line 209
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getPrinters()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v2, "listPrinter":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinters()Ljava/util/List;

    move-result-object v1

    .line 73
    .local v1, "listNetworkDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 83
    return-object v2

    .line 73
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/df/MFPDevice;

    .line 74
    .local v3, "mfpDevice":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 75
    .local v0, "device":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getNote()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getPort()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setPort(I)V

    .line 79
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNetworkDevice()V

    .line 81
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getScanners()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v2, "listScanner":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getScanners()Ljava/util/List;

    move-result-object v1

    .line 89
    .local v1, "listNetworkDevice":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 100
    return-object v2

    .line 89
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/df/MFPDevice;

    .line 90
    .local v3, "mfpDevice":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;-><init>()V

    .line 91
    .local v0, "device":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setHost(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setName(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;->getNote()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNote(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->setNetworkDevice()V

    .line 97
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 4
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    const/4 v1, -0x1

    .line 158
    if-nez p1, :cond_0

    .line 166
    :goto_0
    return v1

    .line 163
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 164
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsPrinterAlive(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 4
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    const/4 v1, -0x1

    .line 120
    if-nez p1, :cond_0

    .line 128
    :goto_0
    return v1

    .line 125
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 126
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsPrinterColorModelBySNMP(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 6
    .param p1, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 133
    if-nez p1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v2

    .line 138
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 139
    iget-object v4, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDuplexBySNMP(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 140
    .local v0, "duplex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 141
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v3, :cond_2

    move v2, v3

    .line 142
    goto :goto_0

    .line 143
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-ne v4, v3, :cond_0

    .line 144
    const/4 v2, 0x0

    goto :goto_0

    .line 150
    .end local v0    # "duplex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public startDiscovery(Z)I
    .locals 2
    .param p1, "scanners"    # Z

    .prologue
    .line 42
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v0, "vendors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->setVendorsList(Ljava/util/List;)V

    .line 45
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSNMPDiscovery()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 51
    .end local v0    # "vendors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartmDNSDiscovery(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 56
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 52
    :catch_0
    move-exception v1

    goto :goto_1

    .line 46
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public stopDiscovery()I
    .locals 2

    .prologue
    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopmDNSDiscovery()V

    .line 62
    iget-object v1, p0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->serviceMFPDiscovery:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPDiscovery()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    const/4 v1, 0x1

    .line 65
    :goto_0
    return v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method
