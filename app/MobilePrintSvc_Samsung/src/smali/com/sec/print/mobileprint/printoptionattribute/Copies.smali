.class public Lcom/sec/print/mobileprint/printoptionattribute/Copies;
.super Ljava/lang/Object;
.source "Copies.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Copies;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private noCopies:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Copies$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Copies$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "noCopies"    # I

    .prologue
    const/4 v0, 0x1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-ge p1, v0, :cond_0

    .line 32
    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->readFromParcel(Landroid/os/Parcel;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Copies;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Copies;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getCopies()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    .line 87
    return-void
.end method

.method public setCopies(I)V
    .locals 1
    .param p1, "noCopies"    # I

    .prologue
    const/4 v0, 0x1

    .line 52
    if-ge p1, v0, :cond_0

    .line 54
    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Copies;->noCopies:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    return-void
.end method
