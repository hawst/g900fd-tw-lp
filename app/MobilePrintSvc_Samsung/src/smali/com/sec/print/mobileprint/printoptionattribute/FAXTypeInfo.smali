.class public Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;
.super Ljava/lang/Object;
.source "FAXTypeInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final FAX_QUALITY_TYPE_FINE:I = 0x1

.field public static final FAX_QUALITY_TYPE_STANDARD:I = 0x2


# instance fields
.field private faxQualityType:I

.field private strFolderPath:Ljava/lang/String;

.field private strPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 85
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "faxQualityType"    # I
    .param p2, "strFolderPath"    # Ljava/lang/String;
    .param p3, "strPrefix"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->faxQualityType:I

    .line 22
    iput-object p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strFolderPath:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strPrefix:Ljava/lang/String;

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public getFolderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getQualityType()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->faxQualityType:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->faxQualityType:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strFolderPath:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strPrefix:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setFolderPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "strFolderPath"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strFolderPath:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "strPrefix"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strPrefix:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setQualityType(I)V
    .locals 0
    .param p1, "compType"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->faxQualityType:I

    .line 32
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->faxQualityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strFolderPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;->strPrefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    return-void
.end method
