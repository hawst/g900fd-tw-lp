.class public Lcom/sec/print/mobileprint/sf/DialogActivity;
.super Landroid/app/Activity;
.source "DialogActivity.java"


# instance fields
.field message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    sget v3, Lcom/sec/print/mobileprint/sf/R$layout;->dialog_activity:I

    invoke-virtual {p0, v3}, Lcom/sec/print/mobileprint/sf/DialogActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/sf/DialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 54
    .local v1, "start":Landroid/content/Intent;
    const-string v3, "com.sec.print.mobileprint.sf.DialogActivity"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "strArray":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/print/mobileprint/sf/DialogActivity;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget v3, Lcom/sec/print/mobileprint/sf/R$id;->buttonClose:I

    invoke-virtual {p0, v3}, Lcom/sec/print/mobileprint/sf/DialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 58
    .local v0, "buttonClose":Landroid/widget/Button;
    new-instance v3, Lcom/sec/print/mobileprint/sf/DialogActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/print/mobileprint/sf/DialogActivity$1;-><init>(Lcom/sec/print/mobileprint/sf/DialogActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method

.method public setText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 69
    sget v2, Lcom/sec/print/mobileprint/sf/R$id;->text:I

    invoke-virtual {p0, v2}, Lcom/sec/print/mobileprint/sf/DialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    .local v0, "textMessage":Landroid/widget/TextView;
    sget v2, Lcom/sec/print/mobileprint/sf/R$id;->title:I

    invoke-virtual {p0, v2}, Lcom/sec/print/mobileprint/sf/DialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 71
    .local v1, "titleMessage":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method
