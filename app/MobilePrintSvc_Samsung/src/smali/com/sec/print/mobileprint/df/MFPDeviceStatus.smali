.class public Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
.super Ljava/lang/Object;
.source "MFPDeviceStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDeviceStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private detectedErrorStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private status:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->detectedErrorStates:Ljava/util/List;

    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/df/MFPDeviceStatus;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public getDetectedErrorStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->detectedErrorStates:Ljava/util/List;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->status:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->status:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->detectedErrorStates:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 48
    return-void
.end method

.method public setDetectedErrorStates(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "detectedErrorStates":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->detectedErrorStates:Ljava/util/List;

    .line 39
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->status:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->status:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->detectedErrorStates:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 43
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->writeToParcel(Landroid/os/Parcel;)V

    .line 57
    return-void
.end method
