.class public final enum Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;
.super Ljava/lang/Enum;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/pagedata/ImageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumCroppingMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

.field public static final enum CROPPING_ON:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    const-string v1, "CROPPING_OFF"

    .line 45
    invoke-direct {v0, v1, v2, v2}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    .line 46
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    const-string v1, "CROPPING_ON"

    .line 47
    invoke-direct {v0, v1, v3, v3}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_ON:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_ON:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->mValue:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->mValue:I

    return v0
.end method
