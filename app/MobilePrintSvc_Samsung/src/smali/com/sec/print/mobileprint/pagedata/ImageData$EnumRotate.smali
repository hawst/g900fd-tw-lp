.class public final enum Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;
.super Ljava/lang/Enum;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/pagedata/ImageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumRotate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

.field public static final enum ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

.field public static final enum ROTATE_180:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

.field public static final enum ROTATE_270:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

.field public static final enum ROTATE_90:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    const-string v1, "ROTATE_0"

    .line 65
    invoke-direct {v0, v1, v3, v3}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .line 66
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    const-string v1, "ROTATE_90"

    .line 67
    const/16 v2, 0x5a

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_90:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .line 68
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    const-string v1, "ROTATE_180"

    .line 69
    const/16 v2, 0xb4

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_180:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .line 70
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    const-string v1, "ROTATE_270"

    .line 71
    const/16 v2, 0x10e

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_270:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_90:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_180:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_270:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->mValue:I

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ENUM$VALUES:[Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->mValue:I

    return v0
.end method
