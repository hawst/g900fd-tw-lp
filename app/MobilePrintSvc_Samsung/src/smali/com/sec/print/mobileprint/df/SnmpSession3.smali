.class public Lcom/sec/print/mobileprint/df/SnmpSession3;
.super Ljava/lang/Object;
.source "SnmpSession3.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/SnmpSessionBase;


# instance fields
.field private final authPassword:Ljava/lang/String;

.field private final authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

.field private final contextName:Ljava/lang/String;

.field private final inetAddress:Ljava/net/InetAddress;

.field private final port:I

.field private final privPassword:Ljava/lang/String;

.field private final privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field private final retryCount:I

.field private final userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;Ljava/lang/String;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;Ljava/lang/String;II)V
    .locals 0
    .param p1, "inetAddress"    # Ljava/net/InetAddress;
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "contextName"    # Ljava/lang/String;
    .param p4, "authProtocol"    # Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    .param p5, "authPassword"    # Ljava/lang/String;
    .param p6, "privProtocol"    # Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    .param p7, "privPassword"    # Ljava/lang/String;
    .param p8, "retryCount"    # I
    .param p9, "port"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->inetAddress:Ljava/net/InetAddress;

    .line 48
    iput-object p2, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->contextName:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    .line 51
    iput-object p5, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authPassword:Ljava/lang/String;

    .line 52
    iput-object p6, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    .line 53
    iput-object p7, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privPassword:Ljava/lang/String;

    .line 54
    iput p8, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->retryCount:I

    .line 55
    iput p9, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->port:I

    .line 56
    return-void
.end method


# virtual methods
.method public createSnmpPdu()Lorg/snmp4j/PDU;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Lorg/snmp4j/ScopedPDU;

    invoke-direct {v0}, Lorg/snmp4j/ScopedPDU;-><init>()V

    .line 135
    .local v0, "pdu":Lorg/snmp4j/ScopedPDU;
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->contextName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 136
    new-instance v1, Lorg/snmp4j/smi/OctetString;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->contextName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/snmp4j/ScopedPDU;->setContextName(Lorg/snmp4j/smi/OctetString;)V

    .line 138
    :cond_0
    return-object v0
.end method

.method public getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;
    .locals 14
    .param p1, "snmp"    # Lorg/snmp4j/Snmp;

    .prologue
    .line 60
    const/4 v3, 0x0

    .line 61
    .local v3, "authenticationProtocol":Lorg/snmp4j/smi/OID;
    const/4 v10, 0x0

    .line 62
    .local v10, "authenticationPassphrase":Lorg/snmp4j/smi/OctetString;
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;->MD5:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    if-ne v0, v1, :cond_9

    .line 63
    sget-object v3, Lorg/snmp4j/security/AuthMD5;->ID:Lorg/snmp4j/smi/OID;

    .line 66
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authPassword:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 67
    new-instance v10, Lorg/snmp4j/smi/OctetString;

    .end local v10    # "authenticationPassphrase":Lorg/snmp4j/smi/OctetString;
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authPassword:Ljava/lang/String;

    invoke-direct {v10, v0}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    .line 69
    .restart local v10    # "authenticationPassphrase":Lorg/snmp4j/smi/OctetString;
    :cond_1
    const/4 v5, 0x0

    .line 70
    .local v5, "privacyProtocol":Lorg/snmp4j/smi/OID;
    const/4 v12, 0x0

    .line 71
    .local v12, "privacyPassphrase":Lorg/snmp4j/smi/OctetString;
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    if-ne v0, v1, :cond_a

    .line 72
    sget-object v5, Lorg/snmp4j/security/PrivDES;->ID:Lorg/snmp4j/smi/OID;

    .line 81
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privPassword:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 82
    new-instance v12, Lorg/snmp4j/smi/OctetString;

    .end local v12    # "privacyPassphrase":Lorg/snmp4j/smi/OctetString;
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privPassword:Ljava/lang/String;

    invoke-direct {v12, v0}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v12    # "privacyPassphrase":Lorg/snmp4j/smi/OctetString;
    :cond_3
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 84
    const/4 v4, 0x0

    .line 85
    .local v4, "autheticationKey":[B
    const/4 v6, 0x0

    .line 87
    .local v6, "privacyKey":[B
    if-eqz v10, :cond_4

    :try_start_0
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Lorg/snmp4j/smi/OctetString;->startsWith(Lorg/snmp4j/smi/OctetString;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    invoke-virtual {v10}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/codec/binary/Hex;->decodeHex([C)[B

    move-result-object v4

    .line 90
    :cond_4
    if-eqz v12, :cond_5

    new-instance v0, Lorg/snmp4j/smi/OctetString;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Lorg/snmp4j/smi/OctetString;->startsWith(Lorg/snmp4j/smi/OctetString;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 91
    invoke-virtual {v12}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/codec/binary/Hex;->decodeHex([C)[B
    :try_end_0
    .catch Lorg/apache/commons/codec/DecoderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 97
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lorg/snmp4j/Snmp;->getUSM()Lorg/snmp4j/security/USM;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/USM;->removeAllUsers()V

    .line 98
    if-nez v4, :cond_6

    if-eqz v6, :cond_e

    .line 99
    :cond_6
    invoke-virtual {p1}, Lorg/snmp4j/Snmp;->getUSM()Lorg/snmp4j/security/USM;

    move-result-object v0

    .line 100
    const/4 v1, 0x0

    new-array v1, v1, [B

    new-instance v2, Lorg/snmp4j/smi/OctetString;

    iget-object v7, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    invoke-direct {v2, v7}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v6}, Lorg/snmp4j/security/USM;->addLocalizedUser([BLorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;[BLorg/snmp4j/smi/OID;[B)Lorg/snmp4j/security/UsmUserEntry;

    .line 111
    .end local v4    # "autheticationKey":[B
    .end local v6    # "privacyKey":[B
    :cond_7
    :goto_3
    new-instance v13, Lorg/snmp4j/UserTarget;

    invoke-direct {v13}, Lorg/snmp4j/UserTarget;-><init>()V

    .line 112
    .local v13, "target":Lorg/snmp4j/UserTarget;
    const/4 v0, 0x3

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setVersion(I)V

    .line 113
    new-instance v0, Lorg/snmp4j/smi/UdpAddress;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->inetAddress:Ljava/net/InetAddress;

    iget v2, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->port:I

    invoke-direct {v0, v1, v2}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setAddress(Lorg/snmp4j/smi/Address;)V

    .line 114
    if-nez v3, :cond_f

    .line 115
    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setSecurityLevel(I)V

    .line 125
    :goto_4
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 126
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setSecurityName(Lorg/snmp4j/smi/OctetString;)V

    .line 127
    :cond_8
    iget v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->retryCount:I

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setRetries(I)V

    .line 128
    const-wide/16 v0, 0x5dc

    invoke-virtual {v13, v0, v1}, Lorg/snmp4j/UserTarget;->setTimeout(J)V

    .line 129
    return-object v13

    .line 64
    .end local v5    # "privacyProtocol":Lorg/snmp4j/smi/OID;
    .end local v12    # "privacyPassphrase":Lorg/snmp4j/smi/OctetString;
    .end local v13    # "target":Lorg/snmp4j/UserTarget;
    :cond_9
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;->SHA:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    if-ne v0, v1, :cond_0

    .line 65
    sget-object v3, Lorg/snmp4j/security/AuthSHA;->ID:Lorg/snmp4j/smi/OID;

    goto/16 :goto_0

    .line 73
    .restart local v5    # "privacyProtocol":Lorg/snmp4j/smi/OID;
    .restart local v12    # "privacyPassphrase":Lorg/snmp4j/smi/OctetString;
    :cond_a
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES3:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    if-ne v0, v1, :cond_b

    .line 74
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->ID:Lorg/snmp4j/smi/OID;

    goto/16 :goto_1

    .line 75
    :cond_b
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES128:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    if-ne v0, v1, :cond_c

    .line 76
    sget-object v5, Lorg/snmp4j/security/PrivAES128;->ID:Lorg/snmp4j/smi/OID;

    goto/16 :goto_1

    .line 77
    :cond_c
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES192:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    if-ne v0, v1, :cond_d

    .line 78
    sget-object v5, Lorg/snmp4j/security/PrivAES192;->ID:Lorg/snmp4j/smi/OID;

    goto/16 :goto_1

    .line 79
    :cond_d
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES256:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    if-ne v0, v1, :cond_2

    .line 80
    sget-object v5, Lorg/snmp4j/security/PrivAES256;->ID:Lorg/snmp4j/smi/OID;

    goto/16 :goto_1

    .line 104
    .restart local v4    # "autheticationKey":[B
    .restart local v6    # "privacyKey":[B
    :cond_e
    invoke-virtual {p1}, Lorg/snmp4j/Snmp;->getUSM()Lorg/snmp4j/security/USM;

    move-result-object v0

    .line 106
    new-instance v1, Lorg/snmp4j/smi/OctetString;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    .line 107
    new-instance v7, Lorg/snmp4j/security/UsmUser;

    new-instance v8, Lorg/snmp4j/smi/OctetString;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/SnmpSession3;->userName:Ljava/lang/String;

    invoke-direct {v8, v2}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    move-object v9, v3

    move-object v11, v5

    invoke-direct/range {v7 .. v12}, Lorg/snmp4j/security/UsmUser;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;)V

    .line 105
    invoke-virtual {v0, v1, v7}, Lorg/snmp4j/security/USM;->addUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V

    goto/16 :goto_3

    .line 118
    .end local v4    # "autheticationKey":[B
    .end local v6    # "privacyKey":[B
    .restart local v13    # "target":Lorg/snmp4j/UserTarget;
    :cond_f
    if-nez v5, :cond_10

    .line 119
    const/4 v0, 0x2

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setSecurityLevel(I)V

    goto :goto_4

    .line 122
    :cond_10
    const/4 v0, 0x3

    invoke-virtual {v13, v0}, Lorg/snmp4j/UserTarget;->setSecurityLevel(I)V

    goto :goto_4

    .line 94
    .end local v13    # "target":Lorg/snmp4j/UserTarget;
    .restart local v4    # "autheticationKey":[B
    .restart local v6    # "privacyKey":[B
    :catch_0
    move-exception v0

    goto/16 :goto_2
.end method
