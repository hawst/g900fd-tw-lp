.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
.super Ljava/lang/Enum;
.source "NUP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/NUP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumNUP"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_16UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_1UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_2UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_4UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_6UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

.field public static final enum LAYOUT_9UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 7
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_1UP"

    invoke-direct {v0, v1, v7, v4}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_1UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 8
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_2UP"

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_2UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 9
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_4UP"

    invoke-direct {v0, v1, v5, v6}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_4UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 10
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_6UP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_6UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 11
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_9UP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_9UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 12
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    const-string v1, "LAYOUT_16UP"

    const/4 v2, 0x5

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_16UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_1UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_2UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_4UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_6UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_9UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_16UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->mValue:I

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->mValue:I

    return v0
.end method
