.class public Lcom/sec/print/mobileprint/sf/scandefs;
.super Ljava/lang/Object;
.source "scandefs.java"


# static fields
.field public static final A4:I = 0x1

.field public static final A5:I = 0x5

.field public static final Executive:I = 0x4

.field public static final Legal:I = 0x6

.field public static final Letter:I = 0x2

.field public static final Statement:I = 0x3

.field public static final W_MM_PER_10_INCHES:I = 0xfe

.field public static final W_PXU_PER_10_INCHES:I = 0x2ee0

.field public static final W_PXU_PER_INCH:I = 0x4b0

.field public static final kADF:I = 0x2

.field public static final kAccountingType_Accounting:I = 0x1

.field public static final kAccountingType_IDOnly:I = 0x2

.field public static final kAccountingType_Undefined:I = 0x0

.field public static final kAllPagesScaned:I = 0x11

.field public static final kAuthError:I = 0xd

.field public static final kAuto:I = 0x8

.field public static final kBadColor:I = -0x16

.field public static final kBadOriginX:I = -0x1a

.field public static final kBadOriginY:I = -0x1b

.field public static final kBadResolution:I = -0x17

.field public static final kBadScale:I = -0x18

.field public static final kBadSource:I = -0x19

.field public static final kBanknote:I = 0x14

.field public static final kBusy:I = 0x7

.field public static final kCanNotCreateCapabilities:I = -0xf

.field public static final kCloseError:I = -0x2

.field public static final kConversionFileError:I = -0x2a

.field public static final kConversionNoFile:I = -0x29

.field public static final kCoverOpen:I = 0x5

.field public static final kCustomArea:I = 0x1

.field public static final kDeviceAccessError:I = 0x15

.field public static final kDeviceIOError:I = 0x16

.field public static final kDeviceTimeout:I = 0x17

.field public static final kDuplex:I = 0x4

.field public static final kEighth:I = 0x8

.field public static final kEmptyPassword:I = 0xe

.field public static final kExceedQuota:I = 0x10

.field public static final kFaltbed:I = 0x1

.field public static final kFileCloseError:I = -0x1f

.field public static final kFileError:I = -0xb

.field public static final kFileType_BMP:I = 0x5

.field public static final kFileType_JPEG:I = 0x1

.field public static final kFileType_PNG:I = 0x2

.field public static final kFileType_TIFF:I = 0x4

.field public static final kFitToPage:I = 0x0

.field public static final kFlushError:I = -0x1e

.field public static final kGRAY:I = 0x2

.field public static final kHalf:I = 0x2

.field public static final kHightRes:I = 0x4

.field public static final kInvalidArea:I = 0x8

.field public static final kInvalidParam:I = 0x9

.field public static final kInvalidUser:I = 0xb

.field public static final kInvalidePasswd:I = 0xc

.field public static final kJam:I = 0x4

.field public static final kJpegPrepareError:I = -0x2b

.field public static final kJpegPrepareNoFile:I = -0x29

.field public static final kJpegPrepareNoMemory:I = -0x28

.field public static final kJpegPrepateFileError:I = -0x2a

.field public static final kLocked:I = 0x6

.field public static final kLowMemory:I = -0xa

.field public static final kLowRes:I = 0x1

.field public static final kMakeDirError:I = -0x1c

.field public static final kMissingDocumentDirectory:I = -0x6

.field public static final kNoDocument:I = 0x3

.field public static final kNoExternalStorage:I = -0x12

.field public static final kNoFilledCapabilities:I = -0x4

.field public static final kNoGetCapabilities:I = -0x3

.field public static final kNoImageBlock:I = -0x32

.field public static final kNoMemory:I = -0x7

.field public static final kNoMemoryPreview:I = -0x9

.field public static final kNoPermission:I = 0xf

.field public static final kNoServiceAndCapabilities:I = -0x10

.field public static final kNoSuportedColorConversion:I = -0x21

.field public static final kNoSupportedColor:I = -0x8

.field public static final kNoWritableExternalStorage:I = -0x13

.field public static final kNormRes:I = 0x2

.field public static final kNotSupported:I = 0x18

.field public static final kNotSupportedByDevice:I = 0x19

.field public static final kOK:I = 0x0

.field public static final kOne:I = 0x1

.field public static final kOpenError:I = -0x1

.field public static final kPaperArea:I = 0x0

.field public static final kParamError:I = -0xe

.field public static final kPermission_Default:I = 0x0

.field public static final kPermission_Group:I = 0x1

.field public static final kPermission_User:I = 0x0

.field public static final kPhotoExportError:I = -0x11

.field public static final kPngPrepareError:I = -0x2e

.field public static final kPngPrepareNoMemory:I = -0x2d

.field public static final kQuarter:I = 0x4

.field public static final kRBG:I = 0x1

.field public static final kRGB565:I = 0x4

.field public static final kRGBA4444:I = 0x5

.field public static final kRGBA8888:I = 0x3

.field public static final kScanHeightMorethenSource:I = -0x15

.field public static final kScanWidthMoreThenSource:I = -0x14

.field public static final kSecurityError:I = 0xa

.field public static final kServiceNotStarter:I = -0xc

.field public static final kServiceRunning:I = -0xd

.field public static final kSessionOpen:I = -0x20

.field public static final kSessionOpenError:I = -0x5

.field public static final kStatusBusy:I = 0x13

.field public static final kTimeOut:I = 0x12

.field public static final kUnitTestMessage:I = 0x12fd1

.field public static final kUnknownError:I = 0x2

.field public static final kUserCancel:I = 0x1

.field public static final kWriteError:I = -0x1d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
