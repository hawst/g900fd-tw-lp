.class public Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
.super Lcom/sec/print/mobileprint/df/DiscoveryBase;
.source "DiscoveryIpRange.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;
    }
.end annotation


# instance fields
.field private final addressScopeId:I

.field private final checkedAddresses:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final concurrentRequestsNumber:I

.field private firstNonCheckedAddress:Ljava/net/InetAddress;

.field private final fromAddress:[I

.field private percentComplete:D

.field private progress:D

.field private final semaphore:Ljava/util/concurrent/Semaphore;

.field private snmp1Discovery:Z

.field private snmp1ReadComunity:Ljava/lang/String;

.field private snmp2Discovery:Z

.field private snmp2ReadComunity:Ljava/lang/String;

.field private snmp3ContextName:Ljava/lang/String;

.field private snmp3Discovery:Z

.field private final timeout:J

.field private final toAddress:[I


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;Ljava/net/InetAddress;JII)V
    .locals 5
    .param p1, "fromAddress"    # Ljava/net/InetAddress;
    .param p2, "toAddress"    # Ljava/net/InetAddress;
    .param p3, "timeout"    # J
    .param p5, "concurrentThreadsNumber"    # I
    .param p6, "concurrentRequestsNumber"    # I

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 90
    invoke-direct {p0, p6, p5}, Lcom/sec/print/mobileprint/df/DiscoveryBase;-><init>(II)V

    .line 24
    iput-wide v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D

    .line 25
    iput-wide v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->progress:D

    .line 27
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;

    .line 31
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1Discovery:Z

    .line 34
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2Discovery:Z

    .line 37
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp3Discovery:Z

    .line 91
    iput-wide p3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->timeout:J

    .line 92
    iput p6, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->concurrentRequestsNumber:I

    .line 93
    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, p6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;

    .line 94
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;

    .line 95
    invoke-static {p1}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressToArray(Ljava/net/InetAddress;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    .line 96
    invoke-static {p2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressToArray(Ljava/net/InetAddress;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    .line 97
    instance-of v1, p1, Ljava/net/Inet6Address;

    if-eqz v1, :cond_0

    .line 98
    check-cast p1, Ljava/net/Inet6Address;

    .end local p1    # "fromAddress":Ljava/net/InetAddress;
    invoke-virtual {p1}, Ljava/net/Inet6Address;->getScopeId()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressScopeId:I

    .line 102
    :goto_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    array-length v2, v2

    if-eq v1, v2, :cond_1

    .line 103
    const-string v1, "DiscoveryIpRange"

    const-string v2, "fromAddress length is not equal to toAddress length"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 100
    .restart local p1    # "fromAddress":Ljava/net/InetAddress;
    :cond_0
    iput v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressScopeId:I

    goto :goto_0

    .line 106
    .end local p1    # "fromAddress":Ljava/net/InetAddress;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    invoke-static {v1}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->increaseAddressArray([I)V

    .line 116
    return-void

    .line 107
    :cond_3
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_4

    .line 108
    const-string v1, "DiscoveryIpRange"

    const-string v2, "fromAddress is greater than toAddress"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 111
    :cond_4
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    aget v2, v2, v0

    if-lt v1, v2, :cond_2

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic access$10(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->timeout:J

    return-wide v0
.end method

.method static synthetic access$11(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1ReadComunity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2ReadComunity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)D
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D

    return-wide v0
.end method

.method static synthetic access$14(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)D
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->progress:D

    return-wide v0
.end method

.method static synthetic access$15(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;D)V
    .locals 0

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D

    return-void
.end method

.method static synthetic access$16(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;

    return-object v0
.end method

.method static synthetic access$18(Ljava/net/InetAddress;)[I
    .locals 1

    .prologue
    .line 163
    invoke-static {p0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressToArray(Ljava/net/InetAddress;)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19([I)V
    .locals 0

    .prologue
    .line 188
    invoke-static {p0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->increaseAddressArray([I)V

    return-void
.end method

.method static synthetic access$20(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressScopeId:I

    return v0
.end method

.method static synthetic access$21([II)Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 172
    invoke-static {p0, p1}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->arrayToAddress([II)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;Ljava/net/InetAddress;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1Discovery:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2Discovery:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp3Discovery:Z

    return v0
.end method

.method private static addressToArray(Ljava/net/InetAddress;)[I
    .locals 4
    .param p0, "address"    # Ljava/net/InetAddress;

    .prologue
    .line 164
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 165
    .local v1, "byteArray":[B
    array-length v3, v1

    new-array v0, v3, [I

    .line 166
    .local v0, "array":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-lt v2, v3, :cond_0

    .line 169
    return-object v0

    .line 167
    :cond_0
    aget-byte v3, v1, v2

    and-int/lit16 v3, v3, 0xff

    aput v3, v0, v2

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static arrayToAddress([II)Ljava/net/InetAddress;
    .locals 5
    .param p0, "array"    # [I
    .param p1, "scopeId"    # I

    .prologue
    .line 173
    array-length v3, p0

    new-array v2, v3, [B

    .line 174
    .local v2, "result":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-lt v1, v3, :cond_0

    .line 178
    :try_start_0
    array-length v3, v2

    const/16 v4, 0x10

    if-ne v3, v4, :cond_1

    if-lez p1, :cond_1

    .line 179
    const/4 v3, 0x0

    invoke-static {v3, v2, p1}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BI)Ljava/net/Inet6Address;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 181
    :goto_1
    return-object v3

    .line 175
    :cond_0
    aget v3, p0, v1

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :cond_1
    :try_start_1
    invoke-static {v2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/net/UnknownHostException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static increaseAddressArray([I)V
    .locals 3
    .param p0, "array"    # [I

    .prologue
    .line 189
    array-length v1, p0

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 197
    :goto_1
    return-void

    .line 190
    :cond_0
    aget v1, p0, v0

    const/16 v2, 0xff

    if-ne v1, v2, :cond_1

    .line 191
    const/4 v1, 0x0

    aput v1, p0, v0

    .line 189
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 193
    :cond_1
    aget v1, p0, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, p0, v0

    goto :goto_1
.end method

.method private static rangeSize([I[I)I
    .locals 5
    .param p0, "from"    # [I
    .param p1, "to"    # [I

    .prologue
    .line 200
    const/4 v1, 0x1

    .local v1, "m":I
    const/4 v2, 0x0

    .line 201
    .local v2, "result":I
    array-length v3, p0

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 205
    return v2

    .line 202
    :cond_0
    aget v3, p1, v0

    aget v4, p0, v0

    sub-int/2addr v3, v4

    mul-int/2addr v3, v1

    add-int/2addr v2, v3

    .line 203
    mul-int/lit16 v1, v1, 0x100

    .line 201
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    return-void
.end method

.method public enableSnmp1(Ljava/lang/String;)V
    .locals 1
    .param p1, "readComunity"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1Discovery:Z

    .line 120
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1ReadComunity:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public enableSnmp2(Ljava/lang/String;)V
    .locals 1
    .param p1, "readComunity"    # Ljava/lang/String;

    .prologue
    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2Discovery:Z

    .line 125
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2ReadComunity:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public enableSnmp3(Ljava/lang/String;)V
    .locals 1
    .param p1, "contextName"    # Ljava/lang/String;

    .prologue
    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp3Discovery:Z

    .line 130
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp3ContextName:Ljava/lang/String;

    .line 131
    return-void
.end method

.method protected getDiscoveryPercentComplete()D
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D

    return-wide v0
.end method

.method public getFirstNonCheckedAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;

    return-object v0
.end method

.method public bridge synthetic getPercentComplete()D
    .locals 2

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->getPercentComplete()D

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic removeDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->removeDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 139
    iget v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->concurrentRequestsNumber:I

    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 141
    .local v2, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    invoke-static {v3, v4}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->rangeSize([I[I)I

    move-result v3

    int-to-double v3, v3

    div-double v3, v6, v3

    iput-wide v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->progress:D

    .line 142
    :goto_0
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->isStopped()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->toAddress:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 148
    :cond_0
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 149
    const-wide v3, 0x7fffffffffffffffL

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    :goto_1
    iput-wide v6, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D

    .line 157
    return-void

    .line 143
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    iget v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressScopeId:I

    invoke-static {v3, v4}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->arrayToAddress([II)Ljava/net/InetAddress;

    move-result-object v0

    .line 144
    .local v0, "address":Ljava/net/InetAddress;
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 145
    new-instance v3, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;

    invoke-direct {v3, p0, v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;-><init>(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;Ljava/net/InetAddress;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 146
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->fromAddress:[I

    invoke-static {v3}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->increaseAddressArray([I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 150
    .end local v0    # "address":Ljava/net/InetAddress;
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 152
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 153
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 154
    .local v1, "e":Ljava/util/concurrent/RejectedExecutionException;
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_1
.end method

.method public bridge synthetic start()V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->start()V

    return-void
.end method

.method public bridge synthetic stop()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->stop()V

    return-void
.end method

.method public bridge synthetic waitDiscovery()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lcom/sec/print/mobileprint/df/DiscoveryBase;->waitDiscovery()V

    return-void
.end method
