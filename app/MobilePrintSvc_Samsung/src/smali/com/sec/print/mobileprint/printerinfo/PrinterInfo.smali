.class public Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
.super Ljava/lang/Object;
.source "PrinterInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private modelName:Ljava/lang/String;

.field private outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

.field private supportedCMDCollate:I

.field private supportedColor:I

.field private supportedDuplex:I

.field private supportedMediaSizeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;"
        }
    .end annotation
.end field

.field private supportedMediaTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private supportedPDLTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 46
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 47
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 48
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 49
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/sec/print/mobileprint/printerinfo/IOutputInfo;)V
    .locals 2
    .param p1, "modelName"    # Ljava/lang/String;
    .param p2, "pdlType"    # Ljava/lang/String;
    .param p3, "supportedColor"    # Z
    .param p4, "outputInfo"    # Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 65
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 67
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 69
    invoke-direct {p0, p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 70
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 71
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/sec/print/mobileprint/printerinfo/IOutputInfo;ZZ)V
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;
    .param p2, "pdlType"    # Ljava/lang/String;
    .param p3, "supportedColor"    # Z
    .param p4, "outputInfo"    # Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    .param p5, "supportedCMDCollate"    # Z
    .param p6, "supportedDuplex"    # Z

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 87
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 89
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 91
    invoke-direct {p0, p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 92
    invoke-direct {p0, p5}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 93
    invoke-direct {p0, p6}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/sec/print/mobileprint/printerinfo/IOutputInfo;)V
    .locals 2
    .param p1, "modelName"    # Ljava/lang/String;
    .param p3, "supportedColor"    # Z
    .param p4, "outputInfo"    # Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "supportedPDLTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 109
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 111
    iput-object p2, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 112
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 113
    invoke-direct {p0, p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 114
    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/sec/print/mobileprint/printerinfo/IOutputInfo;Z)V
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;
    .param p3, "supportedColor"    # Z
    .param p4, "outputInfo"    # Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    .param p5, "supportedCMDCollate"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, "supportedPDLTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 27
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 28
    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 130
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 132
    iput-object p2, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 133
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 134
    invoke-direct {p0, p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 135
    invoke-direct {p0, p5}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 139
    return-void
.end method

.method private booleanToInt(Z)I
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 208
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private intToBoolean(I)Z
    .locals 1
    .param p1, "value"    # I

    .prologue
    const/4 v0, 0x1

    .line 213
    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputInfo()Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    return-object v0
.end method

.method public getSupportedMediaSizeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSupportedMediaTypeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSupportedPDLTypeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isSupportedCMDCollate()Z
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSupportedColor()Z
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSupportedDuplex()Z
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->intToBoolean(I)Z

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 237
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 239
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 243
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 244
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 245
    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setOutputInfo(Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;)V
    .locals 0
    .param p1, "outputInfo"    # Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    .line 155
    return-void
.end method

.method public setSupportedCMDCollate(Z)V
    .locals 1
    .param p1, "supportedCMDCollate"    # Z

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    .line 175
    return-void
.end method

.method public setSupportedColor(Z)V
    .locals 1
    .param p1, "supportedColor"    # Z

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    .line 167
    return-void
.end method

.method public setSupportedDuplex(Z)V
    .locals 1
    .param p1, "supportedDuplex"    # Z

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->booleanToInt(Z)I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    .line 183
    return-void
.end method

.method public setSupportedMediaSizeList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "mediaSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    .line 199
    return-void
.end method

.method public setSupportedMediaTypeList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "mediaTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    .line 191
    return-void
.end method

.method public setSupportedPDLTypeList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "supportedPDLTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    .line 163
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->modelName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedPDLTypeList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 226
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->outputInfo:Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 227
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 228
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedCMDCollate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedDuplex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 230
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaTypeList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 231
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->supportedMediaSizeList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 232
    return-void
.end method
