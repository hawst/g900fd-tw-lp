.class public Lcom/sec/print/mobileprint/pagedata/K2MImageData;
.super Lcom/sec/print/mobileprint/pagedata/ImageData;
.source "K2MImageData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/K2MImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private handleK2Mobile:I

.field private pageNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/K2MImageData$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/pagedata/K2MImageData$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "handleK2Mobile"    # I
    .param p2, "pageNum"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    .line 53
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->handleK2Mobile:I

    .line 54
    iput p2, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->readFromParcel(Landroid/os/Parcel;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/pagedata/K2MImageData;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/pagedata/K2MImageData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getHandleK2Mobile()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->handleK2Mobile:I

    return v0
.end method

.method public getPageNum()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/pagedata/ImageData;->readFromParcel(Landroid/os/Parcel;)V

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->handleK2Mobile:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    .line 88
    return-void
.end method

.method public setHandleK2Mobile(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->handleK2Mobile:I

    .line 59
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/sec/print/mobileprint/pagedata/ImageData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 79
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->handleK2Mobile:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/K2MImageData;->pageNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    return-void
.end method
