.class Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$1;
.super Ljava/lang/Object;
.source "DiscoveryWifiDirect.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$1;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 8
    .param p1, "arg0"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 33
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v1

    .line 34
    .local v1, "deviceList":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 67
    return-void

    .line 34
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 41
    .local v0, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v6, "3-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 57
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v6, "-5"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    .line 58
    iget-object v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v7, "-4"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    .line 57
    or-int/2addr v5, v6

    .line 59
    iget-object v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v7, "-2"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    .line 57
    or-int v3, v5, v6

    .line 60
    .local v3, "scanner":Z
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v6, "-5"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    .line 61
    iget-object v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v7, "-4"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    .line 60
    or-int/2addr v5, v6

    .line 62
    iget-object v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    const-string v7, "-3"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    .line 60
    or-int v2, v5, v6

    .line 64
    .local v2, "fax":Z
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$1;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->discoveryListener:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;
    invoke-static {v5}, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->access$0(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;)Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;

    move-result-object v5

    iget-object v6, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v7, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-interface {v5, v6, v7, v3, v2}, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;->wifiDirectNewDeviceFound(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0
.end method
