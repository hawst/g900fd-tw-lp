.class public Lcom/sec/print/mobileprint/printoptionattribute/Orientation;
.super Ljava/lang/Object;
.source "Orientation.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Orientation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private orientationType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->readFromParcel(Landroid/os/Parcel;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Orientation;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V
    .locals 1
    .param p1, "eOrientation"    # Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->orientationType:I

    .line 61
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public getOrientation()Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;
    .locals 6

    .prologue
    .line 75
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 83
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 75
    :cond_1
    aget-object v0, v1, v2

    .line 77
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->orientationType:I

    if-eq v4, v5, :cond_0

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->orientationType:I

    .line 101
    return-void
.end method

.method public setOrientation(Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;)V
    .locals 1
    .param p1, "eOrientation"    # Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Orientation$EnumOrientation;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->orientationType:I

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Orientation;->orientationType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-void
.end method
