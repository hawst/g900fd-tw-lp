.class Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;
.super Ljava/lang/Object;
.source "MFPDiscoveryService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->serviceAdded(Ljavax/jmdns/ServiceEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

.field private final synthetic val$event:Ljavax/jmdns/ServiceEvent;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;Ljavax/jmdns/ServiceEvent;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    iput-object p2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->val$event:Ljavax/jmdns/ServiceEvent;

    .line 1535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1538
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1549
    :goto_0
    return-void

    .line 1539
    :cond_1
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    iget-object v2, v2, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->jmdns:Ljavax/jmdns/JmDNS;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->val$event:Ljavax/jmdns/ServiceEvent;

    invoke-virtual {v3}, Ljavax/jmdns/ServiceEvent;->getType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->val$event:Ljavax/jmdns/ServiceEvent;

    invoke-virtual {v4}, Ljavax/jmdns/ServiceEvent;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljavax/jmdns/JmDNS;->getServiceInfo(Ljava/lang/String;Ljava/lang/String;)Ljavax/jmdns/ServiceInfo;

    move-result-object v1

    .line 1540
    .local v1, "info":Ljavax/jmdns/ServiceInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljavax/jmdns/ServiceInfo;->hasData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1541
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;->val$event:Ljavax/jmdns/ServiceEvent;

    invoke-virtual {v3}, Ljavax/jmdns/ServiceEvent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->addDevice(Ljava/lang/String;Ljavax/jmdns/ServiceInfo;)V

    .line 1542
    const-string v2, "MFPDiscoveryService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deviceListener] serviceResolved: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1546
    .end local v1    # "info":Ljavax/jmdns/ServiceInfo;
    :catch_0
    move-exception v0

    .line 1547
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
