.class Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo$1;
.super Ljava/lang/Object;
.source "MPUSBOutputInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;-><init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 102
    new-array v0, p1, [Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo$1;->newArray(I)[Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    move-result-object v0

    return-object v0
.end method
