.class public Lcom/sec/print/mobileprint/PrintJob;
.super Ljava/lang/Object;
.source "PrintJob.java"


# static fields
.field private static final CTS_FILES_FOLDER:Ljava/lang/String; = "cts_files"

.field private static final MODEL_INFO_XML:Ljava/lang/String; = "mobileprint_model_list.xml"

.field public static final TEMP_FOLDER_PATH:Ljava/lang/String;


# instance fields
.field private assetMgr:Landroid/content/res/AssetManager;

.field private context:Landroid/content/Context;

.field private cratedTempFileList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private curLoadedModelName:Ljava/lang/String;

.field private deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

.field private isRun:Z

.field private pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

.field private requestCancel:Z

.field private statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

.field private final syncCancel:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    .line 71
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 72
    const-string v1, "/SamsungMobilePrint/temp/PrintService/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/print/mobileprint/PrintingStatusMonitor;Landroid/content/res/AssetManager;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "statusMonitor"    # Lcom/sec/print/mobileprint/PrintingStatusMonitor;
    .param p3, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 62
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    .line 124
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    .line 125
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    .line 89
    iput-object p2, p0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 90
    iput-object p3, p0, Lcom/sec/print/mobileprint/PrintJob;->assetMgr:Landroid/content/res/AssetManager;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->cratedTempFileList:Ljava/util/LinkedList;

    .line 92
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/PrintJob;->isRun:Z

    .line 94
    iput-object p1, p0, Lcom/sec/print/mobileprint/PrintJob;->context:Landroid/content/Context;

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/PrintingStatusMonitor;Landroid/content/res/AssetManager;)V
    .locals 3
    .param p1, "statusMonitor"    # Lcom/sec/print/mobileprint/PrintingStatusMonitor;
    .param p2, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 62
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    .line 124
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    .line 125
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    .line 79
    iput-object p1, p0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 80
    iput-object p2, p0, Lcom/sec/print/mobileprint/PrintJob;->assetMgr:Landroid/content/res/AssetManager;

    .line 81
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->cratedTempFileList:Ljava/util/LinkedList;

    .line 82
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 83
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/PrintJob;->isRun:Z

    .line 84
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->context:Landroid/content/Context;

    .line 85
    return-void
.end method

.method private checkValidation(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;)Z
    .locals 4
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;

    .prologue
    .line 672
    const/4 v1, 0x1

    .line 674
    .local v1, "result":Z
    const-class v2, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    invoke-virtual {p1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 675
    .local v0, "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    if-nez v0, :cond_1

    .line 676
    const/4 v1, 0x0

    .line 690
    :cond_0
    :goto_0
    return v1

    .line 678
    :cond_1
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->getPDLType()Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    move-result-object v2

    sget-object v3, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_DIRECTPRINT:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    if-ne v2, v3, :cond_2

    .line 679
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/print/mobileprint/pagedata/DirectPrintDocSet;

    if-eq v2, v3, :cond_0

    .line 680
    const/4 v1, 0x0

    .line 682
    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->getPDLType()Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    move-result-object v2

    sget-object v3, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    if-eq v2, v3, :cond_3

    .line 683
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->getPDLType()Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    move-result-object v2

    sget-object v3, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_SPL:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    if-ne v2, v3, :cond_0

    .line 684
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/print/mobileprint/pagedata/PageSet;

    if-eq v2, v3, :cond_0

    .line 685
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private copyCTS2SDCard(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "filePathCTS"    # Ljava/lang/String;

    .prologue
    const/16 v13, 0x400

    const/4 v10, 0x0

    .line 956
    new-instance v11, Ljava/lang/StringBuilder;

    sget-object v12, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 957
    .local v5, "newFilePathOnSDCard":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    .line 958
    .local v9, "tempFolderPath":Ljava/io/File;
    if-nez v9, :cond_1

    .line 959
    const-string v5, ""

    .line 1013
    .end local v5    # "newFilePathOnSDCard":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v5

    .line 961
    .restart local v5    # "newFilePathOnSDCard":Ljava/lang/String;
    :cond_1
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_2

    .line 962
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    move-result v11

    if-nez v11, :cond_2

    .line 963
    const-string v10, "ERROR - mkdirs"

    invoke-static {p0, v10}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 964
    const-string v5, ""

    goto :goto_0

    .line 968
    :cond_2
    new-array v0, v13, [B

    .line 969
    .local v0, "arrBuffer":[B
    const/4 v6, 0x0

    .line 970
    .local v6, "offset":I
    const/4 v4, 0x0

    .line 971
    .local v4, "lengthOfRead":I
    const/4 v7, 0x0

    .line 974
    .local v7, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 975
    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    .local v8, "outputStream":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/PrintJob;->assetMgr:Landroid/content/res/AssetManager;

    invoke-virtual {v11, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 978
    .local v3, "inputStream":Ljava/io/InputStream;
    :goto_1
    const/4 v11, 0x0

    const/16 v12, 0x400

    invoke-virtual {v3, v0, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 979
    const/4 v11, -0x1

    if-ne v4, v11, :cond_3

    .line 986
    iget-object v11, p0, Lcom/sec/print/mobileprint/PrintJob;->cratedTempFileList:Ljava/util/LinkedList;

    invoke-virtual {v11, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 988
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 989
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1002
    if-eqz v8, :cond_0

    .line 1004
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1005
    :catch_0
    move-exception v1

    .line 1007
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1008
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {p0, v11}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v10

    .line 1009
    goto :goto_0

    .line 982
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    add-int/2addr v6, v4

    .line 983
    const/4 v11, 0x0

    :try_start_3
    invoke-virtual {v8, v0, v11, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 990
    .end local v3    # "inputStream":Ljava/io/InputStream;
    :catch_1
    move-exception v2

    move-object v7, v8

    .line 992
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .local v2, "e1":Ljava/io/FileNotFoundException;
    .restart local v7    # "outputStream":Ljava/io/FileOutputStream;
    :goto_2
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 993
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {p0, v11}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v10

    .line 994
    goto :goto_0

    .line 995
    .end local v2    # "e1":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 997
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 998
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {p0, v11}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v10

    .line 999
    goto :goto_0

    .line 995
    .end local v1    # "e":Ljava/io/IOException;
    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v7, v8

    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 990
    :catch_4
    move-exception v2

    goto :goto_2
.end method

.method private createFolder(Ljava/lang/String;)Z
    .locals 5
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 605
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 606
    .local v0, "filePath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 607
    .local v1, "folderPath":Ljava/io/File;
    const-string v2, "PrintJob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Create Folder : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 609
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 610
    const-string v2, "PrintJob"

    const-string v3, "ERROR - mkdirs"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const/4 v2, 0x0

    .line 615
    :goto_0
    return v2

    .line 613
    :cond_0
    const-string v2, "PrintJob"

    const-string v3, "Success : "

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getDefaultModel(Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 942
    if-nez p2, :cond_1

    .line 952
    :cond_0
    :goto_0
    return-object v0

    .line 944
    :cond_1
    const-string v1, "SPL3"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 945
    const-string v0, "Samsung ML-1660 Series"

    goto :goto_0

    .line 946
    :cond_2
    const-string v1, "SPL5"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 947
    const-string v0, "Samsung CLP-320 Series"

    goto :goto_0

    .line 948
    :cond_3
    const-string v1, "SPL7"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 949
    const-string v0, "Samsung CLP-320 Series"

    goto :goto_0
.end method

.method private getDeviceCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 4
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 128
    if-nez p1, :cond_0

    .line 147
    :goto_0
    return-object v1

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 133
    iget-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 134
    iget-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    goto :goto_0

    .line 138
    :cond_1
    :try_start_0
    iput-object p1, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    .line 140
    iget-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->assetMgr:Landroid/content/res/AssetManager;

    const-string v3, "mobileprint_model_list.xml"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 139
    invoke-static {v2, p1}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->loadCapability(Ljava/io/InputStream;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 144
    iput-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    goto :goto_1
.end method

.method private getSPLDeviceInfo(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    .locals 12
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 871
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/PrintJob;->getDeviceCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v2

    .line 872
    .local v2, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v2, :cond_0

    move-object v3, v8

    .line 937
    :goto_0
    return-object v3

    .line 876
    :cond_0
    const/4 v3, 0x0

    .line 878
    .local v3, "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getSplVersion()Ljava/lang/String;

    move-result-object v5

    .line 879
    .local v5, "splVersion":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 880
    const-string v9, "ERROR : splVersion == null"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v8

    .line 881
    goto :goto_0

    .line 883
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 884
    .local v6, "valueSPLVersion":I
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "splVersion == "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 886
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "cts_files/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 887
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getCmsFileName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 886
    invoke-direct {p0, v9}, Lcom/sec/print/mobileprint/PrintJob;->copyCTS2SDCard(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 888
    .local v0, "colorTableFilePath":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 889
    const-string v9, "ERROR : colorTableFilePath == null"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v8

    .line 890
    goto :goto_0

    .line 892
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ColorTable : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 894
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getSplCompressionType()Ljava/lang/String;

    move-result-object v1

    .line 895
    .local v1, "compression":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 896
    const-string v9, "ERROR : compression == null"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v8

    .line 897
    goto :goto_0

    .line 899
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "compression == "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 901
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getSplWidthAlign()Ljava/lang/String;

    move-result-object v7

    .line 902
    .local v7, "widthAlign":Ljava/lang/String;
    if-nez v7, :cond_4

    .line 903
    const-string v9, "ERROR : widthAlign == null"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v8

    .line 904
    goto/16 :goto_0

    .line 906
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "widthAlign == "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 908
    const/4 v4, 0x0

    .line 910
    .local v4, "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    packed-switch v6, :pswitch_data_0

    .line 932
    :goto_1
    :pswitch_0
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;->isError()Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    move-object v3, v8

    .line 933
    goto/16 :goto_0

    .line 912
    :pswitch_1
    new-instance v4, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;

    .line 913
    .end local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    const/4 v9, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 914
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 912
    invoke-direct {v4, v0, v9, v10, v11}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 915
    .restart local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    goto :goto_1

    .line 917
    :pswitch_2
    new-instance v4, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;

    .line 918
    .end local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    const/4 v9, 0x2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 919
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 917
    invoke-direct {v4, v0, v9, v10, v11}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 920
    .restart local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    goto :goto_1

    .line 922
    :pswitch_3
    new-instance v4, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;

    .line 923
    .end local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    const/4 v9, 0x3

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 924
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 922
    invoke-direct {v4, v0, v9, v10, v11}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 925
    .restart local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    goto :goto_1

    .line 927
    :pswitch_4
    new-instance v4, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;

    .line 928
    .end local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    const/4 v9, 0x5

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 929
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 927
    invoke-direct {v4, v0, v9, v10, v11}, Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .restart local v4    # "splTypeInfo":Lcom/sec/print/mobileprint/printoptionattribute/SPLTypeInfo;
    goto :goto_1

    .line 935
    :cond_6
    new-instance v3, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .end local v3    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    sget-object v8, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_SPL:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    invoke-direct {v3, v8, v4}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .line 937
    .restart local v3    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    goto/16 :goto_0

    .line 910
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getTempPrefixString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1017
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1019
    .local v0, "currentTimeValue":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private selectPDLType(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;)Z
    .locals 25
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;
    .param p3, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;

    .prologue
    .line 696
    const/4 v15, 0x1

    .line 698
    .local v15, "result":Z
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getOutputInfo()Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    move-result-object v9

    .line 699
    .local v9, "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    instance-of v0, v9, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 700
    const-class v20, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v14

    check-cast v14, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;

    .line 702
    .local v14, "resolution":Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 703
    sget-object v21, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_FAX:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 705
    new-instance v22, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;

    invoke-virtual {v14}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->getResolution()Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->getValue()I

    move-result v23

    move-object/from16 v20, v9

    .line 706
    check-cast v20, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->getSelectedTempFolder()Ljava/lang/String;

    move-result-object v20

    .line 707
    check-cast v9, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;

    .end local v9    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;->getSelectedPrefix()Ljava/lang/String;

    move-result-object v24

    .line 705
    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, v20

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/printoptionattribute/FAXTypeInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 702
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v11, v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .line 708
    .local v11, "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "FaxResolution:"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;->getResolution()Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->getValue()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 709
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    .end local v14    # "resolution":Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;
    :cond_0
    :goto_0
    move/from16 v20, v15

    .line 867
    :goto_1
    return v20

    .line 712
    .restart local v9    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getSupportedPDLTypeList()Ljava/util/ArrayList;

    move-result-object v13

    .line 714
    .local v13, "pdlTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-nez v20, :cond_3

    .line 717
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->getDeviceCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v4

    .line 718
    .local v4, "capability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-eqz v4, :cond_3

    .line 719
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 720
    .local v19, "supportedPDLType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getEmulation()Ljava/lang/String;

    move-result-object v8

    .line 721
    .local v8, "emulation":Ljava/lang/String;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v20

    if-eqz v20, :cond_3

    .line 722
    new-instance v17, Ljava/util/StringTokenizer;

    const-string v20, ","

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    .local v17, "st":Ljava/util/StringTokenizer;
    :goto_2
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v20

    if-nez v20, :cond_5

    .line 729
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->setSupportedPDLTypeList(Ljava/util/ArrayList;)V

    .line 733
    .end local v4    # "capability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .end local v8    # "emulation":Ljava/lang/String;
    .end local v17    # "st":Ljava/util/StringTokenizer;
    .end local v19    # "supportedPDLType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getSupportedPDLTypeList()Ljava/util/ArrayList;

    move-result-object v13

    .line 734
    if-eqz v13, :cond_4

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-nez v20, :cond_6

    .line 735
    :cond_4
    const/16 v20, 0x0

    goto :goto_1

    .line 724
    .restart local v4    # "capability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .restart local v8    # "emulation":Ljava/lang/String;
    .restart local v17    # "st":Ljava/util/StringTokenizer;
    .restart local v19    # "supportedPDLType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    invoke-virtual/range {v17 .. v17}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 725
    .local v10, "pdl":Ljava/lang/String;
    const-string v20, "^\\s+"

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 726
    const-string v20, "\\s+$"

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 727
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 745
    .end local v4    # "capability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .end local v8    # "emulation":Ljava/lang/String;
    .end local v10    # "pdl":Ljava/lang/String;
    .end local v17    # "st":Ljava/util/StringTokenizer;
    .end local v19    # "supportedPDLType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    const-string v20, "GCP"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 746
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 747
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PDF:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 746
    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;)V

    .line 748
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 749
    const/4 v15, 0x1

    .line 751
    goto/16 :goto_0

    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :cond_7
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    const-class v21, Lcom/sec/print/mobileprint/pagedata/DirectPrintDocSet;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 752
    const-string v20, "PDF"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 753
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 754
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_DIRECTPRINT:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 753
    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;)V

    .line 755
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 757
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :cond_8
    const/4 v15, 0x0

    .line 760
    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    const-class v21, Lcom/sec/print/mobileprint/pagedata/PageSet;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_0

    .line 762
    const-string v20, "PCL6"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_a

    .line 763
    const-string v20, "PCLXL"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_12

    .line 765
    :cond_a
    const/4 v11, 0x0

    .line 767
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    const-class v20, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v5

    .line 766
    check-cast v5, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;

    .line 768
    .local v5, "chromaticity":Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->isSupportedColor()Z

    move-result v20

    if-eqz v20, :cond_b

    .line 769
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->getChromaticity()Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    move-result-object v20

    sget-object v21, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 770
    :cond_b
    const-string v20, "PCL6 - RLE"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 771
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 772
    new-instance v21, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;

    .line 773
    const/16 v22, 0x4

    .line 772
    invoke-direct/range {v21 .. v22}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;-><init>(I)V

    .line 771
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v11, v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .line 820
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 776
    :cond_c
    const-class v20, Lcom/sec/print/mobileprint/printoptionattribute/SourceType;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v16

    .line 775
    check-cast v16, Lcom/sec/print/mobileprint/printoptionattribute/SourceType;

    .line 777
    .local v16, "sourceType":Lcom/sec/print/mobileprint/printoptionattribute/SourceType;
    if-eqz v16, :cond_d

    .line 778
    invoke-virtual/range {v16 .. v16}, Lcom/sec/print/mobileprint/printoptionattribute/SourceType;->getSourceType()I

    move-result v20

    sget-object v21, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->PRINT_SOURCE_DOCUMENT:Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;

    .line 779
    invoke-virtual/range {v21 .. v21}, Lcom/sec/print/mobileprint/printoptionattribute/SourceType$EnumSourceType;->getValue()I

    move-result v21

    .line 778
    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    .line 780
    const-string v20, "PCL6 - Delta-Row"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 781
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 782
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 783
    new-instance v21, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;

    .line 784
    const/16 v22, 0x1

    .line 783
    invoke-direct/range {v21 .. v22}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;-><init>(I)V

    .line 781
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v11, v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .line 785
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    goto :goto_3

    .line 787
    :cond_d
    const/16 v18, 0x0

    .line 789
    .local v18, "strPCL6Comp":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v20

    .line 788
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->getDeviceCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v7

    .line 790
    .local v7, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-eqz v7, :cond_e

    .line 792
    invoke-virtual {v7}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getPCL6CompressionType()Ljava/lang/String;

    move-result-object v18

    .line 795
    :cond_e
    if-eqz v18, :cond_f

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_10

    .line 797
    :cond_f
    const-string v18, "RLE, Deltarow, Banded_JPG"

    .line 800
    :cond_10
    const-string v20, "Banded_JPG"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 801
    const-string v20, "PCL6 - Banded_JPG"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 802
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 803
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 804
    new-instance v21, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;

    const/16 v22, 0x2

    invoke-direct/range {v21 .. v22}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;-><init>(I)V

    .line 802
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v11, v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .line 810
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    goto/16 :goto_3

    .line 812
    :cond_11
    const-string v20, "PCL6 - PCL6_COMP_TYPE_BANDED_DELTA_ROW"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 813
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 814
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL6:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 815
    new-instance v21, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;

    const/16 v22, 0x1

    invoke-direct/range {v21 .. v22}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;-><init>(I)V

    .line 813
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v11, v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V

    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    goto/16 :goto_3

    .line 822
    .end local v5    # "chromaticity":Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;
    .end local v7    # "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    .end local v16    # "sourceType":Lcom/sec/print/mobileprint/printoptionattribute/SourceType;
    .end local v18    # "strPCL6Comp":Ljava/lang/String;
    :cond_12
    const-string v20, "SPLC"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_15

    .line 823
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->getSPLDeviceInfo(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    move-result-object v11

    .line 824
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    if-eqz v11, :cond_13

    .line 825
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 827
    :cond_13
    const-string v20, "set default color table"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 828
    const-string v20, "SPL5"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintJob;->getDefaultModel(Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 829
    .local v6, "defaultModel":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/print/mobileprint/PrintJob;->getSPLDeviceInfo(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    move-result-object v12

    .line 830
    .local v12, "pdlTypeDefault":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    if-eqz v12, :cond_14

    .line 831
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 833
    :cond_14
    const-string v20, "Fail to set default color table"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 834
    const/4 v15, 0x0

    .line 838
    goto/16 :goto_0

    .end local v6    # "defaultModel":Ljava/lang/String;
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    .end local v12    # "pdlTypeDefault":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :cond_15
    const-string v20, "SPL"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_16

    .line 839
    const-string v20, "GDI"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_19

    .line 840
    :cond_16
    invoke-virtual/range {p2 .. p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->getSPLDeviceInfo(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    move-result-object v11

    .line 841
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    if-eqz v11, :cond_17

    .line 842
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 844
    :cond_17
    const-string v20, "set default color table"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 845
    const-string v20, "SPL3"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintJob;->getDefaultModel(Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 846
    .restart local v6    # "defaultModel":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/print/mobileprint/PrintJob;->getSPLDeviceInfo(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    move-result-object v12

    .line 847
    .restart local v12    # "pdlTypeDefault":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    if-eqz v12, :cond_18

    .line 848
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 850
    :cond_18
    const-string v20, "Fail to set default color table"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 851
    const/4 v15, 0x0

    .line 854
    goto/16 :goto_0

    .line 856
    .end local v6    # "defaultModel":Ljava/lang/String;
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    .end local v12    # "pdlTypeDefault":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :cond_19
    const-string v20, "PCL3GUI"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1a

    .line 857
    new-instance v11, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;

    .line 858
    sget-object v20, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_PCL3GUI:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .line 857
    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;)V

    .line 859
    .restart local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto/16 :goto_0

    .line 862
    .end local v11    # "pdlType":Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
    :cond_1a
    const/4 v15, 0x0

    goto/16 :goto_0
.end method

.method private setCollateOption(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V
    .locals 3
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    .prologue
    .line 526
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate;

    invoke-virtual {p1, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/Collate;

    .line 527
    .local v0, "collate":Lcom/sec/print/mobileprint/printoptionattribute/Collate;
    if-nez v0, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->isSupportedCMDCollate()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 532
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Collate;->getCollate()Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    move-result-object v1

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    if-ne v1, v2, :cond_0

    .line 533
    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE_CMD:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Collate;->setCollate(Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;)V

    goto :goto_0

    .line 536
    :cond_2
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Collate;->getCollate()Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    move-result-object v1

    sget-object v2, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE_CMD:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    if-ne v1, v2, :cond_0

    .line 537
    const-string v1, "Don\'t use COLLATE_COLLATE_CMD"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 538
    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;->COLLATE_COLLATE:Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/Collate;->setCollate(Lcom/sec/print/mobileprint/printoptionattribute/Collate$EnumCollate;)V

    goto :goto_0
.end method

.method private setJobAccountingPrintInfo(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V
    .locals 6
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 546
    const/4 v2, 0x0

    .line 548
    .local v2, "ver":Ljava/lang/String;
    const-class v3, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;

    invoke-virtual {p1, v3}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v1

    .line 547
    check-cast v1, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;

    .line 549
    .local v1, "jobAccounting":Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;
    if-nez v1, :cond_1

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    invoke-virtual {v1, v4}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setRequiredPWEncryption(Z)V

    .line 555
    invoke-virtual {v1, v4}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setRequiredIDEncryption(Z)V

    .line 556
    invoke-virtual {v1, v4}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setIdOnly(Z)V

    .line 560
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/print/mobileprint/PrintJob;->getDeviceCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 562
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-eqz v0, :cond_2

    .line 563
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getJobAccountinVer()Ljava/lang/String;

    move-result-object v2

    .line 567
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 568
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 572
    const-string v3, "PWD_ENC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_3

    .line 573
    const-string v3, "ID_PWD_ENC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 575
    :cond_3
    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setRequiredPWEncryption(Z)V

    goto :goto_0

    .line 581
    :cond_4
    const-string v3, "ID_PWD_ENC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 582
    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setRequiredIDEncryption(Z)V

    .line 583
    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/printoptionattribute/JobAccounting;->setIdOnly(Z)V

    goto :goto_0
.end method

.method private setManufacturerType(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V
    .locals 3
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    .prologue
    .line 594
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "modelNamelowercase":Ljava/lang/String;
    const-string v1, "dell"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    const-string v1, "ManufacturerType - DELL"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 597
    new-instance v1, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;-><init>(I)V

    invoke-virtual {p1, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 602
    :goto_0
    return-void

    .line 599
    :cond_0
    const-string v1, "ManufacturerType - SAMSUNG"

    invoke-static {p0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 600
    new-instance v1, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;-><init>(I)V

    invoke-virtual {p1, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    goto :goto_0
.end method

.method private setMediaInfo(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V
    .locals 10
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;

    .prologue
    .line 649
    const-class v2, Lcom/sec/print/mobileprint/printoptionattribute/Media;

    invoke-virtual {p1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v9

    check-cast v9, Lcom/sec/print/mobileprint/printoptionattribute/Media;

    .line 650
    .local v9, "media":Lcom/sec/print/mobileprint/printoptionattribute/Media;
    const/4 v0, 0x0

    .line 651
    .local v0, "mediaInfo":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
    if-eqz v9, :cond_0

    .line 653
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMediaSizeName()Ljava/lang/String;

    move-result-object v2

    .line 652
    invoke-static {v2}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->getMediaSizeID(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    move-result-object v1

    .line 655
    .local v1, "eMediaSize":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMediaTypeName()Ljava/lang/String;

    move-result-object v2

    .line 654
    invoke-static {v2}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->getMediaTypeID(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    move-result-object v4

    .line 657
    .local v4, "eMediaType":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;

    .end local v0    # "mediaInfo":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getWidth()I

    move-result v2

    .line 658
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getHeight()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMarginLeft()I

    move-result v5

    .line 659
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMarginTop()I

    move-result v6

    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMarginRight()I

    move-result v7

    .line 660
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printoptionattribute/Media;->getMarginBottom()I

    move-result v8

    .line 657
    invoke-direct/range {v0 .. v8}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;-><init>(Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;IILcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;IIII)V

    .line 665
    .end local v1    # "eMediaSize":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .end local v4    # "eMediaType":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    .restart local v0    # "mediaInfo":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
    :goto_0
    const-class v2, Lcom/sec/print/mobileprint/printoptionattribute/Media;

    invoke-virtual {p1, v2}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->remove(Ljava/lang/Class;)Z

    .line 666
    invoke-virtual {p1, v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z

    .line 667
    return-void

    .line 662
    :cond_0
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;

    .end local v0    # "mediaInfo":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;-><init>()V

    .restart local v0    # "mediaInfo":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 101
    iget-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/PDLComposer;->cancel()V

    .line 103
    monitor-exit v1

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deleteFolder(Ljava/io/File;)V
    .locals 4
    .param p1, "targetFolder"    # Ljava/io/File;

    .prologue
    .line 626
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 627
    .local v0, "childFile":[Ljava/io/File;
    if-nez v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 631
    :cond_0
    array-length v2, v0

    .line 633
    .local v2, "size":I
    if-lez v2, :cond_1

    .line 634
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_2

    .line 643
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 635
    .restart local v1    # "i":I
    :cond_2
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 636
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 634
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 638
    :cond_3
    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/io/File;)V

    goto :goto_2
.end method

.method public deleteFolder(Ljava/lang/String;)V
    .locals 2
    .param p1, "folderPathToBeDeleted"    # Ljava/lang/String;

    .prologue
    .line 619
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 620
    .local v0, "targetFolder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 621
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/io/File;)V

    .line 623
    :cond_0
    return-void
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 114
    iget-object v1, p0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 114
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    iput-boolean v3, p0, Lcom/sec/print/mobileprint/PrintJob;->isRun:Z

    .line 119
    iput-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    .line 120
    iput-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->curLoadedModelName:Ljava/lang/String;

    .line 121
    iput-object v2, p0, Lcom/sec/print/mobileprint/PrintJob;->deivceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    .line 122
    return-void

    .line 114
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public isProcessing()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/PrintJob;->isRun:Z

    return v0
.end method

.method public print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)Z
    .locals 45
    .param p1, "printOptions"    # Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
    .param p2, "docSet"    # Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
    .param p3, "printerInfo"    # Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;

    .prologue
    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/sec/print/mobileprint/PrintJob;->init()V

    .line 154
    const/16 v32, 0x1

    .line 155
    .local v32, "result":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintJob;->setManufacturerType(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V

    .line 156
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PrintJob;->selectPDLType(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;)Z

    move-result v41

    if-nez v41, :cond_0

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 158
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 159
    const/16 v43, 0x6

    .line 158
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 160
    const/16 v32, 0x0

    .line 520
    :goto_0
    const/16 v41, 0x0

    move/from16 v0, v41

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/print/mobileprint/PrintJob;->isRun:Z

    .line 521
    return v32

    .line 163
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->isSupportedColor()Z

    move-result v41

    if-nez v41, :cond_1

    .line 165
    const-class v41, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    move-result-object v11

    .line 164
    check-cast v11, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;

    .line 166
    .local v11, "chromaticity":Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;
    if-eqz v11, :cond_1

    .line 168
    sget-object v41, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->MONOCHROME:Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    move-object/from16 v0, v41

    invoke-virtual {v11, v0}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->setChromaticity(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V

    .line 172
    .end local v11    # "chromaticity":Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/print/mobileprint/PrintJob;->setMediaInfo(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V

    .line 173
    invoke-virtual/range {p3 .. p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getOutputInfo()Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;

    move-result-object v24

    .line 174
    .local v24, "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;

    move/from16 v41, v0

    if-nez v41, :cond_2

    .line 177
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintJob;->setJobAccountingPrintInfo(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V

    .line 179
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/PrintJob;->setCollateOption(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;)V

    .line 181
    const/16 v22, 0x0

    .line 182
    .local v22, "isCanceled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    move-object/from16 v42, v0

    monitor-enter v42

    .line 183
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    move/from16 v41, v0

    if-eqz v41, :cond_4

    .line 184
    const/16 v41, 0x0

    move/from16 v0, v41

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 185
    const/16 v22, 0x1

    .line 182
    :goto_1
    monitor-exit v42
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 191
    if-eqz v22, :cond_5

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 193
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_CANCELED:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 194
    const/16 v43, 0x0

    .line 193
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 513
    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->syncCancel:Ljava/lang/Object;

    move-object/from16 v42, v0

    monitor-enter v42

    .line 514
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    move/from16 v41, v0

    if-eqz v41, :cond_3

    .line 515
    const/16 v41, 0x0

    move/from16 v0, v41

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/print/mobileprint/PrintJob;->requestCancel:Z

    .line 513
    :cond_3
    monitor-exit v42

    goto/16 :goto_0

    :catchall_0
    move-exception v41

    monitor-exit v42
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v41

    .line 187
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_4
    :try_start_2
    new-instance v41, Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v43, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v43

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PDLComposer;-><init>(Lcom/sec/print/mobileprint/PrintingStatusMonitor;)V

    move-object/from16 v0, v41

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    goto :goto_1

    .line 182
    :catchall_1
    move-exception v41

    monitor-exit v42
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v41

    .line 196
    :cond_5
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_6

    move-object/from16 v41, v24

    .line 197
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;

    .line 198
    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->getIpAddr()Ljava/lang/String;

    move-result-object v21

    .local v21, "ipAddr":Ljava/lang/String;
    move-object/from16 v41, v24

    .line 199
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->getPortNum()I

    move-result v27

    .line 200
    .local v27, "portNum":I
    new-instance v23, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;-><init>(Ljava/lang/String;I)V

    .line 202
    .local v23, "netOutputStream":Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    .line 203
    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    .line 202
    if-nez v41, :cond_6

    .line 204
    const/16 v32, 0x0

    .line 207
    .end local v21    # "ipAddr":Ljava/lang/String;
    .end local v23    # "netOutputStream":Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;
    .end local v27    # "portNum":I
    :cond_6
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/FaxOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_9

    .line 208
    new-instance v41, Ljava/lang/StringBuilder;

    sget-object v42, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static/range {v42 .. v42}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v42, "deletethis"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 209
    .local v17, "fileFullPath":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->createFolder(Ljava/lang/String;)Z

    move-result v41

    if-nez v41, :cond_7

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 211
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 212
    const/16 v43, 0x1

    .line 211
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 213
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 214
    const/16 v32, 0x0

    .line 217
    :cond_7
    new-instance v20, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 219
    .local v20, "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    if-nez v41, :cond_8

    .line 221
    const/16 v32, 0x0

    .line 510
    .end local v17    # "fileFullPath":Ljava/lang/String;
    .end local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_8
    :goto_3
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 224
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_9
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/FileOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_a

    .line 225
    check-cast v24, Lcom/sec/print/mobileprint/printerinfo/FileOutputInfo;

    .line 226
    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/print/mobileprint/printerinfo/FileOutputInfo;->getFilefullpath()Ljava/lang/String;

    move-result-object v17

    .line 227
    .restart local v17    # "fileFullPath":Ljava/lang/String;
    new-instance v20, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 229
    .restart local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    if-nez v41, :cond_8

    .line 231
    const/16 v32, 0x0

    .line 233
    goto :goto_3

    .end local v17    # "fileFullPath":Ljava/lang/String;
    .end local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_a
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/USBOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_b

    .line 234
    check-cast v24, Lcom/sec/print/mobileprint/printerinfo/USBOutputInfo;

    .line 235
    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/print/mobileprint/printerinfo/USBOutputInfo;->getUSBPortAddr()Ljava/lang/String;

    move-result-object v39

    .line 236
    .local v39, "usbPortAddr":Ljava/lang/String;
    new-instance v20, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 238
    .restart local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    if-nez v41, :cond_8

    .line 240
    const/16 v32, 0x0

    .line 243
    goto :goto_3

    .end local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    .end local v39    # "usbPortAddr":Ljava/lang/String;
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_b
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_13

    move-object/from16 v41, v24

    .line 244
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->getVendorId()I

    move-result v40

    .line 245
    .local v40, "vid":I
    check-cast v24, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;

    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->getProductId()I

    move-result v26

    .line 247
    .local v26, "pid":I
    if-eqz v40, :cond_c

    if-nez v26, :cond_d

    .line 248
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 249
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 250
    const/16 v43, 0x6

    .line 249
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 251
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 252
    const/16 v32, 0x0

    .line 253
    goto/16 :goto_3

    .line 254
    :cond_d
    new-instance v41, Ljava/lang/StringBuilder;

    sget-object v42, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static/range {v42 .. v42}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lcom/sec/print/mobileprint/PrintJob;->getTempPrefixString()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "USB"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 254
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 257
    .local v36, "strUSBTempFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->createFolder(Ljava/lang/String;)Z

    move-result v41

    if-nez v41, :cond_e

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 259
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 260
    const/16 v43, 0x1

    .line 259
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 261
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 262
    const/16 v32, 0x0

    .line 265
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 266
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_START_MAKE_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 267
    const/16 v43, 0x0

    .line 266
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 268
    new-instance v20, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 269
    .restart local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    if-nez v41, :cond_f

    .line 271
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 272
    const/16 v32, 0x0

    .line 273
    goto/16 :goto_3

    .line 275
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 276
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_END_MAKE_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 277
    const/16 v43, 0x0

    .line 276
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 279
    const/16 v18, 0x0

    .line 281
    .local v18, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v19, Ljava/io/FileInputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .end local v18    # "fileInputStream":Ljava/io/FileInputStream;
    .local v19, "fileInputStream":Ljava/io/FileInputStream;
    move-object/from16 v18, v19

    .line 295
    .end local v19    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v18    # "fileInputStream":Ljava/io/FileInputStream;
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 296
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_START_SEND_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 297
    const/16 v43, 0x0

    .line 296
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 299
    new-instance v38, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->context:Landroid/content/Context;

    move-object/from16 v41, v0

    move-object/from16 v0, v38

    move-object/from16 v1, v41

    move/from16 v2, v26

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;-><init>(Landroid/content/Context;II)V

    .line 300
    .local v38, "usbOutputStream":Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;
    invoke-virtual/range {v38 .. v38}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->open()Z

    move-result v41

    if-eqz v41, :cond_8

    .line 301
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v16

    .line 302
    .local v16, "fc":Ljava/nio/channels/FileChannel;
    const/16 v41, 0x4000

    invoke-static/range {v41 .. v41}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    .line 303
    .local v10, "buffer":Ljava/nio/ByteBuffer;
    const-wide/16 v33, 0x0

    .line 305
    .local v33, "size":J
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/nio/channels/FileChannel;->size()J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-wide v33

    .line 306
    const-wide/16 v12, 0x0

    .line 307
    .local v12, "curCnt":J
    :goto_5
    cmp-long v41, v12, v33

    if-ltz v41, :cond_11

    .line 337
    :cond_10
    :goto_6
    :try_start_5
    invoke-virtual/range {v38 .. v38}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->close()Z

    .line 338
    invoke-virtual/range {v16 .. v16}, Ljava/nio/channels/FileChannel;->close()V

    .line 339
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V

    .line 340
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 342
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_END_SEND_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    const/16 v43, 0x0

    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 345
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_COMPLETED_FILE_JOB:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    const/16 v43, 0x0

    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 347
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_3

    .line 349
    :catch_0
    move-exception v15

    .line 351
    .local v15, "e1":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_3

    .line 355
    .end local v12    # "curCnt":J
    .end local v15    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v14

    .line 356
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 358
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 359
    const/16 v43, 0x4

    .line 358
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 362
    :try_start_7
    invoke-virtual/range {v38 .. v38}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->close()Z

    .line 363
    invoke-virtual/range {v16 .. v16}, Ljava/nio/channels/FileChannel;->close()V

    .line 364
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V

    .line 365
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_3

    .line 366
    :catch_2
    move-exception v15

    .line 368
    .restart local v15    # "e1":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 282
    .end local v10    # "buffer":Ljava/nio/ByteBuffer;
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "e1":Ljava/io/IOException;
    .end local v16    # "fc":Ljava/nio/channels/FileChannel;
    .end local v33    # "size":J
    .end local v38    # "usbOutputStream":Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;
    :catch_3
    move-exception v15

    .line 284
    .local v15, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v15}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 286
    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "Error - new FileInputStream(strIPPTempFilePath) : "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 287
    invoke-virtual {v15}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 286
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 285
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 289
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 290
    const/16 v43, 0x4

    .line 289
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 291
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 292
    const/16 v32, 0x0

    goto/16 :goto_4

    .line 309
    .end local v15    # "e1":Ljava/io/FileNotFoundException;
    .restart local v10    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v12    # "curCnt":J
    .restart local v16    # "fc":Ljava/nio/channels/FileChannel;
    .restart local v33    # "size":J
    .restart local v38    # "usbOutputStream":Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;
    :cond_11
    :try_start_8
    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 310
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v31

    .line 311
    .local v31, "readSize":I
    if-ltz v31, :cond_10

    .line 314
    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 315
    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v41

    move-object/from16 v0, v38

    move-object/from16 v1, v41

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->write([BI)Z

    move-result v41

    if-nez v41, :cond_12

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 317
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 318
    const/16 v43, 0x4

    .line 317
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 320
    :try_start_9
    invoke-virtual/range {v38 .. v38}, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->close()Z

    .line 321
    invoke-virtual/range {v16 .. v16}, Ljava/nio/channels/FileChannel;->close()V

    .line 322
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileInputStream;->close()V

    .line 323
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_6

    .line 324
    :catch_4
    move-exception v15

    .line 326
    .local v15, "e1":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    .line 331
    .end local v15    # "e1":Ljava/io/IOException;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 332
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_START_SEND_FILE_PROGRESS:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    const-wide/16 v43, 0x64

    mul-long v43, v43, v12

    div-long v43, v43, v33

    move-wide/from16 v0, v43

    long-to-int v0, v0

    move/from16 v43, v0

    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    .line 334
    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v41, v0

    add-long v12, v12, v41

    goto/16 :goto_5

    .line 374
    .end local v10    # "buffer":Ljava/nio/ByteBuffer;
    .end local v12    # "curCnt":J
    .end local v16    # "fc":Ljava/nio/channels/FileChannel;
    .end local v18    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    .end local v26    # "pid":I
    .end local v31    # "readSize":I
    .end local v33    # "size":J
    .end local v36    # "strUSBTempFilePath":Ljava/lang/String;
    .end local v38    # "usbOutputStream":Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;
    .end local v40    # "vid":I
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_13
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_14

    move-object/from16 v41, v24

    .line 375
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    .line 376
    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->getSelectedPrinterDomain()Ljava/lang/String;

    move-result-object v5

    .local v5, "selectedPrinterDomain":Ljava/lang/String;
    move-object/from16 v41, v24

    .line 377
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    .line 378
    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->getSelectedPrinterUserName()Ljava/lang/String;

    move-result-object v6

    .local v6, "selectedPrinterUserName":Ljava/lang/String;
    move-object/from16 v41, v24

    .line 379
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    .line 380
    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->getSelectedPrinterPassword()Ljava/lang/String;

    move-result-object v7

    .local v7, "selectedPrinterPassword":Ljava/lang/String;
    move-object/from16 v41, v24

    .line 381
    check-cast v41, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    .line 382
    invoke-virtual/range {v41 .. v41}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->getSelectedPrinterIP()Ljava/lang/String;

    move-result-object v8

    .line 383
    .local v8, "selectedPrinterIPAddress":Ljava/lang/String;
    check-cast v24, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;

    .line 384
    .end local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    invoke-virtual/range {v24 .. v24}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->getSelectedPrinterShareName()Ljava/lang/String;

    move-result-object v9

    .line 386
    .local v9, "selectedPrinterShareName":Ljava/lang/String;
    new-instance v4, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;

    invoke-direct/range {v4 .. v9}, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    .local v4, "smbOutputStream":Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    .line 391
    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    .line 390
    if-nez v41, :cond_8

    .line 392
    const/16 v32, 0x0

    .line 394
    goto/16 :goto_3

    .end local v4    # "smbOutputStream":Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;
    .end local v5    # "selectedPrinterDomain":Ljava/lang/String;
    .end local v6    # "selectedPrinterUserName":Ljava/lang/String;
    .end local v7    # "selectedPrinterPassword":Ljava/lang/String;
    .end local v8    # "selectedPrinterIPAddress":Ljava/lang/String;
    .end local v9    # "selectedPrinterShareName":Ljava/lang/String;
    .restart local v24    # "outputInfo":Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;
    :cond_14
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v41

    const-class v42, Lcom/sec/print/mobileprint/printerinfo/IPPOutputInfo;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-ne v0, v1, :cond_8

    move-object/from16 v25, v24

    .line 396
    check-cast v25, Lcom/sec/print/mobileprint/printerinfo/IPPOutputInfo;

    .line 397
    .local v25, "outputInfoIPP":Lcom/sec/print/mobileprint/printerinfo/IPPOutputInfo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/print/mobileprint/printerinfo/IPPOutputInfo;->getURL()Ljava/net/URL;

    move-result-object v37

    .line 398
    .local v37, "urlIPPPrinter":Ljava/net/URL;
    if-nez v37, :cond_15

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 400
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 401
    const/16 v43, 0x6

    .line 400
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 402
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 403
    const/16 v32, 0x0

    .line 404
    goto/16 :goto_3

    .line 407
    :cond_15
    new-instance v41, Ljava/lang/StringBuilder;

    sget-object v42, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static/range {v42 .. v42}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 408
    invoke-static {}, Lcom/sec/print/mobileprint/PrintJob;->getTempPrefixString()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "IPP"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 407
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 410
    .local v35, "strIPPTempFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->createFolder(Ljava/lang/String;)Z

    move-result v41

    if-nez v41, :cond_16

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 412
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 413
    const/16 v43, 0x1

    .line 412
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 414
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 415
    const/16 v32, 0x0

    .line 416
    goto/16 :goto_3

    .line 420
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 421
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_START_MAKE_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 422
    const/16 v43, 0x0

    .line 421
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 423
    new-instance v20, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;-><init>(Ljava/lang/String;)V

    .line 425
    .restart local v20    # "fileOutputStream":Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->pdlComposer:Lcom/sec/print/mobileprint/PDLComposer;

    move-object/from16 v41, v0

    move-object/from16 v0, v41

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/print/mobileprint/PDLComposer;->print(Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;Lcom/sec/print/mobileprint/pagedata/DocSetInterface;Lcom/sec/print/mobileprint/io/ISPSOutputStream;)I

    move-result v41

    if-nez v41, :cond_17

    .line 427
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 428
    const/16 v32, 0x0

    .line 429
    goto/16 :goto_3

    .line 432
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 433
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_END_MAKE_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 434
    const/16 v43, 0x0

    .line 433
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 438
    :try_start_b
    new-instance v18, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    move-object/from16 v1, v35

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_6

    .line 455
    .restart local v18    # "fileInputStream":Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 456
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_START_SEND_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 457
    const/16 v43, 0x0

    .line 456
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 458
    const-string v41, "CupsPrinter - sending"

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 459
    new-instance v30, Lorg/cups4j/CupsPrinter;

    .line 460
    invoke-virtual/range {p3 .. p3}, Lcom/sec/print/mobileprint/printerinfo/PrinterInfo;->getModelName()Ljava/lang/String;

    move-result-object v41

    .line 461
    const/16 v42, 0x1

    .line 459
    move-object/from16 v0, v30

    move-object/from16 v1, v37

    move-object/from16 v2, v41

    move/from16 v3, v42

    invoke-direct {v0, v1, v2, v3}, Lorg/cups4j/CupsPrinter;-><init>(Ljava/net/URL;Ljava/lang/String;Z)V

    .line 462
    .local v30, "printer":Lorg/cups4j/CupsPrinter;
    new-instance v41, Lorg/cups4j/PrintJob$Builder;

    move-object/from16 v0, v41

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/cups4j/PrintJob$Builder;-><init>(Ljava/io/InputStream;)V

    .line 464
    const-string v42, "Samsung Mobile Print"

    invoke-virtual/range {v41 .. v42}, Lorg/cups4j/PrintJob$Builder;->jobName(Ljava/lang/String;)Lorg/cups4j/PrintJob$Builder;

    move-result-object v41

    .line 465
    const-string v42, "Samsung Mobile Print"

    invoke-virtual/range {v41 .. v42}, Lorg/cups4j/PrintJob$Builder;->userName(Ljava/lang/String;)Lorg/cups4j/PrintJob$Builder;

    move-result-object v41

    const/16 v42, 0x1

    invoke-virtual/range {v41 .. v42}, Lorg/cups4j/PrintJob$Builder;->copies(I)Lorg/cups4j/PrintJob$Builder;

    move-result-object v41

    .line 466
    const-string v42, "1"

    invoke-virtual/range {v41 .. v42}, Lorg/cups4j/PrintJob$Builder;->pageRanges(Ljava/lang/String;)Lorg/cups4j/PrintJob$Builder;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {v41 .. v42}, Lorg/cups4j/PrintJob$Builder;->duplex(Z)Lorg/cups4j/PrintJob$Builder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Lorg/cups4j/PrintJob$Builder;->build()Lorg/cups4j/PrintJob;

    move-result-object v28

    .line 470
    .local v28, "printJob":Lorg/cups4j/PrintJob;
    :try_start_c
    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/cups4j/CupsPrinter;->print(Lorg/cups4j/PrintJob;)Lorg/cups4j/PrintRequestResult;

    move-result-object v29

    .line 471
    .local v29, "printRequestResult":Lorg/cups4j/PrintRequestResult;
    invoke-virtual/range {v29 .. v29}, Lorg/cups4j/PrintRequestResult;->isSuccessfulResult()Z

    move-result v41

    if-eqz v41, :cond_18

    .line 472
    const-string v41, "CupsPrinter - success"

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 474
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_END_SEND_FILE_DATA:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 475
    const/16 v43, 0x0

    .line 474
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 477
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_COMPLETED_FILE_JOB:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 478
    const/16 v43, 0x0

    .line 477
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_3

    .line 493
    .end local v29    # "printRequestResult":Lorg/cups4j/PrintRequestResult;
    :catch_5
    move-exception v14

    .line 495
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 497
    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "Error - printRequestResult exception : "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 497
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 496
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 500
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 501
    const/16 v43, 0x4

    .line 500
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 502
    const/16 v32, 0x0

    goto/16 :goto_3

    .line 440
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v18    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v28    # "printJob":Lorg/cups4j/PrintJob;
    .end local v30    # "printer":Lorg/cups4j/CupsPrinter;
    :catch_6
    move-exception v15

    .line 442
    .local v15, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v15}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 444
    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "Error - new FileInputStream(strIPPTempFilePath) : "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 445
    invoke-virtual {v15}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 444
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 443
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 447
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 448
    const/16 v43, 0x4

    .line 447
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V

    .line 449
    sget-object v41, Lcom/sec/print/mobileprint/PrintJob;->TEMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/PrintJob;->deleteFolder(Ljava/lang/String;)V

    .line 450
    const/16 v32, 0x0

    .line 451
    goto/16 :goto_3

    .line 482
    .end local v15    # "e1":Ljava/io/FileNotFoundException;
    .restart local v18    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v28    # "printJob":Lorg/cups4j/PrintJob;
    .restart local v29    # "printRequestResult":Lorg/cups4j/PrintRequestResult;
    .restart local v30    # "printer":Lorg/cups4j/CupsPrinter;
    :cond_18
    :try_start_d
    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "Error - printRequestResult : "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 484
    invoke-virtual/range {v29 .. v29}, Lorg/cups4j/PrintRequestResult;->getResultCode()Ljava/lang/String;

    move-result-object v42

    .line 483
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 485
    const-string v42, " Descriptioni : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 487
    invoke-virtual/range {v29 .. v29}, Lorg/cups4j/PrintRequestResult;->getResultDescription()Ljava/lang/String;

    move-result-object v42

    .line 486
    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 482
    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 480
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/PrintJob;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    move-object/from16 v41, v0

    .line 489
    sget-object v42, Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;->PRINTING_STATUSTYPE_ERROR:Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;

    .line 490
    const/16 v43, 0x4

    .line 489
    invoke-virtual/range {v41 .. v43}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;->Notify(Lcom/sec/print/mobileprint/PrintingStatusMonitor$EnumPrintingStatusType;I)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    .line 491
    const/16 v32, 0x0

    goto/16 :goto_3
.end method
