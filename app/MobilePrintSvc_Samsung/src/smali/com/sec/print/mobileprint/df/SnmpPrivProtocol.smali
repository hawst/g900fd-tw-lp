.class public final enum Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
.super Ljava/lang/Enum;
.source "SnmpPrivProtocol.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final enum AES128:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field public static final enum AES192:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field public static final enum AES256:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DES:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field public static final enum DES3:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const-string v1, "DES"

    invoke-direct {v0, v1, v2}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const-string v1, "DES3"

    invoke-direct {v0, v1, v3}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES3:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const-string v1, "AES128"

    invoke-direct {v0, v1, v4}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES128:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const-string v1, "AES192"

    invoke-direct {v0, v1, v5}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES192:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const-string v1, "AES256"

    invoke-direct {v0, v1, v6}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES256:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->DES3:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES128:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES192:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->AES256:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->ENUM$VALUES:[Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    .line 9
    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 17
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->ENUM$VALUES:[Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;->writeToParcel(Landroid/os/Parcel;)V

    .line 32
    return-void
.end method
