.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
.super Ljava/lang/Enum;
.source "FaxResolution.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumResolution"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

.field public static final enum FAX_QUALITY_TYPE_FINE:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

.field public static final enum FAX_QUALITY_TYPE_STANDARD:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 21
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    const-string v1, "FAX_QUALITY_TYPE_FINE"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->FAX_QUALITY_TYPE_FINE:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    .line 22
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    const-string v1, "FAX_QUALITY_TYPE_STANDARD"

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->FAX_QUALITY_TYPE_STANDARD:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    .line 20
    new-array v0, v4, [Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->FAX_QUALITY_TYPE_FINE:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->FAX_QUALITY_TYPE_STANDARD:Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->mValue:I

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/FaxResolution$EnumResolution;->mValue:I

    return v0
.end method
