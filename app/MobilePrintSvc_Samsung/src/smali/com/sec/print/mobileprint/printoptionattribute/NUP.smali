.class public Lcom/sec/print/mobileprint/printoptionattribute/NUP;
.super Lcom/sec/print/mobileprint/printoptionattribute/Layout;
.source "NUP.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/NUP;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private directionUpAndRight:Z

.field private marginNUP:I

.field private valueNUP:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 5
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 38
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->LAYOUT_1UP:Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    .line 39
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    .line 41
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 0
    .param p1, "nUpValue"    # I
    .param p2, "marginNUP"    # I
    .param p3, "directionUpAndRight"    # Z

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 52
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    .line 53
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    .line 54
    iput-boolean p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->readFromParcel(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/NUP;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/NUP;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;IZ)V
    .locals 1
    .param p1, "eLayout"    # Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    .param p2, "marginNUP"    # I
    .param p3, "directionUpAndRight"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 45
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    .line 46
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    .line 47
    iput-boolean p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public getMarginNUP()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    return v0
.end method

.method public getNUP()Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    .locals 6

    .prologue
    .line 64
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 72
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 64
    :cond_1
    aget-object v0, v1, v2

    .line 66
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    if-eq v4, v5, :cond_0

    .line 64
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isDirectionUpAndRight()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 118
    .local v0, "directionValue":I
    if-ne v0, v2, :cond_0

    .line 120
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    goto :goto_0
.end method

.method public setDirectionUpAndRight(Z)V
    .locals 0
    .param p1, "directionUpAndRight"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    .line 86
    return-void
.end method

.method public setMarginNUP(I)V
    .locals 0
    .param p1, "marginNUP"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    .line 81
    return-void
.end method

.method public setNUP(Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;)V
    .locals 1
    .param p1, "eLayout"    # Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/NUP$EnumNUP;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    .line 60
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->valueNUP:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->marginNUP:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/NUP;->directionUpAndRight:Z

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
