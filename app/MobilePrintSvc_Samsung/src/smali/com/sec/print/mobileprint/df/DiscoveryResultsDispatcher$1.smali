.class Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;
.super Ljava/lang/Object;
.source "DiscoveryResultsDispatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

.field private final synthetic val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;Lcom/sec/print/mobileprint/df/DiscoveryResult;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

    iput-object p2, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 54
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;->listeners:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;->access$0(Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    return-void

    .line 54
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/df/DiscoveryListener;

    .line 56
    .local v1, "listener":Lcom/sec/print/mobileprint/df/DiscoveryListener;
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/DiscoveryResult;->getException()Ljava/lang/Throwable;

    move-result-object v3

    if-nez v3, :cond_1

    .line 57
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/DiscoveryResult;->getHost()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/DiscoveryResult;->getVersion()I

    move-result v4

    invoke-interface {v1, v3, v4}, Lcom/sec/print/mobileprint/df/DiscoveryListener;->newDeviceFound(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 59
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/DiscoveryResult;->getHost()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryResultsDispatcher$1;->val$newDevice:Lcom/sec/print/mobileprint/df/DiscoveryResult;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/DiscoveryResult;->getException()Ljava/lang/Throwable;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/sec/print/mobileprint/df/DiscoveryListener;->newDeviceFoundException(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
