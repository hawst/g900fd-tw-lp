.class public Lcom/sec/print/mobileprint/io/SPSFileOutputStream;
.super Ljava/lang/Object;
.source "SPSFileOutputStream.java"

# interfaces
.implements Lcom/sec/print/mobileprint/io/ISPSOutputStream;


# instance fields
.field fileFullPath:Ljava/lang/String;

.field fileOutputStream:Ljava/io/FileOutputStream;

.field isCancel:Z

.field isOccurError:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileFullPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isCancel:Z

    .line 17
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    .line 21
    iput-object p1, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileFullPath:Ljava/lang/String;

    .line 22
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isCancel:Z

    .line 23
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    .line 24
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isCancel:Z

    .line 30
    return-void
.end method

.method public close()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileOutputStream:Ljava/io/FileOutputStream;

    if-nez v3, :cond_0

    .line 50
    :goto_0
    return v1

    .line 40
    :cond_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 41
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 50
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "close exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public isConnect()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public open()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 92
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileFullPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileOutputStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return v1

    .line 93
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/io/FileNotFoundException;
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "open exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 98
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public write([BI)Z
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "length"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 56
    iget-boolean v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    if-eqz v3, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->fileOutputStream:Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4, p2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isOccurError:Z

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "write exception : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-boolean v3, p0, Lcom/sec/print/mobileprint/io/SPSFileOutputStream;->isCancel:Z

    if-nez v3, :cond_0

    .line 71
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v1, v2

    .line 72
    goto :goto_0
.end method
