.class Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "DeviceInfoLoader.java"


# static fields
.field public static TYPE_LOAD_ALL:I

.field public static TYPE_LOAD_MODELLIST_WITH_VIDPID:I

.field public static TYPE_LOAD_ONLY_CAPABILITY:I

.field public static TYPE_LOAD_ONLY_MODEL_SERIES:I

.field public static TYPE_LOAD_ONLY_SPECIFIC_MODEL_CAPABILITY:I

.field public static TYPE_LOAD_ONLY_VERSION_STRING:I


# instance fields
.field private builder:Ljava/lang/StringBuilder;

.field private currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

.field private currentModelName:Ljava/lang/String;

.field private currentPid:Ljava/lang/String;

.field private currentProductNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentVid:Ljava/lang/String;

.field private deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

.field private loadType:I

.field private modelListVer:Ljava/lang/String;

.field private requestFindModelName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x1

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ALL:I

    .line 252
    const/4 v0, 0x2

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_MODEL_SERIES:I

    .line 253
    const/4 v0, 0x3

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_CAPABILITY:I

    .line 254
    const/4 v0, 0x4

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_SPECIFIC_MODEL_CAPABILITY:I

    .line 255
    const/4 v0, 0x5

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_MODELLIST_WITH_VIDPID:I

    .line 256
    const/4 v0, 0x6

    sput v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_VERSION_STRING:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "loadType"    # I

    .prologue
    const/4 v1, 0x0

    .line 261
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    .line 259
    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    .line 262
    iput p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    .line 263
    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 266
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    .line 267
    sget v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_SPECIFIC_MODEL_CAPABILITY:I

    iput v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    .line 268
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    .line 269
    return-void
.end method

.method private isRequireCapability()Z
    .locals 2

    .prologue
    .line 301
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ALL:I

    if-eq v0, v1, :cond_0

    .line 302
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_CAPABILITY:I

    if-eq v0, v1, :cond_0

    .line 303
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_SPECIFIC_MODEL_CAPABILITY:I

    if-ne v0, v1, :cond_1

    .line 304
    :cond_0
    const/4 v0, 0x1

    .line 307
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRequireModelSeries()Z
    .locals 2

    .prologue
    .line 320
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ALL:I

    if-eq v0, v1, :cond_0

    .line 321
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_MODEL_SERIES:I

    if-ne v0, v1, :cond_1

    .line 322
    :cond_0
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRequiredVIDPID()Z
    .locals 2

    .prologue
    .line 311
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ALL:I

    if-eq v0, v1, :cond_0

    .line 312
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_MODELLIST_WITH_VIDPID:I

    if-ne v0, v1, :cond_1

    .line 313
    :cond_0
    const/4 v0, 0x1

    .line 315
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 331
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/DefaultHandler;->characters([CII)V

    .line 333
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 336
    :cond_0
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 425
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/DefaultHandler;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    const-string v1, "Version"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 427
    iget v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_VERSION_STRING:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->modelListVer:Ljava/lang/String;

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    const-string v1, "Model"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 432
    iget v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_SPECIFIC_MODEL_CAPABILITY:I

    if-ne v1, v2, :cond_2

    .line 433
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->requestFindModelName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentModelName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentModelName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentProductNames:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v5, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentPid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->addDevice(Ljava/lang/String;Ljava/util/ArrayList;Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;Ljava/lang/String;)V

    .line 435
    new-instance v1, Lorg/xml/sax/SAXException;

    const-string v2, "Found"

    invoke-direct {v1, v2}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 438
    :cond_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentModelName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentProductNames:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v5, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentPid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->addDevice(Ljava/lang/String;Ljava/util/ArrayList;Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_3
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    if-eqz v1, :cond_0

    .line 445
    const-string v1, "ModelName"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 446
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentModelName:Ljava/lang/String;

    goto :goto_0

    .line 447
    :cond_4
    const-string v1, "ModelSeries"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 448
    new-instance v0, Ljava/util/StringTokenizer;

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    .local v0, "st":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentProductNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 452
    .end local v0    # "st":Ljava/util/StringTokenizer;
    :cond_5
    const-string v1, "VID"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 453
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentVid:Ljava/lang/String;

    goto/16 :goto_0

    .line 454
    :cond_6
    const-string v1, "PID"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 455
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentPid:Ljava/lang/String;

    goto/16 :goto_0

    .line 456
    :cond_7
    const-string v1, "Emulation"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 457
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setEmulation(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 458
    :cond_8
    const-string v1, "Duplex"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 459
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setDuplex(Z)V

    goto/16 :goto_0

    .line 460
    :cond_9
    const-string v1, "ColorMode"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 461
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Color"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 462
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setColorMode(Z)V

    goto/16 :goto_0

    .line 464
    :cond_a
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setColorMode(Z)V

    goto/16 :goto_0

    .line 466
    :cond_b
    const-string v1, "JobAccountingVer"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 467
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setJobAccountinVer(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 468
    :cond_c
    const-string v1, "PCL6Compression"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 469
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setPCL6CompressionType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 470
    :cond_d
    const-string v1, "PaperType"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 471
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setPaperType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 472
    :cond_e
    const-string v1, "PaperSize"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 473
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setPaperSize(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 474
    :cond_f
    const-string v1, "Compression"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 475
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setSplCompressionType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 476
    :cond_10
    const-string v1, "CTSFile"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 477
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setCmsFileName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 478
    :cond_11
    const-string v1, "SPLVersion"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 479
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setSplVersion(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 480
    :cond_12
    const-string v1, "WidthAlign"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    iget-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->setSplWidthAlign(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDeviceLists()Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    return-object v0
.end method

.method public getModelSerieList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->getProductList()Ljava/util/HashMap;

    move-result-object v0

    .line 279
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPIDList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->getProductListWithVidPidInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->modelListVer:Ljava/lang/String;

    return-object v0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 340
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->startDocument()V

    .line 342
    new-instance v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->deviceList:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    .line 343
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 348
    invoke-super {p0, p1, p2, p3, p4}, Lorg/xml/sax/helpers/DefaultHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 349
    iput-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    .line 350
    const-string v0, "Version"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    iget v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->loadType:I

    sget v1, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_VERSION_STRING:I

    if-ne v0, v1, :cond_1

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    const-string v0, "Model"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    iput-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentModelName:Ljava/lang/String;

    .line 355
    iput-object v2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentProductNames:Ljava/util/ArrayList;

    .line 356
    new-instance v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    goto :goto_0

    .line 357
    :cond_2
    const-string v0, "ModelName"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto :goto_0

    .line 359
    :cond_3
    const-string v0, "ModelSeries"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 360
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireModelSeries()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->currentProductNames:Ljava/util/ArrayList;

    goto :goto_0

    .line 365
    :cond_4
    const-string v0, "VID"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 366
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequiredVIDPID()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto :goto_0

    .line 370
    :cond_5
    const-string v0, "PID"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 371
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequiredVIDPID()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto :goto_0

    .line 375
    :cond_6
    const-string v0, "Emulation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 376
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 379
    :cond_7
    const-string v0, "Duplex"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 380
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 383
    :cond_8
    const-string v0, "ColorMode"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 384
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 387
    :cond_9
    const-string v0, "JobAccountingVer"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 388
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 391
    :cond_a
    const-string v0, "PCL6Compression"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 392
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 395
    :cond_b
    const-string v0, "PaperType"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 396
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 399
    :cond_c
    const-string v0, "PaperSize"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 400
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 403
    :cond_d
    const-string v0, "Compression"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 404
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 407
    :cond_e
    const-string v0, "CTSFile"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 408
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 411
    :cond_f
    const-string v0, "WidthAlign"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 412
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 415
    :cond_10
    const-string v0, "SPLVersion"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    invoke-direct {p0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->isRequireCapability()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->builder:Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
