.class public Lcom/sec/print/mobileprint/df/MFPDiscoveryService;
.super Landroid/app/Service;
.source "MFPDiscoveryService.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/DiscoveryListener;
.implements Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    }
.end annotation


# static fields
.field private static final MP_DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "MFPDiscoveryService"

.field private static mediaSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPMediaSize;",
            ">;"
        }
    .end annotation
.end field

.field private static mediaTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final prtMediaPathTypeOid:Lorg/snmp4j/smi/OID;

.field private static final samsungHostNameOid:Lorg/snmp4j/smi/OID;

.field private static final samsungLocationOid:Lorg/snmp4j/smi/OID;

.field private static final samsungModeOid:Lorg/snmp4j/smi/OID;

.field private static final samsungModeOid2:Lorg/snmp4j/smi/OID;

.field private static final samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

.field private static scannerModels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static scannerModelsESCL:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static scannerModelsNet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static scannerModelsUSB:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private communityName:Ljava/lang/String;

.field private final devicesRow:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final discoveryFaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field

.field private discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

.field private discoveryIpRangeInstance:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/DiscoveryIpRange;",
            ">;"
        }
    .end annotation
.end field

.field private final discoveryPrinters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final discoveryScanners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field

.field private discoveryType:I

.field private discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

.field private executorRow:Ljava/util/concurrent/ExecutorService;

.field private jmdnsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljavax/jmdns/JmDNS;",
            ">;"
        }
    .end annotation
.end field

.field private listenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mBinder:Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;

.field private mWifiDirectIntentFilter:Landroid/content/IntentFilter;

.field private smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;

.field private snmp:Lorg/snmp4j/Snmp;

.field private snmpV3authPassword:Ljava/lang/String;

.field private snmpV3authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

.field private snmpV3configired:Z

.field private snmpV3contextName:Ljava/lang/String;

.field private snmpV3privPassword:Ljava/lang/String;

.field private snmpV3privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

.field private snmpV3userName:Ljava/lang/String;

.field private vendorsFilter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaTypes:Ljava/util/List;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    .line 103
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.4.1.2699.1.2.1.2.1.1.3.1"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    .line 105
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.4.1.236.11.5.1.1.1.26.0"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid2:Lorg/snmp4j/smi/OID;

    .line 110
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.2.1.1.5.0"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungHostNameOid:Lorg/snmp4j/smi/OID;

    .line 111
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.2.1.1.6.0"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungLocationOid:Lorg/snmp4j/smi/OID;

    .line 112
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.4.1.236.11.5.1.1.1.4.0"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

    .line 118
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.2.1.43.13.4.1.9"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->prtMediaPathTypeOid:Lorg/snmp4j/smi/OID;

    .line 128
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    .line 129
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;

    .line 130
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;

    .line 149
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaTypes:Ljava/util/List;

    const-string v1, "Normal"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "A4"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xdb3

    const/16 v7, 0x9b0

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "A5"

    const/16 v2, 0x39

    const/16 v3, 0x39

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x9b0

    const/16 v7, 0x6d4

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "A6"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x6d4

    const/16 v7, 0x4d8

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "JISB5"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xbdb

    const/16 v7, 0x865

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "Letter"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xce4

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "Legal"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x1068

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "Executive"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xc4e

    const/16 v7, 0x87f

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "Folio"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xf3c

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "A3"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x1360

    const/16 v7, 0xdb3

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "Tabloid"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x13ec

    const/16 v7, 0xce4

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/df/MFPMediaSize;

    const-string v1, "JISB4"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x10c2

    const/16 v7, 0xbd6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-6200 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-6240 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x24 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x28 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-3170 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4500W Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-6x55 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-8380 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-3180 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 174
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x26 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-6545 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4623 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-5635 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-5835_5935 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 179
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-8540 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-8385 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-6220 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-6250 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4623FW Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 184
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-3200 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-6545X Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-6x55X Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-8385X Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 188
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung CLX-8540X Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-5835_5935X Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x25 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 192
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;

    const-string v1, "Samsung CLX-9250 9350 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;

    const-string v1, "Samsung SCX-8030 8040 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-5x30 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung CLX-216x Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung CLX-3160 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4200 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-6x22 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 200
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4725 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-6x45 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 202
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4500 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 203
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4300 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x20 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4x21 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 206
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-1455 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-6x20 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-1430_1450Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 209
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-1630_1650 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 210
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-1460_1470 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung MFP 65x Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 212
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    const-string v1, "Samsung SCX-4600 Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;

    const-string v1, "Samsung SCX-1365W Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;

    const-string v1, "Samsung SL-J1760W Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;

    const-string v1, "Samsung SL-J1760FW Series"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 80
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    .line 83
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    .line 85
    iput v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    .line 87
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    .line 121
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    .line 122
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    .line 123
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    .line 124
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    .line 126
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    .line 134
    const-string v0, "public"

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    .line 137
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    .line 138
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    .line 146
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    .line 3226
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;-><init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mBinder:Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;

    .line 72
    return-void
.end method

.method private IsConfidentialPrintAvailableBySNMP(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Ljava/util/List;)I
    .locals 12
    .param p1, "pdu"    # Lorg/snmp4j/PDU;
    .param p2, "target"    # Lorg/snmp4j/Target;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/snmp4j/PDU;",
            "Lorg/snmp4j/Target;",
            "Ljava/util/List",
            "<",
            "Lorg/snmp4j/smi/OID;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p3, "oids":Ljava/util/List;, "Ljava/util/List<Lorg/snmp4j/smi/OID;>;"
    const/4 v11, 0x2

    const/4 v8, -0x1

    .line 945
    const-string v9, "MFPDiscoveryService"

    const-string v10, "IsConfidentialPrintAvailableBySNMP"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 950
    const/16 v9, -0x60

    invoke-virtual {p1, v9}, Lorg/snmp4j/PDU;->setType(I)V

    .line 952
    iget-object v9, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v9, p1, p2}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 953
    .local v5, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    if-nez v9, :cond_2

    .line 988
    :cond_0
    :goto_1
    return v8

    .line 947
    .end local v5    # "response":Lorg/snmp4j/event/ResponseEvent;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/snmp4j/smi/OID;

    .line 948
    .local v4, "oid":Lorg/snmp4j/smi/OID;
    new-instance v10, Lorg/snmp4j/smi/VariableBinding;

    invoke-direct {v10, v4}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {p1, v10}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    goto :goto_0

    .line 956
    .end local v4    # "oid":Lorg/snmp4j/smi/OID;
    .restart local v5    # "response":Lorg/snmp4j/event/ResponseEvent;
    :cond_2
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 958
    .local v6, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    .line 959
    .local v1, "error":I
    :goto_2
    if-eq v1, v11, :cond_3

    .line 974
    if-nez v1, :cond_0

    .line 977
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->size()I

    move-result v8

    if-lt v3, v8, :cond_4

    .line 988
    const/4 v8, 0x0

    goto :goto_1

    .line 960
    .end local v3    # "i":I
    :cond_3
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v9

    invoke-virtual {p1}, Lorg/snmp4j/PDU;->size()I

    move-result v10

    if-gt v9, v10, :cond_0

    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v9

    if-lez v9, :cond_0

    .line 963
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v2

    .line 964
    .local v2, "errorIndex":I
    add-int/lit8 v9, v2, -0x1

    invoke-virtual {p1, v9}, Lorg/snmp4j/PDU;->remove(I)V

    .line 966
    iget-object v9, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v9, p1, p2}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 967
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 970
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 971
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    goto :goto_2

    .line 978
    .end local v2    # "errorIndex":I
    .restart local v3    # "i":I
    :cond_4
    invoke-virtual {v6, v3}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 979
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v9, 0x81

    if-eq v8, v9, :cond_5

    .line 980
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v9, 0x80

    if-eq v8, v9, :cond_5

    .line 981
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    if-ne v8, v11, :cond_5

    .line 982
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 983
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v7

    check-cast v7, Lorg/snmp4j/smi/Integer32;

    .line 984
    .local v7, "value":Lorg/snmp4j/smi/Integer32;
    invoke-virtual {v7}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v8

    if-lez v8, :cond_5

    .line 985
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 977
    .end local v7    # "value":Lorg/snmp4j/smi/Integer32;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method static synthetic access$0(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;I)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    return-void
.end method

.method static synthetic access$10(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2630
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceLocationBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2686
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSerialNumberBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3userName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$15(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3authPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$17(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3privPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2742
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$21()Ljava/util/Set;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$22()Ljava/util/Set;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$23()Ljava/util/Set;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SmbDiscovery;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    return-void
.end method

.method static synthetic access$4()Ljava/util/List;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5()Ljava/util/List;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaTypes:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 3122
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getCupsPrinters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 3174
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getIppPrinter(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    return v0
.end method

.method private static getAlertNameByType(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 711
    sparse-switch p0, :sswitch_data_0

    .line 807
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 712
    :sswitch_0
    const-string v0, "other"

    goto :goto_0

    .line 713
    :sswitch_1
    const-string v0, "coveropen"

    goto :goto_0

    .line 714
    :sswitch_2
    const-string v0, "cover_closed"

    goto :goto_0

    .line 715
    :sswitch_3
    const-string v0, "interlock_open"

    goto :goto_0

    .line 716
    :sswitch_4
    const-string v0, "interlock_closed"

    goto :goto_0

    .line 717
    :sswitch_5
    const-string v0, "configuration_change"

    goto :goto_0

    .line 718
    :sswitch_6
    const-string v0, "jam"

    goto :goto_0

    .line 719
    :sswitch_7
    const-string v0, "subunit_missing"

    goto :goto_0

    .line 720
    :sswitch_8
    const-string v0, "subunit_life_almost_over"

    goto :goto_0

    .line 721
    :sswitch_9
    const-string v0, "subunit_life_over"

    goto :goto_0

    .line 722
    :sswitch_a
    const-string v0, "subunit_almost_empty"

    goto :goto_0

    .line 723
    :sswitch_b
    const-string v0, "subunit_empty"

    goto :goto_0

    .line 724
    :sswitch_c
    const-string v0, "subunit_almost_full"

    goto :goto_0

    .line 725
    :sswitch_d
    const-string v0, "subunit_full"

    goto :goto_0

    .line 726
    :sswitch_e
    const-string v0, "subunit_near_limit"

    goto :goto_0

    .line 727
    :sswitch_f
    const-string v0, "subunit_at_limit"

    goto :goto_0

    .line 728
    :sswitch_10
    const-string v0, "subunit_opened"

    goto :goto_0

    .line 729
    :sswitch_11
    const-string v0, "subunit_closed"

    goto :goto_0

    .line 730
    :sswitch_12
    const-string v0, "subunit_turned_on"

    goto :goto_0

    .line 731
    :sswitch_13
    const-string v0, "subunit_turned_off"

    goto :goto_0

    .line 732
    :sswitch_14
    const-string v0, "subunit_offline"

    goto :goto_0

    .line 733
    :sswitch_15
    const-string v0, "subunit_power_saver"

    goto :goto_0

    .line 734
    :sswitch_16
    const-string v0, "subunit_warming_up"

    goto :goto_0

    .line 735
    :sswitch_17
    const-string v0, "subunit_added"

    goto :goto_0

    .line 736
    :sswitch_18
    const-string v0, "subunit_removed"

    goto :goto_0

    .line 737
    :sswitch_19
    const-string v0, "subunit_resource_added"

    goto :goto_0

    .line 738
    :sswitch_1a
    const-string v0, "subunit_resource_removed"

    goto :goto_0

    .line 739
    :sswitch_1b
    const-string v0, "subunit_recoverable_failure"

    goto :goto_0

    .line 740
    :sswitch_1c
    const-string v0, "subunit_unrecoverable_failure"

    goto :goto_0

    .line 741
    :sswitch_1d
    const-string v0, "subunit_recoverable_storage_error"

    goto :goto_0

    .line 742
    :sswitch_1e
    const-string v0, "subunit_unrecoverable_storage_error"

    goto :goto_0

    .line 743
    :sswitch_1f
    const-string v0, "subunit_motor_failure"

    goto :goto_0

    .line 744
    :sswitch_20
    const-string v0, "subunit_memory_exhausted"

    goto :goto_0

    .line 745
    :sswitch_21
    const-string v0, "subunit_under_temperature"

    goto :goto_0

    .line 746
    :sswitch_22
    const-string v0, "subunit_over_temperature"

    goto :goto_0

    .line 747
    :sswitch_23
    const-string v0, "subunit_timing_failure"

    goto :goto_0

    .line 748
    :sswitch_24
    const-string v0, "subunit_thermistor_failure"

    goto :goto_0

    .line 749
    :sswitch_25
    const-string v0, "dooropen"

    goto :goto_0

    .line 750
    :sswitch_26
    const-string v0, "doorclosed"

    goto :goto_0

    .line 751
    :sswitch_27
    const-string v0, "power_up"

    goto :goto_0

    .line 752
    :sswitch_28
    const-string v0, "power_down"

    goto :goto_0

    .line 753
    :sswitch_29
    const-string v0, "printer_nms_reset"

    goto :goto_0

    .line 754
    :sswitch_2a
    const-string v0, "printer_manual_reset"

    goto/16 :goto_0

    .line 755
    :sswitch_2b
    const-string v0, "printer_ready_to_print"

    goto/16 :goto_0

    .line 756
    :sswitch_2c
    const-string v0, "input_media_tray_missing"

    goto/16 :goto_0

    .line 757
    :sswitch_2d
    const-string v0, "input_media_size_change"

    goto/16 :goto_0

    .line 758
    :sswitch_2e
    const-string v0, "input_media_weight_change"

    goto/16 :goto_0

    .line 759
    :sswitch_2f
    const-string v0, "input_media_type_change"

    goto/16 :goto_0

    .line 760
    :sswitch_30
    const-string v0, "input_media_color_change"

    goto/16 :goto_0

    .line 761
    :sswitch_31
    const-string v0, "input_media_form_parts_change"

    goto/16 :goto_0

    .line 762
    :sswitch_32
    const-string v0, "input_media_supply_low"

    goto/16 :goto_0

    .line 763
    :sswitch_33
    const-string v0, "input_media_supply_empty"

    goto/16 :goto_0

    .line 764
    :sswitch_34
    const-string v0, "input_media_change_request"

    goto/16 :goto_0

    .line 765
    :sswitch_35
    const-string v0, "input_manual_input_request"

    goto/16 :goto_0

    .line 766
    :sswitch_36
    const-string v0, "input_tray_position_failure"

    goto/16 :goto_0

    .line 767
    :sswitch_37
    const-string v0, "input_tray_elevation_failure"

    goto/16 :goto_0

    .line 768
    :sswitch_38
    const-string v0, "input_cannot_feed_size_selected"

    goto/16 :goto_0

    .line 769
    :sswitch_39
    const-string v0, "output_media_tray_missing"

    goto/16 :goto_0

    .line 770
    :sswitch_3a
    const-string v0, "output_media_tray_almost_full"

    goto/16 :goto_0

    .line 771
    :sswitch_3b
    const-string v0, "output_media_tray_full"

    goto/16 :goto_0

    .line 772
    :sswitch_3c
    const-string v0, "output_mailbox_select_failure"

    goto/16 :goto_0

    .line 773
    :sswitch_3d
    const-string v0, "marker_fuser_under_temperature"

    goto/16 :goto_0

    .line 774
    :sswitch_3e
    const-string v0, "marker_fuser_over_temperature"

    goto/16 :goto_0

    .line 775
    :sswitch_3f
    const-string v0, "marker_fuser_timing_failure"

    goto/16 :goto_0

    .line 776
    :sswitch_40
    const-string v0, "marker_fuser_thermistor_failure"

    goto/16 :goto_0

    .line 777
    :sswitch_41
    const-string v0, "marker_adjusting_print_quality"

    goto/16 :goto_0

    .line 778
    :sswitch_42
    const-string v0, "marker_toner_empty"

    goto/16 :goto_0

    .line 779
    :sswitch_43
    const-string v0, "marker_ink_empty"

    goto/16 :goto_0

    .line 780
    :sswitch_44
    const-string v0, "marker_print_ribbon_empty"

    goto/16 :goto_0

    .line 781
    :sswitch_45
    const-string v0, "marker_toner_almost_empty"

    goto/16 :goto_0

    .line 782
    :sswitch_46
    const-string v0, "marker_ink_almost_empty"

    goto/16 :goto_0

    .line 783
    :sswitch_47
    const-string v0, "marker_print_ribbon_almost_empty"

    goto/16 :goto_0

    .line 784
    :sswitch_48
    const-string v0, "marker_waste_toner_receptacle_almost_full"

    goto/16 :goto_0

    .line 785
    :sswitch_49
    const-string v0, "marker_waste_ink_receptacle_almost_full"

    goto/16 :goto_0

    .line 786
    :sswitch_4a
    const-string v0, "marker_waste_toner_receptacle_full"

    goto/16 :goto_0

    .line 787
    :sswitch_4b
    const-string v0, "marker_waste_ink_receptacle_full"

    goto/16 :goto_0

    .line 788
    :sswitch_4c
    const-string v0, "marker_opc_life_almost_over"

    goto/16 :goto_0

    .line 789
    :sswitch_4d
    const-string v0, "marker_opc_life_over"

    goto/16 :goto_0

    .line 790
    :sswitch_4e
    const-string v0, "marker_developer_almost_empty"

    goto/16 :goto_0

    .line 791
    :sswitch_4f
    const-string v0, "marker_developer_empty"

    goto/16 :goto_0

    .line 792
    :sswitch_50
    const-string v0, "marker_toner_cartridge_missing"

    goto/16 :goto_0

    .line 793
    :sswitch_51
    const-string v0, "media_path_media_tray_missing"

    goto/16 :goto_0

    .line 794
    :sswitch_52
    const-string v0, "media_path_media_tray_almost_full"

    goto/16 :goto_0

    .line 795
    :sswitch_53
    const-string v0, "media_path_media_tray_full"

    goto/16 :goto_0

    .line 796
    :sswitch_54
    const-string v0, "media_path_cannot_duplex_media_selected"

    goto/16 :goto_0

    .line 797
    :sswitch_55
    const-string v0, "interpreter_memory_increase"

    goto/16 :goto_0

    .line 798
    :sswitch_56
    const-string v0, "interpreter_memory_decrease"

    goto/16 :goto_0

    .line 799
    :sswitch_57
    const-string v0, "interpreter_cartridge_added"

    goto/16 :goto_0

    .line 800
    :sswitch_58
    const-string v0, "interpreter_cartridge_deleted"

    goto/16 :goto_0

    .line 801
    :sswitch_59
    const-string v0, "interpreter_resource_added"

    goto/16 :goto_0

    .line 802
    :sswitch_5a
    const-string v0, "interpreter_resource_deleted"

    goto/16 :goto_0

    .line 803
    :sswitch_5b
    const-string v0, "interpreter_resource_unavailable"

    goto/16 :goto_0

    .line 804
    :sswitch_5c
    const-string v0, "interpreter_complex_page_encountered"

    goto/16 :goto_0

    .line 805
    :sswitch_5d
    const-string v0, "alert_removal_of_binary_change_entry"

    goto/16 :goto_0

    .line 711
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_6
        0x9 -> :sswitch_7
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0xc -> :sswitch_a
        0xd -> :sswitch_b
        0xe -> :sswitch_c
        0xf -> :sswitch_d
        0x10 -> :sswitch_e
        0x11 -> :sswitch_f
        0x12 -> :sswitch_10
        0x13 -> :sswitch_11
        0x14 -> :sswitch_12
        0x15 -> :sswitch_13
        0x16 -> :sswitch_14
        0x17 -> :sswitch_15
        0x18 -> :sswitch_16
        0x19 -> :sswitch_17
        0x1a -> :sswitch_18
        0x1b -> :sswitch_19
        0x1c -> :sswitch_1a
        0x1d -> :sswitch_1b
        0x1e -> :sswitch_1c
        0x1f -> :sswitch_1d
        0x20 -> :sswitch_1e
        0x21 -> :sswitch_1f
        0x22 -> :sswitch_20
        0x23 -> :sswitch_21
        0x24 -> :sswitch_22
        0x25 -> :sswitch_23
        0x26 -> :sswitch_24
        0x1f5 -> :sswitch_25
        0x1f6 -> :sswitch_26
        0x1f7 -> :sswitch_27
        0x1f8 -> :sswitch_28
        0x1f9 -> :sswitch_29
        0x1fa -> :sswitch_2a
        0x1fb -> :sswitch_2b
        0x321 -> :sswitch_2c
        0x322 -> :sswitch_2d
        0x323 -> :sswitch_2e
        0x324 -> :sswitch_2f
        0x325 -> :sswitch_30
        0x326 -> :sswitch_31
        0x327 -> :sswitch_32
        0x328 -> :sswitch_33
        0x329 -> :sswitch_34
        0x32a -> :sswitch_35
        0x32b -> :sswitch_36
        0x32c -> :sswitch_37
        0x32d -> :sswitch_38
        0x385 -> :sswitch_39
        0x386 -> :sswitch_3a
        0x387 -> :sswitch_3b
        0x388 -> :sswitch_3c
        0x3e9 -> :sswitch_3d
        0x3ea -> :sswitch_3e
        0x3eb -> :sswitch_3f
        0x3ec -> :sswitch_40
        0x3ed -> :sswitch_41
        0x44d -> :sswitch_42
        0x44e -> :sswitch_43
        0x44f -> :sswitch_44
        0x450 -> :sswitch_45
        0x451 -> :sswitch_46
        0x452 -> :sswitch_47
        0x453 -> :sswitch_48
        0x454 -> :sswitch_49
        0x455 -> :sswitch_4a
        0x456 -> :sswitch_4b
        0x457 -> :sswitch_4c
        0x458 -> :sswitch_4d
        0x459 -> :sswitch_4e
        0x45a -> :sswitch_4f
        0x45b -> :sswitch_50
        0x515 -> :sswitch_51
        0x516 -> :sswitch_52
        0x517 -> :sswitch_53
        0x518 -> :sswitch_54
        0x5dd -> :sswitch_55
        0x5de -> :sswitch_56
        0x5df -> :sswitch_57
        0x5e0 -> :sswitch_58
        0x5e1 -> :sswitch_59
        0x5e2 -> :sswitch_5a
        0x5e3 -> :sswitch_5b
        0x5e5 -> :sswitch_5c
        0x709 -> :sswitch_5d
    .end sparse-switch
.end method

.method private getColorBySNMP(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Lorg/snmp4j/smi/OID;)Ljava/lang/String;
    .locals 6
    .param p1, "pdu"    # Lorg/snmp4j/PDU;
    .param p2, "target"    # Lorg/snmp4j/Target;
    .param p3, "oid"    # Lorg/snmp4j/smi/OID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1372
    const-string v4, "MFPDiscoveryService"

    const-string v5, "getColorBySNMP"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-direct {v4, p3}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {p1, v4}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1375
    const/16 v4, -0x60

    invoke-virtual {p1, v4}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1377
    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v4, p1, p2}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v2

    .line 1378
    .local v2, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v2, :cond_1

    .line 1396
    :cond_0
    :goto_0
    return-object v3

    .line 1381
    :cond_1
    invoke-virtual {v2}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1384
    invoke-virtual {v2}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v4

    if-nez v4, :cond_0

    .line 1387
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/PDU;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1388
    invoke-virtual {v2}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v4

    invoke-virtual {v4, v1}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1389
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v4

    const/16 v5, 0x81

    if-eq v4, v5, :cond_2

    .line 1390
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v4

    const/16 v5, 0x80

    if-eq v4, v5, :cond_2

    .line 1391
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 1392
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1393
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v3

    check-cast v3, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v3}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1387
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getCupsPrinters(Ljava/lang/String;)Ljava/util/List;
    .locals 23
    .param p1, "urlString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 3123
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 3124
    .local v13, "printers":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 3127
    .local v7, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v17, "requested-attributes"

    .line 3128
    const-string v18, "copies-supported page-ranges-supported printer-name printer-info printer-location printer-make-and-model printer-uri-supported"

    .line 3126
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3130
    new-instance v16, Ljava/net/URL;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 3131
    .local v16, "url":Ljava/net/URL;
    new-instance v4, Lorg/cups4j/operations/cups/CupsGetPrintersOperation;

    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->getPort()I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v4, v0}, Lorg/cups4j/operations/cups/CupsGetPrintersOperation;-><init>(I)V

    .line 3132
    .local v4, "command":Lorg/cups4j/operations/cups/CupsGetPrintersOperation;
    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v7}, Lorg/cups4j/operations/cups/CupsGetPrintersOperation;->request(Ljava/net/URL;Ljava/util/Map;)Lch/ethz/vppserver/ippclient/IppResult;

    move-result-object v14

    .line 3133
    .local v14, "result":Lch/ethz/vppserver/ippclient/IppResult;
    invoke-virtual {v14}, Lch/ethz/vppserver/ippclient/IppResult;->getAttributeGroupList()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_1

    .line 3171
    return-object v13

    .line 3133
    :cond_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;

    .line 3134
    .local v6, "group":Lch/ethz/vppserver/schema/ippclient/AttributeGroup;
    invoke-virtual {v6}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getTagName()Ljava/lang/String;

    move-result-object v17

    const-string v19, "printer-attributes-tag"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 3135
    const/4 v11, 0x0

    .line 3136
    .local v11, "printerURI":Ljava/lang/String;
    const/4 v10, 0x0

    .line 3137
    .local v10, "printerName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 3138
    .local v8, "modelName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 3139
    .local v9, "printerLocation":Ljava/lang/String;
    invoke-virtual {v6}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getAttribute()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_2
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_3

    .line 3154
    const/4 v12, 0x0

    .line 3156
    .local v12, "printerUrl":Ljava/net/URL;
    :try_start_0
    new-instance v12, Ljava/net/URL;

    .end local v12    # "printerUrl":Ljava/net/URL;
    invoke-direct {v12, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3163
    .restart local v12    # "printerUrl":Ljava/net/URL;
    new-instance v5, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v5}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 3164
    .local v5, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, " ("

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v19, ")"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 3165
    invoke-virtual {v5, v9}, Lcom/sec/print/mobileprint/df/MFPDevice;->setNote(Ljava/lang/String;)V

    .line 3166
    invoke-virtual {v5, v11}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 3167
    invoke-virtual {v12}, Ljava/net/URL;->getPort()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setPort(I)V

    .line 3168
    invoke-interface {v13, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3139
    .end local v5    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .end local v12    # "printerUrl":Ljava/net/URL;
    :cond_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lch/ethz/vppserver/schema/ippclient/Attribute;

    .line 3140
    .local v3, "attr":Lch/ethz/vppserver/schema/ippclient/Attribute;
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v20, "printer-uri-supported"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 3142
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual/range {v17 .. v17}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v17

    const-string v20, "ipp://([^/]+)/"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 3143
    goto/16 :goto_1

    :cond_4
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v20, "printer-name"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 3144
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual/range {v17 .. v17}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v10

    .line 3145
    goto/16 :goto_1

    :cond_5
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v20, "printer-location"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 3146
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    if-eqz v17, :cond_2

    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_2

    .line 3147
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual/range {v17 .. v17}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 3148
    goto/16 :goto_1

    :cond_6
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v20, "printer-make-and-model"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 3149
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    if-eqz v17, :cond_2

    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_2

    .line 3150
    invoke-virtual {v3}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v17

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual/range {v17 .. v17}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 3157
    .end local v3    # "attr":Lch/ethz/vppserver/schema/ippclient/Attribute;
    :catch_0
    move-exception v15

    .line 3158
    .local v15, "t":Ljava/lang/Throwable;
    const-string v17, "cups4j"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Error encountered building URL from printer uri of printer "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 3159
    const-string v19, ", uri returned was ["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "].  Attribute group tag/description: ["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getTagName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 3160
    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getDescription()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 3158
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3161
    new-instance v17, Ljava/lang/Exception;

    move-object/from16 v0, v17

    invoke-direct {v0, v15}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    throw v17
.end method

.method private getDeviceIDByUSB(II)Ljava/lang/String;
    .locals 11
    .param p1, "vid"    # I
    .param p2, "pid"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2531
    const-string v1, "MFPDiscoveryService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDeviceIDByUSB: vid = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2533
    new-instance v0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    invoke-direct {v0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;-><init>()V

    .line 2534
    .local v0, "usbDevicePort":Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;
    invoke-virtual {v0, p1, p2, v6, v6}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->open(IIZZ)I

    .line 2536
    const/16 v1, 0xff

    new-array v5, v1, [B

    .line 2537
    .local v5, "buffer":[B
    const/16 v1, 0xa1

    array-length v6, v5

    const/16 v7, 0x3e8

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->requestOnEndpointZero(IIII[BII)I

    move-result v9

    .line 2538
    .local v9, "res":I
    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->close()I

    .line 2540
    const-string v1, "MFPDiscoveryService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getDeviceIDByUSB: device id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2542
    if-gez v9, :cond_0

    .line 2543
    const-string v1, "DeviceDiscovery"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot read DeviceId, err="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v10

    .line 2551
    :goto_0
    return-object v1

    .line 2548
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v5, v2, v9}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2549
    :catch_0
    move-exception v8

    .line 2550
    .local v8, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, "DeviceDiscovery: "

    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v10

    .line 2551
    goto :goto_0
.end method

.method private getDeviceLocationBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2631
    const-string v11, "MFPDiscoveryService"

    const-string v12, "getDeviceLocationBySNMP"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2633
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 2634
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_1

    .line 2683
    :cond_0
    :goto_0
    return-object v10

    .line 2638
    :cond_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v8

    .line 2639
    .local v8, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v8, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 2640
    const/4 v11, 0x1

    invoke-interface {v8, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 2642
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v4

    .line 2643
    .local v4, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    sget-object v12, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungLocationOid:Lorg/snmp4j/smi/OID;

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2645
    const/16 v11, -0x60

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 2647
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 2648
    .local v5, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 2651
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 2653
    .local v6, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    .line 2654
    .local v1, "error":I
    :goto_1
    const/4 v11, 0x2

    if-eq v1, v11, :cond_2

    .line 2669
    if-nez v1, :cond_0

    .line 2672
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v3, v11, :cond_0

    .line 2673
    invoke-virtual {v6, v3}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 2674
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_3

    .line 2675
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_3

    .line 2676
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/4 v12, 0x4

    if-ne v11, v12, :cond_3

    .line 2677
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 2678
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    check-cast v9, Lorg/snmp4j/smi/OctetString;

    .line 2679
    .local v9, "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v10, Ljava/lang/String;

    invoke-virtual {v9}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v11

    const-string v12, "UTF8"

    invoke-direct {v10, v11, v12}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/4 v11, 0x0

    const/16 v12, 0x20

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 2655
    .end local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v3    # "i":I
    .end local v9    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_2
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v11

    invoke-virtual {v4}, Lorg/snmp4j/PDU;->size()I

    move-result v12

    if-gt v11, v12, :cond_0

    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v11

    if-lez v11, :cond_0

    .line 2658
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v2

    .line 2659
    .local v2, "errorIndex":I
    add-int/lit8 v11, v2, -0x1

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->remove(I)V

    .line 2661
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 2662
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 2665
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 2666
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    goto :goto_1

    .line 2672
    .end local v2    # "errorIndex":I
    .restart local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private getIppPrinter(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice;
    .locals 21
    .param p1, "urlString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 3175
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 3178
    .local v6, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v15, "requested-attributes"

    .line 3179
    const-string v16, "copies-supported page-ranges-supported printer-name printer-info printer-location printer-make-and-model printer-uri-supported"

    .line 3177
    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3181
    new-instance v14, Ljava/net/URL;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 3182
    .local v14, "url":Ljava/net/URL;
    new-instance v3, Lorg/cups4j/operations/ipp/IppGetPrinterAttributesOperation;

    invoke-direct {v3}, Lorg/cups4j/operations/ipp/IppGetPrinterAttributesOperation;-><init>()V

    .line 3183
    .local v3, "command":Lorg/cups4j/operations/ipp/IppGetPrinterAttributesOperation;
    invoke-virtual {v3, v14, v6}, Lorg/cups4j/operations/ipp/IppGetPrinterAttributesOperation;->request(Ljava/net/URL;Ljava/util/Map;)Lch/ethz/vppserver/ippclient/IppResult;

    move-result-object v12

    .line 3185
    .local v12, "result":Lch/ethz/vppserver/ippclient/IppResult;
    const/4 v10, 0x0

    .line 3186
    .local v10, "printerURI":Ljava/lang/String;
    const/4 v9, 0x0

    .line 3187
    .local v9, "printerName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 3188
    .local v7, "modelName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 3189
    .local v8, "printerLocation":Ljava/lang/String;
    const/4 v11, 0x0

    .line 3191
    .local v11, "printerUrl":Ljava/net/URL;
    invoke-virtual {v12}, Lch/ethz/vppserver/ippclient/IppResult;->getAttributeGroupList()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_0
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_1

    .line 3218
    new-instance v4, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v4}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 3219
    .local v4, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 3220
    invoke-virtual {v4, v8}, Lcom/sec/print/mobileprint/df/MFPDevice;->setNote(Ljava/lang/String;)V

    .line 3221
    invoke-virtual {v4, v10}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 3222
    invoke-virtual {v11}, Ljava/net/URL;->getPort()I

    move-result v15

    invoke-virtual {v4, v15}, Lcom/sec/print/mobileprint/df/MFPDevice;->setPort(I)V

    .line 3223
    return-object v4

    .line 3191
    .end local v4    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;

    .line 3192
    .local v5, "group":Lch/ethz/vppserver/schema/ippclient/AttributeGroup;
    invoke-virtual {v5}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getTagName()Ljava/lang/String;

    move-result-object v15

    const-string v17, "printer-attributes-tag"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 3193
    invoke-virtual {v5}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getAttribute()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_3

    .line 3209
    :try_start_0
    new-instance v11, Ljava/net/URL;

    .end local v11    # "printerUrl":Ljava/net/URL;
    invoke-direct {v11, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v11    # "printerUrl":Ljava/net/URL;
    goto :goto_0

    .line 3193
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lch/ethz/vppserver/schema/ippclient/Attribute;

    .line 3194
    .local v2, "attr":Lch/ethz/vppserver/schema/ippclient/Attribute;
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v18, "printer-uri-supported"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 3196
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual {v15}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v15

    const-string v18, "ipp://([^/]+)/"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 3197
    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v18, "printer-name"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 3198
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual {v15}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 3199
    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v18, "printer-location"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 3200
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    if-eqz v15, :cond_2

    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_2

    .line 3201
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual {v15}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 3202
    goto/16 :goto_1

    :cond_6
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v18, "printer-make-and-model"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 3203
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    if-eqz v15, :cond_2

    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-lez v15, :cond_2

    .line 3204
    invoke-virtual {v2}, Lch/ethz/vppserver/schema/ippclient/Attribute;->getAttributeValue()Ljava/util/List;

    move-result-object v15

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lch/ethz/vppserver/schema/ippclient/AttributeValue;

    invoke-virtual {v15}, Lch/ethz/vppserver/schema/ippclient/AttributeValue;->getValue()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 3210
    .end local v2    # "attr":Lch/ethz/vppserver/schema/ippclient/Attribute;
    .end local v11    # "printerUrl":Ljava/net/URL;
    :catch_0
    move-exception v13

    .line 3211
    .local v13, "t":Ljava/lang/Throwable;
    const-string v15, "cups4j"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Error encountered building URL from printer uri of printer "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 3212
    const-string v17, ", uri returned was ["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "].  Attribute group tag/description: ["

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getTagName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 3213
    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Lch/ethz/vppserver/schema/ippclient/AttributeGroup;->getDescription()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 3211
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3214
    new-instance v15, Ljava/lang/Exception;

    invoke-direct {v15, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    throw v15
.end method

.method private getSerialNumberBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2687
    const-string v11, "MFPDiscoveryService"

    const-string v12, "getSerialNumberSNMP"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2689
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 2690
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_1

    .line 2739
    :cond_0
    :goto_0
    return-object v10

    .line 2694
    :cond_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v8

    .line 2695
    .local v8, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v8, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 2696
    const/4 v11, 0x1

    invoke-interface {v8, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 2698
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v4

    .line 2699
    .local v4, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    sget-object v12, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2701
    const/16 v11, -0x60

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 2703
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 2704
    .local v5, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 2707
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 2709
    .local v6, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    .line 2710
    .local v1, "error":I
    :goto_1
    const/4 v11, 0x2

    if-eq v1, v11, :cond_2

    .line 2725
    if-nez v1, :cond_0

    .line 2728
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v3, v11, :cond_0

    .line 2729
    invoke-virtual {v6, v3}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 2730
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_3

    .line 2731
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_3

    .line 2732
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/4 v12, 0x4

    if-ne v11, v12, :cond_3

    .line 2733
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 2734
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    check-cast v9, Lorg/snmp4j/smi/OctetString;

    .line 2735
    .local v9, "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v10, Ljava/lang/String;

    invoke-virtual {v9}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v11

    const-string v12, "UTF8"

    invoke-direct {v10, v11, v12}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/4 v11, 0x0

    const/16 v12, 0x20

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 2711
    .end local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v3    # "i":I
    .end local v9    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_2
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v11

    invoke-virtual {v4}, Lorg/snmp4j/PDU;->size()I

    move-result v12

    if-gt v11, v12, :cond_0

    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v11

    if-lez v11, :cond_0

    .line 2714
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v2

    .line 2715
    .local v2, "errorIndex":I
    add-int/lit8 v11, v2, -0x1

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->remove(I)V

    .line 2717
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 2718
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 2721
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 2722
    invoke-virtual {v6}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v1

    goto :goto_1

    .line 2728
    .end local v2    # "errorIndex":I
    .restart local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private getValueForKeyInDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "Key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 2558
    const-string v3, "MFPDiscoveryService"

    const-string v4, "getValueForKeyInDeviceID"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2560
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2561
    .local v0, "modePosition":I
    if-ne v0, v5, :cond_1

    .line 2573
    :cond_0
    :goto_0
    return-object v2

    .line 2565
    :cond_1
    const-string v3, ";"

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2566
    .local v1, "semicolonPosition":I
    if-eq v1, v5, :cond_0

    .line 2570
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2571
    .local v2, "value":Ljava/lang/String;
    const-string v3, "^\\s+"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2572
    const-string v3, "\\s+$"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2573
    goto :goto_0
.end method

.method public static isSocketOpen(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0, "ipAddress"    # Ljava/lang/String;
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 424
    const/4 v4, 0x0

    .line 428
    .local v4, "ret":Z
    const/4 v8, 0x0

    .line 429
    .local v8, "url":Ljava/net/URL;
    move-object v3, p0

    .line 430
    .local v3, "ipAdd":Ljava/lang/String;
    if-nez p1, :cond_1

    const/16 v2, 0x238c

    .line 433
    .local v2, "iPort":I
    :goto_0
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    .end local v8    # "url":Ljava/net/URL;
    .local v9, "url":Ljava/net/URL;
    if-eqz v9, :cond_0

    .line 435
    :try_start_1
    invoke-virtual {v9}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 436
    invoke-virtual {v9}, Ljava/net/URL;->getPort()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v2

    .line 437
    const/4 v10, -0x1

    if-ne v2, v10, :cond_0

    .line 438
    const/16 v2, 0x238c

    .line 444
    :cond_0
    const/4 v8, 0x0

    .line 447
    .end local v9    # "url":Ljava/net/URL;
    .restart local v8    # "url":Ljava/net/URL;
    :goto_1
    :try_start_2
    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 448
    .local v0, "addr":Ljava/net/InetAddress;
    new-instance v6, Ljava/net/InetSocketAddress;

    invoke-direct {v6, v0, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 451
    .local v6, "sockaddr":Ljava/net/SocketAddress;
    new-instance v5, Ljava/net/Socket;

    invoke-direct {v5}, Ljava/net/Socket;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 455
    .local v5, "sock":Ljava/net/Socket;
    const/16 v7, 0x3e8

    .line 456
    .local v7, "timeoutMs":I
    :try_start_3
    invoke-virtual {v5, v6, v7}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 457
    const/4 v4, 0x1

    .line 462
    :try_start_4
    invoke-virtual {v5}, Ljava/net/Socket;->close()V

    .line 463
    const/4 v5, 0x0

    .line 464
    const/4 v6, 0x0

    :goto_2
    move v10, v4

    .line 471
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v2    # "iPort":I
    .end local v5    # "sock":Ljava/net/Socket;
    .end local v6    # "sockaddr":Ljava/net/SocketAddress;
    .end local v7    # "timeoutMs":I
    :goto_3
    return v10

    .line 430
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 441
    .restart local v2    # "iPort":I
    :catch_0
    move-exception v10

    .line 444
    :goto_4
    const/4 v8, 0x0

    goto :goto_1

    .line 443
    :catchall_0
    move-exception v10

    .line 444
    :goto_5
    const/4 v8, 0x0

    .line 445
    throw v10
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 469
    .end local v2    # "iPort":I
    :catch_1
    move-exception v1

    .line 470
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 471
    const/4 v10, 0x0

    goto :goto_3

    .line 458
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "addr":Ljava/net/InetAddress;
    .restart local v2    # "iPort":I
    .restart local v5    # "sock":Ljava/net/Socket;
    .restart local v6    # "sockaddr":Ljava/net/SocketAddress;
    .restart local v7    # "timeoutMs":I
    :catch_2
    move-exception v1

    .line 460
    .restart local v1    # "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 462
    :try_start_5
    invoke-virtual {v5}, Ljava/net/Socket;->close()V

    .line 463
    const/4 v5, 0x0

    .line 464
    const/4 v6, 0x0

    goto :goto_2

    .line 461
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v10

    .line 462
    invoke-virtual {v5}, Ljava/net/Socket;->close()V

    .line 463
    const/4 v5, 0x0

    .line 464
    const/4 v6, 0x0

    .line 465
    throw v10
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 443
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v5    # "sock":Ljava/net/Socket;
    .end local v6    # "sockaddr":Ljava/net/SocketAddress;
    .end local v7    # "timeoutMs":I
    .end local v8    # "url":Ljava/net/URL;
    .restart local v9    # "url":Ljava/net/URL;
    :catchall_2
    move-exception v10

    move-object v8, v9

    .end local v9    # "url":Ljava/net/URL;
    .restart local v8    # "url":Ljava/net/URL;
    goto :goto_5

    .line 441
    .end local v8    # "url":Ljava/net/URL;
    .restart local v9    # "url":Ljava/net/URL;
    :catch_3
    move-exception v10

    move-object v8, v9

    .end local v9    # "url":Ljava/net/URL;
    .restart local v8    # "url":Ljava/net/URL;
    goto :goto_4
.end method

.method private isSupportedPrinter(Ljava/lang/String;)Z
    .locals 3
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 2743
    if-eqz p1, :cond_3

    .line 2744
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 2746
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 2747
    :cond_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    const-string v2, "Samsung"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2750
    :cond_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2756
    :cond_3
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 2750
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2752
    .local v0, "vendor":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2753
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public DestroySNMP()V
    .locals 3

    .prologue
    .line 2946
    const-string v1, "MFPDiscoveryService"

    const-string v2, "DestroySNMP"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2947
    monitor-enter p0

    .line 2949
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    if-eqz v1, :cond_0

    .line 2950
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->close()V

    .line 2951
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2947
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2959
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2960
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2961
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2962
    return-void

    .line 2953
    :catch_0
    move-exception v0

    .line 2954
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 2955
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 2947
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public DestroyWFD()V
    .locals 3

    .prologue
    .line 1867
    const-string v1, "MFPDiscoveryService"

    const-string v2, "DestroyWFD"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1869
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 1871
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    if-eqz v1, :cond_0

    .line 1872
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1873
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1879
    :cond_0
    :goto_0
    return-void

    .line 1875
    :catch_0
    move-exception v0

    .line 1876
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public DestroymDNS()V
    .locals 4

    .prologue
    .line 2099
    const-string v2, "MFPDiscoveryService"

    const-string v3, "DestroymDNS"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2101
    monitor-enter p0

    .line 2102
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 2103
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2112
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->executorRow:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 2114
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->executorRow:Ljava/util/concurrent/ExecutorService;

    .line 2116
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2117
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    .line 2118
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2120
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2101
    :cond_0
    monitor-exit p0

    .line 2123
    return-void

    .line 2103
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/jmdns/JmDNS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2105
    .local v1, "jmdns":Ljavax/jmdns/JmDNS;
    :try_start_1
    invoke-virtual {v1}, Ljavax/jmdns/JmDNS;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106
    const/4 v1, 0x0

    goto :goto_0

    .line 2107
    :catch_0
    move-exception v0

    .line 2108
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2101
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "jmdns":Ljavax/jmdns/JmDNS;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public IsCollateSuportedBySNMP(Ljava/lang/String;)I
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1400
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "IsCollateSuportedBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1402
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 1403
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_0

    .line 1404
    const/4 v11, -0x1

    .line 1456
    :goto_0
    return v11

    .line 1407
    :cond_0
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v9

    .line 1408
    .local v9, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v9, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1409
    const/4 v11, 0x1

    invoke-interface {v9, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1411
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1413
    .local v8, "storages":Ljava/util/Set;, "Ljava/util/Set<Lorg/snmp4j/smi/OID;>;"
    new-instance v11, Lorg/snmp4j/smi/OID;

    const-string v12, "1.3.6.1.2.1.25.2.1.4"

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1414
    new-instance v11, Lorg/snmp4j/smi/OID;

    const-string v12, "1.3.6.1.2.1.25.2.1.9"

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1416
    new-instance v3, Lorg/snmp4j/smi/OID;

    const-string v11, "1.3.6.1.2.1.25.2.3.1.2"

    invoke-direct {v3, v11}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 1417
    .local v3, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v3

    .line 1419
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1421
    .local v4, "oids":Ljava/util/List;, "Ljava/util/List<Lorg/snmp4j/smi/OID;>;"
    :cond_1
    invoke-virtual {v1, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1453
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_8

    .line 1454
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-direct {p0, v11, v9, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsConfidentialPrintAvailableBySNMP(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Ljava/util/List;)I

    move-result v11

    goto :goto_0

    .line 1422
    :cond_2
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v5

    .line 1423
    .local v5, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1424
    const/16 v11, -0x5f

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1426
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v5, v9}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v6

    .line 1427
    .local v6, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v6, :cond_3

    .line 1428
    const/4 v11, -0x1

    goto :goto_0

    .line 1430
    :cond_3
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_4

    .line 1431
    const/4 v11, -0x1

    goto :goto_0

    .line 1433
    :cond_4
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_5

    .line 1434
    const/4 v11, -0x1

    goto :goto_0

    .line 1436
    :cond_5
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-nez v11, :cond_6

    .line 1437
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 1439
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v2, v11, :cond_1

    .line 1440
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1441
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_7

    .line 1442
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_7

    .line 1443
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v11

    invoke-virtual {v11, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1444
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v10

    check-cast v10, Lorg/snmp4j/smi/OID;

    .line 1445
    .local v10, "value":Lorg/snmp4j/smi/OID;
    invoke-interface {v8, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1446
    new-instance v11, Lorg/snmp4j/smi/OID;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "1.3.6.1.2.1.25.2.3.1.5."

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v13

    invoke-virtual {v13}, Lorg/snmp4j/smi/OID;->last()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1449
    .end local v10    # "value":Lorg/snmp4j/smi/OID;
    :cond_7
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 1439
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1456
    .end local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v2    # "i":I
    .end local v5    # "pdu":Lorg/snmp4j/PDU;
    .end local v6    # "response":Lorg/snmp4j/event/ResponseEvent;
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method public IsConfidentialPrintAvailableBySNMP(Ljava/lang/String;)I
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 992
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "IsConfidentialPrintAvailableBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 995
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_0

    .line 996
    const/4 v11, -0x1

    .line 1049
    :goto_0
    return v11

    .line 999
    :cond_0
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v9

    .line 1000
    .local v9, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v9, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1001
    const/4 v11, 0x1

    invoke-interface {v9, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1003
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1005
    .local v8, "storages":Ljava/util/Set;, "Ljava/util/Set<Lorg/snmp4j/smi/OID;>;"
    new-instance v11, Lorg/snmp4j/smi/OID;

    const-string v12, "1.3.6.1.2.1.25.2.1.4"

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1006
    new-instance v11, Lorg/snmp4j/smi/OID;

    const-string v12, "1.3.6.1.2.1.25.2.1.8"

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1007
    new-instance v11, Lorg/snmp4j/smi/OID;

    const-string v12, "1.3.6.1.2.1.25.2.1.9"

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1009
    new-instance v3, Lorg/snmp4j/smi/OID;

    const-string v11, "1.3.6.1.2.1.25.2.3.1.2"

    invoke-direct {v3, v11}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 1010
    .local v3, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v3

    .line 1012
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1014
    .local v4, "oids":Ljava/util/List;, "Ljava/util/List<Lorg/snmp4j/smi/OID;>;"
    :cond_1
    invoke-virtual {v1, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1046
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_8

    .line 1047
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-direct {p0, v11, v9, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsConfidentialPrintAvailableBySNMP(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Ljava/util/List;)I

    move-result v11

    goto :goto_0

    .line 1015
    :cond_2
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v5

    .line 1016
    .local v5, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1017
    const/16 v11, -0x5f

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1019
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v5, v9}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v6

    .line 1020
    .local v6, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v6, :cond_3

    .line 1021
    const/4 v11, -0x1

    goto :goto_0

    .line 1023
    :cond_3
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_4

    .line 1024
    const/4 v11, -0x1

    goto :goto_0

    .line 1026
    :cond_4
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_5

    .line 1027
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 1029
    :cond_5
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-nez v11, :cond_6

    .line 1030
    const/4 v11, -0x1

    goto/16 :goto_0

    .line 1032
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v2, v11, :cond_1

    .line 1033
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1034
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_7

    .line 1035
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_7

    .line 1036
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v11

    invoke-virtual {v11, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1037
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v10

    check-cast v10, Lorg/snmp4j/smi/OID;

    .line 1038
    .local v10, "value":Lorg/snmp4j/smi/OID;
    invoke-interface {v8, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1039
    new-instance v11, Lorg/snmp4j/smi/OID;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "1.3.6.1.2.1.25.2.3.1.5."

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v13

    invoke-virtual {v13}, Lorg/snmp4j/smi/OID;->last()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042
    .end local v10    # "value":Lorg/snmp4j/smi/OID;
    :cond_7
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 1032
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1049
    .end local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v2    # "i":I
    .end local v5    # "pdu":Lorg/snmp4j/PDU;
    .end local v6    # "response":Lorg/snmp4j/event/ResponseEvent;
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method public IsPrinterAlive(Ljava/lang/String;)I
    .locals 4
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 476
    const-string v1, "MFPDiscoveryService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IsPrinterAlive : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const/4 v0, 0x0

    .line 490
    .local v0, "ping_result":I
    const-string v1, "9100"

    invoke-static {p1, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSocketOpen(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 492
    :goto_0
    return v0

    .line 490
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public IsPrinterColorModelBySNMP(Ljava/lang/String;)I
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1460
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "IsPrinterColorModelBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v9

    .line 1463
    .local v9, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v9, :cond_0

    .line 1464
    const/4 v11, -0x1

    .line 1513
    :goto_0
    return v11

    .line 1467
    :cond_0
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v9, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v10

    .line 1468
    .local v10, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v10, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1469
    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1471
    new-instance v5, Lorg/snmp4j/smi/OID;

    const-string v11, "1.3.6.1.2.1.43.11.1.1.3"

    invoke-direct {v5, v11}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 1472
    .local v5, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v3, v5

    .line 1474
    .local v3, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1475
    .local v2, "colors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v3, v5}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1508
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_9

    .line 1513
    const/4 v11, 0x0

    goto :goto_0

    .line 1476
    :cond_3
    invoke-interface {v9}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v7

    .line 1477
    .local v7, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v3}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v7, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1478
    const/16 v11, -0x5f

    invoke-virtual {v7, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1480
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v7, v10}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v8

    .line 1481
    .local v8, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v8, :cond_4

    .line 1482
    const/4 v11, -0x1

    goto :goto_0

    .line 1484
    :cond_4
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_5

    .line 1485
    const/4 v11, -0x1

    goto :goto_0

    .line 1487
    :cond_5
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_6

    .line 1488
    const/4 v11, -0x1

    goto :goto_0

    .line 1490
    :cond_6
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-nez v11, :cond_7

    .line 1491
    const/4 v11, 0x0

    goto :goto_0

    .line 1493
    :cond_7
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v4, v11, :cond_1

    .line 1494
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v4}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1495
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_8

    .line 1496
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_8

    .line 1497
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v11

    invoke-virtual {v11, v5}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1498
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "1.3.6.1.2.1.43.12.1.1.4.1."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1499
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v12

    invoke-interface {v12}, Lorg/snmp4j/smi/Variable;->toInt()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1498
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1500
    .local v6, "oid":Ljava/lang/String;
    invoke-interface {v9}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v11

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v6}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v11, v10, v12}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getColorBySNMP(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;Lorg/snmp4j/smi/OID;)Ljava/lang/String;

    move-result-object v1

    .line 1501
    .local v1, "color":Ljava/lang/String;
    if-eqz v1, :cond_8

    .line 1502
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1505
    .end local v1    # "color":Ljava/lang/String;
    .end local v6    # "oid":Ljava/lang/String;
    :cond_8
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v3

    .line 1493
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1508
    .end local v0    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v4    # "i":I
    .end local v7    # "pdu":Lorg/snmp4j/PDU;
    .end local v8    # "response":Lorg/snmp4j/event/ResponseEvent;
    :cond_9
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1509
    .restart local v1    # "color":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    const-string v13, "black"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 1510
    const/4 v11, 0x1

    goto/16 :goto_0
.end method

.method public SetCommunityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "communityName"    # Ljava/lang/String;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    .line 283
    return-void
.end method

.method public StartBroadcastDiscovery()V
    .locals 0

    .prologue
    .line 2922
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSNMPDiscovery()V

    .line 2923
    return-void
.end method

.method public StartDiscovery(Z)V
    .locals 0
    .param p1, "scanners"    # Z

    .prologue
    .line 2024
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartmDNSDiscovery(Z)V

    .line 2025
    return-void
.end method

.method public StartIpRangeDiscovery()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 2965
    const-string v3, "MFPDiscoveryService"

    const-string v4, "SNMP StartIpRangeDiscovery"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2967
    monitor-enter p0

    .line 2968
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    if-nez v3, :cond_1

    .line 2969
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2970
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2971
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2972
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    .line 2973
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v13

    .line 2974
    .local v13, "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v13}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2967
    .end local v13    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_1
    monitor-exit p0

    .line 3006
    return-void

    .line 2975
    .restart local v13    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_2
    invoke-interface {v13}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/net/NetworkInterface;

    .line 2976
    .local v10, "iface":Ljava/net/NetworkInterface;
    invoke-static {v10}, Lcom/sec/print/mobileprint/df/DiscoveryUtil;->isNetworkIntefaceWorking(Ljava/net/NetworkInterface;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2977
    invoke-virtual {v10}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v12

    .line 2978
    .local v12, "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_0
    invoke-interface {v12}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2979
    invoke-interface {v12}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/net/InetAddress;

    .line 2980
    .local v11, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v11}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v7

    .line 2981
    .local v7, "addr":[B
    array-length v3, v7

    new-array v8, v3, [B

    .line 2982
    .local v8, "fromAddr":[B
    array-length v3, v7

    new-array v14, v3, [B

    .line 2983
    .local v14, "toAddr":[B
    const/4 v9, 0x0

    .line 2984
    .local v9, "i":I
    :goto_1
    array-length v3, v7

    add-int/lit8 v3, v3, -0x1

    if-lt v9, v3, :cond_4

    .line 2988
    const/4 v3, 0x0

    aput-byte v3, v8, v9

    .line 2989
    const/4 v3, -0x1

    aput-byte v3, v14, v9

    .line 2990
    invoke-static {v8}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v1

    .line 2991
    .local v1, "from":Ljava/net/InetAddress;
    invoke-static {v14}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v2

    .line 2992
    .local v2, "to":Ljava/net/InetAddress;
    new-instance v0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    const-wide/16 v3, 0x1388

    const/4 v5, 0x5

    const/16 v6, 0x400

    invoke-direct/range {v0 .. v6}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;-><init>(Ljava/net/InetAddress;Ljava/net/InetAddress;JII)V

    .line 2994
    .local v0, "discovery":Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->enableSnmp2(Ljava/lang/String;)V

    .line 2995
    iget-boolean v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    if-eqz v3, :cond_3

    .line 2996
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->enableSnmp3(Ljava/lang/String;)V

    .line 2998
    :cond_3
    invoke-virtual {v0, p0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    .line 2999
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->start()V

    .line 3000
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2967
    .end local v0    # "discovery":Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
    .end local v1    # "from":Ljava/net/InetAddress;
    .end local v2    # "to":Ljava/net/InetAddress;
    .end local v7    # "addr":[B
    .end local v8    # "fromAddr":[B
    .end local v9    # "i":I
    .end local v10    # "iface":Ljava/net/NetworkInterface;
    .end local v11    # "inetAddress":Ljava/net/InetAddress;
    .end local v12    # "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v13    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v14    # "toAddr":[B
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 2985
    .restart local v7    # "addr":[B
    .restart local v8    # "fromAddr":[B
    .restart local v9    # "i":I
    .restart local v10    # "iface":Ljava/net/NetworkInterface;
    .restart local v11    # "inetAddress":Ljava/net/InetAddress;
    .restart local v12    # "inetAddresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .restart local v13    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v14    # "toAddr":[B
    :cond_4
    :try_start_1
    aget-byte v3, v7, v9

    aput-byte v3, v8, v9

    .line 2986
    aget-byte v3, v7, v9

    aput-byte v3, v14, v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2984
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public StartSNMPDiscovery()V
    .locals 4

    .prologue
    .line 2882
    const-string v1, "MFPDiscoveryService"

    const-string v2, "StartSNMPDiscovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2884
    monitor-enter p0

    .line 2885
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 2887
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2893
    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_1

    .line 2895
    :try_start_3
    new-instance v1, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    const/4 v2, 0x5

    const/16 v3, 0x400

    invoke-direct {v1, v2, v3}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;-><init>(II)V

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    .line 2896
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp2(Ljava/lang/String;)V

    .line 2897
    iget-boolean v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    if-eqz v1, :cond_1

    .line 2898
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp3(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2906
    :cond_1
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2907
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2908
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2910
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    if-eqz v1, :cond_2

    .line 2911
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    invoke-virtual {v1, p0}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    .line 2912
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->start()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2884
    :cond_2
    :goto_2
    :try_start_5
    monitor-exit p0

    .line 2918
    return-void

    .line 2888
    :catch_0
    move-exception v0

    .line 2889
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2884
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    .line 2900
    :catch_1
    move-exception v0

    .line 2901
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2914
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 2915
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method public StartSnmp()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1804
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    if-nez v2, :cond_0

    .line 1805
    const-string v2, "MFPDiscoveryService"

    const-string v3, "StartSnmp"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1807
    :try_start_0
    new-instance v1, Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-direct {v1}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;-><init>()V

    .line 1808
    .local v1, "transport":Lorg/snmp4j/transport/DefaultUdpTransportMapping;
    new-instance v2, Lorg/snmp4j/Snmp;

    invoke-direct {v2, v1}, Lorg/snmp4j/Snmp;-><init>(Lorg/snmp4j/TransportMapping;)V

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    .line 1809
    invoke-virtual {v1}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->listen()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1814
    .end local v1    # "transport":Lorg/snmp4j/transport/DefaultUdpTransportMapping;
    :cond_0
    :goto_0
    return-void

    .line 1810
    :catch_0
    move-exception v0

    .line 1811
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public StartWFD()V
    .locals 3

    .prologue
    .line 1817
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 1818
    monitor-enter p0

    .line 1819
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    if-nez v1, :cond_0

    .line 1820
    const-string v1, "MFPDiscoveryService"

    const-string v2, "StartWFD"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1822
    :try_start_1
    new-instance v1, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    invoke-direct {v1, p0}, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;-><init>(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;)V

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1818
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    .line 1829
    :cond_1
    return-void

    .line 1823
    :catch_0
    move-exception v0

    .line 1824
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1818
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public StartWFDDiscovery()V
    .locals 3

    .prologue
    .line 1832
    const-string v1, "MFPDiscoveryService"

    const-string v2, "StartWFDDiscovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1834
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 1835
    monitor-enter p0

    .line 1837
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    if-nez v1, :cond_0

    .line 1838
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartWFD()V

    .line 1841
    :cond_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1842
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1835
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 1848
    :cond_1
    return-void

    .line 1843
    :catch_0
    move-exception v0

    .line 1844
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1835
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public StartmDNS()V
    .locals 11

    .prologue
    .line 1882
    monitor-enter p0

    .line 1883
    :try_start_0
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    if-nez v8, :cond_1

    .line 1884
    const-string v8, "MFPDiscoveryService"

    const-string v9, "StartmDNS"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1886
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    .line 1888
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 1891
    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->executorRow:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1895
    :try_start_1
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    .line 1896
    .local v1, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-nez v8, :cond_2

    .line 1882
    .end local v1    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1939
    return-void

    .line 1897
    .restart local v1    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_2
    :try_start_3
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 1898
    .local v4, "interf":Ljava/net/NetworkInterface;
    invoke-static {v4}, Lcom/sec/print/mobileprint/df/DiscoveryUtil;->isNetworkIntefaceWorking(Ljava/net/NetworkInterface;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "pdp"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1899
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v5

    .line 1900
    .local v5, "ips":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    if-eqz v8, :cond_0

    .line 1902
    :try_start_4
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 1904
    .local v3, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/http/conn/util/InetAddressUtils;->isIPv4Address(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1906
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    .line 1907
    .local v2, "hostname":Ljava/lang/String;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Create mDNS: ip= "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", hostname= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    invoke-static {v3, v2}, Ljavax/jmdns/JmDNS;->create(Ljava/net/InetAddress;Ljava/lang/String;)Ljavax/jmdns/JmDNS;

    move-result-object v6

    .line 1912
    .local v6, "jmdns":Ljavax/jmdns/JmDNS;
    new-instance v7, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    iget-object v9, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->executorRow:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v7, p0, v6, v8, v9}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;-><init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljavax/jmdns/JmDNS;Ljava/util/Map;Ljava/util/concurrent/ExecutorService;)V

    .line 1913
    .local v7, "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    iget v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    .line 1915
    const-string v8, "_scanner._tcp.local."

    invoke-virtual {v6, v8, v7}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 1916
    const-string v8, "_uscan._tcp.local."

    invoke-virtual {v6, v8, v7}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 1923
    :goto_2
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1924
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1927
    .end local v2    # "hostname":Ljava/lang/String;
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v6    # "jmdns":Ljavax/jmdns/JmDNS;
    .end local v7    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :catch_0
    move-exception v0

    .line 1928
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1934
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v4    # "interf":Ljava/net/NetworkInterface;
    .end local v5    # "ips":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :catch_1
    move-exception v0

    .line 1935
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1882
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v8

    .line 1919
    .restart local v1    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v2    # "hostname":Ljava/lang/String;
    .restart local v3    # "inetAddress":Ljava/net/InetAddress;
    .restart local v4    # "interf":Ljava/net/NetworkInterface;
    .restart local v5    # "ips":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .restart local v6    # "jmdns":Ljavax/jmdns/JmDNS;
    .restart local v7    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :cond_4
    :try_start_7
    const-string v8, "_pdl-datastream._tcp.local."

    invoke-virtual {v6, v8, v7}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 1920
    const-string v8, "_uscan._tcp.local."

    invoke-virtual {v6, v8, v7}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method

.method public StartmDNSDiscovery(Z)V
    .locals 7
    .param p1, "scanners"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1983
    const-string v3, "MFPDiscoveryService"

    const-string v5, "StartmDNSDiscovery"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1985
    if-eqz p1, :cond_1

    move v3, v4

    :goto_0
    iput v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    .line 1987
    monitor-enter p0

    .line 1989
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    if-nez v3, :cond_2

    .line 1990
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartmDNS()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987
    :cond_0
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2020
    return-void

    .line 1985
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1992
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 1994
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/jmdns/JmDNS;

    .line 1995
    .local v1, "jmdns":Ljavax/jmdns/JmDNS;
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    if-ltz v5, :cond_3

    .line 1996
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    .line 1997
    .local v2, "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    if-eqz v2, :cond_3

    .line 1999
    const-string v5, "_scanner._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2000
    const-string v5, "_uscan._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2001
    const-string v5, "_pdl-datastream._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2004
    iget v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    if-ne v5, v4, :cond_4

    .line 2005
    const-string v5, "_scanner._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2006
    const-string v5, "_uscan._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2016
    .end local v1    # "jmdns":Ljavax/jmdns/JmDNS;
    .end local v2    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :catch_0
    move-exception v0

    .line 2017
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1987
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 2008
    .restart local v1    # "jmdns":Ljavax/jmdns/JmDNS;
    .restart local v2    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :cond_4
    :try_start_4
    const-string v5, "_pdl-datastream._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2009
    const-string v5, "_uscan._tcp.local."

    invoke-virtual {v1, v5, v2}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public StopBroadcastDiscovery()V
    .locals 0

    .prologue
    .line 2942
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPDiscovery()V

    .line 2943
    return-void
.end method

.method public StopDiscovery()V
    .locals 0

    .prologue
    .line 2095
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopmDNSDiscovery()V

    .line 2096
    return-void
.end method

.method public StopIpRangeDiscovery()V
    .locals 0

    .prologue
    .line 3032
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPIPRangeDiscovery()V

    .line 3033
    return-void
.end method

.method public StopSNMPDiscovery()V
    .locals 3

    .prologue
    .line 2926
    const-string v1, "MFPDiscoveryService"

    const-string v2, "StopSNMPDiscovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2928
    monitor-enter p0

    .line 2930
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    if-eqz v1, :cond_0

    .line 2931
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    invoke-virtual {v1, p0}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->removeDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    .line 2932
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryInstance:Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2928
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 2939
    return-void

    .line 2934
    :catch_0
    move-exception v0

    .line 2936
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2928
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public StopSNMPIPRangeDiscovery()V
    .locals 4

    .prologue
    .line 3009
    const-string v2, "MFPDiscoveryService"

    const-string v3, "StopSNMPIPRangeDiscovery"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3011
    monitor-enter p0

    .line 3013
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 3014
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3018
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryIpRangeInstance:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3011
    :cond_0
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3029
    return-void

    .line 3014
    :cond_1
    :try_start_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    .line 3015
    .local v0, "d":Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->stop()V

    .line 3016
    invoke-virtual {v0, p0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->removeDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3024
    .end local v0    # "d":Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
    :catch_0
    move-exception v1

    .line 3025
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 3026
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 3011
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method public StopWFDDiscovery()V
    .locals 3

    .prologue
    .line 1851
    const-string v1, "MFPDiscoveryService"

    const-string v2, "StopWFDDiscovery"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1853
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 1854
    monitor-enter p0

    .line 1856
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    if-eqz v1, :cond_0

    .line 1857
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1854
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 1864
    :cond_1
    return-void

    .line 1859
    :catch_0
    move-exception v0

    .line 1860
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1854
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public StopmDNSDiscovery()V
    .locals 6

    .prologue
    .line 2064
    const-string v3, "MFPDiscoveryService"

    const-string v4, "StopmDNSDiscovery"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2066
    monitor-enter p0

    .line 2067
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 2068
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2066
    :cond_1
    monitor-exit p0

    .line 2092
    return-void

    .line 2068
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/jmdns/JmDNS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2070
    .local v1, "jmdns":Ljavax/jmdns/JmDNS;
    :try_start_1
    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 2071
    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->listenerList:Ljava/util/List;

    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->jmdnsList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;

    .line 2072
    .local v2, "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    if-eqz v2, :cond_0

    .line 2073
    iget v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 2074
    const-string v4, "_scanner._tcp.local."

    invoke-virtual {v1, v4, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2075
    const-string v4, "_uscan._tcp.local."

    invoke-virtual {v1, v4, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2084
    .end local v2    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :catch_0
    move-exception v0

    .line 2085
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2066
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "jmdns":Ljavax/jmdns/JmDNS;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 2078
    .restart local v1    # "jmdns":Ljavax/jmdns/JmDNS;
    .restart local v2    # "listener":Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
    :cond_3
    :try_start_3
    const-string v4, "_pdl-datastream._tcp.local."

    invoke-virtual {v1, v4, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 2079
    const-string v4, "_uscan._tcp.local."

    invoke-virtual {v1, v4, v2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public getDeviceModeBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v9, 0x0

    .line 1053
    const-string v8, "MFPDiscoveryService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "getDeviceModeBySNMP : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v5

    .line 1056
    .local v5, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v5, :cond_1

    move-object v7, v9

    .line 1095
    :cond_0
    :goto_0
    return-object v7

    .line 1060
    :cond_1
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v5, v8}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v6

    .line 1061
    .local v6, "target":Lorg/snmp4j/Target;
    const-wide/16 v10, 0x1f4

    invoke-interface {v6, v10, v11}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1062
    const/4 v8, 0x1

    invoke-interface {v6, v8}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1064
    invoke-interface {v5}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v3

    .line 1065
    .local v3, "pdu":Lorg/snmp4j/PDU;
    new-instance v8, Lorg/snmp4j/smi/VariableBinding;

    sget-object v10, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-direct {v8, v10}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v3, v8}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1066
    const/16 v8, -0x60

    invoke-virtual {v3, v8}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1068
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v8, v3, v6}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v4

    .line 1069
    .local v4, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v4, :cond_2

    move-object v7, v9

    .line 1070
    goto :goto_0

    .line 1072
    :cond_2
    invoke-virtual {v4}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    if-nez v8, :cond_3

    move-object v7, v9

    .line 1073
    goto :goto_0

    .line 1075
    :cond_3
    invoke-virtual {v4}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v8

    if-eqz v8, :cond_4

    move-object v7, v9

    .line 1076
    goto :goto_0

    .line 1078
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v4}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/PDU;->size()I

    move-result v8

    if-lt v1, v8, :cond_5

    move-object v7, v9

    .line 1095
    goto :goto_0

    .line 1079
    :cond_5
    invoke-virtual {v4}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8, v1}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1080
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v10, 0x81

    if-eq v8, v10, :cond_7

    .line 1081
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v10, 0x80

    if-eq v8, v10, :cond_7

    .line 1082
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/4 v10, 0x4

    if-ne v8, v10, :cond_7

    .line 1083
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 1084
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v8

    check-cast v8, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v8}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1085
    .local v7, "value":Ljava/lang/String;
    const-string v8, "MODE:"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1086
    .local v2, "index":I
    if-ne v2, v12, :cond_6

    move-object v7, v9

    .line 1087
    goto/16 :goto_0

    .line 1088
    :cond_6
    add-int/lit8 v8, v2, 0x5

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1089
    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1090
    if-eq v2, v12, :cond_0

    .line 1091
    const/4 v8, 0x0

    invoke-virtual {v7, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1078
    .end local v2    # "index":I
    .end local v7    # "value":Ljava/lang/String;
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;
    .locals 30
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2287
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : host = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", version = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    if-nez v27, :cond_0

    .line 2290
    invoke-virtual/range {p0 .. p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 2293
    :cond_0
    const/16 v20, 0x0

    .line 2304
    .local v20, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v20

    .line 2305
    if-nez v20, :cond_1

    .line 2306
    const/16 v17, 0x0

    .line 2526
    :goto_0
    return-object v17

    .line 2309
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v21

    .line 2310
    .local v21, "target":Lorg/snmp4j/Target;
    const-wide/16 v27, 0x1f4

    move-object/from16 v0, v21

    move-wide/from16 v1, v27

    invoke-interface {v0, v1, v2}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 2311
    const/16 v27, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-interface {v0, v1}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 2313
    invoke-interface/range {v20 .. v20}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v14

    .line 2316
    .local v14, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    sget-object v28, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-direct/range {v27 .. v28}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2317
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    sget-object v28, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid2:Lorg/snmp4j/smi/OID;

    invoke-direct/range {v27 .. v28}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2319
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    sget-object v28, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungHostNameOid:Lorg/snmp4j/smi/OID;

    invoke-direct/range {v27 .. v28}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2320
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    sget-object v28, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungLocationOid:Lorg/snmp4j/smi/OID;

    invoke-direct/range {v27 .. v28}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2321
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    sget-object v28, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

    invoke-direct/range {v27 .. v28}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2324
    const/16 v27, -0x60

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->setType(I)V

    .line 2326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v15

    .line 2327
    .local v15, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v27

    if-nez v27, :cond_3

    .line 2328
    :cond_2
    const-string v27, "MFPDiscoveryService"

    const-string v28, "getDeviceProperties : response is null"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2332
    :cond_3
    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v16

    .line 2334
    .local v16, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v4

    .line 2335
    .local v4, "error":I
    :goto_1
    const/16 v27, 0x2

    move/from16 v0, v27

    if-eq v4, v0, :cond_4

    .line 2351
    if-eqz v4, :cond_9

    .line 2352
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2336
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v27

    invoke-virtual {v14}, Lorg/snmp4j/PDU;->size()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-gt v0, v1, :cond_5

    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v27

    if-gtz v27, :cond_6

    .line 2337
    :cond_5
    const-string v27, "MFPDiscoveryService"

    const-string v28, "getDeviceProperties : error"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2338
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2340
    :cond_6
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v5

    .line 2341
    .local v5, "errorIndex":I
    add-int/lit8 v27, v5, -0x1

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->remove(I)V

    .line 2343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v15

    .line 2344
    if-eqz v15, :cond_7

    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v27

    if-nez v27, :cond_8

    .line 2345
    :cond_7
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2347
    :cond_8
    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v16

    .line 2348
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v4

    goto :goto_1

    .line 2354
    .end local v5    # "errorIndex":I
    :cond_9
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 2355
    .local v26, "variables":Ljava/util/Map;, "Ljava/util/Map<Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->size()I

    move-result v27

    move/from16 v0, v27

    if-lt v8, v0, :cond_b

    .line 2363
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getModelName(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v12

    .line 2364
    .local v12, "modelName":Ljava/lang/String;
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : getModelName = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2366
    if-eqz v12, :cond_a

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v27

    if-nez v27, :cond_19

    .line 2367
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinterIndexBySNMP(Lcom/sec/print/mobileprint/df/SnmpSessionBase;Lorg/snmp4j/Target;)Ljava/lang/String;

    move-result-object v9

    .line 2368
    .local v9, "index":Ljava/lang/String;
    if-nez v9, :cond_d

    .line 2369
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2356
    .end local v9    # "index":Ljava/lang/String;
    .end local v12    # "modelName":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v3

    .line 2357
    .local v3, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v27

    const/16 v28, 0x81

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_c

    .line 2358
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v27

    const/16 v28, 0x80

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_c

    .line 2359
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v27

    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v28

    invoke-interface/range {v26 .. v28}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355
    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 2371
    .end local v3    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .restart local v9    # "index":Ljava/lang/String;
    .restart local v12    # "modelName":Ljava/lang/String;
    :cond_d
    new-instance v23, Lorg/snmp4j/smi/OID;

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "1.3.6.1.2.1.25.3.2.1.3"

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 2373
    .local v23, "valueOid":Lorg/snmp4j/smi/OID;
    invoke-interface/range {v20 .. v20}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v14

    .line 2374
    new-instance v27, Lorg/snmp4j/smi/VariableBinding;

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2376
    const/16 v27, -0x60

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->setType(I)V

    .line 2378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v15

    .line 2379
    if-eqz v15, :cond_e

    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v27

    if-nez v27, :cond_f

    .line 2380
    :cond_e
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2382
    :cond_f
    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v16

    .line 2384
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v4

    .line 2385
    :goto_3
    const/16 v27, 0x2

    move/from16 v0, v27

    if-eq v4, v0, :cond_10

    .line 2400
    if-eqz v4, :cond_15

    .line 2401
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2386
    :cond_10
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v27

    invoke-virtual {v14}, Lorg/snmp4j/PDU;->size()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-gt v0, v1, :cond_11

    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v27

    if-gtz v27, :cond_12

    .line 2387
    :cond_11
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2389
    :cond_12
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v5

    .line 2390
    .restart local v5    # "errorIndex":I
    add-int/lit8 v27, v5, -0x1

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/snmp4j/PDU;->remove(I)V

    .line 2392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v14, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v15

    .line 2393
    if-eqz v15, :cond_13

    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v27

    if-nez v27, :cond_14

    .line 2394
    :cond_13
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2396
    :cond_14
    invoke-virtual {v15}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v16

    .line 2397
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v4

    goto :goto_3

    .line 2402
    .end local v5    # "errorIndex":I
    :cond_15
    const/4 v8, 0x0

    :goto_4
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/PDU;->size()I

    move-result v27

    move/from16 v0, v27

    if-lt v8, v0, :cond_17

    .line 2413
    :goto_5
    if-eqz v12, :cond_16

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v27

    if-nez v27, :cond_19

    .line 2414
    :cond_16
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2403
    :cond_17
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v3

    .line 2404
    .restart local v3    # "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v27

    const/16 v28, 0x81

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_18

    .line 2405
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v27

    const/16 v28, 0x80

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_18

    .line 2406
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_18

    .line 2407
    invoke-virtual {v3}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v25

    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2408
    .local v25, "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v12

    .line 2409
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : modelName = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 2402
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_18
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 2417
    .end local v3    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v9    # "index":Ljava/lang/String;
    .end local v23    # "valueOid":Lorg/snmp4j/smi/OID;
    :cond_19
    const/4 v7, 0x0

    .line 2418
    .local v7, "hostName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2419
    .local v10, "location":Ljava/lang/String;
    const/16 v19, 0x0

    .line 2421
    .local v19, "serialNumber":Ljava/lang/String;
    const/16 v18, 0x0

    .line 2422
    .local v18, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    const/4 v6, 0x0

    .line 2424
    .local v6, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungHostNameOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 2425
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungHostNameOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/snmp4j/smi/Variable;

    .line 2426
    .local v24, "var":Lorg/snmp4j/smi/Variable;
    invoke-interface/range {v24 .. v24}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1a

    move-object/from16 v25, v24

    .line 2427
    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2428
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    .line 2429
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : hostName = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2433
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_1a
    if-eqz v7, :cond_1b

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v27

    if-nez v27, :cond_1c

    .line 2434
    :cond_1b
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 2436
    :cond_1c
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungLocationOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1d

    .line 2437
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungLocationOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/snmp4j/smi/Variable;

    .line 2438
    .restart local v24    # "var":Lorg/snmp4j/smi/Variable;
    invoke-interface/range {v24 .. v24}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1d

    move-object/from16 v25, v24

    .line 2439
    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2440
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    .line 2441
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : location = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2445
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_1d
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 2446
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungSerialNumberOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/snmp4j/smi/Variable;

    .line 2447
    .restart local v24    # "var":Lorg/snmp4j/smi/Variable;
    invoke-interface/range {v24 .. v24}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1e

    move-object/from16 v25, v24

    .line 2448
    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2449
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v19

    .line 2450
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : serialNumber = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2454
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_1e
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 2455
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/snmp4j/smi/Variable;

    .line 2456
    .restart local v24    # "var":Lorg/snmp4j/smi/Variable;
    invoke-interface/range {v24 .. v24}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1f

    move-object/from16 v25, v24

    .line 2457
    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2458
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v22

    .line 2459
    .local v22, "value":Ljava/lang/String;
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "getDeviceProperties : device id = "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2461
    const-string v27, "MODE:"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 2462
    .local v9, "index":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_1f

    .line 2463
    add-int/lit8 v27, v9, 0x5

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    .line 2464
    const-string v27, ";"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 2465
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_1f

    .line 2466
    const/16 v27, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 2467
    const-string v27, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 2468
    .local v13, "modes":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v28, v0

    const/16 v27, 0x0

    :goto_6
    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_24

    .line 2485
    .end local v9    # "index":I
    .end local v13    # "modes":[Ljava/lang/String;
    .end local v22    # "value":Ljava/lang/String;
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_1f
    :goto_7
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_20

    .line 2486
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface/range {v26 .. v27}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/snmp4j/smi/Variable;

    .line 2487
    .restart local v24    # "var":Lorg/snmp4j/smi/Variable;
    invoke-interface/range {v24 .. v24}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v27

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_20

    move-object/from16 v25, v24

    .line 2488
    check-cast v25, Lorg/snmp4j/smi/OctetString;

    .line 2489
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v27, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v28

    const-string v29, "UTF8"

    invoke-direct/range {v27 .. v29}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v28, 0x0

    const/16 v29, 0x20

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v22

    .line 2490
    .restart local v22    # "value":Ljava/lang/String;
    const-string v27, "MODE:"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 2491
    .restart local v9    # "index":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_20

    .line 2492
    add-int/lit8 v27, v9, 0x5

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    .line 2493
    const-string v27, ";"

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 2494
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_20

    .line 2495
    const/16 v27, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 2496
    const-string v27, ","

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 2497
    .restart local v13    # "modes":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v28, v0

    const/16 v27, 0x0

    :goto_8
    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_28

    .line 2509
    .end local v9    # "index":I
    .end local v13    # "modes":[Ljava/lang/String;
    .end local v22    # "value":Ljava/lang/String;
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_20
    :goto_9
    if-nez v18, :cond_22

    .line 2510
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    move-object/from16 v0, v27

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_21

    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;

    move-object/from16 v0, v27

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2a

    :cond_21
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 2517
    :cond_22
    :goto_a
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 2518
    .local v17, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v27, "modelName"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2519
    const-string v27, "hostName"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520
    const-string v28, "location"

    if-nez v10, :cond_2b

    const-string v27, ""

    :goto_b
    move-object/from16 v0, v17

    move-object/from16 v1, v28

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2521
    const-string v27, "serialNumber"

    if-nez v19, :cond_23

    const-string v10, ""

    .end local v10    # "location":Ljava/lang/String;
    :cond_23
    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2523
    const-string v27, "isScan"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524
    const-string v27, "isFax"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2468
    .end local v17    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v9    # "index":I
    .restart local v10    # "location":Ljava/lang/String;
    .restart local v13    # "modes":[Ljava/lang/String;
    .restart local v22    # "value":Ljava/lang/String;
    .restart local v24    # "var":Lorg/snmp4j/smi/Variable;
    .restart local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_24
    aget-object v11, v13, v27

    .line 2469
    .local v11, "mode":Ljava/lang/String;
    const-string v29, "FAX"

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_25

    .line 2470
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2471
    goto/16 :goto_7

    .line 2472
    :cond_25
    const-string v29, "FAX2"

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_26

    .line 2473
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX2:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2474
    goto/16 :goto_7

    .line 2475
    :cond_26
    const-string v29, "FAX3"

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_27

    .line 2476
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX3:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2477
    goto/16 :goto_7

    .line 2468
    :cond_27
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_6

    .line 2497
    .end local v11    # "mode":Ljava/lang/String;
    :cond_28
    aget-object v11, v13, v27

    .line 2498
    .restart local v11    # "mode":Ljava/lang/String;
    const-string v29, "SCN"

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_29

    .line 2500
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 2501
    goto/16 :goto_9

    .line 2497
    :cond_29
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_8

    .line 2510
    .end local v9    # "index":I
    .end local v11    # "mode":Ljava/lang/String;
    .end local v13    # "modes":[Ljava/lang/String;
    .end local v22    # "value":Ljava/lang/String;
    .end local v24    # "var":Lorg/snmp4j/smi/Variable;
    .end local v25    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_2a
    const/16 v18, 0x0

    goto/16 :goto_a

    .restart local v17    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_2b
    move-object/from16 v27, v10

    .line 2520
    goto :goto_b
.end method

.method public getDevicePropertiesByUSB(II)Ljava/util/Map;
    .locals 10
    .param p1, "vid"    # I
    .param p2, "pid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2577
    const-string v8, "MFPDiscoveryService"

    const-string v9, "getDevicePropertiesByUSB"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2579
    invoke-direct {p0, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceIDByUSB(II)Ljava/lang/String;

    move-result-object v0

    .line 2580
    .local v0, "deviceId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2627
    :goto_0
    return-object v7

    .line 2586
    :cond_0
    const/4 v6, 0x0

    .line 2587
    .local v6, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    const/4 v1, 0x0

    .line 2589
    .local v1, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    const-string v8, "MFG:"

    invoke-direct {p0, v0, v8}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getValueForKeyInDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2590
    .local v2, "mfg":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 2591
    const-string v2, ""

    .line 2594
    :cond_1
    const-string v8, "MDL:"

    invoke-direct {p0, v0, v8}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getValueForKeyInDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2595
    .local v4, "modelName":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 2596
    const-string v4, ""

    .line 2599
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2601
    const-string v8, "MODE:"

    invoke-direct {p0, v0, v8}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getValueForKeyInDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2603
    .local v3, "mode":Ljava/lang/String;
    if-eqz v3, :cond_4

    const-string v8, "SCN"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2605
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 2611
    :goto_1
    if-eqz v3, :cond_3

    .line 2612
    const-string v7, "FAX3"

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2613
    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX3:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2621
    :cond_3
    :goto_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2622
    .local v5, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v7, "modelName"

    invoke-interface {v5, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624
    const-string v7, "isScan"

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2625
    const-string v7, "isFax"

    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v5

    .line 2627
    goto :goto_0

    .line 2608
    .end local v5    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_4
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;

    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsUSB:Ljava/util/Set;

    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    :cond_5
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    :goto_3
    goto :goto_1

    :cond_6
    move-object v6, v7

    goto :goto_3

    .line 2614
    :cond_7
    const-string v7, "FAX2"

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2615
    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX2:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2616
    goto :goto_2

    :cond_8
    const-string v7, "FAX"

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2617
    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    goto :goto_2
.end method

.method public getDuplexBySNMP(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1263
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "getDuplexBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v9

    .line 1266
    .local v9, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v9, :cond_1

    .line 1267
    const/4 v8, 0x0

    .line 1316
    :cond_0
    :goto_0
    return-object v8

    .line 1270
    :cond_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v9, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v10

    .line 1271
    .local v10, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v10, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1272
    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1274
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 1275
    .local v5, "mediaPathTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v11, 0x1

    const-string v12, "other"

    invoke-virtual {v5, v11, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1276
    const/4 v11, 0x2

    const-string v12, "unknown"

    invoke-virtual {v5, v11, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1277
    const/4 v11, 0x3

    const-string v12, "longEdgeBindingDuplex"

    invoke-virtual {v5, v11, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1278
    const/4 v11, 0x4

    const-string v12, "shortEdgeBindingDuplex"

    invoke-virtual {v5, v11, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1279
    const/4 v11, 0x5

    const-string v12, "simplex"

    invoke-virtual {v5, v11, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1281
    sget-object v4, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->prtMediaPathTypeOid:Lorg/snmp4j/smi/OID;

    .line 1282
    .local v4, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v4

    .line 1284
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1285
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v1, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1286
    invoke-interface {v9}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 1287
    .local v6, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v6, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1288
    const/16 v11, -0x5f

    invoke-virtual {v6, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1290
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v6, v10}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v7

    .line 1291
    .local v7, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v7, :cond_3

    .line 1292
    const/4 v8, 0x0

    goto :goto_0

    .line 1294
    :cond_3
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_4

    .line 1295
    const/4 v8, 0x0

    goto :goto_0

    .line 1297
    :cond_4
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_5

    .line 1298
    const/4 v8, 0x0

    goto :goto_0

    .line 1300
    :cond_5
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-nez v11, :cond_6

    .line 1301
    const/4 v8, 0x0

    goto :goto_0

    .line 1303
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v2, v11, :cond_2

    .line 1304
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1305
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_7

    .line 1306
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_7

    .line 1307
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_7

    .line 1308
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    if-eqz v11, :cond_7

    .line 1309
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v11

    invoke-virtual {v11, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1310
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v3

    check-cast v3, Lorg/snmp4j/smi/Integer32;

    .line 1311
    .local v3, "intValue":Lorg/snmp4j/smi/Integer32;
    invoke-virtual {v3}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v11

    invoke-virtual {v5, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1313
    .end local v3    # "intValue":Lorg/snmp4j/smi/Integer32;
    :cond_7
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 1303
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getFaxes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3092
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getFixedModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 221
    const-string v0, ""

    .line 222
    .local v0, "fixedModelName":Ljava/lang/String;
    const-string v1, "Samsung"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 223
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 228
    :goto_0
    const-string v1, "  "

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    const-string v1, "series"

    const-string v2, "Series"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 230
    const-string v1, "Series"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    const-string v1, "Samsung ML-2251NP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    const-string v1, "Samsung ML-3560"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    const-string v1, "Samsung ML-4050 DMV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " Series"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 240
    :cond_0
    const-string v1, "MFPDiscoveryService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fixed model name = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    return-object v0

    .line 225
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Samsung "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPByMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1099
    const-string v5, "MFPDiscoveryService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getIPByMacAddress : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 1103
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    if-nez v5, :cond_0

    .line 1104
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 1107
    :cond_0
    const-string v5, "MFPDiscoveryService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "searching for MacAddress"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    new-instance v1, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    const/4 v5, 0x5

    const/16 v6, 0x400

    invoke-direct {v1, v5, v6}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;-><init>(II)V

    .line 1111
    .local v1, "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp2(Ljava/lang/String;)V

    .line 1112
    iget-boolean v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    if-eqz v5, :cond_1

    .line 1113
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp3(Ljava/lang/String;)V

    .line 1115
    :cond_1
    new-instance v5, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;

    invoke-direct {v5, p0, p1, v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;-><init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    .line 1135
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1139
    const/16 v3, 0x14

    .local v3, "i":I
    move v4, v3

    .line 1140
    .end local v3    # "i":I
    .local v4, "i":I
    :goto_0
    add-int/lit8 v3, v4, -0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    if-gtz v4, :cond_3

    .line 1146
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->stop()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1151
    :goto_1
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1152
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1157
    .end local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .end local v3    # "i":I
    :goto_2
    return-object v5

    .line 1141
    .restart local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .restart local v3    # "i":I
    :cond_3
    const-wide/16 v5, 0x1f4

    :try_start_3
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    .line 1142
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v5

    if-eqz v5, :cond_2

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_0

    .line 1148
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 1149
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 1154
    .end local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "i":I
    :catch_1
    move-exception v2

    .line 1155
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 1157
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public getJobTableBySNMP(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 496
    const-string v10, "MFPDiscoveryService"

    const-string v12, "getJobTableBySNMP"

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 499
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_1

    move-object v6, v11

    .line 545
    :cond_0
    :goto_0
    return-object v6

    .line 503
    :cond_1
    iget-object v10, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v10}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v8

    .line 504
    .local v8, "target":Lorg/snmp4j/Target;
    const-wide/16 v12, 0x1f4

    invoke-interface {v8, v12, v13}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 505
    const/4 v10, 0x1

    invoke-interface {v8, v10}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 507
    new-instance v3, Lorg/snmp4j/smi/OID;

    const-string v10, "1.3.6.1.4.1.236.11.5.1.12.14"

    invoke-direct {v3, v10}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 508
    .local v3, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v3

    .line 510
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v1, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 513
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v4

    .line 514
    .local v4, "pdu":Lorg/snmp4j/PDU;
    new-instance v10, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v10, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v4, v10}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 515
    const/16 v10, -0x5f

    invoke-virtual {v4, v10}, Lorg/snmp4j/PDU;->setType(I)V

    .line 517
    iget-object v10, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v10, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 518
    .local v5, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v5, :cond_3

    move-object v6, v11

    .line 519
    goto :goto_0

    .line 521
    :cond_3
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    if-nez v10, :cond_4

    move-object v6, v11

    .line 522
    goto :goto_0

    .line 524
    :cond_4
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v10

    if-eqz v10, :cond_5

    move-object v6, v11

    .line 525
    goto :goto_0

    .line 527
    :cond_5
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->size()I

    move-result v10

    if-nez v10, :cond_6

    move-object v6, v11

    .line 528
    goto :goto_0

    .line 530
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->size()I

    move-result v10

    if-ge v2, v10, :cond_2

    .line 531
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 532
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/16 v12, 0x81

    if-eq v10, v12, :cond_7

    .line 533
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/16 v12, 0x80

    if-eq v10, v12, :cond_7

    .line 534
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v10

    invoke-virtual {v10, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 535
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/4 v12, 0x4

    if-ne v10, v12, :cond_7

    .line 536
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v10

    if-eqz v10, :cond_7

    .line 537
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v10

    check-cast v10, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v10}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v9

    .line 538
    .local v9, "value":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_7

    .line 539
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542
    .end local v9    # "value":Ljava/lang/String;
    :cond_7
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 530
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getLanguageBySNMP(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1320
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "getLanguageBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1322
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v8

    .line 1323
    .local v8, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v8, :cond_1

    .line 1324
    const/4 v7, 0x0

    .line 1367
    :cond_0
    :goto_0
    return-object v7

    .line 1327
    :cond_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v8, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v9

    .line 1328
    .local v9, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v9, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1329
    const/4 v11, 0x1

    invoke-interface {v9, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1331
    invoke-interface {v8}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v5

    .line 1332
    .local v5, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    sget-object v12, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1333
    const/16 v11, -0x60

    invoke-virtual {v5, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1335
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v5, v9}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v6

    .line 1336
    .local v6, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v6, :cond_2

    .line 1337
    const/4 v7, 0x0

    goto :goto_0

    .line 1339
    :cond_2
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_3

    .line 1340
    const/4 v7, 0x0

    goto :goto_0

    .line 1342
    :cond_3
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_4

    .line 1343
    const/4 v7, 0x0

    goto :goto_0

    .line 1345
    :cond_4
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-lt v1, v11, :cond_5

    .line 1367
    const/4 v7, 0x0

    goto :goto_0

    .line 1346
    :cond_5
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v1}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1347
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_8

    .line 1348
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_8

    .line 1349
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/4 v12, 0x4

    if-ne v11, v12, :cond_8

    .line 1350
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    if-eqz v11, :cond_8

    .line 1351
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    check-cast v11, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v11}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1352
    .local v10, "value":Ljava/lang/String;
    const-string v11, "CMD:"

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1353
    .local v2, "index":I
    const/4 v11, -0x1

    if-ne v2, v11, :cond_6

    .line 1354
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 1355
    :cond_6
    add-int/lit8 v11, v2, 0x4

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1356
    const-string v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1357
    const/4 v11, -0x1

    if-eq v2, v11, :cond_7

    .line 1358
    const/4 v11, 0x0

    invoke-virtual {v10, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 1359
    :cond_7
    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1360
    .local v4, "langs":[Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1361
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    array-length v12, v4

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v12, :cond_0

    aget-object v3, v4, v11

    .line 1362
    .local v3, "l":Ljava/lang/String;
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1361
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1345
    .end local v2    # "index":I
    .end local v3    # "l":Ljava/lang/String;
    .end local v4    # "langs":[Ljava/lang/String;
    .end local v7    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "value":Ljava/lang/String;
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getMacAddressBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1212
    const-string v12, "MFPDiscoveryService"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "getMacAddressBySNMP : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getMacAddressIndexBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1215
    .local v5, "index":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 1216
    const/4 v12, 0x0

    .line 1259
    :goto_0
    return-object v12

    .line 1218
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v9

    .line 1219
    .local v9, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v9, :cond_1

    .line 1220
    const/4 v12, 0x0

    goto :goto_0

    .line 1223
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v9, v12}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v10

    .line 1224
    .local v10, "target":Lorg/snmp4j/Target;
    const-wide/16 v12, 0x1f4

    invoke-interface {v10, v12, v13}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1225
    const/4 v12, 0x1

    invoke-interface {v10, v12}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1227
    invoke-interface {v9}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v6

    .line 1228
    .local v6, "pdu":Lorg/snmp4j/PDU;
    new-instance v12, Lorg/snmp4j/smi/VariableBinding;

    new-instance v13, Lorg/snmp4j/smi/OID;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "1.3.6.1.2.1.2.2.1.6"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    invoke-direct {v12, v13}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v6, v12}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1229
    const/16 v12, -0x60

    invoke-virtual {v6, v12}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1231
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v12, v6, v10}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v7

    .line 1232
    .local v7, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v7, :cond_2

    .line 1233
    const/4 v12, 0x0

    goto :goto_0

    .line 1235
    :cond_2
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v12

    if-nez v12, :cond_3

    .line 1236
    const/4 v12, 0x0

    goto :goto_0

    .line 1238
    :cond_3
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v12

    invoke-virtual {v12}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v12

    if-eqz v12, :cond_4

    .line 1239
    const/4 v12, 0x0

    goto :goto_0

    .line 1241
    :cond_4
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v12

    invoke-virtual {v12}, Lorg/snmp4j/PDU;->size()I

    move-result v12

    if-lt v4, v12, :cond_5

    .line 1259
    const/4 v12, 0x0

    goto :goto_0

    .line 1242
    :cond_5
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v12

    invoke-virtual {v12, v4}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v2

    .line 1243
    .local v2, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v2}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v12

    const/16 v13, 0x81

    if-eq v12, v13, :cond_7

    .line 1244
    invoke-virtual {v2}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v12

    const/16 v13, 0x80

    if-eq v12, v13, :cond_7

    .line 1245
    invoke-virtual {v2}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v12

    const/4 v13, 0x4

    if-ne v12, v13, :cond_7

    .line 1246
    invoke-virtual {v2}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v12

    if-eqz v12, :cond_7

    .line 1247
    invoke-virtual {v2}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v11

    check-cast v11, Lorg/snmp4j/smi/OctetString;

    .line 1248
    .local v11, "variable":Lorg/snmp4j/smi/OctetString;
    invoke-virtual {v11}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v3

    .line 1249
    .local v3, "bytes":[B
    array-length v12, v3

    if-eqz v12, :cond_7

    .line 1250
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 1251
    .local v8, "result":Ljava/lang/StringBuffer;
    array-length v13, v3

    const/4 v12, 0x0

    :goto_2
    if-lt v12, v13, :cond_6

    .line 1255
    const/4 v12, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 1251
    :cond_6
    aget-byte v1, v3, v12

    .line 1252
    .local v1, "b":B
    const-string v14, "%02X"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1253
    const/16 v14, 0x3a

    invoke-virtual {v8, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1251
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 1241
    .end local v1    # "b":B
    .end local v3    # "bytes":[B
    .end local v8    # "result":Ljava/lang/StringBuffer;
    .end local v11    # "variable":Lorg/snmp4j/smi/OctetString;
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public getMacAddressIndexBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v13, 0x6

    const/4 v9, 0x0

    .line 1161
    const-string v10, "MFPDiscoveryService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "getMacAddressIndexBySNMP : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 1164
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_1

    .line 1208
    :cond_0
    :goto_0
    return-object v9

    .line 1168
    :cond_1
    iget-object v10, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v10}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v8

    .line 1169
    .local v8, "target":Lorg/snmp4j/Target;
    const-wide/16 v10, 0x1f4

    invoke-interface {v8, v10, v11}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 1170
    const/4 v10, 0x1

    invoke-interface {v8, v10}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 1172
    new-instance v4, Lorg/snmp4j/smi/OID;

    const-string v10, "1.3.6.1.2.1.2.2.1.3"

    invoke-direct {v4, v10}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 1173
    .local v4, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v4

    .line 1175
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    :cond_2
    invoke-virtual {v1, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1176
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v5

    .line 1177
    .local v5, "pdu":Lorg/snmp4j/PDU;
    new-instance v10, Lorg/snmp4j/smi/VariableBinding;

    new-instance v11, Lorg/snmp4j/smi/OID;

    invoke-direct {v11, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v10, v11}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v5, v10}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 1178
    const/16 v10, -0x5f

    invoke-virtual {v5, v10}, Lorg/snmp4j/PDU;->setType(I)V

    .line 1180
    iget-object v10, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v10, v5, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v6

    .line 1181
    .local v6, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v6, :cond_0

    .line 1184
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 1187
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v10

    if-nez v10, :cond_0

    .line 1190
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->size()I

    move-result v10

    if-eqz v10, :cond_0

    .line 1193
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10}, Lorg/snmp4j/PDU;->size()I

    move-result v10

    if-ge v2, v10, :cond_2

    .line 1194
    invoke-virtual {v6}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v10

    invoke-virtual {v10, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 1195
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/16 v11, 0x81

    if-eq v10, v11, :cond_4

    .line 1196
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/16 v11, 0x80

    if-eq v10, v11, :cond_4

    .line 1197
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_4

    .line 1198
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 1199
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v10

    invoke-virtual {v10, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1200
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v3

    check-cast v3, Lorg/snmp4j/smi/Integer32;

    .line 1201
    .local v3, "intValue":Lorg/snmp4j/smi/Integer32;
    new-instance v10, Lorg/snmp4j/smi/Integer32;

    invoke-direct {v10, v13}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    invoke-virtual {v3, v10}, Lorg/snmp4j/smi/Integer32;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    new-instance v10, Lorg/snmp4j/smi/Integer32;

    invoke-direct {v10, v13}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    invoke-virtual {v3, v10}, Lorg/snmp4j/smi/Integer32;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1202
    :cond_3
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v9

    invoke-virtual {v9}, Lorg/snmp4j/smi/OID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Lorg/snmp4j/smi/OID;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 1205
    .end local v3    # "intValue":Lorg/snmp4j/smi/Integer32;
    :cond_4
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 1193
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getModelName(Ljava/util/Map;)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/snmp4j/smi/OID;",
            "Lorg/snmp4j/smi/Variable;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "variables":Ljava/util/Map;, "Ljava/util/Map<Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;>;"
    const/4 v11, 0x4

    const/4 v10, -0x1

    .line 2143
    :try_start_0
    sget-object v7, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2144
    sget-object v7, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid:Lorg/snmp4j/smi/OID;

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/snmp4j/smi/Variable;

    .line 2145
    .local v5, "var":Lorg/snmp4j/smi/Variable;
    invoke-interface {v5}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v7

    if-ne v7, v11, :cond_0

    .line 2146
    move-object v0, v5

    check-cast v0, Lorg/snmp4j/smi/OctetString;

    move-object v6, v0

    .line 2147
    .local v6, "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v7, Ljava/lang/String;

    invoke-virtual {v6}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v8

    const-string v9, "UTF8"

    invoke-direct {v7, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/4 v8, 0x0

    const/16 v9, 0x20

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    .line 2148
    .local v4, "value":Ljava/lang/String;
    move-object v3, v4

    .line 2149
    .local v3, "mfg":Ljava/lang/String;
    const-string v7, "MFG:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2150
    .local v2, "index":I
    if-eq v2, v10, :cond_0

    .line 2151
    add-int/lit8 v7, v2, 0x4

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2152
    const-string v7, ";"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2153
    if-eq v2, v10, :cond_0

    .line 2154
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2156
    const-string v7, "^\\s+"

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2157
    const-string v7, "\\s+$"

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2158
    const-string v7, "MDL:"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2159
    if-eq v2, v10, :cond_0

    .line 2160
    add-int/lit8 v7, v2, 0x4

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2161
    const-string v7, ";"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2162
    const/4 v7, 0x0

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2163
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2241
    .end local v2    # "index":I
    .end local v3    # "mfg":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    .end local v5    # "var":Lorg/snmp4j/smi/Variable;
    .end local v6    # "variable":Lorg/snmp4j/smi/OctetString;
    :goto_0
    return-object v7

    .line 2197
    :cond_0
    sget-object v7, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid2:Lorg/snmp4j/smi/OID;

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2198
    sget-object v7, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->samsungModeOid2:Lorg/snmp4j/smi/OID;

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/snmp4j/smi/Variable;

    .line 2199
    .restart local v5    # "var":Lorg/snmp4j/smi/Variable;
    invoke-interface {v5}, Lorg/snmp4j/smi/Variable;->getSyntax()I

    move-result v7

    if-ne v7, v11, :cond_1

    .line 2200
    move-object v0, v5

    check-cast v0, Lorg/snmp4j/smi/OctetString;

    move-object v6, v0

    .line 2201
    .restart local v6    # "variable":Lorg/snmp4j/smi/OctetString;
    new-instance v7, Ljava/lang/String;

    invoke-virtual {v6}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v8

    const-string v9, "UTF8"

    invoke-direct {v7, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/4 v8, 0x0

    const/16 v9, 0x20

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    .line 2202
    .restart local v4    # "value":Ljava/lang/String;
    move-object v3, v4

    .line 2203
    .restart local v3    # "mfg":Ljava/lang/String;
    const-string v7, "MFG:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2204
    .restart local v2    # "index":I
    if-eq v2, v10, :cond_1

    .line 2205
    add-int/lit8 v7, v2, 0x4

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2206
    const-string v7, ";"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2207
    if-eq v2, v10, :cond_1

    .line 2208
    const/4 v7, 0x0

    invoke-virtual {v3, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2210
    const-string v7, "^\\s+"

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2211
    const-string v7, "\\s+$"

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2212
    const-string v7, "MDL:"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2213
    if-eq v2, v10, :cond_1

    .line 2214
    add-int/lit8 v7, v2, 0x4

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 2215
    const-string v7, ";"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 2216
    const/4 v7, 0x0

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2217
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto/16 :goto_0

    .line 2238
    .end local v2    # "index":I
    .end local v3    # "mfg":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    .end local v5    # "var":Lorg/snmp4j/smi/Variable;
    .end local v6    # "variable":Lorg/snmp4j/smi/OctetString;
    :catch_0
    move-exception v1

    .line 2239
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2241
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method public getPrinterAlertsBySNMP(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 811
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "getPrinterAlertsBySNMP : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v7

    .line 814
    .local v7, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v7, :cond_1

    move-object v6, v10

    .line 859
    :cond_0
    :goto_0
    return-object v6

    .line 818
    :cond_1
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v7, v11}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v8

    .line 819
    .local v8, "target":Lorg/snmp4j/Target;
    const-wide/16 v11, 0x1f4

    invoke-interface {v8, v11, v12}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 820
    const/4 v11, 0x1

    invoke-interface {v8, v11}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 822
    new-instance v3, Lorg/snmp4j/smi/OID;

    const-string v11, "1.3.6.1.2.1.43.18.1.1.7"

    invoke-direct {v3, v11}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 823
    .local v3, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v3

    .line 825
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 827
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v1, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 828
    invoke-interface {v7}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v4

    .line 829
    .local v4, "pdu":Lorg/snmp4j/PDU;
    new-instance v11, Lorg/snmp4j/smi/VariableBinding;

    new-instance v12, Lorg/snmp4j/smi/OID;

    invoke-direct {v12, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v11, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 830
    const/16 v11, -0x5f

    invoke-virtual {v4, v11}, Lorg/snmp4j/PDU;->setType(I)V

    .line 832
    iget-object v11, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v11, v4, v8}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v5

    .line 833
    .local v5, "response":Lorg/snmp4j/event/ResponseEvent;
    if-nez v5, :cond_3

    move-object v6, v10

    .line 834
    goto :goto_0

    .line 836
    :cond_3
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    if-nez v11, :cond_4

    move-object v6, v10

    .line 837
    goto :goto_0

    .line 839
    :cond_4
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v11

    if-eqz v11, :cond_5

    move-object v6, v10

    .line 840
    goto :goto_0

    .line 842
    :cond_5
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-nez v11, :cond_6

    move-object v6, v10

    .line 843
    goto :goto_0

    .line 845
    :cond_6
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11}, Lorg/snmp4j/PDU;->size()I

    move-result v11

    if-ge v2, v11, :cond_2

    .line 846
    invoke-virtual {v5}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v11

    invoke-virtual {v11, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 847
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x81

    if-eq v11, v12, :cond_7

    .line 848
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v11

    const/16 v12, 0x80

    if-eq v11, v12, :cond_7

    .line 849
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v11

    invoke-virtual {v11, v3}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 850
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    check-cast v9, Lorg/snmp4j/smi/Integer32;

    .line 851
    .local v9, "value":Lorg/snmp4j/smi/Integer32;
    if-eqz v9, :cond_7

    .line 852
    const-string v11, "MFPDiscoveryService"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "printer alerts : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    invoke-virtual {v9}, Lorg/snmp4j/smi/Integer32;->toInt()I

    move-result v11

    invoke-static {v11}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getAlertNameByType(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 856
    .end local v9    # "value":Lorg/snmp4j/smi/Integer32;
    :cond_7
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 845
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getPrinterIndexBySNMP(Lcom/sec/print/mobileprint/df/SnmpSessionBase;Lorg/snmp4j/Target;)Ljava/lang/String;
    .locals 10
    .param p1, "snmpSession"    # Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .param p2, "target"    # Lorg/snmp4j/Target;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2245
    const-string v8, "MFPDiscoveryService"

    const-string v9, "getPrinterIndexBySNMP"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2247
    new-instance v6, Lorg/snmp4j/smi/OID;

    const-string v8, "1.3.6.1.2.1.25.3.1.5"

    invoke-direct {v6, v8}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 2248
    .local v6, "printerTypeOid":Lorg/snmp4j/smi/OID;
    new-instance v4, Lorg/snmp4j/smi/OID;

    const-string v8, "1.3.6.1.2.1.25.3.2.1.2"

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 2249
    .local v4, "mainOid":Lorg/snmp4j/smi/OID;
    move-object v1, v4

    .line 2250
    .local v1, "currentOID":Lorg/snmp4j/smi/OID;
    :cond_0
    invoke-virtual {v1, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2283
    :cond_1
    :goto_0
    return-object v3

    .line 2251
    :cond_2
    invoke-interface {p1}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v5

    .line 2252
    .local v5, "pdu":Lorg/snmp4j/PDU;
    new-instance v8, Lorg/snmp4j/smi/VariableBinding;

    new-instance v9, Lorg/snmp4j/smi/OID;

    invoke-direct {v9, v1}, Lorg/snmp4j/smi/OID;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-direct {v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v5, v8}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 2253
    const/16 v8, -0x5f

    invoke-virtual {v5, v8}, Lorg/snmp4j/PDU;->setType(I)V

    .line 2255
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v8, v5, p2}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v7

    .line 2256
    .local v7, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v7, :cond_1

    .line 2259
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2262
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v8

    if-nez v8, :cond_1

    .line 2265
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/PDU;->size()I

    move-result v8

    if-eqz v8, :cond_1

    .line 2268
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/PDU;->size()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 2269
    invoke-virtual {v7}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v8

    invoke-virtual {v8, v2}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v0

    .line 2270
    .local v0, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v9, 0x82

    if-eq v8, v9, :cond_1

    .line 2272
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v9, 0x81

    if-eq v8, v9, :cond_3

    .line 2273
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v8

    const/16 v9, 0x80

    if-eq v8, v9, :cond_3

    .line 2274
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual {v8, v4}, Lorg/snmp4j/smi/OID;->startsWith(Lorg/snmp4j/smi/OID;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2275
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v8

    invoke-interface {v8, v6}, Lorg/snmp4j/smi/Variable;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2276
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/smi/OID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lorg/snmp4j/smi/OID;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2277
    .local v3, "index":Ljava/lang/String;
    goto/16 :goto_0

    .line 2280
    .end local v3    # "index":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v1

    .line 2268
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getPrinterStatusAndStateBySNMP(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    .locals 24
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 549
    const-string v21, "MFPDiscoveryService"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "getPrinterStatusAndStateBySNMP : "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v18

    .line 552
    .local v18, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v18, :cond_1

    .line 553
    const/4 v7, 0x0

    .line 707
    :cond_0
    :goto_0
    return-object v7

    .line 555
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v19

    .line 556
    .local v19, "target":Lorg/snmp4j/Target;
    const-wide/16 v21, 0x1f4

    move-object/from16 v0, v19

    move-wide/from16 v1, v21

    invoke-interface {v0, v1, v2}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 557
    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 559
    new-instance v14, Lorg/snmp4j/smi/OID;

    const-string v21, "1.3.6.1.2.1.25.3.5.1.1.1"

    move-object/from16 v0, v21

    invoke-direct {v14, v0}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 560
    .local v14, "printerStatusOid":Lorg/snmp4j/smi/OID;
    new-instance v12, Lorg/snmp4j/smi/OID;

    const-string v21, "1.3.6.1.2.1.25.3.5.1.2.1"

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 562
    .local v12, "printerDetectedErrorState":Lorg/snmp4j/smi/OID;
    invoke-interface/range {v18 .. v18}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v15

    .line 563
    .local v15, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v21, Lorg/snmp4j/smi/VariableBinding;

    move-object/from16 v0, v21

    invoke-direct {v0, v14}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 564
    new-instance v21, Lorg/snmp4j/smi/VariableBinding;

    move-object/from16 v0, v21

    invoke-direct {v0, v12}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 566
    const/16 v21, -0x60

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/snmp4j/PDU;->setType(I)V

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v16

    .line 569
    .local v16, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v21

    if-nez v21, :cond_3

    .line 570
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 572
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v17

    .line 574
    .local v17, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v8

    .line 575
    .local v8, "error":I
    :goto_1
    const/16 v21, 0x2

    move/from16 v0, v21

    if-eq v8, v0, :cond_4

    .line 590
    if-eqz v8, :cond_9

    .line 591
    const/4 v7, 0x0

    goto :goto_0

    .line 576
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v21

    invoke-virtual {v15}, Lorg/snmp4j/PDU;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-gt v0, v1, :cond_5

    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v21

    if-gtz v21, :cond_6

    .line 577
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 579
    :cond_6
    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v9

    .line 580
    .local v9, "errorIndex":I
    add-int/lit8 v21, v9, -0x1

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/snmp4j/PDU;->remove(I)V

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v16

    .line 583
    if-eqz v16, :cond_7

    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v21

    if-nez v21, :cond_8

    .line 584
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 586
    :cond_8
    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v17

    .line 587
    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v8

    goto :goto_1

    .line 593
    .end local v9    # "errorIndex":I
    :cond_9
    new-instance v7, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;

    invoke-direct {v7}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;-><init>()V

    .line 595
    .local v7, "deviceStatus":Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    invoke-virtual/range {v17 .. v17}, Lorg/snmp4j/PDU;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v10, v0, :cond_0

    .line 596
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v5

    .line 597
    .local v5, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v21

    const/16 v22, 0x81

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 598
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v21

    const/16 v22, 0x80

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 599
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v21

    if-eqz v21, :cond_a

    .line 600
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Lorg/snmp4j/smi/OID;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v21

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 601
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v20

    check-cast v20, Lorg/snmp4j/smi/Integer32;

    .line 602
    .local v20, "value":Lorg/snmp4j/smi/Integer32;
    invoke-virtual/range {v20 .. v20}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v21

    packed-switch v21, :pswitch_data_0

    .line 595
    .end local v20    # "value":Lorg/snmp4j/smi/Integer32;
    :cond_a
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 604
    .restart local v20    # "value":Lorg/snmp4j/smi/Integer32;
    :pswitch_0
    const-string v21, "other"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    goto :goto_3

    .line 607
    :pswitch_1
    const-string v21, "unknown"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    goto :goto_3

    .line 610
    :pswitch_2
    const-string v21, "idl"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    goto :goto_3

    .line 613
    :pswitch_3
    const-string v21, "printing"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    goto :goto_3

    .line 616
    :pswitch_4
    const-string v21, "warmup"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    goto :goto_3

    .line 619
    .end local v20    # "value":Lorg/snmp4j/smi/Integer32;
    :cond_b
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lorg/snmp4j/smi/OID;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v21

    const/16 v22, 0x4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 620
    invoke-virtual {v5}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v21

    check-cast v21, Lorg/snmp4j/smi/OctetString;

    invoke-virtual/range {v21 .. v21}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v20

    .line 621
    .local v20, "value":[B
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_a

    .line 622
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 623
    .local v6, "detectedErrorStates":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v13, 0x0

    .line 625
    .local v13, "printerErrorMasks":I
    const/16 v21, 0x0

    aget-byte v4, v20, v21

    .line 626
    .local v4, "b2":B
    const/16 v21, 0x1

    aget-byte v3, v20, v21

    .line 628
    .local v3, "b1":B
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_4
    const/16 v21, 0x8

    move/from16 v0, v21

    if-lt v11, v0, :cond_1b

    .line 635
    const/4 v11, 0x0

    :goto_5
    const/16 v21, 0x8

    move/from16 v0, v21

    if-lt v11, v0, :cond_1c

    .line 642
    and-int/lit8 v21, v13, 0x1

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 644
    const-string v21, "lowPaper"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    :cond_c
    and-int/lit8 v21, v13, 0x2

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 648
    const-string v21, "noPaper"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650
    :cond_d
    and-int/lit8 v21, v13, 0x4

    const/16 v22, 0x4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 652
    const-string v21, "lowToner"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 654
    :cond_e
    and-int/lit8 v21, v13, 0x8

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 656
    const-string v21, "noToner"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 658
    :cond_f
    and-int/lit8 v21, v13, 0x10

    const/16 v22, 0x10

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    .line 660
    const-string v21, "doorOpen"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    :cond_10
    and-int/lit8 v21, v13, 0x20

    const/16 v22, 0x20

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_11

    .line 664
    const-string v21, "jammed"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    :cond_11
    and-int/lit8 v21, v13, 0x40

    const/16 v22, 0x40

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_12

    .line 668
    const-string v21, "offline"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 670
    :cond_12
    and-int/lit16 v0, v13, 0x80

    move/from16 v21, v0

    const/16 v22, 0x80

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    .line 672
    const-string v21, "serviceRequested"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 674
    :cond_13
    and-int/lit16 v0, v13, 0x100

    move/from16 v21, v0

    const/16 v22, 0x100

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_14

    .line 676
    const-string v21, "inputTrayMissing"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    :cond_14
    and-int/lit16 v0, v13, 0x200

    move/from16 v21, v0

    const/16 v22, 0x200

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_15

    .line 680
    const-string v21, "outputTrayMissing"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 682
    :cond_15
    and-int/lit16 v0, v13, 0x400

    move/from16 v21, v0

    const/16 v22, 0x400

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_16

    .line 684
    const-string v21, "markerSupplyMissing"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 686
    :cond_16
    and-int/lit16 v0, v13, 0x800

    move/from16 v21, v0

    const/16 v22, 0x800

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_17

    .line 688
    const-string v21, "outputNearFull"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 690
    :cond_17
    and-int/lit16 v0, v13, 0x1000

    move/from16 v21, v0

    const/16 v22, 0x1000

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_18

    .line 692
    const-string v21, "outputFull"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    :cond_18
    and-int/lit16 v0, v13, 0x2000

    move/from16 v21, v0

    const/16 v22, 0x2000

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_19

    .line 696
    const-string v21, "inputTrayEmpty"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 698
    :cond_19
    and-int/lit16 v0, v13, 0x4000

    move/from16 v21, v0

    const/16 v22, 0x4000

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_1a

    .line 700
    const-string v21, "overduePreventMaint"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 702
    :cond_1a
    invoke-virtual {v7, v6}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setDetectedErrorStates(Ljava/util/List;)V

    goto/16 :goto_3

    .line 630
    :cond_1b
    shr-int/lit8 v13, v13, 0x1

    .line 631
    and-int/lit16 v0, v3, 0x80

    move/from16 v21, v0

    shl-int/lit8 v21, v21, 0x8

    or-int v13, v13, v21

    .line 632
    shl-int/lit8 v21, v3, 0x1

    move/from16 v0, v21

    int-to-byte v3, v0

    .line 628
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 637
    :cond_1c
    shr-int/lit8 v13, v13, 0x1

    .line 638
    and-int/lit16 v0, v4, 0x80

    move/from16 v21, v0

    shl-int/lit8 v21, v21, 0x8

    or-int v13, v13, v21

    .line 639
    shl-int/lit8 v21, v4, 0x1

    move/from16 v0, v21

    int-to-byte v4, v0

    .line 635
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_5

    .line 602
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getPrinterStatusBySNMP(Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 863
    const-string v13, "MFPDiscoveryService"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getPrinterStatusBySNMP : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v10

    .line 866
    .local v10, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v10, :cond_0

    .line 867
    const/4 v2, 0x0

    .line 940
    :goto_0
    return-object v2

    .line 870
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v10, v13}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v11

    .line 871
    .local v11, "target":Lorg/snmp4j/Target;
    const-wide/16 v13, 0x1f4

    invoke-interface {v11, v13, v14}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 872
    const/4 v13, 0x1

    invoke-interface {v11, v13}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 874
    new-instance v6, Lorg/snmp4j/smi/OID;

    const-string v13, "1.3.6.1.2.1.25.3.5.1.1.1"

    invoke-direct {v6, v13}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 876
    .local v6, "printerStatusOid":Lorg/snmp4j/smi/OID;
    invoke-interface {v10}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v7

    .line 877
    .local v7, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v13, Lorg/snmp4j/smi/VariableBinding;

    invoke-direct {v13, v6}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 879
    const/16 v13, -0x60

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->setType(I)V

    .line 881
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v13, v7, v11}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v8

    .line 882
    .local v8, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    if-nez v13, :cond_2

    .line 883
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 885
    :cond_2
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    .line 887
    .local v9, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v3

    .line 888
    .local v3, "error":I
    :goto_1
    const/4 v13, 0x2

    if-eq v3, v13, :cond_3

    .line 903
    if-eqz v3, :cond_8

    .line 904
    const/4 v2, 0x0

    goto :goto_0

    .line 889
    :cond_3
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v13

    invoke-virtual {v7}, Lorg/snmp4j/PDU;->size()I

    move-result v14

    if-gt v13, v14, :cond_4

    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v13

    if-gtz v13, :cond_5

    .line 890
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 892
    :cond_5
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v4

    .line 893
    .local v4, "errorIndex":I
    add-int/lit8 v13, v4, -0x1

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->remove(I)V

    .line 895
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v13, v7, v11}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v8

    .line 896
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    if-nez v13, :cond_7

    .line 897
    :cond_6
    const/4 v2, 0x0

    goto :goto_0

    .line 899
    :cond_7
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    .line 900
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v3

    goto :goto_1

    .line 906
    .end local v4    # "errorIndex":I
    :cond_8
    const/4 v2, 0x0

    .line 908
    .local v2, "deviceStatus":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->size()I

    move-result v13

    if-lt v5, v13, :cond_9

    .line 938
    const-string v13, "MFPDiscoveryService"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "deviceStatus : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 909
    :cond_9
    invoke-virtual {v9, v5}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v1

    .line 910
    .local v1, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/16 v14, 0x81

    if-eq v13, v14, :cond_a

    .line 911
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/16 v14, 0x80

    if-eq v13, v14, :cond_a

    .line 912
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v13

    if-eqz v13, :cond_a

    .line 913
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v13

    invoke-virtual {v13, v6}, Lorg/snmp4j/smi/OID;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_a

    .line 914
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v12

    check-cast v12, Lorg/snmp4j/smi/Integer32;

    .line 915
    .local v12, "value":Lorg/snmp4j/smi/Integer32;
    const-string v13, "MFPDiscoveryService"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "printer status : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    invoke-virtual {v12}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v13

    packed-switch v13, :pswitch_data_0

    .line 908
    .end local v12    # "value":Lorg/snmp4j/smi/Integer32;
    :cond_a
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 919
    .restart local v12    # "value":Lorg/snmp4j/smi/Integer32;
    :pswitch_0
    const-string v2, "other"

    .line 920
    goto :goto_3

    .line 922
    :pswitch_1
    const-string v2, "unknown"

    .line 923
    goto :goto_3

    .line 925
    :pswitch_2
    const-string v2, "idl"

    .line 926
    goto :goto_3

    .line 928
    :pswitch_3
    const-string v2, "printing"

    .line 929
    goto :goto_3

    .line 931
    :pswitch_4
    const-string v2, "warmup"

    goto :goto_3

    .line 917
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getPrinters()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3077
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3079
    .local v0, "devices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 3080
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public getScanners()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3085
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->devicesRow:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3087
    .local v0, "devices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 3088
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .locals 12
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 253
    const/4 v11, 0x0

    .line 255
    .local v11, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    .line 268
    .local v1, "inetAddress":Ljava/net/InetAddress;
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z

    if-eqz v2, :cond_0

    .line 269
    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpSession3;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3userName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3authProtocol:Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;

    .line 270
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3authPassword:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3privProtocol:Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;

    iget-object v7, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3privPassword:Ljava/lang/String;

    const/4 v8, 0x0

    const/16 v9, 0xa1

    .line 269
    invoke-direct/range {v0 .. v9}, Lcom/sec/print/mobileprint/df/SnmpSession3;-><init>(Ljava/net/InetAddress;Ljava/lang/String;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;Ljava/lang/String;II)V

    .line 278
    .end local v1    # "inetAddress":Ljava/net/InetAddress;
    .end local v11    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .local v0, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :goto_0
    return-object v0

    .line 272
    .end local v0    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .restart local v1    # "inetAddress":Ljava/net/InetAddress;
    .restart local v11    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :cond_0
    new-instance v0, Lcom/sec/print/mobileprint/df/SnmpSession12;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;

    const/4 v4, 0x1

    const/16 v5, 0xa1

    invoke-direct/range {v0 .. v5}, Lcom/sec/print/mobileprint/df/SnmpSession12;-><init>(Ljava/net/InetAddress;ILjava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v11    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .restart local v0    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    goto :goto_0

    .line 274
    .end local v0    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .end local v1    # "inetAddress":Ljava/net/InetAddress;
    .restart local v11    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :catch_0
    move-exception v10

    .line 275
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v11

    .end local v11    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .restart local v0    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    goto :goto_0
.end method

.method protected isSamsungMFP(Ljavax/jmdns/ServiceInfo;)Z
    .locals 7
    .param p1, "info"    # Ljavax/jmdns/ServiceInfo;

    .prologue
    const/4 v5, 0x1

    .line 3049
    invoke-virtual {p1}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3072
    :cond_0
    :goto_0
    return v5

    .line 3052
    :cond_1
    const-string v6, "usb_MFG"

    invoke-virtual {p1, v6}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3053
    .local v4, "usb_MFG":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-direct {p0, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3056
    :cond_2
    const-string v6, "MFG"

    invoke-virtual {p1, v6}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3057
    .local v0, "MFG":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3060
    :cond_3
    const-string v6, "mfg"

    invoke-virtual {p1, v6}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3061
    .local v1, "mfg":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-direct {p0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3064
    :cond_4
    const-string v6, "product"

    invoke-virtual {p1, v6}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3065
    .local v2, "product":Ljava/lang/String;
    if-eqz v2, :cond_5

    invoke-direct {p0, v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3068
    :cond_5
    const-string v6, "ty"

    invoke-virtual {p1, v6}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3069
    .local v3, "ty":Ljava/lang/String;
    if-eqz v3, :cond_6

    invoke-direct {p0, v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3072
    :cond_6
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public newDeviceFound(Ljava/lang/String;I)V
    .locals 11
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    .line 2780
    const-string v8, "MFPDiscoveryService"

    const-string v9, "SNMP newDeviceFound"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2782
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v8, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2834
    :cond_0
    :goto_0
    return-void

    .line 2787
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v6

    .line 2788
    .local v6, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v6, :cond_0

    .line 2789
    const-string v8, "modelName"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2790
    .local v5, "modelName":Ljava/lang/String;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "modelName = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2792
    invoke-direct {p0, v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2793
    const-string v8, "hostName"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2794
    .local v3, "hostName":Ljava/lang/String;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "hostName = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2795
    const-string v8, "location"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2796
    .local v4, "location":Ljava/lang/String;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "location = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2799
    const-string v8, "isScan"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 2800
    .local v7, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isScan = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2801
    const-string v8, "isFax"

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 2802
    .local v2, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isFax = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2804
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 2805
    .local v0, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 2806
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 2807
    invoke-virtual {v0, v4}, Lcom/sec/print/mobileprint/df/MFPDevice;->setNote(Ljava/lang/String;)V

    .line 2808
    const/16 v8, 0x238c

    invoke-virtual {v0, v8}, Lcom/sec/print/mobileprint/df/MFPDevice;->setPort(I)V

    .line 2809
    invoke-virtual {v0, v2}, Lcom/sec/print/mobileprint/df/MFPDevice;->setFaxType(Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;)V

    .line 2810
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->NETWORK:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-virtual {v0, v8}, Lcom/sec/print/mobileprint/df/MFPDevice;->setDiscoveryType(Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;)V

    .line 2812
    sget-object v8, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    invoke-virtual {v0, v8}, Lcom/sec/print/mobileprint/df/MFPDevice;->setScanType(Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;)V

    .line 2814
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v8, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2818
    if-eqz v7, :cond_2

    .line 2819
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v8, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2821
    :cond_2
    if-eqz v2, :cond_3

    .line 2822
    iget-object v8, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v8, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2826
    .end local v0    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .end local v2    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v3    # "hostName":Ljava/lang/String;
    .end local v4    # "location":Ljava/lang/String;
    .end local v7    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :cond_3
    invoke-interface {v6}, Ljava/util/Map;->clear()V

    .line 2827
    const/4 v6, 0x0

    .line 2829
    const-string v8, "MFPDiscoveryService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "SNMP device found: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2831
    .end local v5    # "modelName":Ljava/lang/String;
    .end local v6    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v1

    .line 2832
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public newDeviceFoundException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 2838
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 2839
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3730
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mBinder:Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 3038
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 3039
    const-string v0, "MFPDiscoveryService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3041
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    .line 3042
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3043
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3044
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3045
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mWifiDirectIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3046
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 3097
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 3098
    const-string v1, "MFPDiscoveryService"

    const-string v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3100
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopmDNSDiscovery()V

    .line 3101
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->DestroymDNS()V

    .line 3103
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPDiscovery()V

    .line 3104
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->DestroySNMP()V

    .line 3106
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopWFDDiscovery()V

    .line 3107
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->DestroyWFD()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3116
    :goto_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    if-eqz v1, :cond_0

    .line 3117
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    invoke-virtual {p0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3118
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryWifiDirect:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;

    .line 3120
    :cond_0
    return-void

    .line 3112
    :catch_0
    move-exception v0

    .line 3113
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setVendorsList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "vendors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->vendorsFilter:Ljava/util/List;

    .line 287
    return-void
.end method

.method public wakeDeviceUp(Ljava/lang/String;)I
    .locals 9
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 291
    const-string v6, "MFPDiscoveryService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "wakeDeviceUp : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v3

    .line 312
    .local v3, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v3, :cond_0

    .line 313
    const/4 v5, -0x1

    .line 357
    .end local v3    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :goto_0
    return v5

    .line 316
    .restart local v3    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :cond_0
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v3, v6}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v4

    .line 317
    .local v4, "target":Lorg/snmp4j/Target;
    const-wide/16 v6, 0x1f4

    invoke-interface {v4, v6, v7}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 318
    const/4 v6, 0x0

    invoke-interface {v4, v6}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 320
    new-instance v1, Lorg/snmp4j/smi/OID;

    const-string v6, "1.3.6.1.4.1.236.11.5.11.53.31.1.30.0"

    invoke-direct {v1, v6}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 322
    .local v1, "printerWarmupOid":Lorg/snmp4j/smi/OID;
    invoke-interface {v3}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v2

    .line 323
    .local v2, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v6, Lorg/snmp4j/smi/VariableBinding;

    invoke-direct {v6, v1}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v2, v6}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 324
    const/16 v6, -0x60

    invoke-virtual {v2, v6}, Lorg/snmp4j/PDU;->setType(I)V

    .line 326
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v6, v2, v4}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 328
    .end local v1    # "printerWarmupOid":Lorg/snmp4j/smi/OID;
    .end local v2    # "requestPdu":Lorg/snmp4j/PDU;
    .end local v3    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .end local v4    # "target":Lorg/snmp4j/Target;
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public wakeDeviceUpBySNMP(Ljava/lang/String;)I
    .locals 16
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 361
    const-string v13, "MFPDiscoveryService"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "wakeDeviceUpBySNMP : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSnmpSessionBase(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/SnmpSessionBase;

    move-result-object v10

    .line 365
    .local v10, "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    if-nez v10, :cond_0

    .line 366
    const/4 v13, -0x1

    .line 420
    .end local v10    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :goto_0
    return v13

    .line 368
    .restart local v10    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-interface {v10, v13}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;

    move-result-object v11

    .line 369
    .local v11, "target":Lorg/snmp4j/Target;
    const-wide/16 v13, 0x1f4

    invoke-interface {v11, v13, v14}, Lorg/snmp4j/Target;->setTimeout(J)V

    .line 370
    const/4 v13, 0x0

    invoke-interface {v11, v13}, Lorg/snmp4j/Target;->setRetries(I)V

    .line 372
    new-instance v6, Lorg/snmp4j/smi/OID;

    const-string v13, "1.3.6.1.4.1.236.11.5.11.53.33.4.1.2.1.1"

    invoke-direct {v6, v13}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    .line 374
    .local v6, "printerExtendedDeviceIdOid":Lorg/snmp4j/smi/OID;
    invoke-interface {v10}, Lcom/sec/print/mobileprint/df/SnmpSessionBase;->createSnmpPdu()Lorg/snmp4j/PDU;

    move-result-object v7

    .line 375
    .local v7, "requestPdu":Lorg/snmp4j/PDU;
    new-instance v13, Lorg/snmp4j/smi/VariableBinding;

    invoke-direct {v13, v6}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;)V

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->add(Lorg/snmp4j/smi/VariableBinding;)V

    .line 377
    const/16 v13, -0x60

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->setType(I)V

    .line 379
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v13, v7, v11}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v8

    .line 380
    .local v8, "response":Lorg/snmp4j/event/ResponseEvent;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    if-nez v13, :cond_2

    .line 381
    :cond_1
    const/4 v13, -0x1

    goto :goto_0

    .line 383
    :cond_2
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    .line 385
    .local v9, "responsePdu":Lorg/snmp4j/PDU;
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v3

    .line 386
    .local v3, "error":I
    :goto_1
    const/4 v13, 0x2

    if-eq v3, v13, :cond_3

    .line 401
    if-eqz v3, :cond_8

    .line 402
    const/4 v13, -0x1

    goto :goto_0

    .line 387
    :cond_3
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v13

    invoke-virtual {v7}, Lorg/snmp4j/PDU;->size()I

    move-result v14

    if-gt v13, v14, :cond_4

    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v13

    if-gtz v13, :cond_5

    .line 388
    :cond_4
    const/4 v13, -0x1

    goto :goto_0

    .line 390
    :cond_5
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorIndex()I

    move-result v4

    .line 391
    .local v4, "errorIndex":I
    add-int/lit8 v13, v4, -0x1

    invoke-virtual {v7, v13}, Lorg/snmp4j/PDU;->remove(I)V

    .line 393
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmp:Lorg/snmp4j/Snmp;

    invoke-virtual {v13, v7, v11}, Lorg/snmp4j/Snmp;->send(Lorg/snmp4j/PDU;Lorg/snmp4j/Target;)Lorg/snmp4j/event/ResponseEvent;

    move-result-object v8

    .line 394
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    if-nez v13, :cond_7

    .line 395
    :cond_6
    const/4 v13, -0x1

    goto :goto_0

    .line 397
    :cond_7
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v9

    .line 398
    invoke-virtual {v9}, Lorg/snmp4j/PDU;->getErrorStatus()I

    move-result v3

    goto :goto_1

    .line 404
    .end local v4    # "errorIndex":I
    :cond_8
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    invoke-virtual {v13}, Lorg/snmp4j/PDU;->size()I

    move-result v13

    if-lt v5, v13, :cond_9

    .line 420
    .end local v3    # "error":I
    .end local v5    # "i":I
    .end local v6    # "printerExtendedDeviceIdOid":Lorg/snmp4j/smi/OID;
    .end local v7    # "requestPdu":Lorg/snmp4j/PDU;
    .end local v8    # "response":Lorg/snmp4j/event/ResponseEvent;
    .end local v9    # "responsePdu":Lorg/snmp4j/PDU;
    .end local v10    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .end local v11    # "target":Lorg/snmp4j/Target;
    :goto_3
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 405
    .restart local v3    # "error":I
    .restart local v5    # "i":I
    .restart local v6    # "printerExtendedDeviceIdOid":Lorg/snmp4j/smi/OID;
    .restart local v7    # "requestPdu":Lorg/snmp4j/PDU;
    .restart local v8    # "response":Lorg/snmp4j/event/ResponseEvent;
    .restart local v9    # "responsePdu":Lorg/snmp4j/PDU;
    .restart local v10    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .restart local v11    # "target":Lorg/snmp4j/Target;
    :cond_9
    invoke-virtual {v8}, Lorg/snmp4j/event/ResponseEvent;->getResponse()Lorg/snmp4j/PDU;

    move-result-object v13

    invoke-virtual {v13, v5}, Lorg/snmp4j/PDU;->get(I)Lorg/snmp4j/smi/VariableBinding;

    move-result-object v1

    .line 406
    .local v1, "binding":Lorg/snmp4j/smi/VariableBinding;
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/16 v14, 0x81

    if-eq v13, v14, :cond_a

    .line 407
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/16 v14, 0x80

    if-eq v13, v14, :cond_a

    .line 408
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getSyntax()I

    move-result v13

    const/4 v14, 0x4

    if-ne v13, v14, :cond_a

    .line 409
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v13

    if-eqz v13, :cond_a

    .line 410
    invoke-virtual {v1}, Lorg/snmp4j/smi/VariableBinding;->getVariable()Lorg/snmp4j/smi/Variable;

    move-result-object v13

    check-cast v13, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v13}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v12

    .line 411
    .local v12, "value":Ljava/lang/String;
    const-string v13, "EWU"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 412
    invoke-virtual/range {p0 .. p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->wakeDeviceUp(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    goto/16 :goto_0

    .line 404
    .end local v12    # "value":Ljava/lang/String;
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 416
    .end local v1    # "binding":Lorg/snmp4j/smi/VariableBinding;
    .end local v3    # "error":I
    .end local v5    # "i":I
    .end local v6    # "printerExtendedDeviceIdOid":Lorg/snmp4j/smi/OID;
    .end local v7    # "requestPdu":Lorg/snmp4j/PDU;
    .end local v8    # "response":Lorg/snmp4j/event/ResponseEvent;
    .end local v9    # "responsePdu":Lorg/snmp4j/PDU;
    .end local v10    # "snmpSession":Lcom/sec/print/mobileprint/df/SnmpSessionBase;
    .end local v11    # "target":Lorg/snmp4j/Target;
    :catch_0
    move-exception v2

    .line 417
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public wifiDirectNewDeviceFound(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "scanner"    # Z
    .param p4, "fax"    # Z

    .prologue
    .line 2762
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2777
    :cond_0
    :goto_0
    return-void

    .line 2765
    :cond_1
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 2766
    .local v0, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 2767
    invoke-virtual {v0, p2}, Lcom/sec/print/mobileprint/df/MFPDevice;->setMacAddress(Ljava/lang/String;)V

    .line 2768
    sget-object v1, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->WIFIDIRECT:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/df/MFPDevice;->setDiscoveryType(Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;)V

    .line 2770
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryPrinters:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2771
    if-eqz p3, :cond_2

    .line 2772
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryScanners:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2774
    :cond_2
    if-eqz p4, :cond_0

    .line 2775
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->discoveryFaxes:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
