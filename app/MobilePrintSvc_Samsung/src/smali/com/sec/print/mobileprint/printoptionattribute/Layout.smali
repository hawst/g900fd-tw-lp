.class public Lcom/sec/print/mobileprint/printoptionattribute/Layout;
.super Ljava/lang/Object;
.source "Layout.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Layout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private layoutType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Layout$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;->LAYOUT_NUP:Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Layout;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;)V
    .locals 1
    .param p1, "eLayout"    # Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public getLayoutType()Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;
    .locals 6

    .prologue
    .line 49
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 57
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 49
    :cond_1
    aget-object v0, v1, v2

    .line 51
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    if-eq v4, v5, :cond_0

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    .line 75
    return-void
.end method

.method public setLayoutType(Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;)V
    .locals 1
    .param p1, "eLayout"    # Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Layout$EnumLayoutType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    .line 45
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Layout;->layoutType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    return-void
.end method
