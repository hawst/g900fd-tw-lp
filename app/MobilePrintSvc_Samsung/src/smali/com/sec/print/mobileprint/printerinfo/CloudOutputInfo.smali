.class public Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;
.super Ljava/lang/Object;
.source "CloudOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field agentId:I

.field countryNumber:Ljava/lang/String;

.field macAddress:Ljava/lang/String;

.field name:Ljava/lang/String;

.field phoneNumber:Ljava/lang/String;

.field photoID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 18
    return-void
.end method

.method public constructor <init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "photoID"    # J
    .param p3, "agentId"    # I
    .param p4, "countryNumber"    # Ljava/lang/String;
    .param p5, "phoneNumber"    # Ljava/lang/String;
    .param p6, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-wide p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->photoID:J

    .line 41
    iput p3, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->agentId:I

    .line 42
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->countryNumber:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->phoneNumber:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->name:Ljava/lang/String;

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getAgentId()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->agentId:I

    return v0
.end method

.method public getCountryNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->countryNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoID()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->photoID:J

    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->photoID:J

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->agentId:I

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->countryNumber:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->phoneNumber:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->name:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setAgentId(I)V
    .locals 0
    .param p1, "agentId"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->agentId:I

    .line 93
    return-void
.end method

.method public setCountryNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryNumber"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->countryNumber:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setMacAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->macAddress:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->name:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->phoneNumber:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setPhotoID(J)V
    .locals 0
    .param p1, "photoID"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->photoID:J

    .line 69
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->photoID:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 104
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->agentId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->countryNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/CloudOutputInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    return-void
.end method
