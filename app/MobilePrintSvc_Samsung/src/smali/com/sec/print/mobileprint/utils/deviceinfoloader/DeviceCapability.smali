.class public Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
.super Ljava/lang/Object;
.source "DeviceCapability.java"


# instance fields
.field private cmsFileName:Ljava/lang/String;

.field private colorMode:Z

.field private duplex:Z

.field private emulation:Ljava/lang/String;

.field private jobAccountinVer:Ljava/lang/String;

.field private paperSize:Ljava/lang/String;

.field private paperType:Ljava/lang/String;

.field private pcl6CompressionType:Ljava/lang/String;

.field private splCompressionType:Ljava/lang/String;

.field private splVersion:Ljava/lang/String;

.field private splWidthAlign:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "colorMode"    # Z
    .param p2, "duplex"    # Z
    .param p3, "a3Support"    # Z
    .param p4, "emulation"    # Ljava/lang/String;
    .param p5, "jobAccountinVer"    # Ljava/lang/String;
    .param p6, "pcl6CompressionType"    # Ljava/lang/String;
    .param p7, "paperType"    # Ljava/lang/String;
    .param p8, "paperSize"    # Ljava/lang/String;
    .param p9, "splVersion"    # Ljava/lang/String;
    .param p10, "cmsFileName"    # Ljava/lang/String;
    .param p11, "splCompType"    # Ljava/lang/String;
    .param p12, "splWidthAlign"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->colorMode:Z

    .line 40
    iput-boolean p2, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->duplex:Z

    .line 41
    iput-object p4, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->emulation:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->jobAccountinVer:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->pcl6CompressionType:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperType:Ljava/lang/String;

    .line 45
    iput-object p8, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperSize:Ljava/lang/String;

    .line 46
    iput-object p9, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splVersion:Ljava/lang/String;

    .line 47
    iput-object p10, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->cmsFileName:Ljava/lang/String;

    .line 48
    iput-object p11, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splCompressionType:Ljava/lang/String;

    .line 49
    iput-object p12, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splWidthAlign:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public getCmsFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->cmsFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmulation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->emulation:Ljava/lang/String;

    return-object v0
.end method

.method public getJobAccountinVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->jobAccountinVer:Ljava/lang/String;

    return-object v0
.end method

.method public getPCL6CompressionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->pcl6CompressionType:Ljava/lang/String;

    return-object v0
.end method

.method public getPaperSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperSize:Ljava/lang/String;

    return-object v0
.end method

.method public getPaperType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperType:Ljava/lang/String;

    return-object v0
.end method

.method public getSplCompressionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splCompressionType:Ljava/lang/String;

    return-object v0
.end method

.method public getSplVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSplWidthAlign()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splWidthAlign:Ljava/lang/String;

    return-object v0
.end method

.method public isColorMode()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->colorMode:Z

    return v0
.end method

.method public isDuplex()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->duplex:Z

    return v0
.end method

.method public setCmsFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "cmsFileName"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->cmsFileName:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setColorMode(Z)V
    .locals 0
    .param p1, "colorMode"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->colorMode:Z

    .line 63
    return-void
.end method

.method public setDuplex(Z)V
    .locals 0
    .param p1, "duplex"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->duplex:Z

    .line 76
    return-void
.end method

.method public setEmulation(Ljava/lang/String;)V
    .locals 0
    .param p1, "emulation"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->emulation:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setJobAccountinVer(Ljava/lang/String;)V
    .locals 0
    .param p1, "jobAccountinVer"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->jobAccountinVer:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setPCL6CompressionType(Ljava/lang/String;)V
    .locals 0
    .param p1, "pcl6CompressionType"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->pcl6CompressionType:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setPaperSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "paperSize"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperSize:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setPaperType(Ljava/lang/String;)V
    .locals 0
    .param p1, "paperType"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperType:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setSplCompressionType(Ljava/lang/String;)V
    .locals 0
    .param p1, "splCompressionType"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splCompressionType:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setSplVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "splVersion"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splVersion:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setSplWidthAlign(Ljava/lang/String;)V
    .locals 0
    .param p1, "splWidthAlign"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splWidthAlign:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Color:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->colorMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Duplex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->duplex:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 28
    const-string v1, ", Emulation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->emulation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 29
    const-string v1, ", JobAccountinVer:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->jobAccountinVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    const-string v1, ", cmsFileName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->cmsFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 31
    const-string v1, ", PCL6CompType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->pcl6CompressionType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 32
    const-string v1, ", PaperType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 33
    const-string v1, ", PaperType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->paperSize:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 34
    const-string v1, ", splCompType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splCompressionType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", splVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", width byte:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->splWidthAlign:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
