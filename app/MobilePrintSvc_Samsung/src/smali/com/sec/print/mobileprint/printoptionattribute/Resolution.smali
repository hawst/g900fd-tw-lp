.class public Lcom/sec/print/mobileprint/printoptionattribute/Resolution;
.super Ljava/lang/Object;
.source "Resolution.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Resolution;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private resolutionX:I

.field private resolutionY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Resolution$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x258

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionX:I

    .line 20
    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionY:I

    .line 21
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "resolutionX"    # I
    .param p2, "resolutionY"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionX:I

    .line 26
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionY:I

    .line 27
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->readFromParcel(Landroid/os/Parcel;)V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Resolution;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionX:I

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionY:I

    .line 46
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionX:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Resolution;->resolutionY:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    return-void
.end method
