.class public abstract Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;
.super Landroid/os/Binder;
.source "IDeviceManagerService.java"

# interfaces
.implements Lcom/sec/print/mobileprint/dm/IDeviceManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/dm/IDeviceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.print.mobileprint.dm.IDeviceManagerService"

.field static final TRANSACTION_getLanguages:I = 0x5

.field static final TRANSACTION_getMediaSizes:I = 0x6

.field static final TRANSACTION_getMediaTypes:I = 0x7

.field static final TRANSACTION_getPrinterAlerts:I = 0xe

.field static final TRANSACTION_getPrinterName:I = 0xc

.field static final TRANSACTION_getPrinterStatus:I = 0xd

.field static final TRANSACTION_getPrinters:I = 0x3

.field static final TRANSACTION_getScanners:I = 0x4

.field static final TRANSACTION_isPrinterAlive:I = 0xa

.field static final TRANSACTION_isPrinterColorModel:I = 0x8

.field static final TRANSACTION_isSupportDuplex:I = 0x9

.field static final TRANSACTION_isSupportedPrinter:I = 0xb

.field static final TRANSACTION_start:I = 0x1

.field static final TRANSACTION_stop:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/print/mobileprint/dm/IDeviceManagerService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/print/mobileprint/dm/IDeviceManagerService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 215
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 50
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->start(I)I

    move-result v1

    .line 51
    .local v1, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 52
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":I
    .end local v1    # "_result":I
    :sswitch_2
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->stop()I

    move-result v1

    .line 59
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 65
    .end local v1    # "_result":I
    :sswitch_3
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getPrinters()Ljava/util/List;

    move-result-object v2

    .line 67
    .local v2, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 73
    .end local v2    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    :sswitch_4
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getScanners()Ljava/util/List;

    move-result-object v2

    .line 75
    .restart local v2    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 81
    .end local v2    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/DeviceInfo;>;"
    :sswitch_5
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_0

    .line 84
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 89
    .local v0, "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getLanguages(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v4

    .line 90
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto :goto_0

    .line 87
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_1

    .line 96
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_6
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_1

    .line 99
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 104
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getMediaSizes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v3

    .line 105
    .local v3, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 102
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v3    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/dm/MediaSizeInfo;>;"
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_2

    .line 111
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_7
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_2

    .line 114
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 119
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getMediaTypes(Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;

    move-result-object v4

    .line 120
    .restart local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 117
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_3

    .line 126
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_8
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_3

    .line 129
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 134
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_4
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->isPrinterColorModel(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v1

    .line 135
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 136
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 132
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v1    # "_result":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_4

    .line 141
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_9
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_4

    .line 144
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 149
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_5
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->isSupportDuplex(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v1

    .line 150
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v1    # "_result":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_5

    .line 156
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_a
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_5

    .line 159
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 164
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_6
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->isPrinterAlive(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v1

    .line 165
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 166
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 162
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v1    # "_result":I
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_6

    .line 171
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_b
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_6

    .line 174
    sget-object v6, Lcom/sec/print/mobileprint/dm/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .line 179
    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :goto_7
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->isSupportedPrinter(Lcom/sec/print/mobileprint/dm/DeviceInfo;)I

    move-result v1

    .line 180
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 181
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 177
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .end local v1    # "_result":I
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    goto :goto_7

    .line 186
    .end local v0    # "_arg0":Lcom/sec/print/mobileprint/dm/DeviceInfo;
    :sswitch_c
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getPrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 191
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_result":Ljava/lang/String;
    :sswitch_d
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 199
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getPrinterStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 200
    .restart local v1    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 201
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_result":Ljava/lang/String;
    :sswitch_e
    const-string v6, "com.sec.print.mobileprint.dm.IDeviceManagerService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 208
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 209
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;->getPrinterAlerts(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 210
    .restart local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 211
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
