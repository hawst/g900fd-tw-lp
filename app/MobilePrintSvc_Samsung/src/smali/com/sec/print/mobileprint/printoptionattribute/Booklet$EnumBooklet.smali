.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
.super Ljava/lang/Enum;
.source "Booklet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/Booklet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumBooklet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOOKLET_FOLDING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

.field public static final enum BOOKLET_FOLDING_STAPLING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

.field public static final enum BOOKLET_OFF:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    const-string v1, "BOOKLET_OFF"

    .line 22
    invoke-direct {v0, v1, v2, v2}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_OFF:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    .line 23
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    const-string v1, "BOOKLET_FOLDING"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_FOLDING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    .line 24
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    const-string v1, "BOOKLET_FOLDING_STAPLING"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_FOLDING_STAPLING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_OFF:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_FOLDING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->BOOKLET_FOLDING_STAPLING:Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->mValue:I

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Booklet$EnumBooklet;->mValue:I

    return v0
.end method
