.class public Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;
.super Ljava/lang/Object;
.source "MPUSBOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private productId:I

.field private usbDeviceName:Ljava/lang/String;

.field private vendorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 18
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "usbDeviceName"    # Ljava/lang/String;
    .param p2, "vendorId"    # I
    .param p3, "productId"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->setUsbDeviceName(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p2}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->setVendorId(I)V

    .line 40
    invoke-virtual {p0, p3}, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->setProductId(I)V

    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public getProductId()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->productId:I

    return v0
.end method

.method public getUsbDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->usbDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getVendorId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->vendorId:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->usbDeviceName:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->vendorId:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->productId:I

    .line 66
    return-void
.end method

.method public setProductId(I)V
    .locals 0
    .param p1, "productId"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->productId:I

    .line 90
    return-void
.end method

.method public setUsbDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "usbDeviceName"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->usbDeviceName:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setVendorId(I)V
    .locals 0
    .param p1, "vendorId"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->vendorId:I

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->usbDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->vendorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/MPUSBOutputInfo;->productId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    return-void
.end method
