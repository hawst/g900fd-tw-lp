.class public Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
.super Lcom/sec/print/mobileprint/df/MFPMediaSize;
.source "MediaSizeInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>()V

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>()V

    .line 23
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/dm/MediaSizeInfo;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;
    .param p2, "leftMargin"    # I
    .param p3, "topMargin"    # I
    .param p4, "rightMargin"    # I
    .param p5, "bottoMargin"    # I
    .param p6, "length"    # I
    .param p7, "width"    # I

    .prologue
    .line 27
    invoke-direct/range {p0 .. p7}, Lcom/sec/print/mobileprint/df/MFPMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    .line 28
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/MFPMediaSize;->readFromParcel(Landroid/os/Parcel;)V

    .line 32
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/df/MFPMediaSize;->writeToParcel(Landroid/os/Parcel;)V

    .line 41
    return-void
.end method
