.class public Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;
.super Ljava/lang/Object;
.source "SPSSMBOutputStream.java"

# interfaces
.implements Lcom/sec/print/mobileprint/io/ISPSOutputStream;


# instance fields
.field isCancel:Z

.field isOccurError:Z

.field mSelectedPrinterDomain:Ljava/lang/String;

.field mSelectedPrinterIPAddress:Ljava/lang/String;

.field mSelectedPrinterPassword:Ljava/lang/String;

.field mSelectedPrinterShareName:Ljava/lang/String;

.field mSelectedPrinterUserName:Ljava/lang/String;

.field smbOutputStream:Ljcifs/smb/SmbFile;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "selectedPrinterDomain"    # Ljava/lang/String;
    .param p2, "selectedPrinterUserName"    # Ljava/lang/String;
    .param p3, "selectedPrinterPassword"    # Ljava/lang/String;
    .param p4, "selectedPrinterIPAddress"    # Ljava/lang/String;
    .param p5, "selectedPrinterShareName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isCancel:Z

    .line 22
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    .line 30
    iput-object p1, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterDomain:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterUserName:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterPassword:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterIPAddress:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterShareName:Ljava/lang/String;

    .line 36
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isCancel:Z

    .line 37
    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    .line 38
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isCancel:Z

    .line 44
    return-void
.end method

.method public close()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 50
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->smbOutputStream:Ljcifs/smb/SmbFile;

    if-nez v3, :cond_0

    .line 64
    :goto_0
    return v1

    .line 54
    :cond_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->smbOutputStream:Ljcifs/smb/SmbFile;

    invoke-virtual {v3}, Ljcifs/smb/SmbFile;->closePrintJob()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 64
    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    .line 59
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "close exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public isConnect()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public open()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 105
    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterDomain:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterDomain:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 107
    :cond_0
    iput-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterDomain:Ljava/lang/String;

    .line 109
    :cond_1
    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterUserName:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterUserName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 111
    :cond_2
    iput-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterUserName:Ljava/lang/String;

    .line 113
    :cond_3
    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterPassword:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterPassword:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    .line 115
    :cond_4
    iput-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterPassword:Ljava/lang/String;

    .line 120
    :cond_5
    new-instance v2, Ljcifs/smb/NtlmPasswordAuthentication;

    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterDomain:Ljava/lang/String;

    .line 121
    iget-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterUserName:Ljava/lang/String;

    .line 122
    iget-object v6, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterPassword:Ljava/lang/String;

    .line 119
    invoke-direct {v2, v4, v5, v6}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .local v2, "ntlm":Ljcifs/smb/NtlmPasswordAuthentication;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "smb://"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    iget-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterIPAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 132
    iget-object v5, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->mSelectedPrinterShareName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 130
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "location":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljcifs/smb/SmbFile;

    invoke-direct {v4, v1, v2}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    iput-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->smbOutputStream:Ljcifs/smb/SmbFile;

    .line 137
    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->smbOutputStream:Ljcifs/smb/SmbFile;

    const-string v5, "MobilePrintJob"

    invoke-virtual {v4, v5}, Ljcifs/smb/SmbFile;->openPrintJob(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_0
    return v3

    .line 139
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v3, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    .line 142
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "open exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 144
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public write([BI)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "length"    # I

    .prologue
    const/4 v1, 0x1

    .line 70
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    if-eqz v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->smbOutputStream:Ljcifs/smb/SmbFile;

    invoke-virtual {v2, p1, p2}, Ljcifs/smb/SmbFile;->writePrintData([BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isOccurError:Z

    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "write exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSSMBOutputStream;->isCancel:Z

    if-nez v2, :cond_0

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 86
    const/4 v1, 0x0

    goto :goto_0
.end method
