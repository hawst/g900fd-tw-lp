.class public Lcom/sec/print/mobileprint/dm/DeviceManagerService;
.super Landroid/app/Service;
.source "DeviceManagerService.java"


# static fields
.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.print.mobileprint"

.field public static final PORT_TYPE_ALL:I = 0x1

.field public static final PORT_TYPE_ONLY_NETWORK:I = 0x2

.field public static final PORT_TYPE_ONLY_USB:I = 0x3

.field private static final TAG:Ljava/lang/String; = "DeviceManagerService"

.field private static mediaSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/dm/MediaSizeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mediaTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

.field dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

.field private final mBinder:Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;

.field private xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaTypes:Ljava/util/List;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    .line 36
    sget-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaTypes:Ljava/util/List;

    const-string v1, "Normal"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaTypes:Ljava/util/List;

    const-string v1, "Thick"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaTypes:Ljava/util/List;

    const-string v1, "Thin"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "A4"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xdb3

    const/16 v7, 0x9b0

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "A5"

    const/16 v2, 0x39

    const/16 v3, 0x39

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x9b0

    const/16 v7, 0x6d4

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "A6"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x6d4

    const/16 v7, 0x4d8

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "JISB5"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xbdb

    const/16 v7, 0x865

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "Letter"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xce4

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "Legal"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x1068

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "Executive"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xc4e

    const/16 v7, 0x87f

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v8, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    const-string v1, "Folio"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xf3c

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    .line 27
    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    .line 28
    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    .line 167
    new-instance v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/dm/DeviceManagerService$1;-><init>(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mBinder:Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;

    .line 15
    return-void
.end method

.method private SPSMediaSizeToMediaSizeInfo(Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .locals 8
    .param p1, "spsMediaSize"    # Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    .prologue
    .line 437
    new-instance v0, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getMediaName()Ljava/lang/String;

    move-result-object v1

    .line 438
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getLeftMargin()I

    move-result v2

    .line 439
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getTopMargin()I

    move-result v3

    .line 440
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getRightMargin()I

    move-result v4

    .line 441
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getBottoMargin()I

    move-result v5

    .line 442
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getLength()I

    move-result v6

    .line 443
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getWidth()I

    move-result v7

    .line 437
    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/dm/MediaSizeInfo;-><init>(Ljava/lang/String;IIIIII)V

    return-object v0
.end method

.method static synthetic access$0(Lcom/sec/print/mobileprint/dm/DeviceManagerService;)Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/print/mobileprint/dm/DeviceManagerService;Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)Lcom/sec/print/mobileprint/dm/MediaSizeInfo;
    .locals 1

    .prologue
    .line 435
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->SPSMediaSizeToMediaSizeInfo(Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)Lcom/sec/print/mobileprint/dm/MediaSizeInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2()Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mediaSizes:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->mBinder:Lcom/sec/print/mobileprint/dm/IDeviceManagerService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    if-nez v0, :cond_1

    .line 63
    new-instance v0, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    if-nez v0, :cond_2

    .line 66
    new-instance v0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->doCreate()V

    .line 70
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    iget-object v0, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;->doDestroy()V

    .line 77
    iput-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmNetwork:Lcom/sec/print/mobileprint/dm/network/NetworkDeviceManager;

    .line 78
    iput-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->dmUSB:Lcom/sec/print/mobileprint/dm/usb/USBDeviceManager;

    .line 79
    iput-object v1, p0, Lcom/sec/print/mobileprint/dm/DeviceManagerService;->xmlDeviceManager:Lcom/sec/print/mobileprint/dm/XMLDeviceManager;

    .line 80
    return-void
.end method
