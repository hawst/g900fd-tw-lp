.class public Lcom/sec/print/mobileprint/pagedata/ImageData;
.super Ljava/lang/Object;
.source "ImageData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/pagedata/IPageData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;,
        Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;,
        Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/ImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cropping:I

.field private label:I

.field private outputMode:I

.field private realSize:I

.field private realSizeHeight:I

.field private realSizeWidth:I

.field private rotate:I

.field private rotatedCropAreaHeight:I

.field private rotatedCropAreaWidth:I

.field private rotatedCropAreaX:I

.field private rotatedCropAreaY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 337
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageData$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 87
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 88
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 90
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 91
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 92
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 93
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 96
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 97
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 98
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 100
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 129
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 131
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 133
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 134
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 135
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 136
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 137
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 139
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 140
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 141
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 142
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V
    .locals 2
    .param p1, "areaRotatedCropArea"    # Landroid/graphics/Rect;
    .param p2, "eRotate"    # Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 87
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 88
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 90
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 91
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 92
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 93
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 96
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 97
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 98
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 100
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 178
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 180
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 182
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_ON:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 183
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 184
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 185
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 186
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 188
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 189
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 190
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 191
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 192
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 87
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 88
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 90
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 91
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 92
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 93
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 96
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 97
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 98
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 100
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/pagedata/ImageData;->readFromParcel(Landroid/os/Parcel;)V

    .line 119
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V
    .locals 2
    .param p1, "eRotate"    # Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 87
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 88
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->ROTATE_0:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 90
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 91
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 92
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 93
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 96
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 97
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 98
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 100
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 153
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->OUTPUT_TYPE_FIT_TO_PAGE:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumOutputMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 155
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 157
    sget-object v0, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->CROPPING_OFF:Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/pagedata/ImageData$EnumCroppingMode;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 158
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 159
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 160
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 161
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 163
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 164
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 165
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 166
    iput v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 167
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public getCropping()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    return v0
.end method

.method public getLabel()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    return v0
.end method

.method public getOutputMode()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    return v0
.end method

.method public getRealSize()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    return v0
.end method

.method public getRealSizeHeight()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    return v0
.end method

.method public getRealSizeWidth()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    return v0
.end method

.method public getRotate()I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    return v0
.end method

.method public getRotatedCropArea()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 251
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    iget v2, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    iget v3, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    iget v4, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    iget v5, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public getRotatedCropAreaHeight()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    return v0
.end method

.method public getRotatedCropAreaWidth()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    return v0
.end method

.method public getRotatedCropAreaX()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    return v0
.end method

.method public getRotatedCropAreaY()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 325
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 335
    return-void
.end method

.method public setCropping(I)V
    .locals 0
    .param p1, "cropping"    # I

    .prologue
    .line 207
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    .line 208
    return-void
.end method

.method public setLabel(I)V
    .locals 0
    .param p1, "label"    # I

    .prologue
    .line 291
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    .line 292
    return-void
.end method

.method public setOutputMode(I)V
    .locals 0
    .param p1, "outputMode"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    .line 200
    return-void
.end method

.method public setRealSize(I)V
    .locals 0
    .param p1, "realSize"    # I

    .prologue
    .line 283
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    .line 284
    return-void
.end method

.method public setRealSizeHeight(I)V
    .locals 0
    .param p1, "realSizeHeight"    # I

    .prologue
    .line 279
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    .line 280
    return-void
.end method

.method public setRealSizeWidth(I)V
    .locals 0
    .param p1, "realSizeWidth"    # I

    .prologue
    .line 275
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    .line 276
    return-void
.end method

.method public setRotate(I)V
    .locals 0
    .param p1, "rotate"    # I

    .prologue
    .line 215
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    .line 216
    return-void
.end method

.method public setRotatedCropArea(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rotatedCropArea"    # Landroid/graphics/Rect;

    .prologue
    .line 256
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 257
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 258
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 259
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 260
    return-void
.end method

.method public setRotatedCropAreaHeight(I)V
    .locals 0
    .param p1, "rotatedCropAreaHeight"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    .line 248
    return-void
.end method

.method public setRotatedCropAreaWidth(I)V
    .locals 0
    .param p1, "rotatedCropAreaWidth"    # I

    .prologue
    .line 239
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    .line 240
    return-void
.end method

.method public setRotatedCropAreaX(I)V
    .locals 0
    .param p1, "rotatedCropAreaX"    # I

    .prologue
    .line 223
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    .line 224
    return-void
.end method

.method public setRotatedCropAreaY(I)V
    .locals 0
    .param p1, "rotatedCropAreaY"    # I

    .prologue
    .line 231
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    .line 232
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 303
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->outputMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 304
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->cropping:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 307
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaX:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 308
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaY:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 309
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 310
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->rotatedCropAreaHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 312
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 313
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 314
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->realSizeHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 316
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageData;->label:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 317
    return-void
.end method
