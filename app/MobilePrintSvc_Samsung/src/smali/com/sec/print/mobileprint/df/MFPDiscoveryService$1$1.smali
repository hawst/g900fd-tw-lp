.class Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;
.super Ljava/lang/Object;
.source "MFPDiscoveryService.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/DiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->getDeviceByMacAddress(Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;

.field private final synthetic val$arg0:Ljava/lang/String;

.field private final synthetic val$devices:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;

    iput-object p2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->val$arg0:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->val$devices:Ljava/util/List;

    .line 3558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public newDeviceFound(Ljava/lang/String;I)V
    .locals 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    .line 3566
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->this$1:Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;
    invoke-static {v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->access$0(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;)Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getMacAddressBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3567
    .local v1, "macAddress":Ljava/lang/String;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->val$arg0:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3568
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;->val$devices:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3574
    .end local v1    # "macAddress":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 3571
    :catch_0
    move-exception v0

    .line 3572
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public newDeviceFoundException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 3561
    return-void
.end method
