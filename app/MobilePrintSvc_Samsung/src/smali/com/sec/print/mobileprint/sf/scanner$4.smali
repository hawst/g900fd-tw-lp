.class Lcom/sec/print/mobileprint/sf/scanner$4;
.super Ljava/lang/Object;
.source "scanner.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/sf/scanner;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field statusText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/sec/print/mobileprint/sf/scanner;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/sf/scanner;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/sf/scanner$4;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->result:I

    invoke-virtual {p1, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner$4;->statusText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 146
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 147
    .local v0, "b":Landroid/widget/Button;
    const-string v1, "scanner"

    const-string v2, "asyncCancel: START"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$4;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-object v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->scanner:Lcom/sec/print/mobileprint/sf/ScannerLib;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/sf/ScannerLib;->asyncCancel()I

    .line 149
    const-string v1, "scanner"

    const-string v2, "asyncCancel: END"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    return-void
.end method
