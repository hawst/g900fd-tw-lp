.class public Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;
.super Lcom/sec/print/mobileprint/printoptionattribute/Layout;
.source "ImageSize.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private frameHeight:I

.field private frameMargin:I

.field private frameWidth:I

.field private imageSizeName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 5
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1
    .param p1, "frameWidth"    # I
    .param p2, "frameHeight"    # I
    .param p3, "frameMargin"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 28
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    .line 29
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    .line 30
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->readFromParcel(Landroid/os/Parcel;)V

    .line 15
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 1
    .param p1, "imageSizeName"    # Ljava/lang/String;
    .param p2, "frameWidth"    # I
    .param p3, "frameHeight"    # I
    .param p4, "frameMargin"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/Layout;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 20
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    .line 21
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    .line 22
    iput p4, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public getFrameHeight()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    return v0
.end method

.method public getFrameMargin()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    return v0
.end method

.method public getFrameWidth()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    return v0
.end method

.method public getImageSizeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    .line 86
    return-void
.end method

.method public setFrameHeight(I)V
    .locals 0
    .param p1, "frameHeight"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    .line 55
    return-void
.end method

.method public setFrameMargin(I)V
    .locals 0
    .param p1, "frameMargin"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    .line 63
    return-void
.end method

.method public setFrameWidth(I)V
    .locals 0
    .param p1, "frameWidth"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    .line 47
    return-void
.end method

.method public setImageSizeName(Ljava/lang/String;)V
    .locals 0
    .param p1, "imageSizeName"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->imageSizeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ImageSize;->frameMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    return-void
.end method
