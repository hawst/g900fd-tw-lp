.class public Lcom/sec/print/mobileprint/dm/XMLDeviceManager;
.super Ljava/lang/Object;
.source "XMLDeviceManager.java"


# instance fields
.field curDeviceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

.field curModelName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curModelName:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curDeviceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    .line 15
    return-void
.end method

.method private loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 3
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "modelName"    # Ljava/lang/String;

    .prologue
    .line 22
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curModelName:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    :try_start_0
    iput-object p2, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curModelName:Ljava/lang/String;

    .line 26
    const-string v2, "mobileprint_model_list.xml"

    invoke-virtual {p1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->loadCapability(Ljava/io/InputStream;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 27
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    iput-object v0, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curDeviceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .end local v0    # "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->curDeviceCapability:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    return-object v2

    .line 28
    :catch_0
    move-exception v1

    .line 30
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getLanguages(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/List;
    .locals 7
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/AssetManager;",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v1, "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 40
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_1

    .line 42
    const/4 v1, 0x0

    .line 55
    .end local v1    # "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-object v1

    .line 44
    .restart local v1    # "listLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getEmulation()Ljava/lang/String;

    move-result-object v4

    .line 45
    .local v4, "strPDLType":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 46
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, ","

    invoke-direct {v3, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .local v3, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 48
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "pdl":Ljava/lang/String;
    const-string v5, "^\\s+"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 50
    const-string v5, "\\s+$"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getMediaSizeList(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/AssetManager;",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 156
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_0

    .line 158
    const/4 v1, 0x0

    .line 173
    :goto_0
    return-object v1

    .line 161
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v4, "paperSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getPaperSize()Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "paperSize":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 164
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v5, v3, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .local v5, "st":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-nez v6, :cond_2

    .line 172
    .end local v5    # "st":Ljava/util/StringTokenizer;
    :cond_1
    invoke-static {v4}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->getSPSMediaList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 173
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    goto :goto_0

    .line 166
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    .restart local v5    # "st":Ljava/util/StringTokenizer;
    :cond_2
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 167
    .local v2, "paper":Ljava/lang/String;
    const-string v6, "^\\s+"

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 168
    const-string v6, "\\s+$"

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getMediaTypeList(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/AssetManager;",
            "Lcom/sec/print/mobileprint/dm/DeviceInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 179
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_1

    .line 181
    const/4 v2, 0x0

    .line 197
    :cond_0
    return-object v2

    .line 185
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 186
    .local v2, "paperTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getPaperType()Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "papers":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 188
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, ","

    invoke-direct {v4, v3, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .local v4, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 190
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "paper":Ljava/lang/String;
    const-string v5, "^\\s+"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    const-string v5, "\\s+$"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getModelListVersion(Landroid/content/res/AssetManager;)Ljava/lang/String;
    .locals 3
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 143
    const/4 v1, 0x0

    .line 145
    .local v1, "version":Ljava/lang/String;
    :try_start_0
    const-string v2, "mobileprint_model_list.xml"

    invoke-virtual {p1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->getModelListVersion(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 150
    :goto_0
    return-object v1

    .line 146
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public isPrinterColorModel(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 3
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 59
    const/4 v1, -0x1

    .line 60
    .local v1, "result":I
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 61
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_0

    .line 63
    const/4 v2, -0x1

    .line 74
    :goto_0
    return v2

    .line 66
    :cond_0
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->isColorMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    const/4 v1, 0x1

    :goto_1
    move v2, v1

    .line 74
    goto :goto_0

    .line 71
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isSupportA3(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 8
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 98
    const/4 v4, -0x1

    .line 99
    .local v4, "result":I
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 100
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_0

    .line 102
    const/4 v6, -0x1

    .line 128
    :goto_0
    return v6

    .line 106
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v3, "paperSizeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->getPaperSize()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "paperSize":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 109
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v5, v2, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .local v5, "st":Ljava/util/StringTokenizer;
    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v6

    if-nez v6, :cond_2

    .line 119
    .end local v5    # "st":Ljava/util/StringTokenizer;
    :cond_1
    const-string v6, "A3"

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 122
    const/4 v4, 0x1

    :goto_2
    move v6, v4

    .line 128
    goto :goto_0

    .line 111
    .restart local v5    # "st":Ljava/util/StringTokenizer;
    :cond_2
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "paper":Ljava/lang/String;
    const-string v6, "^\\s+"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 113
    const-string v6, "\\s+$"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 125
    .end local v1    # "paper":Ljava/lang/String;
    .end local v5    # "st":Ljava/util/StringTokenizer;
    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public isSupportDuplex(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 3
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 78
    const/4 v1, -0x1

    .line 79
    .local v1, "result":I
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 80
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_0

    .line 82
    const/4 v2, -0x1

    .line 93
    :goto_0
    return v2

    .line 85
    :cond_0
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;->isDuplex()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    const/4 v1, 0x1

    :goto_1
    move v2, v1

    .line 93
    goto :goto_0

    .line 90
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isSupportedPrinter(Landroid/content/res/AssetManager;Lcom/sec/print/mobileprint/dm/DeviceInfo;)I
    .locals 3
    .param p1, "assetMgr"    # Landroid/content/res/AssetManager;
    .param p2, "device"    # Lcom/sec/print/mobileprint/dm/DeviceInfo;

    .prologue
    .line 132
    const/4 v1, 0x1

    .line 133
    .local v1, "result":I
    invoke-virtual {p2}, Lcom/sec/print/mobileprint/dm/DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/print/mobileprint/dm/XMLDeviceManager;->loadDeviceInfo(Landroid/content/res/AssetManager;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v0

    .line 134
    .local v0, "deviceCapability":Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    if-nez v0, :cond_0

    .line 136
    const/4 v1, 0x0

    .line 138
    .end local v1    # "result":I
    :cond_0
    return v1
.end method
