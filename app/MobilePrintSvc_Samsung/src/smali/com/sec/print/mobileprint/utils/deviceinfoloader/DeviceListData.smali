.class Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;
.super Ljava/lang/Object;
.source "DeviceInfoLoader.java"


# instance fields
.field modelCapabilities:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;",
            ">;"
        }
    .end annotation
.end field

.field productLists:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field productListsWithVidPid:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productLists:Ljava/util/HashMap;

    .line 172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->modelCapabilities:Ljava/util/HashMap;

    .line 173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productListsWithVidPid:Ljava/util/HashMap;

    .line 174
    return-void
.end method


# virtual methods
.method public addDevice(Ljava/lang/String;Ljava/util/ArrayList;Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;Ljava/lang/String;)V
    .locals 1
    .param p1, "modelName"    # Ljava/lang/String;
    .param p3, "capability"    # Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .param p4, "pid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 206
    .local p2, "productNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productLists:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->modelCapabilities:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    if-eqz p4, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productListsWithVidPid:Ljava/util/HashMap;

    invoke-virtual {v0, p4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 6
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v3, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->modelCapabilities:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 179
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 180
    .local v0, "findKey":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 189
    :goto_0
    if-nez v0, :cond_2

    .line 191
    const/4 v3, 0x0

    .line 193
    :goto_1
    return-object v3

    .line 180
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 184
    move-object v0, v1

    .line 185
    goto :goto_0

    .line 193
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->modelCapabilities:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    goto :goto_1
.end method

.method public getProductList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productLists:Ljava/util/HashMap;

    return-object v0
.end method

.method public getProductListWithVidPidInfo()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productListsWithVidPid:Ljava/util/HashMap;

    return-object v0
.end method
