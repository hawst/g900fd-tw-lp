.class public Lcom/sec/print/mobileprint/df/MFPDevice;
.super Ljava/lang/Object;
.source "MFPDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;,
        Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;,
        Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

.field private faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

.field private host:Ljava/lang/String;

.field private macAddress:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private note:Ljava/lang/String;

.field private port:I

.field private productId:I

.field private scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

.field private vendorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDevice$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/df/MFPDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/df/MFPDevice;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    if-ne p0, p1, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v1

    .line 218
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 219
    goto :goto_0

    .line 220
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 221
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 222
    check-cast v0, Lcom/sec/print/mobileprint/df/MFPDevice;

    .line 223
    .local v0, "other":Lcom/sec/print/mobileprint/df/MFPDevice;
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 224
    goto :goto_0

    .line 225
    :cond_4
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 226
    goto :goto_0

    .line 227
    :cond_5
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 228
    goto :goto_0

    .line 229
    :cond_6
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 230
    iget-object v3, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 231
    goto :goto_0

    .line 232
    :cond_7
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 233
    goto :goto_0

    .line 234
    :cond_8
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 235
    iget-object v3, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 236
    goto :goto_0

    .line 237
    :cond_9
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 238
    goto :goto_0

    .line 239
    :cond_a
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 240
    iget-object v3, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 241
    goto :goto_0

    .line 242
    :cond_b
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 243
    goto :goto_0

    .line 244
    :cond_c
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 245
    iget-object v3, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 246
    goto :goto_0

    .line 247
    :cond_d
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 248
    goto/16 :goto_0

    .line 249
    :cond_e
    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    iget v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 250
    goto/16 :goto_0

    .line 251
    :cond_f
    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    iget v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 252
    goto/16 :goto_0

    .line 253
    :cond_10
    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    iget v4, v0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 254
    goto/16 :goto_0
.end method

.method public getDiscoveryType()Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    return-object v0
.end method

.method public getFaxType()Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    return v0
.end method

.method public getProductId()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    return v0
.end method

.method public getScanType()Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    return-object v0
.end method

.method public getVendorId()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 198
    const/16 v0, 0x1f

    .line 199
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 201
    .local v1, "result":I
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    if-nez v2, :cond_0

    move v2, v3

    .line 200
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 202
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 203
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 204
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 205
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 206
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 207
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    if-nez v4, :cond_6

    :goto_6
    add-int v1, v2, v3

    .line 208
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    add-int v1, v2, v3

    .line 209
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    add-int v1, v2, v3

    .line 210
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    add-int v1, v2, v3

    .line 211
    return v1

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->hashCode()I

    move-result v2

    goto :goto_0

    .line 202
    :cond_1
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->hashCode()I

    move-result v2

    goto :goto_1

    .line 203
    :cond_2
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->hashCode()I

    move-result v2

    goto :goto_2

    .line 204
    :cond_3
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 205
    :cond_4
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 206
    :cond_5
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 207
    :cond_6
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_6
.end method

.method public isFax()Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScan()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScanner()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    .line 165
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 174
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    .line 176
    :try_start_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 180
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    .line 185
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    goto :goto_0

    .line 171
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 172
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    goto :goto_1

    .line 177
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 178
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    goto :goto_2
.end method

.method public setDiscoveryType(Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;)V
    .locals 0
    .param p1, "discoveryType"    # Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    .line 141
    return-void
.end method

.method public setFaxType(Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;)V
    .locals 0
    .param p1, "faxType"    # Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 89
    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setMacAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setNote(Ljava/lang/String;)V
    .locals 0
    .param p1, "note"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    .line 73
    return-void
.end method

.method public setProductId(I)V
    .locals 0
    .param p1, "productId"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    .line 125
    return-void
.end method

.method public setScanType(Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;)V
    .locals 0
    .param p1, "scanType"    # Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 81
    return-void
.end method

.method public setScanner(Z)V
    .locals 1
    .param p1, "isScanner"    # Z

    .prologue
    .line 103
    if-eqz p1, :cond_0

    sget-object v0, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    :goto_0
    iput-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 104
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVendorId(I)V
    .locals 0
    .param p1, "vendorId"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    .line 133
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->host:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->note:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 147
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->port:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->macAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->productId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    iget v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->vendorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->scanType:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->faxType:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDevice;->discoveryType:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 193
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/df/MFPDevice;->writeToParcel(Landroid/os/Parcel;)V

    .line 194
    return-void
.end method
