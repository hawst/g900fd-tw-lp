.class public Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;
.super Ljava/lang/Object;
.source "DeviceInfoLoader.java"


# instance fields
.field deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

.field modelListVer:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static getFixedSamsungModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "modelName"    # Ljava/lang/String;

    .prologue
    .line 140
    const-string v0, ""

    .line 141
    .local v0, "fixedModelName":Ljava/lang/String;
    const-string v1, "Samsung"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_0
    const-string v1, "series"

    const-string v2, "Series"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 148
    const-string v1, "Series"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    const-string v1, "Samsung ML-2251NP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    const-string v1, "Samsung ML-3560"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 151
    const-string v1, "Samsung ML-4050 DMV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    const-string v1, ""

    const-string v2, "+ Series"

    invoke-static {v1, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " Series"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    :cond_0
    return-object v0

    .line 144
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Samsung "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getModelListVersion(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 124
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 126
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 127
    .local v3, "parser":Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;

    sget v5, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_VERSION_STRING:I

    invoke-direct {v2, v5}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;-><init>(I)V

    .line 128
    .local v2, "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    invoke-virtual {v3, p0, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 129
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->getVersion()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 135
    .local v4, "version":Ljava/lang/String;
    return-object v4

    .line 131
    .end local v2    # "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    .end local v3    # "parser":Ljavax/xml/parsers/SAXParser;
    .end local v4    # "version":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public static getModelNamefromPid(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "pid"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 86
    .local v4, "pidList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 88
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 89
    .local v3, "parser":Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;

    sget v5, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_MODELLIST_WITH_VIDPID:I

    invoke-direct {v2, v5}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;-><init>(I)V

    .line 90
    .local v2, "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    invoke-virtual {v3, p0, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 91
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->getPIDList()Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 96
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    return-object v5

    .line 92
    .end local v2    # "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    .end local v3    # "parser":Ljavax/xml/parsers/SAXParser;
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public static loadCapability(Ljava/io/InputStream;Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 10
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 102
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    .line 103
    .local v2, "factory":Ljavax/xml/parsers/SAXParserFactory;
    invoke-static {p1}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->getFixedSamsungModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "fixedModelName":Ljava/lang/String;
    const-string v7, "DeviceInfoLoader"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "LoadCapability : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    new-instance v4, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;

    invoke-direct {v4, v3}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;-><init>(Ljava/lang/String;)V

    .line 107
    .local v4, "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    :try_start_0
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v5

    .line 108
    .local v5, "parser":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v5, p0, v4}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 119
    .end local v5    # "parser":Ljavax/xml/parsers/SAXParser;
    :cond_0
    :goto_0
    return-object v6

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "eSAX":Lorg/xml/sax/SAXException;
    invoke-virtual {v1}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Found"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 111
    invoke-virtual {v4, v3}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v6

    goto :goto_0

    .line 115
    .end local v1    # "eSAX":Lorg/xml/sax/SAXException;
    :catch_1
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static loadModelList(Ljava/io/InputStream;)Ljava/util/HashMap;
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 69
    .local v3, "modelList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 71
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v4

    .line 72
    .local v4, "parser":Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;

    sget v5, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ONLY_MODEL_SERIES:I

    invoke-direct {v2, v5}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;-><init>(I)V

    .line 73
    .local v2, "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    invoke-virtual {v4, p0, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 74
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->getModelSerieList()Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 79
    return-object v3

    .line 75
    .end local v2    # "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    .end local v4    # "parser":Ljavax/xml/parsers/SAXParser;
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method


# virtual methods
.method public getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;
    .locals 2
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    if-nez v1, :cond_0

    .line 59
    const/4 v1, 0x0

    .line 62
    :goto_0
    return-object v1

    .line 61
    :cond_0
    invoke-static {p1}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->getFixedSamsungModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "fixedModelName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-virtual {v1, v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->getCapability(Ljava/lang/String;)Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceCapability;

    move-result-object v1

    goto :goto_0
.end method

.method public getModelNamefromPID(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "pid"    # Ljava/lang/String;

    .prologue
    .line 43
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    iget-object v0, v0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->productListsWithVidPid:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProductList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;->getProductList()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0
.end method

.method public load(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 30
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 32
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    :try_start_0
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 33
    .local v3, "parser":Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;

    sget v4, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->TYPE_LOAD_ALL:I

    invoke-direct {v2, v4}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;-><init>(I)V

    .line 34
    .local v2, "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    invoke-virtual {v3, p1, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 35
    invoke-virtual {v2}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;->getDeviceLists()Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->deviceListData:Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceListData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    return-void

    .line 36
    .end local v2    # "handler":Lcom/sec/print/mobileprint/utils/deviceinfoloader/ModelListHandler;
    .end local v3    # "parser":Ljavax/xml/parsers/SAXParser;
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method
