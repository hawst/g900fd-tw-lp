.class Lcom/sec/print/mobileprint/sf/scanner$3;
.super Ljava/lang/Object;
.source "scanner.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/sf/scanner;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field statusText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/sec/print/mobileprint/sf/scanner;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/sf/scanner;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    sget v0, Lcom/sec/print/mobileprint/sf/R$id;->result:I

    invoke-virtual {p1, v0}, Lcom/sec/print/mobileprint/sf/scanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->statusText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 125
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 126
    .local v0, "b":Landroid/widget/Button;
    iget-object v2, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-boolean v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v2, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    .line 127
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-boolean v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    if-eqz v1, :cond_1

    .line 128
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/sf/scanner;->startScanning()V

    .line 131
    :goto_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-boolean v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    if-eqz v1, :cond_2

    .line 132
    const-string v1, "Clear View"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :goto_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-object v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/sf/scanView;->invalidate()V

    .line 138
    return-void

    .line 126
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-object v1, v1, Lcom/sec/print/mobileprint/sf/scanner;->scanPreview:Lcom/sec/print/mobileprint/sf/scanView;

    iget-object v2, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->this$0:Lcom/sec/print/mobileprint/sf/scanner;

    iget-boolean v2, v2, Lcom/sec/print/mobileprint/sf/scanner;->draw:Z

    invoke-virtual {v1, v2}, Lcom/sec/print/mobileprint/sf/scanView;->updateImage(Z)V

    goto :goto_1

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/sec/print/mobileprint/sf/scanner$3;->statusText:Landroid/widget/TextView;

    const-string v2, "Ready to Scan"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const-string v1, "Scan"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
