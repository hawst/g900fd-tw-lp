.class public Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;
.super Ljava/lang/Object;
.source "NetworkOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field ipAddr:Ljava/lang/String;

.field isManual:Z

.field location:Ljava/lang/String;

.field portNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 18
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "portNum"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    .line 45
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->ipAddr:Ljava/lang/String;

    .line 46
    iput p2, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->portNum:I

    .line 47
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public getIpAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->ipAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getPortNum()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->portNum:I

    return v0
.end method

.method public isManual()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->ipAddr:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->portNum:I

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->location:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 109
    .local v0, "manual":I
    if-ne v0, v2, :cond_0

    .line 110
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    goto :goto_0
.end method

.method public setIpAddr(Ljava/lang/String;)V
    .locals 0
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->ipAddr:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->location:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setManual(Z)V
    .locals 0
    .param p1, "isManual"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    .line 80
    return-void
.end method

.method public setPortNum(I)V
    .locals 0
    .param p1, "portNum"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->portNum:I

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->ipAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->portNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->location:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-boolean v0, p0, Lcom/sec/print/mobileprint/printerinfo/NetworkOutputInfo;->isManual:Z

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
