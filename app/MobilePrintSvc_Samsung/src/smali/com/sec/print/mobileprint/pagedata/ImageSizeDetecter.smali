.class public Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;
.super Ljava/lang/Object;
.source "ImageSizeDetecter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field m_nChannel:I

.field m_nColorMode:I

.field m_nEx:I

.field m_nEy:I

.field m_nImageHeight:I

.field m_nImageWidth:I

.field m_nSx:I

.field m_nSy:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageWidth:I

    .line 41
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageHeight:I

    .line 42
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nChannel:I

    .line 43
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nColorMode:I

    .line 45
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSx:I

    .line 46
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSy:I

    .line 47
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEx:I

    .line 48
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEy:I

    .line 49
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "m_nImageWidth"    # I
    .param p2, "m_nImageHeight"    # I
    .param p3, "m_nChannel"    # I
    .param p4, "m_nColorMode"    # I

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageWidth:I

    .line 54
    iput p2, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageHeight:I

    .line 55
    iput p3, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nChannel:I

    .line 56
    iput p4, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nColorMode:I

    .line 58
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSx:I

    .line 59
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSy:I

    .line 60
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEx:I

    .line 61
    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEy:I

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->readFromParcel(Landroid/os/Parcel;)V

    .line 36
    return-void
.end method

.method public static readClassNameFromParcel(Landroid/os/Parcel;)Ljava/lang/String;
    .locals 1
    .param p0, "in"    # Landroid/os/Parcel;

    .prologue
    .line 115
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "simpleName":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public getEndX()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEx:I

    return v0
.end method

.method public getEndY()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEy:I

    return v0
.end method

.method public getStartX()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSx:I

    return v0
.end method

.method public getStartY()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSy:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageWidth:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageHeight:I

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nChannel:I

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nColorMode:I

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSx:I

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSy:I

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEx:I

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEy:I

    .line 111
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nImageHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nChannel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nColorMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSx:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nSy:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEx:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget v0, p0, Lcom/sec/print/mobileprint/pagedata/ImageSizeDetecter;->m_nEy:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    return-void
.end method
