.class public Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;
.super Ljava/lang/Object;
.source "SPSMediaSize.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bottoMargin:I

.field private height:I

.field private leftMargin:I

.field private mediaName:Ljava/lang/String;

.field private rightMargin:I

.field private topMargin:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->readFromParcel(Landroid/os/Parcel;)V

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;
    .param p2, "leftMargin"    # I
    .param p3, "topMargin"    # I
    .param p4, "rightMargin"    # I
    .param p5, "bottoMargin"    # I
    .param p6, "length"    # I
    .param p7, "width"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->mediaName:Ljava/lang/String;

    .line 24
    iput p7, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->width:I

    .line 25
    iput p6, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->height:I

    .line 26
    iput p3, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->topMargin:I

    .line 27
    iput p5, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->bottoMargin:I

    .line 28
    iput p2, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->leftMargin:I

    .line 29
    iput p4, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->rightMargin:I

    .line 30
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public getBottoMargin()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->bottoMargin:I

    return v0
.end method

.method public getLeftMargin()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->leftMargin:I

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->height:I

    return v0
.end method

.method public getMediaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->mediaName:Ljava/lang/String;

    return-object v0
.end method

.method public getRightMargin()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->rightMargin:I

    return v0
.end method

.method public getTopMargin()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->topMargin:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->width:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->mediaName:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->width:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->height:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->topMargin:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->bottoMargin:I

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->leftMargin:I

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->rightMargin:I

    .line 106
    return-void
.end method

.method public setBottoMargin(I)V
    .locals 0
    .param p1, "bottoMargin"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->bottoMargin:I

    .line 70
    return-void
.end method

.method public setLeftMargin(I)V
    .locals 0
    .param p1, "leftMargin"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->leftMargin:I

    .line 78
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->height:I

    .line 54
    return-void
.end method

.method public setMediaName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaName"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->mediaName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setRightMargin(I)V
    .locals 0
    .param p1, "rightMargin"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->rightMargin:I

    .line 86
    return-void
.end method

.method public setTopMargin(I)V
    .locals 0
    .param p1, "topMargin"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->topMargin:I

    .line 62
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->width:I

    .line 46
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->mediaName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->topMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->bottoMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->leftMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget v0, p0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->rightMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->writeToParcel(Landroid/os/Parcel;)V

    .line 115
    return-void
.end method
