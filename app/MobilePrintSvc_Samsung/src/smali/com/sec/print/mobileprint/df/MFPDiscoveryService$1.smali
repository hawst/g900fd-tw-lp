.class Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;
.super Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;
.source "MFPDiscoveryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/MFPDiscoveryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    .line 3226
    invoke-direct {p0}, Lcom/sec/print/mobileprint/df/IDiscoveryService$Stub;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;)Lcom/sec/print/mobileprint/df/MFPDiscoveryService;
    .locals 1

    .prologue
    .line 3226
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    return-object v0
.end method


# virtual methods
.method public IsCollateSuported(Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3487
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3488
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsCollateSuportedBySNMP(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3490
    :goto_0
    return v1

    .line 3489
    :catch_0
    move-exception v0

    .line 3490
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public IsConfidentialPrintAvailable(Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3466
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3467
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsConfidentialPrintAvailableBySNMP(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3469
    :goto_0
    return v1

    .line 3468
    :catch_0
    move-exception v0

    .line 3469
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public IsDeviceSamsungFax(Ljava/lang/String;)I
    .locals 6
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3507
    :try_start_0
    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3508
    iget-object v4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v5, 0x2

    invoke-virtual {v4, p1, v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v3

    .line 3509
    .local v3, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v3, :cond_1

    .line 3510
    const-string v4, "modelName"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3511
    .local v2, "modelName":Ljava/lang/String;
    const-string v4, "isFax"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 3513
    .local v1, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "samsung"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 3514
    const/4 v4, 0x1

    .line 3522
    .end local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return v4

    .line 3516
    .restart local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .restart local v2    # "modelName":Ljava/lang/String;
    .restart local v3    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 3519
    .end local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v0

    .line 3520
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3522
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v4, -0x1

    goto :goto_0
.end method

.method public IsDeviceSamsungPrinter(Ljava/lang/String;)I
    .locals 5
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3353
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3354
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v1

    .line 3355
    .local v1, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v1, :cond_0

    .line 3356
    const-string v3, "modelName"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3357
    .local v0, "modelName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 3358
    const/4 v2, 0x1

    .line 3366
    .end local v0    # "modelName":Ljava/lang/String;
    .end local v1    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    :goto_0
    return v2

    .line 3364
    :catch_0
    move-exception v2

    .line 3366
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public IsDeviceSamsungScanner(Ljava/lang/String;)I
    .locals 5
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3371
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3372
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v1

    .line 3373
    .local v1, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v1, :cond_1

    .line 3374
    const-string v3, "modelName"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3382
    .local v0, "modelName":Ljava/lang/String;
    const-string v3, "isScan"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 3383
    .local v2, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 3384
    const/4 v3, 0x1

    .line 3391
    .end local v0    # "modelName":Ljava/lang/String;
    .end local v1    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :goto_0
    return v3

    .line 3386
    .restart local v0    # "modelName":Ljava/lang/String;
    .restart local v1    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v2    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 3389
    .end local v0    # "modelName":Ljava/lang/String;
    .end local v1    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :catch_0
    move-exception v3

    .line 3391
    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public IsPrinterAlive(Ljava/lang/String;)I
    .locals 2
    .param p1, "printerHost"    # Ljava/lang/String;

    .prologue
    .line 3344
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3345
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsPrinterAlive(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3347
    :goto_0
    return v1

    .line 3346
    :catch_0
    move-exception v0

    .line 3347
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public IsPrinterColorModel(Ljava/lang/String;)I
    .locals 2
    .param p1, "printerHost"    # Ljava/lang/String;

    .prologue
    .line 3335
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3336
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->IsPrinterColorModelBySNMP(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3338
    :goto_0
    return v1

    .line 3337
    :catch_0
    move-exception v0

    .line 3338
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public SetCommunityName(Ljava/lang/String;)V
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3501
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->SetCommunityName(Ljava/lang/String;)V

    .line 3502
    return-void
.end method

.method public Start(Ljava/util/List;I)I
    .locals 4
    .param p2, "discoveryType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .local p1, "vendors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 3229
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$0(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/util/List;)V

    .line 3230
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$1(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;I)V

    .line 3233
    :try_start_0
    const-string v1, "MFPDiscoveryService"

    const-string v3, "Start SNMP"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3234
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSNMPDiscovery()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3240
    :goto_0
    :try_start_1
    const-string v1, "MFPDiscoveryService"

    const-string v3, "Start mDNS"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3241
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    if-ne p2, v2, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartmDNSDiscovery(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3247
    :goto_2
    :try_start_2
    const-string v1, "MFPDiscoveryService"

    const-string v3, "Start WFD"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3248
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartWFDDiscovery()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 3253
    :goto_3
    return v2

    .line 3235
    :catch_0
    move-exception v0

    .line 3236
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 3241
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 3242
    :catch_1
    move-exception v0

    .line 3243
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 3249
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 3250
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public Stop()I
    .locals 3

    .prologue
    .line 3257
    const-string v1, "MFPDiscoveryService"

    const-string v2, "Stop()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3260
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopWFDDiscovery()V

    .line 3261
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopmDNSDiscovery()V

    .line 3262
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StopSNMPDiscovery()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3267
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 3263
    :catch_0
    move-exception v0

    .line 3264
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3265
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDeviceByMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3546
    const-string v5, "MFPDiscoveryService"

    const-string v6, "getDeviceByMacAddress"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3548
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 3550
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3552
    new-instance v1, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;

    const/4 v5, 0x5

    const/16 v6, 0x400

    invoke-direct {v1, v5, v6}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;-><init>(II)V

    .line 3554
    .local v1, "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->communityName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$8(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp2(Ljava/lang/String;)V

    .line 3555
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3configired:Z
    invoke-static {v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$9(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3556
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->snmpV3contextName:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$10(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->enableSnmp3(Ljava/lang/String;)V

    .line 3558
    :cond_0
    new-instance v5, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;

    invoke-direct {v5, p0, p1, v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1$1;-><init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1, v5}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->addDiscoveryListener(Lcom/sec/print/mobileprint/df/DiscoveryListener;)V

    .line 3576
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3580
    const/16 v3, 0x14

    .local v3, "i":I
    move v4, v3

    .line 3581
    .end local v3    # "i":I
    .local v4, "i":I
    :goto_0
    add-int/lit8 v3, v4, -0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    if-gtz v4, :cond_2

    .line 3587
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;->stop()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3593
    :goto_1
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 3594
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 3599
    .end local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .end local v3    # "i":I
    :goto_2
    return-object v5

    .line 3582
    .restart local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .restart local v3    # "i":I
    :cond_2
    const-wide/16 v5, 0x1f4

    :try_start_3
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    .line 3583
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v5

    if-eqz v5, :cond_1

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_0

    .line 3589
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 3590
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 3596
    .end local v1    # "discoveryInstance":Lcom/sec/print/mobileprint/df/DiscoveryBroadcast;
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v3    # "i":I
    :catch_1
    move-exception v2

    .line 3597
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 3599
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public getDeviceLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3605
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3606
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # invokes: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceLocationBySNMP(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$11(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3610
    :goto_0
    return-object v1

    .line 3607
    :catch_0
    move-exception v0

    .line 3608
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3610
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ipAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3694
    const/4 v1, 0x0

    .line 3696
    .local v1, "macAddress":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3697
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getMacAddressBySNMP(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3701
    :goto_0
    return-object v1

    .line 3698
    :catch_0
    move-exception v0

    .line 3699
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getDeviceMode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3435
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3436
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceModeBySNMP(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3438
    :goto_0
    return-object v1

    .line 3437
    :catch_0
    move-exception v0

    .line 3438
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3396
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3397
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v2

    .line 3398
    .local v2, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_0

    .line 3399
    const-string v3, "modelName"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3400
    .local v1, "modelName":Ljava/lang/String;
    const-string v3, "hostName"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3401
    .local v0, "hostName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 3405
    .end local v0    # "hostName":Ljava/lang/String;
    .end local v1    # "modelName":Ljava/lang/String;
    .end local v2    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-object v3

    .line 3403
    :catch_0
    move-exception v3

    .line 3405
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getDuplex(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "printerHost"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3410
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3411
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDuplexBySNMP(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3413
    :goto_0
    return-object v1

    .line 3412
    :catch_0
    move-exception v0

    .line 3413
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFaxType(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3528
    :try_start_0
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3529
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;

    move-result-object v2

    .line 3530
    .local v2, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_1

    .line 3531
    const-string v3, "isFax"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 3532
    .local v1, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    if-eqz v1, :cond_0

    .line 3533
    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 3541
    .end local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v2    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-object v3

    .line 3535
    .restart local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .restart local v2    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    const-string v3, "NoFax"

    goto :goto_0

    .line 3538
    .end local v1    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v2    # "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v0

    .line 3539
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3541
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getFaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3496
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getFaxes()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIppDevices(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3421
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # invokes: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getCupsPrinters(Ljava/lang/String;)Ljava/util/List;
    invoke-static {v2, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$6(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 3422
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3423
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # invokes: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getIppPrinter(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice;
    invoke-static {v2, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$7(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDevice;

    move-result-object v0

    .line 3424
    .local v0, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3429
    .end local v0    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .end local v1    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 3427
    :catch_0
    move-exception v2

    .line 3429
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getJobTable(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3476
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3477
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getJobTableBySNMP(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3479
    :goto_0
    return-object v1

    .line 3478
    :catch_0
    move-exception v0

    .line 3479
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLanguages(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "printerHost"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3318
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3319
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getLanguageBySNMP(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3321
    :goto_0
    return-object v1

    .line 3320
    :catch_0
    move-exception v0

    .line 3321
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMediaSizes(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "printerHost"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPMediaSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3326
    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaSizes:Ljava/util/List;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$4()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMediaTypes(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "printerHost"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3330
    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->mediaTypes:Ljava/util/List;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$5()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNWDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3708
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3709
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDeviceProperties(Ljava/lang/String;I)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3713
    :goto_0
    return-object v1

    .line 3710
    :catch_0
    move-exception v0

    .line 3711
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3713
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPrinterStatus(Ljava/lang/String;)Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    .locals 6
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3445
    :try_start_0
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3446
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v5, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinterStatusBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3447
    .local v3, "status":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v4

    .line 3458
    .end local v3    # "status":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 3449
    .restart local v3    # "status":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v5, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinterAlertsBySNMP(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 3450
    .local v0, "alerts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    move-object v1, v4

    .line 3451
    goto :goto_0

    .line 3452
    :cond_1
    new-instance v1, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;

    invoke-direct {v1}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;-><init>()V

    .line 3453
    .local v1, "deviceStatus":Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    invoke-virtual {v1, v3}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setStatus(Ljava/lang/String;)V

    .line 3454
    invoke-virtual {v1, v0}, Lcom/sec/print/mobileprint/df/MFPDeviceStatus;->setDetectedErrorStates(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3457
    .end local v0    # "alerts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "deviceStatus":Lcom/sec/print/mobileprint/df/MFPDeviceStatus;
    .end local v3    # "status":Ljava/lang/String;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    move-object v1, v4

    .line 3458
    goto :goto_0
.end method

.method public getPrinters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3309
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getPrinters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getScanners()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3313
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getScanners()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3616
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3617
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # invokes: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSerialNumberBySNMP(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$12(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3621
    :goto_0
    return-object v1

    .line 3618
    :catch_0
    move-exception v0

    .line 3619
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3621
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSmbDevices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3291
    monitor-enter p0

    .line 3292
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3293
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->getDevices()Ljava/util/List;

    move-result-object v0

    monitor-exit p0

    .line 3295
    :goto_0
    return-object v0

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    .line 3291
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSmbState()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3300
    monitor-enter p0

    .line 3301
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3302
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->getState()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    monitor-exit p0

    .line 3304
    :goto_0
    return v0

    :cond_0
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    .line 3300
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getUSBDeviceProperties(II)Ljava/util/Map;
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3720
    :try_start_0
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v1, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDevicePropertiesByUSB(II)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3724
    :goto_0
    return-object v1

    .line 3721
    :catch_0
    move-exception v0

    .line 3722
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3724
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUsbDevices()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3638
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3639
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_0

    .line 3640
    iget-object v6, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const-string v7, "usb"

    invoke-virtual {v6, v7}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbManager;

    .line 3641
    .local v3, "manager":Landroid/hardware/usb/UsbManager;
    if-eqz v3, :cond_0

    .line 3642
    invoke-virtual {v3}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v2

    .line 3643
    .local v2, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3644
    .local v1, "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 3653
    .end local v1    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .end local v2    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    .end local v3    # "manager":Landroid/hardware/usb/UsbManager;
    :cond_0
    return-object v4

    .line 3645
    .restart local v1    # "deviceIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/usb/UsbDevice;>;"
    .restart local v2    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    .restart local v3    # "manager":Landroid/hardware/usb/UsbManager;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/usb/UsbDevice;

    .line 3646
    .local v5, "usbSevice":Landroid/hardware/usb/UsbDevice;
    new-instance v0, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 3647
    .local v0, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/print/mobileprint/df/MFPDevice;->setProductId(I)V

    .line 3648
    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/sec/print/mobileprint/df/MFPDevice;->setVendorId(I)V

    .line 3649
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isUSBDeviceFax(II)Z
    .locals 3
    .param p1, "vid"    # I
    .param p2, "pid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3668
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDevicePropertiesByUSB(II)Ljava/util/Map;

    move-result-object v1

    .line 3669
    .local v1, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v1, :cond_0

    .line 3670
    const-string v2, "isFax"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 3671
    .local v0, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    if-eqz v0, :cond_0

    .line 3672
    const/4 v2, 0x1

    .line 3675
    .end local v0    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isUSBDeviceScaner(II)Z
    .locals 4
    .param p1, "vid"    # I
    .param p2, "pid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3680
    iget-object v3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v3, p1, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getDevicePropertiesByUSB(II)Ljava/util/Map;

    move-result-object v0

    .line 3681
    .local v0, "properties":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v0, :cond_0

    .line 3683
    const-string v3, "isScan"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 3686
    .local v1, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    sget-object v3, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    if-ne v1, v3, :cond_0

    const/4 v2, 0x1

    .line 3689
    .end local v1    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :cond_0
    return v2
.end method

.method public setSnmpV3Settings(Ljava/lang/String;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;Ljava/lang/String;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;
    .param p4, "arg3"    # Ljava/lang/String;
    .param p5, "arg4"    # Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;
    .param p6, "arg5"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3627
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$13(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V

    .line 3628
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$14(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V

    .line 3629
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p3}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$15(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SnmpAuthProtocol;)V

    .line 3630
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p4}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$16(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V

    .line 3631
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p5}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$17(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SnmpPrivProtocol;)V

    .line 3632
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-static {v0, p6}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$18(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)V

    .line 3633
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$19(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Z)V

    .line 3634
    return-void
.end method

.method public startSmb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "domain"    # Ljava/lang/String;
    .param p3, "user"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 3271
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    monitor-enter v1

    .line 3272
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3273
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    new-instance v2, Lcom/sec/print/mobileprint/df/SmbDiscovery;

    invoke-direct {v2}, Lcom/sec/print/mobileprint/df/SmbDiscovery;-><init>()V

    invoke-static {v0, v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$3(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SmbDiscovery;)V

    .line 3274
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->startDiscovery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3271
    :cond_0
    monitor-exit v1

    .line 3277
    const/4 v0, 0x1

    return v0

    .line 3271
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopSmb()I
    .locals 3

    .prologue
    .line 3281
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    monitor-enter v1

    .line 3282
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3283
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->smbDiscovery:Lcom/sec/print/mobileprint/df/SmbDiscovery;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$2(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;)Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->stopDiscovery()V

    .line 3284
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$3(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Lcom/sec/print/mobileprint/df/SmbDiscovery;)V

    .line 3281
    :cond_0
    monitor-exit v1

    .line 3287
    const/4 v0, 0x1

    return v0

    .line 3281
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public wakeDeviceUp(Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3659
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->StartSnmp()V

    .line 3660
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$1;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->wakeDeviceUpBySNMP(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 3663
    :goto_0
    return v0

    .line 3661
    :catch_0
    move-exception v0

    .line 3663
    const/4 v0, -0x1

    goto :goto_0
.end method
