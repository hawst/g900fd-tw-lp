.class public Lcom/sec/print/mobileprint/io/ImageController;
.super Ljava/lang/Object;
.source "ImageController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;
    }
.end annotation


# static fields
.field static final ERROR:I = 0x1

.field static final ERROR_OUT_OF_MEMORY:I = 0x2

.field static final SUCCESS:I


# instance fields
.field m_bitmapData:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    .line 18
    return-void
.end method

.method static calcDestTopFromScaledBandImage(IID)I
    .locals 4
    .param p0, "nOutputTop"    # I
    .param p1, "nSrcTop"    # I
    .param p2, "scaleRate"    # D

    .prologue
    .line 737
    int-to-double v0, p0

    int-to-double v2, p1

    mul-double/2addr v2, p2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method static calcSrcBottom(ID)D
    .locals 4
    .param p0, "nDestBottom"    # I
    .param p1, "scaleRate"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 700
    cmpl-double v2, p1, v0

    if-nez v2, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-wide v0

    .line 705
    :cond_1
    if-eqz p0, :cond_0

    .line 710
    int-to-double v0, p0

    div-double/2addr v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    goto :goto_0
.end method

.method static calcSrcTop(ID)D
    .locals 8
    .param p0, "nDestTop"    # I
    .param p1, "scaleRate"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 715
    cmpl-double v4, p1, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 731
    :cond_0
    :goto_0
    return-wide v0

    .line 720
    :cond_1
    if-nez p0, :cond_2

    move-wide v0, v2

    .line 722
    goto :goto_0

    .line 725
    :cond_2
    int-to-double v4, p0

    div-double/2addr v4, p1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v0, v4, v6

    .line 726
    .local v0, "result":D
    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    move-wide v0, v2

    .line 728
    goto :goto_0
.end method

.method public static downSampling(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 11
    .param p0, "orgFilePath"    # Ljava/lang/String;
    .param p1, "destFilePath"    # Ljava/lang/String;
    .param p2, "nDownSamplingValue"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 837
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 838
    .local v3, "option":Landroid/graphics/BitmapFactory$Options;
    iput p2, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 839
    iput-boolean v8, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 841
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v9, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 842
    invoke-static {p0, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 844
    .local v1, "destBitmap":Landroid/graphics/Bitmap;
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 845
    .local v0, "compressFormat":Landroid/graphics/Bitmap$CompressFormat;
    iget-object v9, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v10, "image/jpeg"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_1

    .line 846
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 857
    :goto_0
    const/4 v4, 0x0

    .line 859
    .local v4, "outStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 860
    .local v6, "tempFolderPath":Ljava/io/File;
    if-nez v6, :cond_5

    .line 880
    .end local v6    # "tempFolderPath":Ljava/io/File;
    :cond_0
    :goto_1
    return v7

    .line 847
    .end local v4    # "outStream":Ljava/io/OutputStream;
    :cond_1
    iget-object v9, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v10, "image/png"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_2

    .line 848
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 849
    goto :goto_0

    :cond_2
    iget-object v9, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v10, "image/bmp"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_3

    .line 850
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 851
    goto :goto_0

    :cond_3
    iget-object v9, v3, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    const-string v10, "image/gif"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_4

    .line 852
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 853
    goto :goto_0

    .line 854
    :cond_4
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0

    .line 863
    .restart local v4    # "outStream":Ljava/io/OutputStream;
    .restart local v6    # "tempFolderPath":Ljava/io/File;
    :cond_5
    :try_start_1
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_6

    .line 865
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 870
    :cond_6
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .end local v4    # "outStream":Ljava/io/OutputStream;
    .local v5, "outStream":Ljava/io/OutputStream;
    move-object v4, v5

    .line 877
    .end local v5    # "outStream":Ljava/io/OutputStream;
    .end local v6    # "tempFolderPath":Ljava/io/File;
    .restart local v4    # "outStream":Ljava/io/OutputStream;
    :goto_2
    const/16 v7, 0x64

    invoke-virtual {v1, v0, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 878
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    move v7, v8

    .line 880
    goto :goto_1

    .line 872
    :catch_0
    move-exception v2

    .line 874
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method public static getImageInfo(Ljava/lang/String;[I[I[I)Z
    .locals 7
    .param p0, "fileBitmapPath"    # Ljava/lang/String;
    .param p1, "width"    # [I
    .param p2, "height"    # [I
    .param p3, "bitCount"    # [I

    .prologue
    const/16 v6, 0x20

    const/16 v5, 0x10

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 223
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 224
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 225
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 227
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v3, p1, v1

    .line 228
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v3, p2, v1

    .line 231
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 233
    const-string v2, "ImageController"

    const-string v3, "ERROR - options.outWidth == -1\n"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :goto_0
    return v1

    .line 247
    :cond_0
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v3, v4, :cond_1

    .line 249
    aput v6, p3, v1

    :goto_1
    move v1, v2

    .line 268
    goto :goto_0

    .line 251
    :cond_1
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    if-ne v3, v4, :cond_2

    .line 253
    const/16 v3, 0x8

    aput v3, p3, v1

    goto :goto_1

    .line 255
    :cond_2
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    if-ne v3, v4, :cond_3

    .line 257
    aput v5, p3, v1

    goto :goto_1

    .line 259
    :cond_3
    iget-object v3, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v3, v4, :cond_4

    .line 261
    aput v5, p3, v1

    goto :goto_1

    .line 265
    :cond_4
    aput v6, p3, v1

    goto :goto_1
.end method

.method static getScaleRate(JJJJ)D
    .locals 8
    .param p0, "lPaperWidth"    # J
    .param p2, "lPaperHeight"    # J
    .param p4, "lImageWidth"    # J
    .param p6, "lImageHeight"    # J

    .prologue
    .line 692
    long-to-double v4, p0

    long-to-double v6, p4

    div-double v2, v4, v6

    .line 693
    .local v2, "lWidthRate":D
    long-to-double v4, p2

    long-to-double v6, p6

    div-double v0, v4, v6

    .line 695
    .local v0, "lHeightRate":D
    cmpl-double v4, v2, v0

    if-lez v4, :cond_0

    .end local v0    # "lHeightRate":D
    :goto_0
    return-wide v0

    .restart local v0    # "lHeightRate":D
    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static getScaledBandedPixelData(Ljava/lang/String;[IIIID)I
    .locals 18
    .param p0, "fileBitmapPath"    # Ljava/lang/String;
    .param p1, "pixelsOutput"    # [I
    .param p2, "nStartPosYOnDestImage"    # I
    .param p3, "nDestHeight"    # I
    .param p4, "nBandHeightStep"    # I
    .param p5, "scaleRate"    # D

    .prologue
    .line 282
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 283
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 285
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v15, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 286
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 288
    .local v16, "srcBitmap":Landroid/graphics/Bitmap;
    move/from16 v0, p2

    move-wide/from16 v1, p5

    invoke-static {v0, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcTop(ID)D

    move-result-wide v4

    double-to-int v14, v4

    .line 289
    .local v14, "nSrcTop":I
    add-int v4, p2, p3

    move-wide/from16 v0, p5

    invoke-static {v4, v0, v1}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcBottom(ID)D

    move-result-wide v4

    double-to-int v4, v4

    sub-int v13, v4, v14

    .line 290
    .local v13, "nSrcHeight":I
    add-int v4, v14, v13

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 292
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int v13, v4, v14

    .line 296
    :cond_0
    const/4 v4, 0x0

    .line 297
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 295
    move-object/from16 v0, v16

    invoke-static {v0, v4, v14, v5, v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 298
    .local v17, "tempBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 299
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 302
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    mul-double v4, v4, p5

    double-to-int v4, v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v5, v5

    mul-double v5, v5, p5

    double-to-int v5, v5

    .line 303
    const/4 v6, 0x1

    .line 301
    move-object/from16 v0, v17

    invoke-static {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 304
    .local v3, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->recycle()V

    .line 305
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 307
    move/from16 v0, p2

    move-wide/from16 v1, p5

    invoke-static {v0, v14, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcDestTopFromScaledBandImage(IID)I

    move-result v8

    .line 308
    .local v8, "nResultTopPos":I
    move/from16 v10, p4

    .line 309
    .local v10, "nResultHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v8

    if-le v10, v4, :cond_1

    .line 311
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    sub-int v10, v4, v8

    .line 316
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 317
    const/4 v7, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    move-object/from16 v4, p1

    .line 316
    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 318
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 319
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 338
    const/4 v4, 0x0

    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "nResultTopPos":I
    .end local v10    # "nResultHeight":I
    .end local v13    # "nSrcHeight":I
    .end local v14    # "nSrcTop":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "tempBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return v4

    .line 321
    .restart local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "nResultTopPos":I
    .restart local v10    # "nResultHeight":I
    .restart local v13    # "nSrcHeight":I
    .restart local v14    # "nSrcTop":I
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "tempBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v12

    .line 323
    .local v12, "ex":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v4, "ImageController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error1 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const/4 v4, 0x2

    goto :goto_0

    .line 326
    .end local v12    # "ex":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v11

    .line 328
    .local v11, "e":Ljava/lang/Exception;
    const-string v4, "ImageController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error2 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 329
    const/4 v4, 0x1

    goto :goto_0

    .line 332
    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "nResultTopPos":I
    .end local v10    # "nResultHeight":I
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v13    # "nSrcHeight":I
    .end local v14    # "nSrcTop":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "tempBitmap":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v12

    .line 334
    .restart local v12    # "ex":Ljava/lang/OutOfMemoryError;
    const-string v4, "ImageController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error3 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const/4 v4, 0x2

    goto :goto_0
.end method

.method public static getScaledBandedPixelDataOnSplittedImage(Ljava/lang/String;[I[I[IIIID)I
    .locals 17
    .param p0, "fileBitmapPath"    # Ljava/lang/String;
    .param p1, "pixelsOutput"    # [I
    .param p2, "widthOutput"    # [I
    .param p3, "heightOutput"    # [I
    .param p4, "nStartPosYOnDestImage"    # I
    .param p5, "nDestHeight"    # I
    .param p6, "nBandHeightStep"    # I
    .param p7, "scaleRate"    # D

    .prologue
    .line 354
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 355
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 357
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v15, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 358
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 359
    .local v16, "srcBitmap":Landroid/graphics/Bitmap;
    if-nez v16, :cond_0

    .line 361
    const-string v4, "ImageController"

    const-string v5, "srcBitmap == null"

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v4, "ImageController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FilePath = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    const-string v4, "ImageController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Load File : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    move/from16 v0, p4

    move-wide/from16 v1, p7

    invoke-static {v0, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcTop(ID)D

    move-result-wide v4

    double-to-int v14, v4

    .line 367
    .local v14, "nSrcTop":I
    add-int v4, p4, p5

    move-wide/from16 v0, p7

    invoke-static {v4, v0, v1}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcBottom(ID)D

    move-result-wide v4

    double-to-int v4, v4

    sub-int v13, v4, v14

    .line 368
    .local v13, "nSrcHeight":I
    add-int v4, v14, v13

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 370
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int v13, v4, v14

    .line 374
    :cond_1
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    mul-double v4, v4, p7

    double-to-int v4, v4

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v5, v5

    mul-double v5, v5, p7

    double-to-int v5, v5

    .line 375
    const/4 v6, 0x1

    .line 373
    move-object/from16 v0, v16

    invoke-static {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 376
    .local v3, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 377
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 379
    move/from16 v0, p4

    move-wide/from16 v1, p7

    invoke-static {v0, v14, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcDestTopFromScaledBandImage(IID)I

    move-result v8

    .line 380
    .local v8, "nResultTopPos":I
    move/from16 v10, p6

    .line 381
    .local v10, "nResultHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v8

    if-le v10, v4, :cond_2

    .line 383
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    sub-int v10, v4, v8

    .line 388
    :cond_2
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 389
    const/4 v7, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    move-object/from16 v4, p1

    .line 388
    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 390
    const/4 v4, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    aput v5, p2, v4

    .line 391
    const/4 v4, 0x0

    aput v10, p3, v4

    .line 392
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 393
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 412
    const/4 v4, 0x0

    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "nResultTopPos":I
    .end local v10    # "nResultHeight":I
    .end local v13    # "nSrcHeight":I
    .end local v14    # "nSrcTop":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return v4

    .line 395
    .restart local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "nResultTopPos":I
    .restart local v10    # "nResultHeight":I
    .restart local v13    # "nSrcHeight":I
    .restart local v14    # "nSrcTop":I
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v12

    .line 397
    .local v12, "ex":Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v4, "ERROR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error4 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const/4 v4, 0x2

    goto :goto_0

    .line 400
    .end local v12    # "ex":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v11

    .line 402
    .local v11, "e":Ljava/lang/Exception;
    const-string v4, "ERROR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error5 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 403
    const/4 v4, 0x1

    goto :goto_0

    .line 406
    .end local v3    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "nResultTopPos":I
    .end local v10    # "nResultHeight":I
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v13    # "nSrcHeight":I
    .end local v14    # "nSrcTop":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "srcBitmap":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v12

    .line 408
    .restart local v12    # "ex":Ljava/lang/OutOfMemoryError;
    const-string v4, "ERROR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "error6 : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const/4 v4, 0x2

    goto :goto_0
.end method

.method public static resizeImage(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 15
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "destFilePath"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 742
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 743
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v14

    .line 744
    .local v14, "tempFolderPath":Ljava/io/File;
    if-nez v14, :cond_0

    .line 745
    const-string v2, "ImageController"

    const-string v3, " Error - tempFolderPath == null"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    const/4 v2, 0x0

    .line 832
    :goto_0
    return v2

    .line 748
    :cond_0
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 750
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_1

    .line 752
    const-string v2, "ImageController"

    const-string v3, " Error - tempFolderPath.mkdirs()"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    const/4 v2, 0x0

    goto :goto_0

    .line 757
    :cond_1
    const/4 v2, 0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 759
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 760
    .local v10, "option":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x4

    iput v2, v10, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 762
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 763
    invoke-static {p0, v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 764
    .local v1, "srcBitmap":Landroid/graphics/Bitmap;
    const-string v2, "ImageController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Width:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v10, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v10, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    const/4 v12, 0x0

    .line 767
    .local v12, "outStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileOutputStream;

    .end local v12    # "outStream":Ljava/io/OutputStream;
    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    .restart local v12    # "outStream":Ljava/io/OutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v12}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 776
    :try_start_1
    invoke-virtual {v12}, Ljava/io/OutputStream;->flush()V

    .line 777
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 832
    .end local v1    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "option":Landroid/graphics/BitmapFactory$Options;
    .end local v12    # "outStream":Ljava/io/OutputStream;
    :cond_2
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 768
    .restart local v1    # "srcBitmap":Landroid/graphics/Bitmap;
    .restart local v10    # "option":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v8

    .line 770
    .local v8, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 771
    const/4 v2, 0x0

    goto :goto_0

    .line 778
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    .restart local v12    # "outStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v8

    .line 780
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 781
    const/4 v2, 0x0

    goto :goto_0

    .line 784
    .end local v1    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "option":Landroid/graphics/BitmapFactory$Options;
    .end local v12    # "outStream":Ljava/io/OutputStream;
    :cond_3
    const/4 v2, 0x2

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 788
    :try_start_2
    new-instance v11, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v11}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 790
    .local v11, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v11, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 791
    invoke-static {p0, v11}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 793
    .restart local v1    # "srcBitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 794
    const/16 v4, 0xaa9

    .line 795
    const/16 v5, 0x89c

    .line 796
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 793
    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 797
    .local v13, "rotatedBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 798
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 800
    const-string v2, "ImageController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Width:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Height:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_4

    .line 801
    const/4 v12, 0x0

    .line 804
    .restart local v12    # "outStream":Ljava/io/OutputStream;
    :try_start_3
    new-instance v12, Ljava/io/FileOutputStream;

    .end local v12    # "outStream":Ljava/io/OutputStream;
    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_4

    .line 812
    .restart local v12    # "outStream":Ljava/io/OutputStream;
    :try_start_4
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v13, v2, v3, v12}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 813
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 814
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_4

    .line 816
    if-eqz v12, :cond_2

    .line 818
    :try_start_5
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 819
    :catch_2
    move-exception v8

    .line 821
    .restart local v8    # "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 822
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 805
    .end local v8    # "e":Ljava/io/IOException;
    .end local v12    # "outStream":Ljava/io/OutputStream;
    :catch_3
    move-exception v8

    .line 807
    .local v8, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 808
    const-string v2, "ImageController"

    const-string v3, " Error - FileNotFoundException e"

    invoke-static {v2, v3}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_4

    .line 809
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 826
    .end local v1    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    .end local v11    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v13    # "rotatedBitmap":Landroid/graphics/Bitmap;
    :catch_4
    move-exception v9

    .line 828
    .local v9, "ex":Ljava/lang/OutOfMemoryError;
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public static splitImageToJPG(Ljava/lang/String;Ljava/lang/String;IIDI)I
    .locals 25
    .param p0, "fileFullPath"    # Ljava/lang/String;
    .param p1, "newFilePath"    # Ljava/lang/String;
    .param p2, "fristBandHeight"    # I
    .param p3, "bandHeight"    # I
    .param p4, "scaleRate"    # D
    .param p6, "rotateValue"    # I

    .prologue
    .line 568
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 569
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 571
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v9, v15, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 572
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 574
    .local v3, "srcBitmap":Landroid/graphics/Bitmap;
    const/16 v21, 0x0

    .line 575
    .local v21, "startPos":I
    move/from16 v12, p2

    .line 578
    .local v12, "height":I
    const/16 v9, 0x5a

    move/from16 v0, p6

    if-eq v0, v9, :cond_0

    const/16 v9, 0x10e

    move/from16 v0, p6

    if-ne v0, v9, :cond_1

    .line 580
    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 581
    .local v16, "orgImageHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    .line 589
    .local v17, "orgImageWidth":I
    :goto_0
    const/16 v20, 0x0

    .line 590
    .local v20, "srcTop":I
    const/16 v19, 0x0

    .line 592
    .local v19, "srcHeight":I
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 593
    .local v8, "matrixRotate":Landroid/graphics/Matrix;
    move/from16 v0, p6

    int-to-float v9, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v8, v9, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 594
    const/16 v18, 0x0

    .line 596
    .local v18, "outStream":Ljava/io/OutputStream;
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v22

    .line 597
    .local v22, "tempFolderPath":Ljava/io/File;
    if-nez v22, :cond_2

    .line 598
    const-string v9, "ImageController"

    const-string v23, "Error tempFolderPath == null"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const/4 v9, 0x1

    .line 687
    .end local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .end local v12    # "height":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "orgImageHeight":I
    .end local v17    # "orgImageWidth":I
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .end local v19    # "srcHeight":I
    .end local v20    # "srcTop":I
    .end local v21    # "startPos":I
    .end local v22    # "tempFolderPath":Ljava/io/File;
    :goto_1
    return v9

    .line 585
    .restart local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "height":I
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v21    # "startPos":I
    :cond_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 586
    .restart local v16    # "orgImageHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .restart local v17    # "orgImageWidth":I
    goto :goto_0

    .line 601
    .restart local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    .restart local v19    # "srcHeight":I
    .restart local v20    # "srcTop":I
    .restart local v22    # "tempFolderPath":Ljava/io/File;
    :cond_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    .line 603
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-nez v9, :cond_3

    .line 605
    const-string v9, "ERROR"

    const-string v23, "error7 : ERROR - mkdirs"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const/4 v9, 0x1

    goto :goto_1

    .line 610
    :cond_3
    const/4 v4, 0x0

    .line 611
    .local v4, "splittedX":I
    const/4 v5, 0x0

    .line 612
    .local v5, "splittedY":I
    const/4 v6, 0x0

    .line 613
    .local v6, "splittedWidth":I
    const/4 v7, 0x0

    .line 616
    .local v7, "splittedHeight":I
    :goto_2
    move/from16 v0, v21

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcTop(ID)D

    move-result-wide v23

    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v20, v0

    .line 617
    move/from16 v0, v16

    move/from16 v1, v20

    if-gt v0, v1, :cond_4

    .line 619
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "break orgImageHeight("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "srcTop ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 680
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 687
    const/4 v9, 0x0

    goto :goto_1

    .line 623
    :cond_4
    add-int v9, v21, v12

    move-wide/from16 v0, p4

    invoke-static {v9, v0, v1}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcBottom(ID)D

    move-result-wide v23

    move-wide/from16 v0, v23

    double-to-int v9, v0

    sub-int v19, v9, v20

    .line 624
    add-int v9, v20, v19

    move/from16 v0, v16

    if-le v9, v0, :cond_5

    .line 626
    sub-int v19, v16, v20

    .line 629
    :cond_5
    sparse-switch p6, :sswitch_data_0

    .line 650
    :goto_3
    :sswitch_0
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "splittedX:"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedY:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedWidth:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedHeight:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 652
    .local v13, "newSplitBitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 654
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v14

    .line 657
    .local v14, "newSplitFilePath":Ljava/lang/String;
    :try_start_1
    new-instance v18, Ljava/io/FileOutputStream;

    .end local v18    # "outStream":Ljava/io/OutputStream;
    move-object/from16 v0, v18

    invoke-direct {v0, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    .line 664
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    :try_start_2
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v23, 0x64

    move/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v13, v9, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 666
    :try_start_3
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    .line 672
    :goto_4
    :try_start_4
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 673
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 675
    add-int v21, v21, v12

    .line 676
    move/from16 v12, p3

    .line 614
    goto/16 :goto_2

    .line 632
    .end local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "newSplitFilePath":Ljava/lang/String;
    :sswitch_1
    const/4 v4, 0x0

    .line 633
    move/from16 v5, v20

    .line 634
    move/from16 v6, v17

    .line 635
    move/from16 v7, v19

    .line 636
    goto/16 :goto_3

    .line 638
    :sswitch_2
    move/from16 v4, v20

    .line 639
    const/4 v5, 0x0

    .line 640
    move/from16 v6, v19

    .line 641
    move/from16 v7, v17

    .line 642
    goto/16 :goto_3

    .line 658
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .restart local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "newSplitFilePath":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 660
    .local v10, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 661
    const-string v9, "ImageController"

    const-string v23, "Error A"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 667
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v10

    .line 669
    .local v10, "e":Ljava/io/IOException;
    const-string v9, "ImageController"

    const-string v23, "Error newSplitBitmap.compress"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 681
    .end local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "splittedX":I
    .end local v5    # "splittedY":I
    .end local v6    # "splittedWidth":I
    .end local v7    # "splittedHeight":I
    .end local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "height":I
    .end local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "newSplitFilePath":Ljava/lang/String;
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "orgImageHeight":I
    .end local v17    # "orgImageWidth":I
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .end local v19    # "srcHeight":I
    .end local v20    # "srcTop":I
    .end local v21    # "startPos":I
    .end local v22    # "tempFolderPath":Ljava/io/File;
    :catch_2
    move-exception v11

    .line 683
    .local v11, "ex":Ljava/lang/OutOfMemoryError;
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "error8 : "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const/4 v9, 0x2

    goto/16 :goto_1

    .line 629
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x5a -> :sswitch_2
        0xb4 -> :sswitch_0
    .end sparse-switch
.end method

.method public static splitImageToJPGCrop(Ljava/lang/String;Ljava/lang/String;IIDIIIIII)I
    .locals 25
    .param p0, "fileFullPath"    # Ljava/lang/String;
    .param p1, "newFilePath"    # Ljava/lang/String;
    .param p2, "fristBandHeight"    # I
    .param p3, "bandHeight"    # I
    .param p4, "scaleRate"    # D
    .param p6, "rotateValue"    # I
    .param p7, "nCropValue"    # I
    .param p8, "cropPosLeft"    # I
    .param p9, "cropPosTop"    # I
    .param p10, "cropWidth"    # I
    .param p11, "cropHeight"    # I

    .prologue
    .line 419
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 420
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 422
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v9, v15, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 423
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 425
    .local v3, "srcBitmap":Landroid/graphics/Bitmap;
    const/16 v21, 0x0

    .line 426
    .local v21, "startPos":I
    move/from16 v12, p2

    .line 429
    .local v12, "height":I
    const/16 v9, 0x5a

    move/from16 v0, p6

    if-eq v0, v9, :cond_0

    const/16 v9, 0x10e

    move/from16 v0, p6

    if-ne v0, v9, :cond_1

    .line 431
    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 432
    .local v16, "orgImageHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    .line 440
    .local v17, "orgImageWidth":I
    :goto_0
    const/16 v20, 0x0

    .line 441
    .local v20, "srcTop":I
    const/16 v19, 0x0

    .line 443
    .local v19, "srcHeight":I
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 444
    .local v8, "matrixRotate":Landroid/graphics/Matrix;
    move/from16 v0, p6

    int-to-float v9, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v8, v9, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 445
    const/16 v18, 0x0

    .line 447
    .local v18, "outStream":Ljava/io/OutputStream;
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v22

    .line 448
    .local v22, "tempFolderPath":Ljava/io/File;
    if-nez v22, :cond_2

    .line 449
    const-string v9, "ImageController"

    const-string v23, "Error tempFolderPath == null"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const/4 v9, 0x1

    .line 560
    .end local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .end local v12    # "height":I
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "orgImageHeight":I
    .end local v17    # "orgImageWidth":I
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .end local v19    # "srcHeight":I
    .end local v20    # "srcTop":I
    .end local v21    # "startPos":I
    .end local v22    # "tempFolderPath":Ljava/io/File;
    :goto_1
    return v9

    .line 436
    .restart local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .restart local v12    # "height":I
    .restart local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v21    # "startPos":I
    :cond_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 437
    .restart local v16    # "orgImageHeight":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .restart local v17    # "orgImageWidth":I
    goto :goto_0

    .line 452
    .restart local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    .restart local v19    # "srcHeight":I
    .restart local v20    # "srcTop":I
    .restart local v22    # "tempFolderPath":Ljava/io/File;
    :cond_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    .line 454
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-nez v9, :cond_3

    .line 456
    const-string v9, "ImageController"

    const-string v23, "error7 : ERROR - mkdirs"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const/4 v9, 0x1

    goto :goto_1

    .line 461
    :cond_3
    const/4 v9, 0x1

    move/from16 v0, p7

    if-ne v0, v9, :cond_4

    .line 463
    move/from16 v16, p11

    .line 464
    move/from16 v17, p10

    .line 480
    :goto_2
    const/4 v4, 0x0

    .line 481
    .local v4, "splittedX":I
    const/4 v5, 0x0

    .line 482
    .local v5, "splittedY":I
    const/4 v6, 0x0

    .line 483
    .local v6, "splittedWidth":I
    const/4 v7, 0x0

    .line 486
    .local v7, "splittedHeight":I
    :goto_3
    move/from16 v0, v21

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcTop(ID)D

    move-result-wide v23

    move-wide/from16 v0, v23

    double-to-int v0, v0

    move/from16 v20, v0

    .line 487
    move/from16 v0, v16

    move/from16 v1, v20

    if-gt v0, v1, :cond_5

    .line 489
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "break orgImageHeight("

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "srcTop ("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 553
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 560
    const/4 v9, 0x0

    goto :goto_1

    .line 476
    .end local v4    # "splittedX":I
    .end local v5    # "splittedY":I
    .end local v6    # "splittedWidth":I
    .end local v7    # "splittedHeight":I
    :cond_4
    const/16 p8, 0x0

    .line 477
    const/16 p9, 0x0

    goto :goto_2

    .line 493
    .restart local v4    # "splittedX":I
    .restart local v5    # "splittedY":I
    .restart local v6    # "splittedWidth":I
    .restart local v7    # "splittedHeight":I
    :cond_5
    add-int v9, v21, v12

    move-wide/from16 v0, p4

    invoke-static {v9, v0, v1}, Lcom/sec/print/mobileprint/io/ImageController;->calcSrcBottom(ID)D

    move-result-wide v23

    move-wide/from16 v0, v23

    double-to-int v9, v0

    sub-int v19, v9, v20

    .line 494
    add-int v9, v20, v19

    move/from16 v0, v16

    if-le v9, v0, :cond_6

    .line 496
    sub-int v19, v16, v20

    .line 499
    :cond_6
    sparse-switch p6, :sswitch_data_0

    .line 522
    :goto_4
    :sswitch_0
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "splittedX:"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedY:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedWidth:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", splittedHeight:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 524
    .local v13, "newSplitBitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 526
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v14

    .line 529
    .local v14, "newSplitFilePath":Ljava/lang/String;
    :try_start_1
    new-instance v18, Ljava/io/FileOutputStream;

    .end local v18    # "outStream":Ljava/io/OutputStream;
    move-object/from16 v0, v18

    invoke-direct {v0, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    .line 536
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    :try_start_2
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "Split - "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v23, 0x64

    move/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v13, v9, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    .line 539
    :try_start_3
    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    .line 545
    :goto_5
    :try_start_4
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 546
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 548
    add-int v21, v21, v12

    .line 549
    move/from16 v12, p3

    .line 484
    goto/16 :goto_3

    .line 502
    .end local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "newSplitFilePath":Ljava/lang/String;
    :sswitch_1
    move/from16 v4, p8

    .line 503
    add-int v5, v20, p9

    .line 504
    move/from16 v6, v17

    .line 505
    move/from16 v7, v19

    .line 506
    goto/16 :goto_4

    .line 508
    :sswitch_2
    add-int v4, v20, p9

    .line 509
    move/from16 v5, p8

    .line 510
    move/from16 v6, v19

    .line 511
    move/from16 v7, v17

    .line 512
    goto/16 :goto_4

    .line 530
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .restart local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "newSplitFilePath":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 532
    .local v10, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 533
    const-string v9, "ImageController"

    const-string v23, "Error A"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 540
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    .restart local v18    # "outStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v10

    .line 542
    .local v10, "e":Ljava/io/IOException;
    const-string v9, "ImageController"

    const-string v23, "Error newSplitBitmap.compress"

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    .line 554
    .end local v3    # "srcBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "splittedX":I
    .end local v5    # "splittedY":I
    .end local v6    # "splittedWidth":I
    .end local v7    # "splittedHeight":I
    .end local v8    # "matrixRotate":Landroid/graphics/Matrix;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "height":I
    .end local v13    # "newSplitBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "newSplitFilePath":Ljava/lang/String;
    .end local v15    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v16    # "orgImageHeight":I
    .end local v17    # "orgImageWidth":I
    .end local v18    # "outStream":Ljava/io/OutputStream;
    .end local v19    # "srcHeight":I
    .end local v20    # "srcTop":I
    .end local v21    # "startPos":I
    .end local v22    # "tempFolderPath":Ljava/io/File;
    :catch_2
    move-exception v11

    .line 556
    .local v11, "ex":Ljava/lang/OutOfMemoryError;
    const-string v9, "ImageController"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "error8 : "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v9, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    const/4 v9, 0x2

    goto/16 :goto_1

    .line 499
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x5a -> :sswitch_2
        0xb4 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public closeImage()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    .line 217
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 219
    :cond_0
    return-void
.end method

.method public extractImageToARGB32RawData(II[I[I)I
    .locals 10
    .param p1, "startHeightPos"    # I
    .param p2, "bandHeightStep"    # I
    .param p3, "pixelsOutput"    # [I
    .param p4, "nOutDataHeight"    # [I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 91
    const-string v0, "error10-1 : m_bitmapData == null "

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    const/4 v0, 0x1

    .line 118
    :goto_0
    return v0

    .line 94
    :cond_0
    move v7, p2

    .line 95
    .local v7, "step":I
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int/2addr v0, p1

    if-le v7, v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v7, v0, p1

    .line 102
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 103
    const/4 v4, 0x0

    iget-object v1, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    move-object v1, p3

    move v5, p1

    .line 102
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 116
    const/4 v0, 0x0

    aput v7, p4, v0

    .line 118
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v9

    .line 107
    .local v9, "ex":Ljava/lang/OutOfMemoryError;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error9 : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    const/4 v0, 0x2

    goto :goto_0

    .line 110
    .end local v9    # "ex":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v8

    .line 112
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "ERROR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error10 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public extractImageToJPGFileData(II[B[I[I)I
    .locals 11
    .param p1, "startHeightPos"    # I
    .param p2, "bandHeightStep"    # I
    .param p3, "outputBuffer"    # [B
    .param p4, "nDataHeight"    # [I
    .param p5, "nDataLength"    # [I

    .prologue
    .line 160
    move v4, p2

    .line 161
    .local v4, "step":I
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "extractImageToJPGFileData => step:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", getHeight:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startHeightPos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int/2addr v0, p1

    if-le v4, v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v4, v0, p1

    .line 166
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "step:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    :try_start_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 171
    .local v5, "matrix":Landroid/graphics/Matrix;
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    .line 172
    const/4 v1, 0x0

    .line 173
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 174
    const/4 v6, 0x1

    move v2, p1

    .line 171
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 176
    .local v7, "bandBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;

    invoke-direct {v10, p0, p3}, Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;-><init>(Lcom/sec/print/mobileprint/io/ImageController;[B)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 178
    .local v10, "outStream":Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v7, v0, v1, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    if-nez v0, :cond_1

    .line 180
    const/4 v0, 0x1

    .line 207
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "bandBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "outStream":Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;
    :goto_0
    return v0

    .line 182
    .restart local v5    # "matrix":Landroid/graphics/Matrix;
    .restart local v7    # "bandBitmap":Landroid/graphics/Bitmap;
    .restart local v10    # "outStream":Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;
    :catch_0
    move-exception v8

    .line 184
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error13 : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    const/4 v0, 0x1

    goto :goto_0

    .line 188
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    aput v1, p4, v0

    .line 189
    const/4 v0, 0x0

    invoke-virtual {v10}, Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;->getLength()I

    move-result v1

    aput v1, p5, v0

    .line 191
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 192
    const/4 v7, 0x0

    .line 193
    invoke-virtual {v10}, Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;->close()V

    .line 194
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 207
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "bandBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "outStream":Lcom/sec/print/mobileprint/io/ImageController$ByteBufferOutputStream;
    :catch_1
    move-exception v9

    .line 198
    .local v9, "ex":Ljava/lang/OutOfMemoryError;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error11 : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    const/4 v0, 0x2

    goto :goto_0

    .line 201
    .end local v9    # "ex":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v8

    .line 203
    .restart local v8    # "e":Ljava/lang/Exception;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error12 : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public loadImage(Ljava/lang/String;)I
    .locals 14
    .param p1, "fileBitmapPath"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/io/ImageController;->closeImage()V

    .line 30
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 31
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 33
    .local v13, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v13, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 34
    invoke-static {p1, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    .line 35
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 37
    const-string v0, "Fail to decode"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    const/4 v0, 0x2

    .line 84
    .end local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return v0

    .line 42
    .restart local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    const/16 v9, 0x80

    .line 43
    .local v9, "m_cnMaxBandHeight":I
    const/16 v10, 0x18

    .line 45
    .local v10, "nBitPerPixelNative":I
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    mul-int/2addr v0, v10

    add-int/lit8 v0, v0, 0x1f

    div-int/lit8 v0, v0, 0x20

    mul-int/lit8 v12, v0, 0x4

    .line 46
    .local v12, "nWidthByte":I
    const/high16 v0, 0x40000

    div-int v11, v0, v12

    .line 47
    .local v11, "nMinimumHeight":I
    const/4 v4, 0x0

    .line 48
    .local v4, "nBandHeight":I
    const/16 v0, 0x80

    if-ge v0, v11, :cond_1

    .line 50
    move v4, v11

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "nBandHeight for Test :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->e(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    :goto_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ge v0, v4, :cond_2

    .line 60
    const-string v0, "Skip - check memory avaiable"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    :goto_2
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 75
    const-string v0, "success load image"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "nBandHeight for Test :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    const/16 v4, 0x80

    goto :goto_1

    .line 63
    :cond_2
    const-string v0, "Working - check memory avaiable"

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 65
    .local v5, "matrix":Landroid/graphics/Matrix;
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    .line 66
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 67
    iget-object v3, p0, Lcom/sec/print/mobileprint/io/ImageController;->m_bitmapData:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 68
    const/4 v6, 0x1

    .line 65
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 69
    .local v7, "bandBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    .line 71
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 77
    .end local v4    # "nBandHeight":I
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "bandBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "m_cnMaxBandHeight":I
    .end local v10    # "nBitPerPixelNative":I
    .end local v11    # "nMinimumHeight":I
    .end local v12    # "nWidthByte":I
    .end local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v8

    .line 79
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/io/ImageController;->closeImage()V

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LoadImage : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x2

    goto/16 :goto_0
.end method
