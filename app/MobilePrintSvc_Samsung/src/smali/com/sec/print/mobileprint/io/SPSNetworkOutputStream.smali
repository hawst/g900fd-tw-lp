.class public Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;
.super Ljava/lang/Object;
.source "SPSNetworkOutputStream.java"

# interfaces
.implements Lcom/sec/print/mobileprint/io/ISPSOutputStream;


# static fields
.field static final CANCEL_TIMEOUT:J = 0xbb8L

.field static final TIMEOUT:J = 0x493e0L

.field static final WAIT_TIME:J = 0x64L


# instance fields
.field buf:Ljava/nio/ByteBuffer;

.field bufLength:I

.field ipAddr:Ljava/lang/String;

.field isCancel:Z

.field isOccurTimeout:Z

.field portNum:I

.field socketChannel:Ljava/nio/channels/SocketChannel;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "portNum"    # I

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    .line 23
    iput v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->bufLength:I

    .line 32
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isCancel:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 37
    iput-object p1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->ipAddr:Ljava/lang/String;

    .line 38
    iput p2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->portNum:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isCancel:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 41
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isCancel:Z

    .line 45
    monitor-enter p0

    .line 46
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 45
    monitor-exit p0

    .line 48
    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 60
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 64
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/io/IOException;
    iput-boolean v1, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 68
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnect()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public open()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 169
    :try_start_0
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->ipAddr:Ljava/lang/String;

    iget v5, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->portNum:I

    invoke-direct {v0, v4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 170
    .local v0, "address":Ljava/net/InetSocketAddress;
    invoke-static {v0}, Ljava/nio/channels/SocketChannel;->open(Ljava/net/SocketAddress;)Ljava/nio/channels/SocketChannel;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    .line 171
    iget-object v4, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 173
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 187
    .end local v0    # "address":Ljava/net/InetSocketAddress;
    :goto_0
    return v2

    .line 174
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 177
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 178
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v2, v3

    .line 179
    goto :goto_0

    .line 180
    .end local v1    # "e":Ljava/net/UnknownHostException;
    :catch_1
    move-exception v1

    .line 182
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 183
    iput-boolean v2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 184
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v2, v3

    .line 185
    goto :goto_0
.end method

.method public write([BI)Z
    .locals 13
    .param p1, "buffer"    # [B
    .param p2, "length"    # I

    .prologue
    const-wide/32 v11, 0x493e0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 78
    :try_start_0
    iget-boolean v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    if-eqz v9, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v7

    .line 82
    :cond_1
    iget-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    if-nez v9, :cond_3

    .line 83
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    .line 84
    invoke-static {p2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    .line 85
    iput p2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->bufLength:I

    .line 94
    :goto_1
    iget-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    const/4 v10, 0x0

    invoke-virtual {v9, p1, v10, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 98
    .local v5, "stime":J
    const-wide/16 v1, 0x0

    .line 99
    .local v1, "rtime":J
    const/4 v3, 0x0

    .line 100
    .local v3, "rvalue":I
    const/4 v4, 0x0

    .line 101
    .local v4, "rvalues":I
    :goto_2
    cmp-long v9, v1, v11

    if-ltz v9, :cond_5

    .line 136
    :cond_2
    :goto_3
    cmp-long v9, v1, v11

    if-lez v9, :cond_0

    .line 137
    const-string v9, "write - connection closed (rtime > timeout) == Timeout"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 139
    iget-boolean v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isCancel:Z

    if-nez v9, :cond_0

    move v7, v8

    .line 142
    goto :goto_0

    .line 86
    .end local v1    # "rtime":J
    .end local v3    # "rvalue":I
    .end local v4    # "rvalues":I
    .end local v5    # "stime":J
    :cond_3
    iget v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->bufLength:I

    if-eq v9, p2, :cond_4

    .line 87
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    .line 88
    invoke-static {p2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    .line 89
    iput p2, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->bufLength:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 144
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 147
    iput-boolean v7, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isOccurTimeout:Z

    .line 148
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v7, v8

    .line 149
    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_1
    iget-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    .line 102
    .restart local v1    # "rtime":J
    .restart local v3    # "rvalue":I
    .restart local v4    # "rvalues":I
    .restart local v5    # "stime":J
    :cond_5
    iget-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v1, v9, v5

    .line 104
    iget-object v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->socketChannel:Ljava/nio/channels/SocketChannel;

    iget-object v10, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->buf:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v10}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v3

    .line 105
    const/4 v9, -0x1

    if-ne v3, v9, :cond_6

    .line 106
    const-string v9, "socketChannel.write(buf) return -1"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v7, v8

    .line 107
    goto :goto_0

    .line 110
    :cond_6
    add-int/2addr v4, v3

    .line 111
    cmp-long v9, v1, v11

    if-gtz v9, :cond_2

    .line 115
    if-ge v4, p2, :cond_2

    .line 120
    iget-boolean v9, p0, Lcom/sec/print/mobileprint/io/SPSNetworkOutputStream;->isCancel:Z

    if-eqz v9, :cond_7

    const-wide/16 v9, 0xbb8

    cmp-long v9, v1, v9

    if-lez v9, :cond_7

    .line 121
    const-string v9, "canceled - Timeout"

    invoke-static {p0, v9}, Lcom/sec/print/mobileprint/MPLogger;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    const-wide/32 v1, 0x493e1

    .line 123
    goto :goto_3

    .line 126
    :cond_7
    monitor-enter p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 128
    const-wide/16 v9, 0x64

    :try_start_2
    invoke-virtual {p0, v9, v10}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    :goto_4
    :try_start_3
    monitor-exit p0

    goto :goto_2

    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v9
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 129
    :catch_1
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4
.end method
