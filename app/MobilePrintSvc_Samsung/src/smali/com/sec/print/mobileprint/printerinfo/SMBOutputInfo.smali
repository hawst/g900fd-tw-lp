.class public Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;
.super Ljava/lang/Object;
.source "SMBOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field portNum:I

.field selectedPrinterDomain:Ljava/lang/String;

.field selectedPrinterIP:Ljava/lang/String;

.field selectedPrinterPassword:Ljava/lang/String;

.field selectedPrinterShareName:Ljava/lang/String;

.field selectedPrinterUserName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterDomain"    # Ljava/lang/String;
    .param p2, "selectedPrinterUserName"    # Ljava/lang/String;
    .param p3, "selectedPrinterPassword"    # Ljava/lang/String;
    .param p4, "selectedPrinterIP"    # Ljava/lang/String;
    .param p5, "selectedPrinterShareName"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterDomain:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterUserName:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterPassword:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterShareName:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedPrinterDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterIP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterShareName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterShareName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterUserName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterDomain:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterUserName:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterPassword:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterShareName:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setSelectedPrinterDomain(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterDomain"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterDomain:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setSelectedPrinterIP(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterIP"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setSelectedPrinterPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterPassword"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterPassword:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setSelectedPrinterShareName(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterShareName"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterShareName:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setSelectedPrinterUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedPrinterUserName"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterUserName:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterIP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/SMBOutputInfo;->selectedPrinterShareName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    return-void
.end method
