.class public final enum Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;
.super Ljava/lang/Enum;
.source "DocumentQuality.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumDocumentQuality"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

.field public static final enum HIGH:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

.field public static final enum NORMAL:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    const-string v1, "NORMAL"

    .line 25
    invoke-direct {v0, v1, v2, v2}, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->NORMAL:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    .line 26
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    const-string v1, "HIGH"

    .line 27
    invoke-direct {v0, v1, v3, v3}, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->HIGH:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->NORMAL:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->HIGH:Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->mValue:I

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    return-object v0
.end method

.method public static values()[Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->ENUM$VALUES:[Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/DocumentQuality$EnumDocumentQuality;->mValue:I

    return v0
.end method
