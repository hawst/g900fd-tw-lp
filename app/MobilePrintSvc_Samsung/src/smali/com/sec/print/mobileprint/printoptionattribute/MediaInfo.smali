.class public Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;
.super Ljava/lang/Object;
.source "MediaInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;,
        Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private height:I

.field private marginBottom:I

.field private marginLeft:I

.field private marginRight:I

.field private marginTop:I

.field private mediaSizeID:I

.field private mediaTypeID:I

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 621
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x32

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A4:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 163
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PRINTERDEFAULT:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 165
    const/16 v0, 0x9b0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 166
    const/16 v0, 0xdb3

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 168
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 169
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 170
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 171
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 172
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 149
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;II)V
    .locals 2
    .param p1, "eMediaSize"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/16 v1, 0x32

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 189
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PRINTERDEFAULT:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 191
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 192
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 194
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 195
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 196
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 197
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 199
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;IILcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;)V
    .locals 2
    .param p1, "eMediaSize"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "eMediaType"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .prologue
    const/16 v1, 0x32

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 216
    invoke-virtual {p4}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 218
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 219
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 221
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 222
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 223
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 224
    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 226
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;IILcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;IIII)V
    .locals 1
    .param p1, "eMediaSize"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "eMediaType"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    .param p5, "marginLeft"    # I
    .param p6, "marginTop"    # I
    .param p7, "marginRight"    # I
    .param p8, "marginBottom"    # I

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 247
    invoke-virtual {p4}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 249
    iput p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 250
    iput p3, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 252
    iput p5, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 253
    iput p6, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 254
    iput p7, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 255
    iput p8, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 257
    return-void
.end method

.method public static getDefaultMediatypeList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 572
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 573
    .local v0, "mediatypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "Normal"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    return-object v0
.end method

.method public static getMediaSizeID(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 367
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_LETTER:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 368
    .local v0, "resultValue":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    const-string v1, "letter"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_LETTER:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 424
    :goto_0
    return-object v0

    .line 370
    :cond_0
    const-string v1, "LEGAL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_LEGAL:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 372
    goto :goto_0

    :cond_1
    const-string v1, "OFICIO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 373
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_OFICIO:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 374
    goto :goto_0

    :cond_2
    const-string v1, "FOLIO"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 375
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_FOLIO:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 376
    goto :goto_0

    :cond_3
    const-string v1, "A3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 377
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A3:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 378
    goto :goto_0

    :cond_4
    const-string v1, "TABLOID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 379
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_TABLOID:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 380
    goto :goto_0

    :cond_5
    const-string v1, "JISB4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 381
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_JISB4:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 382
    goto :goto_0

    :cond_6
    const-string v1, "A4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 383
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A4:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 384
    goto :goto_0

    :cond_7
    const-string v1, "B5ENV"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 385
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_B5ENV:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 386
    goto :goto_0

    :cond_8
    const-string v1, "JISB5"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 387
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_JISB5:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 388
    goto :goto_0

    :cond_9
    const-string v1, "STATEMENT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 389
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_STATEMENT:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 390
    goto :goto_0

    :cond_a
    const-string v1, "Executive"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 391
    const-string v1, "Exec"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 392
    :cond_b
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_EXEC:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 393
    goto/16 :goto_0

    :cond_c
    const-string v1, "A5"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 394
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A5:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 395
    goto/16 :goto_0

    :cond_d
    const-string v1, "A6"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 396
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A6:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 397
    goto/16 :goto_0

    :cond_e
    const-string v1, "MONARCH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 398
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_MONARCH:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 399
    goto/16 :goto_0

    :cond_f
    const-string v1, "COM10"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 400
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_COM10:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 401
    goto/16 :goto_0

    :cond_10
    const-string v1, "DL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 402
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_DL:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 403
    goto/16 :goto_0

    :cond_11
    const-string v1, "C5"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 404
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_C5:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 405
    goto/16 :goto_0

    :cond_12
    const-string v1, "C6ENV"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 406
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_C6ENV:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 407
    goto/16 :goto_0

    :cond_13
    const-string v1, "ENV_NO9"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 408
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_ENV_NO9:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 409
    goto/16 :goto_0

    :cond_14
    const-string v1, "ENV_PERSONAL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 410
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_ENV_PERSONAL:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 411
    goto/16 :goto_0

    :cond_15
    const-string v1, "3.5x5in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 412
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_3_5X5IN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 413
    goto/16 :goto_0

    :cond_16
    const-string v1, "4x6in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 414
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_4X6IN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 415
    goto/16 :goto_0

    :cond_17
    const-string v1, "5x7in"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 416
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_PHOTO_BOARDERLESS_5X7IN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 417
    goto/16 :goto_0

    :cond_18
    const-string v1, "Custom"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 418
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_CUSTOM:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .line 419
    goto/16 :goto_0

    .line 421
    :cond_19
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->MEDIA_SIZE_A4:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    goto/16 :goto_0
.end method

.method public static getMediaTypeID(Ljava/lang/String;)Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 434
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 435
    .local v0, "resultValue":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    const-string v1, "Printer Default"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 437
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PRINTERDEFAULT:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 529
    :goto_0
    return-object v0

    .line 438
    :cond_0
    const-string v1, "Plain"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 439
    const-string v1, "Normal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 441
    :cond_1
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 442
    goto :goto_0

    :cond_2
    const-string v1, "THICK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 444
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_THICK:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 445
    goto :goto_0

    :cond_3
    const-string v1, "THICKER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 447
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_THICKER:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 448
    goto :goto_0

    :cond_4
    const-string v1, "EXTRATHICK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 450
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_EXTRATHICK:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 451
    goto :goto_0

    :cond_5
    const-string v1, "THIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 453
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_THIN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 454
    goto :goto_0

    :cond_6
    const-string v1, "BOND"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 456
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_BOND:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 457
    goto :goto_0

    :cond_7
    const-string v1, "COLOR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 459
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_COLOR:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 460
    goto :goto_0

    :cond_8
    const-string v1, "CARD_STOCK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 462
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_CARD_STOCK:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 463
    goto :goto_0

    :cond_9
    const-string v1, "LABELS"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 465
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_LABELS:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 466
    goto :goto_0

    :cond_a
    const-string v1, "TRANSPARENCY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 468
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_TRANSPARENCY:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 469
    goto :goto_0

    :cond_b
    const-string v1, "ENVELOPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 471
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_ENVELOPE:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 472
    goto/16 :goto_0

    :cond_c
    const-string v1, "PREPRINTED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 474
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PREPRINTED:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 475
    goto/16 :goto_0

    :cond_d
    const-string v1, "COTTON"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 477
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_COTTON:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 478
    goto/16 :goto_0

    :cond_e
    const-string v1, "RECYCLED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 480
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_RECYCLED:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 481
    goto/16 :goto_0

    :cond_f
    const-string v1, "ARCHIVE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 483
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_ARCHIVE:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 484
    goto/16 :goto_0

    :cond_10
    const-string v1, "LETTERHEAD"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 486
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_LETTERHEAD:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 487
    goto/16 :goto_0

    :cond_11
    const-string v1, "PREPUNCHED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 489
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PREPUNCHED:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 490
    goto/16 :goto_0

    :cond_12
    const-string v1, "PHOTO111_130"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 492
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PHOTO111_130:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 493
    goto/16 :goto_0

    :cond_13
    const-string v1, "PHOTO131_175"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 495
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PHOTO131_175:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 496
    goto/16 :goto_0

    :cond_14
    const-string v1, "PHOTO176_220"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 498
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PHOTO176_220:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 499
    goto/16 :goto_0

    :cond_15
    const-string v1, "MATTEPHOTO111_130"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 501
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_MATTEPHOTO111_130:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 502
    goto/16 :goto_0

    :cond_16
    const-string v1, "MATTEPHOTO131_175"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 504
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_MATTEPHOTO131_175:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 505
    goto/16 :goto_0

    :cond_17
    const-string v1, "MATTEPHOTO176_222"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 507
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_MATTEPHOTO176_222:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 508
    goto/16 :goto_0

    :cond_18
    const-string v1, "HEAVY_WEIGHT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 510
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_HEAVY_WEIGHT:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 511
    goto/16 :goto_0

    :cond_19
    const-string v1, "EXTRA_HEAVY_WEIGHT1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 97
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_EXTRA_HEAVY_WEIGHT1:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 514
    goto/16 :goto_0

    :cond_1a
    const-string v1, "EXTRA_HEAVY_WEIGHT2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 516
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_EXTRA_HEAVY_WEIGHT2:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 517
    goto/16 :goto_0

    :cond_1b
    const-string v1, "Photo"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 519
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PHOTO:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 520
    goto/16 :goto_0

    :cond_1c
    const-string v1, "Coated"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 522
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_COATED:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    .line 523
    goto/16 :goto_0

    .line 526
    :cond_1d
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->MEDIA_TYPE_PLAIN:Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    goto/16 :goto_0
.end method

.method public static getSPSMediaList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    .local p0, "deviceMediaSize":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 536
    .local v8, "allMediaSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "A4"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xdb3

    const/16 v7, 0x9b0

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "A5"

    const/16 v2, 0x39

    const/16 v3, 0x39

    const/16 v4, 0x39

    const/16 v5, 0x39

    const/16 v6, 0x9b0

    const/16 v7, 0x6d4

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 538
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "A6"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x6d4

    const/16 v7, 0x4d8

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "JISB5"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xbdb

    const/16 v7, 0x865

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 540
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "Letter"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xce4

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "Legal"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x1068

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "Executive"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xc4e

    const/16 v7, 0x87f

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "Folio"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0xf3c

    const/16 v7, 0x9f6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "A3"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x1360

    const/16 v7, 0xdb3

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "Tabloid"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x13ec

    const/16 v7, 0xce4

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "JISB4"

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x32

    const/16 v5, 0x32

    const/16 v6, 0x10c2

    const/16 v7, 0xbd6

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "5x7"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x834

    const/16 v7, 0x5dc

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "4x6"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x708

    const/16 v7, 0x4b0

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    const-string v1, "3.5x5"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x5dc

    const/16 v7, 0x41a

    invoke-direct/range {v0 .. v7}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;-><init>(Ljava/lang/String;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 556
    .local v10, "supportedMediaSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    if-nez p0, :cond_0

    .line 567
    .end local v8    # "allMediaSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    :goto_0
    return-object v8

    .line 560
    .restart local v8    # "allMediaSizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;>;"
    :cond_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v8, v10

    .line 567
    goto :goto_0

    .line 560
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;

    .line 561
    .local v9, "media":Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;
    invoke-virtual {v9}, Lcom/sec/print/mobileprint/printerinfo/SPSMediaSize;->getMediaName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    return v0
.end method

.method public getMarginBottom()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    return v0
.end method

.method public getMarginLeft()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    return v0
.end method

.method public getMarginRight()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    return v0
.end method

.method public getMarginTop()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    return v0
.end method

.method public getMediaSizeID()Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    .locals 6

    .prologue
    .line 324
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 332
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 324
    :cond_1
    aget-object v0, v1, v2

    .line 326
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    if-eq v4, v5, :cond_0

    .line 324
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getMediaSizeIDInteger()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    return v0
.end method

.method public getMediaTypeID()Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    .locals 6

    .prologue
    .line 346
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 354
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 346
    :cond_1
    aget-object v0, v1, v2

    .line 348
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaType;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    if-eq v4, v5, :cond_0

    .line 346
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getMediaTypeIDInteger()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 599
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 600
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 601
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 602
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 603
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 604
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 605
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 606
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 607
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    .line 274
    return-void
.end method

.method public setMarginBottom(I)V
    .locals 0
    .param p1, "marginBottom"    # I

    .prologue
    .line 305
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    .line 306
    return-void
.end method

.method public setMarginLeft(I)V
    .locals 0
    .param p1, "marginLeft"    # I

    .prologue
    .line 281
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    .line 282
    return-void
.end method

.method public setMarginRight(I)V
    .locals 0
    .param p1, "marginRight"    # I

    .prologue
    .line 297
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    .line 298
    return-void
.end method

.method public setMarginTop(I)V
    .locals 0
    .param p1, "marginTop"    # I

    .prologue
    .line 289
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    .line 290
    return-void
.end method

.method public setMediaSizeID(I)V
    .locals 0
    .param p1, "mediaSizeID"    # I

    .prologue
    .line 309
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 310
    return-void
.end method

.method public setMediaSizeID(Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;)V
    .locals 1
    .param p1, "eMediaSize"    # Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo$EnumMediaSize;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    .line 315
    return-void
.end method

.method public setMediaTypeID(I)V
    .locals 0
    .param p1, "mediaTypeID"    # I

    .prologue
    .line 337
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    .line 338
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 265
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    .line 266
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 587
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaSizeID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 588
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->mediaTypeID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 589
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 590
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 591
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginLeft:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 592
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginTop:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 593
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginRight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 594
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/MediaInfo;->marginBottom:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 595
    return-void
.end method
