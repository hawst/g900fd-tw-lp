.class public Lcom/sec/print/mobileprint/printoptionattribute/PDLType;
.super Ljava/lang/Object;
.source "PDLType.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/PDLType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private pdlType:I

.field private pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 123
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->PDLTYPE_AUTO:Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;)V
    .locals 1
    .param p1, "ePDLType"    # Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V
    .locals 1
    .param p1, "ePDLType"    # Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    .param p2, "pdlTypeInfo"    # Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    .line 50
    iput-object p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public getPDLType()Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    .locals 6

    .prologue
    .line 65
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 73
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 65
    :cond_1
    aget-object v0, v1, v2

    .line 67
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    if-eq v4, v5, :cond_0

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getPDLTypeInfo()Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 104
    .local v0, "pdlTypeInfo_null":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 105
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    iput-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .line 107
    :cond_0
    return-void
.end method

.method public setPDLType(Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;)V
    .locals 1
    .param p1, "ePDLType"    # Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/PDLType$EnumPDLType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    .line 56
    return-void
.end method

.method public setPDLTypeInfo(Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;)V
    .locals 0
    .param p1, "pdlTypeInfo"    # Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    .line 61
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PDLType;->pdlTypeInfo:Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0
.end method
