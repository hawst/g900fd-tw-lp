.class public Lcom/sec/print/mobileprint/utils/deviceinfoloader/MPDeviceInfoStaticValue;
.super Ljava/lang/Object;
.source "MPDeviceInfoStaticValue.java"


# static fields
.field public static final ELEMENT_COLOR_MODE:Ljava/lang/String; = "ColorMode"

.field public static final ELEMENT_COLOR_TABLE:Ljava/lang/String; = "CTSFile"

.field public static final ELEMENT_COMPRESSION:Ljava/lang/String; = "Compression"

.field public static final ELEMENT_DUPLEX:Ljava/lang/String; = "Duplex"

.field public static final ELEMENT_EMULATION:Ljava/lang/String; = "Emulation"

.field public static final ELEMENT_JOB_ACCOUNTING_VER:Ljava/lang/String; = "JobAccountingVer"

.field public static final ELEMENT_MODEL:Ljava/lang/String; = "Model"

.field public static final ELEMENT_MODELLIST:Ljava/lang/String; = "MobileDevices"

.field public static final ELEMENT_MODEL_NAME:Ljava/lang/String; = "ModelName"

.field public static final ELEMENT_MODEL_SERIES:Ljava/lang/String; = "ModelSeries"

.field public static final ELEMENT_PAPER_SIZE:Ljava/lang/String; = "PaperSize"

.field public static final ELEMENT_PAPER_TYPE:Ljava/lang/String; = "PaperType"

.field public static final ELEMENT_PCL6_COMPRESSION:Ljava/lang/String; = "PCL6Compression"

.field public static final ELEMENT_PID:Ljava/lang/String; = "PID"

.field public static final ELEMENT_SPL_VERSION:Ljava/lang/String; = "SPLVersion"

.field public static final ELEMENT_VERSION:Ljava/lang/String; = "Version"

.field public static final ELEMENT_VID:Ljava/lang/String; = "VID"

.field public static final ELEMENT_WIDTH_ALIGN:Ljava/lang/String; = "WidthAlign"

.field public static final VALUE_COLOR:Ljava/lang/String; = "Color"

.field public static final VALUE_JOB_ACCOUNTING_ID_PWD_ENC:Ljava/lang/String; = "ID_PWD_ENC"

.field public static final VALUE_JOB_ACCOUNTING_PWD_ENC:Ljava/lang/String; = "PWD_ENC"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
