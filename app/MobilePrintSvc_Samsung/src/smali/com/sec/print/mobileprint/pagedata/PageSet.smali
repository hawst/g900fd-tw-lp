.class public Lcom/sec/print/mobileprint/pagedata/PageSet;
.super Lcom/sec/print/mobileprint/pagedata/DocSetInterface;
.source "PageSet.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/PageSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field datas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/Page;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/PageSet$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/pagedata/PageSet$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/PageSet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/DocSetInterface;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/DocSetInterface;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/pagedata/PageSet;->readFromParcel(Landroid/os/Parcel;)V

    .line 35
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/print/mobileprint/pagedata/Page;)V
    .locals 1
    .param p1, "page"    # Lcom/sec/print/mobileprint/pagedata/Page;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/Page;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/pagedata/DocSetInterface;->readFromParcel(Landroid/os/Parcel;)V

    .line 73
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 74
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, Lcom/sec/print/mobileprint/pagedata/DocSetInterface;->writeToParcel(Landroid/os/Parcel;I)V

    .line 67
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/PageSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 68
    return-void
.end method
