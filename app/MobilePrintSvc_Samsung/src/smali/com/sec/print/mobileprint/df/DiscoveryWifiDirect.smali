.class public Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;
.super Landroid/content/BroadcastReceiver;
.source "DiscoveryWifiDirect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DiscoveryWifiDirect"


# instance fields
.field private final MP_DEBUG:Z

.field private final discoveryListener:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mManager:Landroid/net/wifi/p2p/WifiP2pManager;

.field final myPeerListListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# direct methods
.method public constructor <init>(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;)V
    .locals 2
    .param p1, "discoveryListener"    # Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->MP_DEBUG:Z

    .line 26
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 27
    iput-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 29
    new-instance v0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$1;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$1;-><init>(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->myPeerListListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    .line 71
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->discoveryListener:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;

    .line 72
    return-void
.end method

.method static synthetic access$0(Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;)Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->discoveryListener:Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect$DiscoveryListener;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 87
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    const-string v2, "wifi_p2p_state"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 92
    .local v1, "state":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v2, :cond_0

    .line 94
    const-string v2, "wifip2p"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 95
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 96
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 110
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string v2, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 102
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v2, :cond_0

    .line 103
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->myPeerListListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    goto :goto_0

    .line 105
    :cond_2
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 107
    const-string v2, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryWifiDirect;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 82
    :cond_0
    return-void
.end method
