.class public Lcom/sec/print/mobileprint/pagedata/FileImageData;
.super Lcom/sec/print/mobileprint/pagedata/ImageData;
.source "FileImageData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/pagedata/FileImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private fileFullPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/sec/print/mobileprint/pagedata/FileImageData$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/pagedata/FileImageData$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 40
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/pagedata/FileImageData;->readFromParcel(Landroid/os/Parcel;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/pagedata/FileImageData;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/pagedata/FileImageData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 53
    iput-object p1, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Rect;Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "areaRotatedCropArea"    # Landroid/graphics/Rect;
    .param p3, "eRotate"    # Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .prologue
    .line 79
    invoke-direct {p0, p2, p3}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>(Landroid/graphics/Rect;Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "eRotate"    # Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;

    .prologue
    .line 65
    invoke-direct {p0, p2}, Lcom/sec/print/mobileprint/pagedata/ImageData;-><init>(Lcom/sec/print/mobileprint/pagedata/ImageData$EnumRotate;)V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 67
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public getImageFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/sec/print/mobileprint/pagedata/ImageData;->readFromParcel(Landroid/os/Parcel;)V

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Lcom/sec/print/mobileprint/pagedata/ImageData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 98
    iget-object v0, p0, Lcom/sec/print/mobileprint/pagedata/FileImageData;->fileFullPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    return-void
.end method
