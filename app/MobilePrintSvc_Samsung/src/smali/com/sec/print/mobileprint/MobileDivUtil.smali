.class public Lcom/sec/print/mobileprint/MobileDivUtil;
.super Ljava/lang/Object;
.source "MobileDivUtil.java"


# static fields
.field private static final MODEL_INFO_XML:Ljava/lang/String; = "mobileprint_model_list.xml"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.print.mobileprint"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAssetManager(Landroid/content/Context;)Landroid/content/res/AssetManager;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v7, 0x0

    .line 24
    .local v7, "mobileDivAssetMgr":Landroid/content/res/AssetManager;
    const/4 v6, 0x0

    .line 26
    .local v6, "mobileAppAssetMgr":Landroid/content/res/AssetManager;
    const/4 v8, 0x0

    .line 27
    .local v8, "mobileDivAssetVersion":Ljava/lang/String;
    const/4 v9, 0x0

    .line 29
    .local v9, "mobilePrintAppAssetVersion":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    .line 32
    :try_start_0
    const-string v10, "mobileprint_model_list.xml"

    invoke-virtual {v7, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->getModelListVersion(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 41
    :goto_0
    invoke-static {p0}, Lcom/sec/print/mobileprint/MobileDivUtil;->isSamsungMobilePrintInstalled(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 42
    const/4 v2, 0x0

    .line 44
    .local v2, "appContext":Landroid/content/Context;
    :try_start_1
    const-string v10, "com.sec.print.mobileprint"

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 51
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    .line 54
    :try_start_2
    const-string v10, "mobileprint_model_list.xml"

    invoke-virtual {v6, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/print/mobileprint/utils/deviceinfoloader/DeviceInfoLoader;->getModelListVersion(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v9

    .line 70
    .end local v2    # "appContext":Landroid/content/Context;
    :cond_0
    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    .line 73
    if-eqz v8, :cond_1

    if-eqz v9, :cond_1

    .line 75
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyy-MM-dd"

    invoke-direct {v5, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 79
    .local v5, "formatter":Ljava/text/SimpleDateFormat;
    :try_start_3
    invoke-virtual {v5, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/print/mobileprint/MobileDivUtil;->removeTime(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v1

    .line 81
    .local v1, "MobileDivDate":Ljava/util/Date;
    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/print/mobileprint/MobileDivUtil;->removeTime(Ljava/util/Date;)Ljava/util/Date;
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    .line 90
    .local v0, "MobileAppDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v10

    if-gez v10, :cond_1

    move-object v7, v6

    .line 103
    .end local v0    # "MobileAppDate":Ljava/util/Date;
    .end local v1    # "MobileDivDate":Ljava/util/Date;
    .end local v5    # "formatter":Ljava/text/SimpleDateFormat;
    .end local v7    # "mobileDivAssetMgr":Landroid/content/res/AssetManager;
    :cond_1
    :goto_1
    return-object v7

    .line 33
    .restart local v7    # "mobileDivAssetMgr":Landroid/content/res/AssetManager;
    :catch_0
    move-exception v4

    .line 35
    .local v4, "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 45
    .end local v4    # "e1":Ljava/io/IOException;
    .restart local v2    # "appContext":Landroid/content/Context;
    :catch_1
    move-exception v3

    .line 47
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 55
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2
    move-exception v3

    .line 57
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 83
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v5    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_3
    move-exception v3

    .line 85
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1
.end method

.method private static isSamsungMobilePrintInstalled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "output":Z
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 111
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.sec.print.mobileprint"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 112
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 114
    const/4 v0, 0x1

    .line 117
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v0

    .line 116
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private static removeTime(Ljava/util/Date;)Ljava/util/Date;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 122
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 123
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 124
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 125
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 126
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 127
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method
