.class public Lcom/sec/print/mobileprint/printingservice/PrintingService;
.super Landroid/app/Service;
.source "PrintingService.java"


# instance fields
.field private final binder:Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;

.field final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/print/mobileprint/IPrintStatusCallback;",
            ">;"
        }
    .end annotation
.end field

.field public printJob:Lcom/sec/print/mobileprint/PrintJob;

.field statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 30
    new-instance v0, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;

    invoke-direct {v0, p0}, Lcom/sec/print/mobileprint/printingservice/PrintingService$1;-><init>(Lcom/sec/print/mobileprint/printingservice/PrintingService;)V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->binder:Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;

    .line 22
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->binder:Lcom/sec/print/mobileprint/ISamsungPrintingService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 98
    invoke-virtual {p0}, Lcom/sec/print/mobileprint/printingservice/PrintingService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 103
    .local v0, "assetMgr":Landroid/content/res/AssetManager;
    new-instance v1, Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    iget-object v2, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-direct {v1, v2}, Lcom/sec/print/mobileprint/PrintingStatusMonitor;-><init>(Landroid/os/RemoteCallbackList;)V

    iput-object v1, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    .line 104
    new-instance v1, Lcom/sec/print/mobileprint/PrintJob;

    iget-object v2, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->statusMonitor:Lcom/sec/print/mobileprint/PrintingStatusMonitor;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/print/mobileprint/PrintJob;-><init>(Landroid/content/Context;Lcom/sec/print/mobileprint/PrintingStatusMonitor;Landroid/content/res/AssetManager;)V

    iput-object v1, p0, Lcom/sec/print/mobileprint/printingservice/PrintingService;->printJob:Lcom/sec/print/mobileprint/PrintJob;

    .line 105
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 111
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    .line 117
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 123
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 128
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 129
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
