.class Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;
.super Ljava/lang/Object;
.source "MFPDiscoveryService.java"

# interfaces
.implements Ljavax/jmdns/ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/MFPDiscoveryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceListener"
.end annotation


# instance fields
.field final devices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/concurrent/ExecutorService;

.field final jmdns:Ljavax/jmdns/JmDNS;

.field final synthetic this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;


# direct methods
.method public constructor <init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljavax/jmdns/JmDNS;Ljava/util/Map;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2, "jmdns"    # Ljavax/jmdns/JmDNS;
    .param p4, "e"    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/jmdns/JmDNS;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/print/mobileprint/df/MFPDevice;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1521
    .local p3, "devices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/print/mobileprint/df/MFPDevice;>;"
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1522
    iput-object p2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->jmdns:Ljavax/jmdns/JmDNS;

    .line 1523
    iput-object p3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    .line 1524
    iput-object p4, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->e:Ljava/util/concurrent/ExecutorService;

    .line 1525
    return-void
.end method


# virtual methods
.method protected addDevice(Ljava/lang/String;Ljavax/jmdns/ServiceInfo;)V
    .locals 32
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "info"    # Ljavax/jmdns/ServiceInfo;

    .prologue
    .line 1572
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "[deviceListener] addDevice(mDNS) type: "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1574
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 1575
    :cond_0
    const-string v27, "MFPDiscoveryService"

    const-string v28, "[deviceListener] addDevice(mDNS) type or info is null"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    :cond_1
    :goto_0
    return-void

    .line 1579
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSamsungMFP(Ljavax/jmdns/ServiceInfo;)Z

    move-result v27

    if-nez v27, :cond_3

    .line 1580
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "[deviceListener] addDevice(mDNS) not supported device: "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1585
    :cond_3
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getInet4Addresses()[Ljava/net/Inet4Address;

    move-result-object v11

    .line 1586
    .local v11, "inet4Addresses":[Ljava/net/Inet4Address;
    array-length v0, v11

    move/from16 v29, v0

    const/16 v27, 0x0

    move/from16 v28, v27

    :goto_1
    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_1

    aget-object v10, v11, v28

    .line 1587
    .local v10, "inet4Address":Ljava/net/Inet4Address;
    const/4 v3, 0x0

    .line 1590
    .local v3, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    const/16 v18, 0x0

    .line 1591
    .local v18, "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    const/4 v6, 0x0

    .line 1593
    .local v6, "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    .line 1594
    .local v7, "host":Ljava/lang/String;
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] addDevice(mDNS) : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 1596
    const-string v27, "_uscan._tcp.local."

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 1597
    const-string v27, "MFPDiscoveryService"

    const-string v28, "[deviceListener] update eSCL"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    check-cast v3, Lcom/sec/print/mobileprint/df/MFPDevice;

    .line 1599
    .restart local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->ESCL:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setScanType(Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;)V

    .line 1602
    :cond_4
    const-string v27, "MFPDiscoveryService"

    const-string v28, "[deviceListener] already added : skip"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1797
    .end local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .end local v6    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .end local v7    # "host":Ljava/lang/String;
    .end local v10    # "inet4Address":Ljava/net/Inet4Address;
    .end local v11    # "inet4Addresses":[Ljava/net/Inet4Address;
    .end local v18    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :catch_0
    move-exception v4

    .line 1798
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1605
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .restart local v6    # "faxType":Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;
    .restart local v7    # "host":Ljava/lang/String;
    .restart local v10    # "inet4Address":Ljava/net/Inet4Address;
    .restart local v11    # "inet4Addresses":[Ljava/net/Inet4Address;
    .restart local v18    # "scanType":Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;
    :cond_5
    :try_start_1
    new-instance v3, Lcom/sec/print/mobileprint/df/MFPDevice;

    .end local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    invoke-direct {v3}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 1608
    .restart local v3    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    const-string v27, "usb_MFG"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1609
    .local v25, "usb_mfg":Ljava/lang/String;
    if-eqz v25, :cond_6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    if-gtz v27, :cond_7

    .line 1610
    :cond_6
    const-string v27, "mfg"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1613
    :cond_7
    const-string v27, "usb_MDL"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1614
    .local v24, "usb_mdl":Ljava/lang/String;
    if-eqz v24, :cond_8

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    if-gtz v27, :cond_9

    .line 1615
    :cond_8
    const-string v27, "mdl"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 1618
    :cond_9
    const/4 v13, 0x0

    .line 1619
    .local v13, "modelName":Ljava/lang/String;
    if-eqz v25, :cond_c

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    if-lez v27, :cond_c

    if-eqz v24, :cond_c

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    if-lez v27, :cond_c

    .line 1620
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v30, " "

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1637
    :cond_a
    :goto_2
    if-eqz v13, :cond_b

    .line 1638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getFixedModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1641
    :cond_b
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] modelName : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    # invokes: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->isSupportedPrinter(Ljava/lang/String;)Z
    invoke-static {v0, v13}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$20(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_f

    .line 1644
    const-string v27, "MFPDiscoveryService"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "[deviceListener] not supported model : "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1622
    :cond_c
    const-string v27, "product"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1623
    if-nez v13, :cond_d

    .line 1624
    const-string v27, "ty"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1625
    if-nez v13, :cond_a

    goto/16 :goto_0

    .line 1628
    :cond_d
    const-string v27, "("

    move-object/from16 v0, v27

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 1629
    const/16 v27, 0x1

    move/from16 v0, v27

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 1631
    :cond_e
    const-string v27, ")"

    move-object/from16 v0, v27

    invoke-virtual {v13, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 1632
    const/16 v27, 0x0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v30

    add-int/lit8 v30, v30, -0x1

    move/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_2

    .line 1649
    :cond_f
    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    .line 1650
    .local v8, "hostName":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_10

    .line 1651
    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    .line 1652
    .local v19, "serviceName":Ljava/lang/String;
    const-string v27, "("

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v20

    .line 1653
    .local v20, "start":I
    const-string v27, ")"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1655
    .local v5, "end":I
    if-ltz v20, :cond_14

    move/from16 v0, v20

    if-le v5, v0, :cond_14

    .line 1656
    add-int/lit8 v27, v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v27

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1667
    .end local v5    # "end":I
    .end local v19    # "serviceName":Ljava/lang/String;
    .end local v20    # "start":I
    :cond_10
    :goto_3
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] hostName : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1669
    const-string v27, "note"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1670
    .local v15, "note":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getPort()I

    move-result v16

    .line 1671
    .local v16, "port":I
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] note : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1672
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] port : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1674
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v30, " ("

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v30, ")"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 1675
    if-nez v15, :cond_11

    const-string v15, ""

    .end local v15    # "note":Ljava/lang/String;
    :cond_11
    invoke-virtual {v3, v15}, Lcom/sec/print/mobileprint/df/MFPDevice;->setNote(Ljava/lang/String;)V

    .line 1676
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setPort(I)V

    .line 1677
    sget-object v27, Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;->NETWORK:Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setDiscoveryType(Lcom/sec/print/mobileprint/df/MFPDevice$DiscoveryType;)V

    .line 1679
    const-string v27, "usb_CMD"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1680
    .local v2, "cmd":Ljava/lang/String;
    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    const/16 v30, 0x5

    move/from16 v0, v27

    move/from16 v1, v30

    if-le v0, v1, :cond_1b

    .line 1681
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1682
    const-string v27, "MFPDiscoveryService"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "[deviceListener] usb_CMD : "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1686
    move-object/from16 v26, v2

    .line 1687
    .local v26, "value":Ljava/lang/String;
    const-string v27, "MODE:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 1688
    .local v9, "index":I
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_12

    .line 1689
    add-int/lit8 v27, v9, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    .line 1690
    const-string v27, ";"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 1691
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_12

    .line 1692
    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    .line 1693
    const-string v27, ","

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 1694
    .local v14, "modes":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v30, v0

    const/16 v27, 0x0

    :goto_4
    move/from16 v0, v27

    move/from16 v1, v30

    if-lt v0, v1, :cond_15

    .line 1707
    .end local v14    # "modes":[Ljava/lang/String;
    :cond_12
    :goto_5
    move-object/from16 v26, v2

    .line 1708
    const-string v27, "MODE:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 1709
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_13

    .line 1710
    add-int/lit8 v27, v9, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    .line 1711
    const-string v27, ";"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 1712
    const/16 v27, -0x1

    move/from16 v0, v27

    if-eq v9, v0, :cond_13

    .line 1713
    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    .line 1714
    const-string v27, ","

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 1715
    .restart local v14    # "modes":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v30, v0

    const/16 v27, 0x0

    :goto_6
    move/from16 v0, v27

    move/from16 v1, v30

    if-lt v0, v1, :cond_17

    .line 1762
    .end local v9    # "index":I
    .end local v14    # "modes":[Ljava/lang/String;
    .end local v26    # "value":Ljava/lang/String;
    :cond_13
    :goto_7
    const-string v27, "_scanner._tcp.local."

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 1763
    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$21()Ljava/util/Set;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1d

    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->ESCL:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1779
    :goto_8
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setScanType(Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;)V

    .line 1780
    invoke-virtual {v3, v6}, Lcom/sec/print/mobileprint/df/MFPDevice;->setFaxType(Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;)V

    .line 1782
    const-string v27, "_ipp._tcp.local."

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_24

    .line 1783
    const-string v27, "rp"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1784
    .local v17, "rp":Ljava/lang/String;
    if-eqz v17, :cond_23

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v27

    if-lez v27, :cond_23

    .line 1785
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v30, "http://"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v30, ":"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getPort()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v30, "/"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 1786
    .local v23, "url":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 1787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1586
    .end local v17    # "rp":Ljava/lang/String;
    .end local v23    # "url":Ljava/lang/String;
    :goto_9
    add-int/lit8 v27, v28, 0x1

    move/from16 v28, v27

    goto/16 :goto_1

    .line 1659
    .end local v2    # "cmd":Ljava/lang/String;
    .end local v16    # "port":I
    .restart local v5    # "end":I
    .restart local v19    # "serviceName":Ljava/lang/String;
    .restart local v20    # "start":I
    :cond_14
    const-string v27, "["

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v20

    .line 1660
    const-string v27, "]"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1662
    if-ltz v20, :cond_10

    move/from16 v0, v20

    if-le v5, v0, :cond_10

    .line 1663
    add-int/lit8 v27, v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v27

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_3

    .line 1694
    .end local v5    # "end":I
    .end local v19    # "serviceName":Ljava/lang/String;
    .end local v20    # "start":I
    .restart local v2    # "cmd":Ljava/lang/String;
    .restart local v9    # "index":I
    .restart local v14    # "modes":[Ljava/lang/String;
    .restart local v16    # "port":I
    .restart local v26    # "value":Ljava/lang/String;
    :cond_15
    aget-object v12, v14, v27

    .line 1695
    .local v12, "mode":Ljava/lang/String;
    const-string v31, "SCN"

    move-object/from16 v0, v31

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_16

    .line 1697
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1698
    goto/16 :goto_5

    .line 1694
    :cond_16
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_4

    .line 1715
    .end local v12    # "mode":Ljava/lang/String;
    :cond_17
    aget-object v12, v14, v27

    .line 1716
    .restart local v12    # "mode":Ljava/lang/String;
    const-string v31, "FAX"

    move-object/from16 v0, v31

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_18

    .line 1717
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 1718
    goto/16 :goto_7

    .line 1719
    :cond_18
    const-string v31, "FAX2"

    move-object/from16 v0, v31

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_19

    .line 1720
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX2:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 1721
    goto/16 :goto_7

    .line 1722
    :cond_19
    const-string v31, "FAX3"

    move-object/from16 v0, v31

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1a

    .line 1723
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX3:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    .line 1724
    goto/16 :goto_7

    .line 1715
    :cond_1a
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_6

    .line 1751
    .end local v9    # "index":I
    .end local v12    # "mode":Ljava/lang/String;
    .end local v14    # "modes":[Ljava/lang/String;
    .end local v26    # "value":Ljava/lang/String;
    :cond_1b
    const-string v27, "Scan"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1752
    .local v22, "txtRecScan":Ljava/lang/String;
    if-eqz v22, :cond_1c

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    const-string v30, "T"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 1753
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1756
    :cond_1c
    const-string v27, "Fax"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 1757
    .local v21, "txtRecFax":Ljava/lang/String;
    if-eqz v21, :cond_13

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    const-string v30, "T"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 1758
    sget-object v6, Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;->FAX3:Lcom/sec/print/mobileprint/df/MFPDevice$FaxType;

    goto/16 :goto_7

    .line 1763
    .end local v21    # "txtRecFax":Ljava/lang/String;
    .end local v22    # "txtRecScan":Ljava/lang/String;
    :cond_1d
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    goto/16 :goto_8

    .line 1764
    :cond_1e
    const-string v27, "_uscan._tcp.local."

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 1765
    const-string v27, "MFPDiscoveryService"

    const-string v30, "[deviceListener] eSCL device"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1766
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->ESCL:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1767
    goto/16 :goto_8

    .line 1769
    :cond_1f
    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModels:Ljava/util/Set;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$22()Ljava/util/Set;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_20

    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsNet:Ljava/util/Set;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$23()Ljava/util/Set;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_21

    .line 1770
    :cond_20
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->SSIP:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1771
    goto/16 :goto_8

    :cond_21
    # getter for: Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->scannerModelsESCL:Ljava/util/Set;
    invoke-static {}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->access$21()Ljava/util/Set;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-interface {v0, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_22

    .line 1772
    sget-object v18, Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;->ESCL:Lcom/sec/print/mobileprint/df/MFPDevice$ScanType;

    .line 1773
    goto/16 :goto_8

    .line 1774
    :cond_22
    const/16 v18, 0x0

    goto/16 :goto_8

    .line 1789
    .restart local v17    # "rp":Ljava/lang/String;
    :cond_23
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v30, "http://"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v30, ":"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {p2 .. p2}, Ljavax/jmdns/ServiceInfo;->getPort()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 1790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    move-object/from16 v27, v0

    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_9

    .line 1793
    .end local v17    # "rp":Ljava/lang/String;
    :cond_24
    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 1794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    move-object/from16 v27, v0

    invoke-virtual {v10}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_9
.end method

.method public serviceAdded(Ljavax/jmdns/ServiceEvent;)V
    .locals 5
    .param p1, "event"    # Ljavax/jmdns/ServiceEvent;

    .prologue
    .line 1527
    const-string v2, "MFPDiscoveryService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deviceListener] serviceAdded: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    :try_start_0
    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getInfo()Ljavax/jmdns/ServiceInfo;

    move-result-object v1

    .line 1530
    .local v1, "info":Ljavax/jmdns/ServiceInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljavax/jmdns/ServiceInfo;->hasData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1531
    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->addDevice(Ljava/lang/String;Ljavax/jmdns/ServiceInfo;)V

    .line 1532
    const-string v2, "MFPDiscoveryService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deviceListener] event serviceResolved: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    .end local v1    # "info":Ljavax/jmdns/ServiceInfo;
    :goto_0
    return-void

    .line 1534
    .restart local v1    # "info":Ljavax/jmdns/ServiceInfo;
    :cond_0
    const-string v2, "MFPDiscoveryService"

    const-string v3, "[deviceListener] event null or no data"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1535
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener$1;-><init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;Ljavax/jmdns/ServiceEvent;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1552
    .end local v1    # "info":Ljavax/jmdns/ServiceInfo;
    :catch_0
    move-exception v0

    .line 1553
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public serviceRemoved(Ljavax/jmdns/ServiceEvent;)V
    .locals 4
    .param p1, "event"    # Ljavax/jmdns/ServiceEvent;

    .prologue
    .line 1558
    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1559
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1560
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->devices:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1562
    :cond_0
    const-string v1, "MFPDiscoveryService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[deviceListener] serviceRemoved: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1563
    return-void
.end method

.method public serviceResolved(Ljavax/jmdns/ServiceEvent;)V
    .locals 4
    .param p1, "event"    # Ljavax/jmdns/ServiceEvent;

    .prologue
    .line 1566
    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getInfo()Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    .line 1567
    .local v0, "info":Ljavax/jmdns/ServiceInfo;
    invoke-virtual {p1}, Ljavax/jmdns/ServiceEvent;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$DeviceListener;->addDevice(Ljava/lang/String;Ljavax/jmdns/ServiceInfo;)V

    .line 1568
    const-string v1, "MFPDiscoveryService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[deviceListener] serviceResolved: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljavax/jmdns/ServiceInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1569
    return-void
.end method
