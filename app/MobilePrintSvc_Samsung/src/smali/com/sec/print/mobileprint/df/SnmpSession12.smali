.class public Lcom/sec/print/mobileprint/df/SnmpSession12;
.super Ljava/lang/Object;
.source "SnmpSession12.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/SnmpSessionBase;


# instance fields
.field private final inetAddress:Ljava/net/InetAddress;

.field private final port:I

.field private final readCommunitry:Ljava/lang/String;

.field private final retryCount:I

.field private final version:I


# direct methods
.method public constructor <init>(Ljava/net/InetAddress;ILjava/lang/String;II)V
    .locals 0
    .param p1, "inetAddress"    # Ljava/net/InetAddress;
    .param p2, "version"    # I
    .param p3, "readCommunitry"    # Ljava/lang/String;
    .param p4, "retryCount"    # I
    .param p5, "port"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->inetAddress:Ljava/net/InetAddress;

    .line 26
    iput p2, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->version:I

    .line 27
    iput-object p3, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->readCommunitry:Ljava/lang/String;

    .line 28
    iput p4, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->retryCount:I

    .line 29
    iput p5, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->port:I

    .line 30
    return-void
.end method

.method private createCommunitytTarget(Ljava/lang/String;)Lorg/snmp4j/Target;
    .locals 5
    .param p1, "communityName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 41
    new-instance v0, Lorg/snmp4j/CommunityTarget;

    .line 42
    new-instance v2, Lorg/snmp4j/smi/UdpAddress;

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->inetAddress:Ljava/net/InetAddress;

    iget v4, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->port:I

    invoke-direct {v2, v3, v4}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 43
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v3, p1}, Lorg/snmp4j/smi/OctetString;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-direct {v0, v2, v3}, Lorg/snmp4j/CommunityTarget;-><init>(Lorg/snmp4j/smi/Address;Lorg/snmp4j/smi/OctetString;)V

    .line 44
    .local v0, "target":Lorg/snmp4j/CommunityTarget;
    iget v2, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->version:I

    if-ne v2, v1, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Lorg/snmp4j/CommunityTarget;->setVersion(I)V

    .line 45
    iget v1, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->retryCount:I

    invoke-virtual {v0, v1}, Lorg/snmp4j/CommunityTarget;->setRetries(I)V

    .line 47
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Lorg/snmp4j/CommunityTarget;->setTimeout(J)V

    .line 49
    return-object v0
.end method


# virtual methods
.method public createSnmpPdu()Lorg/snmp4j/PDU;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lorg/snmp4j/PDU;

    invoke-direct {v0}, Lorg/snmp4j/PDU;-><init>()V

    return-object v0
.end method

.method public getTarget(Lorg/snmp4j/Snmp;)Lorg/snmp4j/Target;
    .locals 1
    .param p1, "snmp"    # Lorg/snmp4j/Snmp;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/SnmpSession12;->readCommunitry:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/print/mobileprint/df/SnmpSession12;->createCommunitytTarget(Ljava/lang/String;)Lorg/snmp4j/Target;

    move-result-object v0

    return-object v0
.end method
