.class Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;
.super Ljava/lang/Object;
.source "DiscoveryIpRange.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/print/mobileprint/df/DiscoveryIpRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceChecker"
.end annotation


# instance fields
.field private final address:Ljava/net/InetAddress;

.field final synthetic this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;Ljava/net/InetAddress;)V
    .locals 0
    .param p2, "address"    # Ljava/net/InetAddress;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    .line 45
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 50
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v5, "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1Discovery:Z
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$7(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2Discovery:Z
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$8(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp3Discovery:Z
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$9(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->timeout:J
    invoke-static {v1}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$10(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp1ReadComunity:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$11(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->snmp2ReadComunity:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$12(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/sec/print/mobileprint/df/DeviceDetector;->pingHost(Ljava/net/InetAddress;JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v8

    .line 61
    .local v8, "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 62
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->newDeviceFound(Ljava/lang/String;I)V

    .line 68
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->percentComplete:D
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$13(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)D

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->progress:D
    invoke-static {v3}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$14(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)D

    move-result-wide v3

    add-double/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$15(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;D)V

    .line 70
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$16(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :try_start_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$16(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    :goto_1
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$16(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;
    invoke-static {v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$17(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 70
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 82
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$6(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 84
    .end local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .end local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :goto_2
    return-void

    .line 63
    .restart local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .restart local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :cond_4
    const/4 v0, 0x2

    :try_start_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->newDeviceFound(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 79
    .end local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .end local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v7

    .line 80
    .local v7, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 82
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$6(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_2

    .line 65
    .end local v7    # "e":Ljava/lang/InterruptedException;
    .restart local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .restart local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :cond_5
    const/4 v0, 0x1

    :try_start_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->address:Ljava/net/InetAddress;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->newDeviceFound(Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 81
    .end local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .end local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v0

    .line 82
    iget-object v1, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->semaphore:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$6(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 83
    throw v0

    .line 73
    .restart local v5    # "pingRequests":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .restart local v8    # "pingResults":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :cond_6
    :try_start_5
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->checkedAddresses:Ljava/util/Set;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$16(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;
    invoke-static {v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$17(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->firstNonCheckedAddress:Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$17(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)Ljava/net/InetAddress;

    move-result-object v0

    # invokes: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressToArray(Ljava/net/InetAddress;)[I
    invoke-static {v0}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$18(Ljava/net/InetAddress;)[I

    move-result-object v6

    .line 75
    .local v6, "bytes":[I
    # invokes: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->increaseAddressArray([I)V
    invoke-static {v6}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$19([I)V

    .line 76
    iget-object v0, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/DiscoveryIpRange$DeviceChecker;->this$0:Lcom/sec/print/mobileprint/df/DiscoveryIpRange;

    # getter for: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->addressScopeId:I
    invoke-static {v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$20(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;)I

    move-result v2

    # invokes: Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->arrayToAddress([II)Ljava/net/InetAddress;
    invoke-static {v6, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$21([II)Ljava/net/InetAddress;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/print/mobileprint/df/DiscoveryIpRange;->access$22(Lcom/sec/print/mobileprint/df/DiscoveryIpRange;Ljava/net/InetAddress;)V

    goto/16 :goto_1

    .line 70
    .end local v6    # "bytes":[I
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
