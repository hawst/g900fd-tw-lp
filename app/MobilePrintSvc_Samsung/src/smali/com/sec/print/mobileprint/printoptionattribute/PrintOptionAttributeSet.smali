.class public Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;
.super Ljava/lang/Object;
.source "PrintOptionAttributeSet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field datas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->readFromParcel(Landroid/os/Parcel;)V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public add(Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;)Z
    .locals 4
    .param p1, "attribute"    # Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    .prologue
    .line 54
    iget-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    iget-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 54
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    .line 55
    .local v0, "data":Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public get(Ljava/lang/Class;)Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;",
            ">;)",
            "Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "category":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;>;"
    iget-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 83
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 78
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    .line 79
    .local v0, "data":Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 139
    return-void
.end method

.method public remove(Ljava/lang/Class;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "category":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;>;"
    iget-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 119
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 109
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;

    .line 110
    .local v0, "data":Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    iget-object v1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 112
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PrintOptionAttributeSet;->datas:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 133
    return-void
.end method
