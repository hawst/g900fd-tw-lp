.class public Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;
.super Ljava/lang/Object;
.source "GCPOutputInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printerinfo/IOutputInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private SelectedAccount:Ljava/lang/String;

.field private SelectedAccountToken:Ljava/lang/String;

.field private SelectedDeviceid:Ljava/lang/String;

.field private SelectedPrinterName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    .line 27
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mSelectedPrinterUserName"    # Ljava/lang/String;
    .param p2, "mSelectedPrinterPassword"    # Ljava/lang/String;
    .param p3, "mSelectedPrinterIP"    # Ljava/lang/String;
    .param p4, "mSelectedPrinterName"    # Ljava/lang/String;
    .param p5, "mSharedPrinterName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    .line 18
    iput-object p1, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 22
    iput-object p5, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedAccount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedAccountToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedDeviceid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedPrinterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedAccountToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedDeviceid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/sec/print/mobileprint/printerinfo/GCPOutputInfo;->SelectedPrinterName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    return-void
.end method
