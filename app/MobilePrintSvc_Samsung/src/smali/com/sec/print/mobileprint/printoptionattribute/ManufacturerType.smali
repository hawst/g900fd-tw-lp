.class public Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;
.super Ljava/lang/Object;
.source "ManufacturerType.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DELL:I = 0x2

.field public static final SAMSUNG:I = 0x1


# instance fields
.field manufacturerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 59
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    .line 14
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->readFromParcel(Landroid/os/Parcel;)V

    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public getManufacturerType()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    .line 45
    return-void
.end method

.method public setManufacturerType(I)V
    .locals 0
    .param p1, "manufacturerType"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    .line 28
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/ManufacturerType;->manufacturerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 40
    return-void
.end method
