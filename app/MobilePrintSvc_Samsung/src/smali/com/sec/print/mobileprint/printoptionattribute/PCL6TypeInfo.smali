.class public Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;
.super Ljava/lang/Object;
.source "PCL6TypeInfo.java"

# interfaces
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPDLTypeInfo;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final PCL6_COMP_TYPE_BANDED_DELTA_ROW:I = 0x1

.field public static final PCL6_COMP_TYPE_BANDED_JPG:I = 0x2

.field public static final PCL6_COMP_TYPE_BANDED_RLE:I = 0x4

.field public static final PCL6_COMP_TYPE_FULL_IMAGE_JPG_FILE:I = 0x3


# instance fields
.field private pcl6CompressionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 62
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "compType"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->pcl6CompressionType:I

    .line 22
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getCompType()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->pcl6CompressionType:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->pcl6CompressionType:I

    .line 48
    return-void
.end method

.method public setCompType(I)V
    .locals 0
    .param p1, "compType"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->pcl6CompressionType:I

    .line 30
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/PCL6TypeInfo;->pcl6CompressionType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    return-void
.end method
