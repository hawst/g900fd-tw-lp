.class public Lcom/sec/print/mobileprint/io/SPLOutputStream;
.super Ljava/lang/Object;
.source "SPLOutputStream.java"


# instance fields
.field private m_Writer:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/print/mobileprint/io/SPLOutputStream;->m_Writer:Ljava/io/OutputStream;

    .line 17
    return-void
.end method


# virtual methods
.method public write([BII)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I

    .prologue
    .line 22
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPLOutputStream;->m_Writer:Ljava/io/OutputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 23
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p3, :cond_0

    .line 29
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 24
    .restart local v1    # "i":I
    :cond_0
    aget-byte v2, p1, v1

    invoke-static {v2}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/print/mobileprint/MPLogger;->v(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
