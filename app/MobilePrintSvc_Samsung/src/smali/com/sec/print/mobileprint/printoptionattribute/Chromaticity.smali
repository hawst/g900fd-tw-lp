.class public Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;
.super Ljava/lang/Object;
.source "Chromaticity.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private chromaticityType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 22
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->readFromParcel(Landroid/os/Parcel;)V

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V
    .locals 1
    .param p1, "eChromaticity"    # Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->chromaticityType:I

    .line 64
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getChromaticity()Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;
    .locals 6

    .prologue
    .line 88
    const-class v1, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    invoke-virtual {v1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_1

    .line 96
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 88
    :cond_1
    aget-object v0, v1, v2

    .line 90
    .local v0, "enumData":Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;
    invoke-virtual {v0}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->getValue()I

    move-result v4

    iget v5, p0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->chromaticityType:I

    if-eq v4, v5, :cond_0

    .line 88
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->chromaticityType:I

    .line 120
    return-void
.end method

.method public setChromaticity(Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;)V
    .locals 1
    .param p1, "eChromaticity"    # Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity$EnumChromaticity;->getValue()I

    move-result v0

    iput v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->chromaticityType:I

    .line 79
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/Chromaticity;->chromaticityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    return-void
.end method
