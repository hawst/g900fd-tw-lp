.class Lcom/sec/print/mobileprint/df/SmbDiscovery$1;
.super Ljava/lang/Object;
.source "SmbDiscovery.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/df/SmbDiscovery;->startDiscovery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

.field private final synthetic val$domain:Ljava/lang/String;

.field private final synthetic val$host:Ljava/lang/String;

.field private final synthetic val$password:Ljava/lang/String;

.field private final synthetic val$user:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/SmbDiscovery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    iput-object p2, p0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$host:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$domain:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$user:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$password:Ljava/lang/String;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    .prologue
    .line 56
    const/4 v11, 0x0

    .line 58
    .local v11, "handle":Ljcifs/dcerpc/DcerpcHandle;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$host:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v12

    .line 60
    .local v12, "inetAddress":Ljava/net/InetAddress;
    new-instance v22, Ljcifs/smb/SmbFile;

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "smb://"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$host:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    new-instance v24, Ljcifs/smb/NtlmPasswordAuthentication;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$domain:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$user:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$password:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-direct/range {v24 .. v27}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v22 .. v24}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    invoke-virtual/range {v22 .. v22}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v18

    .line 61
    .local v18, "shares":[Ljcifs/smb/SmbFile;
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v23, v0

    const/16 v22, 0x0

    :goto_0
    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_2

    .line 148
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljcifs/dcerpc/DcerpcException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_1
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b

    .line 180
    .end local v12    # "inetAddress":Ljava/net/InetAddress;
    .end local v18    # "shares":[Ljcifs/smb/SmbFile;
    :cond_1
    :goto_1
    return-void

    .line 61
    .restart local v12    # "inetAddress":Ljava/net/InetAddress;
    .restart local v18    # "shares":[Ljcifs/smb/SmbFile;
    :cond_2
    :try_start_2
    aget-object v19, v18, v22

    .line 62
    .local v19, "smbFile":Ljcifs/smb/SmbFile;
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v24

    if-nez v24, :cond_0

    .line 64
    invoke-virtual/range {v19 .. v19}, Ljcifs/smb/SmbFile;->getType()I

    move-result v21

    .line 65
    .local v21, "type":I
    const/16 v24, 0x20

    move/from16 v0, v21

    move/from16 v1, v24

    if-ne v0, v1, :cond_6

    .line 66
    new-instance v16, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "\\\\"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljcifs/smb/SmbFile;->getServer()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "\\"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v19 .. v19}, Ljcifs/smb/SmbFile;->getShare()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;-><init>(Ljava/lang/String;)V

    .line 67
    .local v16, "openRpc":Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;
    if-nez v11, :cond_3

    .line 68
    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "ncacn_np:"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    .line 70
    const-string v25, "[\\PIPE\\spoolss]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    .line 68
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 70
    new-instance v25, Ljcifs/smb/NtlmPasswordAuthentication;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$domain:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$user:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->val$password:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v25 .. v28}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static/range {v24 .. v25}, Ljcifs/dcerpc/DcerpcHandle;->getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;

    move-result-object v11

    .line 72
    :cond_3
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    .line 73
    move-object/from16 v0, v16

    iget v0, v0, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;->returnCode:I

    move/from16 v24, v0

    if-nez v24, :cond_6

    .line 74
    new-instance v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;

    move-object/from16 v0, v16

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;->handler:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-direct {v13, v0}, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;-><init>([B)V

    .line 75
    .local v13, "infoRpc":Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;
    invoke-virtual {v11, v13}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    .line 76
    iget v0, v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;->returnCode:I

    move/from16 v24, v0

    const/16 v25, 0x7a

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    iget v0, v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;->needed:I

    move/from16 v24, v0

    if-lez v24, :cond_5

    .line 77
    new-instance v7, Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;

    invoke-direct {v7}, Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;-><init>()V

    .line 78
    .local v7, "data":Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;
    iget v0, v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;->needed:I

    move/from16 v24, v0

    move/from16 v0, v24

    new-array v0, v0, [B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iput-object v0, v7, Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;->data:[B

    .line 79
    new-instance v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;

    .end local v13    # "infoRpc":Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;
    move-object/from16 v0, v16

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;->handler:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-direct {v13, v0, v7}, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;-><init>([BLjcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;)V

    .line 80
    .restart local v13    # "infoRpc":Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;
    invoke-virtual {v11, v13}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    .line 81
    iget v0, v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;->returnCode:I

    move/from16 v24, v0

    if-nez v24, :cond_5

    .line 82
    iget-object v0, v13, Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;->info:Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v4, v0, Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;->data:[B

    .line 83
    .local v4, "buffer":[B
    const/4 v5, 0x0

    .line 84
    .local v5, "bufferIndex":I
    move/from16 v20, v5

    .line 87
    .local v20, "start":I
    add-int/lit8 v5, v5, 0x4

    .line 89
    add-int/lit8 v5, v5, 0x4

    .line 91
    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readInt4([BI)I
    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$1([BI)I

    move-result v15

    .line 92
    .local v15, "offset":I
    add-int/lit8 v5, v5, 0x4

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v24, v0

    add-int/lit8 v25, v15, 0x0

    const/16 v26, 0x100

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readString([BII)Ljava/lang/String;
    invoke-static {v0, v4, v1, v2}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$2(Lcom/sec/print/mobileprint/df/SmbDiscovery;[BII)Ljava/lang/String;

    move-result-object v17

    .line 95
    .local v17, "sharename":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x4

    .line 97
    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readInt4([BI)I
    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$1([BI)I

    move-result v15

    .line 98
    add-int/lit8 v5, v5, 0x4

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v24, v0

    add-int/lit8 v25, v15, 0x0

    const/16 v26, 0x100

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readString([BII)Ljava/lang/String;
    invoke-static {v0, v4, v1, v2}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$2(Lcom/sec/print/mobileprint/df/SmbDiscovery;[BII)Ljava/lang/String;

    move-result-object v9

    .line 101
    .local v9, "drivername":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x4

    .line 103
    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readInt4([BI)I
    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$1([BI)I

    move-result v15

    .line 104
    add-int/lit8 v5, v5, 0x4

    .line 105
    const-string v14, ""

    .line 106
    .local v14, "location":Ljava/lang/String;
    if-eqz v15, :cond_4

    .line 107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v24, v0

    add-int/lit8 v25, v15, 0x0

    const/16 v26, 0x200

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readString([BII)Ljava/lang/String;
    invoke-static {v0, v4, v1, v2}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$2(Lcom/sec/print/mobileprint/df/SmbDiscovery;[BII)Ljava/lang/String;

    move-result-object v14

    .line 110
    :cond_4
    add-int/lit8 v5, v5, 0x4

    .line 112
    add-int/lit8 v5, v5, 0x4

    .line 114
    add-int/lit8 v5, v5, 0x4

    .line 116
    add-int/lit8 v5, v5, 0x4

    .line 118
    add-int/lit8 v5, v5, 0x4

    .line 120
    add-int/lit8 v5, v5, 0x4

    .line 122
    # invokes: Lcom/sec/print/mobileprint/df/SmbDiscovery;->readInt4([BI)I
    invoke-static {v4, v5}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$1([BI)I

    move-result v3

    .line 123
    .local v3, "attributes":I
    add-int/lit8 v5, v5, 0x4

    .line 131
    add-int/lit8 v5, v5, 0x1c

    .line 133
    and-int/lit8 v24, v3, 0x8

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 134
    new-instance v8, Lcom/sec/print/mobileprint/df/MFPDevice;

    invoke-direct {v8}, Lcom/sec/print/mobileprint/df/MFPDevice;-><init>()V

    .line 135
    .local v8, "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v25, " ("

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ")"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setName(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v8, v14}, Lcom/sec/print/mobileprint/df/MFPDevice;->setNote(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setHost(Ljava/lang/String;)V

    .line 138
    const/16 v24, 0x1bd

    move/from16 v0, v24

    invoke-virtual {v8, v0}, Lcom/sec/print/mobileprint/df/MFPDevice;->setPort(I)V

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->devices:Ljava/util/List;
    invoke-static/range {v24 .. v24}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$3(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/List;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    .end local v3    # "attributes":I
    .end local v4    # "buffer":[B
    .end local v5    # "bufferIndex":I
    .end local v7    # "data":Ljcifs/dcerpc/msrpc/spoolss$spoolss_DataBlob;
    .end local v8    # "device":Lcom/sec/print/mobileprint/df/MFPDevice;
    .end local v9    # "drivername":Ljava/lang/String;
    .end local v14    # "location":Ljava/lang/String;
    .end local v15    # "offset":I
    .end local v17    # "sharename":Ljava/lang/String;
    .end local v20    # "start":I
    :cond_5
    new-instance v6, Ljcifs/dcerpc/msrpc/MsrpcClosePrinter;

    move-object/from16 v0, v16

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;->handler:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-direct {v6, v0}, Ljcifs/dcerpc/msrpc/MsrpcClosePrinter;-><init>([B)V

    .line 144
    .local v6, "closeRpc":Ljcifs/dcerpc/msrpc/MsrpcClosePrinter;
    invoke-virtual {v11, v6}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V
    :try_end_2
    .catch Ljcifs/smb/SmbException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljcifs/dcerpc/DcerpcException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    .end local v6    # "closeRpc":Ljcifs/dcerpc/msrpc/MsrpcClosePrinter;
    .end local v13    # "infoRpc":Ljcifs/dcerpc/msrpc/MsrpcGetPrinter;
    .end local v16    # "openRpc":Ljcifs/dcerpc/msrpc/MsrpcOpenPrinter;
    :cond_6
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_0

    .line 149
    .end local v12    # "inetAddress":Ljava/net/InetAddress;
    .end local v18    # "shares":[Ljcifs/smb/SmbFile;
    .end local v19    # "smbFile":Ljcifs/smb/SmbFile;
    .end local v21    # "type":I
    :catch_0
    move-exception v10

    .line 150
    .local v10, "e":Ljcifs/smb/SmbException;
    :try_start_3
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v22

    const-class v23, Ljcifs/smb/SmbAuthException;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x3

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 155
    :goto_2
    const-string v22, "SmbDiscovery"

    invoke-virtual {v10}, Ljcifs/smb/SmbException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_4
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    .line 177
    :catch_1
    move-exception v22

    goto/16 :goto_1

    .line 153
    :cond_7
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x1

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 172
    .end local v10    # "e":Ljcifs/smb/SmbException;
    :catchall_0
    move-exception v22

    .line 174
    if-eqz v11, :cond_8

    .line 175
    :try_start_6
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_a

    .line 179
    :cond_8
    :goto_3
    throw v22

    .line 156
    :catch_2
    move-exception v10

    .line 157
    .local v10, "e":Ljava/net/MalformedURLException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x1

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 158
    const-string v22, "SmbDiscovery"

    invoke-virtual {v10}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_8
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_1

    .line 177
    :catch_3
    move-exception v22

    goto/16 :goto_1

    .line 159
    .end local v10    # "e":Ljava/net/MalformedURLException;
    :catch_4
    move-exception v10

    .line 160
    .local v10, "e":Ljava/net/UnknownHostException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x2

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 161
    const-string v22, "SmbDiscovery"

    invoke-virtual {v10}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_a
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_1

    .line 177
    :catch_5
    move-exception v22

    goto/16 :goto_1

    .line 162
    .end local v10    # "e":Ljava/net/UnknownHostException;
    :catch_6
    move-exception v10

    .line 163
    .local v10, "e":Ljcifs/dcerpc/DcerpcException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x1

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 164
    const-string v22, "SmbDiscovery"

    invoke-virtual {v10}, Ljcifs/dcerpc/DcerpcException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_c
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto/16 :goto_1

    .line 177
    :catch_7
    move-exception v22

    goto/16 :goto_1

    .line 165
    .end local v10    # "e":Ljcifs/dcerpc/DcerpcException;
    :catch_8
    move-exception v10

    .line 166
    .local v10, "e":Ljava/io/IOException;
    :try_start_d
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v22

    const-class v23, Ljcifs/smb/SmbAuthException;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x3

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 171
    :goto_4
    const-string v22, "SmbDiscovery"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 174
    if-eqz v11, :cond_1

    .line 175
    :try_start_e
    invoke-virtual {v11}, Ljcifs/dcerpc/DcerpcHandle;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    goto/16 :goto_1

    .line 177
    :catch_9
    move-exception v22

    goto/16 :goto_1

    .line 169
    :cond_9
    :try_start_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/print/mobileprint/df/SmbDiscovery$1;->this$0:Lcom/sec/print/mobileprint/df/SmbDiscovery;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/print/mobileprint/df/SmbDiscovery;->state:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static/range {v22 .. v22}, Lcom/sec/print/mobileprint/df/SmbDiscovery;->access$0(Lcom/sec/print/mobileprint/df/SmbDiscovery;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v22

    const/16 v23, -0x1

    invoke-virtual/range {v22 .. v23}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_4

    .line 177
    .end local v10    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v23

    goto/16 :goto_3

    .restart local v12    # "inetAddress":Ljava/net/InetAddress;
    .restart local v18    # "shares":[Ljcifs/smb/SmbFile;
    :catch_b
    move-exception v22

    goto/16 :goto_1
.end method
