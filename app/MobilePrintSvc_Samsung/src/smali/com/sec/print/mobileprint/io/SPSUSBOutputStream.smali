.class public Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;
.super Ljava/lang/Object;
.source "SPSUSBOutputStream.java"

# interfaces
.implements Lcom/sec/print/mobileprint/io/ISPSOutputStream;


# static fields
.field static final TIMEOUT:I = 0x1f4


# instance fields
.field mProductId:I

.field mVendorId:I

.field usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "productId"    # I
    .param p3, "vendorId"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p2, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->mProductId:I

    .line 18
    iput p3, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->mVendorId:I

    .line 19
    invoke-static {p1}, Lcom/sec/print/sf/usbsdk/SUSB;->Initialize(Landroid/content/Context;)Z

    .line 20
    new-instance v0, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    invoke-direct {v0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;-><init>()V

    iput-object v0, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    .line 21
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public close()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->close()I

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-static {}, Lcom/sec/print/sf/usbsdk/SUSB;->Deinitialize()V

    .line 61
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnect()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    invoke-virtual {v0}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public open()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 27
    iget-object v2, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    iget v3, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->mVendorId:I

    iget v4, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->mProductId:I

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->open(IIZZ)I

    move-result v2

    if-nez v2, :cond_0

    .line 30
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public write([BI)Z
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "length"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/print/mobileprint/io/SPSUSBOutputStream;->usbDevicePort:Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;

    const/16 v1, 0x1f4

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/print/sf/usbsdk/SAndroidUSBDevicePort;->write([BII)I

    move-result v0

    if-lez v0, :cond_0

    .line 49
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
