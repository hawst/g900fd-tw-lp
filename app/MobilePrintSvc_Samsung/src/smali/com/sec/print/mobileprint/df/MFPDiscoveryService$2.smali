.class Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;
.super Ljava/lang/Object;
.source "MFPDiscoveryService.java"

# interfaces
.implements Lcom/sec/print/mobileprint/df/DiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getIPByMacAddress(Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

.field private final synthetic val$arg0:Ljava/lang/String;

.field private final synthetic val$devices:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/print/mobileprint/df/MFPDiscoveryService;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    iput-object p2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->val$arg0:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->val$devices:Ljava/util/List;

    .line 1115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public newDeviceFound(Ljava/lang/String;I)V
    .locals 5
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    .line 1123
    :try_start_0
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->this$0:Lcom/sec/print/mobileprint/df/MFPDiscoveryService;

    invoke-virtual {v2, p1}, Lcom/sec/print/mobileprint/df/MFPDiscoveryService;->getMacAddressBySNMP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1124
    .local v1, "macAddress":Ljava/lang/String;
    const-string v2, "MFPDiscoveryService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "found device MacAddress:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ipaddress"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->val$arg0:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1126
    const-string v2, "MFPDiscoveryService"

    const-string v3, "device matched"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    iget-object v2, p0, Lcom/sec/print/mobileprint/df/MFPDiscoveryService$2;->val$devices:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1133
    .end local v1    # "macAddress":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1130
    :catch_0
    move-exception v0

    .line 1131
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public newDeviceFoundException(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 1118
    return-void
.end method
