.class public Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;
.super Ljava/lang/Object;
.source "SecurePrint.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private id:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->readFromParcel(Landroid/os/Parcel;)V

    .line 17
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "userID"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    .line 11
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/SecurePrint;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    return-void
.end method
