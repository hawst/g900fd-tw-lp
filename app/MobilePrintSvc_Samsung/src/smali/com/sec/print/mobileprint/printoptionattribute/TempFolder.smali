.class public Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;
.super Ljava/lang/Object;
.source "TempFolder.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/print/mobileprint/printoptionattribute/IPrintOptionAttribute;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private folderPath:Ljava/lang/String;

.field private prefixString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder$1;

    invoke-direct {v0}, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder$1;-><init>()V

    sput-object v0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 75
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->folderPath:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->prefixString:Ljava/lang/String;

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "prefixString"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->folderPath:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->prefixString:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->folderPath:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->prefixString:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public readToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->folderPath:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->prefixString:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->folderPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/print/mobileprint/printoptionattribute/TempFolder;->prefixString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    return-void
.end method
