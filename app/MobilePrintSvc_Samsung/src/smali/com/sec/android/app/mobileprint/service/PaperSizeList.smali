.class public Lcom/sec/android/app/mobileprint/service/PaperSizeList;
.super Ljava/lang/Object;
.source "PaperSizeList.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/mobileprint/service/PaperSizeList;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "PaperSizeList"


# instance fields
.field private mPaperSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$1;

    invoke-direct {v0}, Lcom/sec/android/app/mobileprint/service/PaperSizeList$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    const-class v1, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/mobileprint/service/PaperSizeList;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/mobileprint/service/PaperSizeList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public addPaper(Ljava/lang/String;II)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 54
    const-string v0, "PaperSizeList"

    const-string v1, "paper name is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public getPaperHeight(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 83
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    iget v0, v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;->mHeight:I

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPaperName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 69
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    iget-object v0, v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;->mName:Ljava/lang/String;

    .line 72
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPaperNameList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v1, "paperNameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 65
    return-object v1

    .line 63
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    iget-object v2, v2, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getPaperWidth(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 76
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;

    iget v0, v0, Lcom/sec/android/app/mobileprint/service/PaperSizeList$PaperSize;->mWidth:I

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PaperSizeList ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PaperSizeList;->mPaperSizes:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 97
    return-void
.end method
