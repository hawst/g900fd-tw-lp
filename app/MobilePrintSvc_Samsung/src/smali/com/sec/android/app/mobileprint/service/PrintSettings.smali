.class public Lcom/sec/android/app/mobileprint/service/PrintSettings;
.super Ljava/lang/Object;
.source "PrintSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/mobileprint/service/PrintSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "PrintSettings"


# instance fields
.field private mAlign:I

.field private mColorType:I

.field private mContentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCopies:I

.field private mDuplexType:I

.field private mLanguageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMargin:I

.field private mOrientationType:I

.field private mPaperSize:I

.field private mPaperType:I

.field private mRangeEnd:I

.field private mRangeStart:I

.field private mScale:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/mobileprint/service/PrintSettings$1;

    invoke-direct {v0}, Lcom/sec/android/app/mobileprint/service/PrintSettings$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    .line 69
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    .line 70
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    .line 71
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    .line 72
    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    .line 73
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    .line 76
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    .line 77
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    .line 78
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    .line 80
    iput v2, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    .line 81
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    .line 99
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/mobileprint/service/PrintSettings;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/mobileprint/service/PrintSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public addContent(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 211
    if-nez p1, :cond_0

    .line 212
    const-string v0, "PrintSettings"

    const-string v1, "filepath is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_0
    return-void

    .line 216
    :cond_0
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 217
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    .line 220
    :cond_1
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    iget-object v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 221
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addLanguage(Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 194
    if-nez p1, :cond_0

    .line 195
    const-string v0, "PrintSettings"

    const-string v1, "language is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method public getAlign()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    return v0
.end method

.method public getColorType()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    return v0
.end method

.method public getContentList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCopies()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    return v0
.end method

.method public getDuplexType()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    return v0
.end method

.method public getLanguageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMargin()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    return v0
.end method

.method public getOrientationType()I
    .locals 2

    .prologue
    .line 131
    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    packed-switch v1, :pswitch_data_0

    .line 139
    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    .line 140
    const/4 v0, 0x2

    .line 145
    .local v0, "orientation":I
    :goto_0
    return v0

    .line 135
    .end local v0    # "orientation":I
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    .line 136
    .restart local v0    # "orientation":I
    goto :goto_0

    .line 142
    .end local v0    # "orientation":I
    :cond_0
    const/4 v0, 0x1

    .restart local v0    # "orientation":I
    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getOrientationTypeForUI()I
    .locals 2

    .prologue
    .line 150
    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    packed-switch v1, :pswitch_data_0

    .line 156
    const/4 v0, 0x0

    .line 158
    .local v0, "orientation":I
    :goto_0
    return v0

    .line 153
    .end local v0    # "orientation":I
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    .line 154
    .restart local v0    # "orientation":I
    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getPaperSize()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    return v0
.end method

.method public getPaperType()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    return v0
.end method

.method public getRangeEnd()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    return v0
.end method

.method public getRangeStart()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    return v0
.end method

.method public getScale()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    return v0
.end method

.method public removeAllContent()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 229
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    .line 230
    iput v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    .line 231
    return-void
.end method

.method public removeAllLanguage()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 204
    return-void
.end method

.method public setAlign(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 254
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    .line 255
    return-void
.end method

.method public setColorType(I)V
    .locals 3
    .param p1, "colorType"    # I

    .prologue
    .line 170
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 171
    :cond_0
    const-string v0, "PrintSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid colorType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_1
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    goto :goto_0
.end method

.method public setCopies(I)V
    .locals 3
    .param p1, "copies"    # I

    .prologue
    .line 106
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/16 v0, 0x3e7

    if-le p1, v0, :cond_1

    .line 107
    :cond_0
    const-string v0, "PrintSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid number of copies : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :goto_0
    return-void

    .line 110
    :cond_1
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    goto :goto_0
.end method

.method public setDuplexType(I)V
    .locals 3
    .param p1, "duplexType"    # I

    .prologue
    .line 182
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 183
    :cond_0
    const-string v0, "PrintSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid duplexType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_1
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    goto :goto_0
.end method

.method public setMargin(I)V
    .locals 0
    .param p1, "margin"    # I

    .prologue
    .line 238
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    .line 239
    return-void
.end method

.method public setOrientationType(I)V
    .locals 0
    .param p1, "orientationType"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    .line 163
    return-void
.end method

.method public setPaperSize(I)V
    .locals 0
    .param p1, "paperSize"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    .line 119
    return-void
.end method

.method public setPaperType(I)V
    .locals 0
    .param p1, "paperType"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    .line 127
    return-void
.end method

.method public setRange(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 266
    if-ltz p1, :cond_0

    if-ge p2, p1, :cond_1

    .line 267
    :cond_0
    const-string v0, "PrintSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid range : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ~ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :goto_0
    return-void

    .line 270
    :cond_1
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    .line 271
    iput p2, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    goto :goto_0
.end method

.method public setScale(I)V
    .locals 0
    .param p1, "scale"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    .line 247
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PrintSettings [Copies="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PaperSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PaperType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 299
    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", OrientationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ColorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 300
    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DuplexType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLanguageList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 301
    const-string v1, ", ContentList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Margin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 302
    const-string v1, ", Align="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", RangeStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", RangeEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 303
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 281
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mCopies:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 282
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 283
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mPaperType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 284
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mOrientationType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mColorType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 286
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mDuplexType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mLanguageList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 289
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mMargin:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mAlign:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 292
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeStart:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 293
    iget v0, p0, Lcom/sec/android/app/mobileprint/service/PrintSettings;->mRangeEnd:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    return-void
.end method
