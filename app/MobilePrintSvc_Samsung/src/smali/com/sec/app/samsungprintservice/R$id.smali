.class public final Lcom/sec/app/samsungprintservice/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/samsungprintservice/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_settings:I = 0x7f0c0031

.field public static final add_printer_list:I = 0x7f0c001b

.field public static final btnPrintQueue:I = 0x7f0c002e

.field public static final btnSupplies:I = 0x7f0c0030

.field public static final button_forget_printer:I = 0x7f0c001a

.field public static final button_modify_printer:I = 0x7f0c0019

.field public static final checkbox_off:I = 0x7f0c001d

.field public static final checkbox_showpw:I = 0x7f0c0022

.field public static final details:I = 0x7f0c0012

.field public static final editPassword:I = 0x7f0c0021

.field public static final editUserID:I = 0x7f0c001f

.field public static final handler_msg__bind_service:I = 0x7f0c0001

.field public static final handler_msg__cancel_all_print_jobs:I = 0x7f0c0008

.field public static final handler_msg__cancel_print_job:I = 0x7f0c0007

.field public static final handler_msg__exit:I = 0x7f0c0000

.field public static final handler_msg__get_all_jobs_status:I = 0x7f0c000f

.field public static final handler_msg__get_final_job_parameters:I = 0x7f0c0004

.field public static final handler_msg__get_job_status:I = 0x7f0c000e

.field public static final handler_msg__get_printer_capabilities:I = 0x7f0c0003

.field public static final handler_msg__get_printer_status:I = 0x7f0c000b

.field public static final handler_msg__job_status_update:I = 0x7f0c0010

.field public static final handler_msg__register_status_receiver:I = 0x7f0c0009

.field public static final handler_msg__remove_job:I = 0x7f0c0011

.field public static final handler_msg__resume_print_job:I = 0x7f0c0006

.field public static final handler_msg__start_monitoring_printer_status:I = 0x7f0c000c

.field public static final handler_msg__start_print_job:I = 0x7f0c0005

.field public static final handler_msg__stop_monitoring_printer_status:I = 0x7f0c000d

.field public static final handler_msg__unbind_service:I = 0x7f0c0002

.field public static final handler_msg__unregister_status_receiver:I = 0x7f0c000a

.field public static final hint:I = 0x7f0c0025

.field public static final imageView1:I = 0x7f0c002a

.field public static final image_printer_icon:I = 0x7f0c0014

.field public static final linearlayout1:I = 0x7f0c0029

.field public static final pref_btn_layout:I = 0x7f0c0028

.field public static final pref_cancel:I = 0x7f0c0023

.field public static final pref_ok:I = 0x7f0c0024

.field public static final spinnerPermission:I = 0x7f0c0027

.field public static final textK2:I = 0x7f0c002b

.field public static final textLicense:I = 0x7f0c002f

.field public static final textVersion:I = 0x7f0c002c

.field public static final textView1:I = 0x7f0c0013

.field public static final textView3:I = 0x7f0c002d

.field public static final text_dialog_add_printer_ip_address:I = 0x7f0c0018

.field public static final text_dialog_add_printer_name:I = 0x7f0c0017

.field public static final txtPassword:I = 0x7f0c0020

.field public static final txtPermission:I = 0x7f0c0026

.field public static final txtUserID:I = 0x7f0c001e

.field public static final txt_printer_ip:I = 0x7f0c0016

.field public static final txt_printer_name:I = 0x7f0c0015

.field public static final webView1:I = 0x7f0c001c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
