.class public final Lcom/sec/app/samsungprintservice/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/app/samsungprintservice/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final POtn_txtConfidential:I = 0x7f05000f

.field public static final POtn_txtJobAccounting:I = 0x7f050027

.field public static final POtn_txtSecuThru:I = 0x7f05002e

.field public static final Spalsh_Copyright:I = 0x7f05001c

.field public static final Spalsh_K2:I = 0x7f05001d

.field public static final action_settings:I = 0x7f050001

.field public static final app_name:I = 0x7f050000

.field public static final application_Version:I = 0x7f050033

.field public static final application_info:I = 0x7f050016

.field public static final application_version_number:I = 0x7f050002

.field public static final common_cancel:I = 0x7f050024

.field public static final common_ok:I = 0x7f05002a

.field public static final common_password:I = 0x7f05002b

.field public static final common_showpw:I = 0x7f050011

.field public static final common_user_id:I = 0x7f050032

.field public static final confidential_print_explain:I = 0x7f050010

.field public static final dialog_body__add_printer_address:I = 0x7f050035

.field public static final dialog_body__add_printer_name:I = 0x7f050034

.field public static final dialog_forget_printer:I = 0x7f050022

.field public static final dialog_modify_printer:I = 0x7f050015

.field public static final dialog_title__add_printer:I = 0x7f050023

.field public static final duplex_longEdge:I = 0x7f050028

.field public static final duplex_shortEdge:I = 0x7f05002f

.field public static final duplex_simplex:I = 0x7f050030

.field public static final edit_text_hint__add_printer:I = 0x7f050036

.field public static final ipaddress_allowdChar:I = 0x7f05000b

.field public static final job_accounting_encryption:I = 0x7f050012

.field public static final job_accounting_explain:I = 0x7f050014

.field public static final job_accounting_group:I = 0x7f050026

.field public static final job_accounting_hint:I = 0x7f050013

.field public static final job_accounting_permission:I = 0x7f05002c

.field public static final job_accounting_user:I = 0x7f050031

.field public static final legal_info:I = 0x7f050017

.field public static final magic_printer__address:I = 0x7f05000e

.field public static final magic_printer__model:I = 0x7f05000d

.field public static final off:I = 0x7f050029

.field public static final open_source:I = 0x7f050018

.field public static final password_allowdChar:I = 0x7f050009

.field public static final pin_allowdChar:I = 0x7f05000c

.field public static final plugin_vendor_name:I = 0x7f050003

.field public static final print_settings:I = 0x7f05002d

.field public static final printerName_allowdChar:I = 0x7f05000a

.field public static final setting_summary__support_color:I = 0x7f05001e

.field public static final setting_title__support_color:I = 0x7f050025

.field public static final settings_key__page_run_delay:I = 0x7f050007

.field public static final settings_key__support_borderless:I = 0x7f050006

.field public static final settings_key__support_color:I = 0x7f050004

.field public static final settings_key__support_duplex:I = 0x7f050005

.field public static final text__no_printers_added:I = 0x7f050037

.field public static final txt_fetching_device_info:I = 0x7f050019

.field public static final txt_image_contentDescription:I = 0x7f050020

.field public static final txt_preference_duplex_title:I = 0x7f05001b

.field public static final txt_printer_duplicate_desc:I = 0x7f050021

.field public static final txt_printer_problem_desc:I = 0x7f05001f

.field public static final txt_printer_problem_title:I = 0x7f05001a

.field public static final username_allowdChar:I = 0x7f050008


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
