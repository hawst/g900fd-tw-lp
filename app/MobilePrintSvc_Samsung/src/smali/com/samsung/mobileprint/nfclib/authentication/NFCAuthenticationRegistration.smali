.class public Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
.super Ljava/lang/Object;
.source "NFCAuthenticationRegistration.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;


# instance fields
.field private m_Domain:[B

.field private m_DomainSize:[B

.field private m_EncPWdSize:[B

.field private m_EncPassCode:[B

.field private m_EncPassCodeSize:[B

.field private m_EncPwd:[B

.field private m_SamsungHeader:[B

.field private m_UID:[B

.field private m_UIDSize:[B

.field private m_UserId:[B

.field private m_UserIdSize:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    .line 28
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    .line 30
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    .line 32
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    .line 34
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    .line 36
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    .line 43
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 47
    new-instance v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    .line 48
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->setSamsungHeader([B)V

    .line 49
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 54
    new-instance v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    .line 55
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->byteStreamToClass([B)Z

    .line 56
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_NFCAuthReg:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 103
    const/4 v0, 0x0

    .line 106
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 108
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 109
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    .line 110
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 112
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 113
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    .line 114
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 116
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 117
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    .line 118
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 120
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 121
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    .line 122
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 124
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 125
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    .line 126
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 132
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 128
    :catch_0
    move-exception v1

    .line 129
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getDomain()[B
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    return-object v0
.end method

.method public getDomainSize()[B
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    return-object v0
.end method

.method public getEncPWdSize()[B
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    return-object v0
.end method

.method public getEncPassCode()[B
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    return-object v0
.end method

.method public getEncPassCodeSize()[B
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    return-object v0
.end method

.method public getEncPwd()[B
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 84
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 85
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 86
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 87
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 88
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 89
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 91
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 93
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 94
    :cond_3
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 95
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 97
    :cond_4
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    array-length v1, v1

    .line 65
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    array-length v3, v3

    .line 64
    add-int/2addr v3, v1

    .line 66
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    array-length v1, v1

    .line 64
    :goto_0
    add-int/2addr v1, v3

    .line 67
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    array-length v3, v3

    .line 64
    add-int/2addr v3, v1

    .line 68
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    array-length v1, v1

    .line 64
    :goto_1
    add-int/2addr v1, v3

    .line 69
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    array-length v3, v3

    .line 64
    add-int/2addr v3, v1

    .line 70
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    array-length v1, v1

    .line 64
    :goto_2
    add-int/2addr v1, v3

    .line 71
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    array-length v3, v3

    .line 64
    add-int/2addr v3, v1

    .line 72
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    array-length v1, v1

    .line 64
    :goto_3
    add-int/2addr v1, v3

    .line 73
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    array-length v3, v3

    .line 64
    add-int/2addr v1, v3

    .line 74
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    array-length v2, v2

    .line 64
    :cond_0
    add-int v0, v1, v2

    .line 77
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 66
    goto :goto_0

    :cond_2
    move v1, v2

    .line 68
    goto :goto_1

    :cond_3
    move v1, v2

    .line 70
    goto :goto_2

    :cond_4
    move v1, v2

    .line 72
    goto :goto_3
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getUID()[B
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    return-object v0
.end method

.method public getUIDSize()[B
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    return-object v0
.end method

.method public getUserId()[B
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    return-object v0
.end method

.method public getUserIdSize()[B
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    return-object v0
.end method

.method public setDomain([B)V
    .locals 3
    .param p1, "m_Domain"    # [B

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 182
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 183
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    .line 184
    return-void
.end method

.method public setEncPassCode([B)V
    .locals 3
    .param p1, "m_EncPassCode"    # [B

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 225
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 226
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    .line 227
    return-void
.end method

.method public setEncPwd([B)V
    .locals 3
    .param p1, "m_EncPwd"    # [B

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 210
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 211
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    .line 212
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    .line 155
    return-void
.end method

.method public setUID([B)V
    .locals 3
    .param p1, "m_UID"    # [B

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 167
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 169
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    .line 170
    return-void
.end method

.method public setUserId([B)V
    .locals 3
    .param p1, "m_UserId"    # [B

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 196
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 197
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    .line 198
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCAuthenticationRegistration [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UIDSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UIDSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UID:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_DomainSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_DomainSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Domain="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_Domain:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserIdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserIdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_UserId:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPWdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPWdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPwd:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPassCodeSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCodeSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPassCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->m_EncPassCode:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
