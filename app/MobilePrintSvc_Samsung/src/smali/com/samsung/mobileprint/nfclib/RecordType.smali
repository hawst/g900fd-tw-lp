.class public final enum Lcom/samsung/mobileprint/nfclib/RecordType;
.super Ljava/lang/Enum;
.source "RecordType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/mobileprint/nfclib/RecordType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum HANDOVER_SELECT:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum SAMSUNGAPPS_URI:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

.field public static final enum WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/samsung/mobileprint/nfclib/RecordType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_package:Ljava/lang/String;

.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 8
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "MOBILE_PRINT_CONNECT"

    const-string v4, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v1, v7, v4}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 9
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "AUTHENTICATION_REGISTRATION"

    const-string v4, "com.sec.print.mobileauth"

    invoke-direct {v2, v3, v7, v10, v4}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 10
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "AUTHENTICATION"

    const-string v4, "com.sec.print.mobileauth"

    invoke-direct {v2, v3, v8, v9, v4}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 11
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "TROUBLESHOOTING"

    const-string v4, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v9, v8, v4}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 12
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "WIRELESS_SETTING"

    const/4 v4, 0x5

    const-string v5, "com.sec.print.mes"

    invoke-direct {v2, v3, v10, v4, v5}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 13
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "PRINTER_STATUS"

    const/4 v4, 0x5

    const/4 v5, 0x6

    const-string v6, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 14
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "DEVICE_SETTING"

    const/4 v4, 0x6

    const/4 v5, 0x7

    const-string v6, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 15
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "PRINTER_PUBLIC_KEY"

    const/4 v4, 0x7

    const/16 v5, 0x8

    const-string v6, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 16
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "MOBILE_SECURITY_KEY"

    const/16 v4, 0x8

    const/16 v5, 0x9

    const-string v6, "com.sec.print.mobileprint"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 17
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "HANDOVER_SELECT"

    const/16 v4, 0x9

    const/16 v5, 0x64

    const-string v6, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->HANDOVER_SELECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 18
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "SAMSUNGAPPS_URI"

    const/16 v4, 0xa

    const/16 v5, 0x65

    const-string v6, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->SAMSUNGAPPS_URI:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 19
    new-instance v2, Lcom/samsung/mobileprint/nfclib/RecordType;

    const-string v3, "AAR"

    const/16 v4, 0xb

    const/16 v5, 0x66

    const-string v6, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/RecordType;-><init>(Ljava/lang/String;IBLjava/lang/String;)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

    const/16 v2, 0xc

    new-array v2, v2, [Lcom/samsung/mobileprint/nfclib/RecordType;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v3, v2, v9

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v3, v2, v10

    const/4 v3, 0x5

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->HANDOVER_SELECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->SAMSUNGAPPS_URI:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 22
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->typesByValue:Ljava/util/Map;

    .line 24
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/RecordType;->values()[Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/samsung/mobileprint/nfclib/RecordType;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 27
    return-void

    .line 24
    :cond_0
    aget-object v0, v2, v1

    .line 25
    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/samsung/mobileprint/nfclib/RecordType;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;IBLjava/lang/String;)V
    .locals 0
    .param p3, "value"    # B
    .param p4, "app"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-byte p3, p0, Lcom/samsung/mobileprint/nfclib/RecordType;->m_value:B

    .line 34
    iput-object p4, p0, Lcom/samsung/mobileprint/nfclib/RecordType;->m_package:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static forValue(B)Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 2
    .param p0, "value"    # B

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/RecordType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/RecordType;->m_package:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordTypeValue()B
    .locals 1

    .prologue
    .line 38
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/RecordType;->m_value:B

    return v0
.end method
