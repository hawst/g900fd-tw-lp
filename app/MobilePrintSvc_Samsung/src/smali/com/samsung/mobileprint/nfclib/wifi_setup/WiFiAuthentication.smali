.class public final enum Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;
.super Ljava/lang/Enum;
.source "WiFiAuthentication.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

.field public static final enum WIFI_NO_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

.field public static final enum WIFI_OPEN:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

.field public static final enum WIFI_SHARED:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 8
    new-instance v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    const-string v3, "WIFI_NO_AUTHENTICATION"

    invoke-direct {v2, v3, v1, v1}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;-><init>(Ljava/lang/String;IB)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_NO_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    .line 9
    new-instance v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    const-string v3, "WIFI_OPEN"

    invoke-direct {v2, v3, v4, v4}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;-><init>(Ljava/lang/String;IB)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_OPEN:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    .line 10
    new-instance v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    const-string v3, "WIFI_SHARED"

    invoke-direct {v2, v3, v5, v5}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;-><init>(Ljava/lang/String;IB)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_SHARED:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_NO_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    aput-object v3, v2, v1

    sget-object v3, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_OPEN:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    aput-object v3, v2, v4

    sget-object v3, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->WIFI_SHARED:Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    aput-object v3, v2, v5

    sput-object v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    .line 12
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->typesByValue:Ljava/util/Map;

    .line 14
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->values()[Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 17
    return-void

    .line 14
    :cond_0
    aget-object v0, v2, v1

    .line 15
    sget-object v4, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "value"    # B

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-byte p3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->m_value:B

    .line 23
    return-void
.end method

.method public static forValue(B)Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;
    .locals 2
    .param p0, "value"    # B

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    return-object v0
.end method

.method public static values()[Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()B
    .locals 1

    .prologue
    .line 26
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->m_value:B

    return v0
.end method
