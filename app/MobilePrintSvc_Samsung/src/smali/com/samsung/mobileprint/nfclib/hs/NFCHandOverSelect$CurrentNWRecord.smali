.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CurrentNWRecord"
.end annotation


# instance fields
.field m_currentNWRecord:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 789
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 791
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v3, v0, v1

    aput-byte v3, v0, v2

    const/4 v1, 0x3

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x43

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x63

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x31

    aput-byte v2, v0, v1

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->m_currentNWRecord:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;)V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 800
    const/4 v0, 0x0

    .line 801
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->m_currentNWRecord:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->m_currentNWRecord:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->m_currentNWRecord:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 806
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 802
    :catch_0
    move-exception v1

    .line 803
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->m_currentNWRecord:[B

    return-object v0
.end method
