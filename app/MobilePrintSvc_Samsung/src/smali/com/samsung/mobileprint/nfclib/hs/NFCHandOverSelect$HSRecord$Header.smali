.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Header"
.end annotation


# instance fields
.field m_Flag:[B

.field m_Payload_Length:[B

.field m_Type:[B

.field m_TypeLength:[B

.field final synthetic this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 278
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    new-array v0, v1, [B

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Flag:[B

    .line 280
    new-array v0, v1, [B

    aput-byte v3, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_TypeLength:[B

    .line 281
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Payload_Length:[B

    .line 282
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Type:[B

    return-void

    :array_0
    .array-data 1
        0x48t
        0x73t
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method getByteData(I)[B
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 285
    new-array v1, p1, [B

    .line 286
    .local v1, "data":[B
    const/4 v0, 0x0

    .line 287
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Flag:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Flag:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Flag:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 288
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_TypeLength:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_TypeLength:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_TypeLength:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 289
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Payload_Length:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Payload_Length:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Payload_Length:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 290
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Type:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Type:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Type:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 291
    return-object v1
.end method
