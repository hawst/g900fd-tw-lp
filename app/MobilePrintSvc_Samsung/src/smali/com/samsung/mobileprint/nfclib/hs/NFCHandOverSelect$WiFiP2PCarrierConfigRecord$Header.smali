.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Header"
.end annotation


# instance fields
.field m_Flag:[B

.field m_Id:[B

.field m_IdLength:[B

.field m_Payload_Length:[B

.field m_Type:[B

.field m_TypeLength:[B

.field final synthetic this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 412
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    new-array v0, v2, [B

    const/4 v1, 0x2

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Flag:[B

    .line 414
    new-array v0, v2, [B

    const/16 v1, 0x17

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_TypeLength:[B

    .line 415
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Payload_Length:[B

    .line 416
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_IdLength:[B

    .line 417
    new-instance v0, Ljava/lang/String;

    const-string v1, "application/vnd.wfa.p2p"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Type:[B

    .line 418
    new-array v0, v2, [B

    const/16 v1, 0x30

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Id:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;)V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method getByteData(I)[B
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 421
    new-array v1, p1, [B

    .line 422
    .local v1, "data":[B
    const/4 v0, 0x0

    .line 423
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Flag:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Flag:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Flag:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 424
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_TypeLength:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_TypeLength:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_TypeLength:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 425
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Payload_Length:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Payload_Length:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Payload_Length:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 426
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_IdLength:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_IdLength:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_IdLength:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 427
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Type:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Type:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Type:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 428
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Id:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Id:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Id:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 429
    return-object v1
.end method
