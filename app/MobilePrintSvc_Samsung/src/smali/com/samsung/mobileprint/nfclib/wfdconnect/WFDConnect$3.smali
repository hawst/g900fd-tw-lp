.class Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;
.super Ljava/lang/Object;
.source "WFDConnect.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->discoverWiFiDirectDevices()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;


# direct methods
.method constructor <init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "reasonCode"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$8(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 322
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "wifidirect_device_not_found"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$8(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "discovering_wifi_direct_device"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_0
.end method
