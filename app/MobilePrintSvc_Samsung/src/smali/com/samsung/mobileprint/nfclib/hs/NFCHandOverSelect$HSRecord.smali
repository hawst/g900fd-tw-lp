.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HSRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;
    }
.end annotation


# instance fields
.field m_Auxiliary_Data_Reference_Count:[B

.field m_Auxiliary_Data_Reference_Length1:[B

.field m_Auxiliary_Data_Reference_Length2:[B

.field m_Auxiliary_Data_Reference_Length3:[B

.field m_Auxiliary_Data_Reference_Value1:[B

.field m_Auxiliary_Data_Reference_Value2:[B

.field m_Auxiliary_Data_Reference_Value3:[B

.field m_Carrier_Data_Reference_Length:[B

.field m_Carrier_Data_Reference_Value:[B

.field m_Carrier_Flag:[B

.field m_Flag:[B

.field m_Header:[B

.field m_Payload_Length:[B

.field m_Type:[B

.field m_Type_Length:[B

.field m_Version:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 276
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    const/4 v0, 0x5

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    .line 303
    new-array v0, v2, [B

    const/16 v1, 0x12

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    .line 304
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    .line 305
    new-array v0, v2, [B

    aput-byte v4, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    .line 306
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    .line 307
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    .line 308
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    .line 309
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    .line 310
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    .line 311
    new-array v0, v2, [B

    const/4 v1, 0x3

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Count:[B

    .line 312
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    .line 313
    new-array v0, v2, [B

    const/16 v1, 0x41

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value1:[B

    .line 314
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    .line 315
    new-array v0, v2, [B

    const/16 v1, 0x42

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    .line 316
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    .line 317
    new-array v0, v2, [B

    const/16 v1, 0x43

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    return-void

    .line 307
    :array_0
    .array-data 1
        0x61t
        0x63t
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;)V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 383
    const/4 v0, 0x0

    .line 384
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 385
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 386
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 387
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 388
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 389
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 390
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 391
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 392
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 393
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 394
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 395
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 396
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 397
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 398
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 404
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 400
    :catch_0
    move-exception v1

    .line 401
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 326
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->getByteArraySize()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v2, v3, [B

    .line 329
    .local v2, "nfcData":[B
    new-instance v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;)V

    .line 330
    .local v1, "header":Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;
    iget-object v3, v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->m_Payload_Length:[B

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->getByteArraySize()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 331
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord$Header;->getByteData(I)[B

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    .line 334
    const/4 v0, 0x0

    .line 336
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 337
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 338
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 339
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 340
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 341
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 342
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 343
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 344
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 345
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Count:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Count:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Count:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 346
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 347
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value1:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value1:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value1:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 348
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 349
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 350
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 351
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 353
    return-object v2
.end method

.method public getByteArraySize()J
    .locals 4

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 360
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type:[B

    array-length v1, v1

    .line 361
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Flag:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 362
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Length:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 363
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Carrier_Data_Reference_Value:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 364
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length1:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 365
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value1:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 366
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length2:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 367
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value2:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 368
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Length3:[B

    array-length v2, v2

    .line 360
    add-int/2addr v1, v2

    .line 369
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Auxiliary_Data_Reference_Value3:[B

    array-length v2, v2

    .line 360
    add-int v0, v1, v2

    .line 370
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    const/4 v2, 0x0

    int-to-byte v3, v0

    aput-byte v3, v1, v2

    .line 371
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Header:[B

    array-length v1, v1

    .line 372
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Version:[B

    array-length v2, v2

    .line 371
    add-int/2addr v1, v2

    .line 373
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Flag:[B

    array-length v2, v2

    .line 371
    add-int/2addr v1, v2

    .line 374
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Type_Length:[B

    array-length v2, v2

    .line 371
    add-int/2addr v1, v2

    .line 375
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->m_Payload_Length:[B

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 377
    int-to-long v1, v0

    return-wide v1
.end method
