.class public Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
.super Ljava/lang/Object;
.source "SamsungNdefMessage.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType:[I

.field private static _instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;


# instance fields
.field private mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

.field private m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType()[I
    .locals 3

    .prologue
    .line 7
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->$SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->values()[Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->$SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createMobileSamsungNdefMessage(Lcom/samsung/mobileprint/nfclib/NdefMessageType;)Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    .locals 6
    .param p0, "mMessageType"    # Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 46
    new-instance v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    .line 47
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iput-object p0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 49
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->$SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 78
    :goto_0
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    return-object v0

    .line 51
    :pswitch_0
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    new-array v1, v5, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 52
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v4

    .line 53
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_0

    .line 57
    :pswitch_1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    new-array v1, v5, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 58
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v4

    .line 59
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_0

    .line 65
    :pswitch_2
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    new-array v1, v5, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 66
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v4

    .line 67
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_0

    .line 73
    :pswitch_3
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    new-array v1, v2, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 74
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static createPrinterSamsungNdefMessage(Lcom/samsung/mobileprint/nfclib/NdefMessageType;Z)Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    .locals 6
    .param p0, "mMessageType"    # Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    .param p1, "isChina"    # Z

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 17
    new-instance v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    .line 18
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iput-object p0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 20
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->$SWITCH_TABLE$com$samsung$mobileprint$nfclib$NdefMessageType()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 41
    :goto_0
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    return-object v0

    .line 23
    :pswitch_0
    if-eqz p1, :cond_1

    .line 24
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 28
    :goto_1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v2, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v2

    aput-object v2, v0, v1

    .line 29
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v4

    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v2, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v2

    aput-object v2, v0, v1

    .line 32
    if-eqz p1, :cond_0

    .line 33
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->SAMSUNGAPPS_URI:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v3

    .line 34
    :cond_0
    if-eqz p1, :cond_2

    .line 35
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 26
    :cond_1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    new-array v1, v5, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v1, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    goto :goto_1

    .line 37
    :cond_2
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v0, v0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    sget-object v1, Lcom/samsung/mobileprint/nfclib/RecordType;->AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-static {v1, v4, v3}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 20
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static createSamsungNdefMessage(Landroid/nfc/NdefMessage;)Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;
    .locals 8
    .param p0, "message"    # Landroid/nfc/NdefMessage;

    .prologue
    const/4 v3, 0x0

    .line 82
    new-instance v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    invoke-direct {v2}, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    .line 83
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [Lcom/samsung/mobileprint/nfclib/INFCRecord;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .line 84
    const/4 v0, 0x0

    .line 85
    .local v0, "cnt":I
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v4

    array-length v5, v4

    move v2, v3

    :goto_0
    if-lt v2, v5, :cond_4

    .line 89
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->SAMSUNGAPPS_URI:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_5

    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_5

    .line 90
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 95
    :cond_0
    :goto_1
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_1

    .line 96
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 97
    :cond_1
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_2

    .line 98
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 99
    :cond_2
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v3, :cond_3

    .line 100
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v3, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 102
    :cond_3
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    return-object v2

    .line 85
    :cond_4
    aget-object v1, v4, v2

    .line 86
    .local v1, "record":Landroid/nfc/NdefRecord;
    sget-object v6, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    invoke-static {v1}, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->buildNFCRecordFromByteArray(Landroid/nfc/NdefRecord;)Lcom/samsung/mobileprint/nfclib/INFCRecord;

    move-result-object v7

    aput-object v7, v6, v0

    .line 87
    add-int/lit8 v0, v0, 0x1

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "record":Landroid/nfc/NdefRecord;
    :cond_5
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_6

    .line 92
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    goto :goto_1

    .line 93
    :cond_6
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v3

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    if-ne v2, v4, :cond_0

    .line 94
    sget-object v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->_instance:Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    iput-object v4, v2, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    goto :goto_1
.end method


# virtual methods
.method public getMessageType()Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->m_MessageType:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    return-object v0
.end method

.method public getRecord(Lcom/samsung/mobileprint/nfclib/RecordType;)Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .locals 5
    .param p1, "type"    # Lcom/samsung/mobileprint/nfclib/RecordType;

    .prologue
    .line 107
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 112
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 107
    :cond_1
    aget-object v0, v2, v1

    .line 108
    .local v0, "rec":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v4

    if-eq v4, p1, :cond_0

    .line 107
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getRecordAtIndex(I)Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 148
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v0, v0, p1

    .line 151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecordTypeSequence()[Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 7

    .prologue
    .line 116
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v3, v3

    new-array v2, v3, [Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 117
    .local v2, "recTypes":[Lcom/samsung/mobileprint/nfclib/RecordType;
    const/4 v0, 0x0

    .line 118
    .local v0, "cnt":I
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 121
    return-object v2

    .line 118
    :cond_0
    aget-object v1, v4, v3

    .line 119
    .local v1, "rec":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-interface {v1}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v6

    aput-object v6, v2, v0

    add-int/lit8 v0, v0, 0x1

    .line 118
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getRecordsCount()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v0, v0

    return v0
.end method

.method public setRecord(Lcom/samsung/mobileprint/nfclib/INFCRecord;Lcom/samsung/mobileprint/nfclib/RecordType;)Z
    .locals 3
    .param p1, "record"    # Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .param p2, "type"    # Lcom/samsung/mobileprint/nfclib/RecordType;

    .prologue
    .line 125
    const/4 v1, 0x0

    .line 126
    .local v1, "ret":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 132
    return v1

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/samsung/mobileprint/nfclib/INFCRecord;->getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v2

    if-ne v2, p2, :cond_1

    .line 128
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    aput-object p1, v2, v0

    .line 129
    const/4 v1, 0x1

    .line 126
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeNdefMessage(Landroid/nfc/Tag;)I
    .locals 2
    .param p1, "tag"    # Landroid/nfc/Tag;

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNdefMessage;->mRecordArray:[Lcom/samsung/mobileprint/nfclib/INFCRecord;

    invoke-static {v0, p1, v1, v1}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->writeTag([Lcom/samsung/mobileprint/nfclib/INFCRecord;Landroid/nfc/Tag;ZZ)I

    move-result v0

    return v0
.end method
