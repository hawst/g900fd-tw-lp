.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkIdentityRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;
    }
.end annotation


# instance fields
.field m_Header:[B

.field m_MACAddress:[B

.field m_MACLength:[B

.field m_MACValue:[B

.field m_VersionNumber:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 811
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 840
    const/4 v0, 0x7

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    .line 841
    const/4 v0, 0x2

    new-array v0, v0, [B

    aput-byte v3, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    .line 842
    new-array v0, v3, [B

    const/4 v1, 0x5

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    .line 843
    new-array v0, v3, [B

    aput-byte v4, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    .line 844
    new-array v0, v4, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;)V
    .locals 0

    .prologue
    .line 811
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 884
    const/4 v0, 0x0

    .line 885
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 886
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 887
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 888
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 889
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 894
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 890
    :catch_0
    move-exception v1

    .line 891
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 850
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$6(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 852
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->getByteArraySize()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v2, v3, [B

    .line 855
    .local v2, "nfcData":[B
    new-instance v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;)V

    .line 856
    .local v1, "header":Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;
    iget-object v3, v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;->m_Payload_Length:[B

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->getByteArraySize()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 857
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord$Header;->getByteData(I)[B

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    .line 860
    const/4 v0, 0x0

    .line 862
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 863
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 864
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 865
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 866
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 867
    return-object v2
.end method

.method public getByteArraySize()J
    .locals 3

    .prologue
    .line 873
    const/4 v0, 0x0

    .line 874
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_Header:[B

    array-length v1, v1

    .line 875
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_VersionNumber:[B

    array-length v2, v2

    .line 874
    add-int/2addr v1, v2

    .line 876
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACAddress:[B

    array-length v2, v2

    .line 874
    add-int/2addr v1, v2

    .line 877
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACLength:[B

    array-length v2, v2

    .line 874
    add-int/2addr v1, v2

    .line 878
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->m_MACValue:[B

    array-length v2, v2

    .line 874
    add-int v0, v1, v2

    .line 879
    int-to-long v1, v0

    return-wide v1
.end method
