.class public Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
.super Ljava/lang/Object;
.source "NFCPublicKeyRecord.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;


# instance fields
.field private m_EncAlgorithm:[B

.field private m_PubKey:[B

.field private m_PubKeySize:[B

.field private m_SamsungHeader:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    .line 10
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    .line 17
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    .line 22
    sget-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->setSamsungHeader([B)V

    .line 23
    sget-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 28
    new-instance v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    .line 29
    sget-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->byteStreamToClass([B)Z

    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_NFCPublicKeyRecord:Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 62
    const/4 v0, 0x0

    .line 65
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 66
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 67
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 68
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    .line 69
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 74
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getEncAlgorithm()[B
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 38
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 39
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 40
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 41
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 42
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 43
    :cond_0
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    array-length v1, v1

    .line 51
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    array-length v2, v2

    .line 50
    add-int/2addr v1, v2

    .line 52
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    array-length v2, v2

    .line 50
    add-int/2addr v2, v1

    .line 53
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    array-length v1, v1

    .line 50
    :goto_0
    add-int v0, v2, v1

    .line 55
    int-to-long v1, v0

    return-wide v1

    .line 53
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPubKey()[B
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    return-object v0
.end method

.method public getPubKeySize()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    return-object v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    return-object v0
.end method

.method public setEncAlgorithm([B)V
    .locals 0
    .param p1, "m_EncAlgorithm"    # [B

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_EncAlgorithm:[B

    .line 103
    return-void
.end method

.method public setPubKey([B)V
    .locals 3
    .param p1, "m_PubKey"    # [B

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 119
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKeySize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 120
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_PubKey:[B

    .line 121
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->m_SamsungHeader:[B

    .line 125
    return-void
.end method
