.class public Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
.super Ljava/lang/Object;
.source "NFCTroubleShooting.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;


# instance fields
.field private m_ErrorCode:[B

.field private m_SamsungHeader:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    .line 23
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 27
    new-instance v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    .line 28
    sget-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->setSamsungHeader([B)V

    .line 29
    sget-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    .line 35
    sget-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->byteStreamToClass([B)Z

    .line 36
    sget-object v0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_NFCTroubleShooting:Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 64
    const/4 v0, 0x0

    .line 67
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 69
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 74
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getErrorCode()[B
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 44
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 45
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 46
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 47
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    array-length v1, v1

    .line 55
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    array-length v2, v2

    .line 54
    add-int v0, v1, v2

    .line 57
    int-to-long v1, v0

    return-wide v1
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    return-object v0
.end method

.method public setErrorCode([B)V
    .locals 0
    .param p1, "m_ErrorCode"    # [B

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    .line 107
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    .line 99
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCTroubleShooting [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 13
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_ErrorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->m_ErrorCode:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
