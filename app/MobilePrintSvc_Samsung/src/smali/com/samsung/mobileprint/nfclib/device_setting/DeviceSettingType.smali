.class public final enum Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;
.super Ljava/lang/Enum;
.source "DeviceSettingType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEVICE_SETTING_NA:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

.field public static final enum DEVICE_SETTING_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

.field public static final enum DEVICE_SETTING_WIFI:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

.field public static final enum DEVICE_SETTING_WIFI_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    const-string v3, "DEVICE_SETTING_NA"

    invoke-direct {v2, v3, v1, v1}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_NA:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    .line 15
    new-instance v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    const-string v3, "DEVICE_SETTING_WIFI"

    invoke-direct {v2, v3, v6, v5}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WIFI:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    .line 16
    new-instance v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    const-string v3, "DEVICE_SETTING_WFD"

    invoke-direct {v2, v3, v5, v8}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    .line 17
    new-instance v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    const-string v3, "DEVICE_SETTING_WIFI_WFD"

    const/4 v4, 0x6

    invoke-direct {v2, v3, v7, v4}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WIFI_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    new-array v2, v8, [Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_NA:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WIFI:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->DEVICE_SETTING_WIFI_WFD:Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    aput-object v3, v2, v7

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    .line 20
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->typesByValue:Ljava/util/Map;

    .line 22
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->values()[Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 58
    new-instance v1, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType$1;

    invoke-direct {v1}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType$1;-><init>()V

    sput-object v1, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 65
    return-void

    .line 22
    :cond_0
    aget-object v0, v2, v1

    .line 23
    sget-object v4, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->m_value:B

    .line 31
    return-void
.end method

.method public static forValue(I)Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public getDeviceSettingTypeValue()I
    .locals 1

    .prologue
    .line 34
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->m_value:B

    .line 55
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 51
    return-void
.end method
