.class Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;
.super Ljava/lang/Object;
.source "WFDConnect.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connectDevice()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;


# direct methods
.method constructor <init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    .line 388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 4
    .param p1, "reason"    # I

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 421
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "error_cnnecting_wifidirect_device"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 393
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 394
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$8(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 403
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$10(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/net/wifi/WpsInfo;

    move-result-object v0

    iget v0, v0, Landroid/net/wifi/WpsInfo;->setup:I

    if-nez v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 405
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$8(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 413
    :cond_1
    :goto_1
    return-void

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 397
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "connecting_wifi_direct_device"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_0

    .line 406
    :cond_3
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 408
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "wifidirect_device_press_wps_button"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_1
.end method
