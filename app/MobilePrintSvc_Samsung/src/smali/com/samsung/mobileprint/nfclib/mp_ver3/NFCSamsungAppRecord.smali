.class public Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;
.super Ljava/lang/Object;
.source "NFCSamsungAppRecord.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# instance fields
.field private mPackageName:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 1
    .param p1, "nfcData"    # [B

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public getNFCByteArray()[B
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNFCByteArraySize()J
    .locals 2

    .prologue
    .line 18
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->AAR:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungPackageName()[B
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;->mPackageName:[B

    return-object v0
.end method

.method public setPackageName([B)V
    .locals 1
    .param p1, "mPackageName"    # [B

    .prologue
    .line 38
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;->mPackageName:[B

    .line 39
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;->mPackageName:[B

    .line 40
    return-void
.end method
