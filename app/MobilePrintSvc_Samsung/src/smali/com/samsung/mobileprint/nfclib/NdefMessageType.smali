.class public final enum Lcom/samsung/mobileprint/nfclib/NdefMessageType;
.super Ljava/lang/Enum;
.source "NdefMessageType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/mobileprint/nfclib/NdefMessageType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/mobileprint/nfclib/NdefMessageType;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_PRINTER_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field public static final enum NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/samsung/mobileprint/nfclib/NdefMessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_PRINTER_NORMAL"

    invoke-direct {v2, v3, v1, v6}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 13
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_PRINTER_AUTHENTICATION"

    invoke-direct {v2, v3, v6, v7}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 14
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION"

    invoke-direct {v2, v3, v7, v8}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 15
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION"

    invoke-direct {v2, v3, v8, v9}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 16
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v9, v4}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 17
    new-instance v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const-string v3, "NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING"

    const/4 v4, 0x5

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    const/4 v2, 0x6

    new-array v2, v2, [Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_NORMAL:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_PRINTER_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_REGISTRATION:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_WIFI_SETTINGS:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->NDEF_MESSAGE_TYPE_MOBILE_DEVICE_CLONING:Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    .line 19
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->typesByValue:Ljava/util/Map;

    .line 21
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->values()[Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 57
    new-instance v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType$1;

    invoke-direct {v1}, Lcom/samsung/mobileprint/nfclib/NdefMessageType$1;-><init>()V

    sput-object v1, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 64
    return-void

    .line 21
    :cond_0
    aget-object v0, v2, v1

    .line 22
    sget-object v4, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->m_value:B

    .line 30
    return-void
.end method

.method public static forValue(I)Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/mobileprint/nfclib/NdefMessageType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/mobileprint/nfclib/NdefMessageType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getMessageTypeValue()I
    .locals 1

    .prologue
    .line 33
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->m_value:B

    .line 54
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 49
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/NdefMessageType;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 50
    return-void
.end method
