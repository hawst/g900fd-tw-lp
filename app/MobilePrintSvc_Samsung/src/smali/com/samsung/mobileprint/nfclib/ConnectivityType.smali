.class public final enum Lcom/samsung/mobileprint/nfclib/ConnectivityType;
.super Ljava/lang/Enum;
.source "ConnectivityType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/mobileprint/nfclib/ConnectivityType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final enum CONNECTIVITY_TYPE_WFD:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

.field public static final enum CONNECTIVITY_TYPE_WFD_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

.field public static final enum CONNECTIVITY_TYPE_WFD_WIFI_NW:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

.field public static final enum CONNECTIVITY_TYPE_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/mobileprint/nfclib/ConnectivityType;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/ConnectivityType;

.field private static final typesByValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/samsung/mobileprint/nfclib/ConnectivityType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_value:B


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    const-string v3, "CONNECTIVITY_TYPE_WFD"

    invoke-direct {v2, v3, v1, v5}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    .line 13
    new-instance v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    const-string v3, "CONNECTIVITY_TYPE_WIFI"

    invoke-direct {v2, v3, v5, v6}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    .line 14
    new-instance v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    const-string v3, "CONNECTIVITY_TYPE_WFD_WIFI"

    invoke-direct {v2, v3, v6, v7}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    .line 15
    new-instance v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    const-string v3, "CONNECTIVITY_TYPE_WFD_WIFI_NW"

    const/4 v4, 0x7

    invoke-direct {v2, v3, v7, v4}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD_WIFI_NW:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    sget-object v3, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD_WIFI_NW:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    aput-object v3, v2, v7

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    .line 18
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->typesByValue:Ljava/util/Map;

    .line 20
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->values()[Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/samsung/mobileprint/nfclib/ConnectivityType;
    :goto_0
    if-lt v1, v3, :cond_0

    .line 56
    new-instance v1, Lcom/samsung/mobileprint/nfclib/ConnectivityType$1;

    invoke-direct {v1}, Lcom/samsung/mobileprint/nfclib/ConnectivityType$1;-><init>()V

    sput-object v1, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 63
    return-void

    .line 20
    :cond_0
    aget-object v0, v2, v1

    .line 21
    sget-object v4, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->typesByValue:Ljava/util/Map;

    iget-byte v5, v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->m_value:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->m_value:B

    .line 29
    return-void
.end method

.method public static forValue(I)Lcom/samsung/mobileprint/nfclib/ConnectivityType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->typesByValue:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/mobileprint/nfclib/ConnectivityType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/mobileprint/nfclib/ConnectivityType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->ENUM$VALUES:[Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectivityTypeValue()I
    .locals 1

    .prologue
    .line 32
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->m_value:B

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->m_value:B

    .line 53
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 48
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->m_value:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 49
    return-void
.end method
