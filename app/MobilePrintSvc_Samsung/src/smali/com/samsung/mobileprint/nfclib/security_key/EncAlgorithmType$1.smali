.class Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType$1;
.super Ljava/lang/Object;
.source "EncAlgorithmType.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;
    .locals 1
    .param p1, "pc"    # Landroid/os/Parcel;

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;->forValue(I)Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 61
    new-array v0, p1, [Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType$1;->newArray(I)[Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;

    move-result-object v0

    return-object v0
.end method
