.class public Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
.super Ljava/lang/Object;
.source "NFCMobileSecurityKeyRecord.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;


# instance fields
.field private m_AESKey:[B

.field private m_AESKeySize:[B

.field private m_EncAlgorithm:[B

.field private m_SamsungHeader:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    .line 10
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    .line 17
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    .line 22
    sget-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->setSamsungHeader([B)V

    .line 23
    sget-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 28
    new-instance v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    .line 29
    sget-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->byteStreamToClass([B)Z

    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 62
    const/4 v0, 0x0

    .line 65
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 66
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 67
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 68
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    .line 69
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 74
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getAESKey()[B
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    return-object v0
.end method

.method public getAESKeySize()[B
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    return-object v0
.end method

.method public getEncAlgorithm()[B
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 38
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 39
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 40
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 41
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 42
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 43
    :cond_0
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    array-length v1, v1

    .line 51
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    array-length v2, v2

    .line 50
    add-int/2addr v1, v2

    .line 52
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    array-length v2, v2

    .line 50
    add-int/2addr v2, v1

    .line 53
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    array-length v1, v1

    .line 50
    :goto_0
    add-int v0, v2, v1

    .line 55
    int-to-long v1, v0

    return-wide v1

    .line 53
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    return-object v0
.end method

.method public setAESKey([B)V
    .locals 3
    .param p1, "m_AESKey"    # [B

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 119
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKeySize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 121
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_AESKey:[B

    .line 122
    return-void
.end method

.method public setEncAlgorithm(Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;)V
    .locals 3
    .param p1, "algoType"    # Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_EncAlgorithm:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/security_key/EncAlgorithmType;->getEncAlgorithmTypeValue()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 103
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->m_SamsungHeader:[B

    .line 126
    return-void
.end method
