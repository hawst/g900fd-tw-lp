.class public Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
.super Ljava/lang/Object;
.source "NFCMPConnectV2.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;


# instance fields
.field private m_EncPWdSize:[B

.field private m_EncPwd:[B

.field private m_ModelName:[B

.field private m_ModelNameSize:[B

.field private m_NetIPAddress:[B

.field private m_NetMacAddress:[B

.field private m_SamsungHeader:[B

.field private m_WFDChannel:[B

.field private m_WFDIPAddrerss:[B

.field private m_WFDMacAddress:[B

.field private m_WiFIIPAddress:[B

.field private m_WiFiMacAddress:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x6

    const/4 v1, 0x4

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    .line 34
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    .line 36
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    .line 37
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    .line 38
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    .line 39
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    .line 40
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    .line 41
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    .line 43
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    .line 49
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 53
    new-instance v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .line 54
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->setSamsungHeader([B)V

    .line 55
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 60
    new-instance v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .line 61
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->byteStreamToClass([B)Z

    .line 62
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NFCMPConnectV2:Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 110
    const/4 v0, 0x0

    .line 113
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 115
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 116
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    .line 117
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 119
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 120
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 121
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 122
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 123
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 124
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 125
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 127
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 128
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    .line 129
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 135
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 131
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "messenger"    # Landroid/os/Messenger;

    .prologue
    .line 164
    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->ByteArrayToMacAddress([B)Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 166
    .local v2, "modelName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getDecryptedString([B)Ljava/lang/String;

    move-result-object v3

    .line 167
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public connectWFD(Landroid/content/Context;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wfdConnectionListener"    # Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    .prologue
    .line 152
    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->ByteArrayToMacAddress([B)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 154
    .local v2, "modelName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getDecryptedString([B)Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getEncPWdSize()[B
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    return-object v0
.end method

.method public getEncPwd()[B
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    return-object v0
.end method

.method public getModelName()[B
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    return-object v0
.end method

.method public getModelNameSize()[B
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 90
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 91
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 92
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 93
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 94
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 95
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 96
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 97
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 98
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 99
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 100
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 101
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 102
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 103
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 104
    :cond_1
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    array-length v1, v1

    .line 72
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    array-length v3, v3

    .line 71
    add-int/2addr v3, v1

    .line 73
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    array-length v1, v1

    .line 71
    :goto_0
    add-int/2addr v1, v3

    .line 74
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 75
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 76
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 77
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 78
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 79
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 80
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 81
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    array-length v3, v3

    .line 71
    add-int/2addr v1, v3

    .line 82
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    array-length v2, v2

    .line 71
    :cond_0
    add-int v0, v1, v2

    .line 84
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public getNetIPAddress()[B
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    return-object v0
.end method

.method public getNetMacAddress()[B
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    return-object v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getWFDChannel()[B
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    return-object v0
.end method

.method public getWFDIPAddrerss()[B
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    return-object v0
.end method

.method public getWFDMacAddress()[B
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    return-object v0
.end method

.method public getWiFIIPAddress()[B
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    return-object v0
.end method

.method public getWiFiMacAddress()[B
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    return-object v0
.end method

.method public setEncPwd([B)V
    .locals 3
    .param p1, "m_EncPwd"    # [B

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 265
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 266
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    .line 267
    return-void
.end method

.method public setModelName([B)V
    .locals 3
    .param p1, "m_ModelName"    # [B

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 195
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 196
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    .line 197
    return-void
.end method

.method public setNetIPAddress([B)V
    .locals 0
    .param p1, "m_NetIPAddress"    # [B

    .prologue
    .line 212
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    .line 213
    return-void
.end method

.method public setNetMacAddress([B)V
    .locals 0
    .param p1, "m_NetMacAddress"    # [B

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    .line 205
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 181
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    .line 182
    return-void
.end method

.method public setWFDChannel([B)V
    .locals 0
    .param p1, "m_WFDChannel"    # [B

    .prologue
    .line 252
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    .line 253
    return-void
.end method

.method public setWFDIPAddrerss([B)V
    .locals 0
    .param p1, "m_WFDIPAddrerss"    # [B

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    .line 245
    return-void
.end method

.method public setWFDMacAddress([B)V
    .locals 0
    .param p1, "m_WFDMacAddress"    # [B

    .prologue
    .line 236
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    .line 237
    return-void
.end method

.method public setWiFIIPAddress([B)V
    .locals 0
    .param p1, "m_WiFIIPAddress"    # [B

    .prologue
    .line 228
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    .line 229
    return-void
.end method

.method public setWiFiMacAddress([B)V
    .locals 0
    .param p1, "m_WiFiMacAddress"    # [B

    .prologue
    .line 220
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    .line 221
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCMPConnectV2 [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 19
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_ModelNameSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelNameSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_ModelName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_ModelName:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_NetMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_NetIPAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_NetIPAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WiFiMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFiMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WiFIIPAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WiFIIPAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WFDMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WFDIPAddrerss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDIPAddrerss:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WFDChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_WFDChannel:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPWdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPWdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->m_EncPwd:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
