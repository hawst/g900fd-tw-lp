.class Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;
.super Ljava/lang/Object;
.source "WFDConnect.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->searchAndConnectWifiDirectDevice()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;


# direct methods
.method constructor <init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$0(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->stopPeerDiscovery()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$1(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    .line 284
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->unregisterWFDRecevier()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$3(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V

    .line 290
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # getter for: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "wifidirect_device_not_found"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;->this$0:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    # invokes: Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    goto :goto_0
.end method
