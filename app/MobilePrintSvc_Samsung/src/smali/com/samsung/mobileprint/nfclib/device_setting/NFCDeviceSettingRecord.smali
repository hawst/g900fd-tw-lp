.class public Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
.super Ljava/lang/Object;
.source "NFCDeviceSettingRecord.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;


# instance fields
.field private m_DeviceSetting:[B

.field private m_EncPassword:[B

.field private m_EncPasswordSize:[B

.field private m_SamsungHeader:[B

.field private m_UserId:[B

.field private m_UserIdSize:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    .line 23
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    .line 25
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    .line 32
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 36
    new-instance v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    .line 37
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->setSamsungHeader([B)V

    .line 38
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 43
    new-instance v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    .line 44
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->byteStreamToClass([B)Z

    .line 45
    sget-object v0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_NFCSecurityKeyRecord:Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 81
    const/4 v0, 0x0

    .line 84
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 85
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 86
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    .line 87
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 88
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    .line 89
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 90
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 95
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getDeviceSetting()[B
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    return-object v0
.end method

.method public getEncPassword()[B
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    return-object v0
.end method

.method public getEncPasswordSize()[B
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 52
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 53
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 54
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 55
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 56
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 58
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 59
    :cond_1
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 60
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    array-length v1, v1

    .line 68
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    array-length v3, v3

    .line 67
    add-int/2addr v3, v1

    .line 69
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    array-length v1, v1

    .line 67
    :goto_0
    add-int/2addr v1, v3

    .line 70
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    array-length v3, v3

    .line 67
    add-int/2addr v1, v3

    .line 71
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    array-length v2, v2

    .line 67
    :cond_0
    add-int/2addr v1, v2

    .line 72
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    array-length v2, v2

    .line 67
    add-int v0, v1, v2

    .line 74
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 69
    goto :goto_0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getUserId()[B
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    return-object v0
.end method

.method public getUserIdSize()[B
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    return-object v0
.end method

.method public setDeviceSetting(Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;)V
    .locals 3
    .param p1, "settingType"    # Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/device_setting/DeviceSettingType;->getDeviceSettingTypeValue()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 173
    return-void
.end method

.method public setEncPassword([B)V
    .locals 3
    .param p1, "m_EncPassword"    # [B

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 155
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 157
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    .line 158
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    .line 121
    return-void
.end method

.method public setUserId([B)V
    .locals 3
    .param p1, "m_UserId"    # [B

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 137
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 138
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    .line 139
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCDeviceSettingRecord [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserIdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserIdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_UserId:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPasswordSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPasswordSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_EncPassword:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_DeviceSetting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->m_DeviceSetting:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
