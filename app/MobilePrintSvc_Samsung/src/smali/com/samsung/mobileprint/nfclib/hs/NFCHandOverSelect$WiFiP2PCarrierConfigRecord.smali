.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WiFiP2PCarrierConfigRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;
    }
.end annotation


# instance fields
.field m_CategoryID:[B

.field m_ChannelList:[B

.field m_ChannelListCountry:[B

.field m_ChannelListLength:[B

.field m_ChannelListOperatingClass:[B

.field m_ChannelListType:[B

.field m_ConfigMethods:[B

.field m_DeviceCapabilityBitmap:[B

.field m_DeviceNameLength:[B

.field m_DeviceNameType:[B

.field m_DeviceNameValue:[B

.field m_DevicePassword:[B

.field m_DevicePasswordID:[B

.field m_GroupCapabilityBitmap:[B

.field m_Header:[B

.field m_LengthP2PAttributes:[B

.field m_Length_WSC_Attrib:[B

.field m_Manufacturer:[B

.field m_ManufacturerLength:[B

.field m_ManufacturerValue:[B

.field m_ModelName:[B

.field m_ModelNameLength:[B

.field m_ModelNameValue:[B

.field m_NumberOfChannel:[B

.field m_OOB_DevicePassword:[B

.field m_OOB_DevicePasswordLength:[B

.field m_OOB_GroupOwnerChannelLength:[B

.field m_OOB_GroupOwnerChannelNumber:[B

.field m_OOB_GroupOwnerCountryString:[B

.field m_OOB_GroupOwnerNegotiationChannel:[B

.field m_OOB_GroupOwnerOperatingClass:[B

.field m_OOB_GroupOwnerRoleIndication:[B

.field m_OUI:[B

.field m_P2PCapability:[B

.field m_P2PCapabilityLength:[B

.field m_P2PDeviceAddress:[B

.field m_P2PDeviceInfo:[B

.field m_P2PDeviceInfoLength:[B

.field m_P2PGroupDeviceAddress:[B

.field m_P2PGroupID:[B

.field m_P2PGroupIDLength:[B

.field m_PublicKeyHash:[B

.field m_RF_BandsLength:[B

.field m_RF_BandsType:[B

.field m_RF_BandsValue:[B

.field m_SSID:[B

.field m_SecondaryDeviceTypesNumber:[B

.field m_SerialNumberLength:[B

.field m_SerialNumberType:[B

.field m_SerialNumberValue:[B

.field m_Sub_CategoryID:[B

.field m_UUIDLength:[B

.field m_UUIDValue:[B

.field m_UUID_EType:[B

.field m_WFA_VendorExtension:[B

.field m_WFA_VendorExtensionIDCode:[B

.field m_WFA_VendorExtensionIDCodeLength:[B

.field m_WFA_VendorExtensionVersion:[B

.field m_WFA_VendorExtensionVersionLength:[B

.field m_WFA_VendorExtensionVersionValue:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 411
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440
    const/16 v0, 0x1c

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    .line 441
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    .line 442
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    .line 443
    new-array v0, v2, [B

    const/4 v1, 0x7

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    .line 444
    new-instance v0, Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    .line 445
    new-array v0, v2, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    .line 446
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    .line 448
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    .line 449
    new-array v0, v2, [B

    const/16 v1, 0x2a

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    .line 450
    const/16 v0, 0x14

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    .line 451
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    .line 452
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    .line 453
    new-array v0, v2, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    .line 454
    new-array v0, v2, [B

    aput-byte v3, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsLength:[B

    .line 455
    new-array v0, v3, [B

    aput-byte v3, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    .line 456
    new-array v0, v2, [B

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    .line 457
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    .line 459
    new-array v0, v2, [B

    fill-array-data v0, :array_5

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    .line 460
    new-array v0, v2, [B

    const/16 v1, 0x10

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    .line 461
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    .line 462
    new-array v0, v2, [B

    fill-array-data v0, :array_6

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    .line 463
    new-array v0, v2, [B

    aput-byte v5, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    .line 464
    new-array v0, v5, [B

    const/16 v1, 0x37

    aput-byte v1, v0, v3

    const/16 v1, 0x2a

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    .line 465
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    .line 466
    new-array v0, v3, [B

    aput-byte v3, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    .line 467
    new-array v0, v3, [B

    const/16 v1, 0x20

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    .line 468
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    .line 469
    new-array v0, v3, [B

    aput-byte v2, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    .line 470
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    .line 471
    new-array v0, v3, [B

    aput-byte v6, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    .line 472
    new-array v0, v3, [B

    const/16 v1, 0x41

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    .line 473
    new-array v0, v3, [B

    const/16 v1, 0xd

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    .line 474
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    .line 475
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    .line 476
    new-array v0, v2, [B

    const/16 v1, 0x62

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    .line 477
    new-array v0, v2, [B

    aput-byte v5, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    .line 478
    new-array v0, v6, [B

    const/16 v1, 0x50

    aput-byte v1, v0, v3

    const/16 v1, -0xe

    aput-byte v1, v0, v2

    aput-byte v6, v0, v5

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    .line 479
    new-array v0, v2, [B

    const/4 v1, 0x5

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    .line 480
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    .line 481
    new-array v0, v2, [B

    fill-array-data v0, :array_7

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    .line 482
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    .line 484
    new-array v0, v3, [B

    const/16 v1, 0x13

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    .line 485
    new-array v0, v2, [B

    const/4 v1, 0x6

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    .line 486
    new-array v0, v5, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    .line 487
    new-array v0, v3, [B

    const/16 v1, 0x51

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    .line 488
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    .line 489
    new-array v0, v3, [B

    aput-byte v2, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    .line 490
    new-array v0, v3, [B

    const/16 v1, 0xb

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    .line 491
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    .line 492
    new-array v0, v5, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    .line 493
    new-array v0, v3, [B

    const/16 v1, 0x51

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    .line 494
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    .line 496
    new-array v0, v3, [B

    const/16 v1, 0xf

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    .line 497
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    .line 498
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    return-void

    .line 442
    nop

    :array_0
    .array-data 1
        0x10t
        0x21t
    .end array-data

    .line 445
    nop

    :array_1
    .array-data 1
        0x10t
        0x23t
    .end array-data

    .line 448
    nop

    :array_2
    .array-data 1
        0x10t
        0x2ct
    .end array-data

    .line 453
    nop

    :array_3
    .array-data 1
        0x10t
        0x3ct
    .end array-data

    .line 456
    nop

    :array_4
    .array-data 1
        0x10t
        0x42t
    .end array-data

    .line 459
    nop

    :array_5
    .array-data 1
        0x10t
        0x47t
    .end array-data

    .line 462
    nop

    :array_6
    .array-data 1
        0x10t
        0x49t
    .end array-data

    .line 481
    nop

    :array_7
    .array-data 1
        0x1t
        0x11t
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;)V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 708
    const/4 v0, 0x0

    .line 709
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 710
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 711
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 712
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 713
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 714
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 715
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 716
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    .line 717
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 718
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 719
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 720
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 721
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 722
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 723
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 724
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 725
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 726
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 727
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 728
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    .line 729
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 730
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 731
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 732
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 733
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 734
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 735
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 736
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 737
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 738
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 739
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 740
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 741
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 742
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 743
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 744
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 745
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 746
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 747
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 748
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 749
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 750
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 751
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 752
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 753
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 754
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    .line 755
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 756
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 757
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 758
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 759
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 760
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 761
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 762
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 763
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 764
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 765
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 766
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 767
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    .line 768
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 769
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 770
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 771
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 772
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    add-int/lit8 v3, v3, -0x6

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    .line 773
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 779
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 775
    :catch_0
    move-exception v1

    .line 776
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 506
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    .line 507
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v5}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 508
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v4, v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 509
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v4, v4

    shr-int/lit8 v4, v4, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 512
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$1(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 513
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$2(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 514
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$3(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 516
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$4(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    .line 517
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$4(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B
    invoke-static {v5}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$4(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 518
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v4, v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 519
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v4, v4

    shr-int/lit8 v4, v4, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 521
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$5(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 523
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$6(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 525
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    .line 526
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v5}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 527
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v4, v4

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 528
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v4, v4

    shr-int/lit8 v4, v4, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 530
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$7(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 531
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$7(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 533
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$8(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    .line 534
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$8(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 535
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 537
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$6(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 539
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$9(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    .line 540
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$9(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B
    invoke-static {v5}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$9(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 543
    const/4 v0, 0x0

    .line 545
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v3, v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v4, v4

    add-int v0, v3, v4

    .line 546
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    and-int/lit16 v4, v0, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 547
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    shr-int/lit8 v4, v0, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 550
    const/4 v0, 0x0

    .line 551
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v3, v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v4, v4

    add-int v0, v3, v4

    .line 552
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    and-int/lit16 v4, v0, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 553
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    shr-int/lit8 v4, v0, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 556
    const/4 v0, 0x0

    .line 557
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v3, v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v4, v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v4, v4

    add-int v0, v3, v4

    .line 558
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    and-int/lit16 v4, v0, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v6

    .line 559
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    shr-int/lit8 v4, v0, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 563
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->getByteArraySize()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v2, v3, [B

    .line 566
    .local v2, "nfcData":[B
    new-instance v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;)V

    .line 567
    .local v1, "header":Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;
    iget-object v3, v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->m_Payload_Length:[B

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->getByteArraySize()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 568
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord$Header;->getByteData(I)[B

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    .line 571
    const/4 v0, 0x0

    .line 573
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 574
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 575
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 576
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 577
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 578
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 579
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 580
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 581
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 582
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 583
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 584
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 585
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 586
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 587
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 588
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 589
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 590
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 591
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 592
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 593
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 594
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 595
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 596
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 597
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 598
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 599
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 600
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 601
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 602
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 603
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 604
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 605
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 606
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 607
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 608
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 609
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 610
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 611
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 612
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 613
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 614
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 615
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 616
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 617
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 618
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 619
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 620
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 621
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 622
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 623
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 624
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 625
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 626
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 627
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 628
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 629
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 630
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 631
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 632
    return-object v2
.end method

.method public getByteArraySize()J
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 638
    const/4 v0, 0x0

    .line 639
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapability:[B

    array-length v1, v1

    .line 640
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PCapabilityLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 641
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceCapabilityBitmap:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 642
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_GroupCapabilityBitmap:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 643
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfo:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 644
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceInfoLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 645
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 646
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ConfigMethods:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 647
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_CategoryID:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 648
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OUI:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 649
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Sub_CategoryID:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 650
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SecondaryDeviceTypesNumber:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 651
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameType:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 652
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 653
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DeviceNameValue:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 654
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerNegotiationChannel:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 655
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 656
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 657
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerOperatingClass:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 658
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerChannelNumber:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 659
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerRoleIndication:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 660
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListType:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 661
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 662
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListCountry:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 663
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelListOperatingClass:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 664
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_NumberOfChannel:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 665
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 666
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupID:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 667
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupIDLength:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 668
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PGroupDeviceAddress:[B

    array-length v2, v2

    .line 639
    add-int/2addr v1, v2

    .line 669
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v2, v2

    .line 639
    add-int v0, v1, v2

    .line 670
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    and-int/lit16 v2, v0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 671
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v1, v3

    .line 672
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Manufacturer:[B

    array-length v1, v1

    .line 673
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 674
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ManufacturerValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 675
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelName:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 676
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 677
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 678
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePassword:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 679
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_DevicePasswordLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 680
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 681
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 682
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 683
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsType:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 684
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 685
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_RF_BandsValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 686
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberType:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 687
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 688
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 689
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUID_EType:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 690
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 691
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 692
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtension:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 693
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCodeLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 694
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionIDCode:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 695
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersion:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 696
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionLength:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 697
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_WFA_VendorExtensionVersionValue:[B

    array-length v2, v2

    .line 672
    add-int/2addr v1, v2

    .line 698
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_LengthP2PAttributes:[B

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 699
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    and-int/lit16 v2, v0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 700
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Length_WSC_Attrib:[B

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v1, v3

    .line 701
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_Header:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 702
    int-to-long v1, v0

    return-wide v1
.end method
