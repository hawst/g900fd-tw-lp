.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VerbRecord"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;
    }
.end annotation


# instance fields
.field m_Header:[B

.field m_NoOfServiceTLV:[B

.field m_PrintingProtocol:[B

.field m_ProtocolLength:[B

.field m_ProtocolValue:[B

.field m_VerbID:[B

.field m_VerbType:[B

.field m_VersionNumber:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 900
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 929
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    .line 930
    new-array v0, v3, [B

    aput-byte v3, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    .line 931
    new-array v0, v3, [B

    aput-byte v3, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    .line 932
    new-array v0, v3, [B

    aput-byte v5, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    .line 933
    new-array v0, v3, [B

    aput-byte v3, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    .line 934
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/16 v1, 0x8

    aput-byte v1, v0, v4

    const/4 v1, 0x2

    const/16 v2, 0x9

    aput-byte v2, v0, v1

    aput-byte v3, v0, v5

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    .line 935
    new-array v0, v3, [B

    const/16 v1, 0xd

    aput-byte v1, v0, v4

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    .line 936
    new-instance v0, Ljava/lang/String;

    const-string v1, "v1;wfds-print"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;)V
    .locals 0

    .prologue
    .line 900
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 982
    const/4 v0, 0x0

    .line 983
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 984
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 985
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 986
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 987
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 988
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 989
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 991
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 996
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 992
    :catch_0
    move-exception v1

    .line 993
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 942
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->getByteArraySize()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v2, v3, [B

    .line 945
    .local v2, "nfcData":[B
    new-instance v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;)V

    .line 946
    .local v1, "header":Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;
    iget-object v3, v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;->m_Payload_Length:[B

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->getByteArraySize()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 947
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord$Header;->getByteData(I)[B

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    .line 950
    const/4 v0, 0x0

    .line 952
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 953
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 954
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 955
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 956
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 957
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 958
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 959
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 960
    return-object v2
.end method

.method public getByteArraySize()J
    .locals 3

    .prologue
    .line 966
    const/4 v0, 0x0

    .line 967
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_Header:[B

    array-length v1, v1

    .line 968
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VersionNumber:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 969
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbID:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 970
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_VerbType:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 971
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_NoOfServiceTLV:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 972
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_PrintingProtocol:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 973
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolLength:[B

    array-length v2, v2

    .line 967
    add-int/2addr v1, v2

    .line 974
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->m_ProtocolValue:[B

    array-length v2, v2

    .line 967
    add-int v0, v1, v2

    .line 975
    int-to-long v1, v0

    return-wide v1
.end method
