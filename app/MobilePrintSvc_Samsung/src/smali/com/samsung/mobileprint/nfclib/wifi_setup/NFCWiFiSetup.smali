.class public Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
.super Ljava/lang/Object;
.source "NFCWiFiSetup.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;


# instance fields
.field private m_Authentication:[B

.field private m_Channel:[B

.field private m_EncPWdSize:[B

.field private m_EncPwd:[B

.field private m_EncWEPKey:[B

.field private m_EncWEPKeySize:[B

.field private m_EncWPAKey:[B

.field private m_EncWPAKeySize:[B

.field private m_Encryption:[B

.field private m_Gateway:[B

.field private m_IPAddress:[B

.field private m_IPSettingType:[B

.field private m_SSID:[B

.field private m_SSIDSize:[B

.field private m_SamsungHeader:[B

.field private m_Security:[B

.field private m_SubnetMask:[B

.field private m_UserId:[B

.field private m_UserIdSize:[B

.field private m_WEPKeyIndex:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    .line 37
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    .line 39
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    .line 41
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    .line 43
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    .line 44
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    .line 45
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    .line 46
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    .line 47
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    .line 48
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    .line 50
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    .line 52
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    .line 53
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    .line 54
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    .line 55
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    .line 59
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 63
    new-instance v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    .line 64
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->setSamsungHeader([B)V

    .line 65
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 70
    new-instance v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    .line 71
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->byteStreamToClass([B)Z

    .line 72
    sget-object v0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_NFCWiFiSetup:Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 140
    const/4 v0, 0x0

    .line 143
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 145
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 146
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    .line 147
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 149
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 150
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    .line 151
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 153
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 154
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    .line 155
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 157
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 158
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 159
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 160
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 161
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 164
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 165
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    .line 166
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 168
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 169
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    .line 170
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 172
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 173
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 174
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 175
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 181
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 177
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getAuthentication()[B
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    return-object v0
.end method

.method public getChannel()[B
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    return-object v0
.end method

.method public getEncPWdSize()[B
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    return-object v0
.end method

.method public getEncPwd()[B
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    return-object v0
.end method

.method public getEncWEPKey()[B
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    return-object v0
.end method

.method public getEncWEPKeySize()[B
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    return-object v0
.end method

.method public getEncWPAKey()[B
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    return-object v0
.end method

.method public getEncWPAKeySize()[B
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    return-object v0
.end method

.method public getEncryption()[B
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    return-object v0
.end method

.method public getGateway()[B
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    return-object v0
.end method

.method public getIPAddress()[B
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    return-object v0
.end method

.method public getIPSettingType()[B
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 107
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 108
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 109
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 110
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 111
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 113
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 114
    :cond_1
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 115
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 118
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 119
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 120
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 121
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 123
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 124
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 126
    :cond_3
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 127
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 129
    :cond_4
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 130
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 131
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 132
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 134
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 80
    const/4 v0, 0x0

    .line 81
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    array-length v1, v1

    .line 82
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    array-length v3, v3

    .line 81
    add-int/2addr v3, v1

    .line 83
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    array-length v1, v1

    .line 81
    :goto_0
    add-int/2addr v1, v3

    .line 84
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    array-length v3, v3

    .line 81
    add-int/2addr v3, v1

    .line 85
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    array-length v1, v1

    .line 81
    :goto_1
    add-int/2addr v1, v3

    .line 86
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    array-length v3, v3

    .line 81
    add-int/2addr v3, v1

    .line 87
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    array-length v1, v1

    .line 81
    :goto_2
    add-int/2addr v1, v3

    .line 88
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 89
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 90
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 91
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 92
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 93
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    array-length v3, v3

    .line 81
    add-int/2addr v3, v1

    .line 94
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    array-length v1, v1

    .line 81
    :goto_3
    add-int/2addr v1, v3

    .line 95
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    array-length v3, v3

    .line 81
    add-int/2addr v1, v3

    .line 96
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    array-length v2, v2

    .line 81
    :cond_0
    add-int/2addr v1, v2

    .line 97
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    array-length v2, v2

    .line 81
    add-int/2addr v1, v2

    .line 98
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    array-length v2, v2

    .line 81
    add-int/2addr v1, v2

    .line 99
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    array-length v2, v2

    .line 81
    add-int/2addr v1, v2

    .line 100
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    array-length v2, v2

    .line 81
    add-int v0, v1, v2

    .line 101
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 83
    goto :goto_0

    :cond_2
    move v1, v2

    .line 85
    goto :goto_1

    :cond_3
    move v1, v2

    .line 87
    goto :goto_2

    :cond_4
    move v1, v2

    .line 94
    goto :goto_3
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 352
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSSID()[B
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    return-object v0
.end method

.method public getSSIDSize()[B
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getSecurity()[B
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    return-object v0
.end method

.method public getSubnetMask()[B
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    return-object v0
.end method

.method public getUserId()[B
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    return-object v0
.end method

.method public getUserIdSize()[B
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    return-object v0
.end method

.method public getWEPKeyIndex()[B
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    return-object v0
.end method

.method public setAuthentication(Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;)V
    .locals 3
    .param p1, "m_Authentication"    # Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiAuthentication;->getValue()B

    move-result v2

    aput-byte v2, v0, v1

    .line 279
    return-void
.end method

.method public setChannel([B)V
    .locals 0
    .param p1, "m_Channel"    # [B

    .prologue
    .line 254
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    .line 255
    return-void
.end method

.method public setEncPwd([B)V
    .locals 3
    .param p1, "m_EncPwd"    # [B

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 231
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 232
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    .line 233
    return-void
.end method

.method public setEncWEPKey([B)V
    .locals 3
    .param p1, "m_EncWEPKey"    # [B

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 299
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 300
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    .line 301
    return-void
.end method

.method public setEncWPAKey([B)V
    .locals 3
    .param p1, "m_EncWPAKey"    # [B

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 313
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 314
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    .line 315
    return-void
.end method

.method public setEncryption(Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiEncryption;)V
    .locals 3
    .param p1, "m_Encryption"    # Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiEncryption;

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiEncryption;->getValue()B

    move-result v2

    aput-byte v2, v0, v1

    .line 271
    return-void
.end method

.method public setGateway([B)V
    .locals 0
    .param p1, "m_Gateway"    # [B

    .prologue
    .line 346
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    .line 347
    return-void
.end method

.method public setIPAddress([B)V
    .locals 0
    .param p1, "m_IPAddress"    # [B

    .prologue
    .line 330
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    .line 331
    return-void
.end method

.method public setIPSettingType(Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiIPSetting;)V
    .locals 3
    .param p1, "m_IPSettingType"    # Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiIPSetting;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiIPSetting;->getValue()B

    move-result v2

    aput-byte v2, v0, v1

    .line 323
    return-void
.end method

.method public setSSID([B)V
    .locals 3
    .param p1, "m_SSID"    # [B

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 245
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 246
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    .line 247
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    .line 205
    return-void
.end method

.method public setSecurity(Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiSecurity;)V
    .locals 3
    .param p1, "m_Security"    # Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiSecurity;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/mobileprint/nfclib/wifi_setup/WiFiSecurity;->getValue()B

    move-result v2

    aput-byte v2, v0, v1

    .line 263
    return-void
.end method

.method public setSubnetMask([B)V
    .locals 0
    .param p1, "m_SubnetMask"    # [B

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    .line 339
    return-void
.end method

.method public setUserId([B)V
    .locals 3
    .param p1, "m_UserId"    # [B

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 217
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 218
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    .line 219
    return-void
.end method

.method public setWEPKeyIndex([B)V
    .locals 0
    .param p1, "m_WEPKeyIndex"    # [B

    .prologue
    .line 286
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    .line 287
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCWiFiSetup [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserIdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserIdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_UserId:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPWdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPWdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncPwd:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_SSIDSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSIDSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_SSID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SSID:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Channel:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Security="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Security:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Encryption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Encryption:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Authentication="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Authentication:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WEPKeyIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_WEPKeyIndex:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncWEPKeySize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKeySize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncWEPKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWEPKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncWPAKeySize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKeySize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncWPAKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_EncWPAKey:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_IPSettingType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPSettingType:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_IPAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_IPAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_SubnetMask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_SubnetMask:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Gateway="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->m_Gateway:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
