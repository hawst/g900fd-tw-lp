.class public Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;
.super Ljava/lang/Object;
.source "NFCUtils.java"


# static fields
.field private static final ENC:Ljava/lang/String; = "tfd11111"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ByteArrayToIPAddress([B)Ljava/lang/String;
    .locals 3
    .param p0, "ipAddress"    # [B

    .prologue
    .line 89
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    .line 90
    .local v0, "a":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 95
    .end local v0    # "a":Ljava/net/InetAddress;
    :goto_0
    return-object v2

    .line 91
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 95
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static ByteArrayToMacAddress([B)Ljava/lang/String;
    .locals 8
    .param p0, "macAddress"    # [B

    .prologue
    const/4 v3, 0x0

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 100
    .local v1, "sb":Ljava/lang/StringBuilder;
    array-length v4, p0

    move v2, v3

    :goto_0
    if-lt v2, v4, :cond_0

    .line 105
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 100
    :cond_0
    aget-byte v0, p0, v2

    .line 101
    .local v0, "b":B
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 102
    const/16 v5, 0x3a

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static CheckChecksum(Ljava/lang/String;)Z
    .locals 8
    .param p0, "pin"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x7

    const/4 v4, 0x0

    .line 43
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 44
    invoke-virtual {p0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 45
    .local v2, "lPin":J
    invoke-static {v2, v3}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->wps_computeChecksum(J)J

    move-result-wide v0

    .line 46
    .local v0, "checksum":J
    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    cmp-long v5, v0, v5

    if-nez v5, :cond_0

    .line 47
    const/4 v4, 0x1

    .line 49
    .end local v0    # "checksum":J
    .end local v2    # "lPin":J
    :cond_0
    return v4
.end method

.method public static IPAddressToByteArray(Ljava/lang/String;)[B
    .locals 6
    .param p0, "ipAddress"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    .line 70
    new-array v2, v5, [B

    .line 72
    .local v2, "ipAddressBytes":[B
    :try_start_0
    const-string v4, "\\."

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 75
    .local v3, "ipAddressParts":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v5, :cond_0

    .line 82
    .end local v0    # "i":I
    .end local v3    # "ipAddressParts":[Ljava/lang/String;
    :goto_1
    return-object v2

    .line 76
    .restart local v0    # "i":I
    .restart local v3    # "ipAddressParts":[Ljava/lang/String;
    :cond_0
    aget-object v4, v3, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 77
    .local v1, "integer":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->byteValue()B

    move-result v4

    aput-byte v4, v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    .end local v0    # "i":I
    .end local v1    # "integer":Ljava/lang/Integer;
    .end local v3    # "ipAddressParts":[Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static MacAddressToByteArray(Ljava/lang/String;)[B
    .locals 7
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x6

    .line 53
    new-array v3, v6, [B

    .line 55
    .local v3, "macAddressBytes":[B
    :try_start_0
    const-string v4, ":"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 58
    .local v2, "mac":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 66
    .end local v1    # "i":I
    .end local v2    # "mac":[Ljava/lang/String;
    :goto_1
    return-object v3

    .line 59
    .restart local v1    # "i":I
    .restart local v2    # "mac":[Ljava/lang/String;
    :cond_0
    aget-object v4, v2, v1

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 60
    .local v0, "hex":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->byteValue()B

    move-result v4

    aput-byte v4, v3, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    .end local v0    # "hex":Ljava/lang/Integer;
    .end local v1    # "i":I
    .end local v2    # "mac":[Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static bytArrayToHex([B)Ljava/lang/String;
    .locals 8
    .param p0, "a"    # [B

    .prologue
    const/4 v3, 0x0

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v1, "sb":Ljava/lang/StringBuilder;
    array-length v4, p0

    move v2, v3

    :goto_0
    if-lt v2, v4, :cond_0

    .line 146
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 144
    :cond_0
    aget-byte v0, p0, v2

    .line 145
    .local v0, "b":B
    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    and-int/lit16 v7, v0, 0xff

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static flipWFDToWiFiMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "macAddress"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x10

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 127
    const/4 v3, 0x0

    .line 129
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v9}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    .line 131
    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v9}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 130
    add-int/2addr v4, v5

    int-to-byte v0, v4

    .line 132
    .local v0, "b":B
    const/4 v2, 0x2

    .line 133
    .local v2, "shift":B
    xor-int v4, v0, v2

    int-to-byte v0, v4

    .line 135
    const-string v4, "%02x"

    new-array v5, v6, [Ljava/lang/Object;

    and-int/lit16 v6, v0, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 136
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 137
    .local v1, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v1, v7, v8, v3}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    return-object v3
.end method

.method public static getDecryptedString([B)Ljava/lang/String;
    .locals 2
    .param p0, "pin"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    const-string v0, "tfd11111"

    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getIncrementedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0, v1}, Lcom/samsung/mobileprint/nfclib/utils/SimpleCrypto;->decrypt([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDecryptedString([BLjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "AESkey"    # [B
    .param p1, "pin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/samsung/mobileprint/nfclib/utils/SimpleCrypto;->decrypt([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getEncyptedByteArray(Ljava/lang/String;)[B
    .locals 1
    .param p0, "pin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 25
    if-nez p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "tfd11111"

    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getIncrementedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, p0}, Lcom/samsung/mobileprint/nfclib/utils/SimpleCrypto;->encrypt([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method public static getEncyptedByteArray([BLjava/lang/String;)[B
    .locals 1
    .param p0, "AESkey"    # [B
    .param p1, "pin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    if-nez p1, :cond_0

    const-string v0, ""

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/samsung/mobileprint/nfclib/utils/SimpleCrypto;->encrypt([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private static getIncrementedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 18
    :cond_0
    aget-char v0, v3, v2

    .line 19
    .local v0, "c":C
    add-int/lit8 v5, v0, -0x1

    int-to-char v0, v5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 18
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getInt([BI)I
    .locals 2
    .param p0, "arr"    # [B
    .param p1, "off"    # I

    .prologue
    .line 13
    aget-byte v0, p0, p1

    shl-int/lit8 v0, v0, 0x8

    const v1, 0xff00

    and-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method private static wps_computeChecksum(J)J
    .locals 12
    .param p0, "PIN"    # J

    .prologue
    const-wide/16 v10, 0x1

    const-wide/16 v8, 0x3

    const-wide/16 v6, 0xa

    .line 112
    const-wide/16 v0, 0x0

    .line 114
    .local v0, "accum":J
    mul-long/2addr p0, v6

    .line 115
    const-wide/32 v4, 0x989680

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v8

    add-long/2addr v0, v4

    .line 116
    const-wide/32 v4, 0xf4240

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v10

    add-long/2addr v0, v4

    .line 117
    const-wide/32 v4, 0x186a0

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v8

    add-long/2addr v0, v4

    .line 118
    const-wide/16 v4, 0x2710

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v10

    add-long/2addr v0, v4

    .line 119
    const-wide/16 v4, 0x3e8

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v8

    add-long/2addr v0, v4

    .line 120
    const-wide/16 v4, 0x64

    div-long v4, p0, v4

    rem-long/2addr v4, v6

    mul-long/2addr v4, v10

    add-long/2addr v0, v4

    .line 121
    div-long v4, p0, v6

    rem-long/2addr v4, v6

    mul-long/2addr v4, v8

    add-long/2addr v0, v4

    .line 122
    rem-long v2, v0, v6

    .line 123
    .local v2, "digit":J
    sub-long v4, v6, v2

    rem-long/2addr v4, v6

    return-wide v4
.end method
