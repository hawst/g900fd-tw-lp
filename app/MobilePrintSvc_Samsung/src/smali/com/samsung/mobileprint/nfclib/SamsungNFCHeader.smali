.class public Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
.super Ljava/lang/Object;
.source "SamsungNFCHeader.java"


# static fields
.field private static mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;


# instance fields
.field private m_ApplicationType:B

.field private m_ConnectivityType:B

.field private m_DataType:B

.field private m_DeviceType:B

.field private m_MajorVersion:B

.field private m_MinorVersion:B

.field private m_RecordType:Lcom/samsung/mobileprint/nfclib/RecordType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MajorVersion:B

    .line 10
    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MinorVersion:B

    .line 11
    const/4 v0, 0x3

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DeviceType:B

    .line 12
    sget-object v0, Lcom/samsung/mobileprint/nfclib/DataType;->DATA_TYPE_ACTIVE:Lcom/samsung/mobileprint/nfclib/DataType;

    invoke-virtual {v0}, Lcom/samsung/mobileprint/nfclib/DataType;->getDataTypeValue()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DataType:B

    .line 13
    sget-object v0, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->CONNECTIVITY_TYPE_WFD_WIFI:Lcom/samsung/mobileprint/nfclib/ConnectivityType;

    invoke-virtual {v0}, Lcom/samsung/mobileprint/nfclib/ConnectivityType;->getConnectivityTypeValue()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ConnectivityType:B

    .line 14
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {v0}, Lcom/samsung/mobileprint/nfclib/RecordType;->getRecordTypeValue()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ApplicationType:B

    .line 15
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_RecordType:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 19
    return-void
.end method

.method public static CreateSamsungHeader(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    .locals 2
    .param p0, "type"    # Lcom/samsung/mobileprint/nfclib/RecordType;
    .param p1, "majorversion"    # B
    .param p2, "minVersion"    # B

    .prologue
    .line 33
    new-instance v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    .line 34
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v0, p1}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setMajorVersion(B)V

    .line 35
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v0, p2}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setMinorVersion(B)V

    .line 36
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/RecordType;->getRecordTypeValue()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setApplicationType(B)V

    .line 37
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    return-object v0
.end method

.method public static CreateSamsungHeader([BZ)Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    .locals 1
    .param p0, "message"    # [B
    .param p1, "isNdefMessage"    # Z

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    .line 24
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v0, p0, p1}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getNFCHeaderFromData([BZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    sget-object v0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    .line 27
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getApplicationType()B
    .locals 1

    .prologue
    .line 122
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ApplicationType:B

    return v0
.end method

.method public getConnectivityType()B
    .locals 1

    .prologue
    .line 116
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ConnectivityType:B

    return v0
.end method

.method public getDataType()B
    .locals 1

    .prologue
    .line 128
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DataType:B

    return v0
.end method

.method public getDeviceType()B
    .locals 1

    .prologue
    .line 110
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DeviceType:B

    return v0
.end method

.method public getMajorVersion()B
    .locals 1

    .prologue
    .line 98
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MajorVersion:B

    return v0
.end method

.method public getMinorVersion()B
    .locals 1

    .prologue
    .line 104
    iget-byte v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MinorVersion:B

    return v0
.end method

.method protected getNFCHeaderFromData([BZ)Z
    .locals 9
    .param p1, "message"    # [B
    .param p2, "isNdefMessage"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 50
    const/4 v4, 0x0

    .line 51
    .local v4, "record":Landroid/nfc/NdefRecord;
    const/4 v1, 0x0

    .line 52
    .local v1, "data":[B
    if-eqz p2, :cond_0

    .line 55
    :try_start_0
    new-instance v3, Landroid/nfc/NdefMessage;

    invoke-direct {v3, p1}, Landroid/nfc/NdefMessage;-><init>([B)V

    .line 56
    .local v3, "ndefMessage":Landroid/nfc/NdefMessage;
    invoke-virtual {v3}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v4, v7, v8

    .line 57
    invoke-virtual {v4}, Landroid/nfc/NdefRecord;->getPayload()[B
    :try_end_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 78
    .end local v3    # "ndefMessage":Landroid/nfc/NdefMessage;
    :goto_0
    array-length v7, v1

    const/4 v8, 0x4

    if-lt v7, v8, :cond_1

    .line 79
    aget-byte v7, v1, v6

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 80
    .local v0, "b":Ljava/lang/Byte;
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    ushr-int/lit8 v7, v7, 0x4

    int-to-byte v7, v7

    invoke-virtual {p0, v7}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setMajorVersion(B)V

    .line 81
    aget-byte v6, v1, v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    and-int/lit8 v6, v6, 0xf

    int-to-byte v6, v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setMinorVersion(B)V

    .line 83
    aget-byte v6, v1, v5

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    ushr-int/lit8 v6, v6, 0x2

    int-to-byte v6, v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setDeviceType(B)V

    .line 85
    aget-byte v6, v1, v5

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    and-int/lit8 v6, v6, 0x3

    int-to-byte v6, v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setDataType(B)V

    .line 87
    const/4 v6, 0x2

    aget-byte v6, v1, v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setConnectivityType(B)V

    .line 89
    const/4 v6, 0x3

    aget-byte v6, v1, v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setApplicationType(B)V

    .line 91
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    invoke-static {v6}, Lcom/samsung/mobileprint/nfclib/RecordType;->forValue(B)Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->setRecordtype(Lcom/samsung/mobileprint/nfclib/RecordType;)V

    .line 94
    .end local v0    # "b":Ljava/lang/Byte;
    :goto_1
    return v5

    .line 58
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Landroid/nfc/FormatException;
    invoke-virtual {v2}, Landroid/nfc/FormatException;->printStackTrace()V

    goto :goto_0

    .line 64
    .end local v2    # "e":Landroid/nfc/FormatException;
    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move v5, v6

    .line 94
    goto :goto_1
.end method

.method public getRecordType()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_RecordType:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeaderByteArray()[B
    .locals 5

    .prologue
    .line 41
    sget-object v3, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v3}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMajorVersion()B

    move-result v3

    shl-int/lit8 v3, v3, 0x4

    sget-object v4, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v4

    add-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    .line 42
    .local v2, "version":Ljava/lang/Byte;
    sget-object v3, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v3}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getDeviceType()B

    move-result v3

    shl-int/lit8 v3, v3, 0x2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getDataType()B

    move-result v4

    add-int/2addr v3, v4

    int-to-char v0, v3

    .line 43
    .local v0, "DDType":C
    const/4 v3, 0x4

    new-array v1, v3, [B

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    aput-byte v4, v1, v3

    const/4 v3, 0x1

    int-to-byte v4, v0

    aput-byte v4, v1, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getConnectivityType()B

    move-result v4

    aput-byte v4, v1, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->mHeader:Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getApplicationType()B

    move-result v4

    aput-byte v4, v1, v3

    .line 44
    .local v1, "SSHeader":[B
    return-object v1
.end method

.method public setApplicationType(B)V
    .locals 0
    .param p1, "m_ApplicationType"    # B

    .prologue
    .line 125
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ApplicationType:B

    .line 126
    return-void
.end method

.method public setConnectivityType(B)V
    .locals 0
    .param p1, "m_ConnectivityType"    # B

    .prologue
    .line 119
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_ConnectivityType:B

    .line 120
    return-void
.end method

.method public setDataType(B)V
    .locals 0
    .param p1, "m_DataType"    # B

    .prologue
    .line 131
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DataType:B

    .line 132
    return-void
.end method

.method public setDeviceType(B)V
    .locals 0
    .param p1, "m_DeviceType"    # B

    .prologue
    .line 113
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_DeviceType:B

    .line 114
    return-void
.end method

.method public setMajorVersion(B)V
    .locals 0
    .param p1, "m_MajorVersion"    # B

    .prologue
    .line 101
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MajorVersion:B

    .line 102
    return-void
.end method

.method public setMinorVersion(B)V
    .locals 0
    .param p1, "m_MinorVersion"    # B

    .prologue
    .line 107
    iput-byte p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_MinorVersion:B

    .line 108
    return-void
.end method

.method public setRecordtype(Lcom/samsung/mobileprint/nfclib/RecordType;)V
    .locals 0
    .param p1, "m_RecordType"    # Lcom/samsung/mobileprint/nfclib/RecordType;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->m_RecordType:Lcom/samsung/mobileprint/nfclib/RecordType;

    .line 140
    return-void
.end method
