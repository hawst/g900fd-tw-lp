.class public Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
.super Ljava/lang/Object;
.source "NFCAuthentication.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;


# instance fields
.field private m_Domain:[B

.field private m_DomainSize:[B

.field private m_EncPWdSize:[B

.field private m_EncPwd:[B

.field private m_SamsungHeader:[B

.field private m_UID:[B

.field private m_UIDSize:[B

.field private m_UserId:[B

.field private m_UserIdSize:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    .line 12
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    .line 14
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    .line 16
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    .line 18
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    .line 24
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 28
    new-instance v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    .line 29
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->setSamsungHeader([B)V

    .line 30
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 35
    new-instance v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    .line 36
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->byteStreamToClass([B)Z

    .line 37
    sget-object v0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_NFCAuthentication:Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 79
    const/4 v0, 0x0

    .line 82
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 84
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 85
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    .line 86
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 88
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 89
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    .line 90
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 92
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 93
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    .line 94
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 96
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 97
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getInt([BI)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    .line 98
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 104
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 100
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getDomain()[B
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    return-object v0
.end method

.method public getDomainSize()[B
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    return-object v0
.end method

.method public getEncPWdSize()[B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    return-object v0
.end method

.method public getEncPwd()[B
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 63
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 64
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 65
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 66
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 68
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 70
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 71
    :cond_2
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 72
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 73
    :cond_3
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    array-length v1, v1

    .line 47
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    array-length v3, v3

    .line 46
    add-int/2addr v3, v1

    .line 48
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    array-length v1, v1

    .line 46
    :goto_0
    add-int/2addr v1, v3

    .line 49
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    array-length v3, v3

    .line 46
    add-int/2addr v3, v1

    .line 50
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    array-length v1, v1

    .line 46
    :goto_1
    add-int/2addr v1, v3

    .line 51
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    array-length v3, v3

    .line 46
    add-int/2addr v3, v1

    .line 52
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    array-length v1, v1

    .line 46
    :goto_2
    add-int/2addr v1, v3

    .line 53
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    array-length v3, v3

    .line 46
    add-int/2addr v1, v3

    .line 54
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    array-length v2, v2

    .line 46
    :cond_0
    add-int v0, v1, v2

    .line 56
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 48
    goto :goto_0

    :cond_2
    move v1, v2

    .line 50
    goto :goto_1

    :cond_3
    move v1, v2

    .line 52
    goto :goto_2
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getUID()[B
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    return-object v0
.end method

.method public getUIDSize()[B
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    return-object v0
.end method

.method public getUserId()[B
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    return-object v0
.end method

.method public getUserIdSize()[B
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    return-object v0
.end method

.method public setDomain([B)V
    .locals 3
    .param p1, "m_Domain"    # [B

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 154
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 156
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    .line 157
    return-void
.end method

.method public setEncPwd([B)V
    .locals 3
    .param p1, "m_EncPwd"    # [B

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 183
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 184
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    .line 185
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    .line 127
    return-void
.end method

.method public setUID([B)V
    .locals 3
    .param p1, "m_UID"    # [B

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 140
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 141
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    .line 142
    return-void
.end method

.method public setUserId([B)V
    .locals 3
    .param p1, "m_UserId"    # [B

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    const/4 v1, 0x1

    array-length v2, p1

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 169
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    const/4 v1, 0x0

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 170
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    .line 171
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCAuthentication [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UIDSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UIDSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UID:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_DomainSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_DomainSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_Domain="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_Domain:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserIdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserIdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_UserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_UserId:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPWdSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPWdSize:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->m_EncPwd:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
