.class public Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
.super Ljava/lang/Object;
.source "NFCMPConnectV3.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;


# instance fields
.field private m_EncPwd:[B

.field private m_ModelName:[B

.field private m_NetMacAddress:[B

.field private m_SamsungHeader:[B

.field private m_WFDMacAddress:[B

.field private m_WiFiMacAddress:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    .line 38
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 42
    new-instance v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .line 43
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->setSamsungHeader([B)V

    .line 44
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 49
    new-instance v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .line 50
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->byteStreamToClass([B)Z

    .line 51
    sget-object v0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NFCMPConnectV3:Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 12
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 93
    iget-object v8, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    iget-object v9, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    array-length v9, v9

    invoke-static {p1, v6, v8, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    const/4 v8, 0x4

    array-length v9, p1

    invoke-static {p1, v8, v9}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    .line 95
    .local v3, "payload":[B
    if-eqz v3, :cond_0

    .line 97
    :try_start_0
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v3}, Ljava/lang/String;-><init>([B)V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 98
    .local v4, "splitedString":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 99
    .local v0, "count":I
    array-length v9, v4

    move v8, v6

    :goto_0
    if-lt v8, v9, :cond_1

    .end local v0    # "count":I
    .end local v4    # "splitedString":[Ljava/lang/String;
    :cond_0
    move v6, v7

    .line 125
    :goto_1
    return v6

    .line 99
    .restart local v0    # "count":I
    .restart local v4    # "splitedString":[Ljava/lang/String;
    :cond_1
    aget-object v2, v4, v8

    .line 101
    .local v2, "info":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 103
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    .line 117
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 99
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 104
    :cond_3
    if-ne v0, v7, :cond_4

    .line 106
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    .line 107
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x5

    if-le v10, v11, :cond_2

    .line 108
    invoke-static {v2}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->flipWFDToWiFiMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "wfdmac":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    goto :goto_2

    .line 121
    .end local v0    # "count":I
    .end local v2    # "info":Ljava/lang/String;
    .end local v4    # "splitedString":[Ljava/lang/String;
    .end local v5    # "wfdmac":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 111
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "count":I
    .restart local v2    # "info":Ljava/lang/String;
    .restart local v4    # "splitedString":[Ljava/lang/String;
    :cond_4
    const/4 v10, 0x2

    if-ne v0, v10, :cond_5

    .line 112
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    goto :goto_2

    .line 113
    :cond_5
    const/4 v10, 0x3

    if-ne v0, v10, :cond_2

    .line 114
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "messenger"    # Landroid/os/Messenger;

    .prologue
    .line 142
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>([B)V

    .line 143
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 144
    .local v2, "modelName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getDecryptedString([B)Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public connectWFD(Landroid/content/Context;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wfdConnectionListener"    # Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    .prologue
    .line 155
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>([B)V

    .line 156
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 157
    .local v2, "modelName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->getDecryptedString([B)Ljava/lang/String;

    move-result-object v3

    .line 158
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getEncPwd()[B
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    return-object v0
.end method

.method public getModelName()[B
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 59
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 61
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 62
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 63
    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v0, v2

    .line 64
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 65
    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v0, v2

    .line 66
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 67
    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v2, "/"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v0, v2

    .line 68
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 69
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    array-length v3, v1

    .line 78
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    array-length v1, v1

    .line 77
    :goto_0
    add-int/2addr v1, v3

    .line 79
    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 77
    add-int/2addr v3, v1

    .line 80
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    array-length v1, v1

    .line 77
    :goto_1
    add-int/2addr v1, v3

    .line 81
    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 77
    add-int/2addr v3, v1

    .line 82
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    array-length v1, v1

    .line 77
    :goto_2
    add-int/2addr v1, v3

    .line 83
    const-string v3, "/"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 77
    add-int/2addr v1, v3

    .line 84
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    array-length v2, v2

    .line 77
    :cond_0
    add-int v0, v1, v2

    .line 85
    int-to-long v1, v0

    return-wide v1

    :cond_1
    move v1, v2

    .line 78
    goto :goto_0

    :cond_2
    move v1, v2

    .line 80
    goto :goto_1

    :cond_3
    move v1, v2

    .line 82
    goto :goto_2
.end method

.method public getNWMacAddress()[B
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    return-object v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    return-object v0
.end method

.method public getWFDMacAddress()[B
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    return-object v0
.end method

.method public getWiFiMacAddress()[B
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    return-object v0
.end method

.method public setEncPwd([B)V
    .locals 0
    .param p1, "m_EncPwd"    # [B

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    .line 217
    return-void
.end method

.method public setModelName([B)V
    .locals 0
    .param p1, "m_ModelName"    # [B

    .prologue
    .line 182
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    .line 183
    return-void
.end method

.method public setNWMacAddress([B)V
    .locals 0
    .param p1, "m_NWMacAddress"    # [B

    .prologue
    .line 208
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    .line 209
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    .line 174
    return-void
.end method

.method public setWFDMacAddress([B)V
    .locals 2
    .param p1, "m_WFDMacAddress"    # [B

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    .line 193
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    .line 194
    .local v0, "wiFiMac":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->flipWFDToWiFiMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    .line 196
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    .line 197
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCMPConnectV3 [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 19
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_ModelName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_ModelName:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WFDMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WFDMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_WiFiMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_WiFiMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_NetMacAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_NetMacAddress:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_EncPwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->m_EncPwd:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
