.class public Lcom/samsung/mobileprint/nfclib/NFCWriteTag;
.super Ljava/lang/Object;
.source "NFCWriteTag.java"


# static fields
.field private static ERR_NDEF_MESSAGE_NULL:I

.field private static ERR_NDEF_MESSAGE_SIZE_TOO_BIG:I

.field public static ERR_NDEF_MESSAGE_WRITE_FAIL:I

.field public static ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

.field private static ERR_NDEF_TAG_FORMAT_SUCCESS:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

    .line 29
    const/4 v0, 0x1

    sput v0, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_TAG_FORMAT_SUCCESS:I

    .line 30
    const/4 v0, -0x1

    sput v0, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_NULL:I

    .line 31
    const/4 v0, -0x2

    sput v0, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_SIZE_TOO_BIG:I

    .line 32
    const/4 v0, -0x3

    sput v0, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNdefMessage(Lcom/samsung/mobileprint/nfclib/INFCRecord;)Landroid/nfc/NdefMessage;
    .locals 10
    .param p0, "record"    # Lcom/samsung/mobileprint/nfclib/INFCRecord;

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 36
    const-string v1, "com.sec.mobileprint.nfc:extType"

    .line 37
    .local v1, "recordType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 38
    .local v2, "records":[Landroid/nfc/NdefRecord;
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    if-eqz v3, :cond_1

    .line 39
    new-array v2, v4, [Landroid/nfc/NdefRecord;

    .line 40
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    .line 41
    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {v3}, Lcom/samsung/mobileprint/nfclib/RecordType;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v8

    .line 68
    :cond_0
    :goto_0
    new-instance v0, Landroid/nfc/NdefMessage;

    invoke-direct {v0, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 69
    .local v0, "ndefMessage":Landroid/nfc/NdefMessage;
    return-object v0

    .line 42
    .end local v0    # "ndefMessage":Landroid/nfc/NdefMessage;
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_1
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    if-eqz v3, :cond_2

    .line 43
    new-array v2, v4, [Landroid/nfc/NdefRecord;

    .line 44
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    .line 45
    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {v3}, Lcom/samsung/mobileprint/nfclib/RecordType;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v8

    goto :goto_0

    .line 46
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_2
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    if-eqz v3, :cond_3

    .line 47
    new-array v2, v4, [Landroid/nfc/NdefRecord;

    .line 48
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    .line 49
    sget-object v3, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {v3}, Lcom/samsung/mobileprint/nfclib/RecordType;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v8

    goto :goto_0

    .line 50
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_3
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    if-eqz v3, :cond_4

    .line 51
    new-array v2, v8, [Landroid/nfc/NdefRecord;

    .line 52
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    goto :goto_0

    .line 54
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_4
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    if-eqz v3, :cond_5

    .line 55
    new-array v2, v8, [Landroid/nfc/NdefRecord;

    .line 56
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    goto/16 :goto_0

    .line 58
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_5
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    if-eqz v3, :cond_6

    .line 59
    new-array v2, v8, [Landroid/nfc/NdefRecord;

    .line 60
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    goto/16 :goto_0

    .line 62
    .restart local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_6
    instance-of v3, p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    if-eqz v3, :cond_0

    .line 63
    new-array v2, v8, [Landroid/nfc/NdefRecord;

    .line 64
    new-instance v3, Landroid/nfc/NdefRecord;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    new-array v5, v7, [B

    check-cast p0, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    .end local p0    # "record":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->getNFCByteArray()[B

    move-result-object v6

    invoke-direct {v3, v9, v4, v5, v6}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v3, v2, v7

    goto/16 :goto_0
.end method

.method public static getNdefMessage([Lcom/samsung/mobileprint/nfclib/INFCRecord;Z)Landroid/nfc/NdefMessage;
    .locals 13
    .param p0, "recordArray"    # [Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .param p1, "includeAAR"    # Z

    .prologue
    const/4 v12, 0x4

    const/4 v6, 0x0

    .line 74
    array-length v7, p0

    if-eqz p1, :cond_1

    const/4 v5, 0x1

    :goto_0
    add-int/2addr v5, v7

    new-array v3, v5, [Landroid/nfc/NdefRecord;

    .line 75
    .local v3, "records":[Landroid/nfc/NdefRecord;
    const/4 v0, 0x0

    .line 76
    .local v0, "count":I
    array-length v7, p0

    move v5, v6

    :goto_1
    if-lt v5, v7, :cond_2

    .line 107
    if-eqz p1, :cond_0

    .line 108
    sget-object v5, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {v5}, Lcom/samsung/mobileprint/nfclib/RecordType;->getPackage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v5

    aput-object v5, v3, v0

    .line 110
    :cond_0
    new-instance v1, Landroid/nfc/NdefMessage;

    invoke-direct {v1, v3}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .line 113
    .local v1, "ndefMessage":Landroid/nfc/NdefMessage;
    return-object v1

    .end local v0    # "count":I
    .end local v1    # "ndefMessage":Landroid/nfc/NdefMessage;
    .end local v3    # "records":[Landroid/nfc/NdefRecord;
    :cond_1
    move v5, v6

    .line 74
    goto :goto_0

    .line 76
    .restart local v0    # "count":I
    .restart local v3    # "records":[Landroid/nfc/NdefRecord;
    :cond_2
    aget-object v4, p0, v5

    .line 79
    .local v4, "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    const-string v2, "com.sec.mobileprint.nfc:extType"

    .line 83
    .local v2, "recordType":Ljava/lang/String;
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    if-eqz v8, :cond_4

    .line 84
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 76
    :cond_3
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 85
    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_4
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    if-eqz v8, :cond_5

    .line 86
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 87
    goto :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_5
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    if-eqz v8, :cond_6

    .line 88
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 89
    goto :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_6
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    if-eqz v8, :cond_7

    .line 90
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 91
    goto :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_7
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    if-eqz v8, :cond_8

    .line 92
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 93
    goto :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_8
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    if-eqz v8, :cond_9

    .line 94
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 95
    goto/16 :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_9
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    if-eqz v8, :cond_a

    .line 96
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 97
    goto/16 :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_a
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    if-eqz v8, :cond_b

    .line 98
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 99
    goto/16 :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_b
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    if-eqz v8, :cond_c

    .line 100
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 101
    goto/16 :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_c
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    if-eqz v8, :cond_d

    .line 102
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 103
    goto/16 :goto_2

    .restart local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    :cond_d
    instance-of v8, v4, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    if-eqz v8, :cond_3

    .line 104
    new-instance v8, Landroid/nfc/NdefRecord;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    new-array v10, v6, [B

    check-cast v4, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    .end local v4    # "samNFCRecord":Lcom/samsung/mobileprint/nfclib/INFCRecord;
    invoke-virtual {v4}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->getNFCByteArray()[B

    move-result-object v11

    invoke-direct {v8, v12, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    aput-object v8, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method

.method private static lock(Landroid/nfc/Tag;)Z
    .locals 13
    .param p0, "tag"    # Landroid/nfc/Tag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, -0x70

    const/4 v11, 0x4

    const/4 v6, 0x0

    const/4 v10, 0x2

    const/4 v7, 0x1

    .line 261
    invoke-static {p0}, Landroid/nfc/tech/IsoDep;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/IsoDep;

    move-result-object v3

    .line 262
    .local v3, "nfc":Landroid/nfc/tech/IsoDep;
    if-nez v3, :cond_0

    .line 319
    :goto_0
    return v6

    .line 265
    :cond_0
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->connect()V

    .line 268
    const/16 v8, 0xd

    new-array v0, v8, [B

    const/16 v8, -0x5c

    aput-byte v8, v0, v7

    aput-byte v11, v0, v10

    const/4 v8, 0x7

    aput-byte v8, v0, v11

    const/4 v8, 0x5

    .line 269
    const/16 v9, -0x2e

    aput-byte v9, v0, v8

    const/4 v8, 0x6

    const/16 v9, 0x76

    aput-byte v9, v0, v8

    const/16 v8, 0x9

    const/16 v9, -0x7b

    aput-byte v9, v0, v8

    const/16 v8, 0xa

    aput-byte v7, v0, v8

    const/16 v8, 0xb

    aput-byte v7, v0, v8

    .line 271
    .local v0, "appli_select":[B
    invoke-virtual {v3, v0}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 273
    .local v4, "response":[B
    array-length v8, v4

    if-lt v8, v10, :cond_1

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_1

    .line 274
    aget-byte v8, v4, v7

    if-eqz v8, :cond_2

    .line 275
    :cond_1
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 276
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Can\'t select appli"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 280
    :cond_2
    const/4 v8, 0x7

    new-array v1, v8, [B

    const/16 v8, -0x5c

    aput-byte v8, v1, v7

    const/4 v8, 0x3

    const/16 v9, 0xc

    aput-byte v9, v1, v8

    aput-byte v10, v1, v11

    const/4 v8, 0x5

    .line 281
    const/16 v9, -0x1f

    aput-byte v9, v1, v8

    const/4 v8, 0x6

    const/4 v9, 0x3

    aput-byte v9, v1, v8

    .line 283
    .local v1, "cc_select":[B
    invoke-virtual {v3, v1}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 285
    array-length v8, v4

    if-lt v8, v10, :cond_3

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_3

    .line 286
    aget-byte v8, v4, v7

    if-eqz v8, :cond_4

    .line 287
    :cond_3
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 288
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Can\'t select CC_File"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 294
    :cond_4
    const/4 v8, 0x6

    new-array v5, v8, [B

    const/16 v8, -0x2a

    aput-byte v8, v5, v7

    const/4 v8, 0x3

    const/16 v9, 0xe

    aput-byte v9, v5, v8

    .line 295
    aput-byte v7, v5, v11

    const/4 v8, 0x5

    const/4 v9, -0x1

    aput-byte v9, v5, v8

    .line 297
    .local v5, "update_binary":[B
    invoke-virtual {v3, v5}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 299
    array-length v8, v4

    if-lt v8, v10, :cond_5

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_5

    .line 300
    aget-byte v8, v4, v7

    if-eqz v8, :cond_6

    .line 301
    :cond_5
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 302
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 308
    :cond_6
    const/16 v8, 0xa

    new-array v2, v8, [B

    aput-byte v12, v2, v6

    const/16 v8, 0x5f

    aput-byte v8, v2, v7

    .line 309
    aput-byte v11, v2, v11

    const/4 v8, 0x5

    aput-byte v10, v2, v8

    const/4 v8, 0x7

    const/4 v9, -0x2

    aput-byte v9, v2, v8

    const/16 v8, 0x8

    const/16 v9, -0x11

    aput-byte v9, v2, v8

    .line 311
    .local v2, "change_file_settings":[B
    invoke-virtual {v3, v2}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 313
    array-length v8, v4

    if-lt v8, v10, :cond_7

    aget-byte v6, v4, v6

    const/16 v8, -0x6f

    if-ne v6, v8, :cond_7

    .line 314
    aget-byte v6, v4, v7

    if-eqz v6, :cond_8

    .line 315
    :cond_7
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 316
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 318
    :cond_8
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    move v6, v7

    .line 319
    goto/16 :goto_0
.end method

.method private static unLock(Landroid/nfc/Tag;)Z
    .locals 13
    .param p0, "tag"    # Landroid/nfc/Tag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, -0x70

    const/4 v11, 0x4

    const/4 v6, 0x0

    const/4 v10, 0x2

    const/4 v7, 0x1

    .line 324
    const/4 v4, 0x0

    .line 326
    .local v4, "response":[B
    invoke-static {p0}, Landroid/nfc/tech/IsoDep;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/IsoDep;

    move-result-object v3

    .line 327
    .local v3, "nfc":Landroid/nfc/tech/IsoDep;
    if-nez v3, :cond_0

    .line 382
    :goto_0
    return v6

    .line 330
    :cond_0
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->connect()V

    .line 332
    const/16 v8, 0xd

    new-array v0, v8, [B

    const/16 v8, -0x5c

    aput-byte v8, v0, v7

    aput-byte v11, v0, v10

    const/4 v8, 0x7

    aput-byte v8, v0, v11

    const/4 v8, 0x5

    .line 333
    const/16 v9, -0x2e

    aput-byte v9, v0, v8

    const/4 v8, 0x6

    const/16 v9, 0x76

    aput-byte v9, v0, v8

    const/16 v8, 0x9

    const/16 v9, -0x7b

    aput-byte v9, v0, v8

    const/16 v8, 0xa

    aput-byte v7, v0, v8

    const/16 v8, 0xb

    aput-byte v7, v0, v8

    .line 335
    .local v0, "appli_select":[B
    invoke-virtual {v3, v0}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 336
    array-length v8, v4

    if-lt v8, v10, :cond_1

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_1

    .line 337
    aget-byte v8, v4, v7

    if-eqz v8, :cond_2

    .line 338
    :cond_1
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 339
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Can\'t select appli"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 343
    :cond_2
    const/4 v8, 0x7

    new-array v1, v8, [B

    const/16 v8, -0x5c

    aput-byte v8, v1, v7

    const/4 v8, 0x3

    const/16 v9, 0xc

    aput-byte v9, v1, v8

    aput-byte v10, v1, v11

    const/4 v8, 0x5

    .line 344
    const/16 v9, -0x1f

    aput-byte v9, v1, v8

    const/4 v8, 0x6

    const/4 v9, 0x3

    aput-byte v9, v1, v8

    .line 346
    .local v1, "cc_select":[B
    invoke-virtual {v3, v1}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 348
    array-length v8, v4

    if-lt v8, v10, :cond_3

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_3

    .line 349
    aget-byte v8, v4, v7

    if-eqz v8, :cond_4

    .line 350
    :cond_3
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 351
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Can\'t select CC_File"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 357
    :cond_4
    const/4 v8, 0x6

    new-array v5, v8, [B

    const/16 v8, -0x2a

    aput-byte v8, v5, v7

    const/4 v8, 0x3

    const/16 v9, 0xe

    aput-byte v9, v5, v8

    .line 358
    aput-byte v7, v5, v11

    .line 360
    .local v5, "update_binary":[B
    invoke-virtual {v3, v5}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 362
    array-length v8, v4

    if-lt v8, v10, :cond_5

    aget-byte v8, v4, v6

    if-ne v8, v12, :cond_5

    .line 363
    aget-byte v8, v4, v7

    if-eqz v8, :cond_6

    .line 364
    :cond_5
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 365
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 371
    :cond_6
    const/16 v8, 0xa

    new-array v2, v8, [B

    aput-byte v12, v2, v6

    const/16 v8, 0x5f

    aput-byte v8, v2, v7

    .line 372
    aput-byte v11, v2, v11

    const/4 v8, 0x5

    aput-byte v10, v2, v8

    const/4 v8, 0x7

    const/16 v9, -0x12

    aput-byte v9, v2, v8

    const/16 v8, 0x8

    const/16 v9, -0x12

    aput-byte v9, v2, v8

    .line 374
    .local v2, "change_file_settings":[B
    invoke-virtual {v3, v2}, Landroid/nfc/tech/IsoDep;->transceive([B)[B

    move-result-object v4

    .line 376
    array-length v8, v4

    if-lt v8, v10, :cond_7

    aget-byte v6, v4, v6

    const/16 v8, -0x6f

    if-ne v6, v8, :cond_7

    .line 377
    aget-byte v6, v4, v7

    if-eqz v6, :cond_8

    .line 378
    :cond_7
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    .line 379
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 381
    :cond_8
    invoke-virtual {v3}, Landroid/nfc/tech/IsoDep;->close()V

    move v6, v7

    .line 382
    goto/16 :goto_0
.end method

.method private static write(Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)I
    .locals 5
    .param p0, "tag"    # Landroid/nfc/Tag;
    .param p1, "ndefMessage"    # Landroid/nfc/NdefMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    .prologue
    .line 217
    if-nez p1, :cond_0

    .line 220
    sget v4, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_NULL:I

    .line 253
    :goto_0
    return v4

    .line 222
    :cond_0
    invoke-virtual {p1}, Landroid/nfc/NdefMessage;->toByteArray()[B

    move-result-object v4

    array-length v3, v4

    .line 225
    .local v3, "size":I
    :try_start_0
    invoke-static {p0}, Landroid/nfc/tech/Ndef;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/Ndef;

    move-result-object v2

    .line 226
    .local v2, "ndef":Landroid/nfc/tech/Ndef;
    if-eqz v2, :cond_2

    .line 227
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->connect()V

    .line 228
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->getMaxSize()I

    move-result v4

    if-ge v4, v3, :cond_1

    .line 229
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V

    .line 230
    sget v4, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_SIZE_TOO_BIG:I

    goto :goto_0

    .line 232
    :cond_1
    invoke-virtual {v2, p1}, Landroid/nfc/tech/Ndef;->writeNdefMessage(Landroid/nfc/NdefMessage;)V

    .line 233
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V

    .line 234
    sget v4, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

    goto :goto_0

    .line 237
    :cond_2
    invoke-static {p0}, Landroid/nfc/tech/NdefFormatable;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/NdefFormatable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 238
    .local v1, "format":Landroid/nfc/tech/NdefFormatable;
    if-eqz v1, :cond_3

    .line 240
    :try_start_1
    invoke-virtual {v1}, Landroid/nfc/tech/NdefFormatable;->connect()V

    .line 241
    invoke-virtual {v1, p1}, Landroid/nfc/tech/NdefFormatable;->format(Landroid/nfc/NdefMessage;)V

    .line 242
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V

    .line 243
    sget v4, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_TAG_FORMAT_SUCCESS:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Landroid/nfc/tech/Ndef;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 253
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "format":Landroid/nfc/tech/NdefFormatable;
    .end local v2    # "ndef":Landroid/nfc/tech/Ndef;
    :cond_3
    :goto_1
    sget v4, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I

    goto :goto_0

    .line 250
    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public static writeTag(Lcom/samsung/mobileprint/nfclib/INFCRecord;Landroid/nfc/Tag;Z)I
    .locals 7
    .param p0, "record"    # Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .param p1, "tag"    # Landroid/nfc/Tag;
    .param p2, "writeProtect"    # Z

    .prologue
    .line 169
    invoke-static {p0}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->getNdefMessage(Lcom/samsung/mobileprint/nfclib/INFCRecord;)Landroid/nfc/NdefMessage;

    move-result-object v4

    .line 171
    .local v4, "ndefMessage":Landroid/nfc/NdefMessage;
    const/4 v0, 0x0

    .local v0, "bLock":Z
    const/4 v2, 0x0

    .local v2, "bWrite":Z
    const/4 v1, 0x0

    .line 172
    .local v1, "bUnlock":Z
    if-eqz p1, :cond_3

    .line 174
    if-eqz p2, :cond_0

    .line 175
    :try_start_0
    invoke-static {p1}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->unLock(Landroid/nfc/Tag;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag unlocked successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_0
    :goto_0
    invoke-static {p1, v4}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->write(Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)I

    move-result v5

    sget v6, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

    if-ne v5, v6, :cond_4

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_5

    .line 183
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag Written successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_2
    if-eqz p2, :cond_1

    .line 189
    invoke-static {p1}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->lock(Landroid/nfc/Tag;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 190
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag Locked successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_1
    :goto_3
    if-eqz v2, :cond_7

    .line 197
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

    .line 210
    :goto_4
    return v5

    .line 179
    :cond_2
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag can not be unlocked as it does not support isoDep"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 201
    :catch_0
    move-exception v3

    .line 203
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 210
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    :goto_5
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I

    goto :goto_4

    .line 182
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 186
    :cond_5
    :try_start_1
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag writting Failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/nfc/FormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 205
    :catch_1
    move-exception v3

    .line 207
    .local v3, "e":Landroid/nfc/FormatException;
    invoke-virtual {v3}, Landroid/nfc/FormatException;->printStackTrace()V

    goto :goto_5

    .line 193
    .end local v3    # "e":Landroid/nfc/FormatException;
    :cond_6
    :try_start_2
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag can not be locked as it does not support isoDep"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 199
    :cond_7
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/nfc/FormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method

.method public static writeTag([Lcom/samsung/mobileprint/nfclib/INFCRecord;Landroid/nfc/Tag;ZZ)I
    .locals 7
    .param p0, "records"    # [Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .param p1, "tag"    # Landroid/nfc/Tag;
    .param p2, "writeProtect"    # Z
    .param p3, "includeAAR"    # Z

    .prologue
    .line 118
    invoke-static {p0, p3}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->getNdefMessage([Lcom/samsung/mobileprint/nfclib/INFCRecord;Z)Landroid/nfc/NdefMessage;

    move-result-object v4

    .line 119
    .local v4, "ndefMessage":Landroid/nfc/NdefMessage;
    const/4 v0, 0x0

    .local v0, "bLock":Z
    const/4 v2, 0x0

    .local v2, "bWrite":Z
    const/4 v1, 0x0

    .line 120
    .local v1, "bUnlock":Z
    if-eqz p1, :cond_3

    .line 122
    if-eqz p2, :cond_0

    .line 124
    :try_start_0
    invoke-static {p1}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->unLock(Landroid/nfc/Tag;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag unlocked successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 135
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {p1, v4}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->write(Landroid/nfc/Tag;Landroid/nfc/NdefMessage;)I

    move-result v5

    sget v6, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I

    if-ne v5, v6, :cond_4

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_5

    .line 136
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag Written successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :goto_2
    if-eqz p2, :cond_1

    .line 142
    invoke-static {p1}, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->lock(Landroid/nfc/Tag;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 143
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag Locked successfully"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_1
    :goto_3
    if-eqz v2, :cond_7

    .line 150
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_SUCCESS:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_1 .. :try_end_1} :catch_2

    .line 163
    :goto_4
    return v5

    .line 128
    :cond_2
    :try_start_2
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag can not be unlocked as it does not support isoDep"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 131
    :catch_0
    move-exception v3

    .line 132
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 154
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 156
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 163
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    :goto_5
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I

    goto :goto_4

    .line 135
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 139
    :cond_5
    :try_start_4
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag writting Failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 158
    :catch_2
    move-exception v3

    .line 160
    .local v3, "e":Landroid/nfc/FormatException;
    invoke-virtual {v3}, Landroid/nfc/FormatException;->printStackTrace()V

    goto :goto_5

    .line 146
    .end local v3    # "e":Landroid/nfc/FormatException;
    :cond_6
    :try_start_5
    const-string v5, "SMP_NFC_WRITER"

    const-string v6, "Tag can not be locked as it does not support isoDep"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 152
    :cond_7
    sget v5, Lcom/samsung/mobileprint/nfclib/NFCWriteTag;->ERR_NDEF_MESSAGE_WRITE_FAIL:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/nfc/FormatException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_4
.end method
