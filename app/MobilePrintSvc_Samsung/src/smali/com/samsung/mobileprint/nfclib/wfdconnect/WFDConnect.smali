.class public Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;
.super Landroid/content/BroadcastReceiver;
.source "WFDConnect.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    }
.end annotation


# static fields
.field public static final MSG_TYPE_ERROR:I = 0x1

.field public static final MSG_TYPE_PROGRESS:I = 0x2

.field public static final MSG_WIFIDIRECT_CONNECTED:I = 0x5

.field public static final MSG_WIFIDIRECT_CONNECTING:I = 0x3

.field public static final MSG_WIFIDIRECT_CONNECT_REQUEST_FAILED:I = 0xc

.field public static final MSG_WIFIDIRECT_DEVICEFOUND:I = 0x2

.field public static final MSG_WIFIDIRECT_DISCOVERYFAIL:I = 0x7

.field public static final MSG_WIFIDIRECT_DISCOVERYINITIATED:I = 0x1

.field public static final MSG_WIFIDIRECT_GOT_IPADDRESS:I = 0x6

.field public static final MSG_WIFIDIRECT_NOT_SUPPORTED:I = 0xb

.field public static final MSG_WIFIDIRECT_NO_DEVICE_FOUND:I = 0x8

.field public static final MSG_WIFIDIRECT_PIN_CONECTION_FAILED:I = 0xa

.field public static final MSG_WIFIDIRECT_PRESS_WPS:I = 0x4

.field public static final MSG_WIFIDIRECT_REMOVE_GROUP_FAILED:I = 0x9

.field public static final MSG_WIFIDIRECT_TURNED_OFF:I = 0xd


# instance fields
.field private bDeviceConnected:Z

.field private bDeviceFound:Z

.field private bNowConnected:Z

.field private bNowConnecting:Z

.field private channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private final intentFilter:Landroid/content/IntentFilter;

.field private mMessenger:Landroid/os/Messenger;

.field private m_Context:Landroid/content/Context;

.field private m_MacAddress:Ljava/lang/String;

.field private m_ModelName:Ljava/lang/String;

.field private m_Pin:Ljava/lang/String;

.field private m_WPSInfo:Landroid/net/wifi/WpsInfo;

.field private manager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private receiver:Landroid/content/BroadcastReceiver;

.field private wfdBundle:Landroid/os/Bundle;

.field private wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

.field private wfdIntent:Landroid/content/Intent;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;Landroid/os/Messenger;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "modelName"    # Ljava/lang/String;
    .param p3, "macAddress"    # Ljava/lang/String;
    .param p4, "pin"    # Ljava/lang/String;
    .param p5, "wfdConnectionStatusListener"    # Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    .param p6, "messenger"    # Landroid/os/Messenger;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 102
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 61
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->intentFilter:Landroid/content/IntentFilter;

    .line 62
    iput-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->receiver:Landroid/content/BroadcastReceiver;

    .line 71
    iput-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    .line 72
    iput-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnecting:Z

    .line 73
    iput-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnected:Z

    .line 74
    iput-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceConnected:Z

    .line 80
    iput-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "org.androidprinting.intent.ACTION_WIFI_DIRECT_CONNECTION_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdIntent:Landroid/content/Intent;

    .line 104
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_ModelName:Ljava/lang/String;

    .line 106
    iput-object p3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;

    .line 107
    iput-object p4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Pin:Ljava/lang/String;

    .line 108
    iput-object p5, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    .line 109
    iput-object p6, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    .line 111
    const-string v0, "WFDConnct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "modelName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string v0, "WFDConnct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "macAddress = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v0, "WFDConnct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    new-instance v0, Landroid/net/wifi/WpsInfo;

    invoke-direct {v0}, Landroid/net/wifi/WpsInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    .line 116
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    iput v3, v0, Landroid/net/wifi/WpsInfo;->setup:I

    .line 123
    :goto_0
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 126
    const-string v0, "wifip2p"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 127
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_3

    .line 128
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 129
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    :cond_1
    :goto_1
    return-void

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    const/4 v1, 0x2

    iput v1, v0, Landroid/net/wifi/WpsInfo;->setup:I

    .line 120
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Pin:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    goto :goto_0

    .line 132
    :cond_3
    if-eqz p5, :cond_4

    .line 133
    const/4 v0, 0x1

    const/16 v1, 0xb

    invoke-interface {p5, v0, v1, v4}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    goto :goto_1

    .line 134
    :cond_4
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    if-eqz v0, :cond_1

    .line 135
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    .line 136
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v1, "wifidirect_connection_status_key"

    const-string v2, "error_wifidirect_not_supported"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->stopPeerDiscovery()V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/net/wifi/WpsInfo;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 431
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->unregisterWFDRecevier()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceConnected:Z

    return v0
.end method

.method public static connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "modelName"    # Ljava/lang/String;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "pin"    # Ljava/lang/String;
    .param p4, "messenger"    # Landroid/os/Messenger;

    .prologue
    .line 97
    new-instance v0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;Landroid/os/Messenger;)V

    .line 99
    return-void
.end method

.method public static connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "modelName"    # Ljava/lang/String;
    .param p2, "macAddress"    # Ljava/lang/String;
    .param p3, "pin"    # Ljava/lang/String;
    .param p4, "wfdConnectionStatusListener"    # Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    .prologue
    .line 91
    new-instance v0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;Landroid/os/Messenger;)V

    .line 93
    return-void
.end method

.method private searchAndConnectWifiDirectDevice()V
    .locals 4

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->discoverWiFiDirectDevices()V

    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    .line 278
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;

    invoke-direct {v1, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$2;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    .line 298
    const-wide/16 v2, 0x7530

    .line 278
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 300
    return-void
.end method

.method private sendMessage()V
    .locals 5

    .prologue
    .line 443
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdIntent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 444
    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 446
    .local v1, "returnmsg":Landroid/os/Message;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :goto_0
    return-void

    .line 447
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopPeerDiscovery()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 333
    :try_start_0
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$4;

    invoke-direct {v3, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$4;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private unregisterWFDRecevier()V
    .locals 2

    .prologue
    .line 433
    :try_start_0
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 434
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    invoke-virtual {v1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public connectDevice()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 351
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-nez v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    const-string v2, "wifip2p"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 359
    :cond_1
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    if-ne v1, v5, :cond_2

    .line 361
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$5;

    invoke-direct {v2, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$5;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    .line 377
    const-wide/16 v3, 0x7530

    .line 361
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 382
    :cond_2
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 383
    .local v0, "config":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 384
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    iget v2, v2, Landroid/net/wifi/WpsInfo;->setup:I

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    .line 385
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_WPSInfo:Landroid/net/wifi/WpsInfo;

    iget v1, v1, Landroid/net/wifi/WpsInfo;->setup:I

    if-ne v1, v5, :cond_3

    .line 386
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_Pin:Ljava/lang/String;

    iput-object v2, v1, Landroid/net/wifi/WpsInfo;->pin:Ljava/lang/String;

    .line 388
    :cond_3
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v3, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;

    invoke-direct {v3, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$6;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 428
    return-void
.end method

.method public discoverWiFiDirectDevices()V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;

    invoke-direct {v2, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$3;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 328
    return-void
.end method

.method public onConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 4
    .param p1, "wifiP2pInfo"    # Landroid/net/wifi/p2p/WifiP2pInfo;

    .prologue
    .line 169
    iget-boolean v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    if-eqz v1, :cond_0

    .line 170
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceConnected:Z

    .line 171
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "ipAddress":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    const/4 v2, 0x2

    const/4 v3, 0x6

    invoke-interface {v1, v2, v3, v0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 183
    .end local v0    # "ipAddress":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->unregisterWFDRecevier()V

    .line 184
    return-void

    .line 174
    .restart local v0    # "ipAddress":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 175
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    .line 176
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v2, "wifidirect_connection_status_key"

    const-string v3, "got_ip_address_of_wifidirect_device"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v2, "printer-address"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v2, "printer-make-model"

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_ModelName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    goto :goto_0
.end method

.method public onGroupInfoAvailable(Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 3
    .param p1, "wifiP2pGroup"    # Landroid/net/wifi/p2p/WifiP2pGroup;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pGroup;->getOwner()Landroid/net/wifi/p2p/WifiP2pDevice;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestConnectionInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;)V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    .line 163
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v2, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$1;

    invoke-direct {v2, p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$1;-><init>(Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnected:Z

    goto :goto_0
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 8
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 191
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .local v2, "peersList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 194
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    iget-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnecting:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnected:Z

    if-nez v3, :cond_0

    .line 196
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 221
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 197
    .restart local v0    # "i":I
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 199
    .local v1, "peerItem":Landroid/net/wifi/p2p/WifiP2pDevice;
    iget-object v3, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->m_MacAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 200
    iput-boolean v6, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bDeviceFound:Z

    .line 201
    iget-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnecting:Z

    if-nez v3, :cond_3

    .line 202
    iput-boolean v6, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnecting:Z

    .line 203
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    if-eqz v3, :cond_4

    .line 204
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    const/4 v4, 0x0

    invoke-interface {v3, v7, v7, v4}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 210
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connectDevice()V

    .line 212
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->stopPeerDiscovery()V

    .line 196
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_4
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_2

    .line 206
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    .line 207
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v4, "wifidirect_connection_status_key"

    const-string v5, "wifi_direct_device_found"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    goto :goto_1
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 226
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 229
    const-string v3, "wifi_p2p_state"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 230
    .local v2, "state":I
    if-ne v2, v5, :cond_0

    .line 231
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    if-eqz v3, :cond_1

    .line 232
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    const/16 v4, 0xd

    invoke-interface {v3, v5, v4, v6}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    .line 270
    .end local v2    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 233
    .restart local v2    # "state":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 234
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    .line 235
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v4, "wifidirect_connection_status_key"

    const-string v5, "error_wifidirect_is_turned_off"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    goto :goto_0

    .line 242
    .end local v2    # "state":I
    :cond_2
    const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 245
    const-string v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 246
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 248
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnecting:Z

    .line 249
    iput-boolean v5, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->bNowConnected:Z

    .line 250
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v3, v4, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestGroupInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$GroupInfoListener;)V

    .line 251
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    if-eqz v3, :cond_3

    .line 252
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdConnectionStatusListener:Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    const/4 v4, 0x2

    const/4 v5, 0x5

    invoke-interface {v3, v4, v5, v6}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;->onWFDConnectionStatusUpdate(IILjava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_3
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->mMessenger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 254
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    .line 255
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->wfdBundle:Landroid/os/Bundle;

    const-string v4, "wifidirect_connection_status_key"

    const-string v5, "connected_wifi_direct_device"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->sendMessage()V

    goto :goto_0

    .line 261
    :cond_4
    invoke-direct {p0}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->searchAndConnectWifiDirectDevice()V

    goto :goto_0

    .line 264
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_5
    const-string v3, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 266
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v3, :cond_0

    .line 267
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-virtual {v3, v4, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    goto :goto_0
.end method
