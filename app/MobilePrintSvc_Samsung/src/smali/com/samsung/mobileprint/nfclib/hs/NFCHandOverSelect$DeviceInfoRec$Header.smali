.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Header"
.end annotation


# instance fields
.field m_Flag:[B

.field m_Id:[B

.field m_IdLength:[B

.field m_Payload_Length:[B

.field m_Type:[B

.field m_TypeLength:[B

.field final synthetic this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1003
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->this$1:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004
    new-array v0, v1, [B

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Flag:[B

    .line 1005
    new-array v0, v1, [B

    aput-byte v3, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_TypeLength:[B

    .line 1006
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Payload_Length:[B

    .line 1007
    new-array v0, v1, [B

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_IdLength:[B

    .line 1008
    new-array v0, v3, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Type:[B

    .line 1009
    new-array v0, v1, [B

    const/16 v1, 0x43

    aput-byte v1, v0, v2

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Id:[B

    return-void

    .line 1008
    :array_0
    .array-data 1
        0x44t
        0x69t
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;)V
    .locals 0

    .prologue
    .line 1003
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 1026
    const/4 v0, 0x0

    return v0
.end method

.method getByteData(I)[B
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 1012
    new-array v1, p1, [B

    .line 1013
    .local v1, "data":[B
    const/4 v0, 0x0

    .line 1014
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Flag:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Flag:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Flag:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1015
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_TypeLength:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_TypeLength:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_TypeLength:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1016
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Payload_Length:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Payload_Length:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Payload_Length:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1017
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_IdLength:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_IdLength:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_IdLength:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1018
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Type:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Type:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Type:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1019
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Id:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Id:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Id:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 1020
    return-object v1
.end method
