.class public Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
.super Ljava/lang/Object;
.source "NFCPrinterStatusRecord.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# static fields
.field private static m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;


# instance fields
.field private m_2FactorAuth:[B

.field private m_PrinterStatus:[B

.field private m_SamsungHeader:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    .line 21
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    .line 22
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    .line 27
    return-void
.end method

.method public static createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    .locals 1
    .param p0, "header"    # [B

    .prologue
    .line 31
    new-instance v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    .line 32
    sget-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->setSamsungHeader([B)V

    .line 33
    sget-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 38
    new-instance v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    .line 39
    sget-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->byteStreamToClass([B)Z

    .line 40
    sget-object v0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_NFCPrinterStatus:Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 7
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 70
    const/4 v0, 0x0

    .line 73
    .local v0, "count":I
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    array-length v6, v6

    invoke-static {p1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 75
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 76
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v2

    .line 81
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public get2FactorAuth()[B
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 48
    .local v1, "nfcData":[B
    const/4 v0, 0x0

    .line 49
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 50
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 51
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 52
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    array-length v1, v1

    .line 60
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    array-length v2, v2

    .line 59
    add-int/2addr v1, v2

    .line 61
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    array-length v2, v2

    .line 59
    add-int v0, v1, v2

    .line 63
    int-to-long v1, v0

    return-wide v1
.end method

.method public getPrinterStatus()[B
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    return-object v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSamsungHeader()[B
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    return-object v0
.end method

.method public set2FactorAuth([B)V
    .locals 0
    .param p1, "m_2FactorAuth"    # [B

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    .line 122
    return-void
.end method

.method public setPrinterStatus([B)V
    .locals 0
    .param p1, "m_PrinterStatus"    # [B

    .prologue
    .line 113
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    .line 114
    return-void
.end method

.method public setSamsungHeader([B)V
    .locals 0
    .param p1, "m_SamsungHeader"    # [B

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    .line 106
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NFCPrinterStatusRecord [m_SamsungHeader="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 13
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_SamsungHeader:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_PrinterStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_PrinterStatus:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", m_2FactorAuth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->m_2FactorAuth:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
