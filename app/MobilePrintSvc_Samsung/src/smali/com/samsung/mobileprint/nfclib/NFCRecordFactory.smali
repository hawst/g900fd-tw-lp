.class public Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;
.super Ljava/lang/Object;
.source "NFCRecordFactory.java"


# static fields
.field public static final RTD_ANDROID_APP:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "android.com:pkg"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->RTD_ANDROID_APP:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildNFCRecordFromByteArray(Landroid/nfc/NdefRecord;)Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .locals 20
    .param p0, "record"    # Landroid/nfc/NdefRecord;

    .prologue
    .line 80
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v18

    sget-object v19, Landroid/nfc/NdefRecord;->RTD_URI:[B

    invoke-static/range {v18 .. v19}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 82
    new-instance v3, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppURLRecord;

    invoke-direct {v3}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppURLRecord;-><init>()V

    .line 83
    .local v3, "SamsungAppURLRecord":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppURLRecord;
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppURLRecord;->setSamsungAppUri([B)V

    .line 156
    .end local v3    # "SamsungAppURLRecord":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppURLRecord;
    :goto_0
    return-object v3

    .line 85
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v18

    sget-object v19, Lcom/samsung/mobileprint/nfclib/NFCRecordFactory;->RTD_ANDROID_APP:[B

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 87
    new-instance v2, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;

    invoke-direct {v2}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;-><init>()V

    .line 88
    .local v2, "SamsungAppRecord":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;->setPackageName([B)V

    move-object v3, v2

    .line 89
    goto :goto_0

    .line 92
    .end local v2    # "SamsungAppRecord":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCSamsungAppRecord;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->CreateSamsungHeader([BZ)Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    move-result-object v11

    .line 93
    .local v11, "nfcHeader":Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    invoke-virtual {v11}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getRecordType()Lcom/samsung/mobileprint/nfclib/RecordType;

    move-result-object v16

    .line 94
    .local v16, "type":Lcom/samsung/mobileprint/nfclib/RecordType;
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-virtual {v11}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    .line 96
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    move-result-object v8

    .local v8, "mpConnect1Record":Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;
    move-object v3, v8

    .line 97
    goto :goto_0

    .line 99
    .end local v8    # "mpConnect1Record":Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;
    :cond_2
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-virtual {v11}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 101
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    move-result-object v9

    .local v9, "mpConnect2Record":Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    move-object v3, v9

    .line 102
    goto :goto_0

    .line 104
    .end local v9    # "mpConnect2Record":Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    :cond_3
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-virtual {v11}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v18

    const/16 v19, 0x3

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 106
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    move-result-object v10

    .local v10, "mpConnect3Record":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    move-object v3, v10

    .line 107
    goto/16 :goto_0

    .line 109
    .end local v10    # "mpConnect3Record":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    :cond_4
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 111
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    move-result-object v4

    .local v4, "authRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    move-object v3, v4

    .line 112
    goto/16 :goto_0

    .line 114
    .end local v4    # "authRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    :cond_5
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 116
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    move-result-object v5

    .local v5, "authRegRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    move-object v3, v5

    .line 117
    goto/16 :goto_0

    .line 119
    .end local v5    # "authRegRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    :cond_6
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 121
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    move-result-object v17

    .local v17, "wifiSetupRecord":Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    move-object/from16 v3, v17

    .line 122
    goto/16 :goto_0

    .line 124
    .end local v17    # "wifiSetupRecord":Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    :cond_7
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 126
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    move-result-object v15

    .local v15, "troubleShootingRecord":Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    move-object v3, v15

    .line 127
    goto/16 :goto_0

    .line 129
    .end local v15    # "troubleShootingRecord":Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    :cond_8
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 131
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    move-result-object v6

    .local v6, "deviceSettingRecord":Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    move-object v3, v6

    .line 132
    goto/16 :goto_0

    .line 134
    .end local v6    # "deviceSettingRecord":Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    :cond_9
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 136
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    move-result-object v14

    .local v14, "securityKeyRecord":Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    move-object v3, v14

    .line 137
    goto/16 :goto_0

    .line 139
    .end local v14    # "securityKeyRecord":Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    :cond_a
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 141
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    move-result-object v13

    .local v13, "publicKeyRecord":Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    move-object v3, v13

    .line 142
    goto/16 :goto_0

    .line 144
    .end local v13    # "publicKeyRecord":Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    :cond_b
    sget-object v18, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 146
    invoke-virtual/range {p0 .. p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .local v12, "printerStatusRecord":Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    move-object v3, v12

    .line 147
    goto/16 :goto_0

    .line 152
    .end local v11    # "nfcHeader":Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    .end local v12    # "printerStatusRecord":Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    .end local v16    # "type":Lcom/samsung/mobileprint/nfclib/RecordType;
    :catch_0
    move-exception v7

    .line 153
    .local v7, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 156
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v11    # "nfcHeader":Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    .restart local v16    # "type":Lcom/samsung/mobileprint/nfclib/RecordType;
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public static buildNewNFCRecord(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/INFCRecord;
    .locals 15
    .param p0, "type"    # Lcom/samsung/mobileprint/nfclib/RecordType;
    .param p1, "majorVersion"    # B
    .param p2, "minorVersion"    # B

    .prologue
    .line 161
    invoke-static/range {p0 .. p2}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->CreateSamsungHeader(Lcom/samsung/mobileprint/nfclib/RecordType;BB)Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;

    move-result-object v8

    .line 162
    .local v8, "nfcHeader":Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->HANDOVER_SELECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 164
    invoke-static {}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->createNewNFCMessage()Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    move-result-object v3

    .line 218
    :goto_0
    return-object v3

    .line 167
    :cond_0
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    .line 169
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;

    move-result-object v5

    .local v5, "mpConnect1Record":Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;
    move-object v3, v5

    .line 170
    goto :goto_0

    .line 172
    .end local v5    # "mpConnect1Record":Lcom/samsung/mobileprint/nfclib/mp_ver1/NFCMPConnectV1;
    :cond_1
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_2

    .line 174
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;

    move-result-object v6

    .local v6, "mpConnect2Record":Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    move-object v3, v6

    .line 175
    goto :goto_0

    .line 177
    .end local v6    # "mpConnect2Record":Lcom/samsung/mobileprint/nfclib/mp_ver2/NFCMPConnectV2;
    :cond_2
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_PRINT_CONNECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getMinorVersion()B

    move-result v13

    const/4 v14, 0x3

    if-ne v13, v14, :cond_3

    .line 179
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;

    move-result-object v7

    .local v7, "mpConnect3Record":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    move-object v3, v7

    .line 180
    goto :goto_0

    .line 182
    .end local v7    # "mpConnect3Record":Lcom/samsung/mobileprint/nfclib/mp_ver3/NFCMPConnectV3;
    :cond_3
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 184
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;

    move-result-object v0

    .local v0, "authRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    move-object v3, v0

    .line 185
    goto :goto_0

    .line 187
    .end local v0    # "authRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthentication;
    :cond_4
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->AUTHENTICATION_REGISTRATION:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 189
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;

    move-result-object v1

    .local v1, "authRegRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    move-object v3, v1

    .line 190
    goto :goto_0

    .line 192
    .end local v1    # "authRegRecord":Lcom/samsung/mobileprint/nfclib/authentication/NFCAuthenticationRegistration;
    :cond_5
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->WIRELESS_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 193
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;

    move-result-object v12

    .local v12, "wifiSetupRecord":Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    move-object v3, v12

    .line 194
    goto/16 :goto_0

    .line 196
    .end local v12    # "wifiSetupRecord":Lcom/samsung/mobileprint/nfclib/wifi_setup/NFCWiFiSetup;
    :cond_6
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->TROUBLESHOOTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 197
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;

    move-result-object v11

    .local v11, "troubleShootingRecord":Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    move-object v3, v11

    .line 198
    goto/16 :goto_0

    .line 200
    .end local v11    # "troubleShootingRecord":Lcom/samsung/mobileprint/nfclib/troubleshooting/NFCTroubleShooting;
    :cond_7
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->DEVICE_SETTING:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 201
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;

    move-result-object v2

    .local v2, "deviceSettingRecord":Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    move-object v3, v2

    .line 202
    goto/16 :goto_0

    .line 204
    .end local v2    # "deviceSettingRecord":Lcom/samsung/mobileprint/nfclib/device_setting/NFCDeviceSettingRecord;
    :cond_8
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->MOBILE_SECURITY_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 205
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;

    move-result-object v4

    .local v4, "mobileSecurityKeyRecord":Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    move-object v3, v4

    .line 206
    goto/16 :goto_0

    .line 208
    .end local v4    # "mobileSecurityKeyRecord":Lcom/samsung/mobileprint/nfclib/security_key/NFCMobileSecurityKeyRecord;
    :cond_9
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_STATUS:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 209
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;

    move-result-object v9

    .local v9, "printerStatusRecord":Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    move-object v3, v9

    .line 210
    goto/16 :goto_0

    .line 212
    .end local v9    # "printerStatusRecord":Lcom/samsung/mobileprint/nfclib/status/NFCPrinterStatusRecord;
    :cond_a
    sget-object v13, Lcom/samsung/mobileprint/nfclib/RecordType;->PRINTER_PUBLIC_KEY:Lcom/samsung/mobileprint/nfclib/RecordType;

    invoke-virtual {p0, v13}, Lcom/samsung/mobileprint/nfclib/RecordType;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 213
    invoke-virtual {v8}, Lcom/samsung/mobileprint/nfclib/SamsungNFCHeader;->getSamsungHeaderByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;->createNewNFCRecord([B)Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;

    move-result-object v10

    .local v10, "publicKeyRecord":Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    move-object v3, v10

    .line 214
    goto/16 :goto_0

    .line 218
    .end local v10    # "publicKeyRecord":Lcom/samsung/mobileprint/nfclib/public_key/NFCPublicKeyRecord;
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
