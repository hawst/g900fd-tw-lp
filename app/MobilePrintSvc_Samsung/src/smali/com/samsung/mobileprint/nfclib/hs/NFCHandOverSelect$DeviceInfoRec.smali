.class Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceInfoRec"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;
    }
.end annotation


# instance fields
.field m_Header:[B

.field m_ModelName:[B

.field m_ModelNameLength:[B

.field m_ModelNameValue:[B

.field m_NoOfDeviceInfoTLV:[B

.field m_VerdorName:[B

.field m_VerdorNameLength:[B

.field m_VerdorNameValue:[B

.field m_VersionNumber:[B

.field final synthetic this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# direct methods
.method private constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1002
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1031
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    .line 1032
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    .line 1033
    new-array v0, v2, [B

    aput-byte v4, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    .line 1034
    new-array v0, v2, [B

    aput-byte v2, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    .line 1035
    new-array v0, v2, [B

    const/4 v1, 0x7

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    .line 1036
    new-instance v0, Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    .line 1037
    new-array v0, v2, [B

    aput-byte v4, v0, v3

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    .line 1038
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;)V
    .locals 0

    .prologue
    .line 1002
    invoke-direct {p0, p1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)V

    return-void
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 6
    .param p1, "data"    # [B

    .prologue
    const/4 v2, 0x0

    .line 1091
    const/4 v0, 0x0

    .line 1092
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1093
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1094
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1095
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1096
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1097
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1098
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1099
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1100
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    .line 1101
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v5, v5

    invoke-static {p1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v0

    .line 1106
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 1102
    :catch_0
    move-exception v1

    .line 1103
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getByteArray()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1046
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    .line 1047
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->this$0:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    # getter for: Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B
    invoke-static {v5}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1048
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 1051
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->getByteArraySize()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v2, v3, [B

    .line 1054
    .local v2, "nfcData":[B
    new-instance v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;)V

    .line 1055
    .local v1, "header":Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;
    iget-object v3, v1, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->m_Payload_Length:[B

    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->getByteArraySize()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v6, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v8

    .line 1056
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec$Header;->getByteData(I)[B

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    .line 1059
    const/4 v0, 0x0

    .line 1061
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1062
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1063
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1064
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1065
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1066
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1067
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1068
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1069
    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v4, v4

    invoke-static {v3, v8, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v3, v3

    add-int/2addr v0, v3

    .line 1070
    return-object v2
.end method

.method public getByteArraySize()J
    .locals 3

    .prologue
    .line 1076
    const/4 v0, 0x0

    .line 1077
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_Header:[B

    array-length v1, v1

    .line 1078
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VersionNumber:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1079
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_NoOfDeviceInfoTLV:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1080
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorName:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1081
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameLength:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1082
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_VerdorNameValue:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1083
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelName:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v1, v2

    .line 1084
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameLength:[B

    array-length v2, v2

    .line 1077
    add-int/2addr v2, v1

    .line 1085
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->m_ModelNameValue:[B

    array-length v1, v1

    .line 1077
    :goto_0
    add-int v0, v2, v1

    .line 1086
    int-to-long v1, v0

    return-wide v1

    .line 1085
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
