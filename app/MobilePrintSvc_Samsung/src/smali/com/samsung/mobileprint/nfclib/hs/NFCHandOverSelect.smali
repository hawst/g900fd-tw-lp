.class public Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
.super Ljava/lang/Object;
.source "NFCHandOverSelect.java"

# interfaces
.implements Lcom/samsung/mobileprint/nfclib/INFCRecord;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;,
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;,
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;,
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;,
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;,
        Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;
    }
.end annotation


# static fields
.field private static m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;


# instance fields
.field private carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

.field private carrierConfigRecByteArray:[B

.field private currNWRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;

.field private currNWRecByteArray:[B

.field private deviceInfoRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

.field private deviceInfoRecByteArray:[B

.field private hsRecByteArray:[B

.field private hsRecord:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

.field private m_GCOUNTRY:[B

.field private m_GChannelList:[B

.field private m_GModelName:[B

.field private m_GPublicKey:[B

.field private m_GPwd:[B

.field private m_GPwdID:[B

.field private m_GSSID:[B

.field private m_GSerialNo:[B

.field private m_GUUID:[B

.field private m_GWFDMacAddress:[B

.field private m_SamsungHeader:[B

.field private nwIdentityRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;

.field private nwIdentityRecByteArray:[B

.field private verbRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;

.field private verbRecByteArray:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_SamsungHeader:[B

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    .line 21
    const/16 v0, 0x14

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B

    .line 23
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    .line 25
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B

    .line 52
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecord:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

    .line 53
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    .line 54
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;

    .line 55
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;

    .line 56
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;

    .line 57
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

    invoke-direct {v0, p0, v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;-><init>(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;)V

    iput-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

    .line 58
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;)[B
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B

    return-object v0
.end method

.method public static createNewNFCMessage()Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    .line 63
    sget-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    return-object v0
.end method

.method public static createRecordFromByteArray([B)Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;
    .locals 1
    .param p0, "data"    # [B

    .prologue
    .line 68
    new-instance v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-direct {v0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;-><init>()V

    sput-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    .line 69
    sget-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    invoke-virtual {v0, p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->byteStreamToClass([B)Z

    .line 70
    sget-object v0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_NFCHandOverSelect:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;

    return-object v0
.end method


# virtual methods
.method public byteStreamToClass([B)Z
    .locals 8
    .param p1, "nfcData"    # [B

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v7, 0x1

    .line 111
    :try_start_0
    new-instance v1, Landroid/nfc/NdefMessage;

    invoke-direct {v1, p1}, Landroid/nfc/NdefMessage;-><init>([B)V

    .line 112
    .local v1, "message":Landroid/nfc/NdefMessage;
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    .line 113
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecord:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->byteStreamToClass([B)Z

    .line 114
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    array-length v2, v2

    if-le v2, v7, :cond_0

    .line 115
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    .line 116
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->byteStreamToClass([B)Z

    .line 118
    :cond_0
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    array-length v2, v2

    if-le v2, v4, :cond_1

    .line 119
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    .line 120
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->byteStreamToClass([B)Z

    .line 122
    :cond_1
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    array-length v2, v2

    if-le v2, v5, :cond_2

    .line 123
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    .line 124
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->byteStreamToClass([B)Z

    .line 126
    :cond_2
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    array-length v2, v2

    if-le v2, v6, :cond_3

    .line 127
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    .line 128
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->byteStreamToClass([B)Z

    .line 130
    :cond_3
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    array-length v2, v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_4

    .line 131
    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/nfc/NdefRecord;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    .line 132
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    invoke-virtual {v2, v3}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->byteStreamToClass([B)Z

    .line 134
    :cond_4
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_P2PDeviceAddress:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v2, v2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    .line 136
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ModelNameValue:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 137
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_PublicKeyHash:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePassword:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 139
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_DevicePasswordID:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v2, v2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B

    .line 141
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SerialNumberValue:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_UUIDValue:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_OOB_GroupOwnerCountryString:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v2, v2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B

    .line 145
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_ChannelList:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v2, v2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B

    .line 147
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v2, v2, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    iget-object v6, v6, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->m_SSID:[B

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    .end local v1    # "message":Landroid/nfc/NdefMessage;
    :goto_0
    return v7

    .line 149
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Landroid/nfc/FormatException;
    invoke-virtual {v0}, Landroid/nfc/FormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public connectWFD(Landroid/content/Context;Landroid/os/Messenger;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "messenger"    # Landroid/os/Messenger;

    .prologue
    .line 184
    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->ByteArrayToMacAddress([B)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 186
    .local v2, "modelName":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/codec/binary/Hex;->decodeHex([C)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 187
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public connectWFD(Landroid/content/Context;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wfdConnectionListener"    # Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;

    .prologue
    .line 172
    :try_start_0
    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    invoke-static {v4}, Lcom/samsung/mobileprint/nfclib/utils/NFCUtils;->ByteArrayToMacAddress([B)Ljava/lang/String;

    move-result-object v1

    .line 173
    .local v1, "macAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([B)V

    .line 174
    .local v2, "modelName":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-static {v4}, Lorg/apache/commons/codec/binary/Hex;->decodeHex([C)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    .line 175
    .local v3, "pin":Ljava/lang/String;
    invoke-static {p1, v2, v1, v3, p2}, Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect;->connect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/mobileprint/nfclib/wfdconnect/WFDConnect$IWFDConnectionStatusListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    .end local v1    # "macAddress":Ljava/lang/String;
    .end local v2    # "modelName":Ljava/lang/String;
    .end local v3    # "pin":Ljava/lang/String;
    :goto_0
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getCOUNTRY()[B
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B

    return-object v0
.end method

.method public getChannelList()[B
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B

    return-object v0
.end method

.method public getModelName()[B
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    return-object v0
.end method

.method public getNFCByteArray()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->getNFCByteArraySize()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v1, v2, [B

    .line 78
    .local v1, "data":[B
    const/4 v0, 0x0

    .line 79
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 80
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 81
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 82
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 83
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 84
    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    iget-object v3, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    array-length v2, v2

    add-int/2addr v0, v2

    .line 86
    return-object v1
.end method

.method public getNFCByteArraySize()J
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecord:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$HSRecord;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    .line 95
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$WiFiP2PCarrierConfigRecord;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    .line 96
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$CurrentNWRecord;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    .line 97
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$NetworkIdentityRecord;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    .line 98
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$VerbRecord;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    .line 99
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRec:Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;

    invoke-virtual {v1}, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect$DeviceInfoRec;->getByteArray()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    .line 101
    iget-object v1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->hsRecByteArray:[B

    array-length v1, v1

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->carrierConfigRecByteArray:[B

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->currNWRecByteArray:[B

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->nwIdentityRecByteArray:[B

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->verbRecByteArray:[B

    array-length v2, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->deviceInfoRecByteArray:[B

    array-length v2, v2

    add-int v0, v1, v2

    .line 103
    int-to-long v1, v0

    return-wide v1
.end method

.method public getPublicKey()[B
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B

    return-object v0
.end method

.method public getPwd()[B
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    return-object v0
.end method

.method public getPwdID()[B
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B

    return-object v0
.end method

.method public getRecordtype()Lcom/samsung/mobileprint/nfclib/RecordType;
    .locals 1

    .prologue
    .line 1114
    sget-object v0, Lcom/samsung/mobileprint/nfclib/RecordType;->HANDOVER_SELECT:Lcom/samsung/mobileprint/nfclib/RecordType;

    return-object v0
.end method

.method public getSSID()[B
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B

    return-object v0
.end method

.method public getSerialNo()[B
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B

    return-object v0
.end method

.method public getUUID()[B
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B

    return-object v0
.end method

.method public getWFDMacAddress()[B
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    return-object v0
.end method

.method public setCOUNTRY([B)V
    .locals 0
    .param p1, "m_GCOUNTRY"    # [B

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GCOUNTRY:[B

    .line 257
    return-void
.end method

.method public setChannelList([B)V
    .locals 0
    .param p1, "m_GChannelList"    # [B

    .prologue
    .line 264
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GChannelList:[B

    .line 265
    return-void
.end method

.method public setModelName([B)V
    .locals 0
    .param p1, "m_GModelName"    # [B

    .prologue
    .line 200
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GModelName:[B

    .line 201
    return-void
.end method

.method public setPublicKey([B)V
    .locals 0
    .param p1, "m_GPublicKey"    # [B

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPublicKey:[B

    .line 217
    return-void
.end method

.method public setPwd([B)V
    .locals 0
    .param p1, "m_GPwd"    # [B

    .prologue
    .line 232
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwd:[B

    .line 233
    return-void
.end method

.method public setPwdID([B)V
    .locals 0
    .param p1, "m_GPwdID"    # [B

    .prologue
    .line 224
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GPwdID:[B

    .line 225
    return-void
.end method

.method public setSSID([B)V
    .locals 0
    .param p1, "m_GSSID"    # [B

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSSID:[B

    .line 273
    return-void
.end method

.method public setSerialNo([B)V
    .locals 0
    .param p1, "m_GSerialNo"    # [B

    .prologue
    .line 240
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GSerialNo:[B

    .line 241
    return-void
.end method

.method public setUUID([B)V
    .locals 0
    .param p1, "m_GUUID"    # [B

    .prologue
    .line 248
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GUUID:[B

    .line 249
    return-void
.end method

.method public setWFDMacAddress([B)V
    .locals 0
    .param p1, "m_GWFDMacAddress"    # [B

    .prologue
    .line 208
    iput-object p1, p0, Lcom/samsung/mobileprint/nfclib/hs/NFCHandOverSelect;->m_GWFDMacAddress:[B

    .line 209
    return-void
.end method
