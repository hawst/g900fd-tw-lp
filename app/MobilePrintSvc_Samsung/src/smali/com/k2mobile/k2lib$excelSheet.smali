.class public Lcom/k2mobile/k2lib$excelSheet;
.super Ljava/lang/Object;
.source "k2lib.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/k2mobile/k2lib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "excelSheet"
.end annotation


# instance fields
.field private mName:Ljava/lang/String;

.field private mSheet_Status:I

.field private mSheet_Type:I

.field final synthetic this$0:Lcom/k2mobile/k2lib;


# direct methods
.method private constructor <init>(Lcom/k2mobile/k2lib;Ljava/lang/String;I)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "flag"    # I

    .prologue
    .line 108
    iput-object p1, p0, Lcom/k2mobile/k2lib$excelSheet;->this$0:Lcom/k2mobile/k2lib;

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/k2mobile/k2lib$excelSheet;->mName:Ljava/lang/String;

    .line 110
    shr-int/lit8 v0, p3, 0x8

    iput v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mSheet_Type:I

    .line 111
    and-int/lit16 v0, p3, 0xff

    iput v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mSheet_Status:I

    .line 112
    return-void
.end method

.method synthetic constructor <init>(Lcom/k2mobile/k2lib;Ljava/lang/String;ILcom/k2mobile/k2lib$excelSheet;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3}, Lcom/k2mobile/k2lib$excelSheet;-><init>(Lcom/k2mobile/k2lib;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method isVisible()Z
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mSheet_Status:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mName:Ljava/lang/String;

    return-object v0
.end method

.method status()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mSheet_Status:I

    return v0
.end method

.method type()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/k2mobile/k2lib$excelSheet;->mSheet_Type:I

    return v0
.end method
