.class public final Lcom/k2mobile/k2types;
.super Ljava/lang/Object;
.source "k2types.java"


# static fields
.field public static final COLOR_TYPE_ABGR32:I = 0xc

.field public static final COLOR_TYPE_BGRA32:I = 0xd

.field public static final COLOR_TYPE_BGRO32:I = 0xf

.field public static final COLOR_TYPE_GRAYO:I = 0x10

.field public static final COLOR_TYPE_RGB16:I = 0x3

.field public static final COLOR_TYPE_RGB24:I = 0x9

.field public static final ERR_ALIGN_FAIL:I = 0x8000

.field public static final ERR_BLOCKING:I = 0x2

.field public static final ERR_FILECREATE:I = 0x4000

.field public static final ERR_FILEOPEN:I = 0x20

.field public static final ERR_FILE_BROKEN:I = 0x100

.field public static final ERR_FILE_ENCRYPTED:I = 0x400

.field public static final ERR_FIND_DONE:I = 0x40000000

.field public static final ERR_FIND_FAIL:I = 0x10000000

.field public static final ERR_FIND_MASK:I = -0x10000000

.field public static final ERR_FIND_OK:I = 0x20000000

.field public static final ERR_FONTFILE:I = 0x1000

.field public static final ERR_FONTLIB:I = 0x800

.field public static final ERR_GENERIC:I = 0x1

.field public static final ERR_IDLE_COLLISION:I = 0x10000

.field public static final ERR_IMAGE_CANCELED:I = 0x400000

.field public static final ERR_IMAGE_DRAWING_FAIL:I = 0x10

.field public static final ERR_IMAGE_EMBED_FAIL:I = 0x1

.field public static final ERR_IMAGE_LOW_MEMORY:I = 0x2

.field public static final ERR_IMAGE_NOT_SUPPORT:I = 0x4

.field public static final ERR_IMAGE_OLE_FAIL:I = 0x8

.field public static final ERR_IMAGE_PDF_PAGE_FAIL:I = 0x20

.field public static final ERR_INVALID_ARG:I = 0x10

.field public static final ERR_LIB_HANDLE:I = 0x800000

.field public static final ERR_MASK:I = 0xfffffff

.field public static final ERR_NEED_WAITING:I = 0x4000000

.field public static final ERR_NOMEM:I = 0x4

.field public static final ERR_NOMEM_BIG:I = 0xc

.field public static final ERR_NOMEM_EXIT:I = 0x8000004

.field public static final ERR_NOT_CREATE_DC:I = 0x100000

.field public static final ERR_NOT_CREATE_THREAD:I = 0x200000

.field public static final ERR_NOT_SUPPORT:I = 0x200

.field public static final ERR_OK:I = 0x0

.field public static final ERR_OLD_FILE_VER:I = 0x80

.field public static final ERR_ON_FINDING:I = 0x80000

.field public static final ERR_PASSWORD:I = 0x2000

.field public static final ERR_UNDEF_FILE:I = 0x40

.field public static final ERR_XLS_PRINT_PAGE_ZERO:I = 0x20000

.field public static final ERR_XLS_REFERENCE:I = 0x40000

.field public static final FILE_TYPE_DOC:I = 0x1

.field public static final FILE_TYPE_DOCX:I = 0x2

.field public static final FILE_TYPE_HWP:I = 0x9

.field public static final FILE_TYPE_NONE:I = 0x0

.field public static final FILE_TYPE_PDF:I = 0x7

.field public static final FILE_TYPE_PPT:I = 0x5

.field public static final FILE_TYPE_PPTX:I = 0x6

.field public static final FILE_TYPE_TXT:I = 0x8

.field public static final FILE_TYPE_XLS:I = 0x3

.field public static final FILE_TYPE_XLSX:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
