.class public Lcom/k2mobile/k2lib;
.super Ljava/lang/Object;
.source "k2lib.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/k2mobile/k2lib$OnCreateImageListener;,
        Lcom/k2mobile/k2lib$OnFileLoadingListener;,
        Lcom/k2mobile/k2lib$excelSheet;
    }
.end annotation


# static fields
.field private static mHPrinter:I

.field private static mHViewer:I

.field private static mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

.field private static mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

.field private static m_callback_reading_finished:I

.field private static m_callback_reading_page_count:I

.field private static m_callback_reading_percent:I

.field private static m_loding_stop_depth:I


# instance fields
.field private doSensorCallback:Ljava/util/TimerTask;

.field private mDisplayDpi:I

.field private mErr:[I

.field private mErrcode:I

.field private mFontName:Ljava/lang/String;

.field private mImageMaxSize:I

.field private mPortrait:I

.field private mSensorCallbackTimer:Ljava/util/Timer;

.field private mTemporary:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 134
    sput v0, Lcom/k2mobile/k2lib;->mHViewer:I

    .line 135
    sput v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    .line 144
    sput v0, Lcom/k2mobile/k2lib;->m_callback_reading_percent:I

    .line 145
    sput v0, Lcom/k2mobile/k2lib;->m_callback_reading_page_count:I

    .line 146
    sput v0, Lcom/k2mobile/k2lib;->m_callback_reading_finished:I

    .line 148
    sput-object v1, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    .line 149
    sput-object v1, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    .line 151
    const/4 v0, 0x1

    sput v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object v1, p0, Lcom/k2mobile/k2lib;->mFontName:Ljava/lang/String;

    .line 139
    iput-object v1, p0, Lcom/k2mobile/k2lib;->mTemporary:Ljava/lang/String;

    .line 140
    iput v2, p0, Lcom/k2mobile/k2lib;->mDisplayDpi:I

    .line 141
    iput v2, p0, Lcom/k2mobile/k2lib;->mImageMaxSize:I

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lcom/k2mobile/k2lib;->mPortrait:I

    .line 150
    iput-object v1, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    .line 153
    new-instance v0, Lcom/k2mobile/k2lib$1;

    invoke-direct {v0, p0}, Lcom/k2mobile/k2lib$1;-><init>(Lcom/k2mobile/k2lib;)V

    iput-object v0, p0, Lcom/k2mobile/k2lib;->doSensorCallback:Ljava/util/TimerTask;

    .line 206
    sput-object v1, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    .line 207
    sput-object v1, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    .line 208
    const-string v0, "k2ViewerJni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 209
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    .line 210
    iput v2, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    .line 211
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniCreateViewer([I)I

    move-result v0

    sput v0, Lcom/k2mobile/k2lib;->mHViewer:I

    .line 213
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniCreateViewer"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 214
    iget v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    if-eqz v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/k2mobile/k2lib;->setColorType(I)I

    goto :goto_0
.end method

.method private static native JniBreakLoading(I[I)V
.end method

.method private static native JniCancelLoading(I[I)V
.end method

.method private static native JniCancelPageImage(I[I)V
.end method

.method private static native JniCloseFile(I[I)V
.end method

.method private static native JniContinueLoading(I[I)V
.end method

.method private static native JniCreatePageImage(IIIIIII[I[I)V
.end method

.method private static native JniCreatePageImageBlocking(IIIIIII[I[I)V
.end method

.method private static native JniCreatePageImageWidthSize(II[I[I[I[I)V
.end method

.method private static native JniCreateViewer([I)I
.end method

.method private static native JniDeleteViewer(I[I)V
.end method

.method private static native JniExcelSheetPrint(II[I)V
.end method

.method private static native JniGetExcelSheetCount(I)I
.end method

.method private static native JniGetExcelSheetName(II[C[I)I
.end method

.method private static native JniGetFilePageCount(I)I
.end method

.method private static native JniGetFileType(I)I
.end method

.method private static native JniGetHandle(I)I
.end method

.method private static native JniGetHasImage(I)I
.end method

.method private static native JniGetImageLoadedState(I)I
.end method

.method private static native JniGetPageCount(I)I
.end method

.method private static native JniGetPageImageSize(III[I[I[I)V
.end method

.method private static native JniGetShowBackground(I)I
.end method

.method private static native JniIsPortrait(II)I
.end method

.method private static native JniIsReadingFinished(I)I
.end method

.method private static native JniIsSupportEncrypt(I)I
.end method

.method private static native JniLoadingPercent(II)I
.end method

.method private static native JniOpenFileEx(ILjava/lang/String;I[I)V
.end method

.method private static native JniRenderPageWidthSize(IIIIIZIIII[I[I)V
.end method

.method private static native JniRenderPageWidthZoom(IIFIZIIII[I[I)V
.end method

.method private static native JniSensorCallbacks(I)V
.end method

.method private static native JniSetColorType(II[I)V
.end method

.method private static native JniSetDisplayDPI(II[I)V
.end method

.method private static native JniSetExcelPrintOption(IZ)I
.end method

.method private static native JniSetFontPath(ILjava/lang/String;[I)V
.end method

.method private static native JniSetImageMaxSize(II)V
.end method

.method private static native JniSetPassword(I[C[I)V
.end method

.method private static native JniSetPortrait(II[I)V
.end method

.method private static native JniSetShowBackground(II[I)V
.end method

.method private static native JniSetTemporary(ILjava/lang/String;[I)V
.end method

.method private static native JniSetUseableSpace(J)V
.end method

.method static synthetic access$0(Lcom/k2mobile/k2lib;)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->sensorCallbacks()V

    return-void
.end method

.method private cancelSensorCallback()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    .line 165
    :cond_0
    return-void
.end method

.method private static cbimage(III)V
    .locals 2
    .param p0, "finished_page_index"    # I
    .param p1, "has_undrawed_images"    # I
    .param p2, "error_code"    # I

    .prologue
    .line 279
    sget-object v0, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    if-nez v0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    const/4 v0, -0x1

    if-eq p0, v0, :cond_2

    .line 283
    sget-object v1, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, p0, v0}, Lcom/k2mobile/k2lib$OnCreateImageListener;->onCreateFinish(IZ)V

    .line 285
    :cond_2
    if-eqz p2, :cond_0

    .line 286
    sget-object v0, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    invoke-interface {v0, p2}, Lcom/k2mobile/k2lib$OnCreateImageListener;->onErrorImage(I)V

    goto :goto_0

    .line 283
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static cbload(III)V
    .locals 4
    .param p0, "reading_percent"    # I
    .param p1, "reading_finished"    # I
    .param p2, "error_code"    # I

    .prologue
    const/4 v0, 0x1

    .line 233
    sget-object v1, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    if-nez v1, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    sget v1, Lcom/k2mobile/k2lib;->m_callback_reading_percent:I

    if-ne v1, p0, :cond_2

    sget v1, Lcom/k2mobile/k2lib;->m_callback_reading_finished:I

    if-eq v1, p1, :cond_3

    .line 238
    :cond_2
    sput p0, Lcom/k2mobile/k2lib;->m_callback_reading_percent:I

    .line 239
    sput p1, Lcom/k2mobile/k2lib;->m_callback_reading_finished:I

    .line 240
    sget-object v1, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    sget v2, Lcom/k2mobile/k2lib;->m_callback_reading_percent:I

    sget v3, Lcom/k2mobile/k2lib;->m_callback_reading_finished:I

    if-ne v3, v0, :cond_4

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/k2mobile/k2lib$OnFileLoadingListener;->onLoading(IZ)V

    .line 242
    :cond_3
    if-eqz p2, :cond_0

    .line 243
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    invoke-interface {v0, p2}, Lcom/k2mobile/k2lib$OnFileLoadingListener;->onErrorFile(I)V

    goto :goto_0

    .line 240
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static cbpassword()V
    .locals 1

    .prologue
    .line 260
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    if-nez v0, :cond_0

    .line 263
    :goto_0
    return-void

    .line 262
    :cond_0
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    invoke-interface {v0}, Lcom/k2mobile/k2lib$OnFileLoadingListener;->onPassword()V

    goto :goto_0
.end method

.method private static cbpgcount(I)V
    .locals 2
    .param p0, "reading_page_count"    # I

    .prologue
    .line 249
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    if-nez v0, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    if-lez p0, :cond_0

    sget v0, Lcom/k2mobile/k2lib;->m_callback_reading_page_count:I

    if-eq v0, p0, :cond_0

    .line 253
    sput p0, Lcom/k2mobile/k2lib;->m_callback_reading_page_count:I

    .line 254
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    sget v1, Lcom/k2mobile/k2lib;->m_callback_reading_page_count:I

    invoke-interface {v0, v1}, Lcom/k2mobile/k2lib$OnFileLoadingListener;->onLoadingPage(I)V

    goto :goto_0
.end method

.method private static cbupdated(I)V
    .locals 1
    .param p0, "update_page_index"    # I

    .prologue
    .line 268
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    if-nez v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    .line 272
    sget-object v0, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    invoke-interface {v0, p0}, Lcom/k2mobile/k2lib$OnFileLoadingListener;->onUpdateable(I)V

    goto :goto_0
.end method

.method private createSensorCallback()V
    .locals 6

    .prologue
    .line 168
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Ljava/util/Timer;

    const-string v1, "k2libSensorCallback"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    .line 170
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mSensorCallbackTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/k2mobile/k2lib;->doSensorCallback:Ljava/util/TimerTask;

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 172
    :cond_0
    return-void
.end method

.method private errcode()I
    .locals 2

    .prologue
    .line 292
    iget v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    .line 293
    .local v0, "err_code":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    .line 294
    return v0
.end method

.method private error_code(ILjava/lang/String;)I
    .locals 5
    .param p1, "err"    # I
    .param p2, "func"    # Ljava/lang/String;

    .prologue
    .line 182
    const v2, 0xfffffff

    and-int/2addr v2, p1

    and-int/lit8 v0, v2, -0x3

    .line 183
    .local v0, "err_result":I
    const/high16 v2, -0x10000000

    and-int v1, p1, v2

    .line 185
    .local v1, "find_result":I
    if-nez v0, :cond_0

    .line 186
    if-eqz v1, :cond_1

    const/high16 v2, 0x20000000

    if-eq v1, v2, :cond_1

    .line 188
    :cond_0
    const-string v2, "k2lib"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local p1    # "err":I
    :goto_0
    return p1

    .restart local p1    # "err":I
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private sensorCallbacks()V
    .locals 1

    .prologue
    .line 175
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 176
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniSensorCallbacks(I)V

    .line 178
    :cond_0
    return-void
.end method

.method private setErrcode(I)V
    .locals 2
    .param p1, "err"    # I

    .prologue
    .line 201
    iget v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    const-string v1, ""

    invoke-direct {p0, p1, v1}, Lcom/k2mobile/k2lib;->error_code(ILjava/lang/String;)I

    move-result v1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    .line 202
    return-void
.end method

.method private setErrcode(ILjava/lang/String;)V
    .locals 2
    .param p1, "err"    # I
    .param p2, "func"    # Ljava/lang/String;

    .prologue
    .line 196
    iget v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    invoke-direct {p0, p1, p2}, Lcom/k2mobile/k2lib;->error_code(ILjava/lang/String;)I

    move-result v1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    .line 197
    return-void
.end method

.method public static setUseableSpace(J)V
    .locals 0
    .param p0, "in_space"    # J

    .prologue
    .line 567
    invoke-static {p0, p1}, Lcom/k2mobile/k2lib;->JniSetUseableSpace(J)V

    .line 568
    return-void
.end method


# virtual methods
.method public breakLoading()I
    .locals 2

    .prologue
    .line 465
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 467
    sget v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    .line 468
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniBreakLoading(I[I)V

    .line 469
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniBreakLoading"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 473
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 472
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public cancelLoading()I
    .locals 2

    .prologue
    .line 455
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 456
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniCancelLoading(I[I)V

    .line 457
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniCancelLoading"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 460
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 459
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public cancelPageImage()I
    .locals 3

    .prologue
    .line 628
    const/high16 v1, 0x800000

    .line 629
    .local v1, "r_err":I
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v2, :cond_0

    .line 631
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 632
    .local v0, "err":[I
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v2, v0}, Lcom/k2mobile/k2lib;->JniCancelPageImage(I[I)V

    .line 634
    const/4 v2, 0x0

    aget v1, v0, v2

    .line 636
    .end local v0    # "err":[I
    :cond_0
    return v1
.end method

.method public close()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 306
    const-string v0, "k2lib"

    const-string v1, " call close()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 309
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniDeleteViewer(I[I)V

    .line 310
    sput v2, Lcom/k2mobile/k2lib;->mHViewer:I

    .line 311
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniDeleteViewer"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 313
    :cond_0
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    if-eqz v0, :cond_1

    .line 314
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniDeleteViewer(I[I)V

    .line 315
    sput v2, Lcom/k2mobile/k2lib;->mHPrinter:I

    .line 316
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniDeleteViewer"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 318
    :cond_1
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0
.end method

.method public closeFile()I
    .locals 2

    .prologue
    .line 446
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 447
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniCloseFile(I[I)V

    .line 448
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniCloseFile"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 451
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 450
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public closePrinter()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 762
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    if-eqz v0, :cond_0

    .line 763
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniCloseFile(I[I)V

    .line 764
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniDeleteViewer(I[I)V

    .line 765
    sput v2, Lcom/k2mobile/k2lib;->mHPrinter:I

    .line 766
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniCloseFile"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 769
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 768
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public continueLoading()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 478
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_1

    .line 480
    sget v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    .line 481
    sget v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 483
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniContinueLoading(I[I)V

    .line 484
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniContinueLoading"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 491
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 486
    :cond_0
    invoke-direct {p0, v2}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0

    .line 490
    :cond_1
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public createPageImage(IIIIII[I)I
    .locals 10
    .param p1, "page_index"    # I
    .param p2, "zoom"    # I
    .param p3, "print_x_pos"    # I
    .param p4, "print_y_pos"    # I
    .param p5, "image_buffer_width"    # I
    .param p6, "image_buffer_height"    # I
    .param p7, "out_image_buffer"    # [I

    .prologue
    .line 614
    const/high16 v9, 0x800000

    .line 615
    .local v9, "r_err":I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 617
    const/4 v0, 0x1

    new-array v8, v0, [I

    .line 618
    .local v8, "err":[I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    const/16 v1, 0xd

    invoke-static {v0, v1, v8}, Lcom/k2mobile/k2lib;->JniSetColorType(II[I)V

    .line 619
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/k2mobile/k2lib;->JniCreatePageImage(IIIIIII[I[I)V

    .line 621
    const/4 v0, 0x0

    aget v9, v8, v0

    .line 623
    .end local v8    # "err":[I
    :cond_0
    return v9
.end method

.method public createPageImageBlocking(IIIIII[I)I
    .locals 10
    .param p1, "page_index"    # I
    .param p2, "zoom"    # I
    .param p3, "print_x_pos"    # I
    .param p4, "print_y_pos"    # I
    .param p5, "image_buffer_width"    # I
    .param p6, "image_buffer_height"    # I
    .param p7, "out_image_buffer"    # [I

    .prologue
    .line 642
    const/high16 v9, 0x800000

    .line 643
    .local v9, "r_err":I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 645
    const/4 v0, 0x1

    new-array v8, v0, [I

    .line 646
    .local v8, "err":[I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    const/16 v1, 0xd

    invoke-static {v0, v1, v8}, Lcom/k2mobile/k2lib;->JniSetColorType(II[I)V

    .line 647
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/k2mobile/k2lib;->JniCreatePageImageBlocking(IIIIIII[I[I)V

    .line 649
    const/4 v0, 0x0

    aget v9, v8, v0

    .line 651
    .end local v8    # "err":[I
    :cond_0
    return v9
.end method

.method public createPageImageWidthSize(ILandroid/graphics/Rect;[I)I
    .locals 8
    .param p1, "page_index"    # I
    .param p2, "io_image_size"    # Landroid/graphics/Rect;
    .param p3, "out_image_buffer"    # [I

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 687
    const/high16 v6, 0x800000

    .line 688
    .local v6, "r_err":I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 690
    if-eqz p2, :cond_1

    .line 691
    new-array v2, v1, [I

    .line 692
    .local v2, "w":[I
    new-array v3, v1, [I

    .line 693
    .local v3, "h":[I
    new-array v5, v1, [I

    .line 694
    .local v5, "err":[I
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    aput v0, v2, v7

    .line 695
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    aput v0, v3, v7

    .line 696
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    const/16 v1, 0xd

    invoke-static {v0, v1, v5}, Lcom/k2mobile/k2lib;->JniSetColorType(II[I)V

    .line 697
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/k2mobile/k2lib;->JniCreatePageImageWidthSize(II[I[I[I[I)V

    .line 698
    iput v7, p2, Landroid/graphics/Rect;->left:I

    .line 699
    iput v7, p2, Landroid/graphics/Rect;->top:I

    .line 700
    aget v0, v2, v7

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 701
    aget v0, v3, v7

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 703
    aget v6, v5, v7

    .line 708
    .end local v2    # "w":[I
    .end local v3    # "h":[I
    .end local v5    # "err":[I
    :cond_0
    :goto_0
    return v6

    .line 705
    :cond_1
    const/16 v6, 0x10

    goto :goto_0
.end method

.method public excelSheetCount()I
    .locals 1

    .prologue
    .line 365
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 366
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetExcelSheetCount(I)I

    move-result v0

    .line 368
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public excelSheetName(I)Lcom/k2mobile/k2lib$excelSheet;
    .locals 7
    .param p1, "in_sheet_idx"    # I

    .prologue
    const/4 v6, 0x0

    .line 373
    const-string v2, ""

    .line 374
    .local v2, "name":Ljava/lang/String;
    const/4 v0, 0x0

    .line 375
    .local v0, "flag":I
    sget v5, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v5, :cond_1

    .line 376
    const/16 v5, 0x80

    new-array v3, v5, [C

    .line 377
    .local v3, "sheetname":[C
    const/4 v5, 0x1

    new-array v4, v5, [I

    .line 378
    .local v4, "status":[I
    sget v5, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v5, p1, v3, v4}, Lcom/k2mobile/k2lib;->JniGetExcelSheetName(II[C[I)I

    move-result v1

    .line 379
    .local v1, "len":I
    if-lez v1, :cond_0

    .line 380
    new-instance v2, Ljava/lang/String;

    .end local v2    # "name":Ljava/lang/String;
    invoke-direct {v2, v3, v6, v1}, Ljava/lang/String;-><init>([CII)V

    .line 382
    .restart local v2    # "name":Ljava/lang/String;
    :cond_0
    aget v0, v4, v6

    .line 383
    const/4 v4, 0x0

    .line 386
    .end local v1    # "len":I
    .end local v3    # "sheetname":[C
    .end local v4    # "status":[I
    :cond_1
    new-instance v5, Lcom/k2mobile/k2lib$excelSheet;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v2, v0, v6}, Lcom/k2mobile/k2lib$excelSheet;-><init>(Lcom/k2mobile/k2lib;Ljava/lang/String;ILcom/k2mobile/k2lib$excelSheet;)V

    return-object v5
.end method

.method public excelSheetPrint(I)I
    .locals 2
    .param p1, "in_sheet_idx"    # I

    .prologue
    .line 392
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 393
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniExcelSheetPrint(II[I)V

    .line 394
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniExcelSheetPrint"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 397
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 396
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public filePageCount()I
    .locals 1

    .prologue
    .line 527
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 529
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetFilePageCount(I)I

    move-result v0

    .line 531
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public fileType()I
    .locals 1

    .prologue
    .line 559
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 560
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetFileType(I)I

    move-result v0

    .line 562
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/k2mobile/k2lib;->close()I

    .line 301
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 302
    return-void
.end method

.method public getPageImageSize(II)Landroid/graphics/Rect;
    .locals 8
    .param p1, "page_index"    # I
    .param p2, "zoom"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 599
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v7, v7, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 600
    .local v6, "rt":Landroid/graphics/Rect;
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 602
    new-array v3, v1, [I

    .line 603
    .local v3, "w":[I
    new-array v4, v1, [I

    .line 604
    .local v4, "h":[I
    new-array v5, v1, [I

    .line 605
    .local v5, "err":[I
    aput v7, v4, v7

    aput v7, v3, v7

    .line 606
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/k2mobile/k2lib;->JniGetPageImageSize(III[I[I[I)V

    .line 607
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "rt":Landroid/graphics/Rect;
    aget v0, v3, v7

    aget v1, v4, v7

    invoke-direct {v6, v7, v7, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 609
    .end local v3    # "w":[I
    .end local v4    # "h":[I
    .end local v5    # "err":[I
    .restart local v6    # "rt":Landroid/graphics/Rect;
    :cond_0
    return-object v6
.end method

.method public getPrinterHandle()I
    .locals 1

    .prologue
    .line 734
    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    if-eqz v0, :cond_0

    sget v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetHandle(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewerHandle()I
    .locals 1

    .prologue
    .line 726
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetHandle(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public imageLoadedState()I
    .locals 1

    .prologue
    .line 553
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 554
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetImageLoadedState(I)I

    move-result v0

    .line 556
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHasImage()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 507
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v2, :cond_1

    .line 508
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v2}, Lcom/k2mobile/k2lib;->JniGetHasImage(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 510
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 508
    goto :goto_0

    :cond_1
    move v0, v1

    .line 510
    goto :goto_0
.end method

.method public isPortrait(I)I
    .locals 1
    .param p1, "page_index"    # I

    .prologue
    .line 591
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 593
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0, p1}, Lcom/k2mobile/k2lib;->JniIsPortrait(II)I

    move-result v0

    .line 595
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isReadingFinished()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 547
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v2, :cond_1

    .line 548
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v2}, Lcom/k2mobile/k2lib;->JniIsReadingFinished(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 550
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 548
    goto :goto_0

    :cond_1
    move v0, v1

    .line 550
    goto :goto_0
.end method

.method public isSupportEncrypt()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 535
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v2, :cond_1

    .line 536
    sget v2, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v2}, Lcom/k2mobile/k2lib;->JniIsSupportEncrypt(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 538
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 536
    goto :goto_0

    :cond_1
    move v0, v1

    .line 538
    goto :goto_0
.end method

.method public loadingPercent(I)I
    .locals 1
    .param p1, "in_ascend"    # I

    .prologue
    .line 541
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 542
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0, p1}, Lcom/k2mobile/k2lib;->JniLoadingPercent(II)I

    move-result v0

    .line 544
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openFile(Ljava/lang/String;I)I
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "in_flag"    # I

    .prologue
    const/4 v2, 0x0

    .line 429
    sput v2, Lcom/k2mobile/k2lib;->m_callback_reading_percent:I

    .line 430
    sput v2, Lcom/k2mobile/k2lib;->m_callback_reading_page_count:I

    .line 431
    sput v2, Lcom/k2mobile/k2lib;->m_callback_reading_finished:I

    .line 432
    const/4 v0, 0x1

    sput v0, Lcom/k2mobile/k2lib;->m_loding_stop_depth:I

    .line 433
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_1

    .line 434
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, p2, v1}, Lcom/k2mobile/k2lib;->JniOpenFileEx(ILjava/lang/String;I[I)V

    .line 435
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniOpenFileEx"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 438
    :goto_0
    iget v0, p0, Lcom/k2mobile/k2lib;->mErrcode:I

    if-nez v0, :cond_0

    .line 439
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->createSensorCallback()V

    .line 441
    :cond_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 437
    :cond_1
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public pageCount()I
    .locals 1

    .prologue
    .line 515
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 516
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetPageCount(I)I

    move-result v0

    .line 518
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public renderPage_widthSize(IIIIIIII[IZ)I
    .locals 13
    .param p1, "page_index"    # I
    .param p2, "sizeWidth"    # I
    .param p3, "sizeHeight"    # I
    .param p4, "colorType"    # I
    .param p5, "x"    # I
    .param p6, "y"    # I
    .param p7, "image_width"    # I
    .param p8, "image_height"    # I
    .param p9, "image_buffer"    # [I
    .param p10, "fontSmoothly"    # Z

    .prologue
    .line 672
    const/high16 v12, 0x800000

    .line 673
    .local v12, "r_err":I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 675
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 676
    .local v11, "err":[I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p10

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v0 .. v11}, Lcom/k2mobile/k2lib;->JniRenderPageWidthSize(IIIIIZIIII[I[I)V

    .line 678
    const/4 v0, 0x0

    aget v12, v11, v0

    .line 680
    .end local v11    # "err":[I
    :cond_0
    return v12
.end method

.method public renderPage_widthZoom(IFIIIII[IZ)I
    .locals 12
    .param p1, "page_index"    # I
    .param p2, "zoom"    # F
    .param p3, "colorType"    # I
    .param p4, "x"    # I
    .param p5, "y"    # I
    .param p6, "image_width"    # I
    .param p7, "image_height"    # I
    .param p8, "image_buffer"    # [I
    .param p9, "fontSmoothly"    # Z

    .prologue
    .line 658
    const/high16 v11, 0x800000

    .line 659
    .local v11, "r_err":I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 661
    const/4 v0, 0x1

    new-array v10, v0, [I

    .line 662
    .local v10, "err":[I
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p9

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-static/range {v0 .. v10}, Lcom/k2mobile/k2lib;->JniRenderPageWidthZoom(IIFIZIIII[I[I)V

    .line 664
    const/4 v0, 0x0

    aget v11, v10, v0

    .line 666
    .end local v10    # "err":[I
    :cond_0
    return v11
.end method

.method public setColorType(I)I
    .locals 2
    .param p1, "in_color_type"    # I

    .prologue
    .line 324
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 325
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniSetColorType(II[I)V

    .line 326
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetColorType"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 329
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 328
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public setDisplayDpi(I)I
    .locals 2
    .param p1, "in_dpi"    # I

    .prologue
    .line 335
    iput p1, p0, Lcom/k2mobile/k2lib;->mDisplayDpi:I

    .line 336
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 337
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniSetDisplayDPI(II[I)V

    .line 338
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetDisplayDPI"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 341
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 340
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public setExcelPrintOption(Z)I
    .locals 1
    .param p1, "use_print_area"    # Z

    .prologue
    .line 402
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 403
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0, p1}, Lcom/k2mobile/k2lib;->JniSetExcelPrintOption(IZ)I

    move-result v0

    .line 405
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x800000

    goto :goto_0
.end method

.method public setFont(Ljava/lang/String;)I
    .locals 2
    .param p1, "fontFile"    # Ljava/lang/String;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/k2mobile/k2lib;->mFontName:Ljava/lang/String;

    .line 348
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 349
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniSetFontPath(ILjava/lang/String;[I)V

    .line 350
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetFontPath"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 353
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 352
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public setImageMaxSize(I)V
    .locals 1
    .param p1, "in_w_h_max"    # I

    .prologue
    .line 357
    iput p1, p0, Lcom/k2mobile/k2lib;->mImageMaxSize:I

    .line 358
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 359
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0, p1}, Lcom/k2mobile/k2lib;->JniSetImageMaxSize(II)V

    .line 361
    :cond_0
    return-void
.end method

.method public setOnCreateImageListener(Lcom/k2mobile/k2lib$OnCreateImageListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/k2mobile/k2lib$OnCreateImageListener;

    .prologue
    .line 226
    sput-object p1, Lcom/k2mobile/k2lib;->mOnCreateImageListener:Lcom/k2mobile/k2lib$OnCreateImageListener;

    .line 227
    return-void
.end method

.method public setOnFileLoadingListener(Lcom/k2mobile/k2lib$OnFileLoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/k2mobile/k2lib$OnFileLoadingListener;

    .prologue
    .line 222
    sput-object p1, Lcom/k2mobile/k2lib;->mOnFileLoadingListener:Lcom/k2mobile/k2lib$OnFileLoadingListener;

    .line 223
    return-void
.end method

.method public setPassword(Ljava/lang/String;)I
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 496
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 498
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetPassword(I[C[I)V

    .line 499
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetPassword"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 503
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 502
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public setPortrait(Z)I
    .locals 4
    .param p1, "portrait"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 574
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/k2mobile/k2lib;->mPortrait:I

    .line 575
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_2

    .line 577
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz p1, :cond_1

    :goto_1
    iget-object v3, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v3}, Lcom/k2mobile/k2lib;->JniSetPortrait(II[I)V

    .line 578
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v2

    const-string v1, "JniSetPortrait"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 582
    :goto_2
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 574
    goto :goto_0

    :cond_1
    move v1, v2

    .line 577
    goto :goto_1

    .line 581
    :cond_2
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_2
.end method

.method public setShowBackground(I)I
    .locals 2
    .param p1, "in_show"    # I

    .prologue
    .line 412
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 413
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniSetShowBackground(II[I)V

    .line 414
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetShowBackground"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 417
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 416
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public setTemporary(Ljava/lang/String;)I
    .locals 2
    .param p1, "temporaryPath"    # Ljava/lang/String;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/k2mobile/k2lib;->mTemporary:Ljava/lang/String;

    .line 715
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 716
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, p1, v1}, Lcom/k2mobile/k2lib;->JniSetTemporary(ILjava/lang/String;[I)V

    .line 717
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const-string v1, "JniSetTemporary"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 720
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 719
    :cond_0
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method

.method public showBackground()I
    .locals 1

    .prologue
    .line 420
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_0

    .line 421
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniGetShowBackground(I)I

    move-result v0

    .line 423
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public viewerToPrinter()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 740
    invoke-virtual {p0}, Lcom/k2mobile/k2lib;->closePrinter()I

    .line 741
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_6

    .line 742
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    sput v0, Lcom/k2mobile/k2lib;->mHPrinter:I

    .line 743
    sput v3, Lcom/k2mobile/k2lib;->mHViewer:I

    .line 744
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0}, Lcom/k2mobile/k2lib;->JniCreateViewer([I)I

    move-result v0

    sput v0, Lcom/k2mobile/k2lib;->mHViewer:I

    .line 745
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v3

    const-string v1, "JniCreateViewer"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 746
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    if-eqz v0, :cond_5

    .line 747
    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetColorType(II[I)V

    .line 748
    iget v0, p0, Lcom/k2mobile/k2lib;->mDisplayDpi:I

    if-lez v0, :cond_0

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget v1, p0, Lcom/k2mobile/k2lib;->mDisplayDpi:I

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetDisplayDPI(II[I)V

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mFontName:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mFontName:Ljava/lang/String;

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetFontPath(ILjava/lang/String;[I)V

    .line 750
    :cond_1
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mTemporary:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget-object v1, p0, Lcom/k2mobile/k2lib;->mTemporary:Ljava/lang/String;

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetTemporary(ILjava/lang/String;[I)V

    .line 751
    :cond_2
    iget v0, p0, Lcom/k2mobile/k2lib;->mImageMaxSize:I

    if-eqz v0, :cond_3

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget v1, p0, Lcom/k2mobile/k2lib;->mImageMaxSize:I

    invoke-static {v0, v1}, Lcom/k2mobile/k2lib;->JniSetImageMaxSize(II)V

    .line 752
    :cond_3
    iget v0, p0, Lcom/k2mobile/k2lib;->mPortrait:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    sget v0, Lcom/k2mobile/k2lib;->mHViewer:I

    iget v1, p0, Lcom/k2mobile/k2lib;->mPortrait:I

    iget-object v2, p0, Lcom/k2mobile/k2lib;->mErr:[I

    invoke-static {v0, v1, v2}, Lcom/k2mobile/k2lib;->JniSetPortrait(II[I)V

    .line 753
    :cond_4
    iget-object v0, p0, Lcom/k2mobile/k2lib;->mErr:[I

    aget v0, v0, v3

    const-string v1, "viewerToPrinter"

    invoke-direct {p0, v0, v1}, Lcom/k2mobile/k2lib;->setErrcode(ILjava/lang/String;)V

    .line 757
    :cond_5
    :goto_0
    invoke-direct {p0}, Lcom/k2mobile/k2lib;->errcode()I

    move-result v0

    return v0

    .line 756
    :cond_6
    const/high16 v0, 0x800000

    invoke-direct {p0, v0}, Lcom/k2mobile/k2lib;->setErrcode(I)V

    goto :goto_0
.end method
