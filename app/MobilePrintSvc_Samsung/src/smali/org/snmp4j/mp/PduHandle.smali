.class public Lorg/snmp4j/mp/PduHandle;
.super Ljava/lang/Object;
.source "PduHandle.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final NONE:I = -0x80000000

.field private static final serialVersionUID:J = -0x5857bd0558d648d6L


# instance fields
.field private transactionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/high16 v0, -0x80000000

    iput v0, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    .line 44
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "transactionID"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/high16 v0, -0x80000000

    iput v0, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    .line 52
    invoke-virtual {p0, p1}, Lorg/snmp4j/mp/PduHandle;->setTransactionID(I)V

    .line 53
    return-void
.end method


# virtual methods
.method public copyFrom(Lorg/snmp4j/mp/PduHandle;)V
    .locals 1
    .param p1, "other"    # Lorg/snmp4j/mp/PduHandle;

    .prologue
    .line 79
    iget v0, p1, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    invoke-virtual {p0, v0}, Lorg/snmp4j/mp/PduHandle;->setTransactionID(I)V

    .line 80
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 90
    instance-of v1, p1, Lorg/snmp4j/mp/PduHandle;

    if-eqz v1, :cond_0

    .line 91
    iget v1, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    check-cast p1, Lorg/snmp4j/mp/PduHandle;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p1, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 93
    :cond_0
    return v0
.end method

.method public getTransactionID()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    return v0
.end method

.method public setTransactionID(I)V
    .locals 0
    .param p1, "transactionID"    # I

    .prologue
    .line 70
    iput p1, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    .line 71
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "PduHandle["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lorg/snmp4j/mp/PduHandle;->transactionID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
