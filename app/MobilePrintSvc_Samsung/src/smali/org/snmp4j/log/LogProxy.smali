.class public Lorg/snmp4j/log/LogProxy;
.super Ljava/lang/Object;
.source "LogProxy.java"

# interfaces
.implements Lorg/snmp4j/log/LogAdapter;


# instance fields
.field private logger:Lorg/snmp4j/log/LogAdapter;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lorg/snmp4j/log/LogProxy;->name:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/snmp4j/log/LogAdapter;)V
    .locals 0
    .param p1, "logger"    # Lorg/snmp4j/log/LogAdapter;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    .line 45
    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/Object;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 48
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 51
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/Object;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 57
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 60
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1, p2}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 63
    :cond_0
    return-void
.end method

.method public fatal(Ljava/lang/Object;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 66
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->fatal(Ljava/lang/Object;)V

    .line 69
    :cond_0
    return-void
.end method

.method public fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 72
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1, p2}, Lorg/snmp4j/log/LogAdapter;->fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 75
    :cond_0
    return-void
.end method

.method public getEffectiveLogLevel()Lorg/snmp4j/log/LogLevel;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->getEffectiveLogLevel()Lorg/snmp4j/log/LogLevel;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/snmp4j/log/LogLevel;->OFF:Lorg/snmp4j/log/LogLevel;

    goto :goto_0
.end method

.method public getLogHandler()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->getLogHandler()Ljava/util/Iterator;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public getLogLevel()Lorg/snmp4j/log/LogLevel;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->getLogLevel()Lorg/snmp4j/log/LogLevel;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/snmp4j/log/LogLevel;->OFF:Lorg/snmp4j/log/LogLevel;

    goto :goto_0
.end method

.method public getLogger()Lorg/snmp4j/log/LogAdapter;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->getName()Ljava/lang/String;

    .line 102
    :cond_0
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->name:Ljava/lang/String;

    return-object v0
.end method

.method public info(Ljava/lang/Object;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 106
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->info(Ljava/lang/Object;)V

    .line 109
    :cond_0
    return-void
.end method

.method public isDebugEnabled()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInfoEnabled()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isInfoEnabled()Z

    move-result v0

    .line 122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWarnEnabled()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isWarnEnabled()Z

    move-result v0

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLogLevel(Lorg/snmp4j/log/LogLevel;)V
    .locals 1
    .param p1, "level"    # Lorg/snmp4j/log/LogLevel;

    .prologue
    .line 133
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->setLogLevel(Lorg/snmp4j/log/LogLevel;)V

    .line 136
    :cond_0
    return-void
.end method

.method public setLogger(Lorg/snmp4j/log/LogAdapter;)V
    .locals 0
    .param p1, "logger"    # Lorg/snmp4j/log/LogAdapter;

    .prologue
    .line 159
    iput-object p1, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    .line 160
    return-void
.end method

.method public warn(Ljava/lang/Object;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 139
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lorg/snmp4j/log/LogProxy;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0, p1}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 142
    :cond_0
    return-void
.end method
