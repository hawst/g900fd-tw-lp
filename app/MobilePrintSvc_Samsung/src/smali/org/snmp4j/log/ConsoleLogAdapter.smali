.class public Lorg/snmp4j/log/ConsoleLogAdapter;
.super Ljava/lang/Object;
.source "ConsoleLogAdapter.java"

# interfaces
.implements Lorg/snmp4j/log/LogAdapter;


# static fields
.field private static debugEnabled:Z

.field private static infoEnabled:Z

.field private static warnEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    .line 36
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    .line 37
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static setDebugEnabled(Z)V
    .locals 0
    .param p0, "isDebugEnabled"    # Z

    .prologue
    .line 144
    sput-boolean p0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    .line 145
    return-void
.end method

.method public static setInfoEnabled(Z)V
    .locals 0
    .param p0, "isInfoEnabled"    # Z

    .prologue
    .line 152
    sput-boolean p0, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    .line 153
    return-void
.end method

.method public static setWarnEnabled(Z)V
    .locals 0
    .param p0, "isWarnEnabled"    # Z

    .prologue
    .line 148
    sput-boolean p0, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    .line 149
    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 48
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    if-eqz v0, :cond_0

    .line 49
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 59
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public error(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 69
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public fatal(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 78
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 88
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public getEffectiveLogLevel()Lorg/snmp4j/log/LogLevel;
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lorg/snmp4j/log/ConsoleLogAdapter;->getLogLevel()Lorg/snmp4j/log/LogLevel;

    move-result-object v0

    return-object v0
.end method

.method public getLogHandler()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 201
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getLogLevel()Lorg/snmp4j/log/LogLevel;
    .locals 1

    .prologue
    .line 184
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    if-eqz v0, :cond_0

    .line 185
    sget-object v0, Lorg/snmp4j/log/LogLevel;->DEBUG:Lorg/snmp4j/log/LogLevel;

    .line 193
    :goto_0
    return-object v0

    .line 187
    :cond_0
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    if-eqz v0, :cond_1

    .line 188
    sget-object v0, Lorg/snmp4j/log/LogLevel;->INFO:Lorg/snmp4j/log/LogLevel;

    goto :goto_0

    .line 190
    :cond_1
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    if-eqz v0, :cond_2

    .line 191
    sget-object v0, Lorg/snmp4j/log/LogLevel;->WARN:Lorg/snmp4j/log/LogLevel;

    goto :goto_0

    .line 193
    :cond_2
    sget-object v0, Lorg/snmp4j/log/LogLevel;->OFF:Lorg/snmp4j/log/LogLevel;

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    const-string v0, ""

    return-object v0
.end method

.method public info(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 97
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 100
    :cond_0
    return-void
.end method

.method public isDebugEnabled()Z
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    return v0
.end method

.method public isInfoEnabled()Z
    .locals 1

    .prologue
    .line 119
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    return v0
.end method

.method public isWarnEnabled()Z
    .locals 1

    .prologue
    .line 129
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    return v0
.end method

.method public setLogLevel(Lorg/snmp4j/log/LogLevel;)V
    .locals 2
    .param p1, "level"    # Lorg/snmp4j/log/LogLevel;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 156
    sput-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    .line 157
    sput-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    .line 158
    sput-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    .line 159
    invoke-virtual {p1}, Lorg/snmp4j/log/LogLevel;->getLevel()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 177
    :goto_0
    return-void

    .line 161
    :pswitch_0
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    .line 162
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    .line 163
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    goto :goto_0

    .line 167
    :pswitch_1
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->debugEnabled:Z

    goto :goto_0

    .line 170
    :pswitch_2
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->infoEnabled:Z

    goto :goto_0

    .line 173
    :pswitch_3
    sput-boolean v1, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public warn(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 138
    sget-boolean v0, Lorg/snmp4j/log/ConsoleLogAdapter;->warnEnabled:Z

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 141
    :cond_0
    return-void
.end method
