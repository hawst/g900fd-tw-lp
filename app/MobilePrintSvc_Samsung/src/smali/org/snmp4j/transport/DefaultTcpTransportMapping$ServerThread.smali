.class Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;
.super Ljava/lang/Object;
.source "DefaultTcpTransportMapping.java"

# interfaces
.implements Lorg/snmp4j/util/WorkerTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/snmp4j/transport/DefaultTcpTransportMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServerThread"
.end annotation


# instance fields
.field private buf:[B

.field private lastError:Ljava/lang/Throwable;

.field private pending:Ljava/util/LinkedList;

.field private selector:Ljava/nio/channels/Selector;

.field private ssc:Ljava/nio/channels/ServerSocketChannel;

.field private volatile stop:Z

.field private final this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;


# direct methods
.method public constructor <init>(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 581
    iput-object p1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    iput-boolean v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 575
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->lastError:Ljava/lang/Throwable;

    .line 579
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    .line 582
    invoke-virtual {p1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->getMaxInboundMessageSize()I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->buf:[B

    .line 584
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v1

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    .line 586
    invoke-static {p1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$400(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 588
    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v1

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    .line 589
    iget-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1, v2}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 592
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p1, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->tcpAddress:Lorg/snmp4j/smi/TcpAddress;

    invoke-virtual {v1}, Lorg/snmp4j/smi/TcpAddress;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    iget-object v2, p1, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->tcpAddress:Lorg/snmp4j/smi/TcpAddress;

    invoke-virtual {v2}, Lorg/snmp4j/smi/TcpAddress;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 594
    .local v0, "isa":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->setSocketOptions(Ljava/net/ServerSocket;)V

    .line 595
    iget-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 600
    iget-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/16 v3, 0x10

    invoke-virtual {v1, v2, v3}, Ljava/nio/channels/ServerSocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 602
    .end local v0    # "isa":Ljava/net/InetSocketAddress;
    :cond_0
    return-void
.end method

.method private closeChannel(Ljava/nio/channels/SelectableChannel;)V
    .locals 2
    .param p1, "channel"    # Ljava/nio/channels/SelectableChannel;

    .prologue
    .line 938
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectableChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 943
    :goto_0
    return-void

    .line 940
    :catch_0
    move-exception v0

    .line 941
    .local v0, "channelCloseException":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private connectChannel(Ljava/nio/channels/SelectionKey;Lorg/snmp4j/smi/TcpAddress;)V
    .locals 8
    .param p1, "sk"    # Ljava/nio/channels/SelectionKey;
    .param p2, "incomingAddress"    # Lorg/snmp4j/smi/TcpAddress;

    .prologue
    .line 864
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 866
    .local v2, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v4

    check-cast v4, Ljava/nio/channels/SocketChannel;

    .line 867
    .local v4, "sc":Ljava/nio/channels/SocketChannel;
    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v5

    if-nez v5, :cond_0

    .line 868
    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 869
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 870
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Connected to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 873
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v5, v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$500(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;)V

    .line 874
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/16 v6, 0x8

    invoke-virtual {v2, v5, v6}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->removeRegistration(Ljava/nio/channels/Selector;I)V

    .line 875
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v6, 0x4

    invoke-virtual {v2, v5, v6}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 881
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 882
    if-nez p2, :cond_3

    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v0

    .line 884
    .local v0, "addr":Lorg/snmp4j/smi/Address;
    :goto_1
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Fire connected event for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 885
    new-instance v1, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {v1, v5, v0, v6, v7}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 891
    .local v1, "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v5, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 902
    .end local v0    # "addr":Lorg/snmp4j/smi/Address;
    .end local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    .end local v4    # "sc":Ljava/nio/channels/SocketChannel;
    :cond_1
    :goto_2
    return-void

    .line 878
    .restart local v4    # "sc":Ljava/nio/channels/SocketChannel;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move-object v0, p2

    .line 882
    goto :goto_1

    .line 894
    .end local v4    # "sc":Ljava/nio/channels/SocketChannel;
    :catch_0
    move-exception v3

    .line 895
    .local v3, "iox":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v5

    invoke-interface {v5, v3}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 896
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 897
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->closeChannel(Ljava/nio/channels/SelectableChannel;)V

    .line 898
    if-eqz v2, :cond_1

    .line 899
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private dispatchMessage(Lorg/snmp4j/smi/TcpAddress;Ljava/nio/ByteBuffer;J)V
    .locals 8
    .param p1, "incomingAddress"    # Lorg/snmp4j/smi/TcpAddress;
    .param p2, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p3, "bytesRead"    # J

    .prologue
    const/4 v7, 0x0

    .line 1048
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1049
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    invoke-interface {v2}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1050
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Received message from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    long-to-int v6, p3

    invoke-direct {v4, v5, v7, v6}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1056
    :cond_0
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->isAsyncMsgProcessingSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1057
    long-to-int v2, p3

    new-array v1, v2, [B

    .line 1058
    .local v1, "bytes":[B
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    long-to-int v3, p3

    invoke-static {v2, v7, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1059
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1065
    .end local v1    # "bytes":[B
    .local v0, "bis":Ljava/nio/ByteBuffer;
    :goto_0
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v2, p1, v0}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireProcessMessage(Lorg/snmp4j/smi/Address;Ljava/nio/ByteBuffer;)V

    .line 1066
    return-void

    .line 1062
    .end local v0    # "bis":Ljava/nio/ByteBuffer;
    :cond_1
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    long-to-int v3, p3

    invoke-static {v2, v7, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .restart local v0    # "bis":Ljava/nio/ByteBuffer;
    goto :goto_0
.end method

.method private processPending()V
    .locals 11

    .prologue
    .line 605
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    monitor-enter v7

    .line 606
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_0
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 607
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    .local v2, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_start_1
    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Socket;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 615
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v8, 0x4

    invoke-virtual {v2, v6, v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 606
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 618
    :cond_1
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/16 v8, 0x8

    invoke-virtual {v2, v6, v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V
    :try_end_1
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 621
    :catch_0
    move-exception v0

    .line 622
    .local v0, "ckex":Ljava/nio/channels/CancelledKeyException;
    :try_start_2
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v6

    invoke-interface {v6, v0}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 623
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v6, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 625
    :try_start_3
    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Socket;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/SocketChannel;->close()V

    .line 626
    new-instance v1, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v8

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-direct {v1, v6, v8, v9, v10}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 631
    .local v1, "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v6, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 633
    .end local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    :catch_1
    move-exception v3

    .line 634
    .local v3, "ex":Ljava/io/IOException;
    :try_start_4
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v6

    invoke-interface {v6, v3}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    goto :goto_1

    .line 660
    .end local v0    # "ckex":Ljava/nio/channels/CancelledKeyException;
    .end local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v3    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v6

    .line 637
    .restart local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :catch_2
    move-exception v5

    .line 638
    .local v5, "iox":Ljava/io/IOException;
    :try_start_5
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v6

    invoke-interface {v6, v5}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 639
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v6, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 643
    :try_start_6
    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Socket;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/SocketChannel;->close()V

    .line 644
    new-instance v1, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v8

    const/4 v9, 0x4

    invoke-direct {v1, v6, v8, v9, v5}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 649
    .restart local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v6, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 654
    .end local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    :goto_2
    :try_start_7
    iput-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->lastError:Ljava/lang/Throwable;

    .line 655
    invoke-static {}, Lorg/snmp4j/SNMP4JSettings;->isForwardRuntimeExceptions()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 656
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 651
    :catch_3
    move-exception v3

    .line 652
    .restart local v3    # "ex":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v6

    invoke-interface {v6, v3}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    goto :goto_2

    .line 660
    .end local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v5    # "iox":Ljava/io/IOException;
    :cond_2
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 661
    return-void
.end method

.method private readMessage(Ljava/nio/channels/SelectionKey;Ljava/nio/channels/SocketChannel;Lorg/snmp4j/smi/TcpAddress;)V
    .locals 17
    .param p1, "sk"    # Ljava/nio/channels/SelectionKey;
    .param p2, "readChannel"    # Ljava/nio/channels/SocketChannel;
    .param p3, "incomingAddress"    # Lorg/snmp4j/smi/TcpAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 947
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 948
    .local v8, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    if-eqz v8, :cond_2

    .line 950
    invoke-virtual {v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->used()V

    .line 951
    invoke-virtual {v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getReadBuffer()Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 952
    .local v12, "readBuffer":Ljava/nio/ByteBuffer;
    if-eqz v12, :cond_2

    .line 953
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    .line 954
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 955
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v15, 0x1

    invoke-virtual {v8, v14, v15}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 1044
    .end local v12    # "readBuffer":Ljava/nio/ByteBuffer;
    :cond_0
    :goto_0
    return-void

    .line 958
    .restart local v12    # "readBuffer":Ljava/nio/ByteBuffer;
    :cond_1
    const/4 v14, 0x0

    invoke-virtual {v8, v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->setReadBuffer(Ljava/nio/ByteBuffer;)V

    .line 959
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v14

    int-to-long v14, v14

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v12, v14, v15}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->dispatchMessage(Lorg/snmp4j/smi/TcpAddress;Ljava/nio/ByteBuffer;J)V

    goto :goto_0

    .line 964
    .end local v12    # "readBuffer":Ljava/nio/ByteBuffer;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->buf:[B

    invoke-static {v14}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 965
    .local v3, "byteBuffer":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$700(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/transport/MessageLengthDecoder;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/transport/MessageLengthDecoder;->getMinHeaderLength()I

    move-result v14

    invoke-virtual {v3, v14}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 966
    invoke-virtual/range {p2 .. p2}, Ljava/nio/channels/SocketChannel;->isOpen()Z

    move-result v14

    if-nez v14, :cond_3

    .line 967
    invoke-virtual/range {p1 .. p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 968
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 969
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Read channel not open, no bytes read from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    goto :goto_0

    .line 974
    :cond_3
    const-wide/16 v4, 0x0

    .line 976
    .local v4, "bytesRead":J
    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v14

    int-to-long v4, v14

    .line 977
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 978
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Reading header "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, " bytes from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 990
    :cond_4
    new-instance v10, Lorg/snmp4j/transport/MessageLength;

    const/4 v14, 0x0

    const/high16 v15, -0x80000000

    invoke-direct {v10, v14, v15}, Lorg/snmp4j/transport/MessageLength;-><init>(II)V

    .line 991
    .local v10, "messageLength":Lorg/snmp4j/transport/MessageLength;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$700(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/transport/MessageLengthDecoder;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/transport/MessageLengthDecoder;->getMinHeaderLength()I

    move-result v14

    int-to-long v14, v14

    cmp-long v14, v4, v14

    if-nez v14, :cond_9

    .line 992
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$700(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/transport/MessageLengthDecoder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->buf:[B

    invoke-static {v15}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/transport/MessageLengthDecoder;->getMessageLength(Ljava/nio/ByteBuffer;)Lorg/snmp4j/transport/MessageLength;

    move-result-object v10

    .line 994
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 995
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Message length is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 997
    :cond_5
    invoke-virtual {v10}, Lorg/snmp4j/transport/MessageLength;->getMessageLength()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v15}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->getMaxInboundMessageSize()I

    move-result v15

    if-gt v14, v15, :cond_6

    invoke-virtual {v10}, Lorg/snmp4j/transport/MessageLength;->getMessageLength()I

    move-result v14

    if-gtz v14, :cond_7

    .line 999
    :cond_6
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Received message length "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, " is greater than inboundBufferSize "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->getMaxInboundMessageSize()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 1002
    if-eqz v8, :cond_0

    .line 1003
    invoke-virtual {v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v13

    .line 1004
    .local v13, "s":Ljava/net/Socket;
    if-eqz v13, :cond_0

    .line 1005
    invoke-virtual {v13}, Ljava/net/Socket;->close()V

    .line 1006
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Socket to "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, " closed due to an error"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->info(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 982
    .end local v10    # "messageLength":Lorg/snmp4j/transport/MessageLength;
    .end local v13    # "s":Ljava/net/Socket;
    :catch_0
    move-exception v6

    .line 983
    .local v6, "ccex":Ljava/nio/channels/ClosedChannelException;
    invoke-virtual/range {p1 .. p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 984
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    invoke-interface {v14}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 985
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Read channel not open, no bytes read from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1012
    .end local v6    # "ccex":Ljava/nio/channels/ClosedChannelException;
    .restart local v10    # "messageLength":Lorg/snmp4j/transport/MessageLength;
    :cond_7
    invoke-virtual {v10}, Lorg/snmp4j/transport/MessageLength;->getMessageLength()I

    move-result v14

    invoke-virtual {v3, v14}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1013
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v4, v14

    .line 1014
    invoke-virtual {v10}, Lorg/snmp4j/transport/MessageLength;->getMessageLength()I

    move-result v14

    int-to-long v14, v14

    cmp-long v14, v4, v14

    if-nez v14, :cond_8

    .line 1015
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3, v4, v5}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->dispatchMessage(Lorg/snmp4j/smi/TcpAddress;Ljava/nio/ByteBuffer;J)V

    .line 1026
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v15, 0x1

    invoke-virtual {v8, v14, v15}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    goto/16 :goto_0

    .line 1018
    :cond_8
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v14

    new-array v9, v14, [B

    .line 1019
    .local v9, "message":[B
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v14

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v15

    sub-int v2, v14, v15

    .line 1020
    .local v2, "buflen":I
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1021
    const/4 v14, 0x0

    invoke-virtual {v3, v9, v14, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 1022
    invoke-static {v9}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 1023
    .local v11, "newBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1024
    invoke-virtual {v8, v11}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->setReadBuffer(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    .line 1029
    .end local v2    # "buflen":I
    .end local v9    # "message":[B
    .end local v11    # "newBuffer":Ljava/nio/ByteBuffer;
    :cond_9
    const-wide/16 v14, 0x0

    cmp-long v14, v4, v14

    if-gez v14, :cond_a

    .line 1030
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v14

    const-string v15, "Socket closed remotely"

    invoke-interface {v14, v15}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1031
    invoke-virtual/range {p1 .. p1}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 1032
    invoke-virtual/range {p2 .. p2}, Ljava/nio/channels/SocketChannel;->close()V

    .line 1033
    new-instance v7, Lorg/snmp4j/transport/TransportStateEvent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v15, 0x2

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-direct {v7, v14, v0, v15, v1}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 1039
    .local v7, "e":Lorg/snmp4j/transport/TransportStateEvent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v14, v7}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V

    goto/16 :goto_0

    .line 1042
    .end local v7    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v15, 0x1

    invoke-virtual {v8, v14, v15}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    goto/16 :goto_0
.end method

.method private writeData(Ljava/nio/channels/SelectionKey;Lorg/snmp4j/smi/TcpAddress;)Lorg/snmp4j/smi/TcpAddress;
    .locals 8
    .param p1, "sk"    # Ljava/nio/channels/SelectionKey;
    .param p2, "incomingAddress"    # Lorg/snmp4j/smi/TcpAddress;

    .prologue
    .line 905
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 907
    .local v1, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v4

    check-cast v4, Ljava/nio/channels/SocketChannel;

    .line 908
    .local v4, "sc":Ljava/nio/channels/SocketChannel;
    new-instance v2, Lorg/snmp4j/smi/TcpAddress;

    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Socket;->getPort()I

    move-result v6

    invoke-direct {v2, v5, v6}, Lorg/snmp4j/smi/TcpAddress;-><init>(Ljava/net/InetAddress;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 911
    .end local p2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .local v2, "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    :try_start_1
    invoke-virtual {v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->hasMessage()Z

    move-result v5

    if-nez v5, :cond_0

    .line 912
    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    monitor-enter v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 913
    :try_start_2
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 914
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v7, 0x4

    invoke-virtual {v1, v5, v7}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->removeRegistration(Ljava/nio/channels/Selector;I)V

    .line 915
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 917
    :cond_0
    if-eqz v1, :cond_1

    .line 918
    :try_start_3
    invoke-direct {p0, v1, v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->writeMessage(Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;Ljava/nio/channels/SocketChannel;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    move-object p2, v2

    .line 933
    .end local v2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .end local v4    # "sc":Ljava/nio/channels/SocketChannel;
    .restart local p2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    :goto_0
    return-object p2

    .line 915
    .end local p2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .restart local v2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .restart local v4    # "sc":Ljava/nio/channels/SocketChannel;
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 921
    :catch_0
    move-exception v3

    move-object p2, v2

    .line 922
    .end local v2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .end local v4    # "sc":Ljava/nio/channels/SocketChannel;
    .local v3, "iox":Ljava/io/IOException;
    .restart local p2    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    :goto_1
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v5

    invoke-interface {v5, v3}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 923
    new-instance v0, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v6, 0x2

    invoke-direct {v0, v5, p2, v6, v3}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 929
    .local v0, "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v5, v0}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V

    .line 931
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->closeChannel(Ljava/nio/channels/SelectableChannel;)V

    goto :goto_0

    .line 921
    .end local v0    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    .end local v3    # "iox":Ljava/io/IOException;
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private writeMessage(Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;Ljava/nio/channels/SocketChannel;)V
    .locals 5
    .param p1, "entry"    # Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .param p2, "sc"    # Ljava/nio/channels/SocketChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    .line 1070
    invoke-virtual {p1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->nextMessage()[B

    move-result-object v1

    .line 1071
    .local v1, "message":[B
    if-eqz v1, :cond_2

    .line 1072
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1073
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p2, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1074
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    invoke-interface {v2}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1075
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Send message with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    new-instance v4, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v4, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1080
    :cond_0
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 1092
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    :cond_1
    :goto_0
    return-void

    .line 1083
    :cond_2
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v2, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->removeRegistration(Ljava/nio/channels/Selector;I)V

    .line 1086
    invoke-virtual {p1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->hasMessage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->isRegistered(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1087
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v2, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 1088
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    const-string v3, "Waking up selector"

    invoke-interface {v2, v3}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1089
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v2}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 1095
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 1096
    iget-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$600(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/util/WorkerTask;

    move-result-object v0

    .line 1097
    .local v0, "st":Lorg/snmp4j/util/WorkerTask;
    if-eqz v0, :cond_0

    .line 1098
    invoke-interface {v0}, Lorg/snmp4j/util/WorkerTask;->terminate()V

    .line 1100
    :cond_0
    return-void
.end method

.method public getLastError()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->lastError:Ljava/lang/Throwable;

    return-object v0
.end method

.method public interrupt()V
    .locals 3

    .prologue
    .line 1116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 1117
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Interrupting worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1120
    :cond_0
    iget-object v0, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 1121
    return-void
.end method

.method public join()V
    .locals 3

    .prologue
    .line 1110
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1111
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Joining worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1113
    :cond_0
    return-void
.end method

.method public run()V
    .locals 15

    .prologue
    .line 742
    :goto_0
    :try_start_0
    iget-boolean v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v12, :cond_0

    .line 744
    :try_start_1
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v12}, Ljava/nio/channels/Selector;->select()I

    move-result v12

    if-lez v12, :cond_8

    .line 745
    iget-boolean v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v12, :cond_5

    .line 841
    :cond_0
    :try_start_2
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    if-eqz v12, :cond_1

    .line 842
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->ssc:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v12}, Ljava/nio/channels/ServerSocketChannel;->close()V

    .line 844
    :cond_1
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    if-eqz v12, :cond_2

    .line 845
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v12}, Ljava/nio/channels/Selector;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 852
    :cond_2
    :goto_1
    iget-boolean v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    if-nez v12, :cond_3

    .line 853
    const/4 v12, 0x1

    iput-boolean v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 854
    iget-object v13, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    monitor-enter v13

    .line 855
    :try_start_3
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v14, 0x0

    invoke-static {v12, v14}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$602(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/util/WorkerTask;)Lorg/snmp4j/util/WorkerTask;

    .line 856
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 858
    :cond_3
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    invoke-interface {v12}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 859
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Worker task finished: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 861
    :cond_4
    return-void

    .line 749
    :cond_5
    :try_start_4
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v12}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v9

    .line 750
    .local v9, "readyKeys":Ljava/util/Set;
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 753
    .local v5, "it":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v12

    if-eqz v12, :cond_8

    .line 755
    :try_start_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/nio/channels/SelectionKey;

    .line 756
    .local v11, "sk":Ljava/nio/channels/SelectionKey;
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 757
    const/4 v8, 0x0

    .line 758
    .local v8, "readChannel":Ljava/nio/channels/SocketChannel;
    const/4 v3, 0x0

    .line 759
    .local v3, "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z

    move-result v12

    if-eqz v12, :cond_9

    .line 760
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Key is acceptable"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 763
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v6

    check-cast v6, Ljava/nio/channels/ServerSocketChannel;

    .line 765
    .local v6, "nextReady":Ljava/nio/channels/ServerSocketChannel;
    invoke-virtual {v6}, Ljava/nio/channels/ServerSocketChannel;->accept()Ljava/nio/channels/SocketChannel;

    move-result-object v12

    invoke-virtual {v12}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v10

    .line 766
    .local v10, "s":Ljava/net/Socket;
    invoke-virtual {v10}, Ljava/net/Socket;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v8

    .line 767
    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 769
    new-instance v3, Lorg/snmp4j/smi/TcpAddress;

    .end local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    invoke-virtual {v10}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v12

    invoke-virtual {v10}, Ljava/net/Socket;->getPort()I

    move-result v13

    invoke-direct {v3, v12, v13}, Lorg/snmp4j/smi/TcpAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 771
    .restart local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    new-instance v2, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-direct {v2, v12, v3, v10}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;-><init>(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/smi/TcpAddress;Ljava/net/Socket;)V

    .line 772
    .local v2, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    const/4 v13, 0x1

    invoke-virtual {v2, v12, v13}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addRegistration(Ljava/nio/channels/Selector;I)V

    .line 773
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v12}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v12

    invoke-interface {v12, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v12, v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$500(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;)V

    .line 775
    new-instance v1, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-direct {v1, v12, v3, v13, v14}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 781
    .local v1, "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v12, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V

    .line 782
    invoke-virtual {v1}, Lorg/snmp4j/transport/TransportStateEvent;->isCancelled()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 783
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Incoming connection cancelled"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 784
    invoke-virtual {v10}, Ljava/net/Socket;->close()V

    .line 785
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v12}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v12

    invoke-interface {v12, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    const/4 v8, 0x0

    .line 805
    .end local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    .end local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v6    # "nextReady":Ljava/nio/channels/ServerSocketChannel;
    .end local v10    # "s":Ljava/net/Socket;
    :cond_7
    :goto_3
    if-eqz v8, :cond_6

    .line 806
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Key is reading"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 808
    :try_start_6
    invoke-direct {p0, v11, v8, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->readMessage(Ljava/nio/channels/SelectionKey;Ljava/nio/channels/SocketChannel;Lorg/snmp4j/smi/TcpAddress;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 810
    :catch_0
    move-exception v4

    .line 812
    .local v4, "iox":Ljava/io/IOException;
    :try_start_7
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    invoke-interface {v12, v4}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 813
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->cancel()V

    .line 814
    invoke-virtual {v8}, Ljava/nio/channels/SocketChannel;->close()V

    .line 815
    new-instance v1, Lorg/snmp4j/transport/TransportStateEvent;

    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    const/4 v13, 0x2

    invoke-direct {v1, v12, v3, v13, v4}, Lorg/snmp4j/transport/TransportStateEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/Address;ILjava/io/IOException;)V

    .line 821
    .restart local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    iget-object v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-virtual {v12, v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    :try_end_7
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_2

    .line 825
    .end local v1    # "e":Lorg/snmp4j/transport/TransportStateEvent;
    .end local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .end local v4    # "iox":Ljava/io/IOException;
    .end local v8    # "readChannel":Ljava/nio/channels/SocketChannel;
    .end local v11    # "sk":Ljava/nio/channels/SelectionKey;
    :catch_1
    move-exception v0

    .line 826
    .local v0, "ckex":Ljava/nio/channels/CancelledKeyException;
    :try_start_8
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    invoke-interface {v12}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 827
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Selection key cancelled, skipping it"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_2

    .line 833
    .end local v0    # "ckex":Ljava/nio/channels/CancelledKeyException;
    .end local v5    # "it":Ljava/util/Iterator;
    .end local v9    # "readyKeys":Ljava/util/Set;
    :catch_2
    move-exception v7

    .line 835
    .local v7, "npex":Ljava/lang/NullPointerException;
    :try_start_9
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 836
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "NullPointerException within select()?"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 837
    const/4 v12, 0x1

    iput-boolean v12, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 839
    .end local v7    # "npex":Ljava/lang/NullPointerException;
    :cond_8
    invoke-direct {p0}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->processPending()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 848
    :catch_3
    move-exception v4

    .line 849
    .restart local v4    # "iox":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    invoke-interface {v12, v4}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 850
    iput-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->lastError:Ljava/lang/Throwable;

    goto/16 :goto_1

    .line 789
    .end local v4    # "iox":Ljava/io/IOException;
    .restart local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .restart local v5    # "it":Ljava/util/Iterator;
    .restart local v8    # "readChannel":Ljava/nio/channels/SocketChannel;
    .restart local v9    # "readyKeys":Ljava/util/Set;
    .restart local v11    # "sk":Ljava/nio/channels/SelectionKey;
    :cond_9
    :try_start_a
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 790
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Key is writable"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 791
    invoke-direct {p0, v11, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->writeData(Ljava/nio/channels/SelectionKey;Lorg/snmp4j/smi/TcpAddress;)Lorg/snmp4j/smi/TcpAddress;

    move-result-object v3

    goto :goto_3

    .line 793
    :cond_a
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v12

    if-eqz v12, :cond_b

    .line 794
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Key is readable"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 795
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v8

    .end local v8    # "readChannel":Ljava/nio/channels/SocketChannel;
    check-cast v8, Ljava/nio/channels/SocketChannel;

    .line 796
    .restart local v8    # "readChannel":Ljava/nio/channels/SocketChannel;
    new-instance v3, Lorg/snmp4j/smi/TcpAddress;

    .end local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    invoke-virtual {v8}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v12

    invoke-virtual {v12}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v12

    invoke-virtual {v8}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v13

    invoke-virtual {v13}, Ljava/net/Socket;->getPort()I

    move-result v13

    invoke-direct {v3, v12, v13}, Lorg/snmp4j/smi/TcpAddress;-><init>(Ljava/net/InetAddress;I)V

    .restart local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    goto/16 :goto_3

    .line 800
    :cond_b
    invoke-virtual {v11}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 801
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v12

    const-string v13, "Key is connectable"

    invoke-interface {v12, v13}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 802
    invoke-direct {p0, v11, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->connectChannel(Ljava/nio/channels/SelectionKey;Lorg/snmp4j/smi/TcpAddress;)V
    :try_end_a
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_3

    .line 856
    .end local v3    # "incomingAddress":Lorg/snmp4j/smi/TcpAddress;
    .end local v5    # "it":Ljava/util/Iterator;
    .end local v8    # "readChannel":Ljava/nio/channels/SocketChannel;
    .end local v9    # "readyKeys":Ljava/util/Set;
    .end local v11    # "sk":Ljava/nio/channels/SelectionKey;
    :catchall_0
    move-exception v12

    :try_start_b
    monitor-exit v13
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    throw v12
.end method

.method public sendMessage(Lorg/snmp4j/smi/Address;[B)V
    .locals 10
    .param p1, "address"    # Lorg/snmp4j/smi/Address;
    .param p2, "message"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 670
    const/4 v4, 0x0

    .line 671
    .local v4, "s":Ljava/net/Socket;
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v7}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 672
    .local v1, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    invoke-interface {v7}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 673
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Looking up connection for destination \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\' returned: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 675
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    iget-object v8, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v8}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 677
    :cond_0
    if-eqz v1, :cond_1

    .line 678
    monitor-enter v1

    .line 679
    :try_start_0
    invoke-virtual {v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->used()V

    .line 680
    invoke-virtual {v1}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v4

    .line 681
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/net/Socket;->isClosed()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v4}, Ljava/net/Socket;->isConnected()Z

    move-result v7

    if-nez v7, :cond_7

    .line 684
    :cond_2
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    invoke-interface {v7}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 685
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Socket for address \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\' is closed, opening it..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 688
    :cond_3
    iget-object v8, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    monitor-enter v8

    .line 689
    :try_start_1
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 690
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 691
    const/4 v5, 0x0

    .line 693
    .local v5, "sc":Ljava/nio/channels/SocketChannel;
    :try_start_2
    new-instance v6, Ljava/net/InetSocketAddress;

    move-object v0, p1

    check-cast v0, Lorg/snmp4j/smi/TcpAddress;

    move-object v7, v0

    invoke-virtual {v7}, Lorg/snmp4j/smi/TcpAddress;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v8

    move-object v0, p1

    check-cast v0, Lorg/snmp4j/smi/TcpAddress;

    move-object v7, v0

    invoke-virtual {v7}, Lorg/snmp4j/smi/TcpAddress;->getPort()I

    move-result v7

    invoke-direct {v6, v8, v7}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 696
    .local v6, "targetAddress":Ljava/net/InetSocketAddress;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/net/Socket;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 698
    :cond_4
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v5

    .line 699
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 700
    invoke-virtual {v5, v6}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    .line 709
    :cond_5
    :goto_0
    invoke-virtual {v5}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v4

    .line 710
    new-instance v2, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    iget-object v8, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    move-object v0, p1

    check-cast v0, Lorg/snmp4j/smi/TcpAddress;

    move-object v7, v0

    invoke-direct {v2, v8, v7, v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;-><init>(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/smi/TcpAddress;Ljava/net/Socket;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 711
    .end local v1    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .local v2, "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_start_3
    invoke-virtual {v2, p2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addMessage([B)V

    .line 712
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v7}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 714
    iget-object v8, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    monitor-enter v8
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 715
    :try_start_4
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v7, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 716
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 718
    :try_start_5
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v7}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 719
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Trying to connect to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    move-object v1, v2

    .line 734
    .end local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v5    # "sc":Ljava/nio/channels/SocketChannel;
    .end local v6    # "targetAddress":Ljava/net/InetSocketAddress;
    .restart local v1    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :goto_1
    return-void

    .line 681
    :catchall_0
    move-exception v7

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v7

    .line 690
    :catchall_1
    move-exception v7

    :try_start_7
    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v7

    .line 703
    .restart local v5    # "sc":Ljava/nio/channels/SocketChannel;
    .restart local v6    # "targetAddress":Ljava/net/InetSocketAddress;
    :cond_6
    :try_start_8
    invoke-virtual {v4}, Ljava/net/Socket;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v5

    .line 704
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 705
    invoke-virtual {v5}, Ljava/nio/channels/SocketChannel;->isConnectionPending()Z

    move-result v7

    if-nez v7, :cond_5

    .line 706
    invoke-virtual {v5, v6}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    .line 721
    .end local v6    # "targetAddress":Ljava/net/InetSocketAddress;
    :catch_0
    move-exception v3

    .line 722
    .local v3, "iox":Ljava/io/IOException;
    :goto_2
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    invoke-interface {v7, v3}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 723
    throw v3

    .line 716
    .end local v1    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v3    # "iox":Ljava/io/IOException;
    .restart local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .restart local v6    # "targetAddress":Ljava/net/InetSocketAddress;
    :catchall_2
    move-exception v7

    :try_start_9
    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    throw v7
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    .line 721
    :catch_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .restart local v1    # "entry":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    goto :goto_2

    .line 727
    .end local v5    # "sc":Ljava/nio/channels/SocketChannel;
    .end local v6    # "targetAddress":Ljava/net/InetSocketAddress;
    :cond_7
    invoke-virtual {v1, p2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->addMessage([B)V

    .line 728
    iget-object v8, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    monitor-enter v8

    .line 729
    :try_start_b
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->pending:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 730
    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 731
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v7

    const-string v8, "Waking up selector for new message"

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 732
    iget-object v7, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v7}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    goto :goto_1

    .line 730
    :catchall_3
    move-exception v7

    :try_start_c
    monitor-exit v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v7
.end method

.method public terminate()V
    .locals 3

    .prologue
    .line 1103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$ServerThread;->stop:Z

    .line 1104
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1105
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Terminated worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 1107
    :cond_0
    return-void
.end method
