.class Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;
.super Ljava/util/TimerTask;
.source "DefaultTcpTransportMapping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/snmp4j/transport/DefaultTcpTransportMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SocketTimeout"
.end annotation


# instance fields
.field private entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

.field private final this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;


# direct methods
.method public constructor <init>(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;)V
    .locals 0
    .param p2, "entry"    # Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .prologue
    .line 519
    iput-object p1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 520
    iput-object p2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 521
    return-void
.end method

.method private rescheduleCleanup(J)V
    .locals 8
    .param p1, "now"    # J

    .prologue
    .line 556
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-virtual {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getLastUse()J

    move-result-wide v4

    sub-long v4, p1, v4

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$200(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 558
    .local v0, "nextRun":J
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    invoke-interface {v2}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 559
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Scheduling "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 561
    :cond_0
    iget-object v2, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v2}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$100(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/util/CommonTimer;

    move-result-object v2

    new-instance v3, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;

    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-direct {v3, v4, v5}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;-><init>(Lorg/snmp4j/transport/DefaultTcpTransportMapping;Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;)V

    invoke-interface {v2, v3, v0, v1}, Lorg/snmp4j/util/CommonTimer;->schedule(Ljava/util/TimerTask;J)V

    .line 562
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 2

    .prologue
    .line 565
    invoke-super {p0}, Ljava/util/TimerTask;->cancel()Z

    move-result v0

    .line 567
    .local v0, "result":Z
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 568
    return v0
.end method

.method public run()V
    .locals 8

    .prologue
    const-wide/32 v6, 0xf4240

    .line 524
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 525
    .local v2, "now":J
    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$100(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Lorg/snmp4j/util/CommonTimer;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-virtual {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getLastUse()J

    move-result-wide v4

    sub-long v4, v2, v4

    div-long/2addr v4, v6

    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v6}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$200(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 527
    :cond_0
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v4

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 528
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Socket has not been used for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-virtual {v6}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getLastUse()J

    move-result-wide v6

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " milliseconds, closing it"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 532
    :cond_1
    iget-object v0, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    .line 534
    .local v0, "entryCopy":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :try_start_0
    monitor-enter v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 535
    :try_start_1
    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-virtual {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getLastUse()J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/32 v6, 0xf4240

    div-long/2addr v4, v6

    iget-object v6, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v6}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$200(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 536
    iget-object v4, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->this$0:Lorg/snmp4j/transport/DefaultTcpTransportMapping;

    invoke-static {v4}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$300(Lorg/snmp4j/transport/DefaultTcpTransportMapping;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, p0, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->entry:Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;

    invoke-virtual {v5}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    invoke-virtual {v0}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getSocket()Ljava/net/Socket;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Socket;->close()V

    .line 538
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Socket to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;->getPeerAddress()Lorg/snmp4j/smi/TcpAddress;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " closed due to timeout"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/snmp4j/log/LogAdapter;->info(Ljava/lang/Object;)V

    .line 544
    :goto_0
    monitor-exit v0

    .line 553
    .end local v0    # "entryCopy":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :goto_1
    return-void

    .line 542
    .restart local v0    # "entryCopy":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    :cond_2
    invoke-direct {p0, v2, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->rescheduleCleanup(J)V

    goto :goto_0

    .line 544
    :catchall_0
    move-exception v4

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 546
    :catch_0
    move-exception v1

    .line 547
    .local v1, "ex":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultTcpTransportMapping;->access$000()Lorg/snmp4j/log/LogAdapter;

    move-result-object v4

    invoke-interface {v4, v1}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    goto :goto_1

    .line 551
    .end local v0    # "entryCopy":Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketEntry;
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_3
    invoke-direct {p0, v2, v3}, Lorg/snmp4j/transport/DefaultTcpTransportMapping$SocketTimeout;->rescheduleCleanup(J)V

    goto :goto_1
.end method
