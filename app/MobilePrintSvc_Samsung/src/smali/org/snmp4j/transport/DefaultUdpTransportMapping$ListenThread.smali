.class Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;
.super Ljava/lang/Object;
.source "DefaultUdpTransportMapping.java"

# interfaces
.implements Lorg/snmp4j/util/WorkerTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/snmp4j/transport/DefaultUdpTransportMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListenThread"
.end annotation


# instance fields
.field private buf:[B

.field private volatile stop:Z

.field private final this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;


# direct methods
.method public constructor <init>(Lorg/snmp4j/transport/DefaultUdpTransportMapping;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 313
    iput-object p1, p0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    .line 314
    invoke-virtual {p1}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->getMaxInboundMessageSize()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->buf:[B

    .line 315
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    .line 423
    return-void
.end method

.method public interrupt()V
    .locals 3

    .prologue
    .line 439
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Interrupting worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 442
    :cond_0
    invoke-virtual {p0}, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->close()V

    .line 443
    return-void
.end method

.method public join()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 433
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Joining worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 436
    :cond_0
    return-void
.end method

.method public run()V
    .locals 17

    .prologue
    .line 318
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget-object v9, v11, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->socket:Ljava/net/DatagramSocket;

    .line 319
    .local v9, "socketCopy":Ljava/net/DatagramSocket;
    if-eqz v9, :cond_1

    .line 321
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-virtual {v11}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->getSocketTimeout()I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 322
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-static {v11}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$000(Lorg/snmp4j/transport/DefaultUdpTransportMapping;)I

    move-result v11

    if-lez v11, :cond_0

    .line 323
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-static {v11}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$000(Lorg/snmp4j/transport/DefaultUdpTransportMapping;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget v12, v12, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->maxInboundMessageSize:I

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual {v9, v11}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V

    .line 326
    :cond_0
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 327
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "UDP receive buffer size for socket "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-virtual {v13}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->getAddress()Lorg/snmp4j/smi/UdpAddress;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, " is set to: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v9}, Ljava/net/DatagramSocket;->getReceiveBufferSize()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2

    .line 336
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    if-nez v11, :cond_8

    .line 337
    new-instance v7, Ljava/net/DatagramPacket;

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->buf:[B

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->buf:[B

    array-length v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget-object v13, v13, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->udpAddress:Lorg/snmp4j/smi/UdpAddress;

    invoke-virtual {v13}, Lorg/snmp4j/smi/UdpAddress;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget-object v14, v14, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->udpAddress:Lorg/snmp4j/smi/UdpAddress;

    invoke-virtual {v14}, Lorg/snmp4j/smi/UdpAddress;->getPort()I

    move-result v14

    invoke-direct {v7, v11, v12, v13, v14}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 342
    .local v7, "packet":Ljava/net/DatagramPacket;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget-object v9, v11, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->socket:Ljava/net/DatagramSocket;

    .line 343
    if-nez v9, :cond_3

    .line 344
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/PortUnreachableException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 349
    :catch_0
    move-exception v5

    .line 350
    .local v5, "iiox":Ljava/io/InterruptedIOException;
    :try_start_2
    iget v11, v5, Ljava/io/InterruptedIOException;->bytesTransferred:I

    if-lez v11, :cond_1

    .line 354
    .end local v5    # "iiox":Ljava/io/InterruptedIOException;
    :goto_1
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 355
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Received message from "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getPort()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, " with length "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getLength()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    new-instance v13, Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getLength()I

    move-result v16

    invoke-direct/range {v13 .. v16}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    invoke-virtual {v13}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 364
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    invoke-virtual {v11}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->isAsyncMsgProcessingSupported()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 365
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getLength()I

    move-result v11

    new-array v2, v11, [B

    .line 366
    .local v2, "bytes":[B
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    array-length v14, v2

    invoke-static {v11, v12, v2, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 367
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 372
    .end local v2    # "bytes":[B
    .local v1, "bis":Ljava/nio/ByteBuffer;
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    new-instance v12, Lorg/snmp4j/smi/UdpAddress;

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v13

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getPort()I

    move-result v14

    invoke-direct {v12, v13, v14}, Lorg/snmp4j/smi/UdpAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v11, v12, v1}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->fireProcessMessage(Lorg/snmp4j/smi/Address;Ljava/nio/ByteBuffer;)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/PortUnreachableException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    .line 375
    .end local v1    # "bis":Ljava/nio/ByteBuffer;
    :catch_1
    move-exception v11

    goto/16 :goto_0

    .line 331
    .end local v7    # "packet":Ljava/net/DatagramPacket;
    :catch_2
    move-exception v4

    .line 332
    .local v4, "ex":Ljava/net/SocketException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11, v4}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 333
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->setSocketTimeout(I)V

    goto/16 :goto_0

    .line 347
    .end local v4    # "ex":Ljava/net/SocketException;
    .restart local v7    # "packet":Ljava/net/DatagramPacket;
    :cond_3
    :try_start_3
    invoke-virtual {v9, v7}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_3
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/PortUnreachableException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_1

    .line 378
    :catch_3
    move-exception v8

    .line 379
    .local v8, "purex":Ljava/net/PortUnreachableException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    monitor-enter v12

    .line 380
    :try_start_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    const/4 v13, 0x0

    iput-object v13, v11, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->listener:Lorg/snmp4j/util/WorkerTask;

    .line 381
    monitor-exit v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 382
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11, v8}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 383
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 384
    invoke-virtual {v8}, Ljava/net/PortUnreachableException;->printStackTrace()V

    .line 386
    :cond_4
    invoke-static {}, Lorg/snmp4j/SNMP4JSettings;->isForwardRuntimeExceptions()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 387
    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    .line 370
    .end local v8    # "purex":Ljava/net/PortUnreachableException;
    :cond_5
    :try_start_5
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v11

    invoke-static {v11}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/PortUnreachableException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    move-result-object v1

    .restart local v1    # "bis":Ljava/nio/ByteBuffer;
    goto :goto_2

    .line 381
    .end local v1    # "bis":Ljava/nio/ByteBuffer;
    .restart local v8    # "purex":Ljava/net/PortUnreachableException;
    :catchall_0
    move-exception v11

    :try_start_6
    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v11

    .line 391
    .end local v8    # "purex":Ljava/net/PortUnreachableException;
    :catch_4
    move-exception v10

    .line 392
    .local v10, "soex":Ljava/net/SocketException;
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    if-nez v11, :cond_6

    .line 393
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Socket for transport mapping "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, " error: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v10}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12, v10}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 396
    :cond_6
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    goto/16 :goto_0

    .line 398
    .end local v10    # "soex":Ljava/net/SocketException;
    :catch_5
    move-exception v6

    .line 399
    .local v6, "iox":Ljava/io/IOException;
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11, v6}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 400
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 401
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 403
    :cond_7
    invoke-static {}, Lorg/snmp4j/SNMP4JSettings;->isForwardRuntimeExceptions()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 404
    new-instance v11, Ljava/lang/RuntimeException;

    invoke-direct {v11, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    .line 408
    .end local v6    # "iox":Ljava/io/IOException;
    .end local v7    # "packet":Ljava/net/DatagramPacket;
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    monitor-enter v12

    .line 409
    :try_start_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    const/4 v13, 0x0

    iput-object v13, v11, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->listener:Lorg/snmp4j/util/WorkerTask;

    .line 410
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->stop:Z

    .line 411
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->this$0:Lorg/snmp4j/transport/DefaultUdpTransportMapping;

    iget-object v3, v11, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->socket:Ljava/net/DatagramSocket;

    .line 412
    .local v3, "closingSocket":Ljava/net/DatagramSocket;
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Ljava/net/DatagramSocket;->isClosed()Z

    move-result v11

    if-nez v11, :cond_9

    .line 413
    invoke-virtual {v3}, Ljava/net/DatagramSocket;->close()V

    .line 415
    :cond_9
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 416
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    invoke-interface {v11}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 417
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Worker task stopped:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 419
    :cond_a
    return-void

    .line 415
    .end local v3    # "closingSocket":Ljava/net/DatagramSocket;
    :catchall_1
    move-exception v11

    :try_start_8
    monitor-exit v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v11
.end method

.method public terminate()V
    .locals 3

    .prologue
    .line 426
    invoke-virtual {p0}, Lorg/snmp4j/transport/DefaultUdpTransportMapping$ListenThread;->close()V

    .line 427
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    invoke-static {}, Lorg/snmp4j/transport/DefaultUdpTransportMapping;->access$100()Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Terminated worker task: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 430
    :cond_0
    return-void
.end method
