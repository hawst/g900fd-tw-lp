.class public abstract Lorg/snmp4j/transport/TcpTransportMapping;
.super Lorg/snmp4j/transport/AbstractTransportMapping;
.source "TcpTransportMapping.java"

# interfaces
.implements Lorg/snmp4j/transport/ConnectionOrientedTransportMapping;


# static fields
.field static class$org$snmp4j$smi$TcpAddress:Ljava/lang/Class;

.field static class$org$snmp4j$transport$TcpTransportMapping:Ljava/lang/Class;

.field private static final logger:Lorg/snmp4j/log/LogAdapter;


# instance fields
.field protected tcpAddress:Lorg/snmp4j/smi/TcpAddress;

.field private transient transportStateListeners:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$transport$TcpTransportMapping:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.transport.TcpTransportMapping"

    invoke-static {v0}, Lorg/snmp4j/transport/TcpTransportMapping;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$transport$TcpTransportMapping:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->logger:Lorg/snmp4j/log/LogAdapter;

    return-void

    :cond_0
    sget-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$transport$TcpTransportMapping:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/snmp4j/smi/TcpAddress;)V
    .locals 0
    .param p1, "tcpAddress"    # Lorg/snmp4j/smi/TcpAddress;

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/snmp4j/transport/AbstractTransportMapping;-><init>()V

    .line 53
    iput-object p1, p0, Lorg/snmp4j/transport/TcpTransportMapping;->tcpAddress:Lorg/snmp4j/smi/TcpAddress;

    .line 54
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 47
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public declared-synchronized addTransportStateListener(Lorg/snmp4j/transport/TransportStateListener;)V
    .locals 2
    .param p1, "l"    # Lorg/snmp4j/transport/TransportStateListener;

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    .line 117
    :cond_0
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected fireConnectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    .locals 8
    .param p1, "change"    # Lorg/snmp4j/transport/TransportStateEvent;

    .prologue
    .line 128
    sget-object v5, Lorg/snmp4j/transport/TcpTransportMapping;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 129
    sget-object v5, Lorg/snmp4j/transport/TcpTransportMapping;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Firing transport state event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 131
    :cond_0
    iget-object v3, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    .line 132
    .local v3, "listenersFinalRef":Ljava/util/List;
    if-eqz v3, :cond_1

    .line 135
    :try_start_0
    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 137
    .local v2, "listeners":Ljava/util/List;
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    :try_start_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 139
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 140
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/snmp4j/transport/TransportStateListener;

    invoke-interface {v5, p1}, Lorg/snmp4j/transport/TransportStateListener;->connectionStateChanged(Lorg/snmp4j/transport/TransportStateEvent;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "listeners":Ljava/util/List;
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    .line 144
    :catch_0
    move-exception v4

    .line 145
    .local v4, "rex":Ljava/lang/RuntimeException;
    sget-object v5, Lorg/snmp4j/transport/TcpTransportMapping;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Runtime exception in fireConnectionStateChanged:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 147
    invoke-static {}, Lorg/snmp4j/SNMP4JSettings;->isForwardRuntimeExceptions()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 148
    throw v4

    .line 152
    .end local v4    # "rex":Ljava/lang/RuntimeException;
    :cond_1
    return-void
.end method

.method public getAddress()Lorg/snmp4j/smi/TcpAddress;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->tcpAddress:Lorg/snmp4j/smi/TcpAddress;

    return-object v0
.end method

.method public getListenAddress()Lorg/snmp4j/smi/Address;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->tcpAddress:Lorg/snmp4j/smi/TcpAddress;

    return-object v0
.end method

.method public abstract getMessageLengthDecoder()Lorg/snmp4j/transport/MessageLengthDecoder;
.end method

.method public getSupportedAddressClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$smi$TcpAddress:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.smi.TcpAddress"

    invoke-static {v0}, Lorg/snmp4j/transport/TcpTransportMapping;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$smi$TcpAddress:Ljava/lang/Class;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/snmp4j/transport/TcpTransportMapping;->class$org$snmp4j$smi$TcpAddress:Ljava/lang/Class;

    goto :goto_0
.end method

.method public abstract listen()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public declared-synchronized removeTransportStateListener(Lorg/snmp4j/transport/TransportStateListener;)V
    .locals 1
    .param p1, "l"    # Lorg/snmp4j/transport/TransportStateListener;

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lorg/snmp4j/transport/TcpTransportMapping;->transportStateListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :cond_0
    monitor-exit p0

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract sendMessage(Lorg/snmp4j/smi/Address;[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setConnectionTimeout(J)V
.end method

.method public abstract setMessageLengthDecoder(Lorg/snmp4j/transport/MessageLengthDecoder;)V
.end method
