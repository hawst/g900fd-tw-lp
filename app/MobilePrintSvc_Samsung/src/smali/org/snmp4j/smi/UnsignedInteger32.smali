.class public Lorg/snmp4j/smi/UnsignedInteger32;
.super Lorg/snmp4j/smi/AbstractVariable;
.source "UnsignedInteger32.java"

# interfaces
.implements Lorg/snmp4j/smi/AssignableFromLong;
.implements Lorg/snmp4j/smi/AssignableFromString;


# static fields
.field private static final serialVersionUID:J = -0x1de965a4dae9500fL


# instance fields
.field protected value:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    .line 42
    return-void
.end method

.method public constructor <init>(B)V
    .locals 2
    .param p1, "signedByteValue"    # B

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    .line 75
    and-int/lit16 v0, p1, 0xff

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 76
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "signedIntValue"    # I

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    .line 64
    int-to-long v0, p1

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 65
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 52
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    .line 53
    invoke-virtual {p0, p1, p2}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 54
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Lorg/snmp4j/smi/UnsignedInteger32;

    iget-wide v1, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    invoke-direct {v0, v1, v2}, Lorg/snmp4j/smi/UnsignedInteger32;-><init>(J)V

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v6, 0x0

    .line 124
    iget-wide v2, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    check-cast p1, Lorg/snmp4j/smi/UnsignedInteger32;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p1}, Lorg/snmp4j/smi/UnsignedInteger32;->getValue()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 125
    .local v0, "diff":J
    cmp-long v2, v0, v6

    if-gez v2, :cond_0

    .line 126
    const/4 v2, -0x1

    .line 131
    :goto_0
    return v2

    .line 128
    :cond_0
    cmp-long v2, v0, v6

    if-lez v2, :cond_1

    .line 129
    const/4 v2, 0x1

    goto :goto_0

    .line 131
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public decodeBER(Lorg/snmp4j/asn1/BERInputStream;)V
    .locals 6
    .param p1, "inputStream"    # Lorg/snmp4j/asn1/BERInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v2, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v2}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    .line 84
    .local v2, "type":Lorg/snmp4j/asn1/BER$MutableByte;
    invoke-static {p1, v2}, Lorg/snmp4j/asn1/BER;->decodeUnsignedInteger(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)J

    move-result-wide v0

    .line 85
    .local v0, "newValue":J
    invoke-virtual {v2}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v3

    const/16 v4, 0x42

    if-eq v3, v4, :cond_0

    .line 86
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Wrong type encountered when decoding Gauge: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v2}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 89
    :cond_0
    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 90
    return-void
.end method

.method public encodeBER(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    const/16 v0, 0x42

    iget-wide v1, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    invoke-static {p1, v0, v1, v2}, Lorg/snmp4j/asn1/BER;->encodeUnsignedInteger(Ljava/io/OutputStream;BJ)V

    .line 80
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 117
    instance-of v1, p1, Lorg/snmp4j/smi/UnsignedInteger32;

    if-eqz v1, :cond_0

    .line 118
    check-cast p1, Lorg/snmp4j/smi/UnsignedInteger32;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v1, p1, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    iget-wide v3, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 120
    :cond_0
    return v0
.end method

.method public fromSubIndex(Lorg/snmp4j/smi/OID;Z)V
    .locals 2
    .param p1, "subIndex"    # Lorg/snmp4j/smi/OID;
    .param p2, "impliedLength"    # Z

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/snmp4j/smi/OID;->getUnsigned(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 172
    return-void
.end method

.method public getBERLength()I
    .locals 4

    .prologue
    .line 101
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 102
    const/4 v0, 0x3

    .line 113
    :goto_0
    return v0

    .line 104
    :cond_0
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    const-wide/32 v2, 0x8000

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 105
    const/4 v0, 0x4

    goto :goto_0

    .line 107
    :cond_1
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    const-wide/32 v2, 0x800000

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 108
    const/4 v0, 0x5

    goto :goto_0

    .line 110
    :cond_2
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    const-wide v2, 0x80000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 111
    const/4 v0, 0x6

    goto :goto_0

    .line 113
    :cond_3
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public getSyntax()I
    .locals 1

    .prologue
    .line 93
    const/16 v0, 0x42

    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 151
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    long-to-int v0, v0

    return v0
.end method

.method public setValue(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 143
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0xffffffffL

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 144
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument must be an unsigned 32bit value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_1
    iput-wide p1, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    .line 148
    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/UnsignedInteger32;->setValue(J)V

    .line 140
    return-void
.end method

.method public final toInt()I
    .locals 2

    .prologue
    .line 159
    invoke-virtual {p0}, Lorg/snmp4j/smi/UnsignedInteger32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final toLong()J
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/snmp4j/smi/UnsignedInteger32;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lorg/snmp4j/smi/UnsignedInteger32;->value:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toSubIndex(Z)Lorg/snmp4j/smi/OID;
    .locals 4
    .param p1, "impliedLength"    # Z

    .prologue
    .line 167
    new-instance v0, Lorg/snmp4j/smi/OID;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/snmp4j/smi/UnsignedInteger32;->toInt()I

    move-result v3

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>([I)V

    return-object v0
.end method
