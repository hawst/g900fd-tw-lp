.class public final Lorg/snmp4j/smi/SMIConstants;
.super Ljava/lang/Object;
.source "SMIConstants.java"


# static fields
.field public static final EXCEPTION_END_OF_MIB_VIEW:I = 0x82

.field public static final EXCEPTION_NO_SUCH_INSTANCE:I = 0x81

.field public static final EXCEPTION_NO_SUCH_OBJECT:I = 0x80

.field public static final SYNTAX_BITS:I = 0x4

.field public static final SYNTAX_COUNTER32:I = 0x41

.field public static final SYNTAX_COUNTER64:I = 0x46

.field public static final SYNTAX_GAUGE32:I = 0x42

.field public static final SYNTAX_INTEGER:I = 0x2

.field public static final SYNTAX_INTEGER32:I = 0x2

.field public static final SYNTAX_IPADDRESS:I = 0x40

.field public static final SYNTAX_NULL:I = 0x5

.field public static final SYNTAX_OBJECT_IDENTIFIER:I = 0x6

.field public static final SYNTAX_OCTET_STRING:I = 0x4

.field public static final SYNTAX_OPAQUE:I = 0x44

.field public static final SYNTAX_TIMETICKS:I = 0x43

.field public static final SYNTAX_UNSIGNED_INTEGER32:I = 0x42


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
