.class public Lorg/snmp4j/smi/OctetString;
.super Lorg/snmp4j/smi/AbstractVariable;
.source "OctetString.java"

# interfaces
.implements Lorg/snmp4j/smi/AssignableFromByteArray;
.implements Lorg/snmp4j/smi/AssignableFromString;


# static fields
.field private static final DEFAULT_HEX_DELIMITER:C = ':'

.field private static final serialVersionUID:J = 0x39414b0344051ea1L


# instance fields
.field private value:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 42
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 42
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 81
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/snmp4j/smi/OctetString;)V
    .locals 2
    .param p1, "other"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 42
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 91
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 92
    invoke-virtual {p0, p1}, Lorg/snmp4j/smi/OctetString;->append(Lorg/snmp4j/smi/OctetString;)V

    .line 93
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "rawValue"    # [B

    .prologue
    .line 56
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    .line 57
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "rawValue"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 42
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 70
    new-array v0, p3, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 71
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    return-void
.end method

.method public static fromByteArray([B)Lorg/snmp4j/smi/OctetString;
    .locals 1
    .param p0, "value"    # [B

    .prologue
    .line 568
    if-nez p0, :cond_0

    .line 569
    const/4 v0, 0x0

    .line 571
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v0, p0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    goto :goto_0
.end method

.method public static fromHexString(Ljava/lang/String;)Lorg/snmp4j/smi/OctetString;
    .locals 1
    .param p0, "hexString"    # Ljava/lang/String;

    .prologue
    .line 311
    const/16 v0, 0x3a

    invoke-static {p0, v0}, Lorg/snmp4j/smi/OctetString;->fromHexString(Ljava/lang/String;C)Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    return-object v0
.end method

.method public static fromHexString(Ljava/lang/String;C)Lorg/snmp4j/smi/OctetString;
    .locals 1
    .param p0, "hexString"    # Ljava/lang/String;
    .param p1, "delimiter"    # C

    .prologue
    .line 315
    const/16 v0, 0x10

    invoke-static {p0, p1, v0}, Lorg/snmp4j/smi/OctetString;->fromString(Ljava/lang/String;CI)Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;CI)Lorg/snmp4j/smi/OctetString;
    .locals 6
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "delimiter"    # C
    .param p2, "radix"    # I

    .prologue
    .line 320
    const-string v0, ""

    .line 321
    .local v0, "delim":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 322
    new-instance v3, Ljava/util/StringTokenizer;

    invoke-direct {v3, p0, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    new-array v4, v5, [B

    .line 324
    .local v4, "value":[B
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 325
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, "s":Ljava/lang/String;
    invoke-static {v2, p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    .end local v2    # "s":Ljava/lang/String;
    :cond_0
    new-instance v5, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v5, v4}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    return-object v5
.end method

.method public static fromString(Ljava/lang/String;I)Lorg/snmp4j/smi/OctetString;
    .locals 8
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "radix"    # I

    .prologue
    .line 343
    const-wide/high16 v4, 0x4070000000000000L    # 256.0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v4, v4

    int-to-double v6, p1

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    .line 344
    .local v0, "digits":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    div-int/2addr v4, v0

    new-array v3, v4, [B

    .line 345
    .local v3, "value":[B
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 346
    add-int v4, v1, v0

    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 347
    .local v2, "s":Ljava/lang/String;
    div-int v4, v1, v0

    invoke-static {v2, p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 345
    add-int/2addr v1, v0

    goto :goto_0

    .line 349
    .end local v2    # "s":Ljava/lang/String;
    :cond_0
    new-instance v4, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v4, v3}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    return-object v4
.end method

.method public static final split(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Ljava/util/Collection;
    .locals 11
    .param p0, "octetString"    # Lorg/snmp4j/smi/OctetString;
    .param p1, "delimOctets"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 522
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 523
    .local v6, "parts":Ljava/util/List;
    const/4 v5, -0x1

    .line 524
    .local v5, "maxDelim":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 525
    invoke-virtual {p1, v2}, Lorg/snmp4j/smi/OctetString;->get(I)B

    move-result v8

    and-int/lit16 v1, v8, 0xff

    .line 526
    .local v1, "delim":I
    if-le v1, v5, :cond_0

    .line 527
    move v5, v1

    .line 524
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 530
    .end local v1    # "delim":I
    :cond_1
    const/4 v7, 0x0

    .line 531
    .local v7, "startPos":I
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v8

    if-ge v2, v8, :cond_6

    .line 532
    iget-object v8, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v8, v8, v2

    and-int/lit16 v0, v8, 0xff

    .line 533
    .local v0, "c":I
    const/4 v3, 0x0

    .line 534
    .local v3, "isDelim":Z
    if-gt v0, v5, :cond_4

    .line 535
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v8

    if-ge v4, v8, :cond_4

    .line 536
    invoke-virtual {p1, v4}, Lorg/snmp4j/smi/OctetString;->get(I)B

    move-result v8

    and-int/lit16 v8, v8, 0xff

    if-ne v0, v8, :cond_3

    .line 537
    if-ltz v7, :cond_2

    if-le v2, v7, :cond_2

    .line 538
    new-instance v8, Lorg/snmp4j/smi/OctetString;

    iget-object v9, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    sub-int v10, v2, v7

    invoke-direct {v8, v9, v7, v10}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    :cond_2
    const/4 v7, -0x1

    .line 542
    const/4 v3, 0x1

    .line 535
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 546
    .end local v4    # "j":I
    :cond_4
    if-nez v3, :cond_5

    if-gez v7, :cond_5

    .line 547
    move v7, v2

    .line 531
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 550
    .end local v0    # "c":I
    .end local v3    # "isDelim":Z
    :cond_6
    if-ltz v7, :cond_7

    .line 551
    new-instance v8, Lorg/snmp4j/smi/OctetString;

    iget-object v9, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v10

    sub-int/2addr v10, v7

    invoke-direct {v8, v9, v7, v10}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    :cond_7
    return-object v6
.end method


# virtual methods
.method public append(B)V
    .locals 4
    .param p1, "b"    # B

    .prologue
    const/4 v3, 0x0

    .line 101
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [B

    .line 102
    .local v0, "newValue":[B
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v1, v1

    aput-byte p1, v0, v1

    .line 104
    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 105
    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/OctetString;->append([B)V

    .line 136
    return-void
.end method

.method public append(Lorg/snmp4j/smi/OctetString;)V
    .locals 1
    .param p1, "octetString"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 125
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/OctetString;->append([B)V

    .line 126
    return-void
.end method

.method public append([B)V
    .locals 4
    .param p1, "bytes"    # [B

    .prologue
    const/4 v3, 0x0

    .line 113
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v1, v1

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v0, v1, [B

    .line 114
    .local v0, "newValue":[B
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v1, v1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 117
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 143
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 444
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 212
    instance-of v3, p1, Lorg/snmp4j/smi/OctetString;

    if-eqz v3, :cond_3

    move-object v2, p1

    .line 213
    check-cast v2, Lorg/snmp4j/smi/OctetString;

    .line 214
    .local v2, "other":Lorg/snmp4j/smi/OctetString;
    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v3, v3

    iget-object v4, v2, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 215
    .local v1, "maxlen":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 216
    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v3, v3, v0

    iget-object v4, v2, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_1

    .line 217
    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    iget-object v4, v2, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v4, v4, v0

    and-int/lit16 v4, v4, 0xff

    if-ge v3, v4, :cond_0

    .line 218
    const/4 v3, -0x1

    .line 225
    :goto_1
    return v3

    .line 221
    :cond_0
    const/4 v3, 0x1

    goto :goto_1

    .line 215
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 225
    :cond_2
    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v3, v3

    iget-object v4, v2, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    goto :goto_1

    .line 227
    .end local v0    # "i":I
    .end local v1    # "maxlen":I
    .end local v2    # "other":Lorg/snmp4j/smi/OctetString;
    :cond_3
    new-instance v3, Ljava/lang/ClassCastException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public decodeBER(Lorg/snmp4j/asn1/BERInputStream;)V
    .locals 5
    .param p1, "inputStream"    # Lorg/snmp4j/asn1/BERInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v0}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    .line 151
    .local v0, "type":Lorg/snmp4j/asn1/BER$MutableByte;
    invoke-static {p1, v0}, Lorg/snmp4j/asn1/BER;->decodeString(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)[B

    move-result-object v1

    .line 152
    .local v1, "v":[B
    invoke-virtual {v0}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 153
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Wrong type encountered when decoding OctetString: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v0}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 156
    :cond_0
    invoke-virtual {p0, v1}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 157
    return-void
.end method

.method public encodeBER(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x4

    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v1

    invoke-static {p1, v0, v1}, Lorg/snmp4j/asn1/BER;->encodeString(Ljava/io/OutputStream;B[B)V

    .line 147
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 201
    instance-of v1, p1, Lorg/snmp4j/smi/OctetString;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 202
    check-cast v0, Lorg/snmp4j/smi/OctetString;

    .line 203
    .local v0, "other":Lorg/snmp4j/smi/OctetString;
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    iget-object v2, v0, Lorg/snmp4j/smi/OctetString;->value:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    .line 208
    .end local v0    # "other":Lorg/snmp4j/smi/OctetString;
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 205
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, [B

    if-eqz v1, :cond_1

    .line 206
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    check-cast p1, [B

    .end local p1    # "o":Ljava/lang/Object;
    check-cast p1, [B

    invoke-static {v1, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0

    .line 208
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public fromSubIndex(Lorg/snmp4j/smi/OID;Z)V
    .locals 4
    .param p1, "subIndex"    # Lorg/snmp4j/smi/OID;
    .param p2, "impliedLength"    # Z

    .prologue
    .line 501
    if-eqz p2, :cond_0

    .line 502
    invoke-virtual {p1}, Lorg/snmp4j/smi/OID;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 508
    :goto_0
    return-void

    .line 505
    :cond_0
    new-instance v0, Lorg/snmp4j/smi/OID;

    invoke-virtual {p1}, Lorg/snmp4j/smi/OID;->getValue()[I

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1}, Lorg/snmp4j/smi/OID;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lorg/snmp4j/smi/OID;-><init>([III)V

    .line 506
    .local v0, "suffix":Lorg/snmp4j/smi/OID;
    invoke-virtual {v0}, Lorg/snmp4j/smi/OID;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    goto :goto_0
.end method

.method public final get(I)B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public getBERLength()I
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v0, v0

    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v1, v1

    invoke-static {v1}, Lorg/snmp4j/asn1/BER;->getBERLengthOfLength(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getBERPayloadLength()I
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v0, v0

    return v0
.end method

.method public getSyntax()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x4

    return v0
.end method

.method public getValue()[B
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 194
    .local v0, "hash":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 195
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v2, v2, v1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v1

    xor-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    :cond_0
    return v0
.end method

.method public isPrintable()Z
    .locals 4

    .prologue
    .line 284
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 285
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v2, v2, v1

    int-to-char v0, v2

    .line 286
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v2

    if-nez v2, :cond_0

    and-int/lit16 v2, v0, 0xff

    const/16 v3, 0x80

    if-lt v2, v3, :cond_2

    :cond_0
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_1

    and-int/lit16 v2, v0, 0xff

    const/16 v3, 0x1c

    if-lt v2, v3, :cond_2

    and-int/lit16 v2, v0, 0xff

    const/16 v3, 0x1f

    if-gt v2, v3, :cond_2

    .line 289
    :cond_1
    const/4 v2, 0x0

    .line 292
    .end local v0    # "c":C
    :goto_1
    return v2

    .line 284
    .restart local v0    # "c":C
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 292
    .end local v0    # "c":C
    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v0, v0

    return v0
.end method

.method public mask(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/smi/OctetString;
    .locals 5
    .param p1, "mask"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    const/4 v4, 0x0

    .line 476
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    new-array v1, v2, [B

    .line 477
    .local v1, "masked":[B
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 478
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 479
    aget-byte v2, v1, v0

    invoke-virtual {p1, v0}, Lorg/snmp4j/smi/OctetString;->get(I)B

    move-result v3

    and-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :cond_0
    new-instance v2, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v2, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    return-object v2
.end method

.method public final set(IB)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "b"    # B

    .prologue
    .line 189
    iget-object v0, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aput-byte p2, v0, p1

    .line 190
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 419
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 420
    return-void
.end method

.method public setValue([B)V
    .locals 2
    .param p1, "value"    # [B

    .prologue
    .line 423
    if-nez p1, :cond_0

    .line 424
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OctetString must not be assigned a null value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 427
    :cond_0
    iput-object p1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    .line 428
    return-void
.end method

.method public startsWith(Lorg/snmp4j/smi/OctetString;)Z
    .locals 4
    .param p1, "prefix"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    const/4 v1, 0x0

    .line 262
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v2

    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 270
    :cond_0
    :goto_0
    return v1

    .line 265
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 266
    invoke-virtual {p1, v0}, Lorg/snmp4j/smi/OctetString;->get(I)B

    move-result v2

    iget-object v3, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v3, v3, v0

    if-ne v2, v3, :cond_0

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 270
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public substring(II)Lorg/snmp4j/smi/OctetString;
    .locals 4
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .prologue
    .line 244
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v1

    if-le p2, v1, :cond_1

    .line 245
    :cond_0
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 247
    :cond_1
    sub-int v1, p2, p1

    new-array v0, v1, [B

    .line 248
    .local v0, "substring":[B
    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v1, p1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    new-instance v1, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v1, v0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    return-object v1
.end method

.method public toASCII(C)Ljava/lang/String;
    .locals 4
    .param p1, "placeholder"    # C

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 406
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 407
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v2, v2, v1

    int-to-char v2, v2

    invoke-static {v2}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v2, v2, v1

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0x80

    if-lt v2, v3, :cond_1

    .line 409
    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 406
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 412
    :cond_1
    iget-object v2, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v2, v2, v1

    int-to-char v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 415
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 575
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v0

    return-object v0
.end method

.method public toHexString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/OctetString;->toHexString(C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toHexString(C)Ljava/lang/String;
    .locals 1
    .param p1, "separator"    # C

    .prologue
    .line 307
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, Lorg/snmp4j/smi/OctetString;->toString(CI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toInt()I
    .locals 1

    .prologue
    .line 458
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toLong()J
    .locals 1

    .prologue
    .line 462
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->isPrintable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 299
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(CI)Ljava/lang/String;
    .locals 10
    .param p1, "separator"    # C
    .param p2, "radix"    # I

    .prologue
    .line 353
    const-wide/high16 v6, 0x4070000000000000L    # 256.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    double-to-float v6, v6

    float-to-double v6, v6

    int-to-double v8, p2

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v1, v6

    .line 354
    .local v1, "digits":I
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v6, v6

    add-int/lit8 v7, v1, 0x1

    mul-int/2addr v6, v7

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 355
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 356
    if-lez v2, :cond_0

    .line 357
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 359
    :cond_0
    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v6, v6, v2

    and-int/lit16 v4, v6, 0xff

    .line 360
    .local v4, "v":I
    invoke-static {v4, p2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    .line 361
    .local v5, "val":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v6, v1, v6

    if-ge v3, v6, :cond_1

    .line 362
    const/16 v6, 0x30

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 361
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 364
    :cond_1
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 355
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 366
    .end local v3    # "j":I
    .end local v4    # "v":I
    .end local v5    # "val":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public toString(I)Ljava/lang/String;
    .locals 10
    .param p1, "radix"    # I

    .prologue
    .line 381
    const-wide/high16 v6, 0x4070000000000000L    # 256.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    double-to-float v6, v6

    float-to-double v6, v6

    int-to-double v8, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v1, v6

    .line 382
    .local v1, "digits":I
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v6, v6

    add-int/lit8 v7, v1, 0x1

    mul-int/2addr v6, v7

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 383
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    array-length v6, v6

    if-ge v2, v6, :cond_1

    .line 384
    iget-object v6, p0, Lorg/snmp4j/smi/OctetString;->value:[B

    aget-byte v6, v6, v2

    and-int/lit16 v4, v6, 0xff

    .line 385
    .local v4, "v":I
    invoke-static {v4, p1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    .line 386
    .local v5, "val":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v6, v1, v6

    if-ge v3, v6, :cond_0

    .line 387
    const/16 v6, 0x30

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 386
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 389
    :cond_0
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 383
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 391
    .end local v3    # "j":I
    .end local v4    # "v":I
    .end local v5    # "val":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public toSubIndex(Z)Lorg/snmp4j/smi/OID;
    .locals 6
    .param p1, "impliedLength"    # Z

    .prologue
    .line 486
    const/4 v1, 0x0

    .line 487
    .local v1, "offset":I
    if-nez p1, :cond_0

    .line 488
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [I

    .line 489
    .local v3, "subIndex":[I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .local v2, "offset":I
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    aput v4, v3, v1

    move v1, v2

    .line 494
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 495
    add-int v4, v1, v0

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/OctetString;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    aput v5, v3, v4

    .line 494
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 492
    .end local v0    # "i":I
    .end local v3    # "subIndex":[I
    :cond_0
    invoke-virtual {p0}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    new-array v3, v4, [I

    .restart local v3    # "subIndex":[I
    goto :goto_0

    .line 497
    .restart local v0    # "i":I
    :cond_1
    new-instance v4, Lorg/snmp4j/smi/OID;

    invoke-direct {v4, v3}, Lorg/snmp4j/smi/OID;-><init>([I)V

    return-object v4
.end method
