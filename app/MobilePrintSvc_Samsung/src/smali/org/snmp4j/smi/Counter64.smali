.class public Lorg/snmp4j/smi/Counter64;
.super Lorg/snmp4j/smi/AbstractVariable;
.source "Counter64.java"

# interfaces
.implements Lorg/snmp4j/smi/AssignableFromLong;
.implements Lorg/snmp4j/smi/AssignableFromString;


# static fields
.field private static final serialVersionUID:J = 0x795030065a01ff37L


# instance fields
.field private value:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    .line 51
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 53
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    .line 54
    invoke-virtual {p0, p1, p2}, Lorg/snmp4j/smi/Counter64;->setValue(J)V

    .line 55
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lorg/snmp4j/smi/Counter64;

    iget-wide v1, p0, Lorg/snmp4j/smi/Counter64;->value:J

    invoke-direct {v0, v1, v2}, Lorg/snmp4j/smi/Counter64;-><init>(J)V

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 9
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v7, 0x1

    .line 103
    check-cast p1, Lorg/snmp4j/smi/Counter64;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v1, p1, Lorg/snmp4j/smi/Counter64;->value:J

    .line 104
    .local v1, "other":J
    const/16 v0, 0x3f

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_2

    .line 105
    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    shr-long/2addr v3, v0

    and-long/2addr v3, v7

    shr-long v5, v1, v0

    and-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 107
    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    shr-long/2addr v3, v0

    and-long/2addr v3, v7

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    .line 108
    const/4 v3, 0x1

    .line 115
    :goto_1
    return v3

    .line 111
    :cond_0
    const/4 v3, -0x1

    goto :goto_1

    .line 104
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 115
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public decodeBER(Lorg/snmp4j/asn1/BERInputStream;)V
    .locals 6
    .param p1, "inputStream"    # Lorg/snmp4j/asn1/BERInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    new-instance v2, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v2}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    .line 63
    .local v2, "type":Lorg/snmp4j/asn1/BER$MutableByte;
    invoke-static {p1, v2}, Lorg/snmp4j/asn1/BER;->decodeUnsignedInt64(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)J

    move-result-wide v0

    .line 64
    .local v0, "newValue":J
    invoke-virtual {v2}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v3

    const/16 v4, 0x46

    if-eq v3, v4, :cond_0

    .line 65
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Wrong type encountered when decoding Counter64: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v2}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 68
    :cond_0
    invoke-virtual {p0, v0, v1}, Lorg/snmp4j/smi/Counter64;->setValue(J)V

    .line 69
    return-void
.end method

.method public encodeBER(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/16 v0, 0x46

    iget-wide v1, p0, Lorg/snmp4j/smi/Counter64;->value:J

    invoke-static {p1, v0, v1, v2}, Lorg/snmp4j/asn1/BER;->encodeUnsignedInt64(Ljava/io/OutputStream;BJ)V

    .line 59
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 96
    instance-of v1, p1, Lorg/snmp4j/smi/Counter64;

    if-eqz v1, :cond_0

    .line 97
    check-cast p1, Lorg/snmp4j/smi/Counter64;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v1, p1, Lorg/snmp4j/smi/Counter64;->value:J

    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 99
    :cond_0
    return v0
.end method

.method public fromSubIndex(Lorg/snmp4j/smi/OID;Z)V
    .locals 1
    .param p1, "subIndex"    # Lorg/snmp4j/smi/OID;
    .param p2, "impliedLength"    # Z

    .prologue
    .line 168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getBERLength()I
    .locals 4

    .prologue
    .line 80
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 81
    const/16 v0, 0xb

    .line 92
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide v2, 0x80000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 84
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/32 v2, 0x8000

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 85
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 87
    :cond_2
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/32 v2, 0x800000

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    goto :goto_0

    .line 89
    :cond_4
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide v2, 0x800000000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    .line 90
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide v2, 0x8000000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    const/4 v0, 0x7

    goto :goto_0

    :cond_5
    const/16 v0, 0x8

    goto :goto_0

    .line 92
    :cond_6
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/high16 v2, 0x80000000000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_7

    const/16 v0, 0x9

    goto :goto_0

    :cond_7
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public getSyntax()I
    .locals 1

    .prologue
    .line 72
    const/16 v0, 0x46

    return v0
.end method

.method public getValue()J
    .locals 2

    .prologue
    .line 139
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    long-to-int v0, v0

    return v0
.end method

.method public increment()V
    .locals 4

    .prologue
    .line 152
    iget-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    .line 153
    return-void
.end method

.method public setValue(J)V
    .locals 0
    .param p1, "value"    # J

    .prologue
    .line 135
    iput-wide p1, p0, Lorg/snmp4j/smi/Counter64;->value:J

    .line 136
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/snmp4j/smi/Counter64;->value:J

    .line 132
    return-void
.end method

.method public final toInt()I
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lorg/snmp4j/smi/Counter64;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final toLong()J
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lorg/snmp4j/smi/Counter64;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x8

    .line 119
    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    const-wide v5, 0x7fffffffffffffffL

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    .line 120
    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 127
    :goto_0
    return-object v3

    .line 122
    :cond_0
    new-array v0, v7, [B

    .line 123
    .local v0, "bytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v7, :cond_1

    .line 124
    iget-wide v3, p0, Lorg/snmp4j/smi/Counter64;->value:J

    rsub-int/lit8 v5, v1, 0x7

    mul-int/lit8 v5, v5, 0x8

    shr-long/2addr v3, v5

    const-wide/16 v5, 0xff

    and-long/2addr v3, v5

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    :cond_1
    new-instance v2, Ljava/math/BigInteger;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 127
    .local v2, "i64":Ljava/math/BigInteger;
    invoke-virtual {v2}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public toSubIndex(Z)Lorg/snmp4j/smi/OID;
    .locals 1
    .param p1, "impliedLength"    # Z

    .prologue
    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
