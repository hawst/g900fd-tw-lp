.class public Lorg/snmp4j/smi/Integer32;
.super Lorg/snmp4j/smi/AbstractVariable;
.source "Integer32.java"

# interfaces
.implements Lorg/snmp4j/smi/AssignableFromInteger;
.implements Lorg/snmp4j/smi/AssignableFromString;


# static fields
.field private static final serialVersionUID:J = 0x460776aea10a6dc0L


# instance fields
.field private value:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    .line 44
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Lorg/snmp4j/smi/AbstractVariable;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    .line 52
    invoke-virtual {p0, p1}, Lorg/snmp4j/smi/Integer32;->setValue(I)V

    .line 53
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lorg/snmp4j/smi/Integer32;

    iget v1, p0, Lorg/snmp4j/smi/Integer32;->value:I

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    return-object v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 100
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    check-cast p1, Lorg/snmp4j/smi/Integer32;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lorg/snmp4j/smi/Integer32;->value:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public decodeBER(Lorg/snmp4j/asn1/BERInputStream;)V
    .locals 5
    .param p1, "inputStream"    # Lorg/snmp4j/asn1/BERInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v1, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v1}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    .line 61
    .local v1, "type":Lorg/snmp4j/asn1/BER$MutableByte;
    invoke-static {p1, v1}, Lorg/snmp4j/asn1/BER;->decodeInteger(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)I

    move-result v0

    .line 62
    .local v0, "newValue":I
    invoke-virtual {v1}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 63
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Wrong type encountered when decoding Counter: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Lorg/snmp4j/asn1/BER$MutableByte;->getValue()B

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 65
    :cond_0
    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/Integer32;->setValue(I)V

    .line 66
    return-void
.end method

.method public encodeBER(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x2

    iget v1, p0, Lorg/snmp4j/smi/Integer32;->value:I

    invoke-static {p1, v0, v1}, Lorg/snmp4j/asn1/BER;->encodeInteger(Ljava/io/OutputStream;BI)V

    .line 57
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 93
    instance-of v1, p1, Lorg/snmp4j/smi/Integer32;

    if-eqz v1, :cond_0

    .line 94
    check-cast p1, Lorg/snmp4j/smi/Integer32;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lorg/snmp4j/smi/Integer32;->value:I

    iget v2, p0, Lorg/snmp4j/smi/Integer32;->value:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 96
    :cond_0
    return v0
.end method

.method public fromSubIndex(Lorg/snmp4j/smi/OID;Z)V
    .locals 1
    .param p1, "subIndex"    # Lorg/snmp4j/smi/OID;
    .param p2, "impliedLength"    # Z

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/snmp4j/smi/OID;->get(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/snmp4j/smi/Integer32;->setValue(I)V

    .line 147
    return-void
.end method

.method public getBERLength()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const/16 v1, 0x80

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const/16 v1, -0x80

    if-lt v0, v1, :cond_0

    .line 79
    const/4 v0, 0x3

    .line 89
    :goto_0
    return v0

    .line 81
    :cond_0
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const v1, 0x8000

    if-ge v0, v1, :cond_1

    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const/16 v1, -0x8000

    if-lt v0, v1, :cond_1

    .line 83
    const/4 v0, 0x4

    goto :goto_0

    .line 85
    :cond_1
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const/high16 v1, 0x800000

    if-ge v0, v1, :cond_2

    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    if-lt v0, v1, :cond_2

    .line 87
    const/4 v0, 0x5

    goto :goto_0

    .line 89
    :cond_2
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public getSyntax()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x2

    return v0
.end method

.method public final getValue()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    return v0
.end method

.method public final setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 117
    iput p1, p0, Lorg/snmp4j/smi/Integer32;->value:I

    .line 118
    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    .line 109
    return-void
.end method

.method public final toInt()I
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v0

    return v0
.end method

.method public final toLong()J
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lorg/snmp4j/smi/Integer32;->getValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/snmp4j/smi/Integer32;->value:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toSubIndex(Z)Lorg/snmp4j/smi/OID;
    .locals 4
    .param p1, "impliedLength"    # Z

    .prologue
    .line 142
    new-instance v0, Lorg/snmp4j/smi/OID;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lorg/snmp4j/smi/Integer32;->value:I

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>([I)V

    return-object v0
.end method
