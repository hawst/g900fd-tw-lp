.class public interface abstract Lorg/snmp4j/smi/Address;
.super Ljava/lang/Object;
.source "Address.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Lorg/snmp4j/smi/AssignableFromString;


# virtual methods
.method public abstract isValid()Z
.end method

.method public abstract parseAddress(Ljava/lang/String;)Z
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method
