.class public Lorg/snmp4j/util/TaskScheduler;
.super Ljava/lang/Object;
.source "TaskScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final DEFAULT_SCHEDULER_TIMEOUT:J = 0x5L

.field static class$org$snmp4j$util$TaskScheduler:Ljava/lang/Class;


# instance fields
.field private logger:Lorg/snmp4j/log/LogAdapter;

.field protected schedulerTimeout:J

.field private stop:Z

.field private tasks:Ljava/util/LinkedList;

.field private threadPool:Lorg/snmp4j/util/ThreadPool;


# direct methods
.method public constructor <init>(Lorg/snmp4j/util/ThreadPool;)V
    .locals 2
    .param p1, "threadPool"    # Lorg/snmp4j/util/ThreadPool;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lorg/snmp4j/util/TaskScheduler;->class$org$snmp4j$util$TaskScheduler:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.util.TaskScheduler"

    invoke-static {v0}, Lorg/snmp4j/util/TaskScheduler;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/util/TaskScheduler;->class$org$snmp4j$util$TaskScheduler:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    .line 46
    const-wide/16 v0, 0x5

    iput-wide v0, p0, Lorg/snmp4j/util/TaskScheduler;->schedulerTimeout:J

    .line 56
    iput-object p1, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    .line 57
    return-void

    .line 39
    :cond_0
    sget-object v0, Lorg/snmp4j/util/TaskScheduler;->class$org$snmp4j$util$TaskScheduler:Ljava/lang/Class;

    goto :goto_0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 39
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public declared-synchronized addTask(Lorg/snmp4j/util/SchedulerTask;)V
    .locals 1
    .param p1, "task"    # Lorg/snmp4j/util/SchedulerTask;

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isStop()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lorg/snmp4j/util/TaskScheduler;->stop:Z

    return v0
.end method

.method public declared-synchronized removeTask(Lorg/snmp4j/util/SchedulerTask;)Z
    .locals 1
    .param p1, "task"    # Lorg/snmp4j/util/SchedulerTask;

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 92
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lorg/snmp4j/util/TaskScheduler;->stop:Z

    if-nez v5, :cond_7

    .line 93
    const/4 v3, 0x0

    .line 94
    .local v3, "readyToRun":Z
    monitor-enter p0

    .line 95
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 96
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/snmp4j/util/SchedulerTask;

    .line 97
    .local v4, "task":Lorg/snmp4j/util/SchedulerTask;
    invoke-interface {v4}, Lorg/snmp4j/util/SchedulerTask;->isDone()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 98
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 99
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Task \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\' is done"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 101
    :cond_1
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 95
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 104
    :cond_3
    invoke-interface {v4}, Lorg/snmp4j/util/SchedulerTask;->isReadyToRun()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 105
    const/4 v3, 0x1

    .line 106
    :goto_3
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    invoke-virtual {v5, v4}, Lorg/snmp4j/util/ThreadPool;->tryToExecute(Lorg/snmp4j/util/WorkerTask;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result v5

    if-nez v5, :cond_4

    .line 108
    :try_start_1
    iget-object v6, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 109
    :try_start_2
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    iget-wide v7, p0, Lorg/snmp4j/util/TaskScheduler;->schedulerTimeout:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/Object;->wait(J)V

    .line 110
    monitor-exit v6

    goto :goto_3

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_4
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Scheduler interrupted, aborting..."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 114
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/snmp4j/util/TaskScheduler;->stop:Z

    .line 118
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :cond_4
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    iget-object v6, p0, Lorg/snmp4j/util/TaskScheduler;->tasks:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 119
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 122
    .end local v4    # "task":Lorg/snmp4j/util/SchedulerTask;
    :cond_5
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 123
    if-nez v3, :cond_0

    .line 125
    :try_start_5
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    invoke-virtual {v5}, Lorg/snmp4j/util/ThreadPool;->isIdle()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 126
    monitor-enter p0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1

    .line 127
    :try_start_6
    iget-wide v5, p0, Lorg/snmp4j/util/TaskScheduler;->schedulerTimeout:J

    invoke-virtual {p0, v5, v6}, Ljava/lang/Object;->wait(J)V

    .line 128
    monitor-exit p0

    goto/16 :goto_0

    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1

    .line 136
    :catch_1
    move-exception v1

    .line 137
    .local v1, "ex1":Ljava/lang/InterruptedException;
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Scheduler interrupted, aborting..."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->warn(Ljava/lang/Object;)V

    .line 138
    iput-boolean v9, p0, Lorg/snmp4j/util/TaskScheduler;->stop:Z

    goto/16 :goto_0

    .line 122
    .end local v1    # "ex1":Ljava/lang/InterruptedException;
    :catchall_2
    move-exception v5

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v5

    .line 131
    :cond_6
    :try_start_9
    iget-object v6, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    monitor-enter v6
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1

    .line 132
    :try_start_a
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->threadPool:Lorg/snmp4j/util/ThreadPool;

    iget-wide v7, p0, Lorg/snmp4j/util/TaskScheduler;->schedulerTimeout:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/Object;->wait(J)V

    .line 133
    monitor-exit v6

    goto/16 :goto_0

    :catchall_3
    move-exception v5

    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    throw v5
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_1

    .line 142
    .end local v2    # "i":I
    .end local v3    # "readyToRun":Z
    :cond_7
    iget-object v5, p0, Lorg/snmp4j/util/TaskScheduler;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Scheduler stopped."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->info(Ljava/lang/Object;)V

    .line 143
    return-void
.end method

.method public setStop(Z)V
    .locals 0
    .param p1, "stop"    # Z

    .prologue
    .line 151
    iput-boolean p1, p0, Lorg/snmp4j/util/TaskScheduler;->stop:Z

    .line 152
    return-void
.end method
