.class public Lorg/snmp4j/util/DefaultPDUFactory;
.super Ljava/lang/Object;
.source "DefaultPDUFactory.java"

# interfaces
.implements Lorg/snmp4j/util/PDUFactory;


# instance fields
.field private pduType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/16 v0, -0x60

    iput v0, p0, Lorg/snmp4j/util/DefaultPDUFactory;->pduType:I

    .line 46
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "pduType"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/16 v0, -0x60

    iput v0, p0, Lorg/snmp4j/util/DefaultPDUFactory;->pduType:I

    .line 54
    invoke-virtual {p0, p1}, Lorg/snmp4j/util/DefaultPDUFactory;->setPduType(I)V

    .line 55
    return-void
.end method

.method public static createPDU(I)Lorg/snmp4j/PDU;
    .locals 1
    .param p0, "targetVersion"    # I

    .prologue
    .line 102
    packed-switch p0, :pswitch_data_0

    .line 112
    :pswitch_0
    new-instance v0, Lorg/snmp4j/PDU;

    invoke-direct {v0}, Lorg/snmp4j/PDU;-><init>()V

    .line 114
    .local v0, "request":Lorg/snmp4j/PDU;
    :goto_0
    return-object v0

    .line 104
    .end local v0    # "request":Lorg/snmp4j/PDU;
    :pswitch_1
    new-instance v0, Lorg/snmp4j/ScopedPDU;

    invoke-direct {v0}, Lorg/snmp4j/ScopedPDU;-><init>()V

    .line 105
    .restart local v0    # "request":Lorg/snmp4j/PDU;
    goto :goto_0

    .line 108
    .end local v0    # "request":Lorg/snmp4j/PDU;
    :pswitch_2
    new-instance v0, Lorg/snmp4j/PDUv1;

    invoke-direct {v0}, Lorg/snmp4j/PDUv1;-><init>()V

    .line 109
    .restart local v0    # "request":Lorg/snmp4j/PDU;
    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static createPDU(Lorg/snmp4j/Target;I)Lorg/snmp4j/PDU;
    .locals 2
    .param p0, "target"    # Lorg/snmp4j/Target;
    .param p1, "pduType"    # I

    .prologue
    .line 87
    invoke-interface {p0}, Lorg/snmp4j/Target;->getVersion()I

    move-result v1

    invoke-static {v1}, Lorg/snmp4j/util/DefaultPDUFactory;->createPDU(I)Lorg/snmp4j/PDU;

    move-result-object v0

    .line 88
    .local v0, "request":Lorg/snmp4j/PDU;
    invoke-virtual {v0, p1}, Lorg/snmp4j/PDU;->setType(I)V

    .line 89
    return-object v0
.end method


# virtual methods
.method public createPDU(Lorg/snmp4j/Target;)Lorg/snmp4j/PDU;
    .locals 1
    .param p1, "target"    # Lorg/snmp4j/Target;

    .prologue
    .line 73
    iget v0, p0, Lorg/snmp4j/util/DefaultPDUFactory;->pduType:I

    invoke-static {p1, v0}, Lorg/snmp4j/util/DefaultPDUFactory;->createPDU(Lorg/snmp4j/Target;I)Lorg/snmp4j/PDU;

    move-result-object v0

    return-object v0
.end method

.method public getPduType()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lorg/snmp4j/util/DefaultPDUFactory;->pduType:I

    return v0
.end method

.method public setPduType(I)V
    .locals 0
    .param p1, "pduType"    # I

    .prologue
    .line 58
    iput p1, p0, Lorg/snmp4j/util/DefaultPDUFactory;->pduType:I

    .line 59
    return-void
.end method
