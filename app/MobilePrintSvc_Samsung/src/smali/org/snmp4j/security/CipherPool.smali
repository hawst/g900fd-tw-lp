.class public Lorg/snmp4j/security/CipherPool;
.super Ljava/lang/Object;
.source "CipherPool.java"


# instance fields
.field private availableCiphers:Ljava/util/LinkedList;

.field private currentPoolSize:I

.field private maxPoolSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/snmp4j/security/CipherPool;-><init>(I)V

    .line 49
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxPoolSize"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I

    .line 58
    if-gez p1, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Pool size must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput p1, p0, Lorg/snmp4j/security/CipherPool;->maxPoolSize:I

    .line 62
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/CipherPool;->availableCiphers:Ljava/util/LinkedList;

    .line 63
    return-void
.end method


# virtual methods
.method public getMaxPoolSize()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/snmp4j/security/CipherPool;->maxPoolSize:I

    return v0
.end method

.method public declared-synchronized offerCipher(Ljavax/crypto/Cipher;)V
    .locals 2
    .param p1, "cipher"    # Ljavax/crypto/Cipher;

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I

    iget v1, p0, Lorg/snmp4j/security/CipherPool;->maxPoolSize:I

    if-ge v0, v1, :cond_0

    .line 95
    iget v0, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I

    .line 96
    iget-object v0, p0, Lorg/snmp4j/security/CipherPool;->availableCiphers:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :cond_0
    monitor-exit p0

    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reuseCipher()Ljavax/crypto/Cipher;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/snmp4j/security/CipherPool;->availableCiphers:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Cipher;

    .line 79
    .local v0, "cipher":Ljavax/crypto/Cipher;
    if-nez v0, :cond_0

    .line 80
    const/4 v1, 0x0

    iput v1, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :goto_0
    monitor-exit p0

    return-object v0

    .line 83
    :cond_0
    :try_start_1
    iget v1, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/snmp4j/security/CipherPool;->currentPoolSize:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 78
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
