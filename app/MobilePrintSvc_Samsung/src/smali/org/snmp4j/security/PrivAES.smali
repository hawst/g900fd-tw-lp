.class public abstract Lorg/snmp4j/security/PrivAES;
.super Ljava/lang/Object;
.source "PrivAES.java"

# interfaces
.implements Lorg/snmp4j/security/PrivacyProtocol;


# static fields
.field private static final DECRYPT_PARAMS_LENGTH:I = 0x8

.field static class$org$snmp4j$security$PrivAES:Ljava/lang/Class;

.field private static final logger:Lorg/snmp4j/log/LogAdapter;


# instance fields
.field protected cipherPool:Lorg/snmp4j/security/CipherPool;

.field private keyBytes:I

.field protected salt:Lorg/snmp4j/security/Salt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lorg/snmp4j/security/PrivAES;->class$org$snmp4j$security$PrivAES:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.security.PrivAES"

    invoke-static {v0}, Lorg/snmp4j/security/PrivAES;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/PrivAES;->class$org$snmp4j$security$PrivAES:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    return-void

    :cond_0
    sget-object v0, Lorg/snmp4j/security/PrivAES;->class$org$snmp4j$security$PrivAES:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "keyBytes"    # I

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/16 v0, 0x10

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-eq p1, v0, :cond_0

    const/16 v0, 0x20

    if-eq p1, v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Only 128, 192 and 256 bit AES is allowed. Requested ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    mul-int/lit8 v2, p1, 0x8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    iput p1, p0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    .line 65
    invoke-static {}, Lorg/snmp4j/security/Salt;->getInstance()Lorg/snmp4j/security/Salt;

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/security/PrivAES;->salt:Lorg/snmp4j/security/Salt;

    .line 66
    new-instance v0, Lorg/snmp4j/security/CipherPool;

    invoke-direct {v0}, Lorg/snmp4j/security/CipherPool;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/PrivAES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    .line 67
    return-void
.end method

.method public static asHex([B)Ljava/lang/String;
    .locals 1
    .param p0, "buf"    # [B

    .prologue
    .line 212
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v0, p0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    invoke-virtual {v0}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 45
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public decrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B
    .locals 12
    .param p1, "cryptedData"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "decryptionKey"    # [B
    .param p5, "engineBoots"    # J
    .param p7, "engineTime"    # J
    .param p9, "decryptParams"    # Lorg/snmp4j/security/DecryptParams;

    .prologue
    .line 145
    const/16 v7, 0x10

    new-array v4, v7, [B

    .line 147
    .local v4, "initVect":[B
    move-object/from16 v0, p4

    array-length v7, v0

    iget v8, p0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    if-ge v7, v8, :cond_0

    .line 148
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Needed key length is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget v9, p0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ". Got only "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    move-object/from16 v0, p4

    array-length v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 155
    :cond_0
    const/4 v7, 0x0

    const/16 v8, 0x18

    shr-long v8, p5, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 156
    const/4 v7, 0x1

    const/16 v8, 0x10

    shr-long v8, p5, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 157
    const/4 v7, 0x2

    const/16 v8, 0x8

    shr-long v8, p5, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 158
    const/4 v7, 0x3

    const-wide/16 v8, 0xff

    and-long v8, v8, p5

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 159
    const/4 v7, 0x4

    const/16 v8, 0x18

    shr-long v8, p7, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 160
    const/4 v7, 0x5

    const/16 v8, 0x10

    shr-long v8, p7, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 161
    const/4 v7, 0x6

    const/16 v8, 0x8

    shr-long v8, p7, v8

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 162
    const/4 v7, 0x7

    const-wide/16 v8, 0xff

    and-long v8, v8, p7

    long-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v4, v7

    .line 163
    move-object/from16 v0, p9

    iget-object v7, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    move-object/from16 v0, p9

    iget v8, v0, Lorg/snmp4j/security/DecryptParams;->offset:I

    const/16 v9, 0x8

    const/16 v10, 0x8

    invoke-static {v7, v8, v4, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 164
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v7}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 165
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "initVect is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v4}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 168
    :cond_1
    const/4 v2, 0x0

    .line 171
    .local v2, "decryptedData":[B
    :try_start_0
    iget-object v7, p0, Lorg/snmp4j/security/PrivAES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v7}, Lorg/snmp4j/security/CipherPool;->reuseCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    .line 172
    .local v1, "alg":Ljavax/crypto/Cipher;
    if-nez v1, :cond_2

    .line 173
    const-string v7, "AES/CFB/NoPadding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 175
    :cond_2
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v7, 0x0

    iget v8, p0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    const-string v9, "AES"

    move-object/from16 v0, p4

    invoke-direct {v6, v0, v7, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 177
    .local v6, "key":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 178
    .local v5, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v7, 0x2

    invoke-virtual {v1, v7, v6, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 179
    invoke-virtual {v1, p1, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v2

    .line 180
    iget-object v7, p0, Lorg/snmp4j/security/PrivAES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v7, v1}, Lorg/snmp4j/security/CipherPool;->offerCipher(Ljavax/crypto/Cipher;)V

    .line 182
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v7}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 183
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "aes decrypt: Data to decrypt "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {p1}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 185
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "aes decrypt: used key "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static/range {p4 .. p4}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 187
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "aes decrypt: used privacy_params "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    move-object/from16 v0, p9

    iget-object v9, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    invoke-static {v9}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 190
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "aes decrypt: decrypted Data  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v2}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    .end local v1    # "alg":Ljavax/crypto/Cipher;
    .end local v5    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v6    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :cond_3
    :goto_0
    return-object v2

    .line 194
    :catch_0
    move-exception v3

    .line 195
    .local v3, "e":Ljava/lang/Exception;
    sget-object v7, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Decrypt Exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public encrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B
    .locals 18
    .param p1, "unencryptedData"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "encryptionKey"    # [B
    .param p5, "engineBoots"    # J
    .param p7, "engineTime"    # J
    .param p9, "decryptParams"    # Lorg/snmp4j/security/DecryptParams;

    .prologue
    .line 73
    const/16 v13, 0x10

    new-array v7, v13, [B

    .line 74
    .local v7, "initVect":[B
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/snmp4j/security/PrivAES;->salt:Lorg/snmp4j/security/Salt;

    invoke-virtual {v13}, Lorg/snmp4j/security/Salt;->getNext()J

    move-result-wide v11

    .line 76
    .local v11, "my_salt":J
    move-object/from16 v0, p4

    array-length v13, v0

    move-object/from16 v0, p0

    iget v14, v0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    if-ge v13, v14, :cond_0

    .line 77
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Needed key length is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, ". Got only "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p4

    array-length v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, "."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 83
    :cond_0
    move-object/from16 v0, p9

    iget-object v13, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    if-eqz v13, :cond_1

    move-object/from16 v0, p9

    iget v13, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    const/16 v14, 0x8

    if-ge v13, v14, :cond_2

    .line 85
    :cond_1
    const/16 v13, 0x8

    new-array v13, v13, [B

    move-object/from16 v0, p9

    iput-object v13, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    .line 87
    :cond_2
    const/16 v13, 0x8

    move-object/from16 v0, p9

    iput v13, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    .line 88
    const/4 v13, 0x0

    move-object/from16 v0, p9

    iput v13, v0, Lorg/snmp4j/security/DecryptParams;->offset:I

    .line 91
    const/4 v13, 0x0

    const/16 v14, 0x18

    shr-long v14, p5, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 92
    const/4 v13, 0x1

    const/16 v14, 0x10

    shr-long v14, p5, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 93
    const/4 v13, 0x2

    const/16 v14, 0x8

    shr-long v14, p5, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 94
    const/4 v13, 0x3

    const-wide/16 v14, 0xff

    and-long v14, v14, p5

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 95
    const/4 v13, 0x4

    const/16 v14, 0x18

    shr-long v14, p7, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 96
    const/4 v13, 0x5

    const/16 v14, 0x10

    shr-long v14, p7, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 97
    const/4 v13, 0x6

    const/16 v14, 0x8

    shr-long v14, p7, v14

    const-wide/16 v16, 0xff

    and-long v14, v14, v16

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 98
    const/4 v13, 0x7

    const-wide/16 v14, 0xff

    and-long v14, v14, p7

    long-to-int v14, v14

    int-to-byte v14, v14

    aput-byte v14, v7, v13

    .line 99
    const/16 v6, 0x38

    .local v6, "i":I
    const/16 v9, 0x8

    .local v9, "j":I
    :goto_0
    if-ltz v6, :cond_3

    .line 100
    shr-long v13, v11, v6

    const-wide/16 v15, 0xff

    and-long/2addr v13, v15

    long-to-int v13, v13

    int-to-byte v13, v13

    aput-byte v13, v7, v9

    .line 99
    add-int/lit8 v6, v6, -0x8

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 102
    :cond_3
    const/16 v13, 0x8

    move-object/from16 v0, p9

    iget-object v14, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    const/4 v15, 0x0

    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-static {v7, v13, v14, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v13}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 104
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "initVect is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-static {v7}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 108
    :cond_4
    const/4 v5, 0x0

    .line 111
    .local v5, "encryptedData":[B
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/snmp4j/security/PrivAES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v13}, Lorg/snmp4j/security/CipherPool;->reuseCipher()Ljavax/crypto/Cipher;

    move-result-object v3

    .line 112
    .local v3, "alg":Ljavax/crypto/Cipher;
    if-nez v3, :cond_5

    .line 113
    const-string v13, "AES/CFB/NoPadding"

    invoke-static {v13}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 115
    :cond_5
    new-instance v10, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v14, v0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    const-string v15, "AES"

    move-object/from16 v0, p4

    invoke-direct {v10, v0, v13, v14, v15}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 117
    .local v10, "key":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v8, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v8, v7}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 118
    .local v8, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v13, 0x1

    invoke-virtual {v3, v13, v10, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 119
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v5

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/snmp4j/security/PrivAES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v13, v3}, Lorg/snmp4j/security/CipherPool;->offerCipher(Ljavax/crypto/Cipher;)V

    .line 122
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v13}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 123
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "aes encrypt: Data to encrypt "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 125
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "aes encrypt: used key "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-static/range {p4 .. p4}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 127
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "aes encrypt: created privacy_params "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p9

    iget-object v15, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    invoke-static {v15}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 130
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "aes encrypt: encrypted Data  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-static {v5}, Lorg/snmp4j/security/PrivAES;->asHex([B)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .end local v3    # "alg":Ljavax/crypto/Cipher;
    .end local v8    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v10    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :cond_6
    :goto_1
    return-object v5

    .line 134
    :catch_0
    move-exception v4

    .line 135
    .local v4, "e":Ljava/lang/Exception;
    sget-object v13, Lorg/snmp4j/security/PrivAES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Encrypt Exception "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public extendShortKey([BLorg/snmp4j/smi/OctetString;[BLorg/snmp4j/security/AuthenticationProtocol;)[B
    .locals 6
    .param p1, "shortKey"    # [B
    .param p2, "password"    # Lorg/snmp4j/smi/OctetString;
    .param p3, "engineID"    # [B
    .param p4, "authProtocol"    # Lorg/snmp4j/security/AuthenticationProtocol;

    .prologue
    const/4 v5, 0x0

    .line 233
    invoke-virtual {p0}, Lorg/snmp4j/security/PrivAES;->getMinKeyLength()I

    move-result v4

    new-array v1, v4, [B

    .line 234
    .local v1, "extKey":[B
    array-length v3, p1

    .line 235
    .local v3, "length":I
    invoke-static {p1, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 237
    :goto_0
    array-length v4, v1

    if-ge v3, v4, :cond_0

    .line 239
    invoke-interface {p4, v1, v5, v3}, Lorg/snmp4j/security/AuthenticationProtocol;->hash([BII)[B

    move-result-object v2

    .line 241
    .local v2, "hash":[B
    if-nez v2, :cond_1

    .line 242
    const/4 v1, 0x0

    .line 255
    .end local v1    # "extKey":[B
    .end local v2    # "hash":[B
    :cond_0
    return-object v1

    .line 244
    .restart local v1    # "extKey":[B
    .restart local v2    # "hash":[B
    :cond_1
    array-length v4, v1

    sub-int v0, v4, v3

    .line 245
    .local v0, "bytesToCopy":I
    invoke-interface {p4}, Lorg/snmp4j/security/AuthenticationProtocol;->getDigestLength()I

    move-result v4

    if-le v0, v4, :cond_2

    .line 246
    invoke-interface {p4}, Lorg/snmp4j/security/AuthenticationProtocol;->getDigestLength()I

    move-result v0

    .line 248
    :cond_2
    invoke-static {v2, v5, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    add-int/2addr v3, v0

    .line 254
    goto :goto_0
.end method

.method public getDecryptParamsLength()I
    .locals 1

    .prologue
    .line 224
    const/16 v0, 0x8

    return v0
.end method

.method public getEncryptedLength(I)I
    .locals 0
    .param p1, "scopedPDULength"    # I

    .prologue
    .line 202
    return p1
.end method

.method public getMaxKeyLength()I
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lorg/snmp4j/security/PrivAES;->getMinKeyLength()I

    move-result v0

    return v0
.end method

.method public getMinKeyLength()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lorg/snmp4j/security/PrivAES;->keyBytes:I

    return v0
.end method
