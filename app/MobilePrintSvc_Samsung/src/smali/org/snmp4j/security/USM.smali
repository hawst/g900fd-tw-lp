.class public Lorg/snmp4j/security/USM;
.super Ljava/lang/Object;
.source "USM.java"

# interfaces
.implements Lorg/snmp4j/security/SecurityModel;


# static fields
.field private static final MAXLEN_USMUSERNAME:I = 0x20

.field static class$org$snmp4j$security$USM:Ljava/lang/Class;

.field private static final logger:Lorg/snmp4j/log/LogAdapter;


# instance fields
.field private counterSupport:Lorg/snmp4j/mp/CounterSupport;

.field private engineDiscoveryEnabled:Z

.field private localEngineID:Lorg/snmp4j/smi/OctetString;

.field private securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

.field private timeTable:Lorg/snmp4j/security/UsmTimeTable;

.field private userTable:Lorg/snmp4j/security/UsmUserTable;

.field private transient usmUserListeners:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lorg/snmp4j/security/USM;->class$org$snmp4j$security$USM:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.security.USM"

    invoke-static {v0}, Lorg/snmp4j/security/USM;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/USM;->class$org$snmp4j$security$USM:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    return-void

    :cond_0
    sget-object v0, Lorg/snmp4j/security/USM;->class$org$snmp4j$security$USM:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/snmp4j/security/SecurityProtocols;Lorg/snmp4j/smi/OctetString;I)V
    .locals 1
    .param p1, "securityProtocols"    # Lorg/snmp4j/security/SecurityProtocols;
    .param p2, "localEngineID"    # Lorg/snmp4j/smi/OctetString;
    .param p3, "engineBoots"    # I

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/snmp4j/security/USM;->engineDiscoveryEnabled:Z

    .line 75
    iput-object p2, p0, Lorg/snmp4j/security/USM;->localEngineID:Lorg/snmp4j/smi/OctetString;

    .line 76
    new-instance v0, Lorg/snmp4j/security/UsmTimeTable;

    invoke-direct {v0, p2, p3}, Lorg/snmp4j/security/UsmTimeTable;-><init>(Lorg/snmp4j/smi/OctetString;I)V

    iput-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    .line 77
    new-instance v0, Lorg/snmp4j/security/UsmUserTable;

    invoke-direct {v0}, Lorg/snmp4j/security/UsmUserTable;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    .line 78
    iput-object p1, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    .line 79
    invoke-static {}, Lorg/snmp4j/mp/CounterSupport;->getInstance()Lorg/snmp4j/mp/CounterSupport;

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/security/USM;->counterSupport:Lorg/snmp4j/mp/CounterSupport;

    .line 80
    return-void
.end method

.method private static buildMessageBuffer(Lorg/snmp4j/asn1/BERInputStream;)[B
    .locals 9
    .param p0, "scopedPDU"    # Lorg/snmp4j/asn1/BERInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    const/16 v7, 0x10

    invoke-virtual {p0, v7}, Lorg/snmp4j/asn1/BERInputStream;->mark(I)V

    .line 159
    invoke-virtual {p0}, Lorg/snmp4j/asn1/BERInputStream;->getPosition()J

    move-result-wide v7

    long-to-int v6, v7

    .line 160
    .local v6, "readLengthBytes":I
    new-instance v3, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v3}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    .line 161
    .local v3, "mutableByte":Lorg/snmp4j/asn1/BER$MutableByte;
    invoke-static {p0, v3}, Lorg/snmp4j/asn1/BER;->decodeHeader(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)I

    move-result v2

    .line 162
    .local v2, "length":I
    invoke-virtual {p0}, Lorg/snmp4j/asn1/BERInputStream;->getPosition()J

    move-result-wide v7

    long-to-int v7, v7

    sub-int v6, v7, v6

    .line 163
    add-int v7, v2, v6

    new-array v1, v7, [B

    .line 164
    .local v1, "buf":[B
    invoke-virtual {p0}, Lorg/snmp4j/asn1/BERInputStream;->reset()V

    .line 166
    const/4 v4, 0x0

    .line 167
    .local v4, "offset":I
    invoke-virtual {p0}, Lorg/snmp4j/asn1/BERInputStream;->available()I

    move-result v0

    .line 168
    .local v0, "avail":I
    :goto_0
    array-length v7, v1

    if-ge v4, v7, :cond_0

    if-lez v0, :cond_0

    .line 169
    array-length v7, v1

    sub-int/2addr v7, v4

    invoke-virtual {p0, v1, v4, v7}, Lorg/snmp4j/asn1/BERInputStream;->read([BII)I

    move-result v5

    .line 170
    .local v5, "read":I
    if-gez v5, :cond_1

    .line 175
    .end local v5    # "read":I
    :cond_0
    return-object v1

    .line 173
    .restart local v5    # "read":I
    :cond_1
    add-int/2addr v4, v5

    .line 174
    goto :goto_0
.end method

.method private static buildWholeMessage(Lorg/snmp4j/smi/Integer32;[B[BLorg/snmp4j/security/UsmSecurityParameters;)[B
    .locals 6
    .param p0, "snmpVersion"    # Lorg/snmp4j/smi/Integer32;
    .param p1, "scopedPdu"    # [B
    .param p2, "globalData"    # [B
    .param p3, "usmSecurityParameters"    # Lorg/snmp4j/security/UsmSecurityParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p0}, Lorg/snmp4j/smi/Integer32;->getBERLength()I

    move-result v4

    array-length v5, p2

    add-int/2addr v4, v5

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmSecurityParameters;->getBERLength()I

    move-result v5

    add-int/2addr v4, v5

    array-length v5, p1

    add-int v0, v4, v5

    .line 190
    .local v0, "length":I
    invoke-static {v0}, Lorg/snmp4j/asn1/BER;->getBERLengthOfLength(I)I

    move-result v4

    add-int/2addr v4, v0

    add-int/lit8 v3, v4, 0x1

    .line 192
    .local v3, "totalLength":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 193
    .local v1, "os":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x30

    invoke-static {v1, v4, v0}, Lorg/snmp4j/asn1/BER;->encodeHeader(Ljava/io/OutputStream;II)V

    .line 194
    invoke-virtual {p0, v1}, Lorg/snmp4j/smi/Integer32;->encodeBER(Ljava/io/OutputStream;)V

    .line 195
    invoke-virtual {v1, p2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 196
    invoke-virtual {p3, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->encodeBER(Ljava/io/OutputStream;)V

    .line 197
    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 198
    invoke-virtual {p0}, Lorg/snmp4j/smi/Integer32;->getBERLength()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v0}, Lorg/snmp4j/asn1/BER;->getBERLengthOfLength(I)I

    move-result v5

    add-int/2addr v4, v5

    array-length v5, p2

    add-int v2, v4, v5

    .line 200
    .local v2, "secParamsPos":I
    invoke-virtual {p3, v2}, Lorg/snmp4j/security/UsmSecurityParameters;->setSecurityParametersPosition(I)V

    .line 201
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 48
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method

.method private getSecurityName(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/smi/OctetString;
    .locals 2
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "userName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 538
    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 551
    .end local p2    # "userName":Lorg/snmp4j/smi/OctetString;
    :goto_0
    return-object p2

    .line 541
    .restart local p2    # "userName":Lorg/snmp4j/smi/OctetString;
    :cond_0
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p1, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 542
    .local v0, "user":Lorg/snmp4j/security/UsmUserEntry;
    if-eqz v0, :cond_1

    .line 543
    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v1

    invoke-virtual {v1}, Lorg/snmp4j/security/UsmUser;->getSecurityName()Lorg/snmp4j/smi/OctetString;

    move-result-object p2

    goto :goto_0

    .line 545
    :cond_1
    invoke-virtual {p0}, Lorg/snmp4j/security/USM;->isEngineDiscoveryEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 546
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 547
    if-eqz v0, :cond_2

    .line 548
    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v1

    invoke-virtual {v1}, Lorg/snmp4j/security/UsmUser;->getSecurityName()Lorg/snmp4j/smi/OctetString;

    move-result-object p2

    goto :goto_0

    .line 551
    :cond_2
    const/4 p2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addLocalizedUser([BLorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;[BLorg/snmp4j/smi/OID;[B)Lorg/snmp4j/security/UsmUserEntry;
    .locals 7
    .param p1, "engineID"    # [B
    .param p2, "userName"    # Lorg/snmp4j/smi/OctetString;
    .param p3, "authProtocol"    # Lorg/snmp4j/smi/OID;
    .param p4, "authKey"    # [B
    .param p5, "privProtocol"    # Lorg/snmp4j/smi/OID;
    .param p6, "privKey"    # [B

    .prologue
    .line 990
    new-instance v0, Lorg/snmp4j/security/UsmUserEntry;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/snmp4j/security/UsmUserEntry;-><init>([BLorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;[BLorg/snmp4j/smi/OID;[B)V

    .line 993
    .local v0, "newEntry":Lorg/snmp4j/security/UsmUserEntry;
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, v0}, Lorg/snmp4j/security/UsmUserTable;->addUser(Lorg/snmp4j/security/UsmUserEntry;)Lorg/snmp4j/security/UsmUserEntry;

    .line 994
    new-instance v1, Lorg/snmp4j/event/UsmUserEvent;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v2}, Lorg/snmp4j/event/UsmUserEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/security/UsmUserEntry;I)V

    invoke-virtual {p0, v1}, Lorg/snmp4j/security/USM;->fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 996
    return-object v0
.end method

.method public addUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V
    .locals 1
    .param p1, "userName"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "user"    # Lorg/snmp4j/security/UsmUser;

    .prologue
    .line 815
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v0}, Lorg/snmp4j/smi/OctetString;-><init>()V

    invoke-virtual {p0, p1, v0, p2}, Lorg/snmp4j/security/USM;->addUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V

    .line 816
    return-void
.end method

.method public addUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V
    .locals 9
    .param p1, "userName"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p3, "user"    # Lorg/snmp4j/security/UsmUser;

    .prologue
    .line 832
    const/4 v0, 0x0

    .line 833
    .local v0, "authKey":[B
    const/4 v2, 0x0

    .line 834
    .local v2, "privKey":[B
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 835
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 836
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->isLocalized()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 837
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getAuthenticationPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v0

    .line 845
    :goto_0
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getPrivacyProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 846
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->isLocalized()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 847
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getPrivacyPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v2

    .line 860
    :cond_0
    :goto_1
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->isLocalized()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 861
    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getLocalizationEngineID()Lorg/snmp4j/smi/OctetString;

    move-result-object v3

    .line 866
    .local v3, "userEngineID":Lorg/snmp4j/smi/OctetString;
    :goto_2
    new-instance v1, Lorg/snmp4j/security/UsmUserEntry;

    invoke-direct {v1, p1, v3, p3}, Lorg/snmp4j/security/UsmUserEntry;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V

    .line 868
    .local v1, "entry":Lorg/snmp4j/security/UsmUserEntry;
    invoke-virtual {v1, v0}, Lorg/snmp4j/security/UsmUserEntry;->setAuthenticationKey([B)V

    .line 869
    invoke-virtual {v1, v2}, Lorg/snmp4j/security/UsmUserEntry;->setPrivacyKey([B)V

    .line 870
    iget-object v4, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v4, v1}, Lorg/snmp4j/security/UsmUserTable;->addUser(Lorg/snmp4j/security/UsmUserEntry;)Lorg/snmp4j/security/UsmUserEntry;

    .line 871
    new-instance v4, Lorg/snmp4j/event/UsmUserEvent;

    const/4 v5, 0x1

    invoke-direct {v4, p0, v1, v5}, Lorg/snmp4j/event/UsmUserEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/security/UsmUserEntry;I)V

    invoke-virtual {p0, v4}, Lorg/snmp4j/security/USM;->fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 872
    return-void

    .line 840
    .end local v1    # "entry":Lorg/snmp4j/security/UsmUserEntry;
    .end local v3    # "userEngineID":Lorg/snmp4j/smi/OctetString;
    :cond_1
    iget-object v4, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v5

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getAuthenticationPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v6

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lorg/snmp4j/security/SecurityProtocols;->passwordToKey(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;[B)[B

    move-result-object v0

    goto :goto_0

    .line 850
    :cond_2
    iget-object v4, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getPrivacyProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v5

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v6

    invoke-virtual {p3}, Lorg/snmp4j/security/UsmUser;->getPrivacyPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v7

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v8

    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/snmp4j/security/SecurityProtocols;->passwordToKey(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;[B)[B

    move-result-object v2

    goto :goto_1

    .line 864
    :cond_3
    if-nez p2, :cond_4

    new-instance v3, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v3}, Lorg/snmp4j/smi/OctetString;-><init>()V

    .restart local v3    # "userEngineID":Lorg/snmp4j/smi/OctetString;
    :goto_3
    goto :goto_2

    .end local v3    # "userEngineID":Lorg/snmp4j/smi/OctetString;
    :cond_4
    move-object v3, p2

    goto :goto_3
.end method

.method public declared-synchronized addUsmUserListener(Lorg/snmp4j/event/UsmUserListener;)V
    .locals 2
    .param p1, "l"    # Lorg/snmp4j/event/UsmUserListener;

    .prologue
    .line 1042
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    if-nez v1, :cond_1

    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 1044
    .local v0, "v":Ljava/util/Vector;
    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1045
    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1046
    iput-object v0, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048
    :cond_0
    monitor-exit p0

    return-void

    .line 1042
    .end local v0    # "v":Ljava/util/Vector;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V
    .locals 1
    .param p1, "e"    # Lorg/snmp4j/event/CounterEvent;

    .prologue
    .line 804
    iget-object v0, p0, Lorg/snmp4j/security/USM;->counterSupport:Lorg/snmp4j/mp/CounterSupport;

    invoke-virtual {v0, p1}, Lorg/snmp4j/mp/CounterSupport;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 805
    return-void
.end method

.method protected fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V
    .locals 4
    .param p1, "e"    # Lorg/snmp4j/event/UsmUserEvent;

    .prologue
    .line 1069
    iget-object v3, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    if-eqz v3, :cond_0

    .line 1070
    iget-object v2, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    .line 1071
    .local v2, "listeners":Ljava/util/Vector;
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    .line 1072
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1073
    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/snmp4j/event/UsmUserListener;

    invoke-interface {v3, p1}, Lorg/snmp4j/event/UsmUserListener;->usmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 1072
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1076
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "listeners":Ljava/util/Vector;
    :cond_0
    return-void
.end method

.method public generateRequestMessage(I[BII[B[BILorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/security/SecurityParameters;Lorg/snmp4j/asn1/BEROutputStream;)I
    .locals 12
    .param p1, "snmpVersion"    # I
    .param p2, "globalData"    # [B
    .param p3, "maxMessageSize"    # I
    .param p4, "securityModel"    # I
    .param p5, "securityEngineID"    # [B
    .param p6, "securityName"    # [B
    .param p7, "securityLevel"    # I
    .param p8, "scopedPDU"    # Lorg/snmp4j/asn1/BERInputStream;
    .param p9, "securityParameters"    # Lorg/snmp4j/security/SecurityParameters;
    .param p10, "wholeMsg"    # Lorg/snmp4j/asn1/BEROutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    const/4 v9, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-virtual/range {v0 .. v11}, Lorg/snmp4j/security/USM;->generateResponseMessage(I[BII[B[BILorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/security/SecurityStateReference;Lorg/snmp4j/security/SecurityParameters;Lorg/snmp4j/asn1/BEROutputStream;)I

    move-result v0

    return v0
.end method

.method public generateResponseMessage(I[BII[B[BILorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/security/SecurityStateReference;Lorg/snmp4j/security/SecurityParameters;Lorg/snmp4j/asn1/BEROutputStream;)I
    .locals 29
    .param p1, "snmpVersion"    # I
    .param p2, "globalData"    # [B
    .param p3, "maxMessageSize"    # I
    .param p4, "securityModel"    # I
    .param p5, "securityEngineID"    # [B
    .param p6, "securityName"    # [B
    .param p7, "securityLevel"    # I
    .param p8, "scopedPDU"    # Lorg/snmp4j/asn1/BERInputStream;
    .param p9, "securityStateReference"    # Lorg/snmp4j/security/SecurityStateReference;
    .param p10, "securityParameters"    # Lorg/snmp4j/security/SecurityParameters;
    .param p11, "wholeMsg"    # Lorg/snmp4j/asn1/BEROutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    move-object/from16 v27, p10

    check-cast v27, Lorg/snmp4j/security/UsmSecurityParameters;

    .line 325
    .local v27, "usmSecurityParams":Lorg/snmp4j/security/UsmSecurityParameters;
    if-eqz p9, :cond_3

    move-object/from16 v28, p9

    .line 327
    check-cast v28, Lorg/snmp4j/security/UsmSecurityStateReference;

    .line 329
    .local v28, "usmSecurityStateReference":Lorg/snmp4j/security/UsmSecurityStateReference;
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getSecurityEngineID()[B

    move-result-object v3

    if-nez v3, :cond_0

    .line 330
    move-object/from16 v0, v27

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineID([B)V

    .line 331
    move-object/from16 v0, v28

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityStateReference;->setSecurityEngineID([B)V

    .line 333
    :cond_0
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getSecurityName()[B

    move-result-object v3

    if-nez v3, :cond_2

    .line 334
    new-instance v26, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, v26

    move-object/from16 v1, p6

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    .line 335
    .local v26, "userName":Lorg/snmp4j/smi/OctetString;
    invoke-virtual/range {v26 .. v26}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityStateReference;->setSecurityName([B)V

    .line 336
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->setUserName(Lorg/snmp4j/smi/OctetString;)V

    .line 338
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, p5

    invoke-direct {v3, v0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v3, v1}, Lorg/snmp4j/security/USM;->getSecurityName(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/smi/OctetString;

    move-result-object v24

    .line 341
    .local v24, "secName":Lorg/snmp4j/smi/OctetString;
    if-eqz v24, :cond_1

    invoke-virtual/range {v24 .. v24}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v3

    const/16 v5, 0x20

    if-gt v3, v5, :cond_1

    .line 343
    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->setUserName(Lorg/snmp4j/smi/OctetString;)V

    .line 350
    .end local v24    # "secName":Lorg/snmp4j/smi/OctetString;
    .end local v26    # "userName":Lorg/snmp4j/smi/OctetString;
    :cond_1
    :goto_0
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getAuthenticationProtocol()Lorg/snmp4j/security/AuthenticationProtocol;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationProtocol(Lorg/snmp4j/security/AuthenticationProtocol;)V

    .line 352
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getPrivacyProtocol()Lorg/snmp4j/security/PrivacyProtocol;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyProtocol(Lorg/snmp4j/security/PrivacyProtocol;)V

    .line 354
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getAuthenticationKey()[B

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationKey([B)V

    .line 356
    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getPrivacyKey()[B

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyKey([B)V

    .line 401
    .end local v28    # "usmSecurityStateReference":Lorg/snmp4j/security/UsmSecurityStateReference;
    :goto_1
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v3

    array-length v3, v3

    const/16 v5, 0x20

    if-le v3, v5, :cond_a

    .line 402
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Engine ID too long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    new-instance v6, Lorg/snmp4j/smi/OctetString;

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v8

    invoke-direct {v6, v8}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    invoke-virtual {v6}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 407
    const/16 v3, 0x579

    .line 533
    :goto_2
    return v3

    .line 348
    .restart local v28    # "usmSecurityStateReference":Lorg/snmp4j/security/UsmSecurityStateReference;
    :cond_2
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    invoke-virtual/range {v28 .. v28}, Lorg/snmp4j/security/UsmSecurityStateReference;->getSecurityName()[B

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setUserName(Lorg/snmp4j/smi/OctetString;)V

    goto :goto_0

    .line 359
    .end local v28    # "usmSecurityStateReference":Lorg/snmp4j/security/UsmSecurityStateReference;
    :cond_3
    new-instance v23, Lorg/snmp4j/smi/OctetString;

    invoke-direct/range {v23 .. v23}, Lorg/snmp4j/smi/OctetString;-><init>()V

    .line 360
    .local v23, "secEngineID":Lorg/snmp4j/smi/OctetString;
    if-eqz p5, :cond_4

    .line 361
    move-object/from16 v0, v23

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 363
    :cond_4
    new-instance v24, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, v24

    move-object/from16 v1, p6

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    .line 366
    .restart local v24    # "secName":Lorg/snmp4j/smi/OctetString;
    invoke-virtual/range {v23 .. v23}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v3

    if-nez v3, :cond_8

    .line 367
    invoke-virtual/range {p0 .. p0}, Lorg/snmp4j/security/USM;->isEngineDiscoveryEnabled()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 368
    new-instance v25, Lorg/snmp4j/security/UsmUserEntry;

    invoke-direct/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;-><init>()V

    .line 380
    .local v25, "user":Lorg/snmp4j/security/UsmUserEntry;
    :goto_3
    if-nez v25, :cond_9

    .line 381
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 382
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Security name not found for engineID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual/range {v23 .. v23}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ", securityName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 386
    :cond_5
    const/16 v3, 0x57c

    goto :goto_2

    .line 371
    .end local v25    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :cond_6
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 372
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "Engine ID unknown and discovery disabled"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 374
    :cond_7
    const/16 v3, 0x582

    goto :goto_2

    .line 378
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/snmp4j/security/USM;->getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v25

    .restart local v25    # "user":Lorg/snmp4j/security/UsmUserEntry;
    goto :goto_3

    .line 388
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v5

    invoke-virtual {v5}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/snmp4j/security/SecurityProtocols;->getAuthenticationProtocol(Lorg/snmp4j/smi/OID;)Lorg/snmp4j/security/AuthenticationProtocol;

    move-result-object v13

    .line 390
    .local v13, "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v5

    invoke-virtual {v5}, Lorg/snmp4j/security/UsmUser;->getPrivacyProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/snmp4j/security/SecurityProtocols;->getPrivacyProtocol(Lorg/snmp4j/smi/OID;)Lorg/snmp4j/security/PrivacyProtocol;

    move-result-object v22

    .line 392
    .local v22, "priv":Lorg/snmp4j/security/PrivacyProtocol;
    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationProtocol(Lorg/snmp4j/security/AuthenticationProtocol;)V

    .line 393
    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyProtocol(Lorg/snmp4j/security/PrivacyProtocol;)V

    .line 394
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;->getAuthenticationKey()[B

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationKey([B)V

    .line 395
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;->getPrivacyKey()[B

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyKey([B)V

    .line 396
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v3

    invoke-virtual {v3}, Lorg/snmp4j/security/UsmUser;->getSecurityName()Lorg/snmp4j/smi/OctetString;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setUserName(Lorg/snmp4j/smi/OctetString;)V

    .line 397
    invoke-virtual/range {v23 .. v23}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineID([B)V

    goto/16 :goto_1

    .line 409
    .end local v13    # "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    .end local v22    # "priv":Lorg/snmp4j/security/PrivacyProtocol;
    .end local v23    # "secEngineID":Lorg/snmp4j/smi/OctetString;
    .end local v24    # "secName":Lorg/snmp4j/smi/OctetString;
    .end local v25    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :cond_a
    move-object/from16 v0, p6

    array-length v3, v0

    const/16 v5, 0x20

    if-le v3, v5, :cond_b

    .line 410
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Security name too long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    new-instance v6, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, p6

    invoke-direct {v6, v0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    invoke-virtual {v6}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 414
    const/16 v3, 0x579

    goto/16 :goto_2

    .line 417
    :cond_b
    const/4 v3, 0x2

    move/from16 v0, p7

    if-lt v0, v3, :cond_c

    .line 418
    if-eqz p9, :cond_d

    .line 420
    invoke-virtual/range {p0 .. p0}, Lorg/snmp4j/security/USM;->getEngineBoots()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineBoots(I)V

    .line 421
    invoke-virtual/range {p0 .. p0}, Lorg/snmp4j/security/USM;->getEngineTime()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineTime(I)V

    .line 444
    :cond_c
    :goto_4
    const/4 v3, 0x2

    move/from16 v0, p7

    if-lt v0, v3, :cond_f

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthenticationProtocol()Lorg/snmp4j/security/AuthenticationProtocol;

    move-result-object v3

    if-nez v3, :cond_f

    .line 446
    const/16 v3, 0x57b

    goto/16 :goto_2

    .line 425
    :cond_d
    new-instance v23, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, v23

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    .line 426
    .restart local v23    # "secEngineID":Lorg/snmp4j/smi/OctetString;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Lorg/snmp4j/security/UsmTimeTable;->getTime(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmTimeEntry;

    move-result-object v20

    .line 427
    .local v20, "entry":Lorg/snmp4j/security/UsmTimeEntry;
    if-nez v20, :cond_e

    .line 428
    new-instance v20, Lorg/snmp4j/security/UsmTimeEntry;

    .end local v20    # "entry":Lorg/snmp4j/security/UsmTimeEntry;
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineBoots()I

    move-result v3

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineTime()I

    move-result v5

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v3, v5}, Lorg/snmp4j/security/UsmTimeEntry;-><init>(Lorg/snmp4j/smi/OctetString;II)V

    .line 434
    .restart local v20    # "entry":Lorg/snmp4j/security/UsmTimeEntry;
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lorg/snmp4j/security/UsmTimeTable;->addEntry(Lorg/snmp4j/security/UsmTimeEntry;)V

    goto :goto_4

    .line 437
    :cond_e
    invoke-virtual/range {v20 .. v20}, Lorg/snmp4j/security/UsmTimeEntry;->getEngineBoots()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineBoots(I)V

    .line 438
    invoke-virtual/range {v20 .. v20}, Lorg/snmp4j/security/UsmTimeEntry;->getLatestReceivedTime()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineTime(I)V

    goto :goto_4

    .line 449
    .end local v20    # "entry":Lorg/snmp4j/security/UsmTimeEntry;
    .end local v23    # "secEngineID":Lorg/snmp4j/smi/OctetString;
    :cond_f
    invoke-static/range {p8 .. p8}, Lorg/snmp4j/security/USM;->buildMessageBuffer(Lorg/snmp4j/asn1/BERInputStream;)[B

    move-result-object v4

    .line 451
    .local v4, "scopedPduBytes":[B
    const/4 v3, 0x3

    move/from16 v0, p7

    if-ne v0, v3, :cond_15

    .line 452
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getPrivacyProtocol()Lorg/snmp4j/security/PrivacyProtocol;

    move-result-object v3

    if-nez v3, :cond_11

    .line 453
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 454
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "Unsupported security level (missing or unsupported privacy protocol)"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 456
    :cond_10
    const/16 v3, 0x57b

    goto/16 :goto_2

    .line 458
    :cond_11
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "RFC3414 \u00a73.1.4.a Outgoing message needs to be encrypted"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 460
    new-instance v12, Lorg/snmp4j/security/DecryptParams;

    invoke-direct {v12}, Lorg/snmp4j/security/DecryptParams;-><init>()V

    .line 461
    .local v12, "decryptParams":Lorg/snmp4j/security/DecryptParams;
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getPrivacyProtocol()Lorg/snmp4j/security/PrivacyProtocol;

    move-result-object v3

    const/4 v5, 0x0

    array-length v6, v4

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getPrivacyKey()[B

    move-result-object v7

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineBoots()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineTime()I

    move-result v10

    int-to-long v10, v10

    invoke-interface/range {v3 .. v12}, Lorg/snmp4j/security/PrivacyProtocol;->encrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B

    move-result-object v18

    .line 468
    .local v18, "encryptedScopedPdu":[B
    if-nez v18, :cond_13

    .line 469
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 470
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "Encryption error"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 472
    :cond_12
    const/16 v3, 0x57d

    goto/16 :goto_2

    .line 474
    :cond_13
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    iget-object v5, v12, Lorg/snmp4j/security/DecryptParams;->array:[B

    invoke-direct {v3, v5}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyParameters(Lorg/snmp4j/smi/OctetString;)V

    .line 476
    new-instance v19, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    .line 477
    .local v19, "encryptedString":Lorg/snmp4j/smi/OctetString;
    new-instance v21, Lorg/snmp4j/asn1/BEROutputStream;

    invoke-virtual/range {v19 .. v19}, Lorg/snmp4j/smi/OctetString;->getBERLength()I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Lorg/snmp4j/asn1/BEROutputStream;-><init>(Ljava/nio/ByteBuffer;)V

    .line 479
    .local v21, "os":Lorg/snmp4j/asn1/BEROutputStream;
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/snmp4j/smi/OctetString;->encodeBER(Ljava/io/OutputStream;)V

    .line 480
    invoke-virtual/range {v21 .. v21}, Lorg/snmp4j/asn1/BEROutputStream;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 488
    .end local v12    # "decryptParams":Lorg/snmp4j/security/DecryptParams;
    .end local v18    # "encryptedScopedPdu":[B
    .end local v19    # "encryptedString":Lorg/snmp4j/smi/OctetString;
    .end local v21    # "os":Lorg/snmp4j/asn1/BEROutputStream;
    :goto_5
    const/4 v3, 0x2

    move/from16 v0, p7

    if-lt v0, v3, :cond_16

    .line 490
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v16, v0

    .line 492
    .local v16, "blank":[B
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Lorg/snmp4j/smi/OctetString;-><init>([B)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationParameters(Lorg/snmp4j/smi/OctetString;)V

    .line 493
    new-instance v3, Lorg/snmp4j/smi/Integer32;

    move/from16 v0, p1

    invoke-direct {v3, v0}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v3, v4, v0, v1}, Lorg/snmp4j/security/USM;->buildWholeMessage(Lorg/snmp4j/smi/Integer32;[B[BLorg/snmp4j/security/UsmSecurityParameters;)[B

    move-result-object v7

    .line 497
    .local v7, "wholeMessage":[B
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthParametersPosition()I

    move-result v3

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getSecurityParametersPosition()I

    move-result v5

    add-int v15, v3, v5

    .line 501
    .local v15, "authParamsPos":I
    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthenticationProtocol()Lorg/snmp4j/security/AuthenticationProtocol;

    move-result-object v5

    invoke-virtual/range {v27 .. v27}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthenticationKey()[B

    move-result-object v6

    const/4 v8, 0x0

    array-length v9, v7

    new-instance v10, Lorg/snmp4j/security/ByteArrayWindow;

    const/16 v3, 0xc

    invoke-direct {v10, v7, v15, v3}, Lorg/snmp4j/security/ByteArrayWindow;-><init>([BII)V

    invoke-interface/range {v5 .. v10}, Lorg/snmp4j/security/AuthenticationProtocol;->authenticate([B[BIILorg/snmp4j/security/ByteArrayWindow;)Z

    move-result v14

    .line 511
    .local v14, "authOK":Z
    if-nez v14, :cond_17

    .line 512
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 513
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "Outgoing message could not be authenticated"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 515
    :cond_14
    const/16 v3, 0x57f

    goto/16 :goto_2

    .line 483
    .end local v7    # "wholeMessage":[B
    .end local v14    # "authOK":Z
    .end local v15    # "authParamsPos":I
    .end local v16    # "blank":[B
    :cond_15
    sget-object v3, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v5, "RFC3414 \u00a73.1.4.b Outgoing message is not encrypted"

    invoke-interface {v3, v5}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 484
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v3}, Lorg/snmp4j/smi/OctetString;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setPrivacyParameters(Lorg/snmp4j/smi/OctetString;)V

    goto :goto_5

    .line 520
    :cond_16
    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineBoots(I)V

    .line 521
    new-instance v3, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v3}, Lorg/snmp4j/smi/OctetString;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthenticationParameters(Lorg/snmp4j/smi/OctetString;)V

    .line 522
    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityParameters;->setAuthoritativeEngineTime(I)V

    .line 525
    new-instance v3, Lorg/snmp4j/smi/Integer32;

    move/from16 v0, p1

    invoke-direct {v3, v0}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-static {v3, v4, v0, v1}, Lorg/snmp4j/security/USM;->buildWholeMessage(Lorg/snmp4j/smi/Integer32;[B[BLorg/snmp4j/security/UsmSecurityParameters;)[B

    move-result-object v7

    .line 529
    .restart local v7    # "wholeMessage":[B
    :cond_17
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    array-length v5, v7

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v17

    check-cast v17, Ljava/nio/ByteBuffer;

    .line 531
    .local v17, "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p11

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/snmp4j/asn1/BEROutputStream;->setBuffer(Ljava/nio/ByteBuffer;)V

    .line 533
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public getCounterSupport()Lorg/snmp4j/mp/CounterSupport;
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lorg/snmp4j/security/USM;->counterSupport:Lorg/snmp4j/mp/CounterSupport;

    return-object v0
.end method

.method public getEngineBoots()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmTimeTable;->getEngineBoots()I

    move-result v0

    return v0
.end method

.method public getEngineTime()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmTimeTable;->getEngineTime()I

    move-result v0

    return v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x3

    return v0
.end method

.method public getLocalEngineID()Lorg/snmp4j/smi/OctetString;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/snmp4j/security/USM;->localEngineID:Lorg/snmp4j/smi/OctetString;

    return-object v0
.end method

.method public getSecurityProtocols()Lorg/snmp4j/security/SecurityProtocols;
    .locals 1

    .prologue
    .line 1097
    iget-object v0, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    return-object v0
.end method

.method public getTimeTable()Lorg/snmp4j/security/UsmTimeTable;
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    return-object v0
.end method

.method public getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;
    .locals 9
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    const/4 v2, 0x0

    .line 252
    sget-object v0, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    sget-object v0, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "getUser(engineID="

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v8, ", securityName="

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v8, ")"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v0, p1, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v7

    .line 257
    .local v7, "entry":Lorg/snmp4j/security/UsmUserEntry;
    if-nez v7, :cond_6

    .line 258
    iget-object v0, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v0, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v7

    .line 259
    if-nez v7, :cond_2

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 260
    sget-object v0, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    sget-object v0, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "USM.getUser - User \'"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v8, "\' unknown"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 307
    :cond_1
    :goto_0
    return-object v2

    .line 266
    :cond_2
    if-eqz v7, :cond_3

    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 268
    :cond_3
    new-instance v7, Lorg/snmp4j/security/UsmUserEntry;

    .end local v7    # "entry":Lorg/snmp4j/security/UsmUserEntry;
    invoke-direct {v7}, Lorg/snmp4j/security/UsmUserEntry;-><init>()V

    .line 269
    .restart local v7    # "entry":Lorg/snmp4j/security/UsmUserEntry;
    invoke-virtual {v7, p2}, Lorg/snmp4j/security/UsmUserEntry;->setUserName(Lorg/snmp4j/smi/OctetString;)V

    .line 270
    new-instance v0, Lorg/snmp4j/security/UsmUser;

    move-object v1, p2

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lorg/snmp4j/security/UsmUser;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;)V

    invoke-virtual {v7, v0}, Lorg/snmp4j/security/UsmUserEntry;->setUsmUser(Lorg/snmp4j/security/UsmUser;)V

    move-object v2, v7

    .line 271
    goto :goto_0

    .line 275
    :cond_4
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v3

    .line 276
    .local v3, "authProtocolOID":Lorg/snmp4j/smi/OID;
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->getPrivacyProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v5

    .line 277
    .local v5, "privProtocolOID":Lorg/snmp4j/smi/OID;
    if-eqz v3, :cond_6

    .line 279
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->isLocalized()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 280
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->getAuthenticationPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    .line 288
    .local v4, "authKey":[B
    :goto_1
    const/4 v6, 0x0

    .line 289
    .local v6, "privKey":[B
    if-eqz v5, :cond_5

    .line 290
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->isLocalized()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 291
    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->getPrivacyPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v6

    .line 300
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lorg/snmp4j/security/USM;->addLocalizedUser([BLorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OID;[BLorg/snmp4j/smi/OID;[B)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v7

    .end local v3    # "authProtocolOID":Lorg/snmp4j/smi/OID;
    .end local v4    # "authKey":[B
    .end local v5    # "privProtocolOID":Lorg/snmp4j/smi/OID;
    .end local v6    # "privKey":[B
    :cond_6
    move-object v2, v7

    .line 307
    goto :goto_0

    .line 284
    .restart local v3    # "authProtocolOID":Lorg/snmp4j/smi/OID;
    .restart local v5    # "privProtocolOID":Lorg/snmp4j/smi/OID;
    :cond_7
    iget-object v0, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v1

    invoke-virtual {v1}, Lorg/snmp4j/security/UsmUser;->getAuthenticationPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v1

    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lorg/snmp4j/security/SecurityProtocols;->passwordToKey(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;[B)[B

    move-result-object v4

    .restart local v4    # "authKey":[B
    goto :goto_1

    .line 294
    .restart local v6    # "privKey":[B
    :cond_8
    iget-object v0, p0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual {v7}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v1

    invoke-virtual {v1}, Lorg/snmp4j/security/UsmUser;->getPrivacyPassphrase()Lorg/snmp4j/smi/OctetString;

    move-result-object v1

    invoke-virtual {p1}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v2

    invoke-virtual {v0, v5, v3, v1, v2}, Lorg/snmp4j/security/SecurityProtocols;->passwordToKey(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/OctetString;[B)[B

    move-result-object v6

    goto :goto_2
.end method

.method public getUserTable()Lorg/snmp4j/security/UsmUserTable;
    .locals 1

    .prologue
    .line 926
    iget-object v0, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    return-object v0
.end method

.method public hasUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Z
    .locals 2
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 241
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p1, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 242
    .local v0, "entry":Lorg/snmp4j/security/UsmUserEntry;
    if-nez v0, :cond_0

    .line 243
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p2}, Lorg/snmp4j/security/UsmUserTable;->getUser(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 244
    if-nez v0, :cond_0

    invoke-virtual {p2}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 245
    const/4 v1, 0x0

    .line 248
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEngineDiscoveryEnabled()Z
    .locals 1

    .prologue
    .line 1007
    iget-boolean v0, p0, Lorg/snmp4j/security/USM;->engineDiscoveryEnabled:Z

    return v0
.end method

.method public newSecurityParametersInstance()Lorg/snmp4j/security/SecurityParameters;
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lorg/snmp4j/security/UsmSecurityParameters;

    invoke-direct {v0}, Lorg/snmp4j/security/UsmSecurityParameters;-><init>()V

    return-object v0
.end method

.method public newSecurityStateReference()Lorg/snmp4j/security/SecurityStateReference;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lorg/snmp4j/security/UsmSecurityStateReference;

    invoke-direct {v0}, Lorg/snmp4j/security/UsmSecurityStateReference;-><init>()V

    return-object v0
.end method

.method public processIncomingMsg(IILorg/snmp4j/security/SecurityParameters;Lorg/snmp4j/security/SecurityModel;ILorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/asn1/BEROutputStream;Lorg/snmp4j/smi/Integer32;Lorg/snmp4j/security/SecurityStateReference;Lorg/snmp4j/mp/StatusInformation;)I
    .locals 35
    .param p1, "snmpVersion"    # I
    .param p2, "maxMessageSize"    # I
    .param p3, "securityParameters"    # Lorg/snmp4j/security/SecurityParameters;
    .param p4, "securityModel"    # Lorg/snmp4j/security/SecurityModel;
    .param p5, "securityLevel"    # I
    .param p6, "wholeMsg"    # Lorg/snmp4j/asn1/BERInputStream;
    .param p7, "securityEngineID"    # Lorg/snmp4j/smi/OctetString;
    .param p8, "securityName"    # Lorg/snmp4j/smi/OctetString;
    .param p9, "scopedPDU"    # Lorg/snmp4j/asn1/BEROutputStream;
    .param p10, "maxSizeResponseScopedPDU"    # Lorg/snmp4j/smi/Integer32;
    .param p11, "securityStateReference"    # Lorg/snmp4j/security/SecurityStateReference;
    .param p12, "statusInfo"    # Lorg/snmp4j/mp/StatusInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 569
    move-object/from16 v33, p3

    check-cast v33, Lorg/snmp4j/security/UsmSecurityParameters;

    .local v33, "usmSecurityParameters":Lorg/snmp4j/security/UsmSecurityParameters;
    move-object/from16 v34, p11

    .line 571
    check-cast v34, Lorg/snmp4j/security/UsmSecurityStateReference;

    .line 573
    .local v34, "usmSecurityStateReference":Lorg/snmp4j/security/UsmSecurityStateReference;
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 575
    invoke-static/range {p6 .. p6}, Lorg/snmp4j/security/USM;->buildMessageBuffer(Lorg/snmp4j/asn1/BERInputStream;)[B

    move-result-object v5

    .line 577
    .local v5, "message":[B
    invoke-virtual/range {p7 .. p7}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    invoke-virtual/range {p0 .. p0}, Lorg/snmp4j/security/USM;->isEngineDiscoveryEnabled()Z

    move-result v8

    move-object/from16 v0, p7

    invoke-virtual {v4, v0, v8}, Lorg/snmp4j/security/UsmTimeTable;->checkEngineID(Lorg/snmp4j/smi/OctetString;Z)I

    move-result v4

    if-eqz v4, :cond_3

    .line 582
    :cond_0
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 583
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.3 Unknown engine ID: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {p7 .. p7}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 586
    :cond_1
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineID()[B

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 588
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 590
    if-eqz p12, :cond_2

    .line 591
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsUnknownEngineIDs:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 594
    .local v18, "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 595
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    move/from16 v0, p5

    invoke-direct {v4, v0}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 596
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    .line 599
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :cond_2
    const/16 v31, 0x582

    .line 800
    :goto_0
    return v31

    .line 602
    :cond_3
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 604
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getScopedPduPosition()I

    move-result v27

    .line 607
    .local v27, "scopedPDUPosition":I
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    if-gtz v4, :cond_4

    const/4 v4, 0x1

    move/from16 v0, p5

    if-le v0, v4, :cond_7

    .line 609
    :cond_4
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/security/USM;->getSecurityName(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/smi/OctetString;

    move-result-object v30

    .line 611
    .local v30, "secName":Lorg/snmp4j/smi/OctetString;
    if-nez v30, :cond_9

    .line 612
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 613
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.4 Unknown security name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {p8 .. p8}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 616
    :cond_5
    if-eqz p12, :cond_6

    .line 617
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsUnknownUserNames:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 619
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 620
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x1

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 621
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    .line 624
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :cond_6
    const/16 v31, 0x57c

    goto/16 :goto_0

    .line 628
    .end local v30    # "secName":Lorg/snmp4j/smi/OctetString;
    :cond_7
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 629
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v8, "Accepting zero length security name"

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 631
    :cond_8
    const/4 v4, 0x0

    new-array v4, v4, [B

    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/OctetString;->setValue([B)V

    .line 634
    :cond_9
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v4

    if-gtz v4, :cond_a

    const/4 v4, 0x1

    move/from16 v0, p5

    if-le v0, v4, :cond_18

    .line 636
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p8

    invoke-virtual {v0, v1, v2}, Lorg/snmp4j/security/USM;->getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v32

    .line 637
    .local v32, "user":Lorg/snmp4j/security/UsmUserEntry;
    if-nez v32, :cond_d

    .line 638
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 639
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.4 Unknown security name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {p8 .. p8}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " for engine ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {p7 .. p7}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 643
    :cond_b
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsUnknownUserNames:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 645
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 646
    if-eqz p12, :cond_c

    .line 647
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x1

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 648
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    .line 651
    :cond_c
    const/16 v31, 0x57c

    goto/16 :goto_0

    .line 654
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :cond_d
    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    invoke-virtual {v4}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lorg/snmp4j/security/UsmSecurityStateReference;->setUserName([B)V

    .line 656
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/security/UsmUser;->getAuthenticationProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/snmp4j/security/SecurityProtocols;->getAuthenticationProtocol(Lorg/snmp4j/smi/OID;)Lorg/snmp4j/security/AuthenticationProtocol;

    move-result-object v3

    .line 659
    .local v3, "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/snmp4j/security/USM;->securityProtocols:Lorg/snmp4j/security/SecurityProtocols;

    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v8

    invoke-virtual {v8}, Lorg/snmp4j/security/UsmUser;->getPrivacyProtocol()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/snmp4j/security/SecurityProtocols;->getPrivacyProtocol(Lorg/snmp4j/smi/OID;)Lorg/snmp4j/security/PrivacyProtocol;

    move-result-object v23

    .line 663
    .local v23, "priv":Lorg/snmp4j/security/PrivacyProtocol;
    const/4 v4, 0x2

    move/from16 v0, p5

    if-lt v0, v4, :cond_e

    if-eqz v3, :cond_f

    :cond_e
    const/4 v4, 0x3

    move/from16 v0, p5

    if-lt v0, v4, :cond_11

    if-nez v23, :cond_11

    .line 665
    :cond_f
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 666
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.5 - Unsupported security level: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 669
    :cond_10
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsUnsupportedSecLevels:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 671
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 672
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x1

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 673
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    .line 675
    const/16 v31, 0x57b

    goto/16 :goto_0

    .line 677
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :cond_11
    const/4 v4, 0x2

    move/from16 v0, p5

    if-lt v0, v4, :cond_17

    .line 678
    if-eqz p12, :cond_14

    .line 679
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthParametersPosition()I

    move-result v4

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getSecurityParametersPosition()I

    move-result v8

    add-int v14, v4, v8

    .line 682
    .local v14, "authParamsPos":I
    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getAuthenticationKey()[B

    move-result-object v4

    const/4 v6, 0x0

    array-length v7, v5

    new-instance v8, Lorg/snmp4j/security/ByteArrayWindow;

    const/16 v9, 0xc

    invoke-direct {v8, v5, v14, v9}, Lorg/snmp4j/security/ByteArrayWindow;-><init>([BII)V

    invoke-interface/range {v3 .. v8}, Lorg/snmp4j/security/AuthenticationProtocol;->isAuthentic([B[BIILorg/snmp4j/security/ByteArrayWindow;)Z

    move-result v15

    .line 687
    .local v15, "authentic":Z
    if-nez v15, :cond_13

    .line 688
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 689
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.6 Wrong digest -> authentication failure: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthenticationParameters()Lorg/snmp4j/smi/OctetString;

    move-result-object v9

    invoke-virtual {v9}, Lorg/snmp4j/smi/OctetString;->toHexString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 693
    :cond_12
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsWrongDigests:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 695
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 696
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x1

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 697
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    .line 699
    const/16 v31, 0x580

    goto/16 :goto_0

    .line 701
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :cond_13
    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getAuthenticationKey()[B

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lorg/snmp4j/security/UsmSecurityStateReference;->setAuthenticationKey([B)V

    .line 702
    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getPrivacyKey()[B

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lorg/snmp4j/security/UsmSecurityStateReference;->setPrivacyKey([B)V

    .line 703
    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Lorg/snmp4j/security/UsmSecurityStateReference;->setAuthenticationProtocol(Lorg/snmp4j/security/AuthenticationProtocol;)V

    .line 704
    move-object/from16 v0, v34

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityStateReference;->setPrivacyProtocol(Lorg/snmp4j/security/PrivacyProtocol;)V

    .line 706
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    new-instance v8, Lorg/snmp4j/security/UsmTimeEntry;

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineBoots()I

    move-result v9

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineTime()I

    move-result v10

    move-object/from16 v0, p7

    invoke-direct {v8, v0, v9, v10}, Lorg/snmp4j/security/UsmTimeEntry;-><init>(Lorg/snmp4j/smi/OctetString;II)V

    invoke-virtual {v4, v8}, Lorg/snmp4j/security/UsmTimeTable;->checkTime(Lorg/snmp4j/security/UsmTimeEntry;)I

    move-result v31

    .line 710
    .local v31, "status":I
    packed-switch v31, :pswitch_data_0

    .line 742
    .end local v14    # "authParamsPos":I
    .end local v15    # "authentic":Z
    .end local v31    # "status":I
    :cond_14
    const/4 v4, 0x3

    move/from16 v0, p5

    if-lt v0, v4, :cond_16

    .line 743
    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getPrivacyParameters()Lorg/snmp4j/smi/OctetString;

    move-result-object v24

    .line 744
    .local v24, "privParams":Lorg/snmp4j/smi/OctetString;
    new-instance v13, Lorg/snmp4j/security/DecryptParams;

    invoke-virtual/range {v24 .. v24}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    const/4 v8, 0x0

    invoke-virtual/range {v24 .. v24}, Lorg/snmp4j/smi/OctetString;->length()I

    move-result v9

    invoke-direct {v13, v4, v8, v9}, Lorg/snmp4j/security/DecryptParams;-><init>([BII)V

    .line 747
    .local v13, "decryptParams":Lorg/snmp4j/security/DecryptParams;
    :try_start_0
    array-length v4, v5

    sub-int v26, v4, v27

    .line 748
    .local v26, "scopedPDUHeaderLength":I
    move/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v5, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v16

    .line 750
    .local v16, "bis":Ljava/nio/ByteBuffer;
    new-instance v25, Lorg/snmp4j/asn1/BERInputStream;

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/snmp4j/asn1/BERInputStream;-><init>(Ljava/nio/ByteBuffer;)V

    .line 751
    .local v25, "scopedPDUHeader":Lorg/snmp4j/asn1/BERInputStream;
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/asn1/BERInputStream;->getPosition()J

    move-result-wide v20

    .line 752
    .local v20, "headerStartingPosition":J
    new-instance v4, Lorg/snmp4j/asn1/BER$MutableByte;

    invoke-direct {v4}, Lorg/snmp4j/asn1/BER$MutableByte;-><init>()V

    move-object/from16 v0, v25

    invoke-static {v0, v4}, Lorg/snmp4j/asn1/BER;->decodeHeader(Lorg/snmp4j/asn1/BERInputStream;Lorg/snmp4j/asn1/BER$MutableByte;)I

    move-result v7

    .line 754
    .local v7, "scopedPDULength":I
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/asn1/BERInputStream;->getPosition()J

    move-result-wide v8

    sub-long v8, v8, v20

    long-to-int v4, v8

    add-int v6, v27, v4

    .line 757
    .local v6, "scopedPDUPayloadPosition":I
    invoke-virtual/range {v25 .. v25}, Lorg/snmp4j/asn1/BERInputStream;->close()V

    .line 758
    const/16 v25, 0x0

    .line 759
    invoke-virtual/range {v32 .. v32}, Lorg/snmp4j/security/UsmUserEntry;->getPrivacyKey()[B

    move-result-object v8

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineBoots()I

    move-result v4

    int-to-long v9, v4

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineTime()I

    move-result v4

    int-to-long v11, v4

    move-object/from16 v4, v23

    invoke-interface/range {v4 .. v13}, Lorg/snmp4j/security/PrivacyProtocol;->decrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B

    move-result-object v28

    .line 765
    .local v28, "scopedPduBytes":[B
    invoke-static/range {v28 .. v28}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 766
    .local v17, "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p9

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/snmp4j/asn1/BEROutputStream;->setFilledBuffer(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 794
    .end local v3    # "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    .end local v6    # "scopedPDUPayloadPosition":I
    .end local v7    # "scopedPDULength":I
    .end local v13    # "decryptParams":Lorg/snmp4j/security/DecryptParams;
    .end local v16    # "bis":Ljava/nio/ByteBuffer;
    .end local v20    # "headerStartingPosition":J
    .end local v23    # "priv":Lorg/snmp4j/security/PrivacyProtocol;
    .end local v24    # "privParams":Lorg/snmp4j/smi/OctetString;
    .end local v25    # "scopedPDUHeader":Lorg/snmp4j/asn1/BERInputStream;
    .end local v26    # "scopedPDUHeaderLength":I
    .end local v28    # "scopedPduBytes":[B
    .end local v32    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :goto_1
    move-object/from16 v0, v33

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmSecurityParameters;->getBERMaxLength(I)I

    move-result v22

    .line 796
    .local v22, "maxSecParamsOverhead":I
    sub-int v4, p2, v22

    move-object/from16 v0, p10

    invoke-virtual {v0, v4}, Lorg/snmp4j/smi/Integer32;->setValue(I)V

    .line 799
    invoke-virtual/range {p8 .. p8}, Lorg/snmp4j/smi/OctetString;->getValue()[B

    move-result-object v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lorg/snmp4j/security/UsmSecurityStateReference;->setSecurityName([B)V

    .line 800
    const/16 v31, 0x0

    goto/16 :goto_0

    .line 712
    .end local v17    # "buf":Ljava/nio/ByteBuffer;
    .end local v22    # "maxSecParamsOverhead":I
    .restart local v3    # "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    .restart local v14    # "authParamsPos":I
    .restart local v15    # "authentic":Z
    .restart local v23    # "priv":Lorg/snmp4j/security/PrivacyProtocol;
    .restart local v31    # "status":I
    .restart local v32    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :pswitch_0
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.7.a Not in time window; engineID=\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    move-object/from16 v0, p7

    invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\', engineBoots="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineBoots()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ", engineTime="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {v33 .. v33}, Lorg/snmp4j/security/UsmSecurityParameters;->getAuthoritativeEngineTime()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 718
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsNotInTimeWindows:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 720
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 721
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x2

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 722
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    goto/16 :goto_0

    .line 727
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    :pswitch_1
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v4}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 728
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC3414 \u00a73.2.7.b - Unkown engine ID: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    move-object/from16 v0, p7

    invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 731
    :cond_15
    new-instance v18, Lorg/snmp4j/event/CounterEvent;

    sget-object v4, Lorg/snmp4j/mp/SnmpConstants;->usmStatsUnknownEngineIDs:Lorg/snmp4j/smi/OID;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/snmp4j/event/CounterEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/smi/OID;)V

    .line 733
    .restart local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/USM;->fireIncrementCounter(Lorg/snmp4j/event/CounterEvent;)V

    .line 734
    new-instance v4, Lorg/snmp4j/smi/Integer32;

    const/4 v8, 0x2

    invoke-direct {v4, v8}, Lorg/snmp4j/smi/Integer32;-><init>(I)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setSecurityLevel(Lorg/snmp4j/smi/Integer32;)V

    .line 735
    new-instance v4, Lorg/snmp4j/smi/VariableBinding;

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getOid()Lorg/snmp4j/smi/OID;

    move-result-object v8

    invoke-virtual/range {v18 .. v18}, Lorg/snmp4j/event/CounterEvent;->getCurrentValue()Lorg/snmp4j/smi/Variable;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lorg/snmp4j/smi/VariableBinding;-><init>(Lorg/snmp4j/smi/OID;Lorg/snmp4j/smi/Variable;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lorg/snmp4j/mp/StatusInformation;->setErrorIndication(Lorg/snmp4j/smi/VariableBinding;)V

    goto/16 :goto_0

    .line 768
    .end local v14    # "authParamsPos":I
    .end local v15    # "authentic":Z
    .end local v18    # "event":Lorg/snmp4j/event/CounterEvent;
    .end local v31    # "status":I
    .restart local v13    # "decryptParams":Lorg/snmp4j/security/DecryptParams;
    .restart local v24    # "privParams":Lorg/snmp4j/smi/OctetString;
    :catch_0
    move-exception v19

    .line 769
    .local v19, "ex":Ljava/lang/Exception;
    sget-object v4, Lorg/snmp4j/security/USM;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "RFC 3414 \u00a73.2.8 Decryption error: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 770
    const/16 v31, 0x57e

    goto/16 :goto_0

    .line 774
    .end local v13    # "decryptParams":Lorg/snmp4j/security/DecryptParams;
    .end local v19    # "ex":Ljava/lang/Exception;
    .end local v24    # "privParams":Lorg/snmp4j/smi/OctetString;
    :cond_16
    array-length v4, v5

    sub-int v29, v4, v27

    .line 775
    .local v29, "scopedPduLength":I
    move/from16 v0, v27

    move/from16 v1, v29

    invoke-static {v5, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 777
    .restart local v17    # "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p9

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/snmp4j/asn1/BEROutputStream;->setFilledBuffer(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    .line 781
    .end local v17    # "buf":Ljava/nio/ByteBuffer;
    .end local v29    # "scopedPduLength":I
    :cond_17
    array-length v4, v5

    sub-int v29, v4, v27

    .line 782
    .restart local v29    # "scopedPduLength":I
    move/from16 v0, v27

    move/from16 v1, v29

    invoke-static {v5, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 784
    .restart local v17    # "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p9

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/snmp4j/asn1/BEROutputStream;->setFilledBuffer(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    .line 788
    .end local v3    # "auth":Lorg/snmp4j/security/AuthenticationProtocol;
    .end local v17    # "buf":Ljava/nio/ByteBuffer;
    .end local v23    # "priv":Lorg/snmp4j/security/PrivacyProtocol;
    .end local v29    # "scopedPduLength":I
    .end local v32    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :cond_18
    array-length v4, v5

    sub-int v29, v4, v27

    .line 789
    .restart local v29    # "scopedPduLength":I
    move/from16 v0, v27

    move/from16 v1, v29

    invoke-static {v5, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v17

    .line 791
    .restart local v17    # "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p9

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/snmp4j/asn1/BEROutputStream;->setFilledBuffer(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_1

    .line 710
    nop

    :pswitch_data_0
    .packed-switch 0x582
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public removeAllUsers()V
    .locals 3

    .prologue
    .line 965
    iget-object v0, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUserTable;->clear()V

    .line 966
    new-instance v0, Lorg/snmp4j/event/UsmUserEvent;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p0, v1, v2}, Lorg/snmp4j/event/UsmUserEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/security/UsmUserEntry;I)V

    invoke-virtual {p0, v0}, Lorg/snmp4j/security/USM;->fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 967
    return-void
.end method

.method public removeEngineTime(Lorg/snmp4j/smi/OctetString;)V
    .locals 1
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 1060
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    invoke-virtual {v0, p1}, Lorg/snmp4j/security/UsmTimeTable;->removeEntry(Lorg/snmp4j/smi/OctetString;)V

    .line 1061
    return-void
.end method

.method public removeUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUser;
    .locals 3
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "userName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 953
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p1, p2}, Lorg/snmp4j/security/UsmUserTable;->removeUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 954
    .local v0, "entry":Lorg/snmp4j/security/UsmUserEntry;
    if-eqz v0, :cond_0

    .line 955
    new-instance v1, Lorg/snmp4j/event/UsmUserEvent;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v0, v2}, Lorg/snmp4j/event/UsmUserEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/security/UsmUserEntry;I)V

    invoke-virtual {p0, v1}, Lorg/snmp4j/security/USM;->fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 956
    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v1

    .line 958
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized removeUsmUserListener(Lorg/snmp4j/event/UsmUserListener;)V
    .locals 2
    .param p1, "l"    # Lorg/snmp4j/event/UsmUserListener;

    .prologue
    .line 1026
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    iget-object v1, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 1028
    .local v0, "v":Ljava/util/Vector;
    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1029
    iput-object v0, p0, Lorg/snmp4j/security/USM;->usmUserListeners:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1031
    .end local v0    # "v":Ljava/util/Vector;
    :cond_0
    monitor-exit p0

    return-void

    .line 1026
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setCounterSupport(Lorg/snmp4j/mp/CounterSupport;)V
    .locals 1
    .param p1, "counterSupport"    # Lorg/snmp4j/mp/CounterSupport;

    .prologue
    .line 1107
    if-nez p1, :cond_0

    .line 1108
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1110
    :cond_0
    iput-object p1, p0, Lorg/snmp4j/security/USM;->counterSupport:Lorg/snmp4j/mp/CounterSupport;

    .line 1111
    return-void
.end method

.method public setEngineBoots(I)V
    .locals 1
    .param p1, "engineBoots"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    invoke-virtual {v0, p1}, Lorg/snmp4j/security/UsmTimeTable;->setEngineBoots(I)V

    .line 119
    return-void
.end method

.method public setEngineDiscoveryEnabled(Z)V
    .locals 0
    .param p1, "engineDiscoveryEnabled"    # Z

    .prologue
    .line 1017
    iput-boolean p1, p0, Lorg/snmp4j/security/USM;->engineDiscoveryEnabled:Z

    .line 1018
    return-void
.end method

.method public setLocalEngine(Lorg/snmp4j/smi/OctetString;II)V
    .locals 2
    .param p1, "localEngineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "engineBoots"    # I
    .param p3, "engineTime"    # I

    .prologue
    .line 97
    iput-object p1, p0, Lorg/snmp4j/security/USM;->localEngineID:Lorg/snmp4j/smi/OctetString;

    .line 98
    iget-object v0, p0, Lorg/snmp4j/security/USM;->timeTable:Lorg/snmp4j/security/UsmTimeTable;

    new-instance v1, Lorg/snmp4j/security/UsmTimeEntry;

    invoke-direct {v1, p1, p2, p3}, Lorg/snmp4j/security/UsmTimeEntry;-><init>(Lorg/snmp4j/smi/OctetString;II)V

    invoke-virtual {v0, v1}, Lorg/snmp4j/security/UsmTimeTable;->setLocalTime(Lorg/snmp4j/security/UsmTimeEntry;)V

    .line 100
    return-void
.end method

.method public setUsers([Lorg/snmp4j/security/UsmUser;)V
    .locals 5
    .param p1, "users"    # [Lorg/snmp4j/security/UsmUser;

    .prologue
    .line 900
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 901
    :cond_0
    iget-object v3, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v3}, Lorg/snmp4j/security/UsmUserTable;->clear()V

    .line 913
    :goto_0
    return-void

    .line 904
    :cond_1
    new-instance v2, Ljava/util/Vector;

    array-length v3, p1

    invoke-direct {v2, v3}, Ljava/util/Vector;-><init>(I)V

    .line 905
    .local v2, "v":Ljava/util/Vector;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_2

    .line 906
    new-instance v0, Lorg/snmp4j/security/UsmUserEntry;

    aget-object v3, p1, v1

    invoke-virtual {v3}, Lorg/snmp4j/security/UsmUser;->getSecurityName()Lorg/snmp4j/smi/OctetString;

    move-result-object v4

    aget-object v3, p1, v1

    invoke-virtual {v3}, Lorg/snmp4j/security/UsmUser;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/snmp4j/security/UsmUser;

    invoke-direct {v0, v4, v3}, Lorg/snmp4j/security/UsmUserEntry;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/security/UsmUser;)V

    .line 909
    .local v0, "entry":Lorg/snmp4j/security/UsmUserEntry;
    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 905
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 911
    .end local v0    # "entry":Lorg/snmp4j/security/UsmUserEntry;
    :cond_2
    iget-object v3, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v3, v2}, Lorg/snmp4j/security/UsmUserTable;->setUsers(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public updateUser(Lorg/snmp4j/security/UsmUserEntry;)V
    .locals 3
    .param p1, "entry"    # Lorg/snmp4j/security/UsmUserEntry;

    .prologue
    .line 884
    iget-object v1, p0, Lorg/snmp4j/security/USM;->userTable:Lorg/snmp4j/security/UsmUserTable;

    invoke-virtual {v1, p1}, Lorg/snmp4j/security/UsmUserTable;->addUser(Lorg/snmp4j/security/UsmUserEntry;)Lorg/snmp4j/security/UsmUserEntry;

    move-result-object v0

    .line 885
    .local v0, "oldEntry":Lorg/snmp4j/security/UsmUserEntry;
    new-instance v2, Lorg/snmp4j/event/UsmUserEvent;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v2, p0, p1, v1}, Lorg/snmp4j/event/UsmUserEvent;-><init>(Ljava/lang/Object;Lorg/snmp4j/security/UsmUserEntry;I)V

    invoke-virtual {p0, v2}, Lorg/snmp4j/security/USM;->fireUsmUserChange(Lorg/snmp4j/event/UsmUserEvent;)V

    .line 889
    return-void

    .line 885
    :cond_0
    const/4 v1, 0x3

    goto :goto_0
.end method
