.class public Lorg/snmp4j/security/UsmSecurityStateReference;
.super Ljava/lang/Object;
.source "UsmSecurityStateReference.java"

# interfaces
.implements Lorg/snmp4j/security/SecurityStateReference;


# instance fields
.field private authenticationKey:[B

.field private authenticationProtocol:Lorg/snmp4j/security/AuthenticationProtocol;

.field private privacyKey:[B

.field private privacyProtocol:Lorg/snmp4j/security/PrivacyProtocol;

.field private securityEngineID:[B

.field private securityLevel:I

.field private securityName:[B

.field private userName:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public getAuthenticationKey()[B
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->authenticationKey:[B

    return-object v0
.end method

.method public getAuthenticationProtocol()Lorg/snmp4j/security/AuthenticationProtocol;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->authenticationProtocol:Lorg/snmp4j/security/AuthenticationProtocol;

    return-object v0
.end method

.method public getPrivacyKey()[B
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->privacyKey:[B

    return-object v0
.end method

.method public getPrivacyProtocol()Lorg/snmp4j/security/PrivacyProtocol;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->privacyProtocol:Lorg/snmp4j/security/PrivacyProtocol;

    return-object v0
.end method

.method public getSecurityEngineID()[B
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityEngineID:[B

    return-object v0
.end method

.method public getSecurityLevel()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityLevel:I

    return v0
.end method

.method public getSecurityName()[B
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityName:[B

    return-object v0
.end method

.method public getUserName()[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->userName:[B

    return-object v0
.end method

.method public setAuthenticationKey([B)V
    .locals 0
    .param p1, "authenticationKey"    # [B

    .prologue
    .line 77
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->authenticationKey:[B

    .line 78
    return-void
.end method

.method public setAuthenticationProtocol(Lorg/snmp4j/security/AuthenticationProtocol;)V
    .locals 0
    .param p1, "authenticationProtocol"    # Lorg/snmp4j/security/AuthenticationProtocol;

    .prologue
    .line 65
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->authenticationProtocol:Lorg/snmp4j/security/AuthenticationProtocol;

    .line 66
    return-void
.end method

.method public setPrivacyKey([B)V
    .locals 0
    .param p1, "privacyKey"    # [B

    .prologue
    .line 83
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->privacyKey:[B

    .line 84
    return-void
.end method

.method public setPrivacyProtocol(Lorg/snmp4j/security/PrivacyProtocol;)V
    .locals 0
    .param p1, "privacyProtocol"    # Lorg/snmp4j/security/PrivacyProtocol;

    .prologue
    .line 71
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->privacyProtocol:Lorg/snmp4j/security/PrivacyProtocol;

    .line 72
    return-void
.end method

.method public setSecurityEngineID([B)V
    .locals 0
    .param p1, "securityEngineID"    # [B

    .prologue
    .line 59
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityEngineID:[B

    .line 60
    return-void
.end method

.method public setSecurityLevel(I)V
    .locals 0
    .param p1, "securityLevel"    # I

    .prologue
    .line 89
    iput p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityLevel:I

    .line 90
    return-void
.end method

.method public setSecurityName([B)V
    .locals 0
    .param p1, "securityName"    # [B

    .prologue
    .line 53
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->securityName:[B

    .line 54
    return-void
.end method

.method public setUserName([B)V
    .locals 0
    .param p1, "userName"    # [B

    .prologue
    .line 50
    iput-object p1, p0, Lorg/snmp4j/security/UsmSecurityStateReference;->userName:[B

    .line 51
    return-void
.end method
