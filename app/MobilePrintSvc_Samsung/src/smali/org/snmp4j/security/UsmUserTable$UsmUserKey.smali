.class public Lorg/snmp4j/security/UsmUserTable$UsmUserKey;
.super Ljava/lang/Object;
.source "UsmUserTable.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/snmp4j/security/UsmUserTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsmUserKey"
.end annotation


# instance fields
.field engineID:Lorg/snmp4j/smi/OctetString;

.field securityName:Lorg/snmp4j/smi/OctetString;


# direct methods
.method public constructor <init>(Lorg/snmp4j/security/UsmUserEntry;)V
    .locals 1
    .param p1, "entry"    # Lorg/snmp4j/security/UsmUserEntry;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-virtual {p1}, Lorg/snmp4j/security/UsmUserEntry;->getEngineID()Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->setEngineID(Lorg/snmp4j/smi/OctetString;)V

    .line 133
    invoke-virtual {p1}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/snmp4j/security/UsmUser;->getSecurityName()Lorg/snmp4j/smi/OctetString;

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->securityName:Lorg/snmp4j/smi/OctetString;

    .line 134
    return-void
.end method

.method public constructor <init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)V
    .locals 0
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    invoke-direct {p0, p1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->setEngineID(Lorg/snmp4j/smi/OctetString;)V

    .line 138
    iput-object p2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->securityName:Lorg/snmp4j/smi/OctetString;

    .line 139
    return-void
.end method

.method private setEngineID(Lorg/snmp4j/smi/OctetString;)V
    .locals 1
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 142
    if-nez p1, :cond_0

    .line 143
    new-instance v0, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v0}, Lorg/snmp4j/smi/OctetString;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    iput-object p1, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    goto :goto_0
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 162
    instance-of v2, p1, Lorg/snmp4j/security/UsmUserEntry;

    if-eqz v2, :cond_1

    .line 163
    new-instance v2, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    check-cast p1, Lorg/snmp4j/security/UsmUserEntry;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-direct {v2, p1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/security/UsmUserEntry;)V

    invoke-virtual {p0, v2}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->compareTo(Ljava/lang/Object;)I

    move-result v1

    .line 179
    :cond_0
    :goto_0
    return v1

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    move-object v0, p1

    .line 165
    check-cast v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    .line 166
    .local v0, "other":Lorg/snmp4j/security/UsmUserTable$UsmUserKey;
    const/4 v1, 0x0

    .line 167
    .local v1, "result":I
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-eqz v2, :cond_3

    .line 168
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    iget-object v3, v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v2, v3}, Lorg/snmp4j/smi/OctetString;->compareTo(Ljava/lang/Object;)I

    move-result v1

    .line 176
    :cond_2
    :goto_1
    if-nez v1, :cond_0

    .line 177
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->securityName:Lorg/snmp4j/smi/OctetString;

    iget-object v3, v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->securityName:Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v2, v3}, Lorg/snmp4j/smi/OctetString;->compareTo(Ljava/lang/Object;)I

    move-result v1

    goto :goto_0

    .line 170
    :cond_3
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-nez v2, :cond_4

    .line 171
    const/4 v1, 0x1

    goto :goto_1

    .line 173
    :cond_4
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-nez v2, :cond_2

    iget-object v2, v0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    if-eqz v2, :cond_2

    .line 174
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 155
    instance-of v1, p1, Lorg/snmp4j/security/UsmUserEntry;

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    if-eqz v1, :cond_1

    .line 156
    :cond_0
    invoke-virtual {p0, p1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->compareTo(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 158
    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->engineID:Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v0}, Lorg/snmp4j/smi/OctetString;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;->securityName:Lorg/snmp4j/smi/OctetString;

    invoke-virtual {v1}, Lorg/snmp4j/smi/OctetString;->hashCode()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    xor-int/2addr v0, v1

    return v0
.end method
