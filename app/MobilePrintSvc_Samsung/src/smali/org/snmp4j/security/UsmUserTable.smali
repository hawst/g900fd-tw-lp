.class public Lorg/snmp4j/security/UsmUserTable;
.super Ljava/lang/Object;
.source "UsmUserTable.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/snmp4j/security/UsmUserTable$UsmUserKey;
    }
.end annotation


# static fields
.field static class$org$snmp4j$security$UsmUserTable:Ljava/lang/Class; = null

.field private static final logger:Lorg/snmp4j/log/LogAdapter;

.field private static final serialVersionUID:J = 0x60439173c8733c36L


# instance fields
.field private table:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->class$org$snmp4j$security$UsmUserTable:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.security.UsmUserTable"

    invoke-static {v0}, Lorg/snmp4j/security/UsmUserTable;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/UsmUserTable;->class$org$snmp4j$security$UsmUserTable:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    return-void

    :cond_0
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->class$org$snmp4j$security$UsmUserTable:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    .line 50
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 45
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public declared-synchronized addUser(Lorg/snmp4j/security/UsmUserEntry;)Lorg/snmp4j/security/UsmUserEntry;
    .locals 3
    .param p1, "user"    # Lorg/snmp4j/security/UsmUserEntry;

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Adding user "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/snmp4j/security/UsmUserEntry;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/snmp4j/security/UsmUserEntry;->getUsmUser()Lorg/snmp4j/security/UsmUser;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    new-instance v1, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    invoke-direct {v1, p1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/security/UsmUserEntry;)V

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/security/UsmUserEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 2

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 122
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v0}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v1, "Cleared UsmUserTable"

    invoke-interface {v0, v1}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :cond_0
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUser(Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;
    .locals 3
    .param p1, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    new-instance v1, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    new-instance v2, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v2}, Lorg/snmp4j/smi/OctetString;-><init>()V

    invoke-direct {v1, v2, p1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/security/UsmUserEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;
    .locals 2
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    new-instance v1, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    invoke-direct {v1, p1, p2}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/security/UsmUserEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUserEntries()Ljava/util/List;
    .locals 3

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 94
    .local v1, "l":Ljava/util/LinkedList;
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    .end local v0    # "it":Ljava/util/Iterator;
    .end local v1    # "l":Ljava/util/LinkedList;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 97
    .restart local v0    # "it":Ljava/util/Iterator;
    .restart local v1    # "l":Ljava/util/LinkedList;
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getUserEntries(Lorg/snmp4j/smi/OctetString;)Ljava/util/List;
    .locals 6
    .param p1, "userName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 80
    .local v1, "users":Ljava/util/LinkedList;
    iget-object v3, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/snmp4j/security/UsmUserEntry;

    .line 82
    .local v2, "value":Lorg/snmp4j/security/UsmUserEntry;
    invoke-virtual {v2}, Lorg/snmp4j/security/UsmUserEntry;->getUserName()Lorg/snmp4j/smi/OctetString;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/snmp4j/smi/OctetString;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 79
    .end local v0    # "it":Ljava/util/Iterator;
    .end local v1    # "users":Ljava/util/LinkedList;
    .end local v2    # "value":Lorg/snmp4j/security/UsmUserEntry;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 86
    .restart local v0    # "it":Ljava/util/Iterator;
    .restart local v1    # "users":Ljava/util/LinkedList;
    :cond_1
    :try_start_1
    sget-object v3, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v3}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    sget-object v3, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Returning user entries for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :cond_2
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized removeUser(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)Lorg/snmp4j/security/UsmUserEntry;
    .locals 4
    .param p1, "engineID"    # Lorg/snmp4j/smi/OctetString;
    .param p2, "securityName"    # Lorg/snmp4j/smi/OctetString;

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    new-instance v2, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    invoke-direct {v2, p1, p2}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/smi/OctetString;Lorg/snmp4j/smi/OctetString;)V

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/security/UsmUserEntry;

    .line 104
    .local v0, "entry":Lorg/snmp4j/security/UsmUserEntry;
    sget-object v1, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v1}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Removed user with secName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " and engineID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    monitor-exit p0

    return-object v0

    .line 102
    .end local v0    # "entry":Lorg/snmp4j/security/UsmUserEntry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setUsers(Ljava/util/Collection;)V
    .locals 5
    .param p1, "c"    # Ljava/util/Collection;

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    sget-object v2, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v2}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    sget-object v2, Lorg/snmp4j/security/UsmUserTable;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Setting users to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 63
    :cond_0
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 64
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/snmp4j/security/UsmUserEntry;

    .line 66
    .local v1, "user":Lorg/snmp4j/security/UsmUserEntry;
    iget-object v2, p0, Lorg/snmp4j/security/UsmUserTable;->table:Ljava/util/Map;

    new-instance v3, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;

    invoke-direct {v3, v1}, Lorg/snmp4j/security/UsmUserTable$UsmUserKey;-><init>(Lorg/snmp4j/security/UsmUserEntry;)V

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 60
    .end local v0    # "it":Ljava/util/Iterator;
    .end local v1    # "user":Lorg/snmp4j/security/UsmUserEntry;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 68
    .restart local v0    # "it":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method
