.class public Lorg/snmp4j/security/Priv3DES;
.super Ljava/lang/Object;
.source "Priv3DES.java"

# interfaces
.implements Lorg/snmp4j/security/PrivacyProtocol;


# static fields
.field private static final DECRYPT_PARAMS_LENGTH:I = 0x8

.field public static final ID:Lorg/snmp4j/smi/OID;

.field static class$org$snmp4j$security$Priv3DES:Ljava/lang/Class;

.field private static final logger:Lorg/snmp4j/log/LogAdapter;


# instance fields
.field protected cipherPool:Lorg/snmp4j/security/CipherPool;

.field protected salt:Lorg/snmp4j/security/Salt;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lorg/snmp4j/smi/OID;

    const-string v1, "1.3.6.1.6.3.10.1.2.3"

    invoke-direct {v0, v1}, Lorg/snmp4j/smi/OID;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/snmp4j/security/Priv3DES;->ID:Lorg/snmp4j/smi/OID;

    .line 53
    sget-object v0, Lorg/snmp4j/security/Priv3DES;->class$org$snmp4j$security$Priv3DES:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.snmp4j.security.Priv3DES"

    invoke-static {v0}, Lorg/snmp4j/security/Priv3DES;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/Priv3DES;->class$org$snmp4j$security$Priv3DES:Ljava/lang/Class;

    :goto_0
    invoke-static {v0}, Lorg/snmp4j/log/LogFactory;->getLogger(Ljava/lang/Class;)Lorg/snmp4j/log/LogAdapter;

    move-result-object v0

    sput-object v0, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    return-void

    :cond_0
    sget-object v0, Lorg/snmp4j/security/Priv3DES;->class$org$snmp4j$security$Priv3DES:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lorg/snmp4j/security/Salt;->getInstance()Lorg/snmp4j/security/Salt;

    move-result-object v0

    iput-object v0, p0, Lorg/snmp4j/security/Priv3DES;->salt:Lorg/snmp4j/security/Salt;

    .line 58
    new-instance v0, Lorg/snmp4j/security/CipherPool;

    invoke-direct {v0}, Lorg/snmp4j/security/CipherPool;-><init>()V

    iput-object v0, p0, Lorg/snmp4j/security/Priv3DES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    .line 59
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 53
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public decrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B
    .locals 11
    .param p1, "cryptedData"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "decryptionKey"    # [B
    .param p5, "engineBoots"    # J
    .param p7, "engineTime"    # J
    .param p9, "decryptParams"    # Lorg/snmp4j/security/DecryptParams;

    .prologue
    .line 156
    rem-int/lit8 v8, p3, 0x8

    if-nez v8, :cond_0

    const/16 v8, 0x8

    if-lt p3, v8, :cond_0

    move-object/from16 v0, p9

    iget v8, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1

    .line 159
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Length ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, ") is not multiple of 8 or decrypt "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "params has not length 8 ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    move-object/from16 v0, p9

    iget v10, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, ")."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 164
    :cond_1
    array-length v8, p4

    const/16 v9, 0x20

    if-ge v8, v9, :cond_2

    .line 165
    sget-object v8, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Wrong Key length: need at least 32 bytes, is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    array-length v10, p4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " bytes."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 168
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "decryptionKey has illegal length "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    array-length v10, p4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " (should be at least 32)."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 173
    :cond_2
    const/16 v8, 0x8

    new-array v5, v8, [B

    .line 176
    .local v5, "iv":[B
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/16 v8, 0x8

    if-ge v4, v8, :cond_3

    .line 177
    add-int/lit8 v8, v4, 0x18

    aget-byte v8, p4, v8

    move-object/from16 v0, p9

    iget-object v9, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    aget-byte v9, v9, v4

    xor-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v5, v4

    .line 176
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 180
    :cond_3
    const/4 v2, 0x0

    .line 183
    .local v2, "decryptedData":[B
    :try_start_0
    iget-object v8, p0, Lorg/snmp4j/security/Priv3DES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v8}, Lorg/snmp4j/security/CipherPool;->reuseCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    .line 184
    .local v1, "alg":Ljavax/crypto/Cipher;
    if-nez v1, :cond_4

    .line 185
    const-string v8, "DESede/CBC/NoPadding"

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 187
    :cond_4
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v8, 0x0

    const/16 v9, 0x18

    const-string v10, "DESede"

    invoke-direct {v7, p4, v8, v9, v10}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 189
    .local v7, "key":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v6, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v6, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 190
    .local v6, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v8, 0x2

    invoke-virtual {v1, v8, v7, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 191
    invoke-virtual {v1, p1, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v2

    .line 192
    iget-object v8, p0, Lorg/snmp4j/security/Priv3DES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v8, v1}, Lorg/snmp4j/security/CipherPool;->offerCipher(Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v1    # "alg":Ljavax/crypto/Cipher;
    .end local v6    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v7    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :cond_5
    :goto_1
    return-object v2

    .line 194
    :catch_0
    move-exception v3

    .line 195
    .local v3, "e":Ljava/lang/Exception;
    sget-object v8, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v8, v3}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 196
    sget-object v8, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v8}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 197
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public encrypt([BII[BJJLorg/snmp4j/security/DecryptParams;)[B
    .locals 19
    .param p1, "unencryptedData"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "encryptionKey"    # [B
    .param p5, "engineBoots"    # J
    .param p7, "engineTime"    # J
    .param p9, "decryptParams"    # Lorg/snmp4j/security/DecryptParams;

    .prologue
    .line 68
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/snmp4j/security/Priv3DES;->salt:Lorg/snmp4j/security/Salt;

    invoke-virtual {v5}, Lorg/snmp4j/security/Salt;->getNext()J

    move-result-wide v5

    long-to-int v14, v5

    .line 70
    .local v14, "mySalt":I
    move-object/from16 v0, p4

    array-length v5, v0

    const/16 v6, 0x20

    if-ge v5, v6, :cond_0

    .line 71
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Wrong Key length: need at least 32 bytes, is "

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    move-object/from16 v0, p4

    array-length v15, v0

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v15, " bytes."

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 74
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "encryptionKey has illegal length "

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    move-object/from16 v0, p4

    array-length v15, v0

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v15, " (should be at least 32)."

    invoke-virtual {v6, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 79
    :cond_0
    move-object/from16 v0, p9

    iget-object v5, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    if-eqz v5, :cond_1

    move-object/from16 v0, p9

    iget v5, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    const/16 v6, 0x8

    if-ge v5, v6, :cond_2

    .line 80
    :cond_1
    const/16 v5, 0x8

    new-array v5, v5, [B

    move-object/from16 v0, p9

    iput-object v5, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    .line 82
    :cond_2
    const/16 v5, 0x8

    move-object/from16 v0, p9

    iput v5, v0, Lorg/snmp4j/security/DecryptParams;->length:I

    .line 83
    const/4 v5, 0x0

    move-object/from16 v0, p9

    iput v5, v0, Lorg/snmp4j/security/DecryptParams;->offset:I

    .line 86
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 87
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Preparing decrypt_params."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 89
    :cond_3
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    const/4 v5, 0x4

    if-ge v10, v5, :cond_4

    .line 90
    move-object/from16 v0, p9

    iget-object v5, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    rsub-int/lit8 v6, v10, 0x3

    const-wide/16 v15, 0xff

    mul-int/lit8 v17, v10, 0x8

    shr-long v17, p5, v17

    and-long v15, v15, v17

    long-to-int v15, v15

    int-to-byte v15, v15

    aput-byte v15, v5, v6

    .line 91
    move-object/from16 v0, p9

    iget-object v5, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    rsub-int/lit8 v6, v10, 0x7

    mul-int/lit8 v15, v10, 0x8

    shr-int v15, v14, v15

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v5, v6

    .line 89
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 94
    :cond_4
    const/16 v5, 0x8

    new-array v11, v5, [B

    .line 97
    .local v11, "iv":[B
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 98
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Preparing iv for encryption."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 100
    :cond_5
    const/4 v10, 0x0

    :goto_1
    const/16 v5, 0x8

    if-ge v10, v5, :cond_6

    .line 101
    add-int/lit8 v5, v10, 0x18

    aget-byte v5, p4, v5

    move-object/from16 v0, p9

    iget-object v6, v0, Lorg/snmp4j/security/DecryptParams;->array:[B

    aget-byte v6, v6, v10

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v11, v10

    .line 100
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 104
    :cond_6
    const/4 v7, 0x0

    .line 108
    .local v7, "encryptedData":[B
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/snmp4j/security/Priv3DES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v5}, Lorg/snmp4j/security/CipherPool;->reuseCipher()Ljavax/crypto/Cipher;

    move-result-object v3

    .line 109
    .local v3, "alg":Ljavax/crypto/Cipher;
    if-nez v3, :cond_7

    .line 110
    const-string v5, "DESede/CBC/NoPadding"

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 112
    :cond_7
    new-instance v13, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v5, 0x0

    const/16 v6, 0x18

    const-string v15, "DESede"

    move-object/from16 v0, p4

    invoke-direct {v13, v0, v5, v6, v15}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    .line 114
    .local v13, "key":Ljavax/crypto/spec/SecretKeySpec;
    new-instance v12, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v12, v11}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 115
    .local v12, "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v13, v12}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 118
    rem-int/lit8 v5, p3, 0x8

    if-nez v5, :cond_a

    .line 119
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v7

    .line 134
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/snmp4j/security/Priv3DES;->cipherPool:Lorg/snmp4j/security/CipherPool;

    invoke-virtual {v5, v3}, Lorg/snmp4j/security/CipherPool;->offerCipher(Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v3    # "alg":Ljavax/crypto/Cipher;
    .end local v12    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v13    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :cond_8
    :goto_3
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 144
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Encryption finished."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 146
    :cond_9
    return-object v7

    .line 122
    .restart local v3    # "alg":Ljavax/crypto/Cipher;
    .restart local v12    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .restart local v13    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :cond_a
    :try_start_1
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 123
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    const-string v6, "Using padding."

    invoke-interface {v5, v6}, Lorg/snmp4j/log/LogAdapter;->debug(Ljava/lang/Object;)V

    .line 126
    :cond_b
    div-int/lit8 v5, p3, 0x8

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v5, v5, 0x8

    new-array v7, v5, [B

    .line 127
    const/16 v5, 0x8

    new-array v4, v5, [B

    .line 129
    .local v4, "tmp":[B
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2, v7}, Ljavax/crypto/Cipher;->update([BII[B)I

    move-result v8

    .line 131
    .local v8, "encryptedLength":I
    const/4 v5, 0x0

    rem-int/lit8 v6, p3, 0x8

    rsub-int/lit8 v6, v6, 0x8

    invoke-virtual/range {v3 .. v8}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    add-int/2addr v5, v8

    goto :goto_2

    .line 136
    .end local v3    # "alg":Ljavax/crypto/Cipher;
    .end local v4    # "tmp":[B
    .end local v8    # "encryptedLength":I
    .end local v12    # "ivSpec":Ljavax/crypto/spec/IvParameterSpec;
    .end local v13    # "key":Ljavax/crypto/spec/SecretKeySpec;
    :catch_0
    move-exception v9

    .line 137
    .local v9, "e":Ljava/lang/Exception;
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5, v9}, Lorg/snmp4j/log/LogAdapter;->error(Ljava/lang/Object;)V

    .line 138
    sget-object v5, Lorg/snmp4j/security/Priv3DES;->logger:Lorg/snmp4j/log/LogAdapter;

    invoke-interface {v5}, Lorg/snmp4j/log/LogAdapter;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 139
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public extendShortKey([BLorg/snmp4j/smi/OctetString;[BLorg/snmp4j/security/AuthenticationProtocol;)[B
    .locals 7
    .param p1, "shortKey"    # [B
    .param p2, "password"    # Lorg/snmp4j/smi/OctetString;
    .param p3, "engineID"    # [B
    .param p4, "authProtocol"    # Lorg/snmp4j/security/AuthenticationProtocol;

    .prologue
    const/4 v6, 0x0

    .line 235
    array-length v3, p1

    .line 236
    .local v3, "length":I
    invoke-virtual {p0}, Lorg/snmp4j/security/Priv3DES;->getMinKeyLength()I

    move-result v4

    new-array v1, v4, [B

    .line 237
    .local v1, "extendedKey":[B
    array-length v4, p1

    invoke-static {p1, v6, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    invoke-virtual {p0}, Lorg/snmp4j/security/Priv3DES;->getMinKeyLength()I

    move-result v4

    new-array v2, v4, [B

    .line 240
    .local v2, "key":[B
    array-length v4, p1

    invoke-static {p1, v6, v2, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    :goto_0
    invoke-virtual {p0}, Lorg/snmp4j/security/Priv3DES;->getMinKeyLength()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 242
    new-instance v4, Lorg/snmp4j/smi/OctetString;

    invoke-direct {v4, v2, v6, v3}, Lorg/snmp4j/smi/OctetString;-><init>([BII)V

    invoke-interface {p4, v4, p3}, Lorg/snmp4j/security/AuthenticationProtocol;->passwordToKey(Lorg/snmp4j/smi/OctetString;[B)[B

    move-result-object v2

    .line 244
    invoke-virtual {p0}, Lorg/snmp4j/security/Priv3DES;->getMinKeyLength()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-interface {p4}, Lorg/snmp4j/security/AuthenticationProtocol;->getDigestLength()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 246
    .local v0, "copyBytes":I
    invoke-static {v2, v6, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 247
    add-int/2addr v3, v0

    .line 248
    goto :goto_0

    .line 249
    .end local v0    # "copyBytes":I
    :cond_0
    return-object v1
.end method

.method public getDecryptParamsLength()I
    .locals 1

    .prologue
    .line 225
    const/16 v0, 0x8

    return v0
.end method

.method public getEncryptedLength(I)I
    .locals 1
    .param p1, "scopedPDULength"    # I

    .prologue
    .line 214
    rem-int/lit8 v0, p1, 0x8

    if-nez v0, :cond_0

    .line 217
    .end local p1    # "scopedPDULength":I
    :goto_0
    return p1

    .restart local p1    # "scopedPDULength":I
    :cond_0
    div-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 p1, v0, 0x8

    goto :goto_0
.end method

.method public getID()Lorg/snmp4j/smi/OID;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lorg/snmp4j/security/Priv3DES;->ID:Lorg/snmp4j/smi/OID;

    invoke-virtual {v0}, Lorg/snmp4j/smi/OID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/snmp4j/smi/OID;

    return-object v0
.end method

.method public getMaxKeyLength()I
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lorg/snmp4j/security/Priv3DES;->getMinKeyLength()I

    move-result v0

    return v0
.end method

.method public getMinKeyLength()I
    .locals 1

    .prologue
    .line 221
    const/16 v0, 0x20

    return v0
.end method
