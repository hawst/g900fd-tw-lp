.class public Ljcifs/smb/SmbComOpenPrintFileResponse;
.super Ljcifs/smb/ServerMessageBlock;
.source "SmbComOpenPrintFileResponse.java"


# instance fields
.field fid:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljcifs/smb/ServerMessageBlock;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1}, Ljcifs/smb/ServerMessageBlock;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Ljcifs/smb/ServerMessageBlock;->hashCode()I

    move-result v0

    return v0
.end method

.method readBytesWireFormat([BI)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "bufferIndex"    # I

    .prologue
    .line 10
    const/4 v0, 0x0

    return v0
.end method

.method readParameterWordsWireFormat([BI)I
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "bufferIndex"    # I

    .prologue
    .line 15
    invoke-static {p1, p2}, Ljcifs/smb/SmbComOpenPrintFileResponse;->readInt2([BI)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Ljcifs/smb/SmbComOpenPrintFileResponse;->fid:J

    .line 16
    add-int/lit8 p2, p2, 0x2

    .line 17
    const/4 v0, 0x2

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Ljcifs/smb/ServerMessageBlock;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeBytesWireFormat([BI)I
    .locals 1
    .param p1, "dst"    # [B
    .param p2, "dstIndex"    # I

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method writeParameterWordsWireFormat([BI)I
    .locals 1
    .param p1, "dst"    # [B
    .param p2, "dstIndex"    # I

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method
