.class public Ljcifs/smb/SmbComClosePrintFile;
.super Ljcifs/smb/ServerMessageBlock;
.source "SmbComClosePrintFile.java"


# instance fields
.field fid:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "fid"    # J

    .prologue
    .line 7
    invoke-direct {p0}, Ljcifs/smb/ServerMessageBlock;-><init>()V

    .line 8
    iput-wide p1, p0, Ljcifs/smb/SmbComClosePrintFile;->fid:J

    .line 9
    const/16 v0, -0x3e

    iput-byte v0, p0, Ljcifs/smb/SmbComClosePrintFile;->command:B

    .line 10
    return-void
.end method


# virtual methods
.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1}, Ljcifs/smb/ServerMessageBlock;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Ljcifs/smb/ServerMessageBlock;->hashCode()I

    move-result v0

    return v0
.end method

.method readBytesWireFormat([BI)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "bufferIndex"    # I

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method readParameterWordsWireFormat([BI)I
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "bufferIndex"    # I

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Ljcifs/smb/ServerMessageBlock;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeBytesWireFormat([BI)I
    .locals 1
    .param p1, "dst"    # [B
    .param p2, "dstIndex"    # I

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method writeParameterWordsWireFormat([BI)I
    .locals 2
    .param p1, "dst"    # [B
    .param p2, "dstIndex"    # I

    .prologue
    .line 32
    iget-wide v0, p0, Ljcifs/smb/SmbComClosePrintFile;->fid:J

    invoke-static {v0, v1, p1, p2}, Ljcifs/smb/SmbComClosePrintFile;->writeInt2(J[BI)V

    .line 33
    const/4 v0, 0x2

    return v0
.end method
