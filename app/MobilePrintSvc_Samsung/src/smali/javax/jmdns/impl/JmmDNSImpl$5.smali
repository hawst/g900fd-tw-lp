.class Ljavax/jmdns/impl/JmmDNSImpl$5;
.super Ljava/lang/Object;
.source "JmmDNSImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljavax/jmdns/impl/JmmDNSImpl;->inetAddressAdded(Ljavax/jmdns/NetworkTopologyEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Ljavax/jmdns/impl/JmmDNSImpl;

.field final synthetic val$dns:Ljavax/jmdns/JmDNS;

.field final synthetic val$infos:Ljava/util/Collection;

.field final synthetic val$serviceListeners:Ljava/util/Map;

.field final synthetic val$typeListeners:Ljava/util/Collection;

.field final synthetic val$types:Ljava/util/Collection;


# direct methods
.method constructor <init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljava/util/Collection;Ljavax/jmdns/JmDNS;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->this$0:Ljavax/jmdns/impl/JmmDNSImpl;

    iput-object p2, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$types:Ljava/util/Collection;

    iput-object p3, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$dns:Ljavax/jmdns/JmDNS;

    iput-object p4, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$infos:Ljava/util/Collection;

    iput-object p5, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$typeListeners:Ljava/util/Collection;

    iput-object p6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$serviceListeners:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 647
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$types:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 648
    .local v5, "type":Ljava/lang/String;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$dns:Ljavax/jmdns/JmDNS;

    invoke-virtual {v6, v5}, Ljavax/jmdns/JmDNS;->registerServiceType(Ljava/lang/String;)Z

    goto :goto_0

    .line 651
    .end local v5    # "type":Ljava/lang/String;
    :cond_0
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$infos:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljavax/jmdns/ServiceInfo;

    .line 653
    .local v2, "info":Ljavax/jmdns/ServiceInfo;
    :try_start_0
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$dns:Ljavax/jmdns/JmDNS;

    invoke-virtual {v2}, Ljavax/jmdns/ServiceInfo;->clone()Ljavax/jmdns/ServiceInfo;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/jmdns/JmDNS;->registerService(Ljavax/jmdns/ServiceInfo;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 654
    :catch_0
    move-exception v6

    goto :goto_1

    .line 659
    .end local v2    # "info":Ljavax/jmdns/ServiceInfo;
    :cond_1
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$typeListeners:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljavax/jmdns/ServiceTypeListener;

    .line 661
    .local v3, "listener":Ljavax/jmdns/ServiceTypeListener;
    :try_start_1
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$dns:Ljavax/jmdns/JmDNS;

    invoke-virtual {v6, v3}, Ljavax/jmdns/JmDNS;->addServiceTypeListener(Ljavax/jmdns/ServiceTypeListener;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 662
    :catch_1
    move-exception v6

    goto :goto_2

    .line 667
    .end local v3    # "listener":Ljavax/jmdns/ServiceTypeListener;
    :cond_2
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$serviceListeners:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .end local v0    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 668
    .restart local v5    # "type":Ljava/lang/String;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$serviceListeners:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 669
    .local v4, "listeners":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    monitor-enter v4

    .line 670
    :try_start_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljavax/jmdns/ServiceListener;

    .line 671
    .local v3, "listener":Ljavax/jmdns/ServiceListener;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl$5;->val$dns:Ljavax/jmdns/JmDNS;

    invoke-virtual {v6, v5, v3}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    goto :goto_4

    .line 673
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "listener":Ljavax/jmdns/ServiceListener;
    :catchall_0
    move-exception v6

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 675
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "listeners":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    .end local v5    # "type":Ljava/lang/String;
    :cond_4
    return-void
.end method
