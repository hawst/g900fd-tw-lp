.class public Ljavax/jmdns/impl/JmmDNSImpl;
.super Ljava/lang/Object;
.source "JmmDNSImpl.java"

# interfaces
.implements Ljavax/jmdns/JmmDNS;
.implements Ljavax/jmdns/NetworkTopologyListener;
.implements Ljavax/jmdns/impl/ServiceInfoImpl$Delegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljavax/jmdns/impl/JmmDNSImpl$NetworkChecker;
    }
.end annotation


# static fields
.field private static logger:Ljava/util/logging/Logger;


# instance fields
.field private final _closed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final _isClosing:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final _jmDNSExecutor:Ljava/util/concurrent/ExecutorService;

.field private final _knownMDNS:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/net/InetAddress;",
            "Ljavax/jmdns/JmDNS;",
            ">;"
        }
    .end annotation
.end field

.field private final _listenerExecutor:Ljava/util/concurrent/ExecutorService;

.field private final _networkListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljavax/jmdns/NetworkTopologyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final _serviceListeners:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljavax/jmdns/ServiceListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final _serviceTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final _services:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Ljavax/jmdns/ServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final _timer:Ljava/util/Timer;

.field private final _typeListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljavax/jmdns/ServiceTypeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Ljavax/jmdns/impl/JmmDNSImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_networkListeners:Ljava/util/Set;

    .line 94
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    .line 95
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    .line 96
    new-instance v0, Ljavax/jmdns/impl/util/NamedThreadFactory;

    const-string v1, "JmmDNS Listeners"

    invoke-direct {v0, v1}, Ljavax/jmdns/impl/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_listenerExecutor:Ljava/util/concurrent/ExecutorService;

    .line 97
    new-instance v0, Ljavax/jmdns/impl/util/NamedThreadFactory;

    const-string v1, "JmmDNS"

    invoke-direct {v0, v1}, Ljavax/jmdns/impl/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_jmDNSExecutor:Ljava/util/concurrent/ExecutorService;

    .line 98
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Multihomed mDNS.Timer"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_timer:Ljava/util/Timer;

    .line 99
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    .line 100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_typeListeners:Ljava/util/Set;

    .line 101
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceTypes:Ljava/util/Set;

    .line 102
    new-instance v0, Ljavax/jmdns/impl/JmmDNSImpl$NetworkChecker;

    invoke-static {}, Ljavax/jmdns/NetworkTopologyDiscovery$Factory;->getInstance()Ljavax/jmdns/NetworkTopologyDiscovery;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljavax/jmdns/impl/JmmDNSImpl$NetworkChecker;-><init>(Ljavax/jmdns/NetworkTopologyListener;Ljavax/jmdns/NetworkTopologyDiscovery;)V

    iget-object v1, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_timer:Ljava/util/Timer;

    invoke-virtual {v0, v1}, Ljavax/jmdns/impl/JmmDNSImpl$NetworkChecker;->start(Ljava/util/Timer;)V

    .line 103
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_isClosing:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 104
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 105
    return-void
.end method


# virtual methods
.method public addNetworkTopologyListener(Ljavax/jmdns/NetworkTopologyListener;)V
    .locals 1
    .param p1, "listener"    # Ljavax/jmdns/NetworkTopologyListener;

    .prologue
    .line 601
    iget-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_networkListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 602
    return-void
.end method

.method public addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    .locals 8
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "listener"    # Ljavax/jmdns/ServiceListener;

    .prologue
    .line 380
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 381
    .local v4, "loType":Ljava/lang/String;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 382
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    if-nez v3, :cond_0

    .line 383
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v6, v4, v7}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    check-cast v3, Ljava/util/List;

    .line 386
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    :cond_0
    if-eqz v3, :cond_2

    .line 387
    monitor-enter v3

    .line 388
    :try_start_0
    invoke-interface {v3, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 389
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    :cond_2
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v5, v0, v1

    .line 394
    .local v5, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v5, p1, p2}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 391
    .end local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "mDNS":Ljavax/jmdns/JmDNS;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 396
    .restart local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    :cond_3
    return-void
.end method

.method public addServiceTypeListener(Ljavax/jmdns/ServiceTypeListener;)V
    .locals 5
    .param p1, "listener"    # Ljavax/jmdns/ServiceTypeListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    iget-object v4, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_typeListeners:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 358
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3, p1}, Ljavax/jmdns/JmDNS;->addServiceTypeListener(Ljavax/jmdns/ServiceTypeListener;)V

    .line 357
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 360
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    return-void
.end method

.method public close()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 113
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_isClosing:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 114
    sget-object v6, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 115
    sget-object v6, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cancelling JmmDNS: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_timer:Ljava/util/Timer;

    invoke-virtual {v6}, Ljava/util/Timer;->cancel()V

    .line 118
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_listenerExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 119
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_jmDNSExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 121
    new-instance v6, Ljavax/jmdns/impl/util/NamedThreadFactory;

    const-string v7, "JmmDNS.close"

    invoke-direct {v6, v7}, Ljavax/jmdns/impl/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 123
    .local v2, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 124
    .local v5, "mDNS":Ljavax/jmdns/JmDNS;
    new-instance v6, Ljavax/jmdns/impl/JmmDNSImpl$1;

    invoke-direct {v6, p0, v5}, Ljavax/jmdns/impl/JmmDNSImpl$1;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/JmDNS;)V

    invoke-interface {v2, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 139
    .end local v5    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_1
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 142
    const-wide/16 v6, 0x1388

    :try_start_1
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v8}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 146
    :goto_1
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 147
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 148
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 149
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_typeListeners:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 150
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceTypes:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 151
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 152
    invoke-static {}, Ljavax/jmdns/JmmDNS$Factory;->close()V

    .line 154
    .end local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v2    # "executor":Ljava/util/concurrent/ExecutorService;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    return-void

    .line 139
    .restart local v2    # "executor":Ljava/util/concurrent/ExecutorService;
    :catchall_0
    move-exception v6

    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    throw v6

    .line 143
    .restart local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :catch_0
    move-exception v1

    .line 144
    .local v1, "exception":Ljava/lang/InterruptedException;
    sget-object v6, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v8, "Exception "

    invoke-virtual {v6, v7, v8, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public getDNS()[Ljavax/jmdns/JmDNS;
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v1

    .line 202
    :try_start_0
    iget-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v2

    new-array v2, v2, [Ljavax/jmdns/JmDNS;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljavax/jmdns/JmDNS;

    monitor-exit v1

    return-object v0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getHostNames()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 175
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 176
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 177
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3}, Ljavax/jmdns/JmDNS;->getHostName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    return-object v5
.end method

.method public getInetAddresses()[Ljava/net/InetAddress;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 189
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/InetAddress;>;"
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 190
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3}, Ljavax/jmdns/JmDNS;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 192
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/net/InetAddress;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/net/InetAddress;

    return-object v5
.end method

.method public getInterfaces()[Ljava/net/InetAddress;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 213
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 214
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/InetAddress;>;"
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 215
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3}, Ljavax/jmdns/JmDNS;->getInterface()Ljava/net/InetAddress;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/net/InetAddress;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/net/InetAddress;

    return-object v5
.end method

.method public getNames()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 162
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 163
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 164
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3}, Ljavax/jmdns/JmDNS;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    return-object v5
.end method

.method public getServiceInfos(Ljava/lang/String;Ljava/lang/String;)[Ljavax/jmdns/ServiceInfo;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 226
    const/4 v3, 0x0

    const-wide/16 v4, 0x1770

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->getServiceInfos(Ljava/lang/String;Ljava/lang/String;ZJ)[Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    return-object v0
.end method

.method public getServiceInfos(Ljava/lang/String;Ljava/lang/String;J)[Ljavax/jmdns/ServiceInfo;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    .line 235
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->getServiceInfos(Ljava/lang/String;Ljava/lang/String;ZJ)[Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    return-object v0
.end method

.method public getServiceInfos(Ljava/lang/String;Ljava/lang/String;Z)[Ljavax/jmdns/ServiceInfo;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "persistent"    # Z

    .prologue
    .line 244
    const-wide/16 v4, 0x1770

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->getServiceInfos(Ljava/lang/String;Ljava/lang/String;ZJ)[Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    return-object v0
.end method

.method public getServiceInfos(Ljava/lang/String;Ljava/lang/String;ZJ)[Ljavax/jmdns/ServiceInfo;
    .locals 20
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "persistent"    # Z
    .param p4, "timeout"    # J

    .prologue
    .line 254
    invoke-virtual/range {p0 .. p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v10

    .line 255
    .local v10, "dnsArray":[Ljavax/jmdns/JmDNS;
    new-instance v17, Ljava/util/HashSet;

    array-length v1, v10

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 256
    .local v17, "result":Ljava/util/Set;, "Ljava/util/Set<Ljavax/jmdns/ServiceInfo;>;"
    array-length v1, v10

    if-lez v1, :cond_3

    .line 257
    new-instance v19, Ljava/util/ArrayList;

    array-length v1, v10

    move-object/from16 v0, v19

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 258
    .local v19, "tasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Callable<Ljavax/jmdns/ServiceInfo;>;>;"
    move-object v9, v10

    .local v9, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v0, v9

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    aget-object v3, v9, v14

    .line 259
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    new-instance v1, Ljavax/jmdns/impl/JmmDNSImpl$2;

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-wide/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Ljavax/jmdns/impl/JmmDNSImpl$2;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/JmDNS;Ljava/lang/String;Ljava/lang/String;ZJ)V

    move-object/from16 v0, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 269
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljavax/jmdns/impl/util/NamedThreadFactory;

    const-string v4, "JmmDNS.getServiceInfos"

    invoke-direct {v2, v4}, Ljavax/jmdns/impl/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v12

    .line 271
    .local v12, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    .line 273
    .local v18, "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;>;"
    const-wide/16 v1, 0x64

    add-long v1, v1, p4

    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v19

    invoke-interface {v12, v0, v1, v2, v4}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v18

    .line 280
    :goto_1
    :try_start_2
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/Future;

    .line 281
    .local v13, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;"
    invoke-interface {v13}, Ljava/util/concurrent/Future;->isCancelled()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 285
    :try_start_3
    invoke-interface {v13}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljavax/jmdns/ServiceInfo;

    .line 286
    .local v15, "info":Ljavax/jmdns/ServiceInfo;
    if-eqz v15, :cond_1

    .line 287
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 289
    .end local v15    # "info":Ljavax/jmdns/ServiceInfo;
    :catch_0
    move-exception v11

    .line 290
    .local v11, "exception":Ljava/lang/InterruptedException;
    :try_start_4
    sget-object v1, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v4, "Interrupted "

    invoke-virtual {v1, v2, v4, v11}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 297
    .end local v11    # "exception":Ljava/lang/InterruptedException;
    .end local v13    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v18    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;>;"
    :catchall_0
    move-exception v1

    invoke-interface {v12}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    throw v1

    .line 274
    .local v14, "i$":I
    .restart local v18    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;>;"
    :catch_1
    move-exception v11

    .line 275
    .restart local v11    # "exception":Ljava/lang/InterruptedException;
    :try_start_5
    sget-object v1, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v4, "Interrupted "

    invoke-virtual {v1, v2, v4, v11}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 276
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 292
    .end local v11    # "exception":Ljava/lang/InterruptedException;
    .restart local v13    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;"
    .local v14, "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v11

    .line 293
    .local v11, "exception":Ljava/util/concurrent/ExecutionException;
    sget-object v1, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Exception "

    invoke-virtual {v1, v2, v4, v11}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 297
    .end local v11    # "exception":Ljava/util/concurrent/ExecutionException;
    .end local v13    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;"
    :cond_2
    invoke-interface {v12}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 300
    .end local v9    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v12    # "executor":Ljava/util/concurrent/ExecutorService;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v16    # "len$":I
    .end local v18    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljavax/jmdns/ServiceInfo;>;>;"
    .end local v19    # "tasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Callable<Ljavax/jmdns/ServiceInfo;>;>;"
    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljavax/jmdns/ServiceInfo;

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljavax/jmdns/ServiceInfo;

    return-object v1
.end method

.method public inetAddressAdded(Ljavax/jmdns/NetworkTopologyEvent;)V
    .locals 17
    .param p1, "event"    # Ljavax/jmdns/NetworkTopologyEvent;

    .prologue
    .line 628
    invoke-virtual/range {p1 .. p1}, Ljavax/jmdns/NetworkTopologyEvent;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v8

    .line 630
    .local v8, "address":Ljava/net/InetAddress;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v8}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 631
    move-object/from16 v0, p0

    iget-object v15, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v15
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v8}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 633
    invoke-static {v8}, Ljavax/jmdns/JmDNS;->create(Ljava/net/InetAddress;)Ljavax/jmdns/JmDNS;

    move-result-object v4

    .line 634
    .local v4, "dns":Ljavax/jmdns/JmDNS;
    move-object/from16 v0, p0

    iget-object v1, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v8, v4}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 636
    move-object/from16 v0, p0

    iget-object v3, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceTypes:Ljava/util/Set;

    .line 637
    .local v3, "types":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 638
    .local v5, "infos":Ljava/util/Collection;, "Ljava/util/Collection<Ljavax/jmdns/ServiceInfo;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_typeListeners:Ljava/util/Set;

    .line 639
    .local v6, "typeListeners":Ljava/util/Collection;, "Ljava/util/Collection<Ljavax/jmdns/ServiceTypeListener;>;"
    move-object/from16 v0, p0

    iget-object v7, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    .line 640
    .local v7, "serviceListeners":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljavax/jmdns/ServiceListener;>;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_jmDNSExecutor:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v16, v0

    new-instance v1, Ljavax/jmdns/impl/JmmDNSImpl$5;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Ljavax/jmdns/impl/JmmDNSImpl$5;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljava/util/Collection;Ljavax/jmdns/JmDNS;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Map;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 677
    new-instance v12, Ljavax/jmdns/impl/NetworkTopologyEventImpl;

    invoke-direct {v12, v4, v8}, Ljavax/jmdns/impl/NetworkTopologyEventImpl;-><init>(Ljavax/jmdns/JmDNS;Ljava/net/InetAddress;)V

    .line 678
    .local v12, "jmdnsEvent":Ljavax/jmdns/NetworkTopologyEvent;
    invoke-virtual/range {p0 .. p0}, Ljavax/jmdns/impl/JmmDNSImpl;->networkListeners()[Ljavax/jmdns/NetworkTopologyListener;

    move-result-object v9

    .local v9, "arr$":[Ljavax/jmdns/NetworkTopologyListener;
    array-length v13, v9

    .local v13, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v13, :cond_1

    aget-object v14, v9, v11

    .line 679
    .local v14, "listener":Ljavax/jmdns/NetworkTopologyListener;
    move-object/from16 v0, p0

    iget-object v1, v0, Ljavax/jmdns/impl/JmmDNSImpl;->_listenerExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Ljavax/jmdns/impl/JmmDNSImpl$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v14, v12}, Ljavax/jmdns/impl/JmmDNSImpl$6;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/NetworkTopologyListener;Ljavax/jmdns/NetworkTopologyEvent;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 678
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 690
    .end local v3    # "types":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v5    # "infos":Ljava/util/Collection;, "Ljava/util/Collection<Ljavax/jmdns/ServiceInfo;>;"
    .end local v6    # "typeListeners":Ljava/util/Collection;, "Ljava/util/Collection<Ljavax/jmdns/ServiceTypeListener;>;"
    .end local v7    # "serviceListeners":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljavax/jmdns/ServiceListener;>;>;"
    .end local v9    # "arr$":[Ljavax/jmdns/NetworkTopologyListener;
    .end local v11    # "i$":I
    .end local v12    # "jmdnsEvent":Ljavax/jmdns/NetworkTopologyEvent;
    .end local v13    # "len$":I
    .end local v14    # "listener":Ljavax/jmdns/NetworkTopologyListener;
    :cond_0
    invoke-virtual {v4}, Ljavax/jmdns/JmDNS;->close()V

    .line 693
    .end local v4    # "dns":Ljavax/jmdns/JmDNS;
    :cond_1
    monitor-exit v15

    .line 698
    :cond_2
    :goto_1
    return-void

    .line 693
    :catchall_0
    move-exception v1

    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 695
    :catch_0
    move-exception v10

    .line 696
    .local v10, "e":Ljava/lang/Exception;
    sget-object v1, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unexpected unhandled exception: "

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public inetAddressRemoved(Ljavax/jmdns/NetworkTopologyEvent;)V
    .locals 11
    .param p1, "event"    # Ljavax/jmdns/NetworkTopologyEvent;

    .prologue
    .line 706
    invoke-virtual {p1}, Ljavax/jmdns/NetworkTopologyEvent;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 708
    .local v0, "address":Ljava/net/InetAddress;
    :try_start_0
    iget-object v8, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v8, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 709
    iget-object v9, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    :try_start_1
    iget-object v8, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v8, v0}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 711
    iget-object v8, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_knownMDNS:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v8, v0}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljavax/jmdns/JmDNS;

    .line 712
    .local v7, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v7}, Ljavax/jmdns/JmDNS;->close()V

    .line 713
    new-instance v4, Ljavax/jmdns/impl/NetworkTopologyEventImpl;

    invoke-direct {v4, v7, v0}, Ljavax/jmdns/impl/NetworkTopologyEventImpl;-><init>(Ljavax/jmdns/JmDNS;Ljava/net/InetAddress;)V

    .line 714
    .local v4, "jmdnsEvent":Ljavax/jmdns/NetworkTopologyEvent;
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->networkListeners()[Ljavax/jmdns/NetworkTopologyListener;

    move-result-object v1

    .local v1, "arr$":[Ljavax/jmdns/NetworkTopologyListener;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v1, v3

    .line 715
    .local v6, "listener":Ljavax/jmdns/NetworkTopologyListener;
    iget-object v8, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_listenerExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v10, Ljavax/jmdns/impl/JmmDNSImpl$7;

    invoke-direct {v10, p0, v6, v4}, Ljavax/jmdns/impl/JmmDNSImpl$7;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/NetworkTopologyListener;Ljavax/jmdns/NetworkTopologyEvent;)V

    invoke-interface {v8, v10}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 714
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 726
    .end local v1    # "arr$":[Ljavax/jmdns/NetworkTopologyListener;
    .end local v3    # "i$":I
    .end local v4    # "jmdnsEvent":Ljavax/jmdns/NetworkTopologyEvent;
    .end local v5    # "len$":I
    .end local v6    # "listener":Ljavax/jmdns/NetworkTopologyListener;
    .end local v7    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    monitor-exit v9

    .line 731
    :cond_1
    :goto_1
    return-void

    .line 726
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 728
    :catch_0
    move-exception v2

    .line 729
    .local v2, "e":Ljava/lang/Exception;
    sget-object v8, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unexpected unhandled exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public list(Ljava/lang/String;)[Ljavax/jmdns/ServiceInfo;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 508
    const-wide/16 v0, 0x1770

    invoke-virtual {p0, p1, v0, v1}, Ljavax/jmdns/impl/JmmDNSImpl;->list(Ljava/lang/String;J)[Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    return-object v0
.end method

.method public list(Ljava/lang/String;J)[Ljavax/jmdns/ServiceInfo;
    .locals 16
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "timeout"    # J

    .prologue
    .line 517
    invoke-virtual/range {p0 .. p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v7

    .line 519
    .local v7, "dnsArray":[Ljavax/jmdns/JmDNS;
    new-instance v13, Ljava/util/HashSet;

    array-length v0, v7

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v13, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 520
    .local v13, "result":Ljava/util/Set;, "Ljava/util/Set<Ljavax/jmdns/ServiceInfo;>;"
    array-length v0, v7

    if-lez v0, :cond_3

    .line 521
    new-instance v15, Ljava/util/ArrayList;

    array-length v0, v7

    invoke-direct {v15, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 522
    .local v15, "tasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Callable<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    move-object v6, v7

    .local v6, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v12, v6

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v2, v6, v11

    .line 523
    .local v2, "mDNS":Ljavax/jmdns/JmDNS;
    new-instance v0, Ljavax/jmdns/impl/JmmDNSImpl$4;

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    invoke-direct/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl$4;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/JmDNS;Ljava/lang/String;J)V

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 531
    .end local v2    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljavax/jmdns/impl/util/NamedThreadFactory;

    const-string v3, "JmmDNS.list"

    invoke-direct {v1, v3}, Ljavax/jmdns/impl/util/NamedThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v9

    .line 533
    .local v9, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 535
    .local v14, "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    const-wide/16 v0, 0x64

    add-long v0, v0, p2

    :try_start_1
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v9, v15, v0, v1, v3}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    .line 542
    :goto_1
    :try_start_2
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Future;

    .line 543
    .local v10, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;"
    invoke-interface {v10}, Ljava/util/concurrent/Future;->isCancelled()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 547
    :try_start_3
    invoke-interface {v10}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v13, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 548
    :catch_0
    move-exception v8

    .line 549
    .local v8, "exception":Ljava/lang/InterruptedException;
    :try_start_4
    sget-object v0, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v3, "Interrupted "

    invoke-virtual {v0, v1, v3, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 550
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 556
    .end local v8    # "exception":Ljava/lang/InterruptedException;
    .end local v10    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v14    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    throw v0

    .line 536
    .local v11, "i$":I
    .restart local v14    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    :catch_1
    move-exception v8

    .line 537
    .restart local v8    # "exception":Ljava/lang/InterruptedException;
    :try_start_5
    sget-object v0, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v3, "Interrupted "

    invoke-virtual {v0, v1, v3, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 538
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 551
    .end local v8    # "exception":Ljava/lang/InterruptedException;
    .restart local v10    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;"
    .local v11, "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v8

    .line 552
    .local v8, "exception":Ljava/util/concurrent/ExecutionException;
    sget-object v0, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception "

    invoke-virtual {v0, v1, v3, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 556
    .end local v8    # "exception":Ljava/util/concurrent/ExecutionException;
    .end local v10    # "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 559
    .end local v6    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v9    # "executor":Ljava/util/concurrent/ExecutorService;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "len$":I
    .end local v14    # "results":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    .end local v15    # "tasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Callable<Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;>;"
    :cond_3
    invoke-interface {v13}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljavax/jmdns/ServiceInfo;

    invoke-interface {v13, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljavax/jmdns/ServiceInfo;

    return-object v0
.end method

.method public listBySubtype(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljavax/jmdns/ServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 568
    const-wide/16 v0, 0x1770

    invoke-virtual {p0, p1, v0, v1}, Ljavax/jmdns/impl/JmmDNSImpl;->listBySubtype(Ljava/lang/String;J)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public listBySubtype(Ljava/lang/String;J)Ljava/util/Map;
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "timeout"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljavax/jmdns/ServiceInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 577
    new-instance v5, Ljava/util/HashMap;

    const/4 v8, 0x5

    invoke-direct {v5, v8}, Ljava/util/HashMap;-><init>(I)V

    .line 578
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;>;"
    invoke-virtual {p0, p1, p2, p3}, Ljavax/jmdns/impl/JmmDNSImpl;->list(Ljava/lang/String;J)[Ljavax/jmdns/ServiceInfo;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/ServiceInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v2, v0, v1

    .line 579
    .local v2, "info":Ljavax/jmdns/ServiceInfo;
    invoke-virtual {v2}, Ljavax/jmdns/ServiceInfo;->getSubtype()Ljava/lang/String;

    move-result-object v7

    .line 580
    .local v7, "subtype":Ljava/lang/String;
    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 581
    new-instance v8, Ljava/util/ArrayList;

    const/16 v9, 0xa

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    :cond_0
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 578
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 586
    .end local v2    # "info":Ljavax/jmdns/ServiceInfo;
    .end local v7    # "subtype":Ljava/lang/String;
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/HashMap;-><init>(I)V

    .line 587
    .local v6, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljavax/jmdns/ServiceInfo;>;"
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 588
    .restart local v7    # "subtype":Ljava/lang/String;
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 589
    .local v3, "infoForSubType":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Ljavax/jmdns/ServiceInfo;

    invoke-interface {v3, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 592
    .end local v3    # "infoForSubType":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceInfo;>;"
    .end local v7    # "subtype":Ljava/lang/String;
    :cond_2
    return-object v6
.end method

.method public networkListeners()[Ljavax/jmdns/NetworkTopologyListener;
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_networkListeners:Ljava/util/Set;

    iget-object v1, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_networkListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljavax/jmdns/NetworkTopologyListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljavax/jmdns/NetworkTopologyListener;

    return-object v0
.end method

.method public registerService(Ljavax/jmdns/ServiceInfo;)V
    .locals 9
    .param p1, "info"    # Ljavax/jmdns/ServiceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 446
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v2

    .line 448
    .local v2, "dnsArray":[Ljavax/jmdns/JmDNS;
    iget-object v7, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v7

    .line 449
    move-object v1, v2

    .local v1, "arr$":[Ljavax/jmdns/JmDNS;
    :try_start_0
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v1, v3

    .line 450
    .local v5, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {p1}, Ljavax/jmdns/ServiceInfo;->clone()Ljavax/jmdns/ServiceInfo;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljavax/jmdns/JmDNS;->registerService(Ljavax/jmdns/ServiceInfo;)V

    .line 449
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 452
    .end local v5    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    move-object v0, p1

    check-cast v0, Ljavax/jmdns/impl/ServiceInfoImpl;

    move-object v6, v0

    invoke-virtual {v6, p0}, Ljavax/jmdns/impl/ServiceInfoImpl;->setDelegate(Ljavax/jmdns/impl/ServiceInfoImpl$Delegate;)V

    .line 453
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, Ljavax/jmdns/ServiceInfo;->getQualifiedName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8, p1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    monitor-exit v7

    .line 455
    return-void

    .line 454
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public registerServiceType(Ljava/lang/String;)V
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 496
    iget-object v4, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceTypes:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 497
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 498
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3, p1}, Ljavax/jmdns/JmDNS;->registerServiceType(Ljava/lang/String;)Z

    .line 497
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 500
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    return-void
.end method

.method public removeNetworkTopologyListener(Ljavax/jmdns/NetworkTopologyListener;)V
    .locals 1
    .param p1, "listener"    # Ljavax/jmdns/NetworkTopologyListener;

    .prologue
    .line 610
    iget-object v0, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_networkListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 611
    return-void
.end method

.method public removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "listener"    # Ljavax/jmdns/ServiceListener;

    .prologue
    .line 404
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 405
    .local v4, "loType":Ljava/lang/String;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 406
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljavax/jmdns/ServiceListener;>;"
    if-eqz v3, :cond_1

    .line 407
    monitor-enter v3

    .line 408
    :try_start_0
    invoke-interface {v3, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 409
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 410
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_serviceListeners:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v6, v4, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 412
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :cond_1
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v5, v0, v1

    .line 415
    .local v5, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v5, p1, p2}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V

    .line 414
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 412
    .end local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "mDNS":Ljavax/jmdns/JmDNS;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 417
    .restart local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    :cond_2
    return-void
.end method

.method public removeServiceTypeListener(Ljavax/jmdns/ServiceTypeListener;)V
    .locals 5
    .param p1, "listener"    # Ljavax/jmdns/ServiceTypeListener;

    .prologue
    .line 368
    iget-object v4, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_typeListeners:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 369
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v0

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 370
    .local v3, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v3, p1}, Ljavax/jmdns/JmDNS;->removeServiceTypeListener(Ljavax/jmdns/ServiceTypeListener;)V

    .line 369
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 372
    .end local v3    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    return-void
.end method

.method public requestServiceInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 309
    const/4 v3, 0x0

    const-wide/16 v4, 0x1770

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->requestServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 310
    return-void
.end method

.method public requestServiceInfo(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    .line 327
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->requestServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 328
    return-void
.end method

.method public requestServiceInfo(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "persistent"    # Z

    .prologue
    .line 318
    const-wide/16 v4, 0x1770

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/impl/JmmDNSImpl;->requestServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 319
    return-void
.end method

.method public requestServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 12
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "persistent"    # Z
    .param p4, "timeout"    # J

    .prologue
    .line 337
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v8

    .local v8, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v10, v8

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v2, v8, v9

    .line 338
    .local v2, "mDNS":Ljavax/jmdns/JmDNS;
    iget-object v11, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_jmDNSExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljavax/jmdns/impl/JmmDNSImpl$3;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Ljavax/jmdns/impl/JmmDNSImpl$3;-><init>(Ljavax/jmdns/impl/JmmDNSImpl;Ljavax/jmdns/JmDNS;Ljava/lang/String;Ljava/lang/String;ZJ)V

    invoke-interface {v11, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 337
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 348
    .end local v2    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    return-void
.end method

.method public textValueUpdated(Ljavax/jmdns/ServiceInfo;[B)V
    .locals 9
    .param p1, "target"    # Ljavax/jmdns/ServiceInfo;
    .param p2, "value"    # [B

    .prologue
    .line 426
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v1

    .line 427
    .local v1, "dnsArray":[Ljavax/jmdns/JmDNS;
    iget-object v7, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v7

    .line 428
    move-object v0, v1

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    :try_start_0
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    .line 429
    .local v5, "mDNS":Ljavax/jmdns/JmDNS;
    check-cast v5, Ljavax/jmdns/impl/JmDNSImpl;

    .end local v5    # "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v5}, Ljavax/jmdns/impl/JmDNSImpl;->getServices()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {p1}, Ljavax/jmdns/ServiceInfo;->getQualifiedName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljavax/jmdns/ServiceInfo;

    .line 430
    .local v3, "info":Ljavax/jmdns/ServiceInfo;
    if-eqz v3, :cond_0

    .line 431
    invoke-virtual {v3, p2}, Ljavax/jmdns/ServiceInfo;->setText([B)V

    .line 428
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 433
    :cond_0
    sget-object v6, Ljavax/jmdns/impl/JmmDNSImpl;->logger:Ljava/util/logging/Logger;

    const-string v8, "We have a mDNS that does not know about the service info being updated."

    invoke-virtual {v6, v8}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_1

    .line 436
    .end local v2    # "i$":I
    .end local v3    # "info":Ljavax/jmdns/ServiceInfo;
    .end local v4    # "len$":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v2    # "i$":I
    .restart local v4    # "len$":I
    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 437
    return-void
.end method

.method public unregisterAllServices()V
    .locals 7

    .prologue
    .line 481
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v1

    .line 482
    .local v1, "dnsArray":[Ljavax/jmdns/JmDNS;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v6

    .line 483
    :try_start_0
    iget-object v5, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v5}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 484
    move-object v0, v1

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 485
    .local v4, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v4}, Ljavax/jmdns/JmDNS;->unregisterAllServices()V

    .line 484
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 487
    .end local v4    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    monitor-exit v6

    .line 488
    return-void

    .line 487
    .end local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public unregisterService(Ljavax/jmdns/ServiceInfo;)V
    .locals 8
    .param p1, "info"    # Ljavax/jmdns/ServiceInfo;

    .prologue
    .line 464
    invoke-virtual {p0}, Ljavax/jmdns/impl/JmmDNSImpl;->getDNS()[Ljavax/jmdns/JmDNS;

    move-result-object v1

    .line 465
    .local v1, "dnsArray":[Ljavax/jmdns/JmDNS;
    iget-object v6, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    monitor-enter v6

    .line 466
    :try_start_0
    iget-object v5, p0, Ljavax/jmdns/impl/JmmDNSImpl;->_services:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {p1}, Ljavax/jmdns/ServiceInfo;->getQualifiedName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    move-object v0, v1

    .local v0, "arr$":[Ljavax/jmdns/JmDNS;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 468
    .local v4, "mDNS":Ljavax/jmdns/JmDNS;
    invoke-virtual {v4, p1}, Ljavax/jmdns/JmDNS;->unregisterService(Ljavax/jmdns/ServiceInfo;)V

    .line 467
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 470
    .end local v4    # "mDNS":Ljavax/jmdns/JmDNS;
    :cond_0
    check-cast p1, Ljavax/jmdns/impl/ServiceInfoImpl;

    .end local p1    # "info":Ljavax/jmdns/ServiceInfo;
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Ljavax/jmdns/impl/ServiceInfoImpl;->setDelegate(Ljavax/jmdns/impl/ServiceInfoImpl$Delegate;)V

    .line 471
    monitor-exit v6

    .line 472
    return-void

    .line 471
    .end local v0    # "arr$":[Ljavax/jmdns/JmDNS;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method
